library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

use work.types.all;

entity RsvStnFInv is
	generic (
		rs_tag : cdb_tag_t := "000010"
	);
	Port (
		clk : in std_logic;
		cdb : in cdb_data_t;
		arbiter_accept : in std_logic;
		unit_in : in rsvstn_fpu_in_t;
		unit_out : out rsvstn_for_arbiter_out_t;
		decoder_accept : out std_logic;
		is_busy : out std_logic;
		current_target : out register_id_t
	);
end RsvStnFInv;

architecture struct of RsvStnFInv is
	component FINV is Port(
	input  :  in STD_LOGIC_VECTOR (31 downto 0);
	output : out STD_LOGIC_VECTOR (31 downto 0);
	clk    :  in STD_LOGIC);
    end component;
    
	signal value1 : value_t := x"00000000";
	signal virtual1 : std_logic := '0';
	signal in_proc : std_logic := '0';
	signal dest_reg : register_id_t := "000000";
	signal result_value : value_t := x"00000000";
	
	signal next_value1 : value_t := x"00000000";
	signal next_virtual1 : std_logic := '0';
	
	signal arbiter_out : std_logic := '0';
	
	signal step_counter : std_logic_vector(1 downto 0) := "00";
begin
	finv_unit : FINV 
		port map (
		input => next_value1,
		output => result_value,
		clk => clk
		);
		
	unit_out.arbiter_output <= arbiter_out;
	
	decoder_accept <= (not in_proc) or (arbiter_out and arbiter_accept);
	is_busy <= in_proc;
	current_target <= dest_reg;
	
	process (unit_in, cdb, value1, virtual1, in_proc, dest_reg, result_value, next_value1, next_virtual1, arbiter_out, step_counter)
	variable calc_value1 : value_t := (others => '0');
	variable calc_virtual1 : std_logic := '0';
	
	begin
		if virtual1 = '1' then
			if cdb.tag1 = value1(5 downto 0) then
				calc_value1 := cdb.value1;
				calc_virtual1 := '0';
			elsif cdb.tag2 = value1(5 downto 0) then
				calc_value1 := cdb.value2;
				calc_virtual1 := '0';
			else
				calc_value1 := value1;
				calc_virtual1 := '1';
			end if;
		else
			calc_value1 := value1;
			calc_virtual1 := '0';
		end if;

		next_value1 <= calc_value1;
		next_virtual1 <= calc_virtual1;
		
		if in_proc = '1' and step_counter = "01" then
			-- data is already available
			arbiter_out <= '1';
			unit_out.arbiter_tag <= rs_tag;
			unit_out.arbiter_reg <= dest_reg;
			unit_out.arbiter_value <= result_value;
		else
			arbiter_out <= '0';
			unit_out.arbiter_tag <= rs_tag;
			unit_out.arbiter_reg <= dest_reg;
			unit_out.arbiter_value <= result_value;
		end if;
	end process;
	
	process (clk)
	begin
		if rising_edge(clk) then
			if unit_in.input_enable = '1' then
				-- assume that decoder is clever enough not to disturb working reservation stations
				value1 <= unit_in.input_value1;
				virtual1 <= unit_in.input_virtual1;
				dest_reg <= unit_in.input_dest_reg;
				in_proc <= '1';
				step_counter <= "00";
			elsif in_proc = '1' then
				if arbiter_out = '1' and arbiter_accept = '1' then
					in_proc <= '0';
				else
					value1 <= next_value1;
					virtual1 <= next_virtual1;
					in_proc <= '1';
					
					if step_counter /= "01" and next_virtual1 = '0' then
						step_counter <= step_counter + "01";
					end if;
				end if;
			end if;
		end if;
	end process;
end struct;
