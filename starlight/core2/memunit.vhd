library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

use work.types.all;

entity RsvStnMem is
	generic (
		rs_tag : cdb_tag_t := "100000"
	);
	Port (
		clk : in std_logic;
		cdb : in cdb_data_t;
		arbiter_accept : in std_logic;
		unit_in : in rsvstn_mem_in_t;
		unit_out : out rsvstn_for_arbiter_out_t;
		decoder_accept : out std_logic;
		is_busy : out std_logic; -- '1' if in reading process
		current_target : out register_id_t;
		
		mc_addr : out std_logic_vector(19 downto 0);
		mc_write_enable : out std_logic;
		mc_write_data : out std_logic_vector(31 downto 0);

		mc_read_enable : out std_logic;
		mc_read_data : in std_logic_vector(31 downto 0);
		mc_read_fetched : in std_logic
	);
end RsvStnMem;

architecture struct of RsvStnMem is
	signal value1 : value_t := x"00000000";
	signal virtual1 : std_logic := '0';
	signal value2 : value_t := x"00000000";
	signal virtual2 : std_logic := '0';
	signal value3 : value_t := x"00000000";
	signal virtual3 : std_logic := '0';
	signal value4 : value_t := x"00000000";
	signal in_proc : std_logic := '0';
	signal dest_reg : register_id_t := "000000";
	signal op_read : std_logic := '1';
	
	signal next_value1 : value_t := x"00000000";
	signal next_value2 : value_t := x"00000000";
	signal next_value3 : value_t := x"00000000";
	signal next_virtual1, next_virtual2, next_virtual3 : std_logic := '0';
	
	signal is_finished : std_logic;
	signal arbiter_out : std_logic;
	signal read_issued : std_logic := '0';
	
begin
	unit_out.arbiter_output <= arbiter_out;
	is_finished <= '1' when next_virtual1 = '0' and next_virtual2 = '0' and next_virtual3 = '0' and (op_read = '0' or mc_read_fetched = '1') else '0';
	decoder_accept <= (not in_proc) or is_finished;
	is_busy <= in_proc and op_read;
	current_target <= dest_reg;
	
	process (unit_in, cdb, arbiter_accept, value1, virtual1, value2, virtual2, value3, virtual3, value4, in_proc, dest_reg, op_read, next_value1, next_value2, next_value3, next_virtual1, next_virtual2, next_virtual3, is_finished, op_read, arbiter_out, read_issued)
	variable calc_value1, calc_value2, calc_value3, calc_output : value_t := (others => '0');
	variable calc_virtual1, calc_virtual2, calc_virtual3: std_logic := '0';
	begin
		if virtual1 = '1' then
			if cdb.tag1 = value1(5 downto 0) then
				calc_value1 := cdb.value1;
				calc_virtual1 := '0';
			elsif cdb.tag2 = value1(5 downto 0) then
				calc_value1 := cdb.value2;
				calc_virtual1 := '0';
			else
				calc_value1 := value1;
				calc_virtual1 := '1';
			end if;
		else
			calc_value1 := value1;
			calc_virtual1 := '0';
		end if;

		next_value1 <= calc_value1;
		next_virtual1 <= calc_virtual1;
		
		if virtual2 = '1' then
			if cdb.tag1 = value2(5 downto 0) then
				calc_value2 := cdb.value1;
				calc_virtual2 := '0';
			elsif cdb.tag2 = value2(5 downto 0) then
				calc_value2 := cdb.value2;
				calc_virtual2 := '0';
			else
				calc_value2 := value2;
				calc_virtual2 := '1';
			end if;
		else
			calc_value2 := value2;
			calc_virtual2 := '0';
		end if;
		
		next_value2 <= calc_value2;
		next_virtual2 <= calc_virtual2;
		
		if virtual3 = '1' then
			if cdb.tag1 = value3(5 downto 0) then
				calc_value3 := cdb.value1;
				calc_virtual3 := '0';
			elsif cdb.tag2 = value3(5 downto 0) then
				calc_value3 := cdb.value2;
				calc_virtual3 := '0';
			else
				calc_value3 := value3;
				calc_virtual3 := '1';
			end if;
		else
			calc_value3 := value3;
			calc_virtual3 := '0';
		end if;
		
		next_value3 <= calc_value3;
		next_virtual3 <= calc_virtual3;
		
		mc_write_data <= next_value1;
		mc_addr <= next_value2(19 downto 0) + next_value3(19 downto 0) + value4(19 downto 0);
		if in_proc = '1' and calc_virtual1 = '0' and calc_virtual2 = '0' and calc_virtual3 = '0' and read_issued = '0' then
			if op_read = '1' then
				mc_read_enable <= '1';
				mc_write_enable <= '0';
			else
				mc_read_enable <= '0';
				mc_write_enable <= '1';
			end if;
		else
			-- arbiter_out <= '0';

			mc_read_enable <= '0';
			mc_write_enable <= '0';
		end if;
		
		if is_finished = '1' and op_read = '1' then
			-- memory unit is defiant of the arbiter
			unit_out.arbiter_value <= mc_read_data; -- TODO: support other instructions
			arbiter_out <= '1';
			unit_out.arbiter_tag <= rs_tag;
			unit_out.arbiter_reg <= dest_reg;
		else
			unit_out.arbiter_value <= mc_read_data; -- TODO: support other instructions
			arbiter_out <= '0';
			unit_out.arbiter_tag <= rs_tag;
			unit_out.arbiter_reg <= dest_reg;
		end if;
	end process;
	
	process (clk)
	begin
		if rising_edge(clk) then
			if unit_in.input_enable = '1' then
				-- assume that decoder is clever enough not to disturb working reservation stations
				value1 <= unit_in.input_value1;
				virtual1 <= unit_in.input_virtual1;
				value2 <= unit_in.input_value2;
				virtual2 <= unit_in.input_virtual2;
				value3 <= unit_in.input_value3;
				virtual3 <= unit_in.input_virtual3;
				value4 <= unit_in.input_value4;
				dest_reg <= unit_in.input_dest_reg;
				op_read <= unit_in.op_read;
				in_proc <= '1';
				read_issued <= '0';
			elsif in_proc = '1' then
				if is_finished = '1' then
					in_proc <= '0';
					read_issued <= '0';
				else
					value1 <= next_value1;
					virtual1 <= next_virtual1;
					value2 <= next_value2;
					virtual2 <= next_virtual2;
					value3 <= next_value3;
					virtual3 <= next_virtual3;
					in_proc <= '1';
					
					if next_virtual1 = '0' and next_virtual2 = '0' and next_virtual3 = '0' then
						read_issued <= '1';
					end if;
				end if;
			end if;
		end if;
	end process;
end struct;
