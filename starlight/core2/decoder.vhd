library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

use work.types.all;

entity Decoder is
	Port (
		clk : in std_logic;
		dec_in : in decoder_in_t;
		dec_out : out decoder_out_t
	);
end Decoder;

architecture struct of Decoder is
	signal value1 : value_t := x"00000000";
	signal virtual1 : std_logic := '0';
	signal value2 : value_t := x"00000000";
	signal virtual2 : std_logic := '0';
	signal value3 : value_t := x"00000000";
	signal virtual3 : std_logic := '0';
	signal value_cr : std_logic_vector(5 downto 0) := "000000";
	signal virtual_cr : std_logic := '0';
	
	signal op_alu, op_fadd, op_fmul, op_finv, op_out, op_mem, op_jmp, op_call : std_logic := '0';
	signal alu_op_kind : alu_op_t := "0000";
	signal is_fsub : std_logic := '0';
	signal mem_read : std_logic := '1';
	signal upd_cr : std_logic := '0';
	
	signal reg_target : register_id_t := "000000";
	signal reg_target_resv : cdb_tag_t := "111111";
	
	-- register is here
	signal reg : register_t := (others => x"00000000");
	signal reg_rsv : register_rsv_t := (others => "111111");
	signal cr : cr_t := "000";
	signal cr_rsv : cdb_tag_t := "111111";
	
	signal cdb1_valid, cdb2_valid, cdb_cr_valid : std_logic := '0';
	
begin
	process (
		dec_in, dec_in.cdb,
		value1, virtual1, value2, virtual2, value3, virtual3, value_cr, virtual_cr,
		op_alu, op_fadd, op_fmul, op_finv, op_out, op_mem, op_jmp, op_call, alu_op_kind, mem_read, upd_cr, is_fsub,
		reg_target, reg_target_resv, reg, reg_rsv, cdb1_valid, cdb2_valid, cdb_cr_valid)
	variable opcode : std_logic_vector(5 downto 0) := "000000";
	variable dest_reg : register_id_t := (others => '0');
	
	variable calc_value1, calc_value2, calc_value3, calc_value4 : value_t := (others => '0');
	variable calc_value_cr : std_logic_vector(5 downto 0) := (others => '0');
	variable calc_virtual1, calc_virtual2, calc_virtual3, calc_virtual_cr : std_logic := '0';
	variable resv_id : cdb_tag_t := (others => '0');
	variable sgn : std_logic := '0';
	
	variable origin_busy : std_logic := '0';
	variable origin_target : register_id_t := (others => '0');
	variable stall_flg : std_logic := '0';
	
	begin
		calc_value1 := x"00000000";
		calc_value2 := x"00000000";
		calc_value3 := x"00000000";
		calc_value4 := x"00000000";
		calc_value_cr := "000000";
		calc_virtual1 := '0';
		calc_virtual2 := '0';
		calc_virtual3 := '0';
		calc_virtual_cr := '0';

		dest_reg := "000000";
		origin_busy := '0';
		origin_target := "000000";
		stall_flg := '0';
		
		origin_busy := dec_in.rsvstn_busy(conv_integer(dec_in.cdb.tag1));
		origin_target := dec_in.rsvstn_target(conv_integer(dec_in.cdb.tag1));

		if origin_busy = '0' or origin_target /= dec_in.cdb.reg1 then
			cdb1_valid <= '1';
		else
			cdb1_valid <= '0';
		end if;
		
		origin_busy := dec_in.rsvstn_busy(conv_integer(dec_in.cdb.tag2));
		origin_target := dec_in.rsvstn_target(conv_integer(dec_in.cdb.tag2));
		
		if origin_busy = '0' or origin_target /= dec_in.cdb.reg2 then
			cdb2_valid <= '1';
		else
			cdb2_valid <= '0';
		end if;

		origin_busy := dec_in.rsvstn_busy(conv_integer(dec_in.cdb.tag_cr));
		origin_target := dec_in.rsvstn_target(conv_integer(dec_in.cdb.tag_cr));
		
		if origin_busy = '0' or origin_target /= "000000" then -- TODO: Don't try to write to 0 register!!!
			cdb_cr_valid <= '1';
		else
			cdb_cr_valid <= '0';
		end if;		
		
		opcode := dec_in.instruction(31 downto 26);
		stall_flg := '0';
		
		if dec_in.inst_enable = '1' and opcode = "111101" and dec_in.read_empty = '0' and dec_in.alu1_accept = '1' then
			dec_out.read_pop <= '1';
		else
			dec_out.read_pop <= '0';
		end if;
		
		if dec_in.inst_enable = '1' then
			if opcode = "000000" then
				-- add
				dest_reg := dec_in.instruction(25 downto 20);
				calc_value1 := x"000000" & "00" & dec_in.instruction(19 downto 14);
				calc_value2 := x"000000" & "00" & dec_in.instruction(13 downto 8);
				calc_virtual1 := '1';
				calc_virtual2 := '1';
				
				op_alu <= '1';
				alu_op_kind <= alu_op_add;
			elsif opcode = "000001" then
				-- addi
				dest_reg := dec_in.instruction(25 downto 20);
				calc_value1 := x"000000" & "00" & dec_in.instruction(19 downto 14);

				sgn := dec_in.instruction(13);	
				calc_value2 := sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & 
							   sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn &
							   dec_in.instruction(13 downto 0);
				calc_virtual1 := '1';
				calc_virtual2 := '0';
				
				op_alu <= '1';
				alu_op_kind <= alu_op_add;
			elsif opcode = "001000" then
				-- movi
				dest_reg := dec_in.instruction(25 downto 20);
				calc_value1 := x"00000000";

				sgn := dec_in.instruction(19);	
				calc_value2 := sgn & sgn & sgn & sgn & sgn & sgn & 
							   sgn & sgn & sgn & sgn & sgn & sgn &
							   dec_in.instruction(19 downto 0);
				calc_virtual1 := '0';
				calc_virtual2 := '0';

				op_alu <= '1';
				alu_op_kind <= alu_op_add;
			elsif opcode = "111101" then
				-- in (-> movi)
				if dec_in.read_empty = '1' then
					stall_flg := '1';
				end if;
				
				dest_reg := dec_in.instruction(25 downto 20);
				calc_value1 := x"00000000";

				calc_value2 := x"000000" & dec_in.read_data;
				calc_virtual1 := '0';
				calc_virtual2 := '0';

				op_alu <= '1';
				alu_op_kind <= alu_op_add;
			elsif opcode = "001010" then
				-- movhi
				dest_reg := dec_in.instruction(25 downto 20);
				calc_value1 := x"000000" & "00" & dec_in.instruction(25 downto 20);
				calc_value2 := dec_in.instruction(19 downto 4) & x"0000";
				calc_virtual1 := '1';
				calc_virtual2 := '0';

				op_alu <= '1';
				alu_op_kind <= alu_op_movhi;
			elsif opcode = "001011" then
				-- movhiz
				dest_reg := dec_in.instruction(25 downto 20);
				calc_value1 := x"00000000";
				calc_value2 := dec_in.instruction(19 downto 4) & x"0000";
				calc_virtual1 := '0';
				calc_virtual2 := '0';

				op_alu <= '1';
				alu_op_kind <= alu_op_add;
			elsif opcode = "000010" then
				-- sub
				dest_reg := dec_in.instruction(25 downto 20);
				calc_value1 := x"000000" & "00" & dec_in.instruction(19 downto 14);
				calc_value2 := x"000000" & "00" & dec_in.instruction(13 downto 8);
				calc_virtual1 := '1';
				calc_virtual2 := '1';
				
				op_alu <= '1';
				alu_op_kind <= alu_op_sub;
			elsif opcode = "000100" or opcode = "000110" then
				-- lshift
				dest_reg := dec_in.instruction(25 downto 20);
				calc_value1 := x"000000" & "00" & dec_in.instruction(19 downto 14);
				calc_value2 := x"000000" & "00" & dec_in.instruction(13 downto 8);
				calc_virtual1 := '1';
				calc_virtual2 := '1';
				
				op_alu <= '1';
				
				if opcode = "000100" then
					alu_op_kind <= alu_op_lshift;
				else 
					alu_op_kind <= alu_op_rshift;
				end if;
			elsif opcode = "000101" or opcode = "000111" then
				-- lshifti
				dest_reg := dec_in.instruction(25 downto 20);
				calc_value1 := x"000000" & "00" & dec_in.instruction(19 downto 14);

				sgn := dec_in.instruction(13);	
				calc_value2 := sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn &
							   sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn &
							   dec_in.instruction(13 downto 8);
				calc_virtual1 := '1';
				calc_virtual2 := '0';
				
				op_alu <= '1';
				if opcode = "000101" then
					alu_op_kind <= alu_op_lshift;
				else 
					alu_op_kind <= alu_op_rshift;
				end if;
			elsif opcode = "011111" then
				-- fabs
				dest_reg := dec_in.instruction(25 downto 20);
				calc_value1 := x"000000" & "00" & dec_in.instruction(19 downto 14);
				calc_virtual1 := '1';
				
				op_alu <= '1';
				alu_op_kind <= alu_op_fabs;
			elsif opcode = "001100" then
				-- cmp
				dest_reg := "000000";
				calc_value1 := x"000000" & "00" & dec_in.instruction(25 downto 20);
				calc_value2 := x"000000" & "00" & dec_in.instruction(19 downto 14);
				calc_virtual1 := '1';
				calc_virtual2 := '1';
				
				op_alu <= '1';
				alu_op_kind <= alu_op_cmp;
			elsif opcode = "011110" then
				-- fcmp
				dest_reg := "000000";
				calc_value1 := x"000000" & "00" & dec_in.instruction(25 downto 20);
				calc_value2 := x"000000" & "00" & dec_in.instruction(19 downto 14);
				calc_virtual1 := '1';
				calc_virtual2 := '1';
				
				op_alu <= '1';
				alu_op_kind <= alu_op_fcmp;
			elsif opcode = "001101" then
				-- cmpi
				dest_reg := "000000";
				calc_value1 := x"000000" & "00" & dec_in.instruction(25 downto 20);
				sgn := dec_in.instruction(19);	
				calc_value2 := sgn & sgn & sgn & sgn & sgn & sgn & 
							   sgn & sgn & sgn & sgn & sgn & sgn &
							   dec_in.instruction(19 downto 0);
				calc_virtual1 := '1';
				calc_virtual2 := '0';
				
				op_alu <= '1';
				alu_op_kind <= alu_op_cmp;
			else
				op_alu <= '0';
				alu_op_kind <= alu_op_add;
			end if;
			
			if opcode = "001100" or opcode = "001101" or opcode = "011110" then
				upd_cr <= '1';
			else
				upd_cr <= '0';
			end if;
			
			if opcode = "110100" then
				-- call
				dest_reg := dec_in.instruction(25 downto 20);
				
				calc_value1 := x"000" & dec_in.instruction(19 downto 0);
				calc_value2 := x"000" & (dec_in.pc + x"00001");
				calc_virtual1 := '0';
				calc_virtual2 := '0';

				op_call <= '1';
			else
				op_call <= '0';
			end if;
			
			if opcode(5 downto 1) = "01000" then
				-- fadd
				dest_reg := dec_in.instruction(25 downto 20);
				calc_value1 := x"000000" & "00" & dec_in.instruction(19 downto 14);
				calc_value2 := x"000000" & "00" & dec_in.instruction(13 downto 8);
				calc_virtual1 := '1';
				calc_virtual2 := '1';
			
				is_fsub <= opcode(0);
				op_fadd <= '1';
			else
				is_fsub <= '0';
				op_fadd <= '0';
			end if;
			
			if opcode = "010010" then
				-- fmul
				dest_reg := dec_in.instruction(25 downto 20);
				calc_value1 := x"000000" & "00" & dec_in.instruction(19 downto 14);
				calc_value2 := x"000000" & "00" & dec_in.instruction(13 downto 8);
				calc_virtual1 := '1';
				calc_virtual2 := '1';
				
				op_fmul <= '1';
			else
				op_fmul <= '0';
			end if;

			if opcode = "010011" then
				-- finv
				dest_reg := dec_in.instruction(25 downto 20);
				calc_value1 := x"000000" & "00" & dec_in.instruction(19 downto 14);
				calc_virtual1 := '1';
				
				op_finv <= '1';
			else
				op_finv <= '0';
			end if;
			
			if opcode = "100000" then
				-- ld
				dest_reg := dec_in.instruction(25 downto 20);
				calc_value2 := x"000000" & "00" & dec_in.instruction(19 downto 14);

				sgn := dec_in.instruction(13);	
				calc_value3 := sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & 
							   sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn &
							   dec_in.instruction(13 downto 0);
				calc_virtual1 := '0';
				calc_virtual2 := '1';
				calc_virtual3 := '0';
				
				op_mem <= '1';
				mem_read <= '1';
			elsif opcode = "100001" then
				-- ld
				dest_reg := dec_in.instruction(25 downto 20);
				calc_value2 := x"000000" & "00" & dec_in.instruction(19 downto 14);
				calc_value3 := x"000000" & "00" & dec_in.instruction(13 downto 8);

				sgn := dec_in.instruction(7);	
				calc_value4 := sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn &
							   sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn &
							   dec_in.instruction(7 downto 0);
				calc_virtual1 := '0';
				calc_virtual2 := '1';
				calc_virtual3 := '1';
				
				op_mem <= '1';
				mem_read <= '1';
			elsif opcode = "100100" then
				-- sto
				dest_reg := "000000";
				calc_value1 := x"000000" & "00" & dec_in.instruction(25 downto 20);
				calc_value2 := x"000000" & "00" & dec_in.instruction(19 downto 14);

				sgn := dec_in.instruction(13);	
				calc_value3 := sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & 
							   sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn &
							   dec_in.instruction(13 downto 0);
				calc_virtual1 := '1';
				calc_virtual2 := '1';
				calc_virtual3 := '0';
				
				op_mem <= '1';
				mem_read <= '0';
			elsif opcode = "100101" then
				-- sto
				dest_reg := "000000";
				calc_value1 := x"000000" & "00" & dec_in.instruction(25 downto 20);
				calc_value2 := x"000000" & "00" & dec_in.instruction(19 downto 14);
				calc_value3 := x"000000" & "00" & dec_in.instruction(13 downto 8);

				sgn := dec_in.instruction(7);	
				calc_value4 := sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn &
							   sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn &
							   dec_in.instruction(7 downto 0);
				calc_virtual1 := '1';
				calc_virtual2 := '1';
				calc_virtual3 := '1';
				
				op_mem <= '1';
				mem_read <= '0';
			else
				op_mem <= '0';
				mem_read <= '0';
			end if;
			
			if opcode = "110000" then
				-- jmp
				calc_value1 := x"000000" & "00" & dec_in.instruction(19 downto 14);
				
				calc_virtual1 := '1';
				calc_virtual_cr := '1';
				op_jmp <= '1';
			elsif opcode = "110001" then
				-- jmp
				calc_value1 := x"000" & dec_in.instruction(19 downto 0);
				
				calc_virtual1 := '0';
				calc_virtual_cr := '1';
				op_jmp <= '1';
			else
				op_jmp <= '0';
			end if;
			
			if opcode = "111100" then
				-- out
				calc_value1 := x"000000" & "00" & dec_in.instruction(25 downto 20);
				calc_virtual1 := '1';
				op_out <= '1';
			else
				op_out <= '0';
			end if;
			
			if opcode = "111111" then
				-- halt
				dec_out.is_halt <= '1';
			else
				dec_out.is_halt <= '0';
			end if;
		else
			op_alu <= '0';
			op_out <= '0';
			op_jmp <= '0';
			op_fadd <= '0';
			op_fmul <= '0';
			op_finv <= '0';
			op_call <= '0';
			op_mem <= '0';
			dec_out.is_halt <= '0';
			upd_cr <= '0';
			
			alu_op_kind <= alu_op_add;
			mem_read <= '0';
			is_fsub <= '0';
			calc_virtual1 := '0';
			calc_virtual2 := '0';
			calc_virtual3 := '0';
			calc_virtual_cr := '0';
			calc_value1 := x"00000000";
			calc_value2 := x"00000000";
			calc_value3 := x"00000000";
			calc_value_cr := "000000";
		end if;
		
		-- retrive real values
		if calc_virtual1 = '1' then
			resv_id := reg_rsv(conv_integer(calc_value1(5 downto 0)));
			origin_busy := dec_in.rsvstn_busy(conv_integer(resv_id));
			origin_target := dec_in.rsvstn_target(conv_integer(resv_id));
			
			if resv_id = "111111" then
				calc_value1 := reg(conv_integer(calc_value1(5 downto 0)));
				calc_virtual1 := '0';
			elsif resv_id = dec_in.cdb.tag1 and calc_value1(5 downto 0) = dec_in.cdb.reg1 and cdb1_valid = '1' then
				calc_value1 := dec_in.cdb.value1;
				calc_virtual1 := '0';
			elsif resv_id = dec_in.cdb.tag2 and calc_value1(5 downto 0) = dec_in.cdb.reg2 and cdb2_valid = '1' then
				calc_value1 := dec_in.cdb.value2;
				calc_virtual1 := '0';
			else
				calc_value1 := x"000000" & "00" & resv_id;
			end if;
		end if;

		if calc_virtual2 = '1' then
			resv_id := reg_rsv(conv_integer(calc_value2(5 downto 0)));
			-- origin_busy := dec_in.rsvstn_busy(conv_integer(resv_id));
			-- origin_target := dec_in.rsvstn_target(conv_integer(resv_id));

			if resv_id = "111111" then
				calc_value2 := reg(conv_integer(calc_value2(5 downto 0)));
				calc_virtual2 := '0';
			elsif resv_id = dec_in.cdb.tag1 and calc_value2(5 downto 0) = dec_in.cdb.reg1 and cdb1_valid = '1' then
				calc_value2 := dec_in.cdb.value1;
				calc_virtual2 := '0';
			elsif resv_id = dec_in.cdb.tag2 and calc_value2(5 downto 0) = dec_in.cdb.reg2 and cdb2_valid = '1' then
				calc_value2 := dec_in.cdb.value2;
				calc_virtual2 := '0';
			else
				calc_value2 := x"000000" & "00" & resv_id;
			end if;
		end if;

		if calc_virtual3 = '1' then
			resv_id := reg_rsv(conv_integer(calc_value3(5 downto 0)));
			-- origin_busy := dec_in.rsvstn_busy(conv_integer(resv_id));
			-- origin_target := dec_in.rsvstn_target(conv_integer(resv_id));

			if resv_id = "111111" then
				calc_value3 := reg(conv_integer(calc_value3(5 downto 0)));
				calc_virtual3 := '0';
			elsif resv_id = dec_in.cdb.tag1 and calc_value3(5 downto 0) = dec_in.cdb.reg1 and cdb1_valid = '1' then
				calc_value3 := dec_in.cdb.value1;
				calc_virtual3 := '0';
			elsif resv_id = dec_in.cdb.tag2 and calc_value3(5 downto 0) = dec_in.cdb.reg2 and cdb2_valid = '1' then
				calc_value3 := dec_in.cdb.value2;
				calc_virtual3 := '0';
			else
				calc_value3 := x"000000" & "00" & resv_id;
			end if;
		end if;

		if calc_virtual_cr = '1' then
			resv_id := cr_rsv;

			if resv_id = "111111" then
				calc_value_cr := "000" & cr;
				calc_virtual_cr := '0';
			elsif resv_id = dec_in.cdb.tag_cr and cdb_cr_valid = '1' then
				calc_value_cr := "000" & dec_in.cdb.value_cr;
				calc_virtual_cr := '0';
			else
				calc_value_cr := resv_id;
			end if;
		end if;
		--value1 <= calc_value1;
		--virtual1 <= calc_virtual1;
		--value2 <= calc_value2;
		--virtual2 <= calc_virtual2;
		--value3 <= calc_value3;
		--virtual3 <= calc_virtual3;
		value_cr <= calc_value_cr;
		virtual_cr <= calc_virtual_cr;
				
		
		if op_alu = '1' and dec_in.alu1_accept = '0' then
			stall_flg := '1';
		end if;
		if op_call = '1' and (dec_in.jmp1_accept = '0' or dec_in.alu1_accept = '0') then
			stall_flg := '1';
		end if;
		if op_fadd = '1' and dec_in.fadd1_accept = '0' then
			stall_flg := '1';
		end if;
		if op_fmul = '1' and dec_in.fmul1_accept = '0' then
			stall_flg := '1';
		end if;
		if op_finv = '1' and dec_in.finv1_accept = '0' then
			stall_flg := '1';
		end if;
		if op_mem = '1' and dec_in.mem1_accept = '0' then
			stall_flg := '1';
		end if;
		if op_out = '1' and dec_in.out1_accept = '0' then
			stall_flg := '1';
		end if;
		if op_jmp = '1' and dec_in.jmp1_accept = '0' then
			stall_flg := '1';
		end if;

		if op_alu = '1' and dec_in.alu1_accept = '1' and stall_flg = '0' then
			dec_out.alu1_data.input_enable <= '1';
			dec_out.alu1_data.input_value1 <= calc_value1;
			dec_out.alu1_data.input_virtual1 <= calc_virtual1;
			dec_out.alu1_data.input_value2 <= calc_value2;
			dec_out.alu1_data.input_virtual2 <= calc_virtual2;
			dec_out.alu1_data.input_dest_reg <= dest_reg;
			dec_out.alu1_data.input_op <= alu_op_kind; -- TODO
		elsif op_call = '1' and dec_in.alu1_accept = '1' and dec_in.jmp1_accept = '1' and stall_flg = '0' then
			dec_out.alu1_data.input_enable <= '1';
			dec_out.alu1_data.input_value1 <= x"00000000";
			dec_out.alu1_data.input_virtual1 <= '0';
			dec_out.alu1_data.input_value2 <= calc_value2;
			dec_out.alu1_data.input_virtual2 <= calc_virtual2;
			dec_out.alu1_data.input_dest_reg <= dest_reg;
			dec_out.alu1_data.input_op <= alu_op_add; -- TODO
		else
			dec_out.alu1_data.input_enable <= '0';
			dec_out.alu1_data.input_value1 <= calc_value1;
			dec_out.alu1_data.input_virtual1 <= calc_virtual1;
			dec_out.alu1_data.input_value2 <= calc_value2;
			dec_out.alu1_data.input_virtual2 <= calc_virtual2;
			dec_out.alu1_data.input_dest_reg <= dest_reg;
			dec_out.alu1_data.input_op <= alu_op_kind; -- TODO
		end if;
		
		if op_fadd = '1' and dec_in.fadd1_accept = '1' then
			dec_out.fadd1_data.input_enable <= '1';
			dec_out.fadd1_data.input_value1 <= calc_value1;
			dec_out.fadd1_data.input_virtual1 <= calc_virtual1;
			dec_out.fadd1_data.input_value2 <= calc_value2;
			dec_out.fadd1_data.input_virtual2 <= calc_virtual2;
			dec_out.fadd1_data.input_dest_reg <= dest_reg;
			dec_out.fadd1_data.input_is_fsub <= is_fsub;
		else
			dec_out.fadd1_data.input_enable <= '0';
			dec_out.fadd1_data.input_value1 <= calc_value1;
			dec_out.fadd1_data.input_virtual1 <= calc_virtual1;
			dec_out.fadd1_data.input_value2 <= calc_value2;
			dec_out.fadd1_data.input_virtual2 <= calc_virtual2;
			dec_out.fadd1_data.input_dest_reg <= dest_reg;
			dec_out.fadd1_data.input_is_fsub <= is_fsub;
		end if;

		if op_fmul = '1' and dec_in.fmul1_accept = '1' then
			dec_out.fmul1_data.input_enable <= '1';
			dec_out.fmul1_data.input_value1 <= calc_value1;
			dec_out.fmul1_data.input_virtual1 <= calc_virtual1;
			dec_out.fmul1_data.input_value2 <= calc_value2;
			dec_out.fmul1_data.input_virtual2 <= calc_virtual2;
			dec_out.fmul1_data.input_dest_reg <= dest_reg;
		else
			dec_out.fmul1_data.input_enable <= '0';
			dec_out.fmul1_data.input_value1 <= calc_value1;
			dec_out.fmul1_data.input_virtual1 <= calc_virtual1;
			dec_out.fmul1_data.input_value2 <= calc_value2;
			dec_out.fmul1_data.input_virtual2 <= calc_virtual2;
			dec_out.fmul1_data.input_dest_reg <= dest_reg;
		end if;

		if op_finv = '1' and dec_in.finv1_accept = '1' then
			dec_out.finv1_data.input_enable <= '1';
			dec_out.finv1_data.input_value1 <= calc_value1;
			dec_out.finv1_data.input_virtual1 <= calc_virtual1;
			dec_out.finv1_data.input_value2 <= x"00000000";
			dec_out.finv1_data.input_virtual2 <= '0';
			dec_out.finv1_data.input_dest_reg <= dest_reg;
		else
			dec_out.finv1_data.input_enable <= '0';
			dec_out.finv1_data.input_value1 <= calc_value1;
			dec_out.finv1_data.input_virtual1 <= calc_virtual1;
			dec_out.finv1_data.input_value2 <= x"00000000";
			dec_out.finv1_data.input_virtual2 <= '0';
			dec_out.finv1_data.input_dest_reg <= dest_reg;
		end if;

		if op_mem = '1' and dec_in.mem1_accept = '1' then
			dec_out.mem1_data.input_enable <= '1';
			dec_out.mem1_data.input_value1 <= calc_value1;
			dec_out.mem1_data.input_virtual1 <= calc_virtual1;
			dec_out.mem1_data.input_value2 <= calc_value2;
			dec_out.mem1_data.input_virtual2 <= calc_virtual2;
			dec_out.mem1_data.input_value3 <= calc_value3;
			dec_out.mem1_data.input_virtual3 <= calc_virtual3;
			dec_out.mem1_data.input_value4 <= x"00000000";
			dec_out.mem1_data.input_dest_reg <= dest_reg;
			dec_out.mem1_data.op_read <= mem_read;
		else
			dec_out.mem1_data.input_enable <= '0';
			dec_out.mem1_data.input_value1 <= calc_value1;
			dec_out.mem1_data.input_virtual1 <= calc_virtual1;
			dec_out.mem1_data.input_value2 <= calc_value2;
			dec_out.mem1_data.input_virtual2 <= calc_virtual2;
			dec_out.mem1_data.input_value3 <= calc_value3;
			dec_out.mem1_data.input_virtual3 <= calc_virtual3;
			dec_out.mem1_data.input_value4 <= x"00000000";
			dec_out.mem1_data.input_dest_reg <= dest_reg;
			dec_out.mem1_data.op_read <= mem_read;
		end if;
		
		if op_out = '1' and dec_in.out1_accept = '1' then
			dec_out.out1_data.input_enable <= '1';
			dec_out.out1_data.input_value1 <= calc_value1;
			dec_out.out1_data.input_virtual1 <= calc_virtual1;
		else
			dec_out.out1_data.input_enable <= '0';
			dec_out.out1_data.input_value1 <= calc_value1;
			dec_out.out1_data.input_virtual1 <= calc_virtual1;
		end if;

		if op_jmp = '1' and dec_in.jmp1_accept = '1' then
			dec_out.jmp1_data.input_value1 <= calc_value1;
			dec_out.jmp1_data.input_virtual1 <= calc_virtual1;
			dec_out.jmp1_data.input_value_cr <= calc_value_cr;
			dec_out.jmp1_data.input_virtual_cr <= calc_virtual_cr;
			dec_out.jmp1_data.input_pc <= dec_in.pc;
			dec_out.jmp1_data.input_op <= dec_in.instruction(23 downto 20);
			dec_out.jmp1_data.input_enable <= '1';
		elsif op_call = '1' and dec_in.alu1_accept = '1' and dec_in.jmp1_accept = '1' then
			dec_out.jmp1_data.input_value1 <= calc_value1;
			dec_out.jmp1_data.input_virtual1 <= calc_virtual1;
			dec_out.jmp1_data.input_value_cr <= calc_value_cr;
			dec_out.jmp1_data.input_virtual_cr <= calc_virtual_cr;
			dec_out.jmp1_data.input_pc <= dec_in.pc;
			dec_out.jmp1_data.input_op <= "0000";
			dec_out.jmp1_data.input_enable <= '1';
		else
			dec_out.jmp1_data.input_value1 <= calc_value1;
			dec_out.jmp1_data.input_virtual1 <= calc_virtual1;
			dec_out.jmp1_data.input_value_cr <= calc_value_cr;
			dec_out.jmp1_data.input_virtual_cr <= calc_virtual_cr;
			dec_out.jmp1_data.input_pc <= dec_in.pc;
			dec_out.jmp1_data.input_op <= dec_in.instruction(23 downto 20);
			dec_out.jmp1_data.input_enable <= '0';
		end if;
		
		dec_out.is_stall <= stall_flg;
		dec_out.is_jmp <= (op_jmp or op_call) and (not stall_flg);

		if stall_flg = '0' then
			if op_alu = '1' or op_call = '1' then
				reg_target_resv <= "000000"; -- rsv. stn. id
				reg_target <= dest_reg;
			elsif op_fadd = '1' then
				reg_target_resv <= "000010"; -- rsv. stn. id
				reg_target <= dest_reg;
			elsif op_fmul = '1' then
				reg_target_resv <= "000011"; -- rsv. stn. id
				reg_target <= dest_reg;
			elsif op_finv = '1' then
				reg_target_resv <= "000100"; -- rsv. stn. id
				reg_target <= dest_reg;
			elsif op_mem = '1' then
				reg_target_resv <= "000001";
				if mem_read = '1' then
					reg_target <= dest_reg;
				else
					reg_target <= "000000";
				end if;
			else
				reg_target_resv <= "111111";
				reg_target <= "000000";
			end if;
		else
			reg_target_resv <= "111111";
			reg_target <= "000000";
		end if;
	end process;
	
	process (clk)
	begin
		if rising_edge(clk) then
			-- reserve register
			if reg_target /= "000000" and reg_target_resv /= "111111" then -- no one is allowed to reserve 0 register!!!
				reg_rsv(conv_integer(reg_target)) <= reg_target_resv;
			end if;
			
			-- reserve CR
			if reg_target_resv /= "111111" and upd_cr = '1' then
				cr_rsv <= reg_target_resv;
			end if;
			
			-- CDB -> register
			if dec_in.cdb.tag1 /= "111111" and dec_in.cdb.reg1 /= reg_target and reg_rsv(conv_integer(dec_in.cdb.reg1)) = dec_in.cdb.tag1 and cdb1_valid = '1' then
				reg_rsv(conv_integer(dec_in.cdb.reg1)) <= "111111";
				reg(conv_integer(dec_in.cdb.reg1)) <= dec_in.cdb.value1;
			end if;
			
			if dec_in.cdb.tag2 /= "111111" and dec_in.cdb.reg2 /= reg_target and reg_rsv(conv_integer(dec_in.cdb.reg2)) = dec_in.cdb.tag2 and cdb2_valid = '1' then
				reg_rsv(conv_integer(dec_in.cdb.reg2)) <= "111111";
				reg(conv_integer(dec_in.cdb.reg2)) <= dec_in.cdb.value2;
			end if;
			
			-- CDB -> CR
			if dec_in.cdb.tag_cr /= "111111" and upd_cr = '0' and cdb_cr_valid = '1' then -- TODO: cdb_cr_valid ?
				cr_rsv <= "111111";
				cr <= dec_in.cdb.value_cr;
			end if;
		end if;
	end process;
end;