library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;

entity SRAM_Model is
	Port (
		MCLK1 : in std_logic;
		XE1, E2A, XE3 : in std_logic;
		XGA, XZCKE, ADVA, XLBO, ZZA, XFT : in std_logic;
		XWA : in std_logic;
		XZBE : in std_logic_vector(3 downto 0);
		ZD : inout std_logic_vector(31 downto 0);
		ZA : in std_logic_vector(19 downto 0);
		ZCLKMA : in std_logic_vector(1 downto 0)
	);
end SRAM_Model;

architecture RTL of SRAM_Model is
	signal cur : std_logic_vector(28 downto 0);
	signal aux : std_logic_vector(26 downto 2);
	signal sr : std_logic_vector(20 downto 0);
	signal counter : std_logic_vector(4 downto 0);
	
	type RAM is array (0 to 1048575) of std_logic_vector(31 downto 0);
	signal values : RAM;
	
	signal rd1, rd2 : std_logic := '0';
	signal adr1, adr2 : std_logic_vector(19 downto 0) := (others => '0');

begin
	process (MCLK1)
	begin
		if (MCLK1'event and MCLK1 = '1') then
			adr2 <= adr1;
			rd2 <= rd1;
			adr1 <= ZA;
			rd1 <= XWA;
			
			if rd2 = '0' then
				values(conv_integer(adr2)) <= ZD;
			end if;
			
			if rd1 = '1' then
				ZD <= values(conv_integer(adr1));
			else
				ZD <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
			end if;
		end if;
	end process;
end RTL;

