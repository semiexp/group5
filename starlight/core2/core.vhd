library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

use work.types.all;

entity StarlightCore2 is
	generic (
		read_magic : std_logic_vector(15 downto 0) := x"0238";
		write_magic : std_logic_vector(15 downto 0) := x"0237"
	);
	Port (
		MCLK1 : in  std_logic;
		RS_TX : out std_logic;
		RS_RX : in std_logic;

		XE1, E2A, XE3 : out std_logic;
		XGA, XZCKE, ADVA, XLBO, ZZA, XFT : out std_logic;
		XWA : out std_logic;
		XZBE : out std_logic_vector(3 downto 0);
		ZD : inout std_logic_vector(31 downto 0);
		ZA : out std_logic_vector(19 downto 0);
		ZCLKMA : out std_logic_vector(1 downto 0)
	 );
end StarlightCore2;

ARCHITECTURE struct OF StarlightCore2 IS
	component clockacc
	port (
	CLKIN_IN        : in    std_logic; 
	RST_IN          : in    std_logic; 
	CLKFX_OUT       : out   std_logic; 
	CLKIN_IBUFG_OUT : out   std_logic; 
	CLK0_OUT        : out   std_logic; 
	LOCKED_OUT      : out   std_logic);
	end component;

	component RS232C is
	generic (
		read_magic : std_logic_vector(15 downto 0) := x"0238";
		write_magic : std_logic_vector(15 downto 0) := x"0237"
	);
	Port (
		CLK : in  std_logic;
		RS_TX : out std_logic;
		RS_RX : in std_logic;

		-- FPGA -> PC
		write_busy : out std_logic;
		write_enable : in std_logic;
		write_data : in std_logic_vector(7 downto 0);
		
		-- PC -> FPGA
		read_empty : out std_logic;
		read_pop : in std_logic;
		read_data : out std_logic_vector(7 downto 0)
	 );
	end component;

	component MemoryController is
	Port (
		CLK : in  std_logic;
		
		-- common
		address : in std_logic_vector(19 downto 0);

		write_enable : in std_logic;
		write_data : in std_logic_vector(31 downto 0);

		read_enable : in std_logic;
		read_data : out std_logic_vector(31 downto 0);
		read_fetched : out std_logic;

		XWA : out std_logic;
		ZD : inout std_logic_vector(31 downto 0);
		ZA : out std_logic_vector(19 downto 0)
	 );
	end component;

	component Decoder is
	Port (
		clk : in std_logic;
		dec_in : in decoder_in_t;
		dec_out : out decoder_out_t
	);
	end component;
	
	component Arbiter is
	Port (
		clk : in std_logic;
		alu1_in : in rsvstn_for_arbiter_out_t;
		mem1_in : in rsvstn_for_arbiter_out_t;
		fadd1_in : in rsvstn_for_arbiter_out_t;
		fmul1_in : in rsvstn_for_arbiter_out_t;
		finv1_in : in rsvstn_for_arbiter_out_t;
		cdb : out cdb_data_t;
		alu1_accept : out std_logic;
		mem1_accept : out std_logic;
		fadd1_accept : out std_logic;
		fmul1_accept : out std_logic;
		finv1_accept : out std_logic
	);
	end component;

	component RsvStnALU is
	generic (
		rs_tag : cdb_tag_t
	);
	Port (
		clk : in std_logic;
		cdb : in cdb_data_t;
		arbiter_accept : in std_logic;
		alu_in : in rsvstn_alu_in_t;
		alu_out : out rsvstn_for_arbiter_out_t;
		decoder_accept : out std_logic;
		is_busy : out std_logic;
		current_target : out register_id_t
	);
	end component;
	
	component RsvStnOut is
	Port (
		clk : in std_logic;
		cdb : in cdb_data_t;
		unit_in : in rsvstn_output_in_t;
		rs232c_busy : in std_logic;
		decoder_accept : out std_logic;
		rs232c_enable : out std_logic;
		rs232c_data : out std_logic_vector(7 downto 0)
	);
	end component;
	
	component RsvStnMem is
	generic (
		rs_tag : cdb_tag_t
	);
	Port (
		clk : in std_logic;
		cdb : in cdb_data_t;
		arbiter_accept : in std_logic;
		unit_in : in rsvstn_mem_in_t;
		unit_out : out rsvstn_for_arbiter_out_t;
		decoder_accept : out std_logic;
		is_busy : out std_logic;
		current_target : out register_id_t;
		
		mc_addr : out std_logic_vector(19 downto 0);
		mc_write_enable : out std_logic;
		mc_write_data : out std_logic_vector(31 downto 0);

		mc_read_enable : out std_logic;
		mc_read_data : in std_logic_vector(31 downto 0);
		mc_read_fetched : in std_logic
	);
	end component;
	
	component RsvStnFAdd is
	generic (
		rs_tag : cdb_tag_t
	);
	Port (
		clk : in std_logic;
		cdb : in cdb_data_t;
		arbiter_accept : in std_logic;
		unit_in : in rsvstn_fpu_in_t;
		unit_out : out rsvstn_for_arbiter_out_t;
		decoder_accept : out std_logic;
		is_busy : out std_logic;
		current_target : out register_id_t
	);
	end component;
	
	component RsvStnFMul is
	generic (
		rs_tag : cdb_tag_t
	);
	Port (
		clk : in std_logic;
		cdb : in cdb_data_t;
		arbiter_accept : in std_logic;
		unit_in : in rsvstn_fpu_in_t;
		unit_out : out rsvstn_for_arbiter_out_t;
		decoder_accept : out std_logic;
		is_busy : out std_logic;
		current_target : out register_id_t
	);
	end component;

	component RsvStnFInv is
	generic (
		rs_tag : cdb_tag_t
	);
	Port (
		clk : in std_logic;
		cdb : in cdb_data_t;
		arbiter_accept : in std_logic;
		unit_in : in rsvstn_fpu_in_t;
		unit_out : out rsvstn_for_arbiter_out_t;
		decoder_accept : out std_logic;
		is_busy : out std_logic;
		current_target : out register_id_t
	);
	end component;

	component RsvStnJmp is
	Port (
		clk : in std_logic;
		cdb : in cdb_data_t;
		unit_in : in rsvstn_jmp_in_t;
		decoder_accept : out std_logic;
		addr_enable : out std_logic;
		addr_value : out std_logic_vector(19 downto 0)
	);
	end component;
	
-- I/O related variables
	signal wbusy, wenable, rempty, rpop, rpop_starter : std_logic := '0';
	signal wdata, rdata : std_logic_vector(7 downto 0);
	
-- Program Loader
	signal read_mode : std_logic_vector(2 downto 0) := (others => '0');
	signal program_size : std_logic_vector(23 downto 0) := (others => '0');
	signal program_read_pos : std_logic_vector(23 downto 0) := (others => '0');
	signal program_code : std_logic_vector(23 downto 0) := (others => '0');

-- Memory Controller
	signal mc_addr : std_logic_vector(19 downto 0) := x"00000";
	signal mc_write_enable, mc_read_enable, mc_read_fetched : std_logic := '0';
	signal mc_write_data, mc_read_data : value_t;
	
-- CPU variables
	signal cpu_awake_pre1, cpu_awake_pre2, cpu_running, decoder_enable, decoder_stall : std_logic := '0';
	signal pcounter : std_logic_vector(19 downto 0) := x"00000";
	signal decoder_pc : std_logic_vector(19 downto 0) := x"00000";
	signal instruction : std_logic_vector(31 downto 0) := x"00000000";
	
	type program_memory_t is array(0 to 32767) of std_logic_vector(31 downto 0);
	signal program_memory : program_memory_t;

	signal acc_rst, acc_clock, acc_dummy1, acc_dummy2, acc_dummy3 : std_logic := '0';
	
	signal CDB : cdb_data_t;
	signal decoder_in : decoder_in_t;
	signal decoder_out : decoder_out_t;
	
	signal alu1_in : rsvstn_alu_in_t;
	signal alu1_out : rsvstn_for_arbiter_out_t;
	signal alu1_is_busy, alu1_decoder_accept, alu1_arbiter_accept : std_logic := '0';
	signal alu1_current_target : register_id_t;

	signal fadd1_in : rsvstn_fpu_in_t;
	signal fadd1_out : rsvstn_for_arbiter_out_t;
	signal fadd1_is_busy, fadd1_decoder_accept, fadd1_arbiter_accept : std_logic := '0';
	signal fadd1_current_target : register_id_t;

	signal fmul1_in : rsvstn_fpu_in_t;
	signal fmul1_out : rsvstn_for_arbiter_out_t;
	signal fmul1_is_busy, fmul1_decoder_accept, fmul1_arbiter_accept : std_logic := '0';
	signal fmul1_current_target : register_id_t;

	signal finv1_in : rsvstn_fpu_in_t;
	signal finv1_out : rsvstn_for_arbiter_out_t;
	signal finv1_is_busy, finv1_decoder_accept, finv1_arbiter_accept : std_logic := '0';
	signal finv1_current_target : register_id_t;

	signal jmp1_in : rsvstn_jmp_in_t;
	signal jmp1_decoder_accept : std_logic := '0';
	signal jmp1_addr_enable : std_logic := '0';
	signal jmp1_addr_value : std_logic_vector(19 downto 0) := (others => '0');

	signal mem1_in : rsvstn_mem_in_t;
	signal mem1_out : rsvstn_for_arbiter_out_t;
	signal mem1_is_busy, mem1_decoder_accept, mem1_arbiter_accept : std_logic := '0';
	signal mem1_current_target : register_id_t;
	
	signal outunit_in : rsvstn_output_in_t;
	signal outunit_decoder_accept : std_logic := '0';
	
	signal waiting_addr : std_logic := '0';
BEGIN
--	acc: clockacc port map (
--		CLKIN_IN => MCLK1,
--		RST_IN => acc_rst,
--		CLKFX_OUT => acc_clock,
--		CLKIN_IBUFG_OUT => acc_dummy1,
--		CLK0_OUT => acc_dummy2,
--		LOCKED_OUT => acc_dummy3
--	);
	io_unit : RS232C
		generic map (
		read_magic => read_magic,
		write_magic => write_magic
		)
		PORT MAP (
		CLK => MCLK1,
		RS_TX => RS_TX,
		RS_RX => RS_RX,
		write_busy => wbusy,
		write_enable => wenable,
		write_data => wdata,
		read_empty => rempty,
		read_pop => rpop,
		read_data => rdata);
	
	mem_unit : MemoryController 
		port map (
		CLK => MCLK1,
		
		address => mc_addr,
		write_enable => mc_write_enable,
		write_data => mc_write_data,
		read_enable => mc_read_enable,
		read_data => mc_read_data,
		read_fetched => mc_read_fetched,

		XWA => XWA,
		ZD => ZD,
		ZA => ZA
		);
		
	decoder_b : Decoder
		port map (
		clk => MCLK1,
		dec_in => decoder_in,
		dec_out => decoder_out
		);

	arbiter_b : Arbiter
		port map (
		clk => MCLK1,
		alu1_in => alu1_out,
		mem1_in => mem1_out,
		fadd1_in => fadd1_out,
		fmul1_in => fmul1_out,
		finv1_in => finv1_out,
		cdb => CDB,
		alu1_accept => alu1_arbiter_accept,
		mem1_accept => mem1_arbiter_accept,
		fadd1_accept => fadd1_arbiter_accept,
		fmul1_accept => fmul1_arbiter_accept,
		finv1_accept => finv1_arbiter_accept
		);
		
	alu1 : RsvStnALU
		generic map (
		rs_tag => "000000"
		)
		port map (
		clk => MCLK1,
		cdb => CDB,
		arbiter_accept => alu1_arbiter_accept,
		alu_in => alu1_in,
		alu_out => alu1_out,
		decoder_accept => alu1_decoder_accept,
		is_busy => alu1_is_busy,
		current_target => alu1_current_target
		);

	fadd1 : RsvStnFAdd
		generic map (
		rs_tag => "000010"
		)
		port map (
		clk => MCLK1,
		cdb => CDB,
		arbiter_accept => fadd1_arbiter_accept,
		unit_in => fadd1_in,
		unit_out => fadd1_out,
		decoder_accept => fadd1_decoder_accept,
		is_busy => fadd1_is_busy,
		current_target => fadd1_current_target
		);

	fmul1 : RsvStnFMul
		generic map (
		rs_tag => "000011"
		)
		port map (
		clk => MCLK1,
		cdb => CDB,
		arbiter_accept => fmul1_arbiter_accept,
		unit_in => fmul1_in,
		unit_out => fmul1_out,
		decoder_accept => fmul1_decoder_accept,
		is_busy => fmul1_is_busy,
		current_target => fmul1_current_target
		);

	finv1 : RsvStnFInv
		generic map (
		rs_tag => "000100"
		)
		port map (
		clk => MCLK1,
		cdb => CDB,
		arbiter_accept => finv1_arbiter_accept,
		unit_in => finv1_in,
		unit_out => finv1_out,
		decoder_accept => finv1_decoder_accept,
		is_busy => finv1_is_busy,
		current_target => finv1_current_target
		);

	jmp1 : RsvStnJmp
		port map (
		clk => MCLK1,
		cdb => CDB,
		unit_in => jmp1_in,
		decoder_accept => jmp1_decoder_accept,
		addr_enable => jmp1_addr_enable,
		addr_value => jmp1_addr_value
		);

	outunit : RsvStnOut
		port map (
		clk => MCLK1,
		cdb => CDB,
		unit_in => outunit_in,
		rs232c_busy => wbusy,
		decoder_accept => outunit_decoder_accept,
		rs232c_enable => wenable,
		rs232c_data => wdata
		);
	
	memunit : RsvStnMem
		generic map (
		rs_tag => "000001"
		)
		port map (
		clk => MCLK1,
		cdb => CDB,
		arbiter_accept => mem1_arbiter_accept,
		unit_in => mem1_in,
		unit_out => mem1_out,
		decoder_accept => mem1_decoder_accept,
		is_busy => mem1_is_busy,
		current_target => mem1_current_target,
		
		mc_addr => mc_addr,
		mc_write_enable => mc_write_enable,
		mc_write_data => mc_write_data,

		mc_read_enable => mc_read_enable,
		mc_read_data => mc_read_data,
		mc_read_fetched => mc_read_fetched
		);
		
	XE1 <= '0';
	E2A <= '1';
	XE3 <= '0';
	XGA <= '0';
	XZCKE <= '0';
	ADVA <= '0';
	XLBO <= '1';
	ZZA <= '0';
	XFT <= '1';
	XZBE <= "0000";
	ZCLKMA(0) <= MCLK1;
	ZCLKMA(1) <= MCLK1;
	
	decoder_in.inst_enable <= decoder_enable;
	decoder_in.instruction <= instruction;
	decoder_in.cdb <= CDB;
	
	decoder_in.alu1_accept <= alu1_decoder_accept;
	decoder_in.fadd1_accept <= fadd1_decoder_accept;
	decoder_in.fmul1_accept <= fmul1_decoder_accept;
	decoder_in.finv1_accept <= finv1_decoder_accept;
	decoder_in.mem1_accept <= mem1_decoder_accept;
	decoder_in.out1_accept <= outunit_decoder_accept;
	decoder_in.jmp1_accept <= jmp1_decoder_accept;
	
	decoder_in.read_empty <= rempty;
	decoder_in.read_data <= rdata;
	
	decoder_in.rsvstn_busy(0) <= alu1_is_busy;
	decoder_in.rsvstn_target(0) <= alu1_current_target;
	decoder_in.rsvstn_busy(1) <= mem1_is_busy;
	decoder_in.rsvstn_target(1) <= mem1_current_target;
	decoder_in.rsvstn_busy(2) <= fadd1_is_busy;
	decoder_in.rsvstn_target(2) <= fadd1_current_target;
	decoder_in.rsvstn_busy(3) <= fmul1_is_busy;
	decoder_in.rsvstn_target(3) <= fmul1_current_target;
	decoder_in.rsvstn_busy(4) <= finv1_is_busy;
	decoder_in.rsvstn_target(4) <= finv1_current_target;
	decoder_in.rsvstn_busy(63) <= '0';
	
	decoder_in.pc <= decoder_pc;
	
	alu1_in <= decoder_out.alu1_data;
	fadd1_in <= decoder_out.fadd1_data;
	fmul1_in <= decoder_out.fmul1_data;
	finv1_in <= decoder_out.finv1_data;
	mem1_in <= decoder_out.mem1_data;
	jmp1_in <= decoder_out.jmp1_data;
	outunit_in <= decoder_out.out1_data;
	decoder_stall <= decoder_out.is_stall;
	
	rpop <= decoder_out.read_pop or rpop_starter;
	
	process(MCLK1)
	begin
		if rising_edge(MCLK1) then
			-- CPU starter
			if cpu_running = '0' then
				if rempty = '0' and rpop_starter = '0' and cpu_awake_pre1 = '0' then
					if read_mode <= "010" then
						read_mode <= read_mode + 1;
						program_size <= rdata & program_size(23 downto 8);
					elsif read_mode = "011" then
						read_mode <= read_mode + 1;
					elsif read_mode = "100" then
						program_code(7 downto 0) <= rdata;
						read_mode <= "101";
					elsif read_mode = "101" then
						program_code(15 downto 8) <= rdata;
						read_mode <= "110";
					elsif read_mode = "110" then
						program_code(23 downto 16) <= rdata;
						read_mode <= "111";
					elsif read_mode = "111" then
						read_mode <= "100";
						
						program_read_pos <= program_read_pos + 1;
						
						if program_read_pos(23 downto 15) = "000000000" then
							program_memory(conv_integer(program_read_pos(14 downto 0))) <= rdata & program_code(23 downto 0);
						end if;
						
						if program_read_pos + 1 = program_size then
							cpu_awake_pre1 <= '1';
							pcounter <= x"00000";
						end if;
					end if;
					
					rpop_starter <= '1';
				else
					-- mem_reading <= '1';
					rpop_starter <= '0';
				end if;
			end if;
					
			if cpu_awake_pre1 = '1' then
				cpu_awake_pre1 <= '0';
				cpu_awake_pre2 <= '1';
			end if;
			if cpu_awake_pre2 = '1' then
				cpu_awake_pre2 <= '0';
				cpu_running <= '1';
			end if;

		
			-- instruction fetch stage
			if cpu_running = '1' and decoder_out.is_halt = '0' and decoder_out.is_jmp = '0' and waiting_addr = '0' then
				decoder_enable <= '1';

				if decoder_stall = '0' then
					pcounter <= pcounter + x"00001";
					instruction <= program_memory(conv_integer(pcounter(14 downto 0)));
					decoder_pc <= pcounter;
				end if;
				
			else
				decoder_enable <= '0';
			end if;
			
			if decoder_out.is_jmp = '1' then
				waiting_addr <= '1';
			end if;
			
			if waiting_addr = '1' and jmp1_addr_enable = '1' then
				waiting_addr <= '0';
				pcounter <= jmp1_addr_value;
			end if;
			
			if decoder_out.is_halt = '1' then
				cpu_running <= '0';
				read_mode <= "000";
				program_read_pos <= x"000000";
				pcounter <= x"00000";
			end if;
		end if;
	end process;
END;
