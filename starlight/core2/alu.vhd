library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

use work.types.all;

entity RsvStnALU is
	generic (
		rs_tag : cdb_tag_t := "000000"
	);
	Port (
		clk : in std_logic;
		cdb : in cdb_data_t;
		arbiter_accept : in std_logic;
		alu_in : in rsvstn_alu_in_t;
		alu_out : out rsvstn_for_arbiter_out_t;
		decoder_accept : out std_logic;
		is_busy : out std_logic;
		current_target : out register_id_t
	);
end RsvStnALU;

architecture struct of RsvStnALU is
	component Shifter is
	Port (
		value : in std_logic_vector (31 downto 0);
		shift_w : in std_logic_vector (31 downto 0); -- right shift width
		output : out std_logic_vector (31 downto 0)
	);
	end component;

	signal value1 : value_t := x"00000000";
	signal virtual1 : std_logic := '0';
	signal value2 : value_t := x"00000000";
	signal virtual2 : std_logic := '0';
	signal in_proc : std_logic := '0';
	signal dest_reg : register_id_t := "000000";
	signal op : alu_op_t := "0000";
	
	signal shift_value, shift_width, shift_out : value_t := x"00000000";
	
	signal next_value1 : value_t := x"00000000";
	signal next_value2 : value_t := x"00000000";
	signal next_virtual1, next_virtual2 : std_logic := '0';
	
	signal arbiter_out, arbiter_cr_out : std_logic := '0';
begin
	shift_unit : Shifter 
		port map (
		value => shift_value,
		shift_w => shift_width,
		output => shift_out
		);
		
	alu_out.arbiter_output <= arbiter_out;
	alu_out.arbiter_cr_output <= arbiter_cr_out;
	
	decoder_accept <= (not in_proc) or (arbiter_out and arbiter_accept) or arbiter_cr_out;
	is_busy <= in_proc;
	current_target <= dest_reg;
	
	process (alu_in, cdb, arbiter_accept, value1, virtual1, value2, virtual2, in_proc, dest_reg, op, next_value1, next_value2, shift_value, shift_width, shift_out)
	variable calc_value1, calc_value2, calc_output : value_t := (others => '0');
	variable calc_virtual1, calc_virtual2 : std_logic := '0';
	variable cmp_sub : std_logic_vector(32 downto 0) := (others => '0');
	variable cmp_value, fcmp_value : std_logic_vector(2 downto 0) := (others => '0');
	
	begin
		if virtual1 = '1' then
			if cdb.tag1 = value1(5 downto 0) then
				calc_value1 := cdb.value1;
				calc_virtual1 := '0';
			elsif cdb.tag2 = value1(5 downto 0) then
				calc_value1 := cdb.value2;
				calc_virtual1 := '0';
			else
				calc_value1 := value1;
				calc_virtual1 := '1';
			end if;
		else
			calc_value1 := value1;
			calc_virtual1 := '0';
		end if;

		next_value1 <= calc_value1;
		next_virtual1 <= calc_virtual1;
		
		if virtual2 = '1' then
			if cdb.tag1 = value2(5 downto 0) then
				calc_value2 := cdb.value1;
				calc_virtual2 := '0';
			elsif cdb.tag2 = value2(5 downto 0) then
				calc_value2 := cdb.value2;
				calc_virtual2 := '0';
			else
				calc_value2 := value2;
				calc_virtual2 := '1';
			end if;
		else
			calc_value2 := value2;
			calc_virtual2 := '0';
		end if;
		
		next_value2 <= calc_value2;
		next_virtual2 <= calc_virtual2;
		
		shift_value <= calc_value1;
		if op = alu_op_rshift then
			shift_width <= calc_value2;
		else
			shift_width <= x"00000000" - calc_value2;
		end if;
		
		cmp_sub := (calc_value1(31) & calc_value1) - (calc_value2(31) & calc_value2);
		if cmp_sub = '0' & x"00000000" then
			cmp_value(0) := '1';
		else
			cmp_value(0) := '0';
		end if;
		cmp_value(1) := (not cmp_value(0)) and (not cmp_sub(32));
		cmp_value(2) := cmp_sub(32);
		
		if calc_value1 = calc_value2 or (calc_value1(30 downto 0) = "000" & x"0000000" and calc_value2(30 downto 0) = "000" & x"0000000") then
			fcmp_value(0) := '1';
		else
			fcmp_value(0) := '0';
		end if;
		
		if calc_value1(31) = '0' and calc_value2(31) = '1' then
			fcmp_value(1) := '1';
		elsif calc_value1(31) = '0' and calc_value2(31) = '0' and ('0' & calc_value1(30 downto 0)) > ('0' & calc_value2(30 downto 0)) then
			fcmp_value(1) := '1';
		elsif calc_value1(31) = '1' and calc_value2(31) = '1' and ('0' & calc_value1(30 downto 0)) < ('0' & calc_value2(30 downto 0)) then
			fcmp_value(1) := '1';
		else
			fcmp_value(1) := '0';
		end if;

		if calc_value1(31) = '1' and calc_value2(31) = '0' then
			fcmp_value(2) := '1';
		elsif calc_value1(31) = '0' and calc_value2(31) = '0' and ('0' & calc_value1(30 downto 0)) < ('0' & calc_value2(30 downto 0)) then
			fcmp_value(2) := '1';
		elsif calc_value1(31) = '1' and calc_value2(31) = '1' and ('0' & calc_value1(30 downto 0)) > ('0' & calc_value2(30 downto 0)) then
			fcmp_value(2) := '1';
		else
			fcmp_value(2) := '0';
		end if;
		
		fcmp_value(1) := fcmp_value(1) and (not fcmp_value(0));
		fcmp_value(2) := fcmp_value(2) and (not fcmp_value(0));

		if in_proc = '1' and calc_virtual1 = '0' and calc_virtual2 = '0' then
			-- data is already available
			if op = alu_op_add then
				alu_out.arbiter_value <= calc_value1 + calc_value2;
			elsif op = alu_op_sub then
				alu_out.arbiter_value <= calc_value1 - calc_value2;
			elsif op = alu_op_movhi then
				alu_out.arbiter_value <= calc_value2(31 downto 16) & calc_value1(15 downto 0);
			elsif op = alu_op_lshift then
				alu_out.arbiter_value <= shift_out;
			elsif op = alu_op_rshift then
				alu_out.arbiter_value <= shift_out;
			elsif op = alu_op_fabs then
				alu_out.arbiter_value <= '0' & calc_value1(30 downto 0);
			else
				alu_out.arbiter_value <= x"00000000";
			end if;
			
			if op = alu_op_cmp then
				arbiter_cr_out <= '1';
				arbiter_out <= '0';
				alu_out.arbiter_cr_tag <= rs_tag;
				alu_out.arbiter_cr_value <= cmp_value;
				alu_out.arbiter_tag <= rs_tag;
				alu_out.arbiter_reg <= dest_reg;
			elsif op = alu_op_fcmp then
				arbiter_cr_out <= '1';
				arbiter_out <= '0';
				alu_out.arbiter_cr_tag <= rs_tag;
				alu_out.arbiter_cr_value <= fcmp_value;
				alu_out.arbiter_tag <= rs_tag;
				alu_out.arbiter_reg <= dest_reg;
			else
				arbiter_cr_out <= '0';
				arbiter_out <= '1';
				alu_out.arbiter_tag <= rs_tag;
				alu_out.arbiter_reg <= dest_reg;
				alu_out.arbiter_cr_tag <= rs_tag;
				alu_out.arbiter_cr_value <= cmp_value;
			end if;
		else
			arbiter_out <= '0';
			arbiter_cr_out <= '0';
			alu_out.arbiter_tag <= rs_tag;
			alu_out.arbiter_reg <= dest_reg;
			alu_out.arbiter_cr_tag <= rs_tag;
			alu_out.arbiter_cr_value <= cmp_value;
			alu_out.arbiter_value <= x"00000000";
		end if;
	end process;
	
	process (clk)
	begin
		if rising_edge(clk) then
			if alu_in.input_enable = '1' then
				-- assume that decoder is clever enough not to disturb working reservation stations
				value1 <= alu_in.input_value1;
				virtual1 <= alu_in.input_virtual1;
				value2 <= alu_in.input_value2;
				virtual2 <= alu_in.input_virtual2;
				dest_reg <= alu_in.input_dest_reg;
				op <= alu_in.input_op;
				in_proc <= '1';
			elsif in_proc = '1' then
				if arbiter_cr_out = '1' or (arbiter_out = '1' and arbiter_accept = '1') then
					in_proc <= '0';
				else
					value1 <= next_value1;
					virtual1 <= next_virtual1;
					value2 <= next_value2;
					virtual2 <= next_virtual2;
					in_proc <= '1';
				end if;
			end if;
		end if;
	end process;
end struct;
