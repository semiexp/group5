.globl min_caml_finv
min_caml_finv:
rshifti r2, r1, 23
lshifti r3, r2, 23
cmp r1, r3
jmp !z, finv_normal
movhiz r3, 0x7f00
sub r1, r3, r1
jmp r60

finv_normal:
sub r4, r1, r3
movhiz r5, 0x3f80
add r5, r5, r4 # r5 = (normalized) r1
movhiz r6, 0x4000
movhiz r7, 0x3f00 # r7 = x
fmul r8, r5, r7
fsub r8, r6, r8
fmul r7, r7, r8 # r4 = x
fmul r8, r5, r7
fsub r8, r6, r8
fmul r7, r7, r8 # r4 = x
fmul r8, r5, r7
fsub r8, r6, r8
fmul r7, r7, r8 # r4 = x
fmul r8, r5, r7
fsub r8, r6, r8
fmul r7, r7, r8 # r4 = x
fmul r8, r5, r7
fsub r8, r6, r8
fmul r7, r7, r8 # r4 = x
lshifti r7, r7, 9
rshifti r7, r7, 9
movhiz r4, 0x7e80
sub r1, r4, r3
add r1, r1, r7
jmp r60

