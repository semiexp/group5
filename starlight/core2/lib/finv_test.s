.globl min_caml_start
min_caml_start:
movhiz r1, 0x3f99
call r60, min_caml_finv
rshifti r2, r1, 24
out r2
rshifti r2, r1, 16
out r2
rshifti r2, r1, 8
out r2
rshifti r2, r1, 0
out r2
halt

