library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

use work.types.all;

entity RsvStnJmp is
	Port (
		clk : in std_logic;
		cdb : in cdb_data_t;
		unit_in : in rsvstn_jmp_in_t;
		decoder_accept : out std_logic;
		addr_enable : out std_logic;
		addr_value : out std_logic_vector(19 downto 0)
	);
end RsvStnJmp;

architecture struct of RsvStnJmp is
	signal value1 : value_t := x"00000000";
	signal virtual1 : std_logic := '0';
	signal value_cr : std_logic_vector(5 downto 0) := "000000";
	signal virtual_cr : std_logic := '0';
	signal in_proc : std_logic := '0';
	signal op : jmp_op_t := "0000";
	signal pc : std_logic_vector(19 downto 0) := (others => '0');
	
	signal next_value1 : value_t := x"00000000";
	signal next_value_cr : std_logic_vector(5 downto 0) := "000000";
	signal next_virtual1, next_virtual_cr : std_logic := '0';
	
	signal addr_out : std_logic := '0';
begin
	decoder_accept <= (not in_proc) or addr_out;
	addr_enable <= addr_out;
	
	process (unit_in, cdb, value1, virtual1, value_cr, virtual_cr, in_proc, op, next_value1, next_value_cr, next_virtual1, next_virtual_cr, addr_out)
	variable calc_value1 : value_t := (others => '0');
	variable calc_value_cr : std_logic_vector(5 downto 0) := (others => '0');
	variable calc_virtual1, calc_virtual_cr : std_logic := '0';

	variable cmp_sub : std_logic_vector(32 downto 0) := (others => '0');
	variable cmp_value : std_logic_vector(2 downto 0) := (others => '0');
	variable next_addr : std_logic_vector(19 downto 0) := (others => '0');
	
	begin
		if virtual1 = '1' then
			if cdb.tag1 = value1(5 downto 0) then
				calc_value1 := cdb.value1;
				calc_virtual1 := '0';
			elsif cdb.tag2 = value1(5 downto 0) then
				calc_value1 := cdb.value2;
				calc_virtual1 := '0';
			else
				calc_value1 := value1;
				calc_virtual1 := '1';
			end if;
		else
			calc_value1 := value1;
			calc_virtual1 := '0';
		end if;

		next_value1 <= calc_value1;
		next_virtual1 <= calc_virtual1;
		
		if virtual_cr = '1' then
			if cdb.tag_cr = value_cr then
				calc_value_cr := "000" & cdb.value_cr;
				calc_virtual_cr := '0';
			else
				calc_value_cr := value_cr;
				calc_virtual_cr := '1';
			end if;
		else
			calc_value_cr := value_cr;
			calc_virtual_cr := '0';
		end if;
		
		next_value_cr <= calc_value_cr;
		next_virtual_cr <= calc_virtual_cr;
		
		if op(3 downto 1) = "000" or
		  (op(3 downto 1) = "001" and op(0) = next_value_cr(0)) or 
		  (op(3 downto 1) = "010" and op(0) = next_value_cr(1)) or
		  (op(3 downto 1) = "011" and op(0) = next_value_cr(2)) then
			next_addr := next_value1(19 downto 0);
		else
			next_addr := pc + x"00001";
		end if;
		
		if in_proc = '1' and calc_virtual1 = '0' and calc_virtual_cr = '0' then
			-- data is already available
			addr_out <= '1';
			addr_value <= next_addr;
		else
			addr_out <= '0';
			addr_value <= next_addr;
		end if;
	end process;
	
	process (clk)
	begin
		if rising_edge(clk) then
			if unit_in.input_enable = '1' then
				-- assume that decoder is clever enough not to disturb working reservation stations
				value1 <= unit_in.input_value1;
				virtual1 <= unit_in.input_virtual1;
				value_cr <= unit_in.input_value_cr;
				virtual_cr <= unit_in.input_virtual_cr;
				pc <= unit_in.input_pc;
				op <= unit_in.input_op;
				in_proc <= '1';
			elsif in_proc = '1' then
				if addr_out = '1' then
					in_proc <= '0';
				else
					value1 <= next_value1;
					virtual1 <= next_virtual1;
					value_cr <= next_value_cr;
					virtual_cr <= next_virtual_cr;
					in_proc <= '1';
				end if;
			end if;
		end if;
	end process;
end struct;
