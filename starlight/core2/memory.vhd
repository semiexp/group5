library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity MemoryController is
	Port (
		CLK : in  std_logic;
		
		-- common
		address : in std_logic_vector(19 downto 0);

		write_enable : in std_logic;
		write_data : in std_logic_vector(31 downto 0);

		read_enable : in std_logic;
		read_data : out std_logic_vector(31 downto 0);
		read_fetched : out std_logic;

		XWA : out std_logic;
		ZD : inout std_logic_vector(31 downto 0);
		ZA : out std_logic_vector(19 downto 0)
	 );
end MemoryController;

architecture struct of MemoryController is
	-- cache is direct mapped, index size is 8bit
	type cache_pohepohe_t is array(0 to 255) of std_logic_vector(11 downto 0);
	type cache_data_t is array(0 to 255) of std_logic_vector(31 downto 0);

	signal cache_pohepohe : cache_pohepohe_t := (others => x"fff");
	signal cache_data : cache_data_t := (others => x"12345678");
	signal rd_mem_access : std_logic := '0';
	
	signal reading1, reading2 : std_logic := '1';
	signal fetching1, fetching2, fetching3 : std_logic := '0';
	signal wval1, wval2 : std_logic_vector(31 downto 0) := x"00000000";
	signal addr1, addr2 : std_logic_vector(19 downto 0) := x"00000";
	
	signal mem_addr : std_logic_vector(19 downto 0) := x"00000";
	
	signal ZD_latch : std_logic_vector(31 downto 0) := x"00000000";
	
	signal rwrite_val1, rwrite_val2, rwrite_val3 : std_logic_vector(31 downto 0) := x"00000000";
	signal rwrite_addr1, rwrite_addr2, rwrite_addr3 : std_logic_vector(19 downto 0) := x"00000";

begin
	Za <= mem_addr;
	
	process (address, write_enable, write_data, read_enable, ZD, cache_pohepohe, cache_data, fetching2, fetching3, ZD_latch)
	variable tag : std_logic_vector(11 downto 0);
	variable index : std_logic_vector(7 downto 0);
	
	begin
		index := address(7 downto 0);
		tag := cache_pohepohe(conv_integer(index));
		mem_addr <= address;
		
		if read_enable = '1' then
			XWA <= '1';
			if address = rwrite_addr1 then
				read_data <= rwrite_val1;
				read_fetched <= '1';
				rd_mem_access <= '0';
			elsif address = rwrite_addr2 then
				read_data <= rwrite_val2;
				read_fetched <= '1';
				rd_mem_access <= '0';
			elsif address = rwrite_addr3 then
				read_data <= rwrite_val3;
				read_fetched <= '1';
				rd_mem_access <= '0';
			elsif false and tag = address(19 downto 8) then -- 'false': temporarily disabling cache
				read_data <= cache_data(conv_integer(index));
				read_fetched <= '1';
				rd_mem_access <= '0';
			else
				-- begin fetching
				read_data <= ZD_latch;
				read_fetched <= fetching3;
				rd_mem_access <= '1';
			end if;
		elsif write_enable = '1' then
			read_fetched <= fetching3;
			read_data <= ZD_latch;
			rd_mem_access <= '0';

			XWA <= '0';
		else
			read_fetched <= fetching3;
			read_data <= ZD_latch;
			rd_mem_access <= '0';

			XWA <= '1';
		end if;
	end process;
	
	process(CLK)
	variable tag : std_logic_vector(11 downto 0) := (others => '0');
	variable index : std_logic_vector(7 downto 0) := (others => '0');
	begin
		if rising_edge(CLK) then
			reading2 <= reading1;
			wval2 <= wval1;
			fetching3 <= fetching2;
			fetching2 <= fetching1;
			addr2 <= addr1;
			ZD_latch <= ZD;
			
			-- todo: if two writing to cache happen at the same time, what will happen?
			if fetching3 = '1' then
				-- store to cache
				index := addr2(7 downto 0);
				tag := cache_pohepohe(conv_integer(index));
				cache_pohepohe(conv_integer(index)) <= addr2(19 downto 8);
				cache_data(conv_integer(index)) <= ZD_latch;
			end if;
			
			if reading1 = '1' then
				ZD <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
			else
				ZD <= wval1;
			end if;
			
			if rd_mem_access = '1' then
			--	reading1 <= '1';
				fetching1 <= '1';
				addr1 <= address;
			else
				fetching1 <= '0';
			end if;
			
			if write_enable = '1' then
				rwrite_val1 <= write_data;
				rwrite_addr1 <= address;
				rwrite_val2 <= rwrite_val1;
				rwrite_addr2 <= rwrite_addr1;
				rwrite_val3 <= rwrite_val2;
				rwrite_addr3 <= rwrite_addr2;
				
				index := address(7 downto 0);
				tag := cache_pohepohe(conv_integer(index));
				if tag = address(19 downto 8) then
					cache_data(conv_integer(index)) <= write_data;
				end if;
				
				wval1 <= write_data;
				reading1 <= '0';
			else
				reading1 <= '1';
			end if;
			
		end if;
	end process;
end;
