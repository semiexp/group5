library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

use work.types.all;

entity Arbiter is
	Port (
		clk : in std_logic;
		alu1_in : in rsvstn_for_arbiter_out_t;
		mem1_in : in rsvstn_for_arbiter_out_t;
		fadd1_in : in rsvstn_for_arbiter_out_t;
		fmul1_in : in rsvstn_for_arbiter_out_t;
		finv1_in : in rsvstn_for_arbiter_out_t;
		cdb : out cdb_data_t;
		alu1_accept : out std_logic;
		mem1_accept : out std_logic;
		fadd1_accept : out std_logic;
		fmul1_accept : out std_logic;
		finv1_accept : out std_logic
	);
end Arbiter;

architecture struct of Arbiter is
	signal mem1_dest, alu1_dest, fadd1_dest, fmul1_dest, finv1_dest : std_logic_vector(0 to 1) := "00";
begin
	process (alu1_in, mem1_in, fadd1_in, fmul1_in, finv1_in,
	mem1_dest, alu1_dest, fadd1_dest, fmul1_dest, finv1_dest)
	begin
		mem1_dest <= mem1_in.arbiter_output & '0';
		
		if alu1_in.arbiter_output = '1' then
			if mem1_dest(0) = '0' then
				alu1_dest <= "10";
			elsif mem1_dest(1) = '0' then
				alu1_dest <= "01";
			else
				alu1_dest <= "00";
			end if;
		else
			alu1_dest <= "00";
		end if;

		if fadd1_in.arbiter_output = '1' then
			if mem1_dest(0) = '0' and alu1_dest(0) = '0' then
				fadd1_dest <= "10";
			elsif mem1_dest(1) = '0' and alu1_dest(1) = '0' then
				fadd1_dest <= "01";
			else
				fadd1_dest <= "00";
			end if;
		else
			fadd1_dest <= "00";
		end if;
		
		if fmul1_in.arbiter_output = '1' then
			if mem1_dest(0) = '0' and alu1_dest(0) = '0' and fadd1_dest(0) = '0' then
				fmul1_dest <= "10";
			elsif mem1_dest(1) = '0' and alu1_dest(1) = '0' and fadd1_dest(1) = '0' then
				fmul1_dest <= "01";
			else
				fmul1_dest <= "00";
			end if;
		else
			fmul1_dest <= "00";
		end if;

		if finv1_in.arbiter_output = '1' then
			if mem1_dest(0) = '0' and alu1_dest(0) = '0' and fadd1_dest(0) = '0' and fmul1_dest(0) = '0' then
				finv1_dest <= "10";
			elsif mem1_dest(1) = '0' and alu1_dest(1) = '0' and fadd1_dest(1) = '0' and fadd1_dest(1) = '0' then
				finv1_dest <= "01";
			else
				finv1_dest <= "00";
			end if;
		else
			finv1_dest <= "00";
		end if;
		
		mem1_accept <= mem1_dest(0) or mem1_dest(1);
		alu1_accept <= alu1_dest(0) or alu1_dest(1);
		fadd1_accept <= fadd1_dest(0) or fadd1_dest(1);
		fmul1_accept <= fmul1_dest(0) or fmul1_dest(1);
		finv1_accept <= finv1_dest(0) or finv1_dest(1);
	end process;
	
	process (clk)
	begin
		if rising_edge(clk) then
			if mem1_dest(0) = '1' then
				cdb.tag1 <= mem1_in.arbiter_tag;
				cdb.reg1 <= mem1_in.arbiter_reg;
				cdb.value1 <= mem1_in.arbiter_value;
			elsif alu1_dest(0) = '1' then
				cdb.tag1 <= alu1_in.arbiter_tag;
				cdb.reg1 <= alu1_in.arbiter_reg;
				cdb.value1 <= alu1_in.arbiter_value;
			elsif fadd1_dest(0) = '1' then
				cdb.tag1 <= fadd1_in.arbiter_tag;
				cdb.reg1 <= fadd1_in.arbiter_reg;
				cdb.value1 <= fadd1_in.arbiter_value;
			elsif fmul1_dest(0) = '1' then
				cdb.tag1 <= fmul1_in.arbiter_tag;
				cdb.reg1 <= fmul1_in.arbiter_reg;
				cdb.value1 <= fmul1_in.arbiter_value;
			elsif finv1_dest(0) = '1' then
				cdb.tag1 <= finv1_in.arbiter_tag;
				cdb.reg1 <= finv1_in.arbiter_reg;
				cdb.value1 <= finv1_in.arbiter_value;
			else
				cdb.tag1 <= "111111";
			end if;
			
			if mem1_dest(1) = '1' then
				cdb.tag2 <= mem1_in.arbiter_tag;
				cdb.reg2 <= mem1_in.arbiter_reg;
				cdb.value2 <= mem1_in.arbiter_value;
			elsif alu1_dest(1) = '1' then
				cdb.tag2 <= alu1_in.arbiter_tag;
				cdb.reg2 <= alu1_in.arbiter_reg;
				cdb.value2 <= alu1_in.arbiter_value;
			elsif fadd1_dest(1) = '1' then
				cdb.tag2 <= fadd1_in.arbiter_tag;
				cdb.reg2 <= fadd1_in.arbiter_reg;
				cdb.value2 <= fadd1_in.arbiter_value;
			elsif fmul1_dest(1) = '1' then
				cdb.tag2 <= fmul1_in.arbiter_tag;
				cdb.reg2 <= fmul1_in.arbiter_reg;
				cdb.value2 <= fmul1_in.arbiter_value;
			elsif finv1_dest(1) = '1' then
				cdb.tag2 <= finv1_in.arbiter_tag;
				cdb.reg2 <= finv1_in.arbiter_reg;
				cdb.value2 <= finv1_in.arbiter_value;
			else
				cdb.tag2 <= "111111";
			end if;
			
			if alu1_in.arbiter_cr_output = '1' then
				cdb.tag_cr <= alu1_in.arbiter_cr_tag;
				cdb.value_cr <= alu1_in.arbiter_cr_value;
			else
				cdb.tag_cr <= "111111";
			end if;
			
		end if;
	end process;
end;