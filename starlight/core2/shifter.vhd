library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity Shifter is
	Port (
		value : in std_logic_vector (31 downto 0);
		shift_w : in std_logic_vector (31 downto 0); -- right shift width
		output : out std_logic_vector (31 downto 0));
end Shifter;

architecture struct of Shifter is
	signal shift_w_neg : std_logic_vector (31 downto 0);
	
	signal value_r_1, value_r_2, value_r_4, value_r_8, value_r_16, value_r : std_logic_vector (31 downto 0);
	signal value_l_1, value_l_2, value_l_4, value_l_8, value_l_16, value_l : std_logic_vector (31 downto 0);
	
begin
	shift_w_neg <= x"00000000" - shift_w;
	
	value_r_1 <= ("0" & value(31 downto 1)) when shift_w(0) = '1' else value;
	value_r_2 <= ("00" & value_r_1(31 downto 2)) when shift_w(1) = '1' else value_r_1;
	value_r_4 <= (x"0" & value_r_2(31 downto 4)) when shift_w(2) = '1' else value_r_2;
	value_r_8 <= (x"00" & value_r_4(31 downto 8)) when shift_w(3) = '1' else value_r_4;
	value_r_16 <= (x"0000" & value_r_8(31 downto 16)) when shift_w(4) = '1' else value_r_8;
	value_r <= value_r_16 when shift_w(31 downto 5) = "00000000000000000000000000" else x"00000000";
	
	value_l_1 <= (value(30 downto 0) & "0") when shift_w_neg(0) = '1' else value;
	value_l_2 <= (value_l_1(29 downto 0) & "00") when shift_w_neg(1) = '1' else value_l_1;
	value_l_4 <= (value_l_2(27 downto 0) & x"0") when shift_w_neg(2) = '1' else value_l_2;
	value_l_8 <= (value_l_4(23 downto 0) & x"00") when shift_w_neg(3) = '1' else value_l_4;
	value_l_16 <= (value_l_8(15 downto 0) & x"0000") when shift_w_neg(4) = '1' else value_l_8;
	value_l <= value_l_16 when shift_w_neg(31 downto 5) = "00000000000000000000000000" else x"00000000";

	output <= value_l when shift_w(31) = '1' else value_r;
end struct;

