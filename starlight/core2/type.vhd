library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

package types is
	subtype instruction_t is std_logic_vector(31 downto 0);
	subtype value_t is std_logic_vector(31 downto 0);

	subtype cdb_tag_t is std_logic_vector(5 downto 0);
	subtype register_id_t is std_logic_vector(5 downto 0);
	subtype cr_t is std_logic_vector(2 downto 0);
	subtype alu_op_t is std_logic_vector(3 downto 0);
	subtype jmp_op_t is std_logic_vector(3 downto 0);
	
	constant alu_op_add : alu_op_t := "0000";
	constant alu_op_sub : alu_op_t := "0001";
	constant alu_op_movhi : alu_op_t := "0010";
	constant alu_op_cmp : alu_op_t := "0011";
	constant alu_op_lshift : alu_op_t := "0100";
	constant alu_op_rshift : alu_op_t := "0101";
	constant alu_op_fabs : alu_op_t := "0110";
	constant alu_op_fcmp : alu_op_t := "0111";
	
	type register_t is array(0 to 63) of value_t;
	type register_rsv_t is array(0 to 63) of cdb_tag_t;
	type rsvstn_target_t is array(0 to 63) of register_id_t;
	
	type cdb_data_t is record
		tag1 : cdb_tag_t;
		value1 : value_t;
		reg1 : register_id_t;

		tag2 : cdb_tag_t;
		value2 : value_t;
		reg2 : register_id_t;
		
		tag_cr : cdb_tag_t;
		value_cr : cr_t;
	end record;
	
	type rsvstn_alu_in_t is record
		-- for decoder
		input_enable : std_logic;
		input_value1 : value_t;
		input_virtual1 : std_logic;
		input_value2 : value_t;
		input_virtual2 : std_logic;
		input_dest_reg : register_id_t;
		input_op : alu_op_t;
	end record;

	type rsvstn_fpu_in_t is record
		-- for decoder
		input_enable : std_logic;
		input_value1 : value_t;
		input_virtual1 : std_logic;
		input_value2 : value_t;
		input_virtual2 : std_logic;
		input_dest_reg : register_id_t;
		
		input_is_fsub : std_logic;
	end record;

	type rsvstn_jmp_in_t is record
		-- for decoder
		input_enable : std_logic;
		input_value1 : value_t;
		input_virtual1 : std_logic;
		input_value_cr : std_logic_vector(5 downto 0);
		input_virtual_cr : std_logic;
		input_pc : std_logic_vector(19 downto 0);
		input_op : jmp_op_t;
	end record;

	type rsvstn_mem_in_t is record
		-- for decoder
		-- value : input_value1
		-- address : input_value2 + input_value3 + input_value4
		input_enable : std_logic;
		input_value1 : value_t;
		input_virtual1 : std_logic;
		input_value2 : value_t;
		input_virtual2 : std_logic;
		input_value3 : value_t;
		input_virtual3 : std_logic;
		input_value4 : value_t;
		input_dest_reg : register_id_t;
		op_read : std_logic;
	end record;
	
	type rsvstn_for_arbiter_out_t is record
		arbiter_output : std_logic;
		arbiter_value : value_t;
		arbiter_tag : cdb_tag_t;
		arbiter_reg : register_id_t;
		
		arbiter_cr_output : std_logic;
		arbiter_cr_value : cr_t;
		arbiter_cr_tag : cdb_tag_t;
	end record;
	
	type rsvstn_output_in_t is record
		-- for decoder
		input_enable : std_logic;
		input_value1 : value_t;
		input_virtual1 : std_logic;
	end record;
	
	type decoder_in_t is record
		-- for IO
		read_empty : std_logic;
		read_data : std_logic_vector(7 downto 0);
		
		-- for IF
		inst_enable : std_logic;
		instruction : instruction_t;
		
		-- CDB
		cdb : cdb_data_t;
		
		-- input from reservation stations
		alu1_accept : std_logic;
		mem1_accept : std_logic;
		out1_accept : std_logic;
		jmp1_accept : std_logic;
		fadd1_accept : std_logic;
		fmul1_accept : std_logic;
		finv1_accept : std_logic;
		
		-- status of reservation stations
		rsvstn_busy : std_logic_vector(63 downto 0);
		rsvstn_target : rsvstn_target_t;
		
		pc : std_logic_vector(19 downto 0);
	end record;
	
	type decoder_out_t is record
		-- output to reservation stations
		alu1_data : rsvstn_alu_in_t;
		mem1_data : rsvstn_mem_in_t;
		out1_data : rsvstn_output_in_t;
		jmp1_data : rsvstn_jmp_in_t;
		fadd1_data : rsvstn_fpu_in_t;
		fmul1_data : rsvstn_fpu_in_t;
		finv1_data : rsvstn_fpu_in_t;
		
		is_halt : std_logic;
		is_stall : std_logic;
		is_jmp : std_logic;
		
		read_pop : std_logic;
	end record;
end;

