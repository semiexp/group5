library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

use work.types.all;

entity RsvStnOut is
	Port (
		clk : in std_logic;
		cdb : in cdb_data_t;
		unit_in : in rsvstn_output_in_t;
		rs232c_busy : in std_logic;
		decoder_accept : out std_logic;
		rs232c_enable : out std_logic;
		rs232c_data : out std_logic_vector(7 downto 0)
	);
end RsvStnOut;

architecture struct of RsvStnOut is
	signal value1 : value_t := x"00000000";
	signal virtual1 : std_logic := '0';
	signal in_proc : std_logic := '0';
	
	signal next_value1 : value_t := x"00000000";
	signal next_virtual1 : std_logic := '0';
	
	signal io_out : std_logic := '0';

begin
	rs232c_enable <= in_proc and (not next_virtual1) and (not rs232c_busy);
	decoder_accept <= (not in_proc) or (in_proc and (not next_virtual1) and (not rs232c_busy));
	
	process (unit_in, cdb, value1, virtual1, in_proc, rs232c_busy)
	variable calc_value1 : value_t := (others => '0');
	variable calc_virtual1 : std_logic := '0';
	begin
		if virtual1 = '1' then
			if cdb.tag1 = value1(5 downto 0) then
				calc_value1 := cdb.value1;
				calc_virtual1 := '0';
			elsif cdb.tag2 = value1(5 downto 0) then
				calc_value1 := cdb.value2;
				calc_virtual1 := '0';
			else
				calc_value1 := value1;
				calc_virtual1 := '1';
			end if;
		else
			calc_value1 := value1;
			calc_virtual1 := '0';
		end if;

		next_value1 <= calc_value1;
		next_virtual1 <= calc_virtual1;
		
		if in_proc = '1' and rs232c_busy = '0' and calc_virtual1 = '0' then
			-- data is already available
			io_out <= '1';
			rs232c_data <= calc_value1(7 downto 0);
		else
			io_out <= '0';
			rs232c_data <= calc_value1(7 downto 0);
		end if;
	end process;
	
	process (clk)
	begin
		if rising_edge(clk) then
			if unit_in.input_enable = '1' then
				-- assume that decoder is clever enough not to disturb working reservation stations
				value1 <= unit_in.input_value1;
				virtual1 <= unit_in.input_virtual1;
				in_proc <= '1';
			elsif in_proc = '1' then
				if next_virtual1 = '0' and rs232c_busy = '0' then
					in_proc <= '0';
				else
					value1 <= next_value1;
					virtual1 <= next_virtual1;
					in_proc <= '1';
				end if;
			end if;
		end if;
	end process;
end struct;
