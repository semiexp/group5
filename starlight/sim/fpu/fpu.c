#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#define  one (uint32_t)1

void myread(int fd, char buf[], int size)
{
  int len = size, rlen;
  while (len > 0) {
    if ((rlen = read(fd, buf+size-len, len)) < 0) {
      perror("read");
      exit(1);
    }
    len -= rlen;
  }
}

//第bit番目のビットチェック(bit=1,2,...)(little endian)
int bit_check(uint32_t u, int b)
{
  u %= (one<<b);
  return (u >= (one<<(b-1))) ? 1 : 0;
}

uint64_t myround(uint64_t u, int b)
{
  if (bit_check(u, b) == 0)
    return u>>b;
  else {
    if (bit_check(u, b+1) == 1)
      return (u>>b) + 1;
    else {
      uint64_t d = u%(1<<(b-1));
      if (d == 0)
				return u>>b;
      else
				return (u>>b) + 1;
    }
  }
}

