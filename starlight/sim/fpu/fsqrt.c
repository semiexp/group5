#include <stdio.h>
#include <math.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "fpu.h"
#define  one (uint32_t)1

static uint32_t c[1024] = {}, g[1024] = {};

uint32_t fsqrt(uint32_t input)
{
	if (g[0] == 0) {
		char buf1[24], buf2[14], bufn[1];
		int fd;
		if ((fd = open("table/fsqrt.dat", O_RDONLY)) < 0) {
			perror("open");
			exit(1);
		}

		int i;
		for (i = 0; i < 1024; i++) {
			myread(fd, buf1, 23);
			myread(fd, buf2, 13);
			myread(fd, bufn, 1);

			c[i] = (uint32_t)strtol(buf1, NULL, 2);
			g[i] = (uint32_t)strtol(buf2, NULL, 2);
		}
		close(fd);
	}

  uint32_t a0, a1;
  uint64_t frac;
  uint32_t sign, exp;

  a1 = input % (1<<14);
  a0 = (input % (1<<24))>>14;

  exp = (input<<1)>>24;
  sign = input>>31;

  if (exp  == 0)   return sign<<31;
	if (sign == 1)   return 1023<<22;
  if (exp  == 255) return 255<<23;


	if (exp%2 == 1) {
		exp = (exp+127)/2;
		frac = ((uint64_t)c[a0]<<15) + a1*(g[a0]+(1<<13));
	} else {
		exp = (exp+126)/2;
		frac = ((uint64_t)c[a0]<<15) + a1*(g[a0]+(1<<14));
	}

	exp = exp<<23;

  frac = frac>>15;

  return sign + exp + frac;
}
