#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "fpu.h"
#define  one (uint32_t)1

static uint32_t c[1024] = {}, g[1024] = {};

uint32_t finv(uint32_t input)
{
	if (c[0] == 0) {
		char buf1[24], buf2[14], bufn[1];
		int fd;

		if ((fd = open("table/finv.dat", O_RDONLY)) < 0) {
			perror("open");
			exit(1);
		}

		int i;
		for (i = 0; i < 1024; i++) {
			myread(fd, buf1, 23);
			myread(fd, buf2, 13);
			myread(fd, bufn, 1);

			c[i] = (uint32_t)strtol(buf1, NULL, 2);
			g[i] = (uint32_t)strtol(buf2, NULL, 2);
		}
		close(fd);
	}
  uint32_t a0, a1;
  uint64_t frac;
  uint32_t sign, exp;
  a1 = input % (1<<13);
  a0 = (input % (1<<23))>>13;
  frac = ((uint64_t)c[a0]<<12) - a1*g[a0];
  exp = 254 - ((input<<1)>>24);
  sign = (input>>31)<<31;

  if (((input<<1)>>24) == 255)
    return sign;

  if (((input<<1)>>24) == 0)
    return sign + (255<<23);

  exp--;
  exp = exp<<23;
  frac = frac>>12;

  return sign + exp + frac;
}
