#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

extern uint32_t finv(uint32_t);

/*
void myread(int, char[], int);
int bit_check(uint32_t, int);
uint64_t myround(uint32_t, int);
uint32_t finv(uint32_t);
*/

char opt;
unsigned long long add = 0;
unsigned long long addi = 0;
unsigned long long sub = 0;
unsigned long long lshift = 0;
unsigned long long lshifti = 0;
unsigned long long rshift = 0;
unsigned long long rshifti = 0;
unsigned long long movi = 0;
unsigned long long movhi = 0;
unsigned long long movhiz = 0;
unsigned long long cmp = 0;
unsigned long long cmpi = 0;
unsigned long long fcmp = 0;
unsigned long long fadd = 0;
unsigned long long fsub = 0;
unsigned long long fmul = 0;
unsigned long long finv_ = 0;
unsigned long long fabs_ = 0;
unsigned long long ld = 0;
unsigned long long sto = 0;
unsigned long long jmp = 0;
unsigned long long jmpi = 0;
unsigned long long call = 0;
unsigned long long out = 0;
unsigned long long in = 0;
unsigned long long total;

//2の20乗個のメモリ
unsigned int memory[1048576];
unsigned int rst[64];     //レジスタ               
int zero = 0;
int pos = 0;
int neg = 0;    //condition register

unsigned int pc = 0; //program counter

union mydata{
  unsigned int i;
  float f;
};

union mydata data1;
union mydata data2;

int alu(unsigned int op){
  unsigned int opcode;
  unsigned int p,q,r,i,sign;

  opcode = op >> 26;
  p = ((op & 0x03f00000) >> 20);
  q = ((op & 0x000fc000) >> 14);

  if (opcode == 0){//add
    r = ((op &0x00003f00) >> 8);
    rst[p] = rst[q] + rst[r];
    add++;
  }

  if (opcode == 1){//addi
    i = (op & 0x00003fff);
    if ((i >> 13) == 1){
      i = i | 0xffffc000;
    }
    rst[p] = rst[q] + i;
    addi++;
  }

  if (opcode == 2){//sub
    r = ((op &0x00003f00) >> 8);
    rst[p] = rst[q] - rst[r];
    sub++;
  }

  if (opcode == 4){//lshift1
    r = ((op &0x00003f00) >> 8);
    sign = rst[r] >> 31;
    if (sign == 0 && rst[r] < 32){
      rst[p] = rst[q] << rst[r];
    }else if(sign == 1 && (rst[r] * -1) < 32){
      rst[p] = rst[q] >> (rst[r] * -1);
    }else{
      rst[p] = 0;
    }
    lshift++;
  }

  if (opcode == 5){//lshift2
    i = ((op >> 8) & 0x0000003f);
    sign = i >> 5;
    if (sign == 1){
      i = i | 0xffffffc0;
    }
    if (sign == 0 && i < 32){
      rst[p] = rst[q] << i;
    }else if (sign == 1 && (i * -1) < 32){
      rst[p] = rst[q] >> (i * -1);
    }else{
      rst[p] = 0;
    }
    lshifti++;
  }
  
  if (opcode == 6){//rshift1
    r = ((op &0x00003f00) >> 8);
    sign = rst[r] >> 31;
    if (sign == 0 && rst[r] < 32){
      rst[p] = rst[q] >> rst[r];
    }else if(sign == 1 && (rst[r] * -1) < 32){
      rst[p] = rst[q] << (rst[r] * -1);
    }else{
      rst[p] = 0;
    }
    rshift++;
  }
  
  if (opcode == 7){//rshift2
    i = ((op >> 8) & 0x0000003f);
    sign = i >> 5;
    if (sign == 1){
      i = i | 0xffffffc0;
    }
    if (sign == 0 && i < 32){
      rst[p] = rst[q] >> i;
    }else if (sign == 1 && (i * -1) < 32){
      rst[p] = rst[q] << (i * -1);
    }else{
      rst[p] = 0;
    }
    rshifti++;
  }

  if (opcode == 8){//movi
    i = op & 0x000fffff;
    if ((i >> 19) == 1){
      i = i | 0xfff00000;
    }
    rst[p] = i;
    movi++;
  }

  if (opcode == 10){//movhi
    i = (op >> 4) & 0x0000ffff;
    rst[p] = ((rst[p] & 0x0000ffff) | (i << 16));
    movhi++;
  }

  if (opcode == 11){//movhiz
    i = (op >> 4) & 0x0000ffff;
    rst[p] = (i << 16);
    movhiz++;
  }

  if (opcode == 12){//cmp
    cmp++;
    if ((int)rst[p] < (int)rst[q]){
      pos = 0;
      zero = 0;
      neg = 1;
    }else if(rst[p] == rst[q]){
      pos = 0;
      zero = 1;
      neg = 0;
    }else{
      pos = 1;
      zero = 0;
      neg = 0;
    }
  }

  if (opcode == 13){//cmpi
    i = op & 0x000fffff;
    sign = i >> 19;
    cmpi++; 
    if (sign == 1){
      i = i | 0xfff00000;
    }
    if ((int)rst[p] < (int)i){
      pos = 0;
      zero = 0;
      neg = 1;
    }else if(rst[p] == i){
      pos = 0;
      zero = 1;
      neg = 0;
    }else{
      pos = 1;
      zero = 0;
      neg = 0;
    }
  }
   
  rst[0] = 0;
  return 0;
}

int fpu (unsigned int op){
  unsigned int opcode;
  unsigned int p,q,r;
  opcode = op >> 26;
  p = ((op & 0x03f00000) >> 20);
  q = ((op & 0x000fc000) >> 14);
  r = ((op & 0x3f00) >> 8);

  if (opcode == 30){//fcmp
    fcmp++;
      if (*(float*)&rst[p] < *(float*)&rst[q]){
      pos = 0;
      zero = 0;
      neg = 1;
    }else if(rst[p] == rst[q]){
      pos = 0;
      zero = 1;
      neg = 0;
    }else{
      pos = 1;
      zero = 0;
      neg = 0;
    }
    if (rst[p] == 0x80000000 && rst[q] == 0x00000000){
      pos = 0;
      zero = 1;
      neg = 0;
    }
    if (rst[p] == 0x00000000 && rst[q] == 0x80000000){
      pos = 0;
      zero = 1;
      neg = 0;
    }
  }
  if (opcode == 16){//fadd
    data1.i = rst[q];
    data2.i = rst[r];
    data1.f = data1.f + data2.f;
    rst[p] = data1.i;   
    fadd++;
  }
  if (opcode == 17){//fsub
    data1.i = rst[q];
    data2.i = rst[r];
    data1.f = data1.f - data2.f;
    rst[p] = data1.i;
    fsub++; 
  }
  if (opcode == 18){//fmul
    data1.i = rst[q];
    data2.i = rst[r];
    data1.f = data1.f * data2.f;
    rst[p] = data1.i;
    fmul++;  
  }
  if (opcode == 19){//finv
    rst[p] = finv(rst[q]);
    finv_++;
  }
  
  if (opcode == 31){//fabs
    rst[p] = ((rst[q] << 1) >> 1);
    fabs_++;
  }
  rst[0] = 0;
  return 0;
}

int mop(unsigned int op){
  unsigned int opcode;
  int p,q,r,i,addr;
  opcode = op >> 26;
  p = ((op & 0x03f00000) >> 20);
  q = ((op & 0x000fc000) >> 14);  
  r = ((op & 0x3f00) >> 8);
  if (opcode == 32){//ld1
    ld++;
    i = op & 0x00003fff;
    if ((i >> 13) == 1){
      i = i | 0xffffc000;
    }
    addr = (rst[q] + i) & 0x000fffff;
    rst[p] = memory[addr];
  }
 
  if (opcode == 33){//ld2
    ld++;
    i = op & 0x000000ff;
    if ((i >> 7) == 1){
      i = i | 0xffffff00;
    }
    addr = (rst[q] + rst[r] + i) & 0X000fffff;
    rst[p] = memory[addr];
  }

  if (opcode == 36){//sto1
    sto++;
    i = op & 0x00003fff;
    if ((i >> 13) == 1){
      i = i | 0xffffc000;
    }
    addr = (rst[q] + i) & 0x000fffff;
    memory[addr] = rst[p];
  }
  
  if (opcode ==37){//sto2
    sto++;
    i = op & 0x000000ff;
    if ((i >> 7) == 1){
      i = i | 0xffffff00;
    }
    addr = (rst[q] + rst[r] + i) & 0X000fffff;
    memory[addr] = rst[p];
  }

  rst[0] = 0;
  return 0;
}

int other(unsigned int op, unsigned int *pc){
  unsigned int opcode;
  unsigned int p,q;
  int i;
  opcode = op >> 26;
  p = ((op & 0x03f00000) >> 20);
  q = ((op & 0x000fc000) >> 14);
  i = op & 0x000fffff;

  if (opcode == 48){//jmp1
    jmp++;
    int to;
    to = rst[q] & 0x000fffff;
    if (p == 0){//無条件分岐
      *pc = to;
      rst[0] = 0;
      return 0;
    }
    if (p == 2 && zero == 0){
      *pc = to;
      rst[0] = 0;
      return 0;
    }
    if (p == 3 && zero == 1){
      *pc = to;
      rst[0] = 0;
      return 0;
    }
    if (p == 4 && pos == 0){
      *pc = to;
      rst[0] = 0;
      return 0;
    }
    if (p == 5 && pos == 1){
      *pc = to;
      rst[0] = 0;
      return 0;
    }
    if (p == 6 && neg == 0){
      *pc = to;
      rst[0] = 0;
      return 0;
    }
    if (p == 7 && neg == 1){
      *pc = to;
      rst[0] = 0;
      return 0;
    }else{
      *pc = *pc + 1;
    }
  }
  
  if (opcode == 49){//jmp2
    jmpi++;
    if (p == 0){//無条件分岐
      *pc = i;
      rst[0] = 0;
      return 0;
    }
    if (p == 2 && zero == 0){
      *pc = i;
      rst[0] = 0;
      return 0;
    }
    if (p == 3 && zero == 1){
      *pc = i;
      rst[0] = 0;
      return 0;
    }
    if (p == 4 && pos == 0){
      *pc = i;
      rst[0] = 0;
      return 0;
    }
    if (p == 5 && pos == 1){
      *pc = i;
      rst[0] = 0;
      return 0;
    }
    if (p == 6 && neg == 0){
      *pc = i;
      rst[0] = 0;
      return 0;
    }
    if (p == 7 && neg == 1){
      *pc = i;
      rst[0] = 0;
      return 0;
    }else{
      *pc = *pc + 1;
    }
  }

  if (opcode == 52){//call
    call++;
    rst[p] = *pc + 1;
    *pc = i;
  }
  
  if (opcode == 60){//out
    out++;
    unsigned char output;
    output = rst[p] & 0xff;
    if (opt =='x'){
      fprintf(stdout,"%02x\n", (unsigned int)output);
    }else if(opt == 'b'){
      fprintf(stdout,"%c", (unsigned int)output);
    }else{
      fprintf(stdout,"%02x\n", (unsigned int)output);
    }
    *pc = *pc + 1;
  }

  if (opcode == 61){//in
    in++;
    unsigned char input;
    fscanf(stdin, "%c", &input);
    rst[p] = input & 0xff;
    *pc = *pc + 1;
  }
  if (opcode == 63){//halt
    return 1;
  }
  rst[0] = 0;
  return 0;
}

  int instruction(unsigned int op, unsigned int *pc){
  unsigned int typeop;
  int v = 0;
  typeop = op >> 30;
  if (typeop == 0){//ALU系の命令を実行
    v = alu(op);
    *pc = *pc + 1;
  }
  if (typeop == 1){//FPU系の命令を実行
    v = fpu(op);
    *pc = *pc + 1;
  }
  if (typeop == 2){//メモリ系の命令を実行
    v = mop(op);
    *pc = *pc + 1;
  }
  if (typeop == 3){//その他の命令を実行
    v = other(op, pc);
  }
  return v;
}

int main(int argc, char* argv[]){

  unsigned int filesize;
  unsigned int pc = 0; //program counter
  FILE *fp;int i,readsize = 0,k=0,v;
  rst[0] = 0;
  int m,n=1;
  
  for(m=1;m < argc;m++){
    if(*argv[m] == '-'){
      opt = *(argv[m]+1);
      n++;
    }else{
      break;
    }
  }
    //引数で与えられたファイルを開く
  if ((fp = fopen(argv[n], "rb")) == NULL){
    fprintf(stderr,"failed to open file.\n");
    exit(1);
  }

  //最初の４バイトを読む。そこに命令の個数が書いてある
  if (fread(&filesize, sizeof(int), 1, fp) != 1){
    fprintf(stderr,"failed to read the first byte.\n");
    exit(1);
  }
  fprintf(stderr,"filesize : %d\n", filesize);

  //memoryの０番地からファイルの内容を４バイトずつ書き込んでいく
  while(1){
    i = fread(&memory[k], sizeof(int), 1, fp);
    if (i != 1){//freadのエラー処理
      fprintf(stderr,"failed to read,\n");
      break;
    }
    k++;
    readsize++;
    if (readsize == filesize){
      break;
    }
  }
  fprintf(stderr, "readsize : %d\n", readsize);
 
  while(1){
    v = instruction(memory[pc], &pc);
    if (v == 1){
      fprintf(stderr,"halt.\n");
      break;
    }
  }

  total = add + addi + sub + lshift + lshifti + rshift + rshifti +movi + movhi
    + movhiz + cmp + cmpi + fcmp + fadd + fsub + fmul + finv_ + fabs_
    + ld + sto + jmp + jmpi + call + out + in;

  fprintf(stderr,"add     : %llu\n",add);
  fprintf(stderr,"addi    : %llu\n",addi);
  fprintf(stderr,"sub     : %llu\n",sub);
  fprintf(stderr,"lshift  : %llu\n",lshift);
  fprintf(stderr,"lshifti : %llu\n",lshifti);
  fprintf(stderr,"rshift  : %llu\n",rshift);
  fprintf(stderr,"rshifti : %llu\n",rshifti);
  fprintf(stderr,"movi    : %llu\n",movi);
  fprintf(stderr,"movhi   : %llu\n",movhi);
  fprintf(stderr,"movhiz  : %llu\n",movhiz);
  fprintf(stderr,"cmp     : %llu\n",cmp);
  fprintf(stderr,"cmpi    : %llu\n",cmpi);
  fprintf(stderr,"fcmp    : %llu\n",fcmp);
  fprintf(stderr,"fadd    : %llu\n",fadd);
  fprintf(stderr,"fsub    : %llu\n",fsub);
  fprintf(stderr,"fmul    : %llu\n",fmul);
  fprintf(stderr,"finv    : %llu\n",finv_);
  fprintf(stderr,"fabs    : %llu\n",fabs_);
  fprintf(stderr,"ld      : %llu\n",ld);
  fprintf(stderr,"sto     : %llu\n",sto);
  fprintf(stderr,"jmp     : %llu\n",jmp);
  fprintf(stderr,"jmpi    : %llu\n",jmpi);
  fprintf(stderr,"call    : %llu\n",call);
  fprintf(stderr,"out     : %llu\n",out);
  fprintf(stderr,"in      : %llu\n\n",in);

  fprintf(stderr, "total : %llu\n", total);
  if (opt == 'i'){
    char s[10];
    while(1){
      scanf("%s", s);
      if (strcmp(s,"add") == 0){
	fprintf(stderr, "%s : %llu\n", s, add);
      }else if(strcmp(s, "addi") == 0){
	fprintf(stderr, "%s : %llu\n", s, addi);
      }else{
	break;
      }
    }
  }
   
  //fprintf(stderr, "add:%llu\n",add);
 
  fclose(fp);
  //最初の４バイトをbytesizeに代入
  // fopen,fread,fwriteを使用
  //memoryの０番地から順に、４バイトずつ読んで書き込みを繰り返す。
  //filesize分だけ書き込んだらファイル読み込みは終了
  //次はmemoryのをプログラムカウンタの番号から実行していく
  

  return 0;
}
