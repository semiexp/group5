--1-stage FSQRT
--10.5ns
--~66MHz

library IEEE, STD;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use STD.textio.all;

entity FSQRT is
  Port(
    input  :  in STD_LOGIC_VECTOR (31 downto 0);
    output : out STD_LOGIC_VECTOR (31 downto 0);
    clk    :  in STD_LOGIC);
end FSQRT;

architecture structure of FSQRT is
  type ans_type is (NORM, ZERO, INF, IMG);

  signal man_data : STD_LOGIC_VECTOR (35 downto 0) := (others => '0');
  signal man1     : STD_LOGIC_VECTOR (21 downto 0) := (others => '0');
  signal man2     : STD_LOGIC_VECTOR (28 downto 0) := (others => '0');
  signal man      : STD_LOGIC_VECTOR (37 downto 0) := (others => '0');
  signal exp      : STD_LOGIC_VECTOR (7 downto 0)  := (others => '0');
  signal sign     : STD_LOGIC := '0';
  signal anstype  : ans_type  := NORM;
  
  type RomType is array(0 to 1023) of bit_vector(35 downto 0);
  impure function InitRomFromFile (RomFileName : in string) return RomType is
    FILE RomFile : text open read_mode is RomFileName;
    variable RomFileLine : line;
    variable ROM : RomType;
  begin
    for I in RomType'range loop
      readline (RomFile, RomFileLine);
      read (RomFileLine, ROM(I));
    end loop;
    return ROM;
  end function;

  signal ROM : RomType := InitRomFromFile("fsqrt.dat");
  
begin
  --stage1----------------------------------------------
  man_data <= to_stdLogicVector(ROM(conv_integer(input(23 downto 14))));

  sign <= input(31);
  exp  <= ("0" & input(30 downto 24))
          + "00111111" + input(23);

  process(input, man_data)
  begin
    if (input(23) = '1') then
      man <= (man_data(35 downto 13) &"000"&x"000")
           + (("1" & man_data(12 downto 0)) * input(13 downto 0));
    else
      man <= (man_data(35 downto 13) &"000"&x"000")
           + (("10" & man_data(12 downto 0)) * input(13 downto 0));
    end if;

    if (input(30 downto 23) = x"00") then
      anstype <= ZERO;
    elsif (input(31) = '1') then
      anstype <= IMG;
    elsif (input(30 downto 23) = x"ff") then
      anstype <= INF;
    else
      anstype <= NORM;
    end if;
  end process;

  
  process(sign, exp, man)
  begin
    case anstype is
      when ZERO => output(30 downto 0) <= (others => '0');
      when IMG  => output(30 downto 0) <= "111"&x"fc00000";
      when INF  => output(30 downto 0) <= "111"&x"f800000";
      when others =>
        output(30 downto 23) <= exp;
        output(22 downto 0)  <= man(37 downto 15);
    end case;
    output(31) <= sign;
  end process;
end structure;

