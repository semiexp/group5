--2-stage pipeline
--66MHz

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity FADD is
  Port(
    input1 :  in STD_LOGIC_VECTOR (31 downto 0);
    input2 :  in STD_LOGIC_VECTOR (31 downto 0);
    output : out STD_LOGIC_VECTOR (31 downto 0);
    clk    :  in STD_LOGIC);
end FADD;

architecture structure of FADD is
  type ans_type is (NORM, ZERO, ZERO_NEG, INF, INF_NEG);
  
  signal swapped_in1, swapped_in2
    : STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
  signal man2, man
    : STD_LOGIC_VECTOR (49 downto 0) := (others => '0');
  signal u_man, u_man_reg1, s_man_p2
    : STD_LOGIC_VECTOR (48 downto 0) := (others => '0');
  signal exp_reg1, shift
    : STD_LOGIC_VECTOR (7 downto 0) := (others => '0');
  signal exp_p1, exp_p2
    : STD_LOGIC_VECTOR (8 downto 0) := (others => '0');
  signal man1, s_man_p1
    : STD_LOGIC_VECTOR (25 downto 0) := (others => '0');
  signal r_man_p2
    : STD_LOGIC_VECTOR (24 downto 0) := (others => '0');
  signal r_man_p1
    : STD_LOGIC_VECTOR (23 downto 0) := (others => '0');
  signal sign_reg1, switch_reg1
    : STD_LOGIC := '0';
  signal anstype, anstype_reg1, anstype_p1, anstype_p2
    : ans_type := NORM;
  
begin
  --pipeline registers------------------------------------
  process(clk)
  begin
    if rising_edge(clk) then
      exp_reg1     <= swapped_in1(30 downto 23);
      u_man_reg1   <= u_man(48 downto 0);
      sign_reg1    <= man(49);
      anstype_reg1 <= anstype;
      if (shift = "00000000" or shift = "00000001") then
        switch_reg1 <= '0';
      else
        switch_reg1 <= '1';
      end if;
    end if;
  end process;


  --stage1--------------------------------------------------
  process(input1, input2)
  begin
    --指数部が大きい方を１、小さい方を２の配線につなぐ
    if (input1(30 downto 23) < input2(30 downto 23)) then
      swapped_in1 <= input2;
      swapped_in2 <= input1;
    else
      swapped_in1 <= input1;
      swapped_in2 <= input2;
    end if;
	
	if (input1(30 downto 23) = x"ff") then
	  if (input1(31) = '1') then
        anstype <= INF_NEG;
      else
        anstype <= INF;
      end if;
	elsif (input2(30 downto 23) = x"ff") then
	  if (input2(31) = '1') then
        anstype <= INF_NEG;
      else
        anstype <= INF;
      end if;
	elsif (input1(30 downto 23) = x"00" and input2(30 downto 23) = x"00") then
	  if (input1(31) = '1' and input2(31) = '1') then
	    anstype <= ZERO_NEG;
	  else
	    anstype <= ZERO;
	  end if;
	else
	  anstype <= NORM;
	end if;
  end process;
  
  shift <= swapped_in1(30 downto 23)
         + ((not swapped_in2(30 downto 23)) + "00000001");

  process(swapped_in1, swapped_in2, shift)
  variable man2_1 : STD_LOGIC_VECTOR (49 downto 0) := (others => '0');
  
  begin
    --仮数を生成
    if (swapped_in1(30 downto 23) = x"00") then
      man1 <= (others => '0');
    else
      if (swapped_in1(31) = '0') then
        man1 <= "001" & swapped_in1(22 downto 0);
      else
        man1 <= (not ("001" & swapped_in1(22 downto 0)))
              + ("00" & x"000001");
      end if;
    end if;

    if (swapped_in2(30 downto 23) = x"00") then
      man2_1 := (others => '0');
      man2 <= man2_1;
    else
      man2_1 := to_stdlogicvector
                (to_bitvector("001" & swapped_in2(22 downto 0) & x"000000")
                 srl to_integer(unsigned(shift)));
      if (swapped_in2(31) = '0') then
        man2 <= man2_1;
      else
        man2 <= (not man2_1) + ("00" & x"000001");
      end if;
    end if;
  end process;

  man <= (man1 + man2(49 downto 24)) & man2(23 downto 0);

  u_man <= man(48 downto 0) when (man(49) = '0') else
           not (man(48 downto 0) - ("0"&x"000000000001"));

  
  --stage2--------------------------------------------------------
  --path1---------------------------------------------------------
  process(exp_reg1, u_man_reg1, sign_reg1)
  variable var_shift : integer := 0;

  --use u_man(48 downto 23)
  begin
    if    (u_man_reg1(48) = '1') then var_shift := 26;--(-1)
    elsif (u_man_reg1(47) = '1') then var_shift := 0;
    elsif (u_man_reg1(46) = '1') then var_shift := 1;
    elsif (u_man_reg1(45) = '1') then var_shift := 2;
    elsif (u_man_reg1(44) = '1') then var_shift := 3;
    elsif (u_man_reg1(43) = '1') then var_shift := 4;
    elsif (u_man_reg1(42) = '1') then var_shift := 5;
    elsif (u_man_reg1(41) = '1') then var_shift := 6;
    elsif (u_man_reg1(40) = '1') then var_shift := 7;
    elsif (u_man_reg1(39) = '1') then var_shift := 8;
    elsif (u_man_reg1(38) = '1') then var_shift := 9;
    elsif (u_man_reg1(37) = '1') then var_shift := 10;
    elsif (u_man_reg1(36) = '1') then var_shift := 11;
    elsif (u_man_reg1(35) = '1') then var_shift := 12;
    elsif (u_man_reg1(34) = '1') then var_shift := 13;
    elsif (u_man_reg1(33) = '1') then var_shift := 14;
    elsif (u_man_reg1(32) = '1') then var_shift := 15;
    elsif (u_man_reg1(31) = '1') then var_shift := 16;
    elsif (u_man_reg1(30) = '1') then var_shift := 17;
    elsif (u_man_reg1(29) = '1') then var_shift := 18;
    elsif (u_man_reg1(28) = '1') then var_shift := 19;
    elsif (u_man_reg1(27) = '1') then var_shift := 20;
    elsif (u_man_reg1(26) = '1') then var_shift := 21;
    elsif (u_man_reg1(25) = '1') then var_shift := 22;
    elsif (u_man_reg1(24) = '1') then var_shift := 23;
    elsif (u_man_reg1(23) = '1') then var_shift := 24;
    else                                 var_shift := 25;--man=0
    end if;

    case var_shift is
      --u_man = 1x.xxxx
      when 26 =>
        s_man_p1 <= u_man_reg1(48 downto 23);
        exp_p1 <= ("0" & exp_reg1) + "000000001";

      --u_man = 00.0000
      when 25 =>
        exp_p1 <= (others => '0');
        s_man_p1 <= (others => '0');

      --u_man = 0x.xxxx
      when others =>
        s_man_p1 <=
          to_stdlogicvector(to_bitvector(u_man_reg1(47 downto 23))
                            sll (var_shift)) & "0";
        if (exp_reg1 <= var_shift) then
          exp_p1 <= (others => '0');
        else
          exp_p1 <= "0" & (exp_reg1
                         - std_logic_vector(to_unsigned(var_shift, 8)));
        end if;
    end case;
  end process;

  process(exp_p1, sign_reg1, anstype_reg1)
  begin
    --±0,±∞の検査
    case anstype_reg1 is
      when NORM =>
        if (exp_p1(7 downto 0) = x"00") then
          if (sign_reg1 = '1') then
            anstype_p1 <= ZERO_NEG;
          else
            anstype_p1 <= ZERO;
          end if;
        elsif (exp_p1(8) = '1' or exp_p1 = x"ff") then
          if (sign_reg1 = '1') then
            anstype_p1 <= INF_NEG;
          else
            anstype_p1 <= INF;
          end if;
        else
          anstype_p1 <= NORM;
        end if;
        
      when others => anstype_p1 <= anstype_reg1;
                     
    end case;
  end process;

  r_man_p1 <= "0" & s_man_p1(24 downto 2) when (s_man_p1(1) = '0'
                                            or (s_man_p1(2) = '0'
                                            and s_man_p1(0) = '0')) else
              ("0" & s_man_p1(24 downto 2)) + x"000001";

  
  --path2---------------------------------------------------------------
  process(exp_reg1, u_man_reg1, sign_reg1)
  begin
    --u_man = 1x.xxxx
    if (u_man_reg1(48) = '1') then
      s_man_p2 <= "0" & u_man_reg1(48 downto 1); --u_man(0) = '0'
      exp_p2   <= ("0" & exp_reg1) + "000000001";
    --u_man = 01.xxxx
    elsif (u_man_reg1(47) = '1') then
      s_man_p2 <= u_man_reg1;
      exp_p2   <= "0" & exp_reg1;
    --u_man = 00.1xxx
    else
      s_man_p2 <= u_man_reg1(47 downto 0) & "0";
      exp_p2   <= ("0" & exp_reg1) - "000000001";
    end if;
  end process;

  process(exp_p2, sign_reg1, anstype_reg1)
  begin
    --±0,±∞の検査
	case anstype_reg1 is
	  when NORM =>
		if (exp_reg1 = x"00") then
		  if (sign_reg1 = '1') then
			anstype_p2 <= ZERO_NEG;
		  else
			anstype_p2 <= ZERO;
		  end if;
		elsif (exp_p2(8) = '1' or exp_p2(7 downto 0) = x"ff") then
		  if (sign_reg1 = '1') then
			anstype_p2 <= INF_NEG;
		  else
			anstype_p2 <= INF;
		  end if;
		else
		  anstype_p2 <= NORM;
		end if;
		
	  when others => anstype_p2 <= anstype_reg1;

    end case;
  end process;

  r_man_p2 <=
    s_man_p2(48 downto 24) when (s_man_p2(23) = '0'
                             or (s_man_p2(22 downto 0) = "000"&x"00000"
                             and s_man_p2(24) = '0')) else
    s_man_p2(48 downto 24) + x"000001";

  --merge path-----------------------------------------------
  process(switch_reg1, exp_p1, exp_p2, r_man_p1, r_man_p2, sign_reg1, anstype_p1, anstype_p2)
  begin
    --path1
    if (switch_reg1 = '0') then
      case anstype_p1 is
        when ZERO =>
          output <= x"00000000";

        when ZERO_NEG =>
          output <= x"80000000";

        when INF =>
          output <= x"7f800000";

        when INF_NEG =>
          output <= x"ff800000";

        when NORM =>
          --丸めによる桁のズレを補正
          if (r_man_p1(23) = '1') then
            if (exp_p1(7 downto 1) = "1111111") then
              output(30 downto 0) <= "111" & x"f800000";
            else
              output(22 downto 0) <= "0" & r_man_p1(22 downto 1);
              output(30 downto 23) <= (exp_p1(7 downto 0) + "00000001");
            end if;
          else
            output(22 downto 0) <= r_man_p1(22 downto 0);
            output(30 downto 23) <= exp_p1(7 downto 0);
          end if;
          output(31) <= sign_reg1;

      end case;

    --path2
    else
      case anstype_p2 is
        when ZERO =>
          output <= x"00000000";

        when ZERO_NEG =>
          output <= x"80000000";

        when INF =>
          output <= x"7f800000";

        when INF_NEG =>
          output <= x"ff800000";

        when NORM =>
          --丸めによる桁のズレを補正
          if (r_man_p2(24) = '1') then
            if (exp_p2(7 downto 1) = "1111111") then
              output(30 downto 0) <= "111" & x"f800000";
            else
              output(22 downto 0) <= r_man_p2(23 downto 1);
              output(30 downto 23) <= (exp_p2(7 downto 0) + "00000001");
            end if;
          else
            output(22 downto 0) <= r_man_p2(22 downto 0);
            output(30 downto 23) <= exp_p2(7 downto 0);
          end if;
          output(31) <= sign_reg1;
      end case;
    end if;
  end process;

end structure;
