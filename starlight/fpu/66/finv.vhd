--1-stage
--66MHz

library IEEE, STD;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use STD.textio.all;

entity FINV is
  Port(
    input  :  in STD_LOGIC_VECTOR (31 downto 0);
    output : out STD_LOGIC_VECTOR (31 downto 0);
    clk    :  in STD_LOGIC);
end FINV;

architecture structure of FINV is

  signal exp : STD_LOGIC_VECTOR (7  downto 0) := (others => '0');
  signal man1, man : STD_LOGIC_VECTOR (34 downto 0) := (others => '0');
  signal man2 : STD_LOGIC_VECTOR (25 downto 0) := (others => '0');
  signal man_data : STD_LOGIC_VECTOR (35 downto 0) := (others => '0');

  type RomType is array(0 to 1023) of bit_vector(35 downto 0);
  impure function InitRomFromFile (RomFileName : in string) return RomType is
    FILE RomFile : text is in RomFileName;
    variable RomFileLine : line;
    variable ROM : RomType;
  begin
    for I in RomType'range loop
      readline (RomFile, RomFileLine);
      read (RomFileLine, ROM(I));
    end loop;
    return ROM;
  end function;

  signal ROM : RomType := InitRomFromFile("finv.dat");

begin
  exp  <= "11111110" - input(30 downto 23);
  
  man_data <= to_stdLogicVector(ROM(conv_integer(input(22 downto 13))));

  man1 <= man_data(35 downto 13) & x"000";
  man2 <= man_data(12 downto 0) * input(12 downto 0);
  man  <= man1 - (x"00"&"0" & man2);

  process(exp, man)
  begin
    --input = inf
    if (exp = "11111111") then
      output(30 downto 0) <= (others => '0');
    --input = 0
    elsif (exp = "11111110") then
      output(30 downto 23) <= (others => '1');
      output(22 downto 0) <= (others => '0');
    else
      output(30 downto 23) <= exp - x"01";
      output(22 downto 0) <= man(34 downto 12);
    end if;
    output(31) <= input(31);
  end process;
end structure;
