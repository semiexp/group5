--1-stage
--9.333ns
--~66MHz

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity FMUL is
  Port(
    input1 :  in STD_LOGIC_VECTOR (31 downto 0);
    input2 :  in STD_LOGIC_VECTOR (31 downto 0);
    output : out STD_LOGIC_VECTOR (31 downto 0);
    clk    :  in STD_LOGIC);
end FMUL;

architecture structure of FMUL is
  type ans_type is (NORM, ZERO, INF);
  
  signal man              : STD_LOGIC_VECTOR (47 downto 0) := (others => '0');
  signal s_man            : STD_LOGIC_VECTOR (46 downto 0) := (others => '0');
  signal r_man            : STD_LOGIC_VECTOR (23 downto 0) := (others => '0');
  signal exp1, exp2, exp3 : STD_LOGIC_VECTOR (8 downto 0)  := (others => '0');
  signal sign             : STD_LOGIC := '0';
  signal anstype          : ans_type  := NORM;

begin
  sign <= input1(31) xor input2(31);

  man <= ("1" & input1(22 downto 0))
       * ("1" & input2(22 downto 0));

  exp1 <= ("0" & input1(30 downto 23))
        + ("0" & input2(30 downto 23))
        - ("0"&x"7f");

  exp2 <= ("0" & input1(30 downto 23))
        + ("0" & input2(30 downto 23))
        - ("0"&x"7e");

  exp3 <= ("0" & input1(30 downto 23))
        + ("0" & input2(30 downto 23));
  
  anstype <= ZERO when (input1(30 downto 23) = x"00"
                     or input2(30 downto 23) = x"00") else
             INF  when (input1(30 downto 23) = x"ff"
                     or input2(30 downto 23) = x"ff") else
             NORM;
  
  --shift (economized form)
  s_man <= man(46 downto 0) when (man(47) = '1') else
           man(45 downto 0) & "0";
  
  --round (man < fffffe000002)
  r_man <= "0" & s_man(46 downto 24) when (s_man(23) = '0'
                               or (s_man(22 downto 0) = "00"&x"0000"
                               and s_man(24) = '0')) else
           ("0" & s_man(46 downto 24)) + 1;

  process(exp1, exp2, exp3, r_man, man, sign, anstype)
  begin
    -- exp3 <= "001111110" -> zero
    if (anstype = ZERO or (exp3(8 downto 7) = "00" and exp3(6 downto 0) /= "1111111")) then
      output(30 downto 0) <= (others => '0');
    -- exp1 >= "011111111" -> inf
    elsif (anstype = INF or exp1(8) = '1' or exp1(7 downto 0) = x"ff") then
      output(30 downto 0) <= "111"&x"f800000";
    else
      if (r_man(23) = '1' or man(47) = '1') then
        if (exp2(7 downto 0) = x"ff") then
          output(30 downto 0) <= "111"&x"f800000";
        else
          output(30 downto 23) <= exp2(7 downto 0);
          if (r_man(23) = '1') then
            output(22 downto 0)  <= "0" & r_man(22 downto 1);
          else
            output(22 downto 0)  <= r_man(22 downto 0);
          end if;
        end if;
      else
        if (exp1(7 downto 0) = x"00") then
          output(30 downto 0) <= (others => '0');
        else
          output(30 downto 23) <= exp1(7 downto 0);
          output(22 downto 0)  <= r_man(22 downto 0);
        end if;
      end if;
    end if;
    output(31) <= sign;
  end process;
end structure;
