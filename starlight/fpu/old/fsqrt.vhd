library IEEE, STD;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use STD.textio.all;

entity FSQRT is
  Port(
    input  :  in STD_LOGIC_VECTOR (31 downto 0);
    output : out STD_LOGIC_VECTOR (31 downto 0);
    clk    :  in STD_LOGIC);
end FSQRT;

architecture structure of FSQRT is
  signal input_l  : STD_LOGIC_VECTOR (21 downto 0) := (others => '0');
  signal input_l2 : STD_LOGIC_VECTOR (8 downto 0) := (others => '0');
  signal exp : STD_LOGIC_VECTOR (7 downto 0) := (others => '0');
  signal frac_i1, frac, frac_l : STD_LOGIC_VECTOR (37 downto 0) := (others => '0');
  signal frac2 : STD_LOGIC_VECTOR (43 downto 0) := (others => '0');
  signal frac_i2 : STD_LOGIC_VECTOR (26 downto 0) := (others => '0');
  signal frac_data : STD_LOGIC_VECTOR (35 downto 0) := (others => '0');

  type RamType is array(0 to 1023) of bit_vector(35 downto 0);
  impure function InitRamFromFile (RamFileName : in string) return RamType is
    FILE RamFile : text is in RamFileName;
    variable RamFileLine : line;
    variable RAM : RamType;
  begin
    for I in RamType'range loop
      readline (RamFile, RamFileLine);
      read (RamFileLine, RAM(I));
    end loop;
    return RAM;
  end function;
  signal RAM : RamType := InitRamFromFile("fsqrt.dat");
  signal addr : std_logic_vector(9 downto 0);

begin
  process(clk)
  begin
    if rising_edge(clk) then
      addr <= input(22 downto 13);

      --latchs for pipeline
      input_l(21 downto 13)  <= input(31 downto 23);
      input_l(12 downto 0)  <= input(12 downto 0);

      input_l2 <= input_l(21 downto 13);
      frac_l   <= frac;
    end if;
  end process;

  --clock1
  --wait for BlockRAM

  --clock2
  frac_data <= to_stdLogicVector(RAM(conv_integer(addr)));
  frac_i1   <= frac_data(35 downto 13) & "000"&x"000";
  frac_i2   <= ("1" & frac_data(12 downto 0)) * input_l(12 downto 0);
  frac      <= frac_i1 + (x"00"&"000" & frac_i2);
  
  --clock3
  process(frac_l, input_l2)
  begin
    case (input_l2(0)) is
      when '0'    => frac2 <= (x"6a09e667f00") + x"b504f"*frac_l(36 downto 13);
      when others => frac2 <= frac_l & "000000";
    end case;
  end process;

  exp <= ("0" & input_l2(7 downto 1))
       + "00111111" + ("0000000" & input_l2(0));
  
  process(input_l2, exp, frac2)
  begin
    --input = 0
    if (input_l2(7 downto 0) = "00000000") then
      output(30 downto 0) <= (others => '0');
    --input = neg
    elsif (input_l2(8) = '1') then
      output(30 downto 22) <= (others => '1');
      output(21 downto 0)  <= (others => '0');
    --input = inf
    elsif (input_l2(7 downto 0) = "11111111") then
      output(30 downto 23) <= (others => '1');
      output(22 downto 0)  <= (others => '0');
    else
      output(30 downto 23) <= exp;
      if (frac2(20) = '0' or (frac2(21) = '0' and frac2(19) = '0')) then
        output(22 downto 0) <= frac2(43 downto 21);
      else
        output(22 downto 0) <= frac2(43 downto 21) + ("000" & x"00001");
      end if;
    end if;
    output(31) <= input_l2(8);
  end process;
end structure;

