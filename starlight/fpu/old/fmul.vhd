library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity FMUL is
  Port(
    input1 :  in STD_LOGIC_VECTOR (31 downto 0);
    input2 :  in STD_LOGIC_VECTOR (31 downto 0);
    output : out STD_LOGIC_VECTOR (31 downto 0);
    clk    :  in STD_LOGIC);
end FMUL;

architecture structure of FMUL is  
  signal man1, man2 : STD_LOGIC_VECTOR (23 downto 0) := (others => '0');
  signal mul, mul_l, shifted_mul : STD_LOGIC_VECTOR (47 downto 0) := (others => '0');
  signal exp, exp_l, exp_l2, exp_l_l : STD_LOGIC_VECTOR (8 downto 0);
  signal rounded_mul, rounded_mul_l : STD_LOGIC_VECTOR (24 downto 0);
  signal sign, sign_l, sign_l2, sign_l_l : STD_LOGIC;

begin
  process(input1, input2)
  begin
    sign <= input1(31) xor input2(31);
    if (input1(30 downto 23) = x"00") or (input2(30 downto 23) = x"00") then
      exp <= (others => '0');
    else
      exp <= ("0" & input1(30 downto 23)) + ("0" & input2(30 downto 23))
             - "001111111";
    end if;
    man1 <= x"800000" + ("0" & input1(22 downto 0));
    man2 <= x"800000" + ("0" & input2(22 downto 0));
  end process;

  process(man1, man2)
  begin
    mul  <= man1 * man2;
  end process;
  
  process(clk)
  begin
    --ラッチ１
    if rising_edge(clk) then
      mul_l <= mul;
      exp_l <= exp;
      sign_l <= sign;
    end if;
  end process;

  process(mul_l, exp_l, sign_l)
  begin
    --シフト(1*.~ -> 01.~)
    if mul_l(47) = '1' then
      if (exp_l /= "000000000") then
        exp_l2 <= exp_l + ("0" & x"01");
      else
        exp_l2 <= (others => '0');
      end if;
      shifted_mul(46 downto 0) <= mul_l(47 downto 1);
      shifted_mul(47) <= '0';
    else
      shifted_mul <= mul_l;
      exp_l2 <= exp_l;
    end if;
    sign_l2 <= sign_l;
  end process;

  process(shifted_mul)
  begin
    --丸め
    if shifted_mul(22) = '0' then
      rounded_mul <= shifted_mul(47 downto 23);
    elsif shifted_mul(23 downto 22) = "11" then
      rounded_mul <= shifted_mul(47 downto 23) + ("0" & x"000001");
    else
      for i in 0 to 21 loop
        if shifted_mul(21 - i) = '1' then
          rounded_mul <= shifted_mul(47 downto 23) + ("0" & x"000001");
          exit;
        end if;
        if i = 21 then
          rounded_mul <= shifted_mul(47 downto 23);
        end if;
      end loop;
    end if;
  end process;

  process(clk)
  begin
    --ラッチ２
    if rising_edge(clk) then
      exp_l_l <= exp_l2;
      rounded_mul_l <= rounded_mul;
      sign_l_l <= sign_l2;
    end if;
  end process;
  
  process(exp_l_l, rounded_mul_l, sign_l_l)
  begin
    if exp_l_l = x"00" then
      output(30 downto 0) <= (others => '0');
    elsif (rounded_mul_l(24) = '1') then
      if ((exp_l_l + "000000001") > "011111111") then
        output(30 downto 23) <= (others => '1');
        output(22 downto 0) <= (others => '0');
      else
        output(30 downto 23) <= (exp_l_l(7 downto 0) + x"01");
        output(22 downto 0) <= rounded_mul_l(23 downto 1);
      end if;
    else
      if (exp_l_l > "011111111") then
        output(30 downto 23) <= (others => '1');
        output(22 downto 0) <= (others => '0');
      else
        output(22 downto 0) <= rounded_mul_l(22 downto 0);
        output(30 downto 23) <= exp_l_l(7 downto 0);
      end if;
    end if;
    output(31) <= sign_l_l;
  end process;
end structure;
