library IEEE, STD;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use STD.textio.all;

entity FTOI is
  Port(
    input  :  in STD_LOGIC_VECTOR (31 downto 0);
    output : out STD_LOGIC_VECTOR (31 downto 0);
    clk    :  in STD_LOGIC);
end FTOI;

architecture structure of FTOI is
  signal u_out  : STD_LOGIC_VECTOR (31 downto 0);
  signal sign   : STD_LOGIC;
  signal switch : STD_LOGIC;

begin
  process(input)
  variable exp : integer := 0;

  begin
    exp := to_integer(unsigned(input(30 downto 23)));

    if (exp >= 158) then
      u_out  <= (31 => '1', others => '0');
      sign   <= '0';
      switch <= '0';
    elsif (exp <= 125) then
      u_out  <= (others => '0');
      sign   <= '0';
      switch <= '0';
    elsif (exp = 150) then
      u_out(31 downto 0) <= x"00"&"1" & input(22 downto 0);
      sign   <= input(31);
      switch <= '0';
    elsif (exp <= 149) then
      u_out(31 downto 0)
        <= to_stdlogicvector(to_bitvector(x"00"&"1"&input(22 downto 0))
                             srl (149-exp));
      sign   <= input(31);
      switch <= '1';
    else
      u_out(31 downto 0)
        <= to_stdlogicvector(to_bitvector(x"00"&"1"&input(22 downto 0))
                             sll (exp-150));
      sign   <= input(31);
      switch <= '0';
    end if;
  end process;


  process(u_out, switch, sign)
  begin
    case switch is
      when '0' =>
        case sign is
          when '0'    => output <= u_out;
          when others => output <= (not u_out) + x"00000001";
        end case;

      when others =>
        case sign is
          when '0' =>
            case u_out(0) is
              when '0'    =>
                output <= "0" & u_out(31 downto 1);
              when others =>
                output <= ("0" & u_out(31 downto 1)) + x"00000001";
            end case;
              
          when others =>
            case u_out(0) is
              when '0'    =>
                output <= (not ("0" & u_out(31 downto 1))) + x"00000001";
              when others =>
                output <= not ("0" & u_out(31 downto 1));
            end case;
            
        end case;
        
    end case;
  end process;
end structure;
