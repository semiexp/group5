library IEEE, STD;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use STD.textio.all;

entity FINV is
  Port(
    input  :  in STD_LOGIC_VECTOR (31 downto 0);
    output : out STD_LOGIC_VECTOR (31 downto 0);
    clk    :  in STD_LOGIC);
end FINV;

architecture structure of FINV is
  signal a1 : STD_LOGIC_VECTOR (12 downto 0);
  signal sign : STD_LOGIC;
  signal exp : STD_LOGIC_VECTOR (7 downto 0);
  signal frac1, frac : STD_LOGIC_VECTOR (34 downto 0);
  signal frac2 : STD_LOGIC_VECTOR (25 downto 0);
  signal frac_data : STD_LOGIC_VECTOR (35 downto 0);

  type RamType is array(0 to 1023) of bit_vector(35 downto 0);
  impure function InitRamFromFile (RamFileName : in string) return RamType is
    FILE RamFile : text is in RamFileName;
    variable RamFileLine : line;
    variable RAM : RamType;
  begin
    for I in RamType'range loop
      readline (RamFile, RamFileLine);
      read (RamFileLine, RAM(I));
    end loop;
    return RAM;
  end function;
  signal RAM : RamType := InitRamFromFile("finv.dat");
  signal addr : std_logic_vector(9 downto 0);

begin
  process(clk)
  begin
    if rising_edge(clk) then
      sign <= input(31);
      exp  <= "11111110" - input(30 downto 23);
      addr <= input(22 downto 13);
      a1   <= input(12 downto 0);
    end if;
  end process;

  frac_data <= to_stdLogicVector(RAM(conv_integer(addr)));

  frac1 <= frac_data(35 downto 13) & x"000";
  frac2 <= frac_data(12 downto 0) * a1;
  
  frac <= frac1-(x"00" & "0" & frac2);

  process(sign, exp, frac)
  begin
    --input = inf
    if (exp = "11111111") then
      output(30 downto 0) <= (others => '0');
    --input = 0
    elsif (exp = "11111110") then
      output(30 downto 23) <= (others => '1');
      output(22 downto 0) <= (others => '0');
    else
      output(30 downto 23) <= exp - x"01";
      if (frac(11) = '0' or (frac(10) = '0' and frac(12) = '0')) then
        output(22 downto 0) <= frac(34 downto 12);
      else
        output(22 downto 0) <= frac(34 downto 12) + ("000" & x"00001");
      end if;
    end if;
    output(31) <= sign;
  end process;
end structure;
