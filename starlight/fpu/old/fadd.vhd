library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity FADD is
  Port(
    input1 :  in STD_LOGIC_VECTOR (31 downto 0);
    input2 :  in STD_LOGIC_VECTOR (31 downto 0);
    output : out STD_LOGIC_VECTOR (31 downto 0);
    clk    :  in STD_LOGIC);
end FADD;

architecture structure of FADD is
  type ans_type is (NORM, ZERO, ZERO_NEG, INF, INF_NEG);
  
  signal swapped_in1, swapped_in2
    : STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
  signal man2, man
    : STD_LOGIC_VECTOR (49 downto 0) := (others => '0');
  signal unsigned_man, unsigned_man_l, shifted_man
    : STD_LOGIC_VECTOR (48 downto 0) := (others => '0');
  signal exp, exp_l, exp1_l, shift
    : STD_LOGIC_VECTOR (7 downto 0) := (others => '0');
  signal man1
    : STD_LOGIC_VECTOR (25 downto 0) := (others => '0');
  signal rounded_man, rounded_man_l
    : STD_LOGIC_VECTOR (24 downto 0) := (others => '0');
  signal sign, sign_l, sign_ll, sign_l2, s_in1_sign, s_in2_sign
    : STD_LOGIC := '0';
  signal anstype, anstype_l : ans_type := NORM;
  
begin
  swap : process(input1, input2)
  begin
    --指数部が大きい方を１、小さい方を２の配線につなぐ
    if (input1(30 downto 23) < input2(30 downto 23)) then
      swapped_in1 <= input2;
      swapped_in2 <= input1;
    else
      swapped_in1 <= input1;
      swapped_in2 <= input2;
    end if;
  end process;

  --シフトするビット数(指数の差)
  shift <= swapped_in1(30 downto 23)
         + ((not swapped_in2(30 downto 23)) + "00000001");

  process(swapped_in1, swapped_in2, shift)
  variable man2_1 : STD_LOGIC_VECTOR (49 downto 0) := (others => '0');
    
  begin
    --仮数を生成
    if (swapped_in1(30 downto 23) = x"00") then
      man1 <= (others => '0');
    else
      if (swapped_in1(31) = '0') then
        man1 <= ("00" & x"800000") + ("000" & swapped_in1(22 downto 0));
      else
        man1 <= (not (("00" & x"800000")
                    + ("000" & swapped_in1(22 downto 0))))
              + ("00" & x"000001");
      end if;
    end if;

    if (swapped_in2(30 downto 23) = x"00") then
      man2_1 := (others => '0');
      man2 <= man2_1;
    else
      man2_1 :=
        to_stdlogicvector(to_bitvector
            ((("00" & x"800000") + ("000" & swapped_in2(22 downto 0)))
           & x"000000")
           srl to_integer(unsigned(shift)));
      if (swapped_in2(31) = '0') then
        man2 <= man2_1;
      else
        man2 <= (not man2_1) + ("00" & x"000001");
      end if;
    end if;
  end process;

  man <= (man1 & x"000000") + man2;

  unsign : process(man)
  begin
    if (man(49) = '1') then
      unsigned_man <= not (man(48 downto 0) - ("0" & x"000000000001"));
    else
      unsigned_man <= man(48 downto 0);
    end if;
  end process;
  sign <= man(49);

  latch1 : process(clk)
  begin
    --ラッチ１
    if rising_edge(clk) then
      exp1_l <= swapped_in1(30 downto 23);
      s_in1_sign <= swapped_in1(31);
      s_in2_sign <= swapped_in2(31);
      unsigned_man_l <= unsigned_man(48 downto 0);
      sign_l <= sign;
    end if;
  end process;

  shift1 : process(exp1_l, s_in1_sign, s_in2_sign, unsigned_man_l, sign_l)
  variable var_shift : integer := 0;
  variable infi      : integer := 0;
  begin
    if    (unsigned_man_l(48) = '1') then var_shift := 26;--(-1)
    elsif (unsigned_man_l(47) = '1') then var_shift := 0;
    elsif (unsigned_man_l(46) = '1') then var_shift := 1;
    elsif (unsigned_man_l(45) = '1') then var_shift := 2;
    elsif (unsigned_man_l(44) = '1') then var_shift := 3;
    elsif (unsigned_man_l(43) = '1') then var_shift := 4;
    elsif (unsigned_man_l(42) = '1') then var_shift := 5;
    elsif (unsigned_man_l(41) = '1') then var_shift := 6;
    elsif (unsigned_man_l(40) = '1') then var_shift := 7;
    elsif (unsigned_man_l(39) = '1') then var_shift := 8;
    elsif (unsigned_man_l(38) = '1') then var_shift := 9;
    elsif (unsigned_man_l(37) = '1') then var_shift := 10;
    elsif (unsigned_man_l(36) = '1') then var_shift := 11;
    elsif (unsigned_man_l(35) = '1') then var_shift := 12;
    elsif (unsigned_man_l(34) = '1') then var_shift := 13;
    elsif (unsigned_man_l(33) = '1') then var_shift := 14;
    elsif (unsigned_man_l(32) = '1') then var_shift := 15;
    elsif (unsigned_man_l(31) = '1') then var_shift := 16;
    elsif (unsigned_man_l(30) = '1') then var_shift := 17;
    elsif (unsigned_man_l(29) = '1') then var_shift := 18;
    elsif (unsigned_man_l(28) = '1') then var_shift := 19;
    elsif (unsigned_man_l(27) = '1') then var_shift := 20;
    elsif (unsigned_man_l(26) = '1') then var_shift := 21;
    elsif (unsigned_man_l(25) = '1') then var_shift := 22;
    elsif (unsigned_man_l(24) = '1') then var_shift := 23;
    elsif (unsigned_man_l(23) = '1') then var_shift := 24;
    else                                  var_shift := 25;--man=0
    end if;

    case var_shift is
      when 26 =>
        if (exp1_l >= x"fe") then
          infi := 1;
        else
          infi := 0;
        end if;
        shifted_man <= "0" & unsigned_man_l(48 downto 1);
        exp <= (exp1_l + "00000001");

      when 25 =>
        exp <= (others => '0');
        shifted_man <= (others => '0');
        infi := 0;

      when others =>
        infi := 0;
        shifted_man <=
          "0" & to_stdlogicvector(to_bitvector(unsigned_man_l)
                                  sll (var_shift))(47 downto 0);
        exp <= exp1_l - std_logic_vector(to_unsigned(var_shift, 8));

    end case;
    --±0,±∞の検査
    if (exp1_l = x"00") then
      if (s_in1_sign = '1') and (s_in2_sign = '1') then
        anstype <= ZERO_NEG;
      else
        anstype <= ZERO;
      end if;
    elsif (exp1_l = x"ff") or (infi = 1) then
      if (s_in1_sign = '1') then
        anstype <= INF_NEG;
      else
        anstype <= INF;
      end if;
    else
      anstype <= NORM;
    end if;
    sign_ll <= sign_l;
  end process;

  round : process(shifted_man)
  begin
    if shifted_man(23) = '0' then
      rounded_man <= shifted_man(48 downto 24);
    elsif (shifted_man(24 downto 23) = "11")
       or (shifted_man(22 downto 0) >= 1) then
      rounded_man <= shifted_man(48 downto 24) + x"000001";
    else
      rounded_man <= shifted_man(48 downto 24);
    end if;
  end process;
    
  latch2 : process(clk)
  begin
    --ラッチ２
    if rising_edge(clk) then
      sign_l2 <= sign_ll;
      exp_l <= exp;
      rounded_man_l <= rounded_man;
      anstype_l <= anstype;
    end if;
  end process;

  shift2 : process(exp_l, rounded_man_l, sign_l2, anstype_l)
  begin
    case anstype_l is
      when ZERO =>
        output <= x"00000000";

      when ZERO_NEG =>
        output <= x"80000000";

      when INF =>
        output <= x"7f800000";

      when INF_NEG =>
        output <= x"ff800000";

      when NORM =>
        --丸めによる桁のズレを補正
        if (rounded_man_l(24) = '1') then
          if (exp_l = x"ff") or (exp_l = x"fe") then
            output(30 downto 0) <= "111" & x"f800000";
          else
            output(22 downto 0) <= rounded_man_l(23 downto 1);
            output(30 downto 23) <= (exp_l + "00000001");
          end if;
        elsif (unsigned(rounded_man_l) = 0) then
          output(30 downto 0) <= (others => '0');
        else
          output(22 downto 0) <= rounded_man_l(22 downto 0);
          output(30 downto 23) <= exp_l;
        end if;
        output(31) <= sign_l2;
        
    end case;
  end process;
end structure;
