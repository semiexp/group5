library IEEE, STD;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use STD.textio.all;

entity ITOF is
  Port(
    input  :  in STD_LOGIC_VECTOR (31 downto 0);
    output : out STD_LOGIC_VECTOR (31 downto 0);
    clk    :  in STD_LOGIC);
end ITOF;

architecture structure of ITOF is
  signal u_input : STD_LOGIC_VECTOR (31 downto 0);
  signal sign : STD_LOGIC;

begin
  process(clk)
  begin
    if rising_edge(clk) then
      if (input(31) = '1') then
        u_input <= (not input(31 downto 0)) + ("000"&x"0000001");
        sign    <= '1';
      else
        u_input <= input(31 downto 0);
        sign    <= '0';
      end if;
    end if;
  end process;

  
  process(u_input)
  variable var_shift : integer := 0;
  
  begin
  if    (u_input(31) = '1') then var_shift := 31; --(-8)
  elsif (u_input(30) = '1') then var_shift := 30; --(-7)
  elsif (u_input(29) = '1') then var_shift := 29; --(-6)
  elsif (u_input(28) = '1') then var_shift := 28; --(-5)
  elsif (u_input(27) = '1') then var_shift := 27; --(-4)
  elsif (u_input(26) = '1') then var_shift := 26; --(-3)
  elsif (u_input(25) = '1') then var_shift := 25; --(-2)
  elsif (u_input(24) = '1') then var_shift := 24; --(-1)
  elsif (u_input(23) = '1') then var_shift := 0;
  elsif (u_input(22) = '1') then var_shift := 1; 
  elsif (u_input(21) = '1') then var_shift := 2; 
  elsif (u_input(20) = '1') then var_shift := 3; 
  elsif (u_input(19) = '1') then var_shift := 4; 
  elsif (u_input(18) = '1') then var_shift := 5; 
  elsif (u_input(17) = '1') then var_shift := 6; 
  elsif (u_input(16) = '1') then var_shift := 7; 
  elsif (u_input(15) = '1') then var_shift := 8; 
  elsif (u_input(14) = '1') then var_shift := 9; 
  elsif (u_input(13) = '1') then var_shift := 10; 
  elsif (u_input(12) = '1') then var_shift := 11; 
  elsif (u_input(11) = '1') then var_shift := 12; 
  elsif (u_input(10) = '1') then var_shift := 13; 
  elsif (u_input(9) = '1')  then var_shift := 14; 
  elsif (u_input(8) = '1')  then var_shift := 15; 
  elsif (u_input(7) = '1')  then var_shift := 16; 
  elsif (u_input(6) = '1')  then var_shift := 17; 
  elsif (u_input(5) = '1')  then var_shift := 18; 
  elsif (u_input(4) = '1')  then var_shift := 19; 
  elsif (u_input(3) = '1')  then var_shift := 20; 
  elsif (u_input(2) = '1')  then var_shift := 21; 
  elsif (u_input(1) = '1')  then var_shift := 22; 
  elsif (u_input(0) = '1')  then var_shift := 23;
  else                           var_shift := 32; --man=0
  end if;

  case var_shift is
    when 32 =>
      output(31) <= '0';

    when others =>
      output(31) <= sign;

  end case;

  case var_shift is
    when 32 =>
      output(30 downto 0) <= (others => '0');

    when 31 =>
      output(30 downto 23) <= x"9e";
      output(22 downto 0)  <= (others => '0');

    when 30 =>
      output(30 downto 23) <= x"9d";
      if u_input(6) = '0' then
        output(22 downto 0) <= u_input(29 downto 7);
      elsif (u_input(7 downto 6) = "11")
         or (u_input(5 downto 0) >= 1) then
        output(22 downto 0) <= u_input(29 downto 7) + ("000"&x"00001");
      else
        output(22 downto 0) <= u_input(29 downto 7);
      end if;

    when 29 =>
      output(30 downto 23) <= x"9c";
      if u_input(5) = '0' then
        output(22 downto 0) <= u_input(28 downto 6);
      elsif (u_input(6 downto 5) = "11")
         or (u_input(4 downto 0) >= 1) then
        output(22 downto 0) <= u_input(28 downto 6) + ("000"&x"00001");
      else
        output(22 downto 0) <= u_input(28 downto 6);
      end if;

    when 28 =>
      output(30 downto 23) <= x"9b";
      if u_input(4) = '0' then
        output(22 downto 0) <= u_input(27 downto 5);
      elsif (u_input(5 downto 4) = "11")
         or (u_input(3 downto 0) >= 1) then
        output(22 downto 0) <= u_input(27 downto 5) + ("000"&x"00001");
      else
        output(22 downto 0) <= u_input(27 downto 5);
      end if;

    when 27 =>
      output(30 downto 23) <= x"9a";
      if u_input(3) = '0' then
        output(22 downto 0) <= u_input(26 downto 4);
      elsif (u_input(4 downto 3) = "11")
         or (u_input(2 downto 0) >= 1) then
        output(22 downto 0) <= u_input(26 downto 4) + ("000"&x"00001");
      else
        output(22 downto 0) <= u_input(26 downto 4);
      end if;

    when 26 =>
      output(30 downto 23) <= x"99";
      if u_input(2) = '0' then
        output(22 downto 0) <= u_input(25 downto 3);
      elsif (u_input(3 downto 2) = "11")
         or (u_input(1 downto 0) >= 1) then
        output(22 downto 0) <= u_input(25 downto 3) + ("000"&x"00001");
      else
        output(22 downto 0) <= u_input(25 downto 3);
      end if;

    when 25 =>
      output(30 downto 23) <= x"98";
      if u_input(1) = '0' then
        output(22 downto 0) <= u_input(24 downto 2);
      elsif (u_input(2 downto 1) = "11")
         or (u_input(0) = '1') then
        output(22 downto 0) <= u_input(24 downto 2) + ("000"&x"00001");
      else
        output(22 downto 0) <= u_input(24 downto 2);
      end if;

    when 24 =>
      output(30 downto 23) <= x"97";
      if u_input(1 downto 0) = "11" then
        output(22 downto 0) <= u_input(23 downto 1) + ("000"&x"00001");
      else
        output(22 downto 0) <= u_input(23 downto 1);
      end if;

    when 23 =>
      output(30 downto 23) <= x"96";
      output(22 downto 0) <= u_input(22 downto 0);

    when others =>
      output(30 downto 23)
        <= x"96" - std_logic_vector(to_unsigned(var_shift, 8));
      output(22 downto 0)
        <= to_stdlogicvector(to_bitvector(u_input(22 downto 0))
                             sll (var_shift));

    end case;
  end process;
end structure;
