--2-stage FMUL
--stage1:4.0ns, stage2:5.6ns
--~120MHz?

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity FMUL is
  Port(
    input1 :  in STD_LOGIC_VECTOR (31 downto 0);
    input2 :  in STD_LOGIC_VECTOR (31 downto 0);
    output : out STD_LOGIC_VECTOR (31 downto 0);
    clk    :  in STD_LOGIC);
end FMUL;

architecture structure of FMUL is
  type ans_type is (NORM, ZERO, INF);

  signal mul1_reg,
         mul1,
         r_man_fwd1,
         r_man_fwd2 : STD_LOGIC_VECTOR (30 downto 0) := (others => '0');
  signal mul2_reg : STD_LOGIC_VECTOR (23 downto 0) := (others => '0');
  signal mul2 : STD_LOGIC_VECTOR (40 downto 0) := (others => '0');
  signal man : STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
  signal s_man : STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
  signal r_man : STD_LOGIC_VECTOR (24 downto 0) := (others => '0');
  signal exp1,
         exp2,
         exp3,
         exp1_reg,
         exp2_reg : STD_LOGIC_VECTOR (8 downto 0) := (others => '0');
  signal sign_reg : STD_LOGIC := '0';
  signal anstype : ans_type  := NORM;

  attribute use_dsp48 : string;
  attribute use_dsp48 of man : signal is "no";
  
begin
  --stage1------------------------------------------------
  exp1 <= ("0" & input1(30 downto 23))
          + ("0" & input2(30 downto 23))
          - ("0"&x"7f");

  exp2 <= ("0" & input1(30 downto 23))
          + ("0" & input2(30 downto 23))
          - ("0"&x"7e");

  exp3 <= ("0" & input1(30 downto 23))
          + ("0" & input2(30 downto 23));

  mul1 <= ("1" & input1(22 downto 17))
        * ("1" & input2(22 downto 0));

  mul2 <= input1(16 downto 0)
        * ("1" & input2(22 downto 0));

  process(clk)
  begin
    if rising_edge(clk) then
      mul1_reg <= mul1;
      mul2_reg <= mul2(40 downto 17);
      if (mul2(16 downto 0) = "0"&x"0000") then
        man(0) <= '0';
      else
        man(0) <= '1';
      end if;

      sign_reg <= input1(31) xor input2(31);
      exp1_reg <= exp1;
      exp2_reg <= exp2;

      -- exp3 <= "001111110" -> zero
      if (input1(30 downto 23) = x"00" or input2(30 downto 23) = x"00"
          or (exp3(8 downto 7) = "00" and exp3(6 downto 0) /= "1111111")) then
        anstype <= ZERO;
      -- exp1 >= "011111111" -> inf
      elsif (input1(30 downto 23) = x"ff" or input2(30 downto 23) = x"ff"
             or exp1(8) = '1' or exp1(7 downto 0) = x"ff") then
        anstype <= INF;
      else
        anstype <= NORM;
      end if;
    end if;
  end process;

  
  --stage2-----------------------------------------------
  man(31 downto 1) <= mul1_reg + mul2_reg;
  r_man_fwd1 <= mul1_reg + mul2_reg + x"40";
  r_man_fwd2 <= mul1_reg + mul2_reg + x"80";

  --shift
  s_man <= man(31 downto 0) when (man(31) = '1') else
           man(30 downto 0) & "0";
  
  --round (man < fffffe000002)
  process(s_man, man, r_man_fwd1, r_man_fwd2)
  begin
    if (s_man(7) = '0' or (s_man(6 downto 0) = "0000000"
                           and s_man(8) = '0')) then
      if (man(31) = '0') then
        r_man <= "0" & s_man(31 downto 8);
      else
        r_man <= s_man(31 downto 8) & "0";
      end if;
    else
      if (man(31) = '0') then
        r_man <= r_man_fwd1(30 downto 6);
      else
        r_man <= r_man_fwd2(30 downto 6);
      end if;
    end if;
  end process;
     
  process(exp1_reg, exp2_reg, man, r_man, sign_reg, anstype)
  begin
    if (anstype = ZERO or (r_man(24) = '0'
                           and exp1_reg(7 downto 0) = x"00")) then
      output(30 downto 0) <= (others => '0');
    elsif (anstype = INF or (r_man(24) = '1'
                             and exp2_reg(7 downto 0) = x"ff")) then
      output(30 downto 0) <= "111"&x"f800000";
    elsif (r_man(24) = '1') then
      output(30 downto 23) <= exp2_reg(7 downto 0);
      output(22 downto 0) <= r_man(23 downto 1);
    else
      output(30 downto 23) <= exp1_reg(7 downto 0);
      output(22 downto 0)  <= r_man(22 downto 0);
    end if;
    output(31) <= sign_reg;
  end process;
end structure;
