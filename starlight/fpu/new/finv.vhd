--2-stage FINV
--stage1:3.5ns, stage2:5.2ns
--~120MHz

library IEEE, STD;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use STD.textio.all;

entity FINV is
  Port(
    input  :  in STD_LOGIC_VECTOR (31 downto 0);
    output : out STD_LOGIC_VECTOR (31 downto 0);
    clk    :  in STD_LOGIC);
end FINV;

architecture structure of FINV is

  signal exp_reg   : STD_LOGIC_VECTOR (7  downto 0) := (others => '0');
  signal man       : STD_LOGIC_VECTOR (34 downto 0) := (others => '0');
  signal const_reg : STD_LOGIC_VECTOR (22 downto 0) := (others => '0');
  signal grad_reg  : STD_LOGIC_VECTOR (12 downto 0) := (others => '0');
  signal a1_reg    : STD_LOGIC_VECTOR (12 downto 0) := (others => '0');
  signal res       : STD_LOGIC_VECTOR (34 downto 0) := (others => '0');
  signal man_data  : STD_LOGIC_VECTOR (35 downto 0) := (others => '0');
  signal sign_reg  : STD_LOGIC := '0';
  
  type RomType is array(0 to 1023) of bit_vector(35 downto 0);
  impure function InitRomFromFile (RomFileName : in string) return RomType is
    FILE RomFile : text open read_mode is RomFileName;
    variable RomFileLine : line;
    variable ROM : RomType;
  begin
    for I in RomType'range loop
      readline (RomFile, RomFileLine);
      read (RomFileLine, ROM(I));
    end loop;
    return ROM;
  end function;

  signal ROM : RomType := InitRomFromFile("finv.dat");

begin
  --stage1---------------------------------------------------------
  man_data <= to_stdLogicVector(ROM(conv_integer(input(22 downto 13))));

  process(clk)
  begin
    if rising_edge(clk) then
      exp_reg   <= "11111110" - input(30 downto 23);
      sign_reg  <= input(31);
    end if;
  end process;

  res <= (const_reg&x"000") - a1_reg*grad_reg;

  process(clk)
  begin
    if rising_edge(clk) then
      const_reg <= man_data(35 downto 13);
      grad_reg  <= man_data(12 downto 0);
      a1_reg    <= input(12 downto 0);
    end if;
  end process;

  
  --stage2----------------------------------------------------------
  man <= res;

  process(sign_reg, exp_reg, man)
  begin
    --input = inf
    if (exp_reg = "11111111") then
      output(30 downto 0) <= (others => '0');
    --input = 0
    elsif (exp_reg = "11111110") then
      output(30 downto 23) <= (others => '1');
      output(22 downto 0) <= (others => '0');
    else
      output(30 downto 23) <= exp_reg - x"01";
      output(22 downto 0) <= man(34 downto 12);
    end if;
    output(31) <= sign_reg;
  end process;
end structure;
