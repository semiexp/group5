--2-stage FSQRT
--stage1:3.5ns, stage2:5.0ns
--~120MHz

library IEEE, STD;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use STD.textio.all;

entity FSQRT is
  Port(
    input  :  in STD_LOGIC_VECTOR (31 downto 0);
    output : out STD_LOGIC_VECTOR (31 downto 0);
    clk    :  in STD_LOGIC);
end FSQRT;

architecture structure of FSQRT is
  type ans_type is (NORM, ZERO, INF, IMG);

  signal man_data     : STD_LOGIC_VECTOR (35 downto 0) := (others => '0');
  signal const_reg    : STD_LOGIC_VECTOR (22 downto 0) := (others => '0');
  signal grad_reg     : STD_LOGIC_VECTOR (12 downto 0) := (others => '0');
  signal a1_reg       : STD_LOGIC_VECTOR (13 downto 0) := (others => '0');
  signal man,
         res1, res2   : STD_LOGIC_VECTOR (37 downto 0) := (others => '0');
  signal exp_reg      : STD_LOGIC_VECTOR (7 downto 0)  := (others => '0');
  signal exp0_reg     : STD_LOGIC := '0';
  signal sign_reg     : STD_LOGIC := '0';
  signal anstype_reg  : ans_type  := NORM;
  
  type RomType is array(0 to 1023) of bit_vector(35 downto 0);
  impure function InitRomFromFile (RomFileName : in string) return RomType is
    FILE RomFile : text is in RomFileName;
    variable RomFileLine : line;
    variable ROM : RomType;
  begin
    for I in RomType'range loop
      readline (RomFile, RomFileLine);
      read (RomFileLine, ROM(I));
    end loop;
    return ROM;
  end function;

  signal ROM : RomType := InitRomFromFile("fsqrt.dat");
  
begin
  --stage1----------------------------------------------
  man_data <= to_stdLogicVector(ROM(conv_integer(input(23 downto 14))));

  process(clk)
  begin
    if rising_edge(clk) then
      sign_reg <= input(31);
      exp_reg  <= ("0" & input(30 downto 24))
                + "00111111" + input(23);
      exp0_reg <= input(23);

      if (input(30 downto 23) = x"00") then
        anstype_reg <= ZERO;
      elsif (input(31) = '1') then
        anstype_reg <= IMG;
      elsif (input(30 downto 23) = x"ff") then
        anstype_reg <= INF;
      else
        anstype_reg <= NORM;
      end if;
    end if;
  end process;

  res1 <= (const_reg&"000"&x"000") + ("1"&grad_reg)*a1_reg;
  res2 <= (const_reg&"000"&x"000") + ("10"&grad_reg)*a1_reg;

  process(clk)
  begin
    if rising_edge(clk) then
      a1_reg    <= input(13 downto 0);
      const_reg <= man_data(35 downto 13);
      grad_reg  <= man_data(12 downto 0);
    end if;
  end process;
  
  
  --stage2----------------------------------------------
  man <= res1 when (exp0_reg = '1') else
         res2;
  
  process(sign_reg, exp_reg, man)
  begin
    case anstype_reg is
      when ZERO => output(30 downto 0) <= (others => '0');
      when IMG  => output(30 downto 0) <= "111"&x"fc00000";
      when INF  => output(30 downto 0) <= "111"&x"f800000";
      when others =>
        output(30 downto 23) <= exp_reg;
        output(22 downto 0)  <= man(37 downto 15);
    end case;
    output(31) <= sign_reg;
  end process;
end structure;

