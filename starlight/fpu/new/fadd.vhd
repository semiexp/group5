--3-stage pipeline
--~133MHz

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity FADD is
  Port(
    input1 :  in STD_LOGIC_VECTOR (31 downto 0);
    input2 :  in STD_LOGIC_VECTOR (31 downto 0);
    output : out STD_LOGIC_VECTOR (31 downto 0);
    clk    :  in STD_LOGIC);
end FADD;

architecture structure of FADD is
  type ans_type is (NORM, ZERO, INF);

  --stage1
  signal sw1_p2   : STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
  signal sw2_p2   : STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
  signal man_p1   : STD_LOGIC_VECTOR (25 downto 0) := (others => '0');
  signal man2_p2  : STD_LOGIC_VECTOR (49 downto 0) := (others => '0');
  signal shift_p2 : STD_LOGIC_VECTOR (8 downto 0)  := (others => '0');
  --stage2
  signal man_p1_reg1   : STD_LOGIC_VECTOR (25 downto 0) := (others => '0');
  signal frac1_p2_reg1 : STD_LOGIC_VECTOR (22 downto 0) := (others => '0');
  signal frac2_p2_reg1 : STD_LOGIC_VECTOR (22 downto 0) := (others => '0');
  signal shift_p2_reg1 : STD_LOGIC_VECTOR (7 downto 0)  := (others => '0');
  signal sign1_p2_reg1 : STD_LOGIC := '0';
  signal sign2_p2_reg1 : STD_LOGIC := '0';
  signal exp_reg1      : STD_LOGIC_VECTOR (7 downto 0)  := (others => '0');
  signal switch_reg1   : STD_LOGIC := '0';

  signal s_man_p1      : STD_LOGIC_VECTOR (24 downto 0) := (others => '0');
  signal exp_p1        : STD_LOGIC_VECTOR (8 downto 0)  := (others => '0');
  signal man_p2        : STD_LOGIC_VECTOR (49 downto 0) := (others => '0');
  signal s_man_p2      : STD_LOGIC_VECTOR (46 downto 0) := (others => '0');
  signal exp_p2        : STD_LOGIC_VECTOR (8 downto 0)  := (others => '0');
  --stage3
  signal s_man_reg2   : STD_LOGIC_VECTOR (46 downto 0) := (others => '0');
  signal exp_reg2     : STD_LOGIC_VECTOR (8 downto 0)  := (others => '0');
  signal sign_reg2    : STD_LOGIC := '0';
  signal anstype_reg2 : ans_type  := NORM;

  signal r_man        : STD_LOGIC_VECTOR (23 downto 0) := (others => '0');
  signal anstype      : ans_type  := NORM;
  
begin
  --stage1--------------------------------------------------
  --path1(|exp1-exp2|<=1)-----------------------------------
  --add
  process(input1, input2)
  begin
    if (input1(30 downto 23) = x"00") then
      man_p1 <= "01"&input2(22 downto 0)&"0";
    elsif (input2(30 downto 23) = x"00") then
      man_p1 <= "01"&input1(22 downto 0)&"0";
    --exp1 = exp2
    elsif (input1(23) = input2(23)) then
      if (input1(31) = input2(31)) then
        man_p1 <= "1"&(("0"&input1(22 downto 0))
                     + ("0"&input2(22 downto 0)))&"0";
      else
        man_p1 <=
          std_logic_vector(abs(signed(("00"&input1(22 downto 0))
                                    - ("00"&input2(22 downto 0))))) & "0";
      end if;
    --exp1 - exp2 = 1
    elsif (input1(30 downto 23) - input2(30 downto 23) = x"01") then
      if (input1(31) = input2(31)) then
        man_p1 <= ("01"&input1(22 downto 0)&"0")
                + ("001"&input2(22 downto 0));
      else
        man_p1 <= ("01"&input1(22 downto 0)&"0")
                - ("001"&input2(22 downto 0));
      end if;
    --exp1 - exp2 = -1
    else
      if (input1(31) = input2(31)) then
        man_p1 <= ("001"&input1(22 downto 0))
                + ("01"&input2(22 downto 0)&"0");
      else
        man_p1 <= ("01"&input2(22 downto 0)&"0")
                - ("001"&input1(22 downto 0));
      end if;
    end if;
  end process;

  --path2(|exp1-exp2|>1)---------------------------------------------
  --swap
  process(input1, input2)
  begin
    if (input1(30 downto 23) < input2(30 downto 23)) then
      sw1_p2   <= input2;
      sw2_p2   <= input1;
    else
      sw1_p2   <= input1;
      sw2_p2   <= input2;
    end if;
  end process;

  shift_p2 <=
    std_logic_vector(abs(signed(("0"&input1(30 downto 23))
                              - ("0"&input2(30 downto 23)))));

  --pipeline registers--------------------------------------------
  process(clk)
  begin
    if rising_edge(clk) then
      exp_reg1      <= sw1_p2(30 downto 23);
      man_p1_reg1   <= man_p1;
      shift_p2_reg1 <= shift_p2(7 downto 0);
      sign1_p2_reg1 <= sw1_p2(31);
      sign2_p2_reg1 <= sw2_p2(31);
      frac1_p2_reg1 <= sw1_p2(22 downto 0);
      frac2_p2_reg1 <= sw2_p2(22 downto 0);
    end if;
  end process;
  
  process(clk)
  begin
    if rising_edge(clk) then
      if (shift_p2(7 downto 1) = "0000000") then
        switch_reg1 <= '0';
      else
        switch_reg1 <= '1';
      end if;
    end if;
  end process;


  --stage2--------------------------------------------------------
  --path1---------------------------------------------------------
  --lshift
  process(exp_reg1, man_p1_reg1)
  variable var_shift : integer := 0;

  begin
    if    (man_p1_reg1(25) = '1') then var_shift := 26;--(-1)
    elsif (man_p1_reg1(24) = '1') then var_shift := 0;
    elsif (man_p1_reg1(23) = '1') then var_shift := 1;
    elsif (man_p1_reg1(22) = '1') then var_shift := 2;
    elsif (man_p1_reg1(21) = '1') then var_shift := 3;
    elsif (man_p1_reg1(20) = '1') then var_shift := 4;
    elsif (man_p1_reg1(19) = '1') then var_shift := 5;
    elsif (man_p1_reg1(18) = '1') then var_shift := 6;
    elsif (man_p1_reg1(17) = '1') then var_shift := 7;
    elsif (man_p1_reg1(16) = '1') then var_shift := 8;
    elsif (man_p1_reg1(15) = '1') then var_shift := 9;
    elsif (man_p1_reg1(14) = '1') then var_shift := 10;
    elsif (man_p1_reg1(13) = '1') then var_shift := 11;
    elsif (man_p1_reg1(12) = '1') then var_shift := 12;
    elsif (man_p1_reg1(11) = '1') then var_shift := 13;
    elsif (man_p1_reg1(10) = '1') then var_shift := 14;
    elsif (man_p1_reg1(9)  = '1') then var_shift := 15;
    elsif (man_p1_reg1(8)  = '1') then var_shift := 16;
    elsif (man_p1_reg1(7)  = '1') then var_shift := 17;
    elsif (man_p1_reg1(6)  = '1') then var_shift := 18;
    elsif (man_p1_reg1(5)  = '1') then var_shift := 19;
    elsif (man_p1_reg1(4)  = '1') then var_shift := 20;
    elsif (man_p1_reg1(3)  = '1') then var_shift := 21;
    elsif (man_p1_reg1(2)  = '1') then var_shift := 22;
    elsif (man_p1_reg1(1)  = '1') then var_shift := 23;
    elsif (man_p1_reg1(0)  = '1') then var_shift := 24;
    else                               var_shift := 25;--man=0
    end if;

    case var_shift is
      --u_man = 1x.xxxx
      when 26 =>
        s_man_p1 <= man_p1_reg1(24 downto 0);
        exp_p1   <= ("0" & exp_reg1) + "000000001";

      --u_man = 00.0000
      when 25 =>
        s_man_p1 <= (others => '0');
        exp_p1   <= (others => '0');

      --u_man = 0x.xxxx
      when others =>
        s_man_p1 <=
          to_stdlogicvector(to_bitvector(man_p1_reg1(23 downto 0))
                            sll (var_shift)) & "0";
        if (exp_reg1 <= var_shift) then
          exp_p1 <= (others => '0');
        else
          exp_p1 <= "0" & (exp_reg1
                         - std_logic_vector(to_unsigned(var_shift, 8)));
        end if;
    end case;
  end process;

  --path2---------------------------------------------------------
  --rshift
  man2_p2 <=
    (others => '0') when (exp_reg1 = shift_p2_reg1) else
    to_stdlogicvector(
      to_bitvector("001" & frac2_p2_reg1(22 downto 0) & x"000000")
      srl to_integer(unsigned(shift_p2_reg1)));
  
  --add
  man_p2 <=
    (("001" & frac1_p2_reg1) + man2_p2(49 downto 24)) & man2_p2(23 downto 0)
  when ((sign1_p2_reg1 xor sign2_p2_reg1) = '0') else
    ("001" & frac1_p2_reg1 & x"000000") - man2_p2;

  --shift
  s_man_p2 <=
    --man = 1x.xxxx...0, man(0) = '0'
    man_p2(47 downto 1) when (man_p2(48) = '1') else
    --man = 01.xxxx
    man_p2(46 downto 0) when (man_p2(47) = '1') else
    --man = 00.1xxx
    man_p2(45 downto 0) & "0";

  process(exp_reg1, man_p2)
  begin
    if (man_p2(48) = '1') then
      exp_p2 <= ("0" & exp_reg1) + "000000001";
    elsif (man_p2(47) = '1') then
      exp_p2 <= "0" & exp_reg1;
    else
      exp_p2 <= ("0" & exp_reg1) - "000000001";
    end if;
  end process;

  --pipeline registers--------------------------------------------
  --irregular
  process(clk)
  begin
    if rising_edge(clk) then
      if (exp_reg1 = x"ff") then
        anstype_reg2 <= INF;
      elsif (exp_reg1 = x"00") then
        anstype_reg2 <= ZERO;
      else
        anstype_reg2 <= NORM;
      end if;
    end if;
  end process;

  --sign
  process(clk)
  begin
    if rising_edge(clk) then
      if ((sign1_p2_reg1 xor sign2_p2_reg1) = '1') then
        if (exp_reg1 = x"00") then
          sign_reg2 <= '0';
        elsif (shift_p2_reg1 = x"00") then
          if (frac1_p2_reg1 = frac2_p2_reg1) then
            sign_reg2 <= '0';
          elsif (frac1_p2_reg1 > frac2_p2_reg1) then
            sign_reg2 <= sign1_p2_reg1;
          else
            sign_reg2 <= sign2_p2_reg1;
          end if;
        else
          sign_reg2 <= sign1_p2_reg1;
        end if;
      else
        sign_reg2 <= sign1_p2_reg1;
      end if;
    end if;
  end process;

  --select path
  process(clk)
  begin
    if rising_edge(clk) then
      if (switch_reg1 = '0') then
        exp_reg2   <= exp_p1;
        s_man_reg2 <= s_man_p1 & "00"&x"00000";
      else
        exp_reg2   <= exp_p2;
        s_man_reg2 <= s_man_p2;
      end if;
    end if;
  end process;


  --stage3--------------------------------------------------------
  --round
  --s_man is economized
  r_man <=
    "0" & s_man_reg2(46 downto 24) when (s_man_reg2(23) = '0'
                                     or (s_man_reg2(22 downto 0) = "000"&x"00000"
                                     and s_man_reg2(24) = '0')) else
    ("0" & s_man_reg2(46 downto 24)) + 1;

  --irregular
  process(exp_reg2, anstype_reg2)
  begin
    case anstype_reg2 is
      --exp_reg2 /= ff, 00
      when NORM =>
        if (exp_reg2 = "000000000") then
          anstype <= ZERO;
        elsif (exp_reg2(7 downto 0) = x"ff" or exp_reg2(8) = '1') then
          anstype <= INF;
        else
          anstype <= NORM;
        end if;

      when others => anstype <= anstype_reg2;
    end case;
  end process;

  --output
  process(exp_reg2, r_man, sign_reg2, anstype)
  begin
    case anstype is
      when ZERO   => output(30 downto 0) <= (others => '0');
      when INF    => output(30 downto 0) <= "111" & x"f800000";
      when others =>
        --10.xxxx -> 1.0xxxx (r_man is economized)
        if (r_man(23) = '1') then
          if (exp_reg2(7 downto 1) = "1111111") then
            output(30 downto 0) <= "111" & x"f800000";
          else
            output(22 downto 0)  <= "0" & r_man(22 downto 1);
            output(30 downto 23) <= (exp_reg2(7 downto 0) + 1);
          end if;
        else
          output(22 downto 0)  <= r_man(22 downto 0);
          output(30 downto 23) <= exp_reg2(7 downto 0);
        end if;
    end case;
    output(31) <= sign_reg2;
  end process;
end structure;
