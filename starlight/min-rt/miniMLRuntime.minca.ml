let fabs = [fabs] in
let fneg = [fneg] in
let read_char = [read] in
let print_char = [out] in
let rec fless x y = x < y in
let rec fispos x = x > 0.0 in
let rec fisneg x = x < 0.0 in
let rec fiszero x = (x = 0.0) in
let rec fhalf x = x *. 0.5 in
let rec fsqr x = x *. x in
let _ = fless 0.0 0.0 in
  ()
