library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

package common is
	constant n_register : integer := 64;
	constant register_index_size : integer := 6;
	constant rob_size : integer := 16;
	constant rob_index_size : integer := 4;
	constant cdb_width : integer := 2;
	
	subtype instruction_t is std_logic_vector(31 downto 0);
	subtype value_t is std_logic_vector(31 downto 0);
	subtype vtvalue_t is std_logic_vector(32 downto 0); -- representing a value which may be virtual

	subtype register_idx_t is std_logic_vector((register_index_size - 1) downto 0);
	subtype rob_index_t is std_logic_vector((rob_index_size - 1) downto 0);
	subtype rob_location_t is integer range 0 to (rob_size - 1);
	
	subtype unit_op_t is std_logic_vector(3 downto 0);
	constant alu_op_add : unit_op_t := "0000";
	constant alu_op_sub : unit_op_t := "0001";
	constant alu_op_movhi : unit_op_t := "0010";
	constant alu_op_cmp : unit_op_t := "0011";
	constant alu_op_lshift : unit_op_t := "0100";
	constant alu_op_rshift : unit_op_t := "0101";
	constant alu_op_fabs : unit_op_t := "0110";
	constant alu_op_fcmp : unit_op_t := "0111";
	
	type register_t is array(0 to (n_register - 1)) of value_t;
	type register_rsv_t is array(0 to (n_register - 1)) of rob_index_t;
	subtype register_is_virtual_t is std_logic_vector(0 to (n_register - 1));
	
	type rob_value_t is array(0 to (rob_size - 1)) of vtvalue_t;
	type rob_target_t is array(0 to (rob_size - 1)) of register_idx_t;
	
	type cdb_destination_t is array(0 to (cdb_width - 1)) of rob_index_t;
	type cdb_value_t is array(0 to (cdb_width - 1)) of value_t;
	subtype cdb_enable_t is std_logic_vector(0 to (cdb_width - 1));
	
	type cdb_t is record
		destination : cdb_destination_t;
		data : cdb_value_t;
		enable : cdb_enable_t;
	end record;
	
	-- decoder -> alu / fpu
	type decoder_to_afu_unit_t is record
		enable : std_logic;
		value1, value2 : vtvalue_t;
		destination : rob_index_t;
		operation : unit_op_t;
	end record;
	
	-- decoder -> memory unit
	type decoder_to_mem_unit_t is record
		enable : std_logic;
		addr, wvalue, ofs : vtvalue_t;
		destination : rob_index_t;
		is_write : std_logic;
	end record;
	
	-- decoder -> outunit
	type decoder_to_out_unit_t is record
		enable : std_logic;
		value1 : vtvalue_t;
	end record;

	-- (any unit) -> arbiter
	type unit_to_arbiter_t is record
		destination : rob_index_t;
		data : value_t;
		enable : std_logic;
	end record;
	
	type decoder_in_t is record
		-- from instruction fetch
		inst_enable : std_logic;
		instruction : instruction_t;
		pc : std_logic_vector(19 downto 0);
		
		-- from each unit
		alu_acceptable : std_logic;
		fadd_acceptable : std_logic;
		mem_acceptable : std_logic;
		ounit_acceptable : std_logic;
		
		-- from CDB
		cdb : cdb_t;
	end record;
	
	type decoder_out_t is record
		-- to each unit
		alu_out : decoder_to_afu_unit_t;
		fadd_out : decoder_to_afu_unit_t;
		mem_out : decoder_to_mem_unit_t;
		ounit_out : decoder_to_out_unit_t;
		
		-- to instruction fetch
		is_halt : std_logic;
		is_stall : std_logic;
	end record;
	
	type arbiter_in_t is record
		-- from each unit
		alu_in : unit_to_arbiter_t;
		fadd_in : unit_to_arbiter_t;
		mem_in : unit_to_arbiter_t;
	end record;
	
	type arbiter_out_t is record
		alu_accepted : std_logic;
		fadd_accepted : std_logic;
		mem_accepted : std_logic;
		
		cdb : cdb_t;
	end record;
	
	type afu_unit_in_t is record
		-- from decoder
		input : decoder_to_afu_unit_t;
		
		-- from arbiter
		accepted : std_logic;
		
		-- from CDB
		cdb : cdb_t;
	end record;
	
	type afu_unit_out_t is record
		-- to decoder
		acceptable : std_logic;
		
		-- to arbiter
		output : unit_to_arbiter_t;
	end record;
	
	type mem_unit_in_t is record
		-- from decoder
		input : decoder_to_mem_unit_t;
		
		-- from arbiter
		accepted : std_logic;
		
		-- from CDB
		cdb : cdb_t;
		
		-- from memory controller
		read_data : value_t;
		read_fetched : std_logic;
	end record;
	
	type mem_unit_out_t is record
		-- to decoder
		acceptable : std_logic;
		
		-- to arbiter
		output : unit_to_arbiter_t;
		
		-- to memory controller
		address : std_logic_vector(19 downto 0);
		write_enable, read_enable : std_logic;
		write_data : value_t;
	end record;
	
	type out_unit_in_t is record
		-- from decoder
		input : decoder_to_out_unit_t;
		
		-- from CDB
		cdb : cdb_t;
		
		-- from I/O unit
		write_busy : std_logic;
	end record;
	
	type out_unit_out_t is record
		-- to decoder
		acceptable : std_logic;
		
		-- to I/O unit
		write_enable : std_logic;
		write_data : std_logic_vector(7 downto 0);
	end record;
	
	function retrive_cdb(cdb : cdb_t; v : vtvalue_t) return vtvalue_t;
	function is_virtual(v : vtvalue_t) return std_logic;
	function is_real(v : vtvalue_t) return std_logic;
	function get_value(v : vtvalue_t) return value_t;

	function perform_ALU(value1 : value_t; value2 : value_t; op : unit_op_t) return value_t;
end;

package body common is
	function retrive_cdb(cdb : cdb_t; v : vtvalue_t) return vtvalue_t is
		variable ret : vtvalue_t;
	begin
		ret := v;
		
		if v(32) = '1' then
			if v(3 downto 0) = cdb.destination(0) and cdb.enable(0) = '1' then
				ret := '0' & cdb.data(0);
			elsif v(3 downto 0) = cdb.destination(1) and cdb.enable(1) = '1' then
				ret := '0' & cdb.data(1);
			end if;
		end if;
		
		return ret;
	end retrive_cdb;

	function is_virtual(v : vtvalue_t) return std_logic is
	begin
		return v(32);
	end is_virtual;

	function is_real(v : vtvalue_t) return std_logic is
	begin
		return (not v(32));
	end is_real;

	function get_value(v : vtvalue_t) return value_t is
	begin
		return v(31 downto 0);
	end get_value;

	function perform_ALU(value1 : value_t; value2 : value_t; op : unit_op_t) return value_t is
		variable ret : value_t;
		variable neg_value2 : value_t;
	begin
		case op is
		when alu_op_add =>
			ret := value1 + value2;
		when alu_op_lshift =>
			if value2(31) = '1' then
				neg_value2 := x"00000000" - value2;
				ret := shr(value1, neg_value2(4 downto 0));
			else
				ret := shl(value1, value2(4 downto 0));
			end if;
		when alu_op_rshift =>
			if value2(31) = '1' then
				neg_value2 := x"00000000" - value2;
				ret := shl(value1, neg_value2(4 downto 0));
			else
				ret := shr(value1, value2(4 downto 0));
			end if;
		when others =>
			ret := x"00000000";
		end case;
		
		return ret;
	end perform_ALU;
end common;

