library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

use work.common.all;

entity MemUnit is
	Port (
		clk : in std_logic;
		unit_in : in mem_unit_in_t;
		unit_out : out mem_unit_out_t
	 );
end MemUnit;

architecture rtl of MemUnit is
	type values_t is array(0 to 3) of vtvalue_t;
	type dest_t is array(0 to 3) of rob_index_t;
	subtype state_t is std_logic;
	subtype rs_index is integer range 0 to 4;
	
	constant rs_waiting : state_t := '0';
	constant rs_running : state_t := '1';
	
	type reg_type is record
		-- reservation station
		addr, ofs, wvalue : values_t;
		dest : dest_t;
		is_write : std_logic_vector(0 to 3);
		status : state_t;
		
		rs_size : rs_index;
	end record;
	
	constant rzero : reg_type := (
		addr => (others => '0' & x"00000000"),
		ofs => (others => '0' & x"00000000"),
		wvalue => (others => '0' & x"00000000"),
		dest => (others => "0000"),
		is_write => "0000",
		status => rs_waiting,
		rs_size => 0
	);
	
	signal r, rin : reg_type := rzero;
	
begin
	process (r, unit_in)
		variable v : reg_type;
		
		variable n_addr, n_wvalue : vtvalue_t;
		
		variable wenable, renable : std_logic;
		variable mc_addr : std_logic_vector(19 downto 0);
		variable wdata : value_t;
		variable rsv_pop : std_logic;
		
		variable output_enable : std_logic;
		variable output_dest : rob_index_t;
		variable output_data : value_t;
	begin
		v := r;
		
		-- decide acceptance
		if v.rs_size <= 2 or (v.rs_size = 3 and unit_in.input.enable = '0') then
			unit_out.acceptable <= '1';
		else
			unit_out.acceptable <= '0';
		end if;
		
		for i in 0 to 3 loop
			v.addr(i) := retrive_cdb(unit_in.cdb, v.addr(i));
			v.wvalue(i) := retrive_cdb(unit_in.cdb, v.wvalue(i));
		end loop;
		if unit_in.input.enable = '1' and v.rs_size < 4 then
			v.addr(v.rs_size) := retrive_cdb(unit_in.cdb, unit_in.input.addr);
			v.wvalue(v.rs_size) := retrive_cdb(unit_in.cdb, unit_in.input.wvalue);
			v.ofs(v.rs_size) := unit_in.input.ofs;
			v.is_write(v.rs_size) := unit_in.input.is_write;
			v.dest(v.rs_size) := unit_in.input.destination;
			v.rs_size := v.rs_size + 1;
		end if;
		
		wenable := '0';
		renable := '0';
		mc_addr := x"00000";
		wdata := x"00000000";
		rsv_pop := '0';
		if v.rs_size > 0 and is_real(v.addr(0)) = '1' and is_real(v.wvalue(0)) = '1' and v.status = rs_waiting then
			if v.is_write(0) = '1' then
				-- perform write
				wenable := '1';
				mc_addr := v.addr(0)(19 downto 0) + v.ofs(0)(19 downto 0);
				wdata := get_value(v.wvalue(0));
				rsv_pop := '1';
			else
				renable := '1';
				mc_addr := v.addr(0)(19 downto 0) + v.ofs(0)(19 downto 0);
				v.status := rs_running;
			end if;
		end if;
		
		output_enable := '0';
		output_dest := "0000";
		output_data := x"00000000";
		if v.status = rs_running and unit_in.read_fetched = '1' then
			v.status := rs_waiting;
			rsv_pop := '1';
			
			output_enable := '1';
			output_dest := v.dest(0);
			output_data := unit_in.read_data;
		end if;
		
		if rsv_pop = '1' then
			for i in 0 to 2 loop
				v.addr(i) := v.addr(i + 1);
				v.wvalue(i) := v.wvalue(i + 1);
				v.ofs(i) := v.ofs(i + 1);
				v.is_write(i) := v.is_write(i +  1);
				v.dest(i) := v.dest(i + 1);
			end loop;
			v.addr(3) := '0' & x"00000000";
			v.wvalue(3) := '0' & x"00000000";
			v.ofs(3) := '0' & x"00000000";
			v.is_write(3) := '0';
			v.dest(3) := "0000";
			v.rs_size := v.rs_size - 1;
		end if;
		
		rin <= v;

		if output_enable = '1' then
			-- memory unit is defiant of the acceptance by the arbiter
			unit_out.output.destination <= output_dest;
			unit_out.output.data <= output_data;
			unit_out.output.enable <= '1';
		else
			unit_out.output.destination <= "0000";
			unit_out.output.data <= x"00000000";
			unit_out.output.enable <= '0';
		end if;

		unit_out.write_enable <= wenable;
		unit_out.read_enable <= renable;
		unit_out.address <= mc_addr;
		unit_out.write_data <= wdata;
	end process;
	
	process (clk)
	begin
		if rising_edge(clk) then
			r <= rin;
		end if;
	end process;
end;
