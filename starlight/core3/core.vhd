library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_arith.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

use work.common.all;

entity StarlightCore3 is
	generic (
		read_magic : std_logic_vector(15 downto 0) := x"0238";
		write_magic : std_logic_vector(15 downto 0) := x"0237";
		simulation_mode : std_logic := '0'
	);
	Port (
		MCLK1 : in  std_logic;
		RS_TX : out std_logic;
		RS_RX : in std_logic;

		XE1, E2A, XE3 : out std_logic;
		XGA, XZCKE, ADVA, XLBO, ZZA, XFT : out std_logic;
		XWA : out std_logic;
		XZBE : out std_logic_vector(3 downto 0);
		ZD : inout std_logic_vector(31 downto 0);
		ZA : out std_logic_vector(19 downto 0);
		ZCLKMA : out std_logic_vector(1 downto 0)
	 );
end StarlightCore3;

ARCHITECTURE struct OF StarlightCore3 IS
	component clockacc
	port (
	CLKIN_IN        : in    std_logic; 
	RST_IN          : in    std_logic; 
	CLKFX_OUT       : out   std_logic; 
	CLKIN_IBUFG_OUT : out   std_logic; 
	CLK0_OUT        : out   std_logic; 
	LOCKED_OUT      : out   std_logic);
	end component;

	component RS232C is
	generic (
		read_magic : std_logic_vector(15 downto 0) := x"0238";
		write_magic : std_logic_vector(15 downto 0) := x"0237"
	);
	Port (
		CLK : in  std_logic;
		RS_TX : out std_logic;
		RS_RX : in std_logic;

		-- FPGA -> PC
		write_busy : out std_logic;
		write_enable : in std_logic;
		write_data : in std_logic_vector(7 downto 0);
		
		-- PC -> FPGA
		read_empty : out std_logic;
		read_pop : in std_logic;
		read_data : out std_logic_vector(7 downto 0)
	 );
	end component;

	component MemoryController is
	Port (
		CLK : in  std_logic;
		
		-- common
		address : in std_logic_vector(19 downto 0);

		write_enable : in std_logic;
		write_data : in std_logic_vector(31 downto 0);

		read_enable : in std_logic;
		read_data : out std_logic_vector(31 downto 0);
		read_fetched : out std_logic;

		XWA : out std_logic;
		ZD : inout std_logic_vector(31 downto 0);
		ZA : out std_logic_vector(19 downto 0)
	 );
	end component;

	component Decoder is
	Port (
		clk : in std_logic;
		dec_in : in decoder_in_t;
		dec_out : out decoder_out_t
	);
	end component;

	component Arbiter is
	Port (
		clk : in std_logic;
		unit_in : in arbiter_in_t;
		unit_out : out arbiter_out_t
	);
	end component;
	
	component ALUUnit is
	Port (
		clk : in std_logic;
		unit_in : in afu_unit_in_t;
		unit_out : out afu_unit_out_t
	);
	end component;

	component MemUnit is
	Port (
		clk : in std_logic;
		unit_in : in mem_unit_in_t;
		unit_out : out mem_unit_out_t
	);
	end component;

	component FAddUnit is
	Port (
		clk : in std_logic;
		unit_in : in afu_unit_in_t;
		unit_out : out afu_unit_out_t
	);
	end component;

	component OutUnit is
	Port (
		clk : in std_logic;
		unit_in : in out_unit_in_t;
		unit_out : out out_unit_out_t
	);
	end component;
	
-- I/O related variables
	signal wbusy, wenable, rempty, rpop, rpop_starter : std_logic := '0';
	signal wdata, rdata : std_logic_vector(7 downto 0);
	
-- Program Loader
	signal rd_mode : std_logic_vector(2 downto 0) := (others => '0');
	signal program_size : std_logic_vector(23 downto 0) := (others => '0');
	signal program_read_pos : std_logic_vector(23 downto 0) := (others => '0');
	signal program_code : std_logic_vector(23 downto 0) := (others => '0');

-- Memory Controller
	signal mc_addr : std_logic_vector(19 downto 0) := x"00000";
	signal mc_write_enable, mc_read_enable, mc_read_fetched : std_logic := '0';
	signal mc_write_data, mc_read_data : value_t;
	
-- CPU variables
	signal cpu_awake_pre2, cpu_running, decoder_enable, decoder_stall : std_logic := '0';
	signal cpu_awake_pre1 : std_logic := simulation_mode;
	signal pcounter : std_logic_vector(19 downto 0) := x"00000";
	signal decoder_pc : std_logic_vector(19 downto 0) := x"00000";
	signal instruction : std_logic_vector(31 downto 0) := x"00000000";
	
	type program_memory_t is array(0 to 32767) of std_logic_vector(31 downto 0);
	
	impure function set_initial_sub (program_file : in string) return program_memory_t is
	type BIN is file of character;
	file RFile : BIN open read_mode is program_file;
	variable pmem : program_memory_t := (others => x"00000000");
	variable loc : integer := 0;
	variable read_char : character;
	variable word : std_logic_vector(31 downto 0);
	begin
		-- ignore the header
		read(RFile, read_char);
		read(RFile, read_char);
		read(RFile, read_char);
		read(RFile, read_char);
		
		loc := 0;
		while (endfile(RFile) = FALSE) loop
			read(RFile, read_char);
			word( 7 downto  0) := conv_std_logic_vector(character'pos(read_char), 8);
			read(RFile, read_char);
			word(15 downto  8) := conv_std_logic_vector(character'pos(read_char), 8);
			read(RFile, read_char);
			word(23 downto 16) := conv_std_logic_vector(character'pos(read_char), 8);
			read(RFile, read_char);
			word(31 downto 24) := conv_std_logic_vector(character'pos(read_char), 8);
			
			pmem(loc) := word;
			loc := loc + 1;
		end loop;
		return pmem;
	end function;
	impure function set_initial	(RamFileName : in string) return program_memory_t is
	variable pmem : program_memory_t := (others => x"00000000");
	begin
		if simulation_mode = '1' then
			pmem := set_initial_sub(RamFileName);
		end if;
		return pmem;
	end function;

	signal program_memory : program_memory_t := set_initial("program.bin");

	signal acc_rst, acc_clock, acc_dummy1, acc_dummy2, acc_dummy3 : std_logic := '0';
	
	signal CDB : cdb_t;
	
	signal decoder_in : decoder_in_t;
	signal decoder_out : decoder_out_t;
	signal arbiter_in : arbiter_in_t;
	signal arbiter_out : arbiter_out_t;
	signal alu_in, fadd_in : afu_unit_in_t;
	signal alu_out, fadd_out : afu_unit_out_t;
	signal mem_in : mem_unit_in_t;
	signal mem_out : mem_unit_out_t;
	signal ounit_in : out_unit_in_t;
	signal ounit_out : out_unit_out_t;
	
BEGIN
--	acc: clockacc port map (
--		CLKIN_IN => MCLK1,
--		RST_IN => acc_rst,
--		CLKFX_OUT => acc_clock,
--		CLKIN_IBUFG_OUT => acc_dummy1,
--		CLK0_OUT => acc_dummy2,
--		LOCKED_OUT => acc_dummy3
--	);
	io_unit : RS232C
		generic map (
		read_magic => read_magic,
		write_magic => write_magic
		)
		PORT MAP (
		CLK => MCLK1,
		RS_TX => RS_TX,
		RS_RX => RS_RX,
		write_busy => wbusy,
		write_enable => wenable,
		write_data => wdata,
		read_empty => rempty,
		read_pop => rpop,
		read_data => rdata);

	mem_unit : MemoryController 
		port map (
		CLK => MCLK1,
		
		address => mc_addr,
		write_enable => mc_write_enable,
		write_data => mc_write_data,
		read_enable => mc_read_enable,
		read_data => mc_read_data,
		read_fetched => mc_read_fetched,

		XWA => XWA,
		ZD => ZD,
		ZA => ZA
		);

	dec : Decoder
		port map (
		clk => MCLK1,
		dec_in => decoder_in,
		dec_out => decoder_out
		);

	arb : Arbiter
		port map (
		clk => MCLK1,
		unit_in => arbiter_in,
		unit_out => arbiter_out
		);

	alu : ALUUnit
		port map (
		clk => MCLK1,
		unit_in => alu_in,
		unit_out => alu_out
		);

	fadd : FAddUnit
		port map (
		clk => MCLK1,
		unit_in => fadd_in,
		unit_out => fadd_out
		);

	mem : MemUnit
		port map (
		clk => MCLK1,
		unit_in => mem_in,
		unit_out => mem_out
		);

	ounit : OutUnit
		port map (
		clk => MCLK1,
		unit_in => ounit_in,
		unit_out => ounit_out
		);

	XE1 <= '0';
	E2A <= '1';
	XE3 <= '0';
	XGA <= '0';
	XZCKE <= '0';
	ADVA <= '0';
	XLBO <= '1';
	ZZA <= '0';
	XFT <= '1';
	XZBE <= "0000";
	ZCLKMA(0) <= MCLK1;
	ZCLKMA(1) <= MCLK1;
	
	decoder_in.inst_enable <= decoder_enable;
	decoder_in.instruction <= instruction;
	decoder_in.pc <= decoder_pc;
	decoder_in.cdb <= CDB;
	decoder_in.alu_acceptable <= alu_out.acceptable;
	decoder_in.fadd_acceptable <= fadd_out.acceptable;
	decoder_in.ounit_acceptable <= ounit_out.acceptable;
	decoder_in.mem_acceptable <= mem_out.acceptable;
	
	alu_in.input <= decoder_out.alu_out;
	alu_in.accepted <= arbiter_out.alu_accepted;
	alu_in.cdb <= CDB;
	
	fadd_in.input <= decoder_out.fadd_out;
	fadd_in.accepted <= arbiter_out.fadd_accepted;
	fadd_in.cdb <= CDB;

	mem_in.input <= decoder_out.mem_out;
	mem_in.accepted <= arbiter_out.mem_accepted;
	mem_in.cdb <= cdb;
	mem_in.read_data <= mc_read_data;
	mem_in.read_fetched <= mc_read_fetched;

	mc_addr <= mem_out.address;
	mc_write_enable <= mem_out.write_enable;
	mc_read_enable <= mem_out.read_enable;
	mc_write_data <= mem_out.write_data;
	
	ounit_in.input <= decoder_out.ounit_out;
	ounit_in.cdb <= CDB;
	ounit_in.write_busy <= wbusy;
	
	wenable <= ounit_out.write_enable;
	wdata <= ounit_out.write_data;
	
	arbiter_in.alu_in <= alu_out.output;
	arbiter_in.fadd_in <= fadd_out.output;
	arbiter_in.mem_in <= mem_out.output;
	
	CDB <= arbiter_out.cdb;
	
	rpop <= rpop_starter;
	
	process(MCLK1)
	begin
		if rising_edge(MCLK1) then
			-- CPU starter
			if cpu_running = '0' then
				if rempty = '0' and rpop_starter = '0' and cpu_awake_pre1 = '0' then
					if rd_mode <= "010" then
						rd_mode <= rd_mode + 1;
						program_size <= rdata & program_size(23 downto 8);
					elsif rd_mode = "011" then
						rd_mode <= rd_mode + 1;
					elsif rd_mode = "100" then
						program_code(7 downto 0) <= rdata;
						rd_mode <= "101";
					elsif rd_mode = "101" then
						program_code(15 downto 8) <= rdata;
						rd_mode <= "110";
					elsif rd_mode = "110" then
						program_code(23 downto 16) <= rdata;
						rd_mode <= "111";
					elsif rd_mode = "111" then
						rd_mode <= "100";
						
						program_read_pos <= program_read_pos + 1;
						
						if program_read_pos(23 downto 15) = "000000000" then
							program_memory(conv_integer(program_read_pos(14 downto 0))) <= rdata & program_code(23 downto 0);
						end if;
						
						if program_read_pos + 1 = program_size then
							cpu_awake_pre1 <= '1';
							pcounter <= x"00000";
						end if;
					end if;
					
					rpop_starter <= '1';
				else
					-- mem_reading <= '1';
					rpop_starter <= '0';
				end if;
			end if;
					
			if cpu_awake_pre1 = '1' then
				cpu_awake_pre1 <= '0';
				cpu_awake_pre2 <= '1';
			end if;
			if cpu_awake_pre2 = '1' then
				cpu_awake_pre2 <= '0';
				cpu_running <= '1';
			end if;

		
			-- instruction fetch stage
			if cpu_running = '1' and decoder_out.is_halt = '0' then
				decoder_enable <= '1';

				if decoder_out.is_stall = '0' then
					pcounter <= pcounter + x"00001";
					instruction <= program_memory(conv_integer(pcounter(14 downto 0)));
					decoder_pc <= pcounter;
				end if;
			else
				decoder_enable <= '0';
			end if;

			if decoder_out.is_halt = '1' then
				cpu_running <= '0';
				rd_mode <= "000";
				program_read_pos <= x"000000";
				pcounter <= x"00000";
			end if;
		end if;
	end process;
END;
