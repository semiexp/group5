library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

use work.common.all;

entity Arbiter is
	Port (
		clk : in std_logic;
		unit_in : in arbiter_in_t;
		unit_out : out arbiter_out_t
	 );
end Arbiter;

architecture rtl of Arbiter is
	type reg_type is record
		enable0, enable1 : std_logic;
		destination0, destination1 : rob_index_t;
		data0, data1 : value_t;
	end record;

	constant rzero : reg_type := (
		enable0 => '0',
		enable1 => '0',
		destination0 => "0000",
		destination1 => "0000",
		data0 => x"00000000",
		data1 => x"00000000"
	);

	signal rin : reg_type := rzero;
begin	
	process (unit_in)
	variable v : reg_type;
	begin
		v := rzero;
		
		if unit_in.mem_in.enable = '1' then
			v.enable0 := '1';
			v.destination0 := unit_in.mem_in.destination;
			v.data0 := unit_in.mem_in.data;
			
			unit_out.mem_accepted <= '1';
		else
			unit_out.mem_accepted <= '0';
		end if;
		
		if unit_in.alu_in.enable = '1' then
			if v.enable0 = '0' then
				v.enable0 := '1';
				v.destination0 := unit_in.alu_in.destination;
				v.data0 := unit_in.alu_in.data;
			else
				v.enable1 := '1';
				v.destination1 := unit_in.alu_in.destination;
				v.data1 := unit_in.alu_in.data;
			end if;
			unit_out.alu_accepted <= '1';
		else
			unit_out.alu_accepted <= '0';
		end if;
		
		if unit_in.fadd_in.enable = '1' then
			if v.enable0 = '0' then
				v.enable0 := '1';
				v.destination0 := unit_in.fadd_in.destination;
				v.data0 := unit_in.fadd_in.data;
				unit_out.fadd_accepted <= '1';
			elsif v.enable1 = '0' then
				v.enable1 := '1';
				v.destination1 := unit_in.fadd_in.destination;
				v.data1 := unit_in.fadd_in.data;
				unit_out.fadd_accepted <= '1';
			else
				unit_out.fadd_accepted <= '0';
			end if;
		else
			unit_out.fadd_accepted <= '0';
		end if;
		
		rin <= v;
	end process;
	
	process (clk)
	begin
		if rising_edge(clk) then
			unit_out.cdb.enable(0) <= rin.enable0;
			unit_out.cdb.destination(0) <= rin.destination0;
			unit_out.cdb.data(0) <= rin.data0;

			unit_out.cdb.enable(1) <= rin.enable1;
			unit_out.cdb.destination(1) <= rin.destination1;
			unit_out.cdb.data(1) <= rin.data1;
		end if;
	end process;
end;
