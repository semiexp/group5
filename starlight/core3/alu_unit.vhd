library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

use work.common.all;

entity ALUUnit is
	Port (
		clk : in std_logic;
		unit_in : in afu_unit_in_t;
		unit_out : out afu_unit_out_t
	 );
end ALUUnit;

architecture rtl of ALUUnit is
	type values_t is array(0 to 3) of vtvalue_t;
	type dest_t is array(0 to 3) of rob_index_t;
	subtype state_t is std_logic_vector(1 downto 0);
	type all_state_t is array(0 to 3) of state_t;
	type all_ops_t is array(0 to 3) of unit_op_t;
	subtype rs_index is integer range 0 to 3;
	
	constant rs_empty : state_t := "00";
	constant rs_waiting : state_t := "01";
	constant rs_completed : state_t := "10";
	
	type reg_type is record
		-- reservation station
		value1, value2 : values_t;
		dest : dest_t;
		ops : all_ops_t;
		status : all_state_t;
	end record;
	
	constant rzero : reg_type := (
		value1 => (others => '0' & x"00000000"),
		value2 => (others => '0' & x"00000000"),
		dest => (others => "0000"),
		ops => (others => "0000"),
		status => (others => rs_empty)
	);
	
	signal r, rin : reg_type := rzero;

begin
	process (r, unit_in)
		variable v : reg_type;
		variable n_value1, n_value2 : vtvalue_t;
		variable n_status : state_t;
		variable n_storage : rs_index;
		variable empty_loc : integer;
		
		variable output_enable, output_is_new : std_logic;
		variable output_index : rs_index;
		variable output_value : value_t;
		variable output_dest : rob_index_t;
		
		variable vtvalue_tmp : vtvalue_t;
		
		variable acceptable : std_logic;
	begin
		v := r;
		
		for i in 0 to 3 loop
			v.value1(i) := retrive_cdb(unit_in.cdb, v.value1(i));
			v.value2(i) := retrive_cdb(unit_in.cdb, v.value2(i));
		end loop;
		n_value1 := retrive_cdb(unit_in.cdb, unit_in.input.value1);
		n_value2 := retrive_cdb(unit_in.cdb, unit_in.input.value2);
		
		if unit_in.input.enable = '1' then
			n_status := rs_waiting;
		else
			n_status := rs_empty;
		end if;
		
		n_storage := 0;
		empty_loc := 0;
		for i in 0 to 3 loop
			if v.status(i) = rs_waiting and is_real(v.value1(i)) = '1' and is_real(v.value2(i)) = '1' then
				v.status(i) := rs_completed;
				
				v.value1(i) := '0' & perform_ALU(get_value(v.value1(i)), get_value(v.value2(i)), v.ops(i));
			end if;
			
			if v.status(i) = rs_empty then
				n_storage := i;
				empty_loc := empty_loc + 1;
			end if;
		end loop;
		
		if unit_in.input.enable = '0' then
			empty_loc := empty_loc + 1;
		end if;
		
		-- acceptance should be determined regardless of the output of the arbiter in order to avoid timing problems
		if empty_loc >= 2 then
			unit_out.acceptable <= '1';
		else
			unit_out.acceptable <= '0';
		end if;
		
		if n_status = rs_waiting and is_real(n_value1) = '1' and is_real(n_value2) = '1' then
			n_status := rs_completed;
			n_value1 := '0' & perform_ALU(get_value(n_value1), get_value(n_value2), unit_in.input.operation);
		end if;
		
		output_enable := '0';
		output_index := 0;
		output_is_new := '0';
		
		for i in 0 to 3 loop
			if v.status(i) = rs_completed then
				output_enable := '1';
				output_index := i;
				exit;
			end if;
		end loop;
		
		if output_enable = '0' and n_status = rs_completed then
			output_enable := '1';
			output_is_new := '1';
		end if;
		
		if output_enable = '1' then
			if output_is_new = '0' then
				unit_out.output.destination <= v.dest(output_index);
				unit_out.output.data <= get_value(v.value1(output_index));
				unit_out.output.enable <= '1';
			else
				unit_out.output.destination <= unit_in.input.destination;
				unit_out.output.data <= get_value(n_value1);
				unit_out.output.enable <= '1';
			end if;
		else
			unit_out.output.destination <= "0000";
			unit_out.output.data <= x"00000000";
			unit_out.output.enable <= '0';
		end if;
		
		if unit_in.accepted = '1' then
			if output_is_new = '0' then
				v.status(output_index) := rs_empty;
			end if;
		end if;
		
		if (output_is_new = '0' or unit_in.accepted = '0') and unit_in.input.enable = '1' then
			v.status(n_storage) := n_status;
			v.value1(n_storage) := n_value1;
			v.value2(n_storage) := n_value2;
			v.dest(n_storage) := unit_in.input.destination;
			v.ops(n_storage) := unit_in.input.operation;
		end if;
		
		rin <= v;
	end process;
	
	process (clk)
	begin
		if rising_edge(clk) then
			r <= rin;
		end if;
	end process;
end;
