library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

use work.common.all;

entity Decoder is
	Port (
		clk : in std_logic;
		dec_in : in decoder_in_t;
		dec_out : out decoder_out_t
	 );
end Decoder;

architecture rtl of Decoder is
	type reg_type is record
		-- for internal signals (register file, ROB)
		reservation : std_logic;
		rsv_reg  : register_idx_t;
		rsv_dest : rob_index_t;
		
		writeback : std_logic;
		reg_writeback_idx : rob_index_t;
		reg_writeback_target : register_idx_t;
		reg_writeback_value : value_t;
		reg_writeback_upd_vt : std_logic;
		
		-- for output
		op_alu, op_out, op_fadd, op_mem: std_logic;
		unit_op : unit_op_t;
		value1, value2, value3 : vtvalue_t;
		
		stall : std_logic;
		is_halt : std_logic;
		
		rtop, rend : rob_index_t;
	end record;
	constant rzero : reg_type := (
		reservation => '0',
		rsv_reg => "000000",
		rsv_dest => "0000",
		writeback => '0',
		reg_writeback_idx => "0000",
		reg_writeback_target => "000000",
		reg_writeback_value => x"00000000",
		reg_writeback_upd_vt => '0',
		op_alu => '0',
		op_out => '0',
		op_fadd => '0',
		op_mem => '0',
		unit_op => "0000",
		value1 => (others => '0'),
		value2 => (others => '0'),
		value3 => (others => '0'),
		stall => '0',
		is_halt => '0',
		rtop => "0000",
		rend => "0000"
	);
	
	signal r, rin : reg_type := rzero;
	signal reg : register_t := (others => x"00000000");
	signal reg_rsv : register_rsv_t := (others => "0000");
	signal reg_vt : register_is_virtual_t := (others => '0');
	signal rob : rob_value_t := (others => '0' & x"00000000");
	signal rob_target : rob_target_t := (others => "000000");
	
begin
	process (r, dec_in, reg, reg_rsv, reg_vt, rob, rob_target)
		variable v : reg_type;
		
		variable opcode, reg1, reg2, reg3 : std_logic_vector(5 downto 0);
		variable value1, value2, value3 : vtvalue_t;
		variable sgn : std_logic;
	begin
		v := r;
		
		opcode := dec_in.instruction(31 downto 26);
		reg1 := dec_in.instruction(25 downto 20);
		reg2 := dec_in.instruction(19 downto 14);
		reg3 := dec_in.instruction(13 downto 8);
		value1 := '0' & x"00000000";
		value2 := '0' & x"00000000";
		value3 := '0' & x"00000000";
		v.rsv_reg := "000000";
		v.rsv_dest := "0000";
		v.stall := '0';
		v.op_alu := '0';
		v.op_out := '0';
		v.op_fadd := '0';
		v.op_mem := '0';
		v.unit_op := "0000";
		
		-- decode instruction
		if opcode = "000000" then
			-- add
			v.rsv_reg := reg1;
			value1 := "100" & x"000000" & reg2;
			value2 := "100" & x"000000" & reg3;
			v.op_alu := '1';
			v.unit_op := alu_op_add;
		elsif opcode = "001000" then
			-- movi
			v.rsv_reg := reg1;
			value1 := '0' & x"00000000";
			
			sgn := dec_in.instruction(19);
			value2 := "0" &
				sgn & sgn & sgn & sgn & sgn & sgn &
				sgn & sgn & sgn & sgn & sgn & sgn &
				dec_in.instruction(19 downto 0);
			v.op_alu := '1';
			v.unit_op := alu_op_add;
		elsif opcode = "001011" then
			-- movhiz
			v.rsv_reg := reg1;
			value1 := '0' & x"00000000";
			value2 := '0' & dec_in.instruction(19 downto 4) & x"0000";
			v.op_alu := '1';
			v.unit_op := alu_op_add;
		elsif opcode = "000100" then
			-- lshift
			v.rsv_reg := reg1;
			value1 := "100" & x"000000" & reg2;
			value2 := "100" & x"000000" & reg3;
			v.op_alu := '1';
			v.unit_op := alu_op_lshift;
		elsif opcode = "000110" then
			-- rshift
			v.rsv_reg := reg1;
			value1 := "100" & x"000000" & reg2;
			value2 := "100" & x"000000" & reg3;
			v.op_alu := '1';
			v.unit_op := alu_op_rshift;
		elsif opcode = "000101" then
			-- lshifti
			v.rsv_reg := reg1;
			value1 := "100" & x"000000" & reg2;
			
			sgn := dec_in.instruction(13);
			value2 := "0" &
				sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn &
				sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn &
				dec_in.instruction(13 downto 8);
			v.op_alu := '1';
			v.unit_op := alu_op_lshift;
		elsif opcode = "000111" then
			-- rshifti
			v.rsv_reg := reg1;
			value1 := "100" & x"000000" & reg2;
			
			sgn := dec_in.instruction(13);
			value2 := "0" &
				sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn &
				sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn &
				dec_in.instruction(13 downto 8);
			v.op_alu := '1';
			v.unit_op := alu_op_rshift;
		end if;
		
		if opcode = "111100" then
			-- out
			value1 := "100" & x"000000" & reg1;
			v.op_out := '1';
		end if;
		
		if opcode = "010000" then
			-- fadd
			v.rsv_reg := reg1;
			value1 := "100" & x"000000" & reg2;
			value2 := "100" & x"000000" & reg3;
			v.op_fadd := '1';
		end if;
		
		if opcode = "100000" then
			-- ld
			v.op_mem := '1';
			v.rsv_reg := reg1;
			value1 := '0' & x"00000000";
			value2 := "100" & x"000000" & reg2;
			sgn := dec_in.instruction(13);
			value3 := '0' &
				sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn &
				sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn &
				dec_in.instruction(13 downto 0);
			v.unit_op := "0000";
		end if;

		if opcode = "100100" then
			-- sto
			v.op_mem := '1';
			v.rsv_reg := "000000";
			value1 := "100" & x"000000" & reg1;
			value2 := "100" & x"000000" & reg2;
			sgn := dec_in.instruction(13);
			value3 := '0' &
				sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn &
				sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn &
				dec_in.instruction(13 downto 0);
			v.unit_op := "0001";
		end if;

		if opcode = "111111" and dec_in.inst_enable = '1' then
			v.is_halt := '1';
		else
			v.is_halt := '0';
		end if;
		
		-- translate virtual values (if possible)
		if value1(32) = '1' then
			if reg_vt(conv_integer(value1(5 downto 0))) = '1' then
				value1 := '1' & x"0000000" & reg_rsv(conv_integer(value1(5 downto 0)));
				
				if is_real(rob(conv_integer(value1(3 downto 0)))) = '1' then
					value1 := rob(conv_integer(value1(3 downto 0)));
				else
					value1 := retrive_cdb(dec_in.cdb, value1);
				end if;
			else
				value1 := '0' & reg(conv_integer(value1(5 downto 0)));
			end if;
		end if;

		if value2(32) = '1' then
			if reg_vt(conv_integer(value2(5 downto 0))) = '1' then
				value2 := '1' & x"0000000" & reg_rsv(conv_integer(value2(5 downto 0)));

				if is_real(rob(conv_integer(value2(3 downto 0)))) = '1' then
					value2 := rob(conv_integer(value2(3 downto 0)));
				else
					value2 := retrive_cdb(dec_in.cdb, value2);
				end if;
			else
				value2 := '0' & reg(conv_integer(value2(5 downto 0)));
			end if;
		end if;
		
		-- check stall for !!! each operation type !!!
		if v.op_alu = '1' and dec_in.alu_acceptable = '0' then
			v.stall := '1';
		end if;
		if v.op_fadd = '1' and dec_in.fadd_acceptable = '0' then
			v.stall := '1';
		end if;
		if v.op_out = '1' and dec_in.ounit_acceptable = '0' then
			v.stall := '1';
		end if;
		if v.op_mem = '1' and dec_in.mem_acceptable = '0' then
			v.stall := '1';
		end if;
		
		-- check stall caused by full ROB
		if (v.rend + "0001") = v.rtop then
			v.stall := '1';
		end if;
		
		-- register write back
		v.writeback := '0';
		if v.rtop /= v.rend then
			if is_real(rob(conv_integer(v.rtop))) = '1' then
				v.writeback := '1';
				v.reg_writeback_idx := v.rtop;
				v.reg_writeback_target := rob_target(conv_integer(v.rtop));
				v.reg_writeback_value := get_value(rob(conv_integer(v.rtop)));
				
				if reg_rsv(conv_integer(v.reg_writeback_target)) = v.rtop and v.rsv_reg /= v.reg_writeback_target then
					v.reg_writeback_upd_vt := '1';
				else
					v.reg_writeback_upd_vt := '0';
				end if;
				
				v.rtop := v.rtop + "0001";
			end if;
		end if;
		
		if dec_in.inst_enable = '0' or v.stall = '1' then
			v.op_alu := '0';
			v.op_fadd := '0';
			v.op_out := '0';
			v.op_mem := '0';
		end if;
				
		v.value1 := value1;
		v.value2 := value2;
		v.value3 := value3;
		-- for all kind of instructions which require register reservation
		v.reservation := (v.op_alu or v.op_fadd or (v.op_mem and (not v.unit_op(0)))) and (not v.stall);
		if v.reservation = '1' then
			v.rsv_dest := v.rend;
			v.rend := v.rend + "0001";
		end if;
		
		rin <= v;
		
		dec_out.alu_out <= (
			enable => r.op_alu, value1 => r.value1, value2 => r.value2,
			destination => r.rsv_dest, operation => r.unit_op
		);
		dec_out.fadd_out <= (
			enable => r.op_fadd, value1 => r.value1, value2 => r.value2,
			destination => r.rsv_dest, operation => r.unit_op
		);
		dec_out.ounit_out <= (
			enable => r.op_out, value1 => r.value1
		);
		dec_out.mem_out <= (
			enable => r.op_mem, addr => r.value2, wvalue => r.value1,
			ofs => r.value3, destination => r.rsv_dest, is_write => r.unit_op(0)
		);
		dec_out.is_halt <= v.is_halt;
		dec_out.is_stall <= v.stall;
	end process;
	
	process (clk)
		variable cdb : cdb_t;
		variable target_reg : register_idx_t;
	begin
		if rising_edge(clk) then
			r <= rin;
			
			cdb := dec_in.cdb;
			
			if rin.reservation = '1' then
				-- write to ROB (issue an instruction)
				rob(conv_integer(rin.rsv_dest)) <= '1' & x"00000000";
				rob_target(conv_integer(rin.rsv_dest)) <= rin.rsv_reg;
				
				-- reserve register
				reg_vt(conv_integer(rin.rsv_reg)) <= '1';
				reg_rsv(conv_integer(rin.rsv_reg)) <= rin.rsv_dest;
			end if;
			
			if rin.writeback = '1' then
				-- write back to register file
				reg(conv_integer(rin.reg_writeback_target)) <= rin.reg_writeback_value;
				if rin.reg_writeback_upd_vt = '1' then
					reg_vt(conv_integer(rin.reg_writeback_target)) <= '0';
				end if;
			end if;
			
			-- write to ROB (completed instructions)
			if cdb.enable(0) = '1' then
				rob(conv_integer(cdb.destination(0))) <= '0' & cdb.data(0);
			end if;
			if cdb.enable(1) = '1' then
				rob(conv_integer(cdb.destination(1))) <= '0' & cdb.data(1);
			end if;
		end if;
	end process;
end;
