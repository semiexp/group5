library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity fifo8 is
	Port (
		clk : in std_logic;
		wr_en : in std_logic;
		rd_en : in std_logic;
		din : in std_logic_vector(7 downto 0);
		dout : out std_logic_vector(7 downto 0);
		full : out std_logic;
		empty : out std_logic
	 );
end fifo8;

architecture struct of fifo8 is
	type buf_t is array(0 to 15) of std_logic_vector(7 downto 0);
	
	signal buf : buf_t := (others => x"00");
	signal last : integer := 0;
begin
	empty <= '1' when last = 0 else '0';
	full  <= '1' when last = 16 else '0';
	dout <= buf(0);

	process(CLK)
	variable last2 : integer := 0;
	variable buf2 : buf_t;
	begin
		if rising_edge(CLK) then
			if rd_en = '1' then
				for i in 0 to 14 loop
					buf2(i) := buf2(i + 1);
				end loop;
				buf2(15) := x"00";
			else
				buf2 := buf;
			end if;
			
			last2 := last;
			if rd_en = '1' then
				last2 := last2 - 1;
			end if;

			if wr_en = '1' then
				buf2(last2) := din;
				last2 := last2 + 1;
			end if;
			
			last <= last2;
			buf <= buf2;
		end if;
	end process;
end;
