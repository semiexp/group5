library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

--library UNISIM;
--use UNISIM.VComponents.all;

entity StarlightCore is
	generic (
		read_magic : std_logic_vector(15 downto 0) := x"0238";
		write_magic : std_logic_vector(15 downto 0) := x"0237"
	);
	Port (
		MCLK1 : in  std_logic;
		RS_TX : out std_logic;
		RS_RX : in std_logic;

		XE1, E2A, XE3 : out std_logic;
		XGA, XZCKE, ADVA, XLBO, ZZA, XFT : out std_logic;
		XWA : out std_logic;
		XZBE : out std_logic_vector(3 downto 0);
		ZD : inout std_logic_vector(31 downto 0);
		ZA : out std_logic_vector(19 downto 0);
		ZCLKMA : out std_logic_vector(1 downto 0)
	 );
end StarlightCore;

ARCHITECTURE struct OF StarlightCore IS
	component clockacc
	port (
	CLKIN_IN        : in    std_logic; 
	RST_IN          : in    std_logic; 
	CLKFX_OUT       : out   std_logic; 
	CLKIN_IBUFG_OUT : out   std_logic; 
	CLK0_OUT        : out   std_logic; 
	LOCKED_OUT      : out   std_logic);
	end component;

	component FAdd
	port (
	input1 : in std_logic_vector(31 downto 0);
	input2 : in std_logic_vector(31 downto 0);
	output : out std_logic_vector(31 downto 0);
	clk : in std_logic
	);
	end component;

	component FMul
	port (
	input1 : in std_logic_vector(31 downto 0);
	input2 : in std_logic_vector(31 downto 0);
	output : out std_logic_vector(31 downto 0);
	clk : in std_logic
	);
	end component;

	component Shifter
	Port (
	value : in std_logic_vector (31 downto 0);
	shift_w : in std_logic_vector (31 downto 0);
	output : out std_logic_vector (31 downto 0)
	);
	end component;
	
	component RS232C is
	generic (
		read_magic : std_logic_vector(15 downto 0) := x"0238";
		write_magic : std_logic_vector(15 downto 0) := x"0237"
	);
	Port (
		CLK : in  std_logic;
		RS_TX : out std_logic;
		RS_RX : in std_logic;

		-- FPGA -> PC
		write_busy : out std_logic;
		write_enable : in std_logic;
		write_data : in std_logic_vector(7 downto 0);
		
		-- PC -> FPGA
		read_empty : out std_logic;
		read_pop : in std_logic;
		read_data : out std_logic_vector(7 downto 0)
	 );
	end component;

	component MemoryController is
	Port (
		CLK : in  std_logic;
		
		address : in std_logic_vector(19 downto 0);

		write_enable : in std_logic;
		write_data : in std_logic_vector(31 downto 0);

		read_enable : in std_logic;
		read_data : out std_logic_vector(31 downto 0);
		read_fetched : out std_logic;

		XWA : out std_logic;
		ZD : inout std_logic_vector(31 downto 0);
		ZA : out std_logic_vector(19 downto 0)
	 );
	end component;

-- I/O related variables
	signal wbusy, wenable, rempty, rpop : std_logic := '0';
	signal wdata, rdata : std_logic_vector(7 downto 0);
	
	signal read_mode : std_logic_vector(2 downto 0) := (others => '0');
	signal program_size : std_logic_vector(23 downto 0) := (others => '0');
	signal program_read_pos : std_logic_vector(23 downto 0) := (others => '0');
	signal program_code : std_logic_vector(23 downto 0) := (others => '0');

-- Memory related variables
	signal mem_addr : std_logic_vector(19 downto 0) := (others => '0');
	signal mem_write_val, mem_write_val2, mem_write_val3 : std_logic_vector(31 downto 0) := (others => '0');
	signal mem_reading, mem_reading2, mem_reading3 : std_logic := '1';
	
-- CPU variables
	signal cpu_awake_pre1, cpu_awake_pre2 : std_logic := '0';
	signal cpu_state : std_logic_vector(3 downto 0) := "1111";
	-- 0000: fetch instruction
	-- 0001: [start fetching]
	-- 0010: [waiting]
	-- 0011: load instruction finish
	-- 0100: set operands
	-- 0101: perform instruction
	-- 0110: waiting
	-- 0111: waiting
	-- 1000: write operand
	-- 1111: idle state
	signal pcounter : std_logic_vector(19 downto 0) := x"00000";
	signal instruction : std_logic_vector(31 downto 0) := x"00000000";
	signal operandA, operandB, operandC : std_logic_vector(31 downto 0) := x"00000000";
	signal dest_register : std_logic_vector(5 downto 0) := "000000";
	
	type reg32_64 is array(0 to 63) of std_logic_vector(31 downto 0);
	type program_memory_t is array(0 to 32767) of std_logic_vector(31 downto 0);
	
	signal program_memory : program_memory_t;
	signal regs : reg32_64 := (others => x"00000000");
	signal cond_zero, cond_pos, cond_neg : std_logic := '0';

	signal op_cmp, op_fcmp, op_add, op_sub, op_jmp, op_call, op_out, op_in, op_halt, op_shift, op_ld, op_sto, op_fadd, op_fmul : std_logic := '0';
	
	signal acc_rst, acc_clock, acc_dummy1, acc_dummy2, acc_dummy3 : std_logic := '0';
	
	signal fadd_in1, fadd_in2, fadd_out, fmul_in1, fmul_in2, fmul_out, shift_in, shift_in_w, shift_out: std_logic_vector(31 downto 0) := (others => '0');
	
	signal buf_wren, buf_rden, buf_full, buf_empty : std_logic := '0';
	signal buf_din, buf_dout : std_logic_vector(7 downto 0);
	
	signal mcu_we, mcu_re, mcu_rf : std_logic := '0';
	signal mcu_wdata, mcu_rd : std_logic_vector(31 downto 0) := (others => '0');
	signal mcu_addr : std_logic_vector(19 downto 0) := x"00000";
	
BEGIN
--	acc: clockacc port map (
--		CLKIN_IN => MCLK1,
--		RST_IN => acc_rst,
--		CLKFX_OUT => acc_clock,
--		CLKIN_IBUFG_OUT => acc_dummy1,
--		CLK0_OUT => acc_dummy2,
--		LOCKED_OUT => acc_dummy3
--	);
	f_adder : FAdd port map (
		input1 => fadd_in1,
		input2 => fadd_in2,
		output => fadd_out,
		clk => MCLK1
	);
	f_multiplier : FMul port map (
		input1 => fmul_in1,
		input2 => fmul_in2,
		output => fmul_out,
		clk => MCLK1
	);
	i_shifter : Shifter port map (
		value => shift_in,
		shift_w => shift_in_w,
		output => shift_out
	);
	io_unit : RS232C
		generic map (
		read_magic => read_magic,
		write_magic => write_magic
		)
		PORT MAP (
		CLK => MCLK1,
		RS_TX => RS_TX,
		RS_RX => RS_RX,
		write_busy => wbusy,
		write_enable => wenable,
		write_data => wdata,
		read_empty => rempty,
		read_pop => rpop,
		read_data => rdata);
	mcu : MemoryController port map (
		CLK => MCLK1,
		address => mcu_addr,
		write_enable => mcu_we,
		write_data => mcu_wdata,
		read_enable => mcu_re,
		read_data => mcu_rd,
		read_fetched => mcu_rf,
		XWA => XWA,
		ZD => ZD,
		ZA => ZA
		 );

	XE1 <= '0';
	E2A <= '1';
	XE3 <= '0';
	XGA <= '0';
	XZCKE <= '0';
	ADVA <= '0';
	XLBO <= '1';
	ZZA <= '0';
	XFT <= '1';
	XZBE <= "0000";
	--ZA <= mem_addr;
	--XWA <= mem_reading;
	ZCLKMA(0) <= MCLK1;
	ZCLKMA(1) <= MCLK1;
	
	process(cpu_state, op_ld, op_sto, operandA, operandB, operandC) 
	begin
		--mcu_wd <= x"9876543f"; --operandC;
		if cpu_state = "1000" and op_ld = '1' then
			mcu_re <= '1';
			mcu_we <= '0';
			mcu_addr <= operandA(19 downto 0) + operandB(19 downto 0);
			--mcu_wd <= x"00000000";
		elsif cpu_state = "1000" and op_sto = '1' then
			mcu_re <= '0';
			mcu_we <= '1';
			mcu_addr <= operandA(19 downto 0) + operandB(19 downto 0);
			--mcu_wd <= operandC;
		else
			mcu_re <= '0';
			mcu_we <= '0';
			mcu_addr <= x"00000";
			--mcu_wd <= x"00000000";
		end if;
	end process;
	
	process(MCLK1)
	variable opcode : std_logic_vector(5 downto 0) := "111111";
	variable op_reg1, op_reg2, op_reg3 : std_logic_vector(5 downto 0) := "000000";
	variable sgn : std_logic := '0';
	variable reg1_val : std_logic_vector(31 downto 0);
	variable cmp_sub : std_logic_vector(32 downto 0);
	variable fcmp_value : std_logic_vector(2 downto 0);
	
	begin
		if rising_edge(MCLK1) then
			-- memory controller
			mem_reading2 <= mem_reading;
			mem_write_val2 <= mem_write_val;
			mem_reading3 <= mem_reading2;
			mem_write_val3 <= mem_write_val2;
		
			if mem_reading2 = '1' then
			--	ZD <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
			else
			--	ZD <= mem_write_val2;
			end if;		
			
			-- CPU starter
			if cpu_state = "1111" then
				if rempty = '0' and rpop = '0' and cpu_awake_pre1 = '0' then
					if read_mode <= "010" then
						read_mode <= read_mode + 1;
						program_size <= rdata & program_size(23 downto 8);
					elsif read_mode = "011" then
						read_mode <= read_mode + 1;
					elsif read_mode = "100" then
						program_code(7 downto 0) <= rdata;
						read_mode <= "101";
					elsif read_mode = "101" then
						program_code(15 downto 8) <= rdata;
						read_mode <= "110";
					elsif read_mode = "110" then
						program_code(23 downto 16) <= rdata;
						read_mode <= "111";
					elsif read_mode = "111" then
						read_mode <= "100";
						
						-- temporarily disabled
						-- mem_addr <= program_read_pos(19 downto 0);
						program_read_pos <= program_read_pos + 1;
						-- mem_reading <= '0';
						-- mem_write_val <= rdata & program_code(23 downto 0);
						
						if program_read_pos(23 downto 15) = "000000000" then
							program_memory(conv_integer(program_read_pos(14 downto 0))) <= rdata & program_code(23 downto 0);
						end if;
						
						if program_read_pos + 1 = program_size then
							cpu_awake_pre1 <= '1';
							pcounter <= x"00000";
						end if;
					end if;
					
					rpop <= '1';
				else
					mem_reading <= '1';
					rpop <= '0';
				end if;
			end if;
					
			if cpu_awake_pre1 = '1' then
				cpu_awake_pre1 <= '0';
				cpu_awake_pre2 <= '1';
			end if;
			if cpu_awake_pre2 = '1' then
				cpu_awake_pre2 <= '0';
				cpu_state <= "0000";
			end if;

			if cpu_state = "0000" then
				if pcounter(19 downto 15) = "00000" then
					instruction <= program_memory(conv_integer(pcounter(14 downto 0)));
					cpu_state <= "0100";
				else
					mem_reading <= '1';
					mem_addr <= pcounter;
					cpu_state <= "0001";
				end if;
			elsif cpu_state = "0001" then
				cpu_state <= "0010";
			elsif cpu_state = "0010" then
				cpu_state <= "0011";
			elsif cpu_state = "0011" then
				instruction <= ZD;
				cpu_state <= "0100";
			elsif cpu_state = "0100" then
				-- set operation
				opcode := instruction(31 downto 26);
				op_reg1 := instruction(25 downto 20);
				op_reg2 := instruction(19 downto 14);
				op_reg3 := instruction(13 downto 8);
				
				if opcode = "001100" then
					op_cmp <= '1';

					operandA <= regs(conv_integer(op_reg1));
					operandB <= regs(conv_integer(op_reg2));
				elsif opcode = "001101" then
					op_cmp <= '1';

					operandA <= regs(conv_integer(op_reg1));
					sgn := instruction(19);
					operandB <= 
						sgn & sgn & sgn & sgn & sgn & sgn &
						sgn & sgn & sgn & sgn & sgn & sgn & 
						instruction(19 downto 0);
				else
					op_cmp <= '0';
				end if;
				
				if opcode = "011110" then
					op_fcmp <= '1';

					operandA <= regs(conv_integer(op_reg1));
					operandB <= regs(conv_integer(op_reg2));
				else
					op_fcmp <= '0';
				end if;
				
				if opcode = "000000" then
					-- add register
					op_add <= '1';
					
					dest_register <= op_reg1;
					operandA <= regs(conv_integer(op_reg2));
					operandB <= regs(conv_integer(op_reg3));
				elsif opcode = "000001" then
					-- add immediate
					op_add <= '1';
					
					dest_register <= op_reg1;
					operandA <= regs(conv_integer(op_reg2));
					
					sgn := instruction(13);
					operandB <= 
						sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn &
						sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn &
						instruction(13 downto 0);
				elsif opcode = "001000" then
					-- movi
					op_add <= '1';
					
					dest_register <= op_reg1;
					
					sgn := instruction(19);
					operandA <= 
						sgn & sgn & sgn & sgn & sgn & sgn &
						sgn & sgn & sgn & sgn & sgn & sgn & 
						instruction(19 downto 0);
					operandB <= x"00000000";
				elsif opcode = "001010" then
					-- movhi
					op_add <= '1';
					
					dest_register <= op_reg1;

					reg1_val := regs(conv_integer(op_reg1));
					operandA <= x"0000" & reg1_val(15 downto 0);
					operandB <= instruction(19 downto 4) & x"0000";
				elsif opcode = "001011" then
					-- movhiz
					op_add <= '1';
					
					dest_register <= op_reg1;

					operandA <= x"00000000";
					operandB <= instruction(19 downto 4) & x"0000";
				elsif opcode = "011111" then
					-- fabs
					op_add <= '1';
					
					dest_register <= op_reg1;

					reg1_val := regs(conv_integer(op_reg2));
					operandA <= x"00000000";
					operandB <= "0" & reg1_val(30 downto 0);
				else
					op_add <= '0';
				end if;
				
				if opcode = "000010" then
					-- sub
					op_sub <= '1';

					dest_register <= op_reg1;
					operandA <= regs(conv_integer(op_reg2));
					operandB <= regs(conv_integer(op_reg3));
				else
					op_sub <= '0';
				end if;
				
				if opcode = "111100" then
					-- out
					op_out <= '1';
					dest_register <= "000000";
					operandA <= regs(conv_integer(op_reg1));
				else
					op_out <= '0';
				end if;

				if opcode = "111101" then
					-- in
					op_in <= '1';
					dest_register <= op_reg1;
				else
					op_in <= '0';
				end if;
								
				if opcode = "000111" then
					-- rshift imm
					op_shift <= '1';
					dest_register <= op_reg1;
					shift_in <= regs(conv_integer(op_reg2));

					sgn := instruction(13);
					shift_in_w <= 
						sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn &
						sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn &
						instruction(13 downto 8);
				elsif opcode = "000110" then
					-- rshift
					op_shift <= '1';
					dest_register <= op_reg1;
					shift_in <= regs(conv_integer(op_reg2));
					shift_in_w <= regs(conv_integer(op_reg3));
				elsif opcode = "000101" then
					-- lshift imm
					op_shift <= '1';
					dest_register <= op_reg1;
					shift_in <= regs(conv_integer(op_reg2));

					sgn := instruction(13);
					shift_in_w <= x"00000000" - 
						(sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn &
						sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn &
						instruction(13 downto 8));
				elsif opcode = "000100" then
					-- lshift
					op_shift <= '1';
					dest_register <= op_reg1;
					shift_in <= regs(conv_integer(op_reg2));
					shift_in_w <= x"00000000" - regs(conv_integer(op_reg3));
				else
					op_shift <= '0';
				end if;
				
				if opcode = "111111" then
					-- halt
					op_halt <= '1';
				else
					op_halt <= '0';
				end if;
				
				if opcode = "100000" then
					-- ld (reg + imm)
					
					op_ld <= '1';
					dest_register <= op_reg1;
					operandA <= regs(conv_integer(op_reg2));
					
					sgn := instruction(13);
					operandB <= 
						sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn &
						sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn &
						instruction(13 downto 0);
					cpu_state <= "1000";
				elsif opcode = "100001" then
					-- ld (reg + reg + imm)
					op_ld <= '1';
					dest_register <= op_reg1;
					operandA <= regs(conv_integer(op_reg2));
					
					sgn := instruction(7);
					operandB <= 
						regs(conv_integer(op_reg3)) +
						(sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn &
						 sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn &
						instruction(7 downto 0));
					cpu_state <= "1000";
				else
					op_ld <= '0';
					cpu_state <= "0101";
				end if;

				if opcode = "100100" then
					-- sto
					op_sto <= '1';
					operandC <= regs(conv_integer(op_reg1));
					operandA <= regs(conv_integer(op_reg2));

					sgn := instruction(13);
					operandB <= 
						sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn &
						sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn &
						instruction(13 downto 0);
				elsif opcode = "100101" then
					-- sto (reg + reg + imm)
					op_sto <= '1';
					operandC <= regs(conv_integer(op_reg1));
					operandA <= regs(conv_integer(op_reg2));
					
					sgn := instruction(7);
					operandB <= 
						regs(conv_integer(op_reg3)) +
						(sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn &
						 sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn &
						instruction(7 downto 0));
				else
					op_sto <= '0';
				end if;
				
				if opcode = "110000" then
					-- jmp
					op_jmp <= '1';
					operandA(5 downto 0) <= instruction(25 downto 20);
					operandB <= regs(conv_integer(op_reg2));
				elsif opcode = "110001" then
					-- jmpr
					op_jmp <= '1';
					operandA(5 downto 0) <= instruction(25 downto 20);
					operandB <= x"000" & instruction(19 downto 0);
				else
					op_jmp <= '0';
				end if;
				
				if opcode = "110100" then
					op_call <= '1';
					dest_register <= op_reg1;
					operandA <= x"000" & instruction(19 downto 0);
					operandC <= (x"000" & pcounter) + 1;
				else
					op_call <= '0';
				end if;
				
				if opcode(5 downto 1) = "01000" then
					-- fadd (fsub)
					op_fadd <= '1';
					dest_register <= op_reg1;
					fadd_in1 <= regs(conv_integer(op_reg2));
					fadd_in2 <= regs(conv_integer(op_reg3)) xor (opcode(0) & "000" & x"0000000");
				else
					op_fadd <= '0';
				end if;
				
				if opcode = "010010" then
					op_fmul <= '1';
					dest_register <= op_reg1;
					fmul_in1 <= regs(conv_integer(op_reg2));
					fmul_in2 <= regs(conv_integer(op_reg3));
				else
					op_fmul <= '0';
				end if;
				
				-- cpu_state <= "0101";
			elsif cpu_state = "0101" then
				if op_cmp = '1' then
					cmp_sub := (operandA(31) & operandA) - (operandB(31) & operandB);
					if cmp_sub = '0' & x"00000000" then
						operandC(0) <= '1';
					else
						operandC(0) <= '0';
					end if;
					operandC(1) <= not cmp_sub(32); -- pos (or zero)
					operandC(2) <= cmp_sub(32);
					cpu_state <= "1000";
				elsif op_fcmp = '1' then
					if operandA = operandB or (operandA(30 downto 0) = "000" & x"0000000" and operandB(30 downto 0) = "000" & x"0000000") then
						fcmp_value(0) := '1';
					else
						fcmp_value(0) := '0';
					end if;
					
					if operandA(31) = '0' and operandB(31) = '1' then
						fcmp_value(1) := '1';
					elsif operandA(31) = '0' and operandB(31) = '0' and ('0' & operandA(30 downto 0)) > ('0' & operandB(30 downto 0)) then
						fcmp_value(1) := '1';
					elsif operandA(31) = '1' and operandB(31) = '1' and ('0' & operandA(30 downto 0)) < ('0' & operandB(30 downto 0)) then
						fcmp_value(1) := '1';
					else
						fcmp_value(1) := '0';
					end if;

					if operandA(31) = '1' and operandB(31) = '0' then
						fcmp_value(2) := '1';
					elsif operandA(31) = '0' and operandB(31) = '0' and ('0' & operandA(30 downto 0)) < ('0' & operandB(30 downto 0)) then
						fcmp_value(2) := '1';
					elsif operandA(31) = '1' and operandB(31) = '1' and ('0' & operandA(30 downto 0)) > ('0' & operandB(30 downto 0)) then
						fcmp_value(2) := '1';
					else
						fcmp_value(2) := '0';
					end if;
					
					fcmp_value(1) := fcmp_value(1) and (not fcmp_value(0));
					fcmp_value(2) := fcmp_value(2) and (not fcmp_value(0));
					
					operandC <= x"0000000" & '0' & fcmp_value(2 downto 0);
					cpu_state <= "1000";
				elsif op_add = '1' then
					operandC <= operandA + operandB;
					cpu_state <= "1000";
				elsif op_sub = '1' then
					operandC <= operandA - operandB;
					cpu_state <= "1000";
				elsif op_shift = '1' then
					operandC <= shift_out;
					cpu_state <= "1000";
				elsif op_out = '1' then
					if wbusy = '0' then
						wenable <= '1';
						wdata <= operandA(7 downto 0);
						cpu_state <= "0110";
					end if;
				elsif op_in = '1' then
					if rempty = '0' then
						rpop <= '1';
						cpu_state <= "0110";
					end if;
				elsif op_sto = '1' then
					--mem_reading <= '0';
					--mem_addr <= operandA(19 downto 0) + operandB(19 downto 0);
					--mem_write_val <= operandC;
					
					mcu_wdata <= operandC;
					cpu_state <= "1000";
				elsif op_jmp = '1' then
					if operandA(3 downto 0) = "0000" or
							(operandA(3 downto 0) = "0010" and cond_zero = '0') or
							(operandA(3 downto 0) = "0011" and cond_zero = '1') or
							(operandA(3 downto 0) = "0100" and cond_pos = '0') or
							(operandA(3 downto 0) = "0101" and cond_pos = '1') or
							(operandA(3 downto 0) = "0110" and cond_neg = '0') or
							(operandA(3 downto 0) = "0111" and cond_neg = '1') then
						operandC <= operandB;
					else
						operandC <= (x"000" & pcounter) + 1;
					end if;
					cpu_state <= "1000";
				elsif op_call = '1' then
					cpu_state <= "1000";
				elsif op_fadd = '1' then
					cpu_state <= "0110";
				elsif op_fmul = '1' then
					cpu_state <= "0110";
				elsif op_halt = '1' then		
					read_mode <= "000";
					cpu_state <= "1111";
					program_read_pos <= x"000000";
				end if;
			elsif cpu_state = "0110" then
				if op_fadd = '1' then
				elsif op_in = '1' then
					operandC <= x"000000" & rdata;
					rpop <= '0';
				elsif op_out = '1' then
					wenable <= '0';
				elsif op_fmul = '1' then
				end if;
				cpu_state <= "0111";
			elsif cpu_state = "0111" then
				if op_fadd = '1' then
					operandC <= fadd_out;
				elsif op_fmul = '1' then
					operandC <= fmul_out;
				end if;
				cpu_state <= "1000";
			elsif cpu_state = "1000" then
				if op_ld = '1' then
					--mem_reading <= '1';
					--mem_addr <= operandA(19 downto 0) + operandB(19 downto 0);
					if mcu_rf = '1' then
						if dest_register /= "000000" then
							regs(conv_integer(dest_register)) <= mcu_rd;
						end if;
						pcounter <= pcounter + 1;
						cpu_state <= "0000";
					end if;
				else
					if op_jmp = '1' then
						pcounter <= operandC(19 downto 0);
					elsif op_call = '1' then
						pcounter <= operandA(19 downto 0);
					else
						pcounter <= pcounter + 1;
					end if;
					cpu_state <= "0000";
				end if;
				
				if not (dest_register = "000000") then
					if op_add = '1' or op_sub = '1' or op_in = '1' or op_fadd = '1' or op_fmul = '1' or op_shift = '1' or op_call = '1' then
						regs(conv_integer(dest_register)) <= operandC;
					end if;
				end if;
				
				if op_cmp = '1' or op_fcmp = '1' then
					cond_zero <= operandC(0);
					cond_pos <= operandC(1) and (not operandC(0));
					cond_neg <= operandC(2);
				end if;
			end if;
		end if;
	end process;
END;
