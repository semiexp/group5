open KNormal

(* インライン展開する関数の最大サイズ (caml2html: inline_threshold) *)
let threshold = ref 0 (* Mainで-inlineオプションによりセットされる *)

let rec size = function
  | If(_,_,(_, e1, e2))
  | Let(_,_,(_, e1, e2))  | LetRec(_,_,({ body = e1 }, e2)) -> 1 + size e1 + size e2
  | LetTuple(_,(_, _, e)) -> 1 + size e
  | _ -> 1

let rec g env = function (* インライン展開ルーチン本体 (caml2html: inline_g) *)
  | If(p1,p2,(x, e1, e2)) -> If(p1,p2,(x, g env e1, g env e2))
  | Let(p,ls,(xt, e1, e2)) -> Let(p,ls,(xt, g env e1, g env e2))
  | LetRec(p,ls,({ name = (x, t); args = yts; body = e1 }, e2)) -> (* 関数定義の場合 (caml2html: inline_letrec) *)
      let env = if SLS.mem SLS.NoInline ls || size e1 > !threshold then env else M.add x (yts, e1) env in
      LetRec(p,ls,({ name = (x, t); args = yts; body = g env e1}, g env e2))
  | App(_,(x, ys)) when M.mem x env -> (* 関数適用の場合 (caml2html: inline_app) *)
      let (zs, e) = M.find x env in
      Format.eprintf "inlining %s@." x;
      Format.eprintf "%s / %s@." (join ", " (List.map fst zs)) (join ", " ys);
      let env' =
        List.fold_left2
          (fun env' (z, t) y -> M.add z y env')
          M.empty
          zs
          ys in
        Alpha.g env' e
  | LetTuple(p,(xts, y, e)) -> LetTuple(p,(xts, y, g env e))
  | e -> e

let f e = g M.empty e
