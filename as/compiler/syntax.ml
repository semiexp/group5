(* ¥½¡¼¥¹¥³¡¼¥ÉÃæ¤Î°ÌÃÖ *)
type pospos = { fname: string; line: int; column: int}
type pos_t = { s: pospos; e: pospos }

(* pos_t¤ò¹çÀ® *)
let merge_pos pos1 pos2 =
  if pos1.s.fname <> pos2.s.fname then
    failwith "cannot merge positions of different files."
  else
    let mf pos1 pos2 =
      if pos1.line < pos2.line then
        pos1
      else
        if pos1.line > pos2.line then
          pos2
        else
          if pos1.column < pos2.column then
            pos1
          else
            pos2
    in
    let me pos1 pos2 =
      if pos1.line < pos2.line then
        pos2
      else
        if pos1.line > pos2.line then
          pos1
        else
          if pos1.column < pos2.column then
            pos2
          else
            pos1
    in {
      s = mf pos1.s pos2.s;
      e = me pos1.s pos2.s
    }

type t = (* MinCaml¤Î¹½Ê¸¤òÉ½¸½¤¹¤ë¥Ç¡¼¥¿·¿ (caml2html: syntax_t) *)
  | Unit of pos_t
  | Bool of pos_t * bool
  | Int of pos_t * int
  | Float of pos_t * float
  | Not of pos_t * t
  | Neg of pos_t * t
  | Add of pos_t * (t * t)
  | Sub of pos_t * (t * t)
  | Mul of pos_t * (t * t)
  | Div of pos_t * (t * t)
  | FNeg of pos_t * t
  | FAdd of pos_t * (t * t)
  | FSub of pos_t * (t * t)
  | FMul of pos_t * (t * t)
  | FDiv of pos_t * (t * t)
  | Eq of pos_t * (t * t)
  | Lt of pos_t * (t * t)
  | If of pos_t * (t * t * t)
  | Let of pos_t * SLS.t * ((Id.t * Type.t) * t * t)
  | Var of pos_t * Id.t
  | LetRec of pos_t * SLS.t * (fundef * t)
  | App of pos_t * (t * t list)
  | Tuple of pos_t * (t list)
  | LetTuple of pos_t * ((Id.t * Type.t) list * t * t)
  | Array of pos_t * (t * t)
  | Get of pos_t * (t * t)
  | Put of pos_t * (t * t * t)
  | Native of pos_t * (Id.t * Type.t)
and fundef = { pos: pos_t; name : Id.t * Type.t; args : (Id.t * Type.t) list; body : t }

(* dummyのposを生成 *)
let dummy_pos () =
  let p1 = {
   fname = ""; 
   line = -1;
   column = -1} in
    {s = p1; e = p1}

(* 複数のtを順にマージする *)
let merge_t (ts: t list) =
  let rec f e = function
    | Let(p,ls,(xt, e1, e2)) ->
        Let(p, ls, (xt, e1, f e e2))
    | LetRec(p,ls,(fd,e2)) ->
        LetRec(p,ls,(fd, f e e2))
    | e2 ->
        Let(dummy_pos(), SLS.ls_default, ((Id.genid "t", Type.gentyp()), e2, e)) in
    List.fold_right (fun (e:t) (e2:t) ->
                       f e2 e) ts (Unit(dummy_pos()))


(* posをget/set *)
let get_pos (exp: t) =
  match exp with
    | Unit p
    | Bool (p, _)
    | Int (p, _)
    | Float (p, _)
    | Not (p, _)
    | Neg (p, _)
    | Add (p, _)
    | Sub (p, _)
    | Mul (p, _)
    | Div (p, _)
    | FNeg (p, _)
    | FAdd (p, _)
    | FSub (p, _)
    | FMul (p, _)
    | FDiv (p, _)
    | Eq (p, _)
    | Lt (p, _)
    | If (p, _)
    | Let (p, _, _)
    | Var (p, _)
    | LetRec (p, _, _)
    | App (p, _)
    | Tuple (p, _)
    | LetTuple (p, _)
    | Array (p, _)
    | Get (p, _)
    | Put (p, _)
    | Native(p, _) -> p

let set_pos (exp: t) (new_pos: pos_t) =
  match exp with
    | Unit _ -> Unit new_pos
    | Bool (_, v) -> Bool (new_pos, v)
    | Int (_, v) -> Int (new_pos, v)
    | Float (_, v) -> Float (new_pos, v)
    | Not (_,e) -> Not (new_pos, e)
    | Neg (_,e) -> Neg (new_pos, e)
    | Add (_,es) -> Add (new_pos, es)
    | Sub (_,es) -> Sub (new_pos, es)
    | Mul (_,es) -> Mul (new_pos, es)
    | Div (_,es) -> Div (new_pos, es)
    | FNeg (_,e) -> FNeg (new_pos, e)
    | FAdd (_,es) -> FAdd (new_pos, es)
    | FSub (_,es) -> FSub (new_pos, es)
    | FMul (_,es) -> FMul (new_pos, es)
    | FDiv (_,es) -> FDiv (new_pos, es)
    | Eq (_,es) -> Eq (new_pos, es)
    | Lt (_,es) -> Lt (new_pos, es)
    | If (_,es) -> If (new_pos, es)
    | Let (_,ls, es) -> Let (new_pos, ls, es)
    | Var (_,e) -> Var (new_pos, e)
    | LetRec (_,ls, es) -> LetRec (new_pos, ls, es)
    | App (_,es) -> App (new_pos, es)
    | Tuple (_,es) -> Tuple (new_pos, es)
    | LetTuple (_,es) -> LetTuple (new_pos, es)
    | Array (_,es) -> Array (new_pos, es)
    | Get (_,es) -> Get (new_pos, es)
    | Put (_,es) -> Put (new_pos, es)
    | Native (_,xt) -> Native (new_pos, xt)

(* tどうしでmerge_posするメソッド *)
let merge_get_pos e1 e2 =
  merge_pos (get_pos e1) (get_pos e2)

    (* listの要素のposをmergeするメソッド *)
let merge_arr_get_pos l =
  match l with
    | [] -> failwith "cannot merge_arr_get_pos the empty list"
    | e::[] -> get_pos e
    | e::l' ->
        let rec last l =
          match l with
            | [] -> failwith "a"
            | x::[] -> x
            | _::l' -> last l'
        in merge_get_pos e (last l')


(* posを文字列に *)
let pos_str p =
  if p.s.line = p.e.line then
    Printf.sprintf "line %d, column %d-%d" p.s.line p.s.column p.e.column
  else
    Printf.sprintf "line %d-%d" p.s.line p.e.line


let rec join gl l =
  match l with
    | [] -> ""
    | x::[] -> x
    | x::l' -> x ^ gl ^ join gl l'

(*week1-q1: t -> string の関数ができる*)
let tree = Tree.make (function
  | Unit _ -> ("UNIT",[])
  | Bool (_,v) -> ("BOOL " ^ string_of_bool v, [])
  | Int (_,v) -> ("INT " ^ string_of_int v, [])
  | Float (_,v) -> ("FLOAT " ^ string_of_float v, [])
  | Not (_,e') -> ("NOT", [e'])
  | Neg (_,e') -> ("NEG",[e'])
  | Add (_,(e1,e2)) -> ("ADD",[e1;e2])
  | Sub (_,(e1,e2)) -> ("SUB",[e1;e2])
  | Mul (_,(e1,e2)) -> ("MUL",[e1;e2])
  | Div (_,(e1,e2)) -> ("DIV",[e1;e2])
  | FNeg (_,e') -> ("FNEG",[e'])
  | FAdd (_,(e1,e2)) -> ("FADD",[e1;e2])
  | FSub (_,(e1,e2)) -> ("FSUB",[e1;e2])
  | FMul (_,(e1,e2)) -> ("FMUL",[e1;e2])
  | FDiv (_,(e1,e2)) -> ("FDIV",[e1;e2])
  | Eq (_,(e1,e2)) -> ("EQ",[e1;e2])
  | Lt (_,(e1,e2)) -> ("LT",[e1;e2])
  | If (_,(e1,e2,e3)) -> ("IF",[e1;e2;e3])
  | Let (_,_,((id,_),e1,e2)) -> ("LET " ^ id, [e1;e2])
  | Var (_,id) -> ("VAR " ^ id, [])
  | LetRec (_,_,({pos = pos; name = name,_; args = args; body = e1},e2)) -> ("LETREC " ^ name ^ " " ^ join " " (List.map fst args), [e1;e2])
  | App (_,(e1,es)) -> ("APP",e1::es)
  | Tuple (_,es) -> ("TUPLE",es)
  | LetTuple (_,(ids, e1,e2)) -> ("LETTUPLE " ^ join ", " (List.map fst ids),[e1;e2])
  | Array (_,(e1,e2)) -> ("ARRAY", [e1;e2])
  | Get (_,(e1,e2)) -> ("GET", [e1;e2])
  | Put (_,(e1,e2,e3)) -> ("PUT", [e1;e2;e3])
  | Native(_,(id,_)) -> ("NATIVE " ^ id,[])) (fun p -> pos_str (get_pos p)) "  "
