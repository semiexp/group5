(* global test *)

export let g_tuple =
  let rec double x = x + x in
    (double 3, double 5) in
  ()
