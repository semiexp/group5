let rec f x =
  let rec g y = x + y in g in 
let h = f 6 in
  print_int (h 4)
