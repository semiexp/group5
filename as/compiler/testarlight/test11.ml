let rec fless x y = x < y in
let a = read_float() in
  if fless a 10.0 then
    print_int 10
  else
    print_int (-10)
