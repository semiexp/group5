let rec id x = x in
let x = id 2.5 in
  out32 (10.0 /. x)
