let rec id x = x in
let a = id 8 in
let b = a + a in
let c = id (-14) in
  print_int b;
  print_newline();
  print_int c;
  print_newline()
