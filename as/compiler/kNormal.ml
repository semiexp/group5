(* give names to intermediate values (K-normalization) *)

type t = (* K正規化後の式 (caml2html: knormal_t) *)
  | Unit of Syntax.pos_t
  | Int of Syntax.pos_t * int
  | Float of Syntax.pos_t * float
  | Neg of Syntax.pos_t * Id.t
  | Add of Syntax.pos_t * (Id.t * Id.t)
  | Sub of Syntax.pos_t * (Id.t * Id.t)
  | Mul of Syntax.pos_t * (Id.t * Id.t)
  | Div of Syntax.pos_t * (Id.t * Id.t)
  | FNeg of Syntax.pos_t * Id.t
  | FAdd of Syntax.pos_t * (Id.t * Id.t)
  | FSub of Syntax.pos_t * (Id.t * Id.t)
  | FMul of Syntax.pos_t * (Id.t * Id.t)
  | FDiv of Syntax.pos_t * (Id.t * Id.t)
  | CmpEq of Syntax.pos_t * (Id.t * Id.t)
  | CmpLt of Syntax.pos_t * (Id.t * Id.t)
  | If of Syntax.pos_t * Syntax.pos_t * (Id.t * t * t) (* 比較 + 分岐 (caml2html: knormal_branch) *)
  | Let of Syntax.pos_t * SLS.t * ((Id.t * Type.t) * t * t)
  | Var of Syntax.pos_t * Id.t
  | LetRec of Syntax.pos_t * SLS.t * (fundef * t)
  | App of Syntax.pos_t * (Id.t * Id.t list)
  | Tuple of Syntax.pos_t * Id.t list
  | LetTuple of Syntax.pos_t * ((Id.t * Type.t) list * Id.t * t)
  | Array of Syntax.pos_t * (int * Type.t * Id.t)
  | Get of Syntax.pos_t * (Id.t * Id.t)
  | Put of Syntax.pos_t * (Id.t * Id.t * Id.t)
  | ExtArray of Syntax.pos_t * Id.t
  | ExtTuple of Syntax.pos_t * Id.t
  | ExtFunApp of Syntax.pos_t * (Id.t * Id.t list)
  | Native of Syntax.pos_t * (Id.t * Type.t)
and fundef = { name : Id.t * Type.t; args : (Id.t * Type.t) list; body : t }

let rec fv = function (* 式に出現する（自由な）変数 (caml2html: knormal_fv) *)
  | Unit(_) | Int(_) | Float(_) | ExtArray(_) | ExtTuple(_) | Native(_) -> S.empty
  | Neg(_,x) | FNeg(_,x) | Array(_,(_,_,x)) -> S.singleton x
  | Add(_,(x, y)) | Sub(_,(x, y)) | Mul(_,(x, y)) | Div(_,(x, y)) | FAdd(_,(x, y)) | FSub(_,(x, y)) | FMul(_,(x, y)) | FDiv(_,(x, y))
  | CmpEq(_, (x, y)) | CmpLt(_, (x, y))
  | Get(_,(x, y)) -> S.of_list [x; y]
  | If(_,_,(x, e1, e2)) -> S.add x (S.union (fv e1) (fv e2))
  | Let(_,_,((x, t), e1, e2)) -> S.union (fv e1) (S.remove x (fv e2))
  | Var(_,x) -> S.singleton x
  | LetRec(_,_,({ name = (x, t); args = yts; body = e1 }, e2)) ->
      let zs = S.diff (fv e1) (S.of_list (List.map fst yts)) in
      S.diff (S.union zs (fv e2)) (S.singleton x)
  | App(_,(x, ys)) -> S.of_list (x :: ys)
  | Tuple(_,xs) | ExtFunApp(_,(_, xs)) -> S.of_list xs
  | Put(_,(x, y, z)) -> S.of_list [x; y; z]
  | LetTuple(_,(xs, y, e)) -> S.add y (S.diff (fv e) (S.of_list (List.map fst xs)))

                                 (* tからpos_tを得る *)
let get_pos (e: t) =
  match e with
    | Unit(p) -> p
    | Int(p,_) | Float(p,_) -> p
    | Neg(p,_) | Add(p,_) | Sub(p,_) | Mul(p,_) | Div(p,_) | FNeg(p,_)
    | FAdd(p,_) | FSub(p,_) | FMul(p,_) | FDiv(p,_)
    | CmpEq(p,_) | CmpLt(p,_)
    | If(p,_,_) | Let(p,_,_) | Var(p,_)
    | LetRec(p,_,_)
    | App(p,_) | Tuple(p,_) | LetTuple(p,_) | Array(p,_)
    | Get(p,_) | Put(p,_) | ExtArray(p,_) | ExtTuple(p,_) | ExtFunApp(p,_)
    | Native(p,_)
      -> p


let insert_let (e, t) k = (* letを挿入する補助関数 (caml2html: knormal_insert) *)
  match e with
  | Var(_,x) -> k x
  | _ ->
      let x = Id.gentmp t in
      let e', t' = k x in
      let p = get_pos e' in
      Let(p,SLS.ls_default, ((x, t), e, e')), t'

let rec g env = function (* K正規化ルーチン本体 (caml2html: knormal_g) *)
  | Syntax.Unit(p) -> Unit(p), Type.Unit
  | Syntax.Bool(p,b) -> Int(p, if b then 1 else 0), Type.Int (* 論理値true, falseを整数1, 0に変換 (caml2html: knormal_bool) *)
  | Syntax.Int(p,i) -> Int(p,i), Type.Int
  | Syntax.Float(p,d) -> Float(p,d), Type.Float
  | Syntax.Not(p,e) -> g env (Syntax.If(p,(e, Syntax.Bool(p,false), Syntax.Bool(p,true))))
  | Syntax.Neg(p,e) ->
      insert_let (g env e)
	(fun x -> Neg(p,x), Type.Int)
  | Syntax.Add(p,(e1, e2)) -> (* 足し算のK正規化 (caml2html: knormal_add) *)
      insert_let (g env e1)
	(fun x -> insert_let (g env e2)
	    (fun y -> Add(p,(x, y)), Type.Int))
  | Syntax.Sub(p,(e1, e2)) ->
      insert_let (g env e1)
	(fun x -> insert_let (g env e2)
	    (fun y -> Sub(p,(x, y)), Type.Int))
  | Syntax.Mul(p,(e1, e2)) ->
      insert_let (g env e1)
	(fun x -> insert_let (g env e2)
	    (fun y -> Mul(p,(x, y)), Type.Int))
  | Syntax.Div(p,(e1, e2)) ->
      insert_let (g env e1)
	(fun x -> insert_let (g env e2)
	    (fun y -> Div(p,(x, y)), Type.Int))
  | Syntax.FNeg(p,e) ->
      insert_let (g env e)
	(fun x -> FNeg(p,x), Type.Float)
  | Syntax.FAdd(p,(e1, e2)) ->
      insert_let (g env e1)
	(fun x -> insert_let (g env e2)
	    (fun y -> FAdd(p,(x, y)), Type.Float))
  | Syntax.FSub(p,(e1, e2)) ->
      insert_let (g env e1)
	(fun x -> insert_let (g env e2)
	    (fun y -> FSub(p,(x, y)), Type.Float))
  | Syntax.FMul(p,(e1, e2)) ->
      insert_let (g env e1)
	(fun x -> insert_let (g env e2)
	    (fun y -> FMul(p,(x, y)), Type.Float))
  | Syntax.FDiv(p,(e1, e2)) ->
      insert_let (g env e1)
	(fun x -> insert_let (g env e2)
	    (fun y -> FDiv(p,(x, y)), Type.Float))
  | Syntax.Eq(p, (e1, e2)) ->
      insert_let (g env e1)
        (fun x -> insert_let (g env e2)
            (fun y -> CmpEq(p, (x, y)), Type.Bool))
  | Syntax.Lt(p, (e1, e2)) ->
      insert_let (g env e1)
        (fun x -> insert_let (g env e2)
            (fun y -> CmpLt(p, (x, y)), Type.Bool))
  | Syntax.If(p,(Syntax.Not(_,e1), e2, e3)) -> g env (Syntax.If(p,(e1, e3, e2))) (* notによる分岐を変換 (caml2html: knormal_not) *)
  | Syntax.If(p,(e1, e2, e3)) ->
      let p2 = Syntax.get_pos e1 in
        insert_let (g env e1)
          (fun x ->
             let e2', _ = g env e2 in
             let e3', t3= g env e3 in
             If(p, p2, (x, e2', e3')), t3)
  | Syntax.Let(p,ls,((x, t), e1, e2)) when SLS.mem SLS.Exported ls ->
      (* ついでに型に問題ないか調べる *)
      (match t with
      | Type.Array(_)
      | Type.Tuple(_) -> 
          let e1', t1 = g env e1 in
          let e2', t2 = g env e2 in (* 外部変数なので追加しない *)
            Let(p,ls,((x, t), e1', e2')), t2
      | _ -> failwith (Printf.sprintf "exported variable %s does not have an array type nor a tuple type but has %s" x (Type.type_str t)))

  | Syntax.Let(p,ls,((x, t), e1, e2)) ->
      let e1', _ = g env e1 in
      let e2', t2 = g (M.add x t env) e2 in
      Let(p,ls,((x, t), e1', e2')), t2
  | Syntax.Var(p,x) when M.mem x env -> Var(p,x), M.find x env
  | Syntax.Var(p,x) -> (* 外部配列の参照 (caml2html: knormal_extarray) *)
      (match M.find x !Typing.extenv with
      | Type.Array(_) as t -> ExtArray(p, x), t
      | Type.Tuple(_) as t -> ExtTuple(p, x), t
      | t -> failwith (Printf.sprintf "external variable %s does not have an array type nor a tuple type but has %s" x (Type.type_str t)))
  | Syntax.LetRec(p,ls,({ Syntax.name = (x, t); Syntax.args = yts; Syntax.body = e1 }, e2)) ->
      let env' = M.add x t env in
      let e2', t2 = g env' e2 in
      let e1', t1 = g (M.add_list yts env') e1 in
      LetRec(p,ls,({ name = (x, t); args = yts; body = e1' }, e2')), t2
  | Syntax.App(p,(Syntax.Var(_,f), e2s)) when not (M.mem f env) -> (* 外部関数の呼び出し (caml2html: knormal_extfunapp) *)
      (match M.find f !Typing.extenv with
      | Type.Fun(_, t) ->
	  let rec bind xs = function (* "xs" are identifiers for the arguments *)
	    | [] -> ExtFunApp(p, (f, xs)), t
	    | e2 :: e2s ->
		insert_let (g env e2)
		  (fun x -> bind (xs @ [x]) e2s) in
	  bind [] e2s (* left-to-right evaluation *)
      | _ -> assert false)
  | Syntax.App(p,(e1, e2s)) ->
      (match g env e1 with
      | _, Type.Fun(_, t) as g_e1 ->
	  insert_let g_e1
	    (fun f ->
	      let rec bind xs = function (* "xs" are identifiers for the arguments *)
		| [] -> App(p,(f, xs)), t
		| e2 :: e2s ->
		    insert_let (g env e2)
		      (fun x -> bind (xs @ [x]) e2s) in
	      bind [] e2s) (* left-to-right evaluation *)
      | _ -> assert false)
  | Syntax.Tuple(p,es) ->
      let rec bind xs ts = function (* "xs" and "ts" are identifiers and types for the elements *)
	| [] -> Tuple(p,xs), Type.Tuple(ts)
	| e :: es ->
	    let _, t as g_e = g env e in
	    insert_let g_e
	      (fun x -> bind (xs @ [x]) (ts @ [t]) es) in
      bind [] [] es
  | Syntax.LetTuple(p,(xts, e1, e2)) ->
      insert_let (g env e1)
	(fun y ->
	  let e2', t2 = g (M.add_list xts env) e2 in
	  LetTuple(p,(xts, y, e2')), t2)
  | Syntax.Array(p,(Syntax.Int(_,i), e2)) ->
      (* サイズが固定されている配列 *)
      let _,t2 as g_e2 = g env e2 in
      insert_let g_e2
        (fun y ->
           Array(p,(i, t2, y)), Type.Array(t2))
  | Syntax.Array(p,(e1, e2)) ->
      insert_let (g env e1)
	(fun x ->
	  let _, t2 as g_e2 = g env e2 in
	  insert_let g_e2
	    (fun y ->
	      let l =
		match t2 with
		| Type.Float -> "create_float_array"
		| _ -> "create_array" in
	      ExtFunApp(p,(l, [x; y])), Type.Array(t2)))
  | Syntax.Get(p,(e1, e2)) ->
      (match g env e1 with
      |	_, Type.Array(t) as g_e1 ->
	  insert_let g_e1
	    (fun x -> insert_let (g env e2)
		(fun y -> Get(p,(x, y)), t))
      | _ -> assert false)
  | Syntax.Put(p,(e1, e2, e3)) ->
      insert_let (g env e1)
	(fun x -> insert_let (g env e2)
	    (fun y -> insert_let (g env e3)
		(fun z -> Put(p,(x, y, z)), Type.Unit)))
  | Syntax.Native(p, (x, t)) ->
      Native(p, (x, t)), t

let f e = fst (g M.empty e)


(* week1-q1: tをきれいに表示 *)
let rec join gl l =
  match l with
    | [] -> ""
    | x::[] -> x
    | x::l' -> x ^ gl ^ join gl l'
let tree =
  let f = function
    | Unit _ -> ("UNIT", [])
    | Int(_,v) -> ("INT " ^ string_of_int v, [])
    | Float(_,v) -> ("FLOAT " ^ string_of_float v, [])
    | Neg(_,id) -> ("NEG " ^ id, [])
    | Add(_,(id1,id2)) -> ("ADD " ^ id1 ^ " " ^ id2, [])
    | Sub(_,(id1,id2)) -> ("SUB " ^ id1 ^ " " ^ id2, [])
    | Mul(_,(id1,id2)) -> ("MUL " ^ id1 ^ " " ^ id2, [])
    | Div(_,(id1,id2)) -> ("DIV " ^ id1 ^ " " ^ id2, [])
    | FNeg(_,id) -> ("FNEG " ^ id, [])
    | FAdd(_,(id1,id2)) -> ("FSUB " ^ id1 ^ " " ^ id2, [])
    | FSub(_,(id1,id2)) -> ("FSUB " ^ id1 ^ " " ^ id2, [])
    | FMul(_,(id1,id2)) -> ("FMUL " ^ id1 ^ " " ^ id2, [])
    | FDiv(_,(id1,id2)) -> ("FDIV " ^ id1 ^ " " ^ id2, [])
    | CmpEq(_,(id1,id2)) -> ("CMPEQ " ^ id1 ^ " " ^ id2, [])
    | CmpLt(_,(id1,id2)) -> ("CMPLT " ^ id1 ^ " " ^ id2, [])
    | If(_,_,(id1,e1,e2)) -> ("IF " ^ id1, [e1;e2])
    | Let(_,_,((id,_),e1,e2)) -> ("LET " ^ id, [e1;e2])
    | Var(_,id) -> ("VAR " ^ id, [])
    | LetRec(_,_,({name = (id,_); args = args; body = e1},e2)) -> ("LETREC " ^ id ^ " " ^ join " " (List.map fst args), [e1;e2])
    | App(_,(id,ids)) -> ("APP " ^ id ^ " " ^ join " " ids, [])
    | Tuple(_,ids) -> ("TUPLE (" ^ " " ^ join ", " ids ^ ")",  [])
    | LetTuple(_,(ids,id,e)) -> ("LETTUPLE (" ^ join ", " (List.map fst ids) ^ ") = " ^ id, [e])
    | Array(_,(i,t,id)) -> ("ARRAY " ^ string_of_int i  ^ " " ^ id,[])
    | Get(_,(id1,id2)) -> ("GET " ^ id1 ^ " " ^ id2,[])
    | Put(_,(id1,id2,id3)) -> ("PUT " ^ id1 ^ " " ^ id2 ^ " " ^ id3,[])
    | ExtArray(_,id) -> ("EXTARRAY " ^ id,[])
    | ExtTuple(_,id) -> ("EXTTUPLE " ^ id,[])
    | ExtFunApp(_,(id,ids)) -> ("EXTFunApp " ^ id ^ " " ^ join " " ids,[])
    | Native(_,(id,_)) -> ("NATIVE " ^ id, [])
  in
  let g = function
    | Unit p | Int(p,_) | Float(p,_) | Neg(p,_) | Add(p,_) | Sub (p,_) | Mul(p,_) | Div(p,_)
    | FNeg(p,_) | FAdd(p,_) | FSub(p,_) | FMul(p,_) | FDiv(p,_)
    | CmpEq(p,_) | CmpLt(p,_)
    | If(p,_,_) | Let(p,_,_) | Var(p,_) | LetRec(p,_,_)
    | App(p,_) | Tuple(p,_) | LetTuple(p,_) | Array(p,_) | Get(p,_) | Put(p,_) 
    | ExtArray(p,_) | ExtTuple(p,_) | ExtFunApp(p,_)
    | Native(p,_)-> Syntax.pos_str p
  in Tree.make f g "  "


