open KNormal
(* タプル展開の最適化
 * 1: ネストしたタプルは平坦化する
 * 2: 関数の引数にタプルがある場合は展開してわたす
 *)


(* 2つのリストが同じ長さか調べる *)
let rec hasSameLength l1 l2 =
  match l1, l2 with
    | [], [] -> true
    | x::l1', y::l2' -> hasSameLength l1' l2'
    | _ -> false

(* listをsplitする関数 *)
let rec split start number l =
  match l with
    | [] -> []
    | x::l' ->
        if start > 0 then
          split (start-1) number l'
        else if number>0 then
          x :: split 0 (number-1) l'
        else
          []

(* タプルの型の平坦化 *)
let rec flatten_type t =
  match t with
    | Type.Tuple ts ->
        List.concat (List.map flatten_type ts)
    | _ -> [t]


(* タプルの平坦化 xs: タプルの構成要素の変数 *)
let rec flatten tenv xs =
  List.concat (List.map (fun x ->
                           try
                             let ys = M.find x tenv in
                               flatten tenv ys
                           with Not_found ->
                             [x]) xs)
(* 型の展開 *)
let rec expand_type = function
  | Type.Tuple ts ->
      (* タプルを平坦化する *)
      let ts' = List.map expand_type ts in
      Type.Tuple(flatten_type (Type.Tuple ts'))
  | Type.Fun(ts, rt) ->
      (* 引数にタプルがあったら展開する必要がある *)
      let ts' = List.map expand_type ts in
      let rt' = expand_type rt in
      let ts''= List.fold_right (fun t ts ->
                                   match t with
                                     | Type.Tuple ts2 ->
                                         ts2@ts
                                     | _ -> t::ts) ts' [] in
        Type.Fun(ts'', rt')
  | Type.Array t ->
      Type.Array (expand_type t)
  | Type.Var _ -> assert false
  | t -> t

(* タプルxを一時変数に展開して(x, t)リストを返す。ついでにtenvも更新する *)
let expand tenv x t =
  (* 型を平坦化して型を並べたものを得る *)
  let t' = expand_type t in
    match t' with
      | Type.Tuple ts' ->
          (* 一時変数を生成 *)
          let tmps = List.map (fun t -> Id.gentmp t) ts' in
          let tenv' = M.add x tmps tenv in
            (List.combine tmps ts', tenv')
      | _ -> assert false



let rec g tenv = function
  | Let(p,ls,((x, t), e1, e2)) ->
      let e1' = g tenv e1 in
      let t' = expand_type t in (* 型を展開 *)
        (match t' with
          | Type.Tuple _ ->
              (* タプルなので平坦化が必要 *)
              (match e1' with
                 | Tuple(p2, xs) ->
                     (* 平坦化済のタプルなので覚えておけばいい *)
                     let tenv' = M.add x xs tenv in
                     let e2' = g tenv' e2 in
                       Let(p,ls,((x, t'), e1', e2'))
                 | Var(p,y) ->
                     (* コピーなら別にいいが、覚える必要がある *)
                     let tenv' = M.add x (M.find y tenv) tenv in
                       Let(p,ls,((x, t'), e1', g tenv' e2))
                 | _ ->
                     (* 展開する必要がある *)
                     let (tmps, tenv') = expand tenv x t in
                       Let(p,ls,((x, t'), e1',
                               LetTuple(p, (tmps, x, g tenv' e2)))))
          | _ ->
              (* タプル以外はそのまま *)
              Let(p, ls,((x, t'), e1', g tenv e2)))
  | Tuple(p, xs) ->
      (* 平坦化したらどの変数を並べればいいか調べて並べる *)
      Tuple(p, flatten tenv xs)
  | LetTuple(p, (xts, y, e)) ->
      (* タプルを分解する *)
      (* yはysに平坦化されているので必要に応じて部分タプルを再構成する *)
      let ys = (try M.find y tenv with Not_found -> failwith y) in
        if hasSameLength xts ys then
          (* タプルを平坦化すると要素の数は増える一方なので、両者の要素数が一致していればすでにyは目的のタプルである *)
          (* 型を調整する *)
          let xts' = List.map (fun (x, t) -> (x, expand_type t)) xts in
          LetTuple(p, (xts', y, g tenv e))
        else
            (* タプルに一段階深入りする *)
            let (_, tenv', f) = List.fold_left (fun (i, tenv, f) (x, t) ->
                                                  let t' = expand_type t in
                                                  (match t' with
                                                     | Type.Tuple ts' ->
                                                         let l = List.length ts' in
                                                         (* この要素は平坦化後にl要素つかっている *)
                                                         let els = split i l ys in
                                                         (* xの情報を登録する *)
                                                         let tenv' = M.add x els tenv in
                                                         let f' = (fun tenv ->
                                                                     Let(p,SLS.ls_default,((x, t'), Tuple(p, split i l ys), f tenv))) in
                                                           (i+l, tenv', f')
                                                     | _ ->
                                                         (* タプルではないのでそのままアレする *)
                                                         let f' = (fun tenv ->
                                                                     Let(p,SLS.ls_default,((x, t'), Var(p, List.nth ys i), f tenv))) in
                                                           (i+1, tenv, f'))) (0, tenv, (fun tenv -> g tenv e)) xts in
              f tenv'
  | LetRec(p, ls, ({name = (x, t); args = yts; body = e1}, e2)) ->
      (* 関数のときは引数のタプルを展開する *)
      (* 中の処理のために関数の最初でタプルを作る（あとのことは別の最適化でうまいことやる） *)
      let tenv', yts', f = List.fold_right (fun (y, t) (tenv', yts, f) ->
                                            match t with
                                              | Type.Tuple _ ->
                                                  (* 引数yはタプルだから展開する *)
                                                  Printf.eprintf "expanding argument %s of function %s\n" y x;
                                                  (* tenvに再構成されたタプルの情報を付加 *)
                                                  let tmps, tenv'' = expand tenv' y t in
                                                  (* 関数の最初でタプルを再構成する *)
                                                  let f' tenv =
                                                    let tnames = List.map fst tmps in
                                                    let ttype  = Type.Tuple(List.map snd tmps) in
                                                      (* タプルを再構成 *)
                                                      Let(Syntax.dummy_pos(),
                                                          SLS.ls_default,
                                                          ((y, ttype),
                                                           Tuple(Syntax.dummy_pos(),
                                                                 tnames),
                                                           f tenv)) in
                                                    (tenv'', tmps@yts, f')
                                              | _ ->
                                                  (* タプル以外の引数はそのまま *)
                                                  (tenv', (y, expand_type t)::yts, f)) yts (tenv, [], (fun tenv ->
                                                                                     (* 関数内部の平坦化 *)
                                                                                     g tenv e1)) in
        (* fをつかってあれする *)
        (match t with
           | Type.Fun(_) ->
               (* 返り値の型を取得して引数の型を再構成 *)
               (*Printf.eprintf "%s => %s\n" (join ", " (List.map fst yts)) (join ", " (List.map fst yts'));*)
               let t' = expand_type t (*Type.Fun(List.map snd yts', expand_type rt)*) in
                 LetRec(p, ls, ({name = (x, t'); args = yts'; body = f tenv'}, g tenv e2))
           | _ -> assert false)
  | App(p, (x, ys)) ->
      (* 関数適用のときは引数にタプルがあれば展開する *)
      let ys' = List.fold_right (fun y ys' ->
                                   try
                                     let tmps = M.find y tenv in
                                       tmps@ys'
                                   with Not_found -> y::ys') ys [] in
        App(p, (x, ys'))
  | ExtFunApp(p, (x, ys)) when x <> "create_array" -> (* super-tenuki *)
      (* 外部関数であっても例外ではない！！！！！！！！ *)
      let ys' = List.fold_right (fun y ys' ->
                                   try
                                     let tmps = M.find y tenv in
                                       tmps@ys'
                                   with Not_found -> y::ys') ys [] in
        ExtFunApp(p, (x, ys'))
  | If(p1, p2, (x, e1, e2)) ->
      If(p1, p2, (x, g tenv e1, g tenv e2))
  | Array(p, (i, t, x)) ->
      Array(p, (i, expand_type t, x))
  | Native(p, (x, t)) ->
      Native(p, (x, expand_type t))
  | e -> e

(* 関数 *)
(*
let h { pos= pos; name= name; args= yts; formal_fv= zts; body= e} =
  (* 引数・自由変数にタプルがあったら全部展開しておく *)
  let tenv', f = List.fold_left (fun (tenv', f) (y, t) ->
                                    match t with
                                      | Type.Tuple _ ->
                                          (* タプルを展開 *)
                                          let tmps, tenv'' = expand tenv' y t in
                                          let f' tenv = LetTuple(Syntax.dummy_pos(), (tmps, y, f tenv)) in
                                            (tenv'', f')
                                      | _ -> (tenv', f)) (M.empty, (fun tenv -> 
                                                                      (* tenvができたらそれを使って内部を平坦化 *)
                                                                      g tenv e))  (yts@zts) in
    { pos=pos; name=name; args=yts; formal_fv=zts; body=f tenv'} *)

let f e = g M.empty e
