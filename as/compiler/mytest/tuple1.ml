let rec id x = x in
let t1 = (id 2, (id 3, id 5), (id 7, (id 11, id 13), id 17)) in
let (a, b, c) = t1 in
  print_int a;
  print_newline();
  let (d, e) = b in
    print_int d;
    print_newline();
    print_int e;
    print_newline();
    let (f, g, h) = c in
      print_int f;
      print_newline();
      let (i, j) = g in
        print_int i;
        print_newline();
        print_int j;
        print_newline();
        print_int h;
        print_newline()

