let rec fib n =
  if n <= 1 then
    n
  else
    fib(n-1) + fib(n-2) in
let rec square x = x * 2 in
let rec f n =
  let x = square (fib n) + 1 in
  let y = 2 * n in
    x + y in
let a = f 10 in
  print_int a;
  print_newline()
