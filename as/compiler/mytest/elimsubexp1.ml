let rec id x = x in
let a = id 5 in
let b = id 3 in
let c = a + b in
let d = a + b in
  print_int (c + d)
