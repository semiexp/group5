let t0 = (0, 0) in
let rec add2 x y =
  let (a, b) = x in
  let (c, d) = y in
    (a+c, b+d) in
export let a1 = create_array 2 t0 in
a1.(1) <- (3, 5);
a1.(0) <- (2, 3);
a1.(1) <- add2 a1.(0) a1.(1);
let (x, y) = a1.(1) in
  print_int x;
  print_newline();
  print_int y;
  print_newline()
