let rec fst y =
  let (a, b) = y in a in
let rec snd y =
  let (a, b) = y in b in
let rec add y =
  let (a, b) = y in a + b in
let rec id x = x in
let t1 = (id 3, id 5) in
  print_int (fst t1);
  print_newline();
  print_int (snd t1);
  print_newline();
  print_int (add t1);
  print_newline();

