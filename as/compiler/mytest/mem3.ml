let rec veccpy dest src =
  dest.(0) <- src.(0);
  dest.(1) <- src.(1);
  dest.(2) <- src.(2) in
let rec f x =
  let b = create_array 3 0 in
    veccpy a b;
    x in
  print_int (f 3)
