let a1 = create_array 3 0 in
let a2 = create_array 3 0 in
let t0 = (a1, a2) in
let t1 = (a2, a1) in
let rec id x = x in
let rec add2 x y i =
  let (a, b) = x in
  let (c, d) = y in
    (a.(i) + c.(i), b.(i) + d.(i)) in
a1.(1) <- 5;
a2.(1) <- (-3);
a1.(0) <- 2;
a2.(0) <- 4;
let (a, b) = add2 t0 t1 0 in
  print_int a;
  print_newline();
  print_int b;
  print_newline();
  let (c, d) = add2 t1 t0 1 in
    print_int c;
    print_newline();
    print_int d;
    print_newline();
