let a = create_array 3 (0, 0) in
  a.(1) <- (3, 5);
  a.(2) <- (1, 1);
  let rec f a i =
    if i >= 3 then
      ()
    else
      let (x, y) = a.(i) in
        print_int x;
        print_int y;
        f a (i+1) in
    f a 0

