let rec id x = x in
noinline let rec add x y = id (x+y) in
let a = id 5 in
let b = id 3 in
let c = add a b in
  print_int c;
  print_newline()
