let rec id x = x in
let rec add2 x y =
  let (a, b) = x in
  let (c, d) = y in
    (a+c, b+d) in
let t1 = (id 2, id 3) in
export let t2 = (id 5, id 7) in
let (x, y) = add2 t1 t2 in
  print_int x;
  print_newline();
  print_int y;
  print_newline()

