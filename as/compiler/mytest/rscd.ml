let rec add x y = x + y in
let rec id x = x in
let a = id 5 in
let b = id 3 in
let c = add a b in
let d = add a c in
let e = add b c in
  print_int d;
  print_int e
