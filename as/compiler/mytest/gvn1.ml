let a = 3 in
let b = 3 in
let rec id x = x in
let c = id (a, b) in
let (x, y) = c in
  print_int x;
