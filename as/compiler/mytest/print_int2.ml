noinline let rec print_int n =
  let rec print_int3 n u uc c =
    if n < (uc + u) then
      (print_char c;
       (* div10 *)
       let u3 = ([rshift] u 1) + ([rshift] u 2) in
       let u3 = u3 + ([rshift] u3 4) in
       let u3 = u3 + ([rshift] u3 8) in
       let u3 = u3 + ([rshift] u3 16) in
       let u3 = [rshift] u3 3 in
       let r = u - ([rshift] (([rshift] u3 2) + u3) 1) in
       let u3 = u3 + (if r > 9 then 1 else 0) in
         if u3 = 0 then ()
         else
           print_int3 (n - uc) u3 0 48)
    else
      print_int3 n u (uc+u) (c+1) in
  let rec print_int2 n u =
    let u2 = ([lshift] u 3) + ([lshift] u 1) in
      if n < u2 then
        print_int3 n u 0 48
      else
        print_int2 n u2 in
  if n < 0 then
    print_char 45;
    print_int2 (-n) 1
  else
    print_int2 n 1
      in
  print_int 123
