let rec id x = x in
export let t1 = (id 3, (id 5, id 7)) in
export let t2 = (id 3, id 5) in
let rec add321 x y i =
  let (a, b) = x in
  let (c, d) = b in
  let (e, f) = y in
    a + c + d + e + f + i in
let foo = add321 t1 t2 1 in
  print_int foo;
  print_newline()

