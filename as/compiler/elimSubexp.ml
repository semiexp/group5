(* week2-q2: 共通部分式最適化 *)
open KNormal

(* assoc listの中から目的のものを探す *)

type vt = {
  neg: (Id.t * Id.t) list;
  add: ((Id.t * Id.t) * Id.t) list;
  sub: ((Id.t * Id.t) * Id.t) list;
  mul: ((Id.t * Id.t) * Id.t) list;
  div: ((Id.t * Id.t) * Id.t) list;
  fneg: (Id.t * Id.t) list;
  fadd: ((Id.t * Id.t) * Id.t) list;
  fsub: ((Id.t * Id.t) * Id.t) list;
  fmul: ((Id.t * Id.t) * Id.t) list;
  fdiv: ((Id.t * Id.t) * Id.t) list;
  tuple: ((Id.t list) * Id.t) list;
}

let t (mp : ('a * Id.t) list) (v: 'a) p no =
  try
    let x = List.assoc v mp in
    Printf.eprintf "eliminated subexp into %s\n" x;
    Var(p, x)
  with Not_found ->
    no
  

let rec g (vs: vt)= function
  | Neg(p,x) as exp ->
      t vs.neg x p exp
  | Add(p,(x,y)) as exp ->
      t vs.add (x,y) p exp
  | Sub(p,(x,y)) as exp ->
      t vs.sub (x,y) p exp
  | Mul(p,(x,y)) as exp ->
      t vs.mul (x,y) p exp
  | Div(p,(x,y)) as exp ->
      t vs.div (x,y) p exp
  | FNeg(p,x) as exp ->
      t vs.fneg x p exp
  | FAdd(p,(x,y)) as exp ->
      t vs.fadd (x,y) p exp
  | FSub(p,(x,y)) as exp ->
      t vs.fsub (x,y) p exp
  | FMul(p,(x,y)) as exp ->
      t vs.fmul (x,y) p exp
  | FDiv(p,(x,y)) as exp ->
      t vs.fdiv (x,y) p exp
  | IfEq(p1,p2,(x,y,e1,e2)) ->
      IfEq(p1,p2,(x,y, g vs e1, g vs e2))
  | IfLE(p1,p2,(x,y,e1,e2)) ->
      IfLE(p1,p2,(x,y, g vs e1, g vs e2))
  | Let(p,((x,t),e1,e2)) ->
      (if Elim.effect e1 then
         (* e1に副作用があるので記録しない *)
         Let(p,((x,t),g vs e1, g vs e2))
       else
           (* e1には副作用がないのでe1とxを対応付けたい *)
           let e1' = g vs e1 in
           let vs' = match e1' with
             | Neg(p,y) -> (* x = -y *)
                 {vs with neg = (y,x)::vs.neg}
             | Add(p,(y,z)) -> {vs with add = ((y,z),x)::vs.add}
             | Sub(p,(y,z)) -> {vs with sub = ((y,z),x)::vs.sub}
             | Mul(p,(y,z)) -> {vs with mul = ((y,z),x)::vs.mul}
             | Div(p,(y,z)) -> {vs with div = ((y,z),x)::vs.div}
             | FNeg(p,y) -> {vs with fneg = (y,x)::vs.fneg}
             | FAdd(p,(y,z)) -> {vs with fadd = ((y,z),x)::vs.fadd}
             | FSub(p,(y,z)) -> {vs with fsub = ((y,z),x)::vs.fsub}
             | FMul(p,(y,z)) -> {vs with fmul = ((y,z),x)::vs.fmul}
             | FDiv(p,(y,z)) -> {vs with fdiv = ((y,z),x)::vs.fdiv}
             | Tuple(p,ys) -> {vs with tuple = (ys,x)::vs.tuple}
             | _ -> vs
           in
             Let(p,((x,t),e1', g vs' e2)))
  | ExportLet(p,((x,t),e1,e2)) ->
      (if Elim.effect e1 then
         (* e1に副作用があるので記録しない *)
         ExportLet(p,((x,t),g vs e1, g vs e2))
       else
           (* e1には副作用がないのでe1とxを対応付けたい *)
           let e1' = g vs e1 in
           let vs' = match e1' with
             | Neg(p,y) -> (* x = -y *)
                 {vs with neg = (y,x)::vs.neg}
             | Add(p,(y,z)) -> {vs with add = ((y,z),x)::vs.add}
             | Sub(p,(y,z)) -> {vs with sub = ((y,z),x)::vs.sub}
             | Mul(p,(y,z)) -> {vs with mul = ((y,z),x)::vs.mul}
             | Div(p,(y,z)) -> {vs with div = ((y,z),x)::vs.div}
             | FNeg(p,y) -> {vs with fneg = (y,x)::vs.fneg}
             | FAdd(p,(y,z)) -> {vs with fadd = ((y,z),x)::vs.fadd}
             | FSub(p,(y,z)) -> {vs with fsub = ((y,z),x)::vs.fsub}
             | FMul(p,(y,z)) -> {vs with fmul = ((y,z),x)::vs.fmul}
             | FDiv(p,(y,z)) -> {vs with fdiv = ((y,z),x)::vs.fdiv}
             | Tuple(p,ys) -> {vs with tuple = (ys,x)::vs.tuple}
             | _ -> vs
           in
             ExportLet(p,((x,t),e1', g vs' e2)))
  | LetRec(p,(f,e)) ->
      LetRec(p,(f,g vs e))
  | ExportLetRec(p,(f,e)) ->
      ExportLetRec(p,(f,g vs e))
  | Tuple(p,xs) as exp ->
      t vs.tuple xs p exp
  | LetTuple(p,(xts,y,e)) ->
      LetTuple(p,(xts,y,g vs e))
  | exp -> exp

let f = g {
  neg = [];
  add = [];
  sub = [];
  mul = [];
  div = [];
  fneg= [];
  fadd= [];
  fsub= [];
  fmul= [];
  fdiv= [];
  tuple=[]}

