(* ネストした即値IFを展開する最適化 *)
open KNormal

let rec g env = function
   | IfEq(p1,p2,(x, y, e1, e2)) ->
       (try
          (match M.find x env with
             | `IfEq(v, w, a, b) ->
                 (match M.find y env with
                    | `Int(c) ->
                        if a = c then
                          (if b = c then
                             (let e1' = g env e1 in
                                IfEq(p1,p2, (v, w, e1', e1')))
                           else
                             IfEq(p1, p2, (v, w, g env e1, g env e2)))
                        else
                          (if b = c then
                             IfEq(p1, p2, (v, w, g env e2, g env e1))
                           else
                             (let e2' = g env e2 in
                                IfEq(p1, p2, (v, w, e2', e2'))))
                    | _ -> IfEq(p1, p2, (x, y, g env e1, g env e2)))
             | `IfLE(v, w, a, b) ->
                 (match M.find y env with
                    | `Int(c) ->
                        if a = c then
                          (if b = c then
                             (let e1' = g env e1 in
                                IfLE(p1,p2, (v, w, e1', e1')))
                           else
                             IfLE(p1, p2, (v, w, g env e1, g env e2)))
                        else
                          (if b = c then
                             IfLE(p1, p2, (v, w, g env e2, g env e1))
                           else
                             (let e2' = g env e2 in
                                IfLE(p1, p2, (v, w, e2', e2'))))
                    | _ -> IfEq(p1, p2, (x, y, g env e1, g env e2)))
             | _ -> IfEq(p1, p2, (x, y, g env e1, g env e2)))
        with Not_found -> IfEq(p1, p2, (x, y, g env e1, g env e2)))
  | IfLE(p1, p2, (x, y, e1, e2)) ->
      IfLE(p1, p2, (x, y, g env e1, g env e2))
  | Let(p, ls, ((x, t), e1, e2)) ->
      let env' =
        match e1 with
          | IfEq(_,_,(v, w, Int(_,a), Int(_,b))) ->
              M.add x (`IfEq(v, w, a, b)) env
          | IfLE(_,_,(v, w, Int(_,a), Int(_,b))) ->
              M.add x (`IfLE(v, w, a, b)) env
          | Int(_,a) ->
              M.add x (`Int(a)) env
          | _ -> env in
        Let(p, ls, ((x, t), g env e1, g env' e2))
  | LetRec(p, ls, ({name = name; args = args; body = e1}, e2)) ->
      LetRec(p, ls, ({name = name; args = args; body = g env e1}, g env e2))
  | LetTuple(p,(xts,y,e)) ->
      LetTuple(p,(xts, y, g env e))
  | e -> e

let rec f = g M.empty



