(* メモリアクセスをなんとかする最適化 *)
open KNormal

type venv = MVExt of Id.t | MVImm of int | MVImmf of float
type vidx = MIdx of int | MIdxV of Id.t

let rec g env venv menv = function
  (* env : 関数のメモリに対する副作用 
   * venv: 変数の中身
   * menv: 判明しているメモリの状態 *)
  | Put(p, (x, y, z)) as exp ->
      (* Put: xのy番地にzを書き込む *)
      let idx = try
        (match M.find y venv with
           | MVImm i -> MIdx i
           | _ -> MIdxV y)
      with Not_found -> MIdxV y in
        (try
          (match M.find x venv with
             | MVExt av ->
                 (* グローバル変数なので登録する *)
                 let md = try M.find av menv with Not_found -> [] in
                 let md' = (idx, z) :: List.remove_assoc idx md in
                 let menv' = M.add av md' menv in
                   (*Printf.eprintf "Put: %s %s\n" av (match idx with | MIdx i -> string_of_int i | MIdxV y -> y);*)
                   (exp, menv')
             | _ -> (exp, menv))
         with Not_found -> (exp, menv))
  | Get(p, (x, y)) as exp->
      (* Get: xのy番地を得る *)
      let idx = try
        (match M.find y venv with
           | MVImm i -> MIdx i
           | _ -> MIdxV y)
      with Not_found -> MIdxV y in
      let av = try
        (match M.find x venv with
           | MVExt y -> y
           | _ -> x)
      with Not_found -> x in
        (*Printf.eprintf "Get: %s %s\n" av (match idx with | MIdx i -> string_of_int i | MIdxV y -> y);*)
        (*M.iter (fun x md ->
                   Printf.eprintf "\t%s:\n" x;
                   List.iter (fun (idx, v) ->
                                Printf.eprintf "\t\t%s = %s\n"
                                  (match idx with |MIdx i -> string_of_int i | MIdxV y -> y)
                                  v) md) menv;*)
        (try
           let md = M.find av menv in
           let z = List.assoc idx md in
             (* z: メモリに入っている値が判明した *)
             (*Printf.eprintf "\t%s.(%s) = %s\n" av (match idx with |MIdx i -> string_of_int i | MIdxV y -> y) z;*)
             (Var(p, z), menv)
         with Not_found ->
           (* メモリの値が不明だ *)
           (exp, menv))

  | Let(p, ls, ((x, t), e1, e2)) ->
      (* e1の処理次第でメモリがかわる *)
      let (e1', menv') = g env venv menv e1 in
      (* venvを更新できるかも *)
      let venv' = (match e1' with
                     | Int(_,i) -> M.add x (MVImm i) venv
                     | Float(_,i) -> M.add x (MVImmf i) venv
                     | ExtArray(_,y) -> M.add x (MVExt ("min_caml_" ^ y)) venv
                     | Array(_,(i, _, _)) when SLS.mem SLS.Exported ls ->
                         (* ExtArray扱いにできる *)
                         Printf.eprintf "exteeeeerrrrrrrn %s\n" x;
                         M.add x (MVExt x) venv
                     | Var(_, y) when M.mem y venv ->
                         M.add x (M.find y venv) venv
                     | _ -> venv) in
      let x' = (try
                  (match M.find x venv' with
                     | MVExt y -> y
                     | _ -> x)
                with Not_found -> x) in
      (* menvも更新できるかも *)
      let menv' = (match e1' with
                      | Array(_, (n, _, v)) ->
                          let rec mkmd md i =
                            if i >= n then
                              md
                            else
                              let md' = (MIdx i, v) :: md in
                                mkmd md' (i+1) in
                          M.add x' (mkmd [] 0) menv'
                     | Get(_, (y, z)) ->
                         (* 不明だったけど今回でわかったぞ *)
                         let y' = (try
                                     (match M.find y venv' with
                                        | MVExt y' -> y'
                                        | _ -> y)
                                   with Not_found -> y) in
                         let md = (try M.find y' menv' with Not_found -> []) in
                         let idx = (try
                                      (match M.find z venv' with
                                         | MVImm i -> MIdx i
                                         | _ -> MIdxV z)
                                    with Not_found -> MIdxV z) in
                         let md' = (idx, x) :: List.remove_assoc idx md in
                           M.add y' md' menv'
                     | _ -> menv') in
      let (e2', menv') = g env venv' menv' e2 in
        (Let(p, ls, ((x, t), e1', e2'))), menv'
  | LetRec(p, ls, ({ name = (x, t); args = yts; body = e1}, e2)) ->
      (* e1によってメモリが書き換えられるときは気をつける *)
      let (e1', menv2) = g env venv M.empty e1 in
        (* TODO 引数そのままのときは最適化可能？ *)
      let env' = M.add x menv2 env in
      let (e2', menv') = g env' venv menv e2 in
        LetRec(p, ls, ({name = (x, t); args = yts; body = e1'}, e2')), menv'
  | LetTuple(p, (xts, y, e1)) ->
      (* 特に副作用はない *)
      let (e1', menv') = g env venv menv e1 in
      LetTuple(p, (xts, y, e1')), menv'
  | If(p1,p2,(x, e1, e2)) ->
      (* e1とe2が別々にメモリを書き換えるかも…… *)
      let (e1', menv'1) = g env venv menv e1 in
      let (e2', menv'2) = g env venv menv e2 in
        (* 共通でしかも同じもののみ残す *)
      let menv' = M.filter (fun x v ->
                                M.mem x menv'2 && v = M.find x menv'2) menv'1 in
        If(p1,p2,(x, e1', e2')), menv'
  | App(p, (x, ys)) as exp ->
      (* 関数適用 *)
      (* メモリが書き換えられてしまう *)
      (*let menv' = (try
                     let menv2 = M.find x env in
                       M.fold (fun z _ menv ->
                                 (* zが書き換えられる *)
                                 (try
                                    (match M.find z venv with
                                       | MVExt v ->
                                           (* zは外部変数vだ *)
                                           M.remove v menv
                                       | _ ->
                                           (* 知らない変数が出てきてしまったのでメモリの中身は保証できない *)
                                           M.empty)
                                  with Not_found -> M.empty)) menv2 menv 
                   with
                     | Not_found ->
                         (* 関数xについての情報がないのでメモリキャッシュをクリア *)
                         M.empty) in
        exp, menv'*)
      exp, M.empty
  | ExtFunApp(_) as exp ->
      (* 外部関数はわからん *)
      (* XXX assuming it has no memory-breaking sideeffect *)
      exp, M.empty
  | exp ->
      (* ほかは副作用ないしそのまま *)
      exp, menv

let rec f e = fst (g M.empty M.empty M.empty e)
