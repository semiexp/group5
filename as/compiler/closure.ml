type closure = { entry : Id.l; actual_fv : Id.t list }
type t = (* クロージャ変換後の式 (caml2html: closure_t) *)
  | Unit of Syntax.pos_t
  | Int of Syntax.pos_t * int
  | Float of Syntax.pos_t * float
  | Neg of Syntax.pos_t * Id.t
  | Add of Syntax.pos_t * (Id.t * Id.t)
  | Sub of Syntax.pos_t * (Id.t * Id.t)
  | Mul of Syntax.pos_t * (Id.t * Id.t)
  | Div of Syntax.pos_t * (Id.t * Id.t)
  | FNeg of Syntax.pos_t * Id.t
  | FAdd of Syntax.pos_t * (Id.t * Id.t)
  | FSub of Syntax.pos_t * (Id.t * Id.t)
  | FMul of Syntax.pos_t * (Id.t * Id.t)
  | FDiv of Syntax.pos_t * (Id.t * Id.t)
  | CmpEq of Syntax.pos_t * (Id.t * Id.t)
  | CmpLt of Syntax.pos_t * (Id.t * Id.t)
  | If of Syntax.pos_t * Syntax.pos_t * (Id.t * t * t)
  | Let of Syntax.pos_t * ((Id.t * Type.t) * t * t)
  | Var of Syntax.pos_t * (Id.t)
  | MakeCls of Syntax.pos_t * ((Id.t * Type.t) * closure * t)
  | AppCls of Syntax.pos_t * (Id.t * Id.t list)
  | AppDir of Syntax.pos_t * (Id.l * Id.t list)
  | Tuple of Syntax.pos_t * (Id.t list)
  | LetTuple of Syntax.pos_t * ((Id.t * Type.t) list * Id.t * t)
  | Get of Syntax.pos_t * (Id.t * Id.t)
  | Put of Syntax.pos_t * (Id.t * Id.t * Id.t)
  | ExtArray of Syntax.pos_t * Id.l
  | ExtTuple of Syntax.pos_t * Id.l
  (* 長さのきまった配列を作成する *)
  | Array of Syntax.pos_t * (int * Type.t * Id.t)
  (* グローバル変数として配列を作成する *)
  | GlobalArray of Syntax.pos_t * (Id.l * int * Id.t)
  | GlobalTuple of Syntax.pos_t * (Id.l * Id.t list)
  (* ネイティブな関数的な何かを適用 *)
  | AppNative of Syntax.pos_t * ((Id.t * Type.t)  * Id.t list)
type fundef = { pos : Syntax.pos_t;
                name : Id.l * Type.t;
		args : (Id.t * Type.t) list;
		formal_fv : (Id.t * Type.t) list;
		body : t }
(* グローバル変数（場所が固定されている） *)
type global = { pos : Syntax.pos_t;
                name: Id.l * Type.t;
                size: int }
(* 関数定義が必要なネイティブ関数 *)
type native = Id.t * (Id.l * Type.t)
(* fundef list: 先に定義されたものほど前にある *)
type prog = Prog of fundef list * global list * native list * t

let rec fv = function
  | Unit(_) | Int(_) | Float(_) | ExtArray(_) | ExtTuple(_) -> S.empty
  | Neg(_,x) | FNeg(_,x) | Array(_,(_,_,x)) | GlobalArray(_,(_,_,x))-> S.singleton x
  | Add(_,(x, y)) | Sub(_,(x, y)) | Mul(_,(x, y)) | Div(_,(x, y)) | FAdd(_,(x, y)) | FSub(_,(x, y)) | FMul(_,(x, y)) | FDiv(_,(x, y))
  | CmpEq(_,(x, y)) | CmpLt(_,(x, y)) | Get(_,(x, y)) -> S.of_list [x; y]
  | If(_,_,(x, e1, e2)) -> S.add x (S.union (fv e1) (fv e2))
  | Let(_,((x, t), e1, e2)) -> S.union (fv e1) (S.remove x (fv e2))
  | Var(_,x) -> S.singleton x
  | MakeCls(_,((x, t), { entry = l; actual_fv = ys }, e)) -> S.remove x (S.union (S.of_list ys) (fv e))
  | AppCls(_,(x, ys)) -> S.of_list (x :: ys)
  | AppDir(_,(_, xs)) | Tuple(_,xs) | GlobalTuple(_,(_,xs)) 
  | AppNative(_,(_,xs)) -> S.of_list xs
  | LetTuple(_,(xts, y, e)) -> S.add y (S.diff (fv e) (S.of_list (List.map fst xts)))
  | Put(_,(x, y, z)) -> S.of_list [x; y; z]

let toplevel : fundef list ref = ref []
let globals : global list ref = ref []
let natives : native list ref = ref []

let rec join gl l =
  match l with
    | [] -> ""
    | x::[] -> x
    | x::l' -> x ^ gl ^ join gl l'

let rec g env known known_native = function (* クロージャ変換ルーチン本体 (caml2html: closure_g) *)
  (* env: 型環境
   * known: トップレベル関数の集合
   * known_native: nativeであるとわかっている変数の環境 *)
  | KNormal.Unit(p) -> Unit(p)
  | KNormal.Int(p,i) -> Int(p,i)
  | KNormal.Float(p,d) -> Float(p,d)
  | KNormal.Neg(p,x) -> Neg(p,x)
  | KNormal.Add(p,(x, y)) -> Add(p,(x, y))
  | KNormal.Sub(p,(x, y)) -> Sub(p,(x, y))
  | KNormal.Mul(p,(x, y)) -> Mul(p,(x, y))
  | KNormal.Div(p,(x, y)) -> Div(p,(x, y))
  | KNormal.FNeg(p,x) -> FNeg(p,x)
  | KNormal.FAdd(p,(x, y)) -> FAdd(p,(x, y))
  | KNormal.FSub(p,(x, y)) -> FSub(p,(x, y))
  | KNormal.FMul(p,(x, y)) -> FMul(p,(x, y))
  | KNormal.FDiv(p,(x, y)) -> FDiv(p,(x, y))
  | KNormal.CmpEq(p,(x, y)) -> CmpEq(p,(x, y))
  | KNormal.CmpLt(p,(x, y)) -> CmpLt(p,(x, y))
  | KNormal.If(p1,p2,(x, e1, e2)) -> If(p1,p2,(x, g env known known_native e1, g env known known_native e2))
  | KNormal.Let(p,ls,((x, t), e1, e2)) when SLS.mem SLS.Exported ls ->
      (* exportできるのは配列かタプルに限られていた *)
      (match e1 with
        | KNormal.Array(p2, (i, t2, y)) ->
            (* この配列はグローバルに作る *)
            globals := {
              pos = p2;
              name = (Id.L(x), t);
              size = i
            } :: !globals;
            Let(p, ((x, t), GlobalArray(p, (Id.L(x), i, y)), g env known known_native e2))
        | KNormal.Tuple(p2, xs) ->
            globals := {
              pos = p2;
              name = (Id.L(x), t);
              size = List.length xs
            } :: !globals;
            Let(p, ((x, t), GlobalTuple(p, (Id.L(x), xs)), g env known known_native e2))
        | _ -> assert false)
  | KNormal.Let(p,ls,((x, t), e1, e2)) ->
      (match e1 with
         | KNormal.Native(_, (id, ty)) ->
             (* これはnativeですね…… *)
             let known_native' = M.add x (id, ty) known_native in
             let e1' = g env known known_native e1 in
             let e2' = g (M.add x t env) known known_native' e2 in
               if S.mem x (fv e2') then (* クロージャを作って使う必要があるか *)
                 Let(p,((x, t), e1', e2'))
               else
                 e2'
         | _ ->
             (* ふつうに処理 *)
             Let(p,((x, t), g env known known_native e1, g (M.add x t env) known known_native e2)))
  | KNormal.Var(p,x) -> Var(p,x)
  | KNormal.LetRec(p,ls, ({ KNormal.name = (x, t); KNormal.args = yts; KNormal.body = e1 }, e2)) when not (SLS.mem SLS.Exported ls) -> (* 関数定義の場合 (caml2html: closure_letrec) *)
      (* 関数定義let rec x y1 ... yn = e1 in e2の場合は、
	 xに自由変数がない(closureを介さずdirectに呼び出せる)
	 と仮定し、knownに追加してe1をクロージャ変換してみる *)
      let toplevel_backup = !toplevel in
      let env' = M.add x t env in
      let known' = S.add x known in
      let e1' = g (M.add_list yts env') known' known_native e1 in
      (* 本当に自由変数がなかったか、変換結果e1'を確認する *)
      (* 注意: e1'にx自身が変数として出現する場合はclosureが必要!
         (thanks to nuevo-namasute and azounoman; test/cls-bug2.ml参照) *)
      let zs = S.diff (fv e1') (S.of_list (List.map fst yts)) in
      let known', e1' =
        if S.is_empty zs then known', e1' else
          (* 駄目だったら状態(toplevelの値)を戻して、クロージャ変換をやり直す *)
          (Format.eprintf "free variable(s) %s found in function %s@." (Id.pp_list (S.elements zs)) x;
           Format.eprintf "function %s cannot be directly applied in fact@." x;
           toplevel := toplevel_backup;
           let e1' = g (M.add_list yts env') known known_native e1 in
             known, e1') in
      let zs = S.elements (S.diff (fv e1') (S.add x (S.of_list (List.map fst yts)))) in (* 自由変数のリスト *)
      let zts = List.map (fun z -> (z, M.find z env')) zs in (* ここで自由変数zの型を引くために引数envが必要 *)
        toplevel := { pos = p; name = (Id.L(x), t); args = yts; formal_fv = zts; body = e1' } :: !toplevel; (* トップレベル関数を追加 *)
        let e2' = g env' known' known_native e2 in
          if S.mem x (fv e2') then (* xが変数としてe2'に出現するか *)
            MakeCls(p,((x, t), { entry = Id.L(x); actual_fv = zs }, e2')) (* 出現していたら削除しない *)
          else
              (Format.eprintf "eliminating closure(s) %s@." x;
               e2') (* 出現しなければMakeClsを削除 *)
  | KNormal.LetRec(p,ls,({ KNormal.name = (x, t); KNormal.args = yts; KNormal.body = e1 }, e2)) ->
      assert (SLS.mem SLS.Exported ls);
      (* 外部に出るやつなので自由変数があってはいけない！！！！！！！！！ *)
      let env' = M.add x t env in
      let known' = S.add x known in
      let e1' = g (M.add_list yts env') known' known_native e1 in
      (* 自由変数があったらエラーにする *)
      let zs = S.diff (fv e1') (S.of_list (List.map fst yts)) in
      (if not(S.is_empty zs) then
         let fvs = join ", " (S.elements zs) in
         failwith (Format.sprintf "exported function %s has free variable(s): %s" x fvs)
       else ());
        toplevel := { pos = p; name = (Id.L(x), t); args = yts; formal_fv = []; body = e1' } :: !toplevel; (* トップレベル関数を追加 *)
        g env' known' known_native e2
  | KNormal.App(p,(x, ys)) when S.mem x known -> (* 関数適用の場合 (caml2html: closure_app) *)
      Format.eprintf "directly applying %s@." x;
      AppDir(p,(Id.L(x), ys))
  | KNormal.App(p,(x, ys)) when M.mem x known_native -> 
      (* nativeを適用するようだ *)
      AppNative(p, (M.find x known_native, ys))
  | KNormal.App(p,(f, xs)) -> AppCls(p,(f, xs))
  | KNormal.Tuple(p,xs) -> Tuple(p,xs)
  | KNormal.LetTuple(p,(xts, y, e)) -> LetTuple(p,(xts, y, g (M.add_list xts env) known known_native e))
  | KNormal.Array(p, (i, t, x)) ->
      (* 長さのきまった配列をつくる *)
      Array(p, (i, t, x))
  | KNormal.Get(p,(x, y)) -> Get(p,(x, y))
  | KNormal.Put(p,(x, y, z)) -> Put(p,(x, y, z))
  | KNormal.ExtArray(p,x) -> ExtArray(p,(Id.L(x)))
  | KNormal.ExtTuple(p,x) -> ExtTuple(p,(Id.L(x)))
  | KNormal.ExtFunApp(p,(x, ys)) -> AppDir(p,(Id.L("min_caml_" ^ x), ys))
  | KNormal.Native(p, (id, t)) ->
      (* 基本はクロージャを作る *)
      let (clid, _) = try List.assoc id !natives
      with Not_found -> (let clid = Id.L(Id.genid("min_caml_native_" ^ id)) in
                           natives := (id, (clid, t)) :: !natives;
                           (clid, t)) in
      let cls = {
        entry = clid;
        actual_fv = []
      } in
        MakeCls(p,((id, t), cls, Unit(p)))
        

let f e =
  toplevel := [];
  globals := [];
  natives := [];
  let e' = g M.empty S.empty M.empty e in
  Prog(List.rev !toplevel, List.rev !globals, List.rev !natives, e')

(* week1-q1: tをきれいに表示 *)
let tree p =
  let f = function
    | Unit _ -> ("UNIT", [])
    | Int(_,v) -> ("INT " ^ string_of_int v, [])
    | Float(_,v) -> ("FLOAT " ^ string_of_float v, [])
    | Neg(_,id) -> ("NEG " ^ id, [])
    | Add(_,(id1,id2)) -> ("ADD " ^ id1 ^ " " ^ id2, [])
    | Sub(_,(id1,id2)) -> ("SUB " ^ id1 ^ " " ^ id2, [])
    | Mul(_,(id1,id2)) -> ("MUL " ^ id1 ^ " " ^ id2, [])
    | Div(_,(id1,id2)) -> ("DIV " ^ id1 ^ " " ^ id2, [])
    | FNeg(_,id) -> ("FNEG " ^ id, [])
    | FAdd(_,(id1,id2)) -> ("FSUB " ^ id1 ^ " " ^ id2, [])
    | FSub(_,(id1,id2)) -> ("FSUB " ^ id1 ^ " " ^ id2, [])
    | FMul(_,(id1,id2)) -> ("FMUL " ^ id1 ^ " " ^ id2, [])
    | FDiv(_,(id1,id2)) -> ("FDIV " ^ id1 ^ " " ^ id2, [])
    | CmpEq(_,(id1,id2)) -> ("CMPEQ " ^ id1 ^ " " ^ id2, [])
    | CmpLt(_,(id1,id2)) -> ("CMPLT " ^ id1 ^ " " ^ id2, [])
    | If(_,_,(id1,e1,e2)) -> ("IF " ^ id1, [e1;e2])
    | Let(_,((id,_),e1,e2)) -> ("LET " ^ id, [e1;e2])
    | Var(_,id) -> ("VAR " ^ id, [])
    | MakeCls(_,((id,_), {entry = Id.L n; actual_fv = ids}, e)) -> ("MAKECLS " ^ id ^ " (entry = L(" ^ n ^ "); fv = " ^ join ", " ids ^ ")", [e])
    | AppCls(_,(id,ids)) -> ("APPCLS " ^ id ^ " " ^ join " " ids, [])
    | AppDir(_,(Id.L(id),ids)) -> ("APPDIR L(" ^ id ^ ") " ^ join " " ids, [])
    | Tuple(_,ids) -> ("TUPLE (" ^ " " ^ join ", " ids ^ ")",  [])
    | LetTuple(_,(ids,id,e)) -> ("LETTUPLE (" ^ join ", " (List.map fst ids) ^ ") = " ^ id, [e])
    | Get(_,(id1,id2)) -> ("GET " ^ id1 ^ " " ^ id2,[])
    | Put(_,(id1,id2,id3)) -> ("PUT " ^ id1 ^ " " ^ id2 ^ " " ^ id3,[])
    | ExtArray(_,Id.L(id)) -> ("EXTARRAY L(" ^ id ^ ")",[])
    | ExtTuple(_,Id.L(id)) -> ("EXTTUPLE L(" ^ id ^ ")",[])
    | Array(_,(i, _, id)) -> ("ARRAY " ^ id ^ " " ^ string_of_int i, [])
    | GlobalArray(_,(Id.L(id), i, id2)) -> ("GLOBALARRAY L(" ^ id ^ ") " ^ string_of_int i ^ " " ^ id2, [])
    | GlobalTuple(_,(Id.L(id), ids)) -> ("GLOBALTUPLE L(" ^ id ^ ") " ^ join ", " ids, [])
    | AppNative(_,((id, _),ids)) -> ("APPNATIVE " ^ id ^ " " ^ join " " ids, [])
  in
  let g = function
    | Unit p | Int(p,_) | Float(p,_) | Neg(p,_) | Add(p,_) | Sub (p,_) | Mul(p,_) | Div(p,_)
    | FNeg(p,_) | FAdd(p,_) | FSub(p,_) | FMul(p,_) | FDiv(p,_)
    | CmpEq(p,_) | CmpLt(p,_)
    | If(p,_,_) | Let(p,_)  | Var(p,_)
    | MakeCls(p,_) | AppCls(p,_) | AppDir(p,_) | Tuple(p,_) | LetTuple(p,_) | Get(p,_) | Put(p,_) 
    | ExtArray(p,_) | ExtTuple(p,_)
    | Array(p,_)
    | GlobalArray(p,_) | GlobalTuple(p,_) 
    | AppNative(p,_) -> Syntax.pos_str p
  in
  let Prog(fs,gs,ns,e) = p in
  let buf = Buffer.create 1024 in
    List.iter (fun (fd : fundef) ->
                 let (Id.L(id),_) = fd.name in
                   Buffer.add_string buf ("  FUNC L("^id^") " ^ join " " (List.map fst fd.args) ^ "\n");
                   Buffer.add_string buf ((Tree.make f g "    " fd.body))) fs;
    Buffer.add_string buf (Tree.make f g "  " e);
    Buffer.contents buf
