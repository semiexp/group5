type t = 
  | Unit of Syntax.pos_t
  | Int of Syntax.pos_t * int
  | Float of Syntax.pos_t * float
  | Neg of Syntax.pos_t * Id.t
  | Add of Syntax.pos_t * (Id.t * Id.t)
  | Sub of Syntax.pos_t * (Id.t * Id.t)
  | Mul of Syntax.pos_t * (Id.t * Id.t)
  | Div of Syntax.pos_t * (Id.t * Id.t)
  | FNeg of Syntax.pos_t * Id.t
  | FAdd of Syntax.pos_t * (Id.t * Id.t)
  | FSub of Syntax.pos_t * (Id.t * Id.t)
  | FMul of Syntax.pos_t * (Id.t * Id.t)
  | FDiv of Syntax.pos_t * (Id.t * Id.t)
  | CmpEq of Syntax.pos_t * (Id.t * Id.t)
  | CmpLt of Syntax.pos_t * (Id.t * Id.t)
  | If of Syntax.pos_t * Syntax.pos_t * (Id.t * t * t)
  | Let of Syntax.pos_t * SLS.t * ((Id.t * Type.t) * t * t)
  | Var of Syntax.pos_t * Id.t
  | LetRec of Syntax.pos_t * SLS.t * (fundef * t)
  | App of Syntax.pos_t * (Id.t * Id.t list)
  | Tuple of Syntax.pos_t * Id.t list
  | LetTuple of Syntax.pos_t * ((Id.t * Type.t) list * Id.t * t)
  | Array of Syntax.pos_t * (int * Type.t * Id.t)
  | Get of Syntax.pos_t * (Id.t * Id.t)
  | Put of Syntax.pos_t * (Id.t * Id.t * Id.t)
  | ExtArray of Syntax.pos_t * Id.t
  | ExtTuple of Syntax.pos_t * Id.t
  | ExtFunApp of Syntax.pos_t * (Id.t * Id.t list)
  | Native of Syntax.pos_t * (Id.t * Type.t)
and fundef = { name : Id.t * Type.t; args : (Id.t * Type.t) list; body : t }

val fv : t -> S.t
val f : Syntax.t -> t
val join : string -> string list -> string
val tree : t -> string
