(* translation into assembly with infinite number of virtual registers *)

open Asm

let data = ref [] (* 浮動小数点数の定数テーブル (caml2html: virtual_data) *)

let classify xts ini addf addi =
  List.fold_left
    (fun acc (x, t) ->
      match t with
      | Type.Unit -> acc
      | Type.Float -> addf acc x
      | _ -> addi acc x t)
    ini
    xts

let separate xts =
  classify
    xts
    ([], [])
    (fun (int, float) x -> (int, float @ [x]))
    (fun (int, float) x _ -> (int @ [x], float))

let expand xts ini addf addi =
  classify
    xts
    ini
    (fun (offset, acc) x ->
      let offset = align offset in
      (offset + 8, addf x offset acc))
    (fun (offset, acc) x t ->
      (offset + 4, addi x t offset acc))

let rec g env = function (* 式の仮想マシンコード生成 (caml2html: virtual_g) *)
  | Closure.Unit(_) -> Ans(Nop)
  | Closure.Int(p,i) -> Ans(Set(p,i))
  | Closure.Float(p,d) ->
      let l =
        try
          (* すでに定数テーブルにあったら再利用 *)
          let (l, _) = List.find (fun (_, d') -> d = d') !data in
            l
        with Not_found ->
          let l = Id.L(Id.genid "l") in
            data := (l, d) :: !data;
            l in
      let x = Id.genid "l" in
        Let((x, Type.Int), SetL(p,l), Ans(LdDF(p,(x, C(0), 1))))
  | Closure.Neg(p,x) -> Ans(Neg(p,x))
  | Closure.Add(p,(x, y)) -> Ans(Add(p,(x, V(y))))
  | Closure.Sub(p,(x, y)) -> Ans(Sub(p,(x, V(y))))
  (* XXX もともとmin-camlにないんで *)
  | Closure.Mul(p,(x, y)) -> failwith "MUL is not supported for x86"
  | Closure.Div(p,(x, y)) -> failwith "DIV is not supported for x86"
  | Closure.FNeg(p,x) -> Ans(FNegD(p,x))
  | Closure.FAdd(p,(x, y)) -> Ans(FAddD(p,(x, y)))
  | Closure.FSub(p,(x, y)) -> Ans(FSubD(p,(x, y)))
  | Closure.FMul(p,(x, y)) -> Ans(FMulD(p,(x, y)))
  | Closure.FDiv(p,(x, y)) -> Ans(FDivD(p,(x, y)))
  | Closure.IfEq(p1,p2,(x, y, e1, e2)) ->
      (match M.find x env with
      | Type.Bool | Type.Int -> Ans(IfEq(p1,p2,(x, V(y), g env e1, g env e2)))
      | Type.Float -> Ans(IfFEq(p1,p2,(x, y, g env e1, g env e2)))
      | _ -> failwith "equality supported only for bool, int, and float")
  | Closure.IfLE(p1,p2,(x, y, e1, e2)) ->
      (match M.find x env with
      | Type.Bool | Type.Int -> Ans(IfLE(p1,p2,(x, V(y), g env e1, g env e2)))
      | Type.Float -> Ans(IfFLE(p1,p2,(x, y, g env e1, g env e2)))
      | _ -> failwith "inequality supported only for bool, int, and float")
  | Closure.Let(_,((x, t1), e1, e2)) ->
      let e1' = g env e1 in
      let e2' = g (M.add x t1 env) e2 in
      concat e1' (x, t1) e2'
  | Closure.Var(p,x) ->
      (match M.find x env with
      | Type.Unit -> Ans(Nop)
      | Type.Float -> Ans(FMovD(p,x))
      | _ -> Ans(Mov(p,x)))
  | Closure.MakeCls(p,((x, t), { Closure.entry = l; Closure.actual_fv = ys }, e2)) -> (* クロージャの生成 (caml2html: virtual_makecls) *)
      (* Closureのアドレスをセットしてから、自由変数の値をストア *)
      let e2' = g (M.add x t env) e2 in
      let offset, store_fv =
        expand
          (List.map (fun y -> (y, M.find y env)) ys)
          (4, e2')
          (fun y offset store_fv -> seq(StDF(p,(y, x, C(offset), 1)), store_fv))
          (fun y _ offset store_fv -> seq(St(p,(y, x, C(offset), 1)), store_fv)) in
        Let((x, t), Mov(p,reg_hp),
            Let((reg_hp, Type.Int), Add(p,(reg_hp, C(align offset))),
                let z = Id.genid "l" in
                  Let((z, Type.Int), SetL(p,l),
                      seq(St(p,(z, x, C(0), 1)),
                          store_fv))))
  | Closure.AppCls(p,(x, ys)) ->
      let (int, float) = separate (List.map (fun y -> (y, M.find y env)) ys) in
      Ans(CallCls(p,(x, int, float)))
  | Closure.AppDir(p,(Id.L(x), ys)) ->
      let (int, float) = separate (List.map (fun y -> (y, M.find y env)) ys) in
      Ans(CallDir(p,(Id.L(x), int, float)))
  | Closure.Tuple(p,xs) -> (* 組の生成 (caml2html: virtual_tuple) *)
      let y = Id.genid "t" in
      let (offset, store) =
        expand
          (List.map (fun x -> (x, M.find x env)) xs)
          (0, Ans(Mov(p,y)))
          (fun x offset store -> seq(StDF(p,(x, y, C(offset), 1)), store))
          (fun x _ offset store -> seq(St(p,(x, y, C(offset), 1)), store)) in
        Let((y, Type.Tuple(List.map (fun x -> M.find x env) xs)), Mov(p,reg_hp),
            Let((reg_hp, Type.Int), Add(p,(reg_hp, C(align offset))),
                store))
  | Closure.LetTuple(p,(xts, y, e2)) ->
      let s = Closure.fv e2 in
      let (offset, load) =
        expand
          xts
          (0, g (M.add_list xts env) e2)
          (fun x offset load ->
             if not (S.mem x s) then load else (* [XX] a little ad hoc optimization *)
               fletd(x, LdDF(p,(y, C(offset), 1)), load))
          (fun x t offset load ->
             if not (S.mem x s) then load else (* [XX] a little ad hoc optimization *)
               Let((x, t), Ld(p,(y, C(offset), 1)), load)) in
        load
  | Closure.Get(p,(x, y)) -> (* 配列の読み出し (caml2html: virtual_get) *)
      (match M.find x env with
      | Type.Array(Type.Unit) -> Ans(Nop)
      | Type.Array(Type.Float) -> Ans(LdDF(p,(x, V(y), 8)))
      | Type.Array(_) -> Ans(Ld(p,(x, V(y), 4)))
      | _ -> assert false)
  | Closure.Put(p,(x, y, z)) ->
      (match M.find x env with
      | Type.Array(Type.Unit) -> Ans(Nop)
      | Type.Array(Type.Float) -> Ans(StDF(p,(z, x, V(y), 8)))
      | Type.Array(_) -> Ans(St(p,(z, x, V(y), 4)))
      | _ -> assert false)
  | Closure.ExtArray(p,Id.L(x)) -> Ans(SetL(p,Id.L("min_caml_" ^ x)))
  | Closure.GlobalArray(_) -> failwith "GlobalArray is not supported for x86"
  | Closure.GlobalTuple(_) -> failwith "GlobalTuple is not supported for x86"

(* 関数の仮想マシンコード生成 (caml2html: virtual_h) *)
let h { Closure.pos = p; Closure.name = (Id.L(x), t); Closure.args = yts; Closure.formal_fv = zts; Closure.body = e } =
  let (int, float) = separate yts in
  let (offset, load) =
    expand
      zts
      (4, g (M.add x t (M.add_list yts (M.add_list zts M.empty))) e)
      (fun z offset load -> fletd(z, LdDF(p,(reg_cl, C(offset), 1)), load))
      (fun z t offset load -> Let((z, t), Ld(p,(reg_cl, C(offset), 1)), load)) in
    match t with
      | Type.Fun(_, t2) ->
          { name = Id.L(x); args = int; fargs = float; body = load; ret = t2 }
      | _ -> assert false

(* プログラム全体の仮想マシンコード生成 (caml2html: virtual_f) *)
let f (Closure.Prog(fundefs, e)) =
  data := [];
  let fundefs = List.map h fundefs in
  let e = g M.empty e in
  Prog(!data, fundefs, e)
