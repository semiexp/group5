open Asm

external gethi : float -> int32 = "gethi"
external getlo : float -> int32 = "getlo"

(* Syntax.pos_tをコメント文字列にする *)
let posstr (p : Syntax.pos_t) =
  if p.Syntax.s.Syntax.line < 0 then
    (* it's dummy *)
    ""
  else
      Printf.sprintf "# %s" (Syntax.pos_str p)



let stackset = ref S.empty (* すでにSaveされた変数の集合 (caml2html: emit_stackset) *)
let stackmap = ref [] (* Saveされた変数の、スタックにおける位置 (caml2html: emit_stackmap) *)
let save x =
  stackset := S.add x !stackset;
  if not (List.mem x !stackmap) then
    stackmap := !stackmap @ [x]
let savef x =
  stackset := S.add x !stackset;
  if not (List.mem x !stackmap) then
    (let pad =
      if List.length !stackmap mod 2 = 0 then [] else [Id.gentmp Type.Int] in
    stackmap := !stackmap @ pad @ [x; x])
let locate x =
  let rec loc = function
    | [] -> []
    | y :: zs when x = y -> 0 :: List.map succ (loc zs)
    | y :: zs -> List.map succ (loc zs) in
  loc !stackmap
let offset x = 4 * List.hd (locate x)
let stacksize () = align (List.length !stackmap * 4)

let pp_id_or_imm = function
  | V(x) -> x
  | C(i) -> "$" ^ string_of_int i

(* 関数呼び出しのために引数を並べ替える(register shuffling) (caml2html: emit_shuffle) *)
let rec shuffle sw xys =
  (* remove identical moves *)
  let _, xys = List.partition (fun (x, y) -> x = y) xys in
  (* find acyclic moves *)
  match List.partition (fun (_, y) -> List.mem_assoc y xys) xys with
  | [], [] -> []
  | (x, y) :: xys, [] -> (* no acyclic moves; resolve a cyclic move *)
      (y, sw) :: (x, y) :: shuffle sw (List.map
					 (function
					   | (y', z) when y = y' -> (sw, z)
					   | yz -> yz)
					 xys)
  | xys, acyc -> acyc @ shuffle sw xys

type dest = Tail | NonTail of Id.t (* 末尾かどうかを表すデータ型 (caml2html: emit_dest) *)
let rec g oc = function (* 命令列のアセンブリ生成 (caml2html: emit_g) *)
  | dest, Ans(exp) -> g' oc (dest, exp)
  | dest, Let((x, t), exp, e) ->
      g' oc (NonTail(x), exp);
      g oc (dest, e)
and g' oc = function (* 各命令のアセンブリ生成 (caml2html: emit_gprime) *)
  (* 末尾でなかったら計算結果をdestにセット (caml2html: emit_nontail) *)
  | NonTail(_), Nop -> ()
  | NonTail(x), Set(p,i) -> Printf.fprintf oc "\tmovl\t$%d, %s\t%s\n" i x (posstr p)
  | NonTail(x), SetL(p,Id.L(y)) -> Printf.fprintf oc "\tmovl\t$%s, %s\t%s\n" y x (posstr p)
  | NonTail(x), Mov(p,y) ->
      if x <> y then Printf.fprintf oc "\tmovl\t%s, %s\t%s\n" y x (posstr p)
  | NonTail(x), Neg(p,y) ->
      if x <> y then Printf.fprintf oc "\tmovl\t%s, %s\t%s\n" y x (posstr p);
      Printf.fprintf oc "\tnegl\t%s\t%s\n" x (posstr p)
  | NonTail(x), Add(p,(y, z')) ->
      if V(x) = z' then
        Printf.fprintf oc "\taddl\t%s, %s\t%s\n" y x (posstr p)
      else
        (if x <> y then Printf.fprintf oc "\tmovl\t%s, %s\t%s\n" y x (posstr p);
         Printf.fprintf oc "\taddl\t%s, %s\t%s\n" (pp_id_or_imm z') x (posstr p))
  | NonTail(x), Sub(p,(y, z')) ->
      if V(x) = z' then
        (Printf.fprintf oc "\tsubl\t%s, %s\t%s\n" y x (posstr p);
         Printf.fprintf oc "\tnegl\t%s\t%s\n" x (posstr p))
      else
        (if x <> y then Printf.fprintf oc "\tmovl\t%s, %s\t%s\n" y x (posstr p);
         Printf.fprintf oc "\tsubl\t%s, %s\t%s\n" (pp_id_or_imm z') x (posstr p))
  | NonTail(x), Ld(p,(y, V(z), i)) -> Printf.fprintf oc "\tmovl\t(%s,%s,%d), %s\t%s\n" y z i x (posstr p)
  | NonTail(x), Ld(p,(y, C(j), i)) -> Printf.fprintf oc "\tmovl\t%d(%s), %s\t%s\n" (j * i) y x (posstr p)
  | NonTail(_), St(p,(x, y, V(z), i)) -> Printf.fprintf oc "\tmovl\t%s, (%s,%s,%d)\t%s\n" x y z i (posstr p)
  | NonTail(_), St(p,(x, y, C(j), i)) -> Printf.fprintf oc "\tmovl\t%s, %d(%s)\t%s\n" x (j * i) y (posstr p)
  | NonTail(x), FMovD(p,y) ->
      if x <> y then Printf.fprintf oc "\tmovsd\t%s, %s\t%s\n" y x (posstr p)
  | NonTail(x), FNegD(p,y) ->
      if x <> y then Printf.fprintf oc "\tmovsd\t%s, %s\t%s\n" y x (posstr p);
      Printf.fprintf oc "\txorpd\tmin_caml_fnegd, %s\t%s\n" x (posstr p)
  | NonTail(x), FAddD(p,(y, z)) ->
      if x = z then
        Printf.fprintf oc "\taddsd\t%s, %s\t%s\n" y x (posstr p)
      else
        (if x <> y then Printf.fprintf oc "\tmovsd\t%s, %s\t%s\n" y x (posstr p);
	 Printf.fprintf oc "\taddsd\t%s, %s\t%s\n" z x (posstr p))
  | NonTail(x), FSubD(p,(y, z)) ->
      if x = z then (* [XXX] ugly *)
        let ss = stacksize () in
          Printf.fprintf oc "\tmovsd\t%s, %d(%s)\t%s\n" z ss reg_sp (posstr p);
          if x <> y then Printf.fprintf oc "\tmovsd\t%s, %s\t%s\n" y x (posstr p);
          Printf.fprintf oc "\tsubsd\t%d(%s), %s\t%s\n" ss reg_sp x (posstr p)
          else
            (if x <> y then Printf.fprintf oc "\tmovsd\t%s, %s\t%s\n" y x (posstr p);
             Printf.fprintf oc "\tsubsd\t%s, %s\t%s\n" z x (posstr p))
  | NonTail(x), FMulD(p,(y, z)) ->
      if x = z then
        Printf.fprintf oc "\tmulsd\t%s, %s\t%s\n" y x (posstr p)
      else
        (if x <> y then Printf.fprintf oc "\tmovsd\t%s, %s\t%s\n" y x (posstr p);
	 Printf.fprintf oc "\tmulsd\t%s, %s\t%s\n" z x (posstr p))
  | NonTail(x), FDivD(p,(y, z)) ->
      if x = z then (* [XXX] ugly *)
        let ss = stacksize () in
          Printf.fprintf oc "\tmovsd\t%s, %d(%s)\t%s\n" z ss reg_sp (posstr p);
          if x <> y then Printf.fprintf oc "\tmovsd\t%s, %s\t%s\n" y x (posstr p);
          Printf.fprintf oc "\tdivsd\t%d(%s), %s\t%s\n" ss reg_sp x (posstr p)
          else
            (if x <> y then Printf.fprintf oc "\tmovsd\t%s, %s\t%s\n" y x (posstr p);
             Printf.fprintf oc "\tdivsd\t%s, %s\t%s\n" z x (posstr p))
  | NonTail(x), LdDF(p,(y, V(z), i)) -> Printf.fprintf oc "\tmovsd\t(%s,%s,%d), %s\t%s\n" y z i x (posstr p)
  | NonTail(x), LdDF(p,(y, C(j), i)) -> Printf.fprintf oc "\tmovsd\t%d(%s), %s\t%s\n" (j * i) y x (posstr p)
  | NonTail(_), StDF(p,(x, y, V(z), i)) -> Printf.fprintf oc "\tmovsd\t%s, (%s,%s,%d)\t%s\n" x y z i (posstr p)
  | NonTail(_), StDF(p,(x, y, C(j), i)) -> Printf.fprintf oc "\tmovsd\t%s, %d(%s)\t%s\n" x (j * i) y (posstr p)
  | NonTail(_), Comment(p,s) -> Printf.fprintf oc "\t# %s\t%s\n" s (posstr p)
  (* 退避の仮想命令の実装 (caml2html: emit_save) *)
  | NonTail(_), Save(p,(x, y)) when List.mem x allregs && not (S.mem y !stackset) ->
      save y;
      Printf.fprintf oc "\tmovl\t%s, %d(%s)\t%s\n" x (offset y) reg_sp (posstr p)
  | NonTail(_), Save(p,(x, y)) when List.mem x allfregs && not (S.mem y !stackset) ->
      savef y;
      Printf.fprintf oc "\tmovsd\t%s, %d(%s)\t%s\n" x (offset y) reg_sp (posstr p)
  | NonTail(_), Save(p,(x, y)) -> assert (S.mem y !stackset); ()
  (* 復帰の仮想命令の実装 (caml2html: emit_restore) *)
  | NonTail(x), Restore(p,y) when List.mem x allregs ->
      Printf.fprintf oc "\tmovl\t%d(%s), %s\t%s\n" (offset y) reg_sp x (posstr p)
  | NonTail(x), Restore(p,y) ->
      assert (List.mem x allfregs);
      Printf.fprintf oc "\tmovsd\t%d(%s), %s\t%s\n" (offset y) reg_sp x (posstr p)
  (* 末尾だったら計算結果を第一レジスタにセットしてret (caml2html: emit_tailret) *)
  | Tail, (Nop | St _ | StDF _ | Comment _ | Save _ as exp) ->
      g' oc (NonTail(Id.gentmp Type.Unit), exp);
      Printf.fprintf oc "\tret\n";
  | Tail, (Set _ | SetL _ | Mov _ | Neg _ | Add _ | Sub _ | Ld _ as exp) ->
      g' oc (NonTail(regs.(0)), exp);
      Printf.fprintf oc "\tret\n";
  | Tail, (FMovD _ | FNegD _ | FAddD _ | FSubD _ | FMulD _ | FDivD _ | LdDF _  as exp) ->
      g' oc (NonTail(fregs.(0)), exp);
      Printf.fprintf oc "\tret\n";
  | Tail, (Restore(p,x) as exp) ->
      (match locate x with
      | [i] -> g' oc (NonTail(regs.(0)), exp)
      | [i; j] when i + 1 = j -> g' oc (NonTail(fregs.(0)), exp)
      | _ -> assert false);
      Printf.fprintf oc "\tret\n";
  | Tail, IfEq(p1,p2,(x, y', e1, e2)) ->
      Printf.fprintf oc "\tcmpl\t%s, %s\t%s\n" (pp_id_or_imm y') x (posstr p2);
      g'_tail_if oc p1 e1 e2 "je" "jne"
  | Tail, IfLE(p1,p2,(x, y', e1, e2)) ->
      Printf.fprintf oc "\tcmpl\t%s, %s\t%s\n" (pp_id_or_imm y') x (posstr p2);
      g'_tail_if oc p1 e1 e2 "jle" "jg"
  | Tail, IfGE(p1,p2,(x, y', e1, e2)) ->
      Printf.fprintf oc "\tcmpl\t%s, %s\t%s\n" (pp_id_or_imm y') x (posstr p2);
      g'_tail_if oc p1 e1 e2 "jge" "jl"
  | Tail, IfFEq(p1,p2,(x, y, e1, e2)) ->
      Printf.fprintf oc "\tcomisd\t%s, %s\t%s\n" y x (posstr p2);
      g'_tail_if oc p1 e1 e2 "je" "jne"
  | Tail, IfFLE(p1,p2,(x, y, e1, e2)) ->
      Printf.fprintf oc "\tcomisd\t%s, %s\t%s\n" y x (posstr p2);
      g'_tail_if oc p1 e1 e2 "jbe" "ja"
  | NonTail(z), IfEq(p1,p2,(x, y', e1, e2)) ->
      Printf.fprintf oc "\tcmpl\t%s, %s\t%s\n" (pp_id_or_imm y') x (posstr p2);
      g'_non_tail_if oc p1 (NonTail(z)) e1 e2 "je" "jne"
  | NonTail(z), IfLE(p1,p2,(x, y', e1, e2)) ->
      Printf.fprintf oc "\tcmpl\t%s, %s\t%s\n" (pp_id_or_imm y') x (posstr p2);
      g'_non_tail_if oc p1 (NonTail(z)) e1 e2 "jle" "jg"
  | NonTail(z), IfGE(p1,p2,(x, y', e1, e2)) ->
      Printf.fprintf oc "\tcmpl\t%s, %s\t%s\n" (pp_id_or_imm y') x (posstr p2);
      g'_non_tail_if oc p1 (NonTail(z)) e1 e2 "jge" "jl"
  | NonTail(z), IfFEq(p1,p2,(x, y, e1, e2)) ->
      Printf.fprintf oc "\tcomisd\t%s, %s\t%s\n" y x (posstr p2);
      g'_non_tail_if oc p1 (NonTail(z)) e1 e2 "je" "jne"
  | NonTail(z), IfFLE(p1,p2,(x, y, e1, e2)) ->
      Printf.fprintf oc "\tcomisd\t%s, %s\t%s\n" y x (posstr p2);
      g'_non_tail_if oc p1 (NonTail(z)) e1 e2 "jbe" "ja"
  (* 関数呼び出しの仮想命令の実装 (caml2html: emit_call) *)
  | Tail, CallCls(p,(x, ys, zs)) -> (* 末尾呼び出し (caml2html: emit_tailcall) *)
      g'_args oc [(x, reg_cl)] ys zs;
      Printf.fprintf oc "\tjmp\t*(%s)\t%s\n" reg_cl (posstr p);
  | Tail, CallDir(p,(Id.L(x), ys, zs)) -> (* 末尾呼び出し *)
      g'_args oc [] ys zs;
      Printf.fprintf oc "\tjmp\t%s\t%s\n" x (posstr p);
  | NonTail(a), CallCls(p,(x, ys, zs)) ->
      g'_args oc [(x, reg_cl)] ys zs;
      let ss = stacksize () in
      if ss > 0 then Printf.fprintf oc "\taddl\t$%d, %s\t%s\n" ss reg_sp (posstr p);
      Printf.fprintf oc "\tcall\t*(%s)\t%s\n" reg_cl (posstr p);
      if ss > 0 then Printf.fprintf oc "\tsubl\t$%d, %s\t%s\n" ss reg_sp (posstr p);
      if List.mem a allregs && a <> regs.(0) then
        Printf.fprintf oc "\tmovl\t%s, %s\t%s\n" regs.(0) a (posstr p)
      else if List.mem a allfregs && a <> fregs.(0) then
        Printf.fprintf oc "\tmovsd\t%s, %s\t%s\n" fregs.(0) a (posstr p)
  | NonTail(a), CallDir(p,(Id.L(x), ys, zs)) ->
      g'_args oc [] ys zs;
      let ss = stacksize () in
      if ss > 0 then Printf.fprintf oc "\taddl\t$%d, %s\t%s\n" ss reg_sp (posstr p);
      Printf.fprintf oc "\tcall\t%s\t%s\n" x (posstr p);
      if ss > 0 then Printf.fprintf oc "\tsubl\t$%d, %s\t%s\n" ss reg_sp (posstr p);
      if List.mem a allregs && a <> regs.(0) then
        Printf.fprintf oc "\tmovl\t%s, %s\t%s\n" regs.(0) a (posstr p)
      else if List.mem a allfregs && a <> fregs.(0) then
        Printf.fprintf oc "\tmovsd\t%s, %s\t%s\n" fregs.(0) a (posstr p)
and g'_tail_if oc p e1 e2 b bn =
  let b_else = Id.genid (b ^ "_else") in
  Printf.fprintf oc "\t%s\t%s\t%s\n" bn b_else (posstr p);
  let stackset_back = !stackset in
  g oc (Tail, e1);
  Printf.fprintf oc "%s:\t%s\n\n" b_else (posstr p);
  stackset := stackset_back;
  g oc (Tail, e2)
and g'_non_tail_if oc p dest e1 e2 b bn =
  let b_else = Id.genid (b ^ "_else") in
  let b_cont = Id.genid (b ^ "_cont") in
  Printf.fprintf oc "\t%s\t%s\t%s\n" bn b_else (posstr p);
  let stackset_back = !stackset in
  g oc (dest, e1);
  let stackset1 = !stackset in
  Printf.fprintf oc "\tjmp\t%s\t%s\n" b_cont (posstr p);
  Printf.fprintf oc "%s:\t%s\n" b_else (posstr p);
  stackset := stackset_back;
  g oc (dest, e2);
  Printf.fprintf oc "%s:\t%s\n" b_cont (posstr p);
  let stackset2 = !stackset in
  stackset := S.inter stackset1 stackset2
and g'_args oc x_reg_cl ys zs =
  assert (List.length ys <= Array.length regs - List.length x_reg_cl);
  assert (List.length zs <= Array.length fregs);
  let sw = Printf.sprintf "%d(%s)" (stacksize ()) reg_sp in
  let (i, yrs) =
    List.fold_left
      (fun (i, yrs) y -> (i + 1, (y, regs.(i)) :: yrs))
      (0, x_reg_cl)
      ys in
  List.iter
    (fun (y, r) -> Printf.fprintf oc "\tmovl\t%s, %s\n" y r)
    (shuffle sw yrs);
  let (d, zfrs) =
    List.fold_left
      (fun (d, zfrs) z -> (d + 1, (z, fregs.(d)) :: zfrs))
      (0, [])
      zs in
  List.iter
    (fun (z, fr) -> Printf.fprintf oc "\tmovsd\t%s, %s\n" z fr)
    (shuffle sw zfrs)

let h oc { name = Id.L(x); args = _; fargs = _; body = e; ret = _ } =
  Printf.fprintf oc "%s:\n" x;
  stackset := S.empty;
  stackmap := [];
  g oc (Tail, e)

let f oc (Prog(data, fundefs, e)) =
  Format.eprintf "generating assembly...@.";
  Printf.fprintf oc ".data\n";
  Printf.fprintf oc ".balign\t8\n";
  List.iter
    (fun (Id.L(x), d) ->
      Printf.fprintf oc "%s:\t# %f\n" x d;
      Printf.fprintf oc "\t.long\t0x%lx\n" (gethi d);
      Printf.fprintf oc "\t.long\t0x%lx\n" (getlo d))
    data;
  Printf.fprintf oc ".text\n";
  List.iter (fun fundef -> h oc fundef) fundefs;
  Printf.fprintf oc ".globl\tmin_caml_start\n";
  Printf.fprintf oc "min_caml_start:\n";
  Printf.fprintf oc ".globl\t_min_caml_start\n";
  Printf.fprintf oc "_min_caml_start: # for cygwin\n";
  Printf.fprintf oc "\tpushl\t%%eax\n";
  Printf.fprintf oc "\tpushl\t%%ebx\n";
  Printf.fprintf oc "\tpushl\t%%ecx\n";
  Printf.fprintf oc "\tpushl\t%%edx\n";
  Printf.fprintf oc "\tpushl\t%%esi\n";
  Printf.fprintf oc "\tpushl\t%%edi\n";
  Printf.fprintf oc "\tpushl\t%%ebp\n";
  Printf.fprintf oc "\tmovl\t32(%%esp),%s\n" reg_sp;
  Printf.fprintf oc "\tmovl\t36(%%esp),%s\n" regs.(0);
  Printf.fprintf oc "\tmovl\t%s,%s\n" regs.(0) reg_hp;
  stackset := S.empty;
  stackmap := [];
  g oc (NonTail(regs.(0)), e);
  Printf.fprintf oc "\tpopl\t%%ebp\n";
  Printf.fprintf oc "\tpopl\t%%edi\n";
  Printf.fprintf oc "\tpopl\t%%esi\n";
  Printf.fprintf oc "\tpopl\t%%edx\n";
  Printf.fprintf oc "\tpopl\t%%ecx\n";
  Printf.fprintf oc "\tpopl\t%%ebx\n";
  Printf.fprintf oc "\tpopl\t%%eax\n";
  Printf.fprintf oc "\tret\n";
