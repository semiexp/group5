type id_or_imm = V of Id.t | C of int
type t =
  | Ans of exp
  | Let of (Id.t * Type.t) * exp * t
and exp =
  | Nop
  | Set of Syntax.pos_t * int
  | SetL of Syntax.pos_t * Id.l
  | Mov of Syntax.pos_t * Id.t
  | Neg of Syntax.pos_t * Id.t
  | Add of Syntax.pos_t * (Id.t * id_or_imm)
  | Sub of Syntax.pos_t * (Id.t * id_or_imm)
  | Ld of Syntax.pos_t * (Id.t * id_or_imm * int)
  | St of Syntax.pos_t * (Id.t * Id.t * id_or_imm * int)
  | FMovD of Syntax.pos_t * Id.t
  | FNegD of Syntax.pos_t * Id.t
  | FAddD of Syntax.pos_t * (Id.t * Id.t)
  | FSubD of Syntax.pos_t * (Id.t * Id.t)
  | FMulD of Syntax.pos_t * (Id.t * Id.t)
  | FDivD of Syntax.pos_t * (Id.t * Id.t)
  | LdDF of Syntax.pos_t * (Id.t * id_or_imm * int)
  | StDF of Syntax.pos_t * (Id.t * Id.t * id_or_imm * int)
  | Comment of Syntax.pos_t * string
  (* virtual instructions *)
  | IfEq of Syntax.pos_t * Syntax.pos_t * (Id.t * id_or_imm * t * t)
  | IfLE of Syntax.pos_t * Syntax.pos_t * (Id.t * id_or_imm * t * t)
  | IfGE of Syntax.pos_t * Syntax.pos_t * (Id.t * id_or_imm * t * t) (* 左右対称ではないので必要 *)
  | IfFEq of Syntax.pos_t * Syntax.pos_t * (Id.t * Id.t * t * t)
  | IfFLE of Syntax.pos_t * Syntax.pos_t * (Id.t * Id.t * t * t)
  (* closure address, integer arguments, and float arguments *)
  | CallCls of Syntax.pos_t * (Id.t * Id.t list * Id.t list)
  | CallDir of Syntax.pos_t * (Id.l * Id.t list * Id.t list)
  | Save of Syntax.pos_t * (Id.t * Id.t) (* レジスタ変数の値をスタック変数へ保存 (caml2html: sparcasm_save) *)
  | Restore of Syntax.pos_t * Id.t (* スタック変数から値を復元 (caml2html: sparcasm_restore) *)
type fundef = { name : Id.l; args : Id.t list; fargs : Id.t list; body : t; ret : Type.t }
type prog = Prog of (Id.l * float) list * fundef list * t

val fletd : Id.t * exp * t -> t (* shorthand of Let for float *)
val seq : exp * t -> t (* shorthand of Let for unit *)

val regs : Id.t array
val fregs : Id.t array
val allregs : Id.t list
val allfregs : Id.t list
val reg_cl : Id.t
(*
val reg_sw : Id.t
val reg_fsw : Id.t
val reg_ra : Id.t
*)
val reg_hp : Id.t
val reg_sp : Id.t
val is_reg : Id.t -> bool

val fv : t -> Id.t list
val concat : t -> Id.t * Type.t -> t -> t

val align : int -> int

val tree: prog -> string
