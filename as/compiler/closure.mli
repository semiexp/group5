type closure = { entry : Id.l; actual_fv : Id.t list }
type t =
  | Unit of Syntax.pos_t
  | Int of Syntax.pos_t * int
  | Float of Syntax.pos_t * float
  | Neg of Syntax.pos_t * Id.t
  | Add of Syntax.pos_t * (Id.t * Id.t)
  | Sub of Syntax.pos_t * (Id.t * Id.t)
  | Mul of Syntax.pos_t * (Id.t * Id.t)
  | Div of Syntax.pos_t * (Id.t * Id.t)
  | FNeg of Syntax.pos_t * Id.t
  | FAdd of Syntax.pos_t * (Id.t * Id.t)
  | FSub of Syntax.pos_t * (Id.t * Id.t)
  | FMul of Syntax.pos_t * (Id.t * Id.t)
  | FDiv of Syntax.pos_t * (Id.t * Id.t)
  | CmpEq of Syntax.pos_t * (Id.t * Id.t)
  | CmpLt of Syntax.pos_t * (Id.t * Id.t)
  | If of Syntax.pos_t * Syntax.pos_t * (Id.t * t * t)
  | Let of Syntax.pos_t * ((Id.t * Type.t) * t * t)
  | Var of Syntax.pos_t * (Id.t)
  | MakeCls of Syntax.pos_t * ((Id.t * Type.t) * closure * t)
  | AppCls of Syntax.pos_t * (Id.t * Id.t list)
  | AppDir of Syntax.pos_t * (Id.l * Id.t list)
  | Tuple of Syntax.pos_t * (Id.t list)
  | LetTuple of Syntax.pos_t * ((Id.t * Type.t) list * Id.t * t)
  | Get of Syntax.pos_t * (Id.t * Id.t)
  | Put of Syntax.pos_t * (Id.t * Id.t * Id.t)
  | ExtArray of Syntax.pos_t * Id.l
  | ExtTuple of Syntax.pos_t * Id.l
  | Array of Syntax.pos_t * (int * Type.t * Id.t)
  | GlobalArray of Syntax.pos_t * (Id.l * int * Id.t)
  | GlobalTuple of Syntax.pos_t * (Id.l * Id.t list)
  | AppNative of Syntax.pos_t * ((Id.t * Type.t)  * Id.t list)
type fundef = { pos : Syntax.pos_t;
                name : Id.l * Type.t;
		args : (Id.t * Type.t) list;
		formal_fv : (Id.t * Type.t) list;
		body : t }
type global = { pos : Syntax.pos_t;
                name: Id.l * Type.t;
                size: int }
type native = Id.t * (Id.l * Type.t)
type prog = Prog of fundef list * global list * native list * t

val fv : t -> S.t
val f : KNormal.t -> prog
val tree: prog -> string
