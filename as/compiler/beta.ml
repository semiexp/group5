open KNormal

let find x env = try M.find x env with Not_found -> x (* 置換のための関数 (caml2html: beta_find) *)

let rec g env = function (* β簡約ルーチン本体 (caml2html: beta_g) *)
  | Unit(p) -> Unit(p)
  | Int(p,i) -> Int(p,i)
  | Float(p,d) -> Float(p,d)
  | Neg(p,x) -> Neg(p,find x env)
  | Add(p,(x, y)) -> Add(p,(find x env, find y env))
  | Sub(p,(x, y)) -> Sub(p,(find x env, find y env))
  | Mul(p,(x, y)) -> Mul(p,(find x env, find y env))
  | Div(p,(x, y)) -> Div(p,(find x env, find y env))
  | FNeg(p,x) -> FNeg(p,find x env)
  | FAdd(p,(x, y)) -> FAdd(p,(find x env, find y env))
  | FSub(p,(x, y)) -> FSub(p,(find x env, find y env))
  | FMul(p,(x, y)) -> FMul(p,(find x env, find y env))
  | FDiv(p,(x, y)) -> FDiv(p,(find x env, find y env))
  | CmpEq(p,(x, y)) -> CmpEq(p, (find x env, find y env))
  | CmpLt(p,(x, y)) -> CmpLt(p, (find x env, find y env))
  | If(p1,p2,(x, e1, e2)) -> If(p1,p2,(find x env, g env e1, g env e2))
  | Let(p,ls,((x, t), e1, e2)) when SLS.mem SLS.Exported ls ->
      (* exportしているから消してはいけない XXX <- really? *)
      Let(p,ls,((x, t), g env e1, g env e2))
  | Let(p,ls,((x, t), e1, e2)) -> (* letのβ簡約 (caml2html: beta_let) *)
      (match g env e1 with
      | Var(_,y) ->
	  Format.eprintf "beta-reducing %s = %s@." x y;
	  g (M.add x y env) e2
      | e1' ->
	  let e2' = g env e2 in
	  Let(p,ls,((x, t), e1', e2')))
  | LetRec(p,ls,({ name = xt; args = yts; body = e1 }, e2)) ->
      LetRec(p,ls,({ name = xt; args = yts; body = g env e1 }, g env e2))
  | Var(p,x) -> Var(p,find x env) (* 変数を置換 (caml2html: beta_var) *)
  | Tuple(p,xs) -> Tuple(p,List.map (fun x -> find x env) xs)
  | LetTuple(p,(xts, y, e)) -> LetTuple(p,(xts, find y env, g env e))
  | Array(p,(i, t, y)) -> Array(p,(i, t, find y env))
  | Get(p,(x, y)) -> Get(p,(find x env, find y env))
  | Put(p,(x, y, z)) -> Put(p,(find x env, find y env, find z env))
  | App(p,(g, xs)) -> App(p,(find g env, List.map (fun x -> find x env) xs))
  | ExtArray(p,x) -> ExtArray(p,x)
  | ExtTuple(p,x) -> ExtTuple(p,x)
  | ExtFunApp(p,(x, ys)) -> ExtFunApp(p,(x, List.map (fun y -> find y env) ys))
  | Native(p,xt) -> Native(p,xt)

let f = g M.empty
