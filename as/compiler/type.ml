type t = (* MinCamlの型を表現するデータ型 (caml2html: type_t) *)
  | Unit
  | Bool
  | Int
  | Float
  | Fun of t list * t (* arguments are uncurried *)
  | Tuple of t list
  | Array of t
  | Var of t option ref

let gentyp () = Var(ref None) (* 新しい型変数を作る *)

(* week1-q2: 型を文字列で表現 *)
let rec type_str ty =
  (* 複数の型を,でつなげる *)
  let rec join dlm tys =
    match tys with
      | [] -> ""
      | ty::[] -> type_str ty
      | ty::tys' -> type_str ty ^ dlm ^ join dlm tys' (* ^ をループで使うのはよくないらしいがエラー表示用だし…… *)
  in
    match ty with
      | Unit -> "unit"
      | Bool -> "bool"
      | Int -> "int"
      | Float -> "float"
      | Fun (ts,t) -> join "," ts ^ " -> " ^ type_str t
      | Tuple ts -> join "*" ts
      | Array t -> "array(" ^ type_str t ^ ")"
      | Var tr ->
          match !tr with
            | None -> "?"
            | Some ty' -> type_str ty'

