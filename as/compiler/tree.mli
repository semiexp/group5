val make: ('a -> string * 'a list) -> ('a -> string) -> string -> 'a -> string
