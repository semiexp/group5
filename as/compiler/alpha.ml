(* rename identifiers to make them unique (alpha-conversion) *)

open KNormal

let find x env = try M.find x env with Not_found -> x

let rec g env = function (* α変換ルーチン本体 (caml2html: alpha_g) *)
  | Unit(p) -> Unit(p)
  | Int(p,i) -> Int(p,i)
  | Float(p,d) -> Float(p,d)
  | Neg(p,x) -> Neg(p,find x env)
  | Add(p,(x, y)) -> Add(p,(find x env, find y env))
  | Sub(p,(x, y)) -> Sub(p,(find x env, find y env))
  | Mul(p,(x, y)) -> Mul(p,(find x env, find y env))
  | Div(p,(x, y)) -> Div(p,(find x env, find y env))
  | FNeg(p,x) -> FNeg(p,find x env)
  | FAdd(p,(x, y)) -> FAdd(p,(find x env, find y env))
  | FSub(p,(x, y)) -> FSub(p,(find x env, find y env))
  | FMul(p,(x, y)) -> FMul(p,(find x env, find y env))
  | FDiv(p,(x, y)) -> FDiv(p,(find x env, find y env))
  | CmpEq(p,(x, y)) -> CmpEq(p,(find x env, find y env))
  | CmpLt(p,(x, y)) -> CmpLt(p,(find x env, find y env))
  | If(p1,p2,(x, e1, e2)) -> If(p1,p2,(find x env, g env e1, g env e2))
  | Let(p,ls,((x, t), e1, e2)) when not (SLS.mem SLS.Exported ls) -> (* letのα変換 (caml2html: alpha_let) *)
      let x' = Id.genid x in
      Let(p,ls,((x', t), g env e1, g (M.add x x' env) e2))
  | Let(p,ls,((x, t), e1, e2)) ->
      (* exportedなのでなまえを変えてはいけない *)
      assert (SLS.mem SLS.Exported ls);
      Let(p,ls,((x, t), g env e1, g env e2))
  | Var(p,x) -> Var(p,find x env)
  | LetRec(p,ls,({ name = (x, t); args = yts; body = e1 }, e2)) when not (SLS.mem SLS.Exported ls) -> (* let recのα変換 (caml2html: alpha_letrec) *)
      let env = M.add x (Id.genid x) env in
      let ys = List.map fst yts in
      let env' = M.add_list2 ys (List.map Id.genid ys) env in
      LetRec(p,ls,({ name = (find x env, t);
	       args = List.map (fun (y, t) -> (find y env', t)) yts;
	       body = g env' e1 },
	     g env e2))
  | LetRec(p,ls,({ name = (x, t); args = yts; body = e1 }, e2)) ->
      (* 外部から参照される変数なのでおなまえはそのまま *)
      assert (SLS.mem SLS.Exported ls);
      let ys = List.map fst yts in
      let env' = M.add_list2 ys (List.map Id.genid ys) env in
      LetRec(p,ls,({ name = (x, t);
	       args = List.map (fun (y, t) -> (find y env', t)) yts;
	       body = g env' e1 },
	     g env e2))
  | App(p,(x, ys)) -> App(p,(find x env, List.map (fun y -> find y env) ys))
  | Tuple(p,xs) -> Tuple(p,List.map (fun x -> find x env) xs)
  | LetTuple(p,(xts, y, e)) -> (* LetTupleのα変換 (caml2html: alpha_lettuple) *)
      let xs = List.map fst xts in
      let env' = M.add_list2 xs (List.map Id.genid xs) env in
      LetTuple(p,(List.map (fun (x, t) -> (find x env', t)) xts,
	       find y env,
	       g env' e))
  | Array(p,(i, t, y)) -> Array(p,(i, t, find y env))
  | Get(p,(x, y)) -> Get(p,(find x env, find y env))
  | Put(p,(x, y, z)) -> Put(p,(find x env, find y env, find z env))
  | ExtArray(p,x) -> ExtArray(p,x)
  | ExtTuple(p,x) -> ExtTuple(p,x)
  | ExtFunApp(p,(x, ys)) -> ExtFunApp(p,(x, List.map (fun y -> find y env) ys))
  | Native(p,xt) -> Native(p,xt)

(* ExportLet, ExportLetRecに接頭辞をつける対応をつくる（ついでに名前も変えてしまう） *)
let rec findexported env = function
  | If(p1,p2,(x, e1, e2)) ->
      let e1', env = findexported env e1 in
      let e2', env =findexported env e2 in
        (If(p1,p2,(x, e1', e2')), env)
  | Let(p,ls,((x, t), e1, e2)) when SLS.mem SLS.Exported ls ->
      (* exportされている *)
      let x' = "min_caml_" ^ x in
      let e1', env = findexported env e1 in
      let env' = M.add x x' env in
      let e2', env'= findexported env' e2 in
        (Let(p,ls,((x', t), e1', e2')), env')
  | Let(p,ls,(xt, e1, e2)) ->
      (* exportされていない *)
      let e1', env = findexported env e1 in
      let e2', env = findexported env e2 in
        (Let(p,ls,(xt, e1', e2')), env)
  | LetRec(p,ls,({ name = (x, t); args = yts; body = e1 }, e2)) when SLS.mem SLS.Exported ls ->
      (* exportされているので追加 *)
      let x' = "min_caml_" ^ x in
      let env' = M.add x x' env in
      let e1', env' = findexported env' e1 in
      let e2', env' = findexported env' e2 in
        (LetRec(p,ls,({ name = (x', t); args = yts; body = e1'}, e2')), env')
  | LetRec(p,ls,({ name = xt; args = yts; body = e1 }, e2)) ->
      let e1', env = findexported env e1 in
      let e2', env = findexported env e2 in
        (LetRec(p,ls,({ name = xt; args = yts; body = e1'}, e2')), env)
  | LetTuple(p,(xts, y, e)) ->
      let e', env = findexported env e in
        (LetTuple(p,(xts,y,e')), env)
  | e -> (e, env)

let f e =
  (* ここで外部関数は接頭辞をつけてしまう *)
  let e', env = findexported M.empty e in
     g env e'
