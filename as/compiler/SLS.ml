(* Let Status *)
type letstatus = Exported | NoInline

module SLS = Set.Make(struct
                        type t = letstatus
                        let compare: t -> t -> int = compare
                      end)
include SLS

let ls_default = empty
let ls_exported = singleton Exported
