let limit = ref 1000

let verbose = ref false

let rec iter n e = (* 最適化処理をくりかえす (caml2html: main_iter) *)
  Format.eprintf "iteration %d@." n;
  if n = 0 then e else
  (*let e' = Mem.f (ExpandIf.f (ElimSubexp.f (Elim.f (ConstFold.f (Inline.f (Assoc.f (Beta.f e))))))) in*)
  let e' = Mem.f (Gvn.f (Elim.f (Cond.f (ConstFold.f (Inline.f (UnusedArgs.f (Assoc.f (Beta.f e)))))))) in
  if e = e' then e else
  iter (n - 1) e'

(* Syntax.tを受け取ってもろもろやって出力する *)
let lexbuf outchan t = (* バッファをコンパイルしてチャンネルへ出力する (caml2html: main_lexbuf) *)
  Id.counter := 0;
  Typing.extenv := M.empty;
  Emit.f outchan
    ((fun p ->
        if !verbose then Format.eprintf "\nAsm.t(after rscd):\n%s\n" (Asm.tree p) else Format.eprintf "\nRscd ended\n";
        p
    )
       (Rscd.f
          ((fun p ->
              (* 仮想命令出力結果を出力 *)
              if !verbose then Format.eprintf "\nAsm.t:\n%s\n" (Asm.tree p) else Format.eprintf "\nRegAlloc ended\n";
              p
          )
             (RegAlloc.f
                ((fun p ->
                    (* 仮想命令出力結果を出力 *)
                    if !verbose then Format.eprintf "\nAsm.t:\n%s\n" (Asm.tree p) else Format.eprintf "\nSimm ended\n";
                    p
                )
                   (Simm.f
                      ((fun p ->
                          (* 仮想命令出力結果を出力 *)
                          if !verbose then Format.eprintf "\nAsm.t:\n%s\n" (Asm.tree p) else Format.eprintf "\nVirtual ended\n";
                          p
                      )
                         (Virtual.f
                            ((fun p ->
                                (* クロージャ変換結果を出力 *)
                                if !verbose then Format.eprintf "\nClosure.t:\n%s\n" (Closure.tree p) else Format.eprintf "\nClosure ended\n";
                                p
                            )
                               (Closure.f
                                  ((fun t ->
                                      (* 最適化結果を出力 *)
                                      if !verbose then Format.eprintf "\nKnormal.t (after optimization):\n%s\n" (KNormal.tree t) else Format.eprintf "\nkNormal optimization ended\n";
                                      t)
                                     (iter !limit
                                        (Tuple.f
                                           (Alpha.f
                                              ((fun t ->
                                                  (* K Normalize結果を出力 *)
                                                  if !verbose then Format.eprintf "\nKnormal.t:\n%s\n" (KNormal.tree t) else Format.eprintf "\nKNormal ended\n";
                                                  t)
                                                 (KNormal.f
                                                    (Typing.f
                                                       ((fun t ->
                                                           (* 構文解析結果を出力 *)
                                                           if !verbose then Format.eprintf "\nSyntax.t:\n%s\n" (Syntax.tree t) else Format.eprintf "\nParsing ended\n";
                                                           t)
                                                          t))))))))))))))))))

let sntx l = (* バッファを構文解析してSyntax.tにするところまで *)
  Parser.exp Lexer.token l

let string s = lexbuf stdout (sntx (Lexing.from_string s)) (* 文字列をコンパイルして標準出力に表示する (caml2html: main_string) *)

let file f = (* ファイルをコンパイルしてファイルに出力する (caml2html: main_file) *)
  let inchan = open_in (f ^ ".ml") in
  let outchan = open_out (f ^ ".s") in
  try
    lexbuf outchan (sntx (Lexing.from_channel inchan));
    close_in inchan;
    close_out outchan;
  with e -> (close_in inchan; close_out outchan; raise e)

let () = (* ここからコンパイラの実行が開始される (caml2html: main_entry) *)
  let files = ref [] in
  Arg.parse
    [("-inline", Arg.Int(fun i -> Inline.threshold := i), "maximum size of functions inlined");
     ("-iter", Arg.Int(fun i -> limit := i), "maximum number of optimizations iterated");
     ("-verbose", Arg.Unit(fun () -> verbose := true), "verbose output");
     ("-sortware-finv", Arg.Unit(fun () -> Virtual.software_finv := true), "use sortware finv")]
    (fun s -> files := !files @ [s])
    ("Mitou Min-Caml Compiler (C) Eijiro Sumii\n" ^
     Printf.sprintf "usage: %s [-inline m] [-iter n] ...filenames without \".ml\"..." Sys.argv.(0));
  (* 複数ファイルは別々に構文木を作ってから統合 *)
  let ts = List.map (fun f ->
                       let inchan = open_in (f ^ ".ml") in
                         try
                           let t = sntx (Lexing.from_channel inchan) in
                             close_in inchan;
                             t
                         with e -> close_in inchan; raise e) !files in
    (* 最後のファイル名を.sにしたやつに出力 *)
  let t = Syntax.merge_t ts in
  let outchan = open_out ((List.hd (List.rev !files)) ^ ".s") in
    try
      lexbuf outchan t;
      close_out outchan
    with e -> close_out outchan; raise e
