(* 条件式最適化 *)
open KNormal

(* 条件 *)
type stat =
  | SLti of Id.t * int | SEqi of Id.t * int | SGti of Id.t * int
  | SNEi of Id.t * int | SLEi of Id.t * int | SGEi of Id.t * int
  | SLtf of Id.t * float | SEqf of Id.t * float | SGtf of Id.t * float
  | SNEf of Id.t * float | SLEf of Id.t * float | SGEf of Id.t * float
  | SNull

let getv = function
  | SLti(x,_) | SEqi(x,_) | SGti(x,_)
  | SNEi(x,_) | SLEi(x,_) | SGEi(x,_)
  | SLtf(x,_) | SEqf(x,_) | SGtf(x,_)
  | SNEf(x,_) | SLEf(x,_) | SGEf(x,_) -> x
  | SNull -> assert false

(* xが示す条件とその否定 *)
let getStat x env venv =
  if M.mem x venv then
    (match M.find x venv with
       | CmpEq(_, (y, z)) when M.mem y venv ->
           (* この中ではyとzが等しい *)
           (match M.find y venv with
              | Int(_, i) ->
                  (* zの値が判明 *)
                  (SEqi(z,i), SNEi(z,i))
              | Float(_, i) ->
                  (SEqf(z,i), SNEf(z,i))
              | _ -> 
                  (SNull,SNull))
       | CmpEq(_, (y, z)) when M.mem z venv ->
           (* この中ではyとzが等しい *)
           (match M.find z venv with
              | Int(_, i) ->
                  (SEqi(y,i), SNEi(y,i))
              | Float(_, i) ->
                  (SEqf(y,i), SNEf(y,i))
              | _ -> 
                  (SNull,SNull))
       | CmpLt(_, (y, z)) when M.mem y venv ->
           (match M.find y venv with
              | Int(_, i) ->
                  (SGti(z,i),SLEi(z,i))
              | Float(_, i) ->
                  (SGtf(z,i),SLEf(z,i))
              | _ -> 
                  (SNull,SNull))
       | CmpLt(_, (y, z)) when M.mem z venv ->
           (match M.find z venv with
              | Int(_, i) ->
                  (SLti(y,i),SGEi(y,i))
              | Float(_, i) ->
                  (SLtf(y,i),SGEf(y,i))
              | _ -> 
                  (SNull,SNull))
       | _ -> (SNull,SNull))
  else
    (SNull,SNull)

let skipable s env =
  (* 条件sの真偽が既に判明しているかも *)
  match s with
    | SNull -> None
    | SLti(x, i) when M.mem x env ->
        (match M.find x env with
           | SLti(_, j) when j <= i ->
               (* x < j and j <= i |- x < i *)
               Some true
           | SEqi(_, j) when j < i ->
               (* x = j and j < i |- x < i *)
               Some true
           | SEqi(_, j) when j >= i ->
               Some false
           | SGti(_, j) when i <= j ->
               (* x > j and i <= j |- x > i *)
               Some false
           | SLEi(_, j) when j < i ->
               (* x <= j and j < i |- x < i *)
               Some true
           | SGEi(_, j) when i <= j ->
               (* x >= j and i <= j |- x >= i *)
               Some false
           | _ -> None)
    | SEqi(x, i) when M.mem x env ->
        (match M.find x env with
           | SLti(_, j) when j <= i -> 
               (* x < j and j <= i |- x < i |- x != i *)
               Some false
           | SEqi(_, j) ->
               Some (i=j)
           | SGti(_, j) when i <= j ->
               (* x > j and j >= i |- x > i |- x != i *)
               Some false
           | SNEi(_, j) when i = j ->
               Some true
           | SLEi(_, j) when j < i ->
               (* x <= j and j < i |- x < i |- x != i *)
               Some false
           | SGEi(_, j) when i < j -> 
               (* x >= j and j > i |- x > i |- x != i *)
               Some false
           | _ -> None)
    | SGti(x, i) when M.mem x env ->
        (match M.find x env with
           | SLti(_, j) when j <= i ->
               (* x < j and j <= i |- x < i *)
               Some false
           | SEqi(_, j) when j > i ->
               (* x = j and j > i |- x > i *)
               Some true
           | SEqi(_, j) when j <= i ->
               Some false
           | SGti(_, j) when j >= i ->
               (* x > j and j >= i |- x > i *)
               Some true
           | SLEi(_, j) when j <= i ->
               (* x <= j and j <= i |- x <= i *)
               Some false
           | SGEi(_, j) when j > i ->
               (* x >= j and j > i |- x > i *)
               Some true
           | _ -> None)
    | SNEi(x, i) when M.mem x env ->
        (match M.find x env with
           | SLti(_, j) when i = j ->
               (* x < j and i = j |- x < i *)
               Some true
           | SEqi(_, j) ->
               Some (i <> j)
           | SGti(_, j) when i = j ->
               (* x > j and i = j |- x > i *)
               Some true
           | SNEi(_, j) when i = j ->
               Some true
           | SLEi(_, j) when j < i ->
               (* x <= j and j < i |- x < i *)
               Some true
           | SGEi(_, j) when j > i ->
               (* x >= j and j > i |- x > i *)
               Some true
           | _ -> None)
    | SLEi(x, i) when M.mem x env ->
        (match M.find x env with
           | SLti(_, j) when j <= i ->
               (* x < j and j <= i |- x < i *)
               Some true
           | SEqi(_, j) when j <= i ->
               (* x = j and j <= i |- x <= i *)
               Some true
           | SGti(_, j) when j >= i ->
               (* x > j and j >= i |- x > i *)
               Some false
           | SLEi(_, j) when j <= i ->
               (* x <= j and j <= i |- x <= i *)
               Some true
           | SGEi(_, j) when j > i ->
               (* x >= j and j > i |- x > i *)
               Some false
           | _ -> None)
    | SGEi(x, i) when M.mem x env ->
        (match M.find x env with
           | SLti(_, j) when j <= i ->
               (* x < j and j <= i |- x < i *)
               Some false
           | SEqi(_, j) when j >= i ->
               Some true
           | SGti(_, j) when j >= i ->
               (* x > j and j >= i |- x > i *)
               Some true
           | SLEi(_, j) when j < i ->
               (* x <= j and j < i |- x < i *)
               Some false
           | SGEi(_, j) when j >= i ->
               (* x >= j and j >= i |- x >= i *)
               Some true
           | _ -> None)
    | SLtf(x, i) when M.mem x env ->
        (match M.find x env with
           | SLtf(_, j) when j <= i ->
               (* x < j and j <= i |- x < i *)
               Some true
           | SEqf(_, j) when j < i ->
               (* x = j and j < i |- x < i *)
               Some true
           | SGtf(_, j) when i <= j ->
               (* x > j and i <= j |- x >= i *)
               Some false
           | SLEf(_, j) when j < i ->
               (* x <= j and j < i |- x < i *)
               Some true
           | SGEf(_, j) when i <= j ->
               (* x >= j and i <= j |- x >= i *)
               Some false
           | _ -> None)
    | SEqf(x, i) when M.mem x env ->
        (match M.find x env with
           | SLtf(_, j) when j < i -> 
               (* x < j and j < i |- x < i |- x != i *)
               Some false
           | SEqf(_, j) ->
               Some (i=j)
           | SGtf(_, j) when i <= j ->
               (* x > j and j >= i |- x > i |- x != i *)
               Some false
           | SNEf(_, j) when i = j ->
               Some false
           | SLEf(_, j) when j < i ->
               (* x <= j and j < i |- x < i |- x != i *)
               Some false
           | SGEf(_, j) when i < j -> 
               (* x >= j and j > i |- x > i |- x != i *)
               Some false
           | _ -> None)
    | SGtf(x, i) when M.mem x env ->
        (match M.find x env with
           | SLtf(_, j) when j <= i ->
               (* x < j and j <= i |- x < i *)
               Some false
           | SEqf(_, j) when j > i ->
               (* x = j and j > i |- x > i *)
               Some true
           | SGtf(_, j) when j >= i ->
               (* x > j and j >= i |- x > i *)
               Some true
           | SLEf(_, j) when j <= i ->
               (* x <= j and j <= i |- x <= i *)
               Some false
           | SGEf(_, j) when j > i ->
               (* x >= j and j > i |- x > i *)
               Some true
           | _ -> None)
    | SNEf(x, i) when M.mem x env ->
        (match M.find x env with
           | SLtf(_, j) when i = j ->
               (* x < j and i = j |- x < i *)
               Some true
           | SEqf(_, j) ->
               Some (i <> j)
           | SGtf(_, j) when i = j ->
               (* x > j and i = j |- x > i *)
               Some true
           | SNEf(_, j) when i = j ->
               Some true
           | SLEf(_, j) when j < i ->
               (* x <= j and j < i |- x < i *)
               Some true
           | SGEf(_, j) when j > i ->
               (* x >= j and j > i |- x > i *)
               Some true
           | _ -> None)
    | SLEf(x, i) when M.mem x env ->
        (match M.find x env with
           | SLtf(_, j) when j <= i ->
               (* x < j and j <= i |- x < i *)
               Some true
           | SEqf(_, j) when j <= i ->
               (* x = j and j <= i |- x <= i *)
               Some true
           | SGtf(_, j) when j >= i ->
               (* x > j and j >= i |- x > i *)
               Some false
           | SLEf(_, j) when j <= i ->
               (* x <= j and j <= i |- x <= i *)
               Some true
           | SGEf(_, j) when j > i ->
               (* x >= j and j > i |- x > i *)
               Some false
           | _ -> None)
    | SGEf(x, i) when M.mem x env ->
        (match M.find x env with
           | SLtf(_, j) when j <= i ->
               (* x < j and j <= i |- x < i *)
               Some false
           | SEqf(_, j) when j >= i ->
               Some true
           | SGtf(_, j) when j >= i ->
               (* x > j and j >= i |- x > i *)
               Some true
           | SLEf(_, j) when j < i ->
               (* x <= j and j < i |- x < i *)
               Some false
           | SGEf(_, j) when j >= i ->
               (* x >= j and j >= i |- x >= i *)
               Some true
           | _ -> None)
    | _ -> None


let rec g env venv = function
  | Let(p,ls,((x, t), e1, e2)) ->
      let e1' = g env venv e1 in
      let venv' = (match e1' with
                     | Int(_) | Float(_) | CmpEq(_) | CmpLt(_) ->
                         M.add x e1' venv
                     | Var(_,y) when M.mem y venv ->
                         M.add x (M.find y venv) venv
                     | _ ->
                         venv) in
      let e2' = g env venv' e2 in
        Let(p,ls,((x, t), e1', e2'))
  | If(p1,p2,(x, e1, e2)) when M.mem x venv ->
      (* xの条件をたしかめる *)
      let (s1, s2) = getStat x env venv in
      (* この条件をスキップ可能か調べる *)
      let inc = skipable s1 env in
        (match inc with
           | Some true ->
               (* 条件は常に真だ *)
               Printf.eprintf "resolved condition %s = true\n" x;
               g (M.add (getv s1) s1 env) venv e1
           | Some false ->
               (* 常に偽だ *)
               Printf.eprintf "resolved condition %s = false\n" x;
               g (M.add (getv s2) s2 env) venv e2
           | None ->
               (* わからない *)
               let env1 = if s1 = SNull then env else M.add (getv s1) s1 env in
               let env2 = if s2 = SNull then env else M.add (getv s2) s2 env in
               If(p1,p2,(x, g env1 venv e1, g env2 venv e2)))
  | LetRec(p,ls, ({ name = name; args = args; body = e1}, e2)) ->
      LetRec(p,ls, ({ name = name; args = args; body = g M.empty M.empty e1}, g env venv e2))
  | e -> e

let f e = g M.empty M.empty e
