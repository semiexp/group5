(* translation into assembly with infinite number of virtual registers *)

let software_finv = ref false

open Asm


(*let data = ref [] (* 浮動小数点数の定数テーブル (caml2html: virtual_data) *)*)

let classify xts ini addf addi =
  List.fold_left
    (fun acc (x, t) ->
      match t with
      | Type.Unit -> acc
      | Type.Float -> addf acc x
      | _ -> addi acc x t)
    ini
    xts

let separate xts =
  classify
    xts
    ([], [])
    (fun (int, float) x -> (int, float @ [x]))
    (fun (int, float) x _ -> (int @ [x], float))

let expand xts ini addf addi =
  classify
    xts
    ini
    (fun (offset, acc) x ->
      (offset + 1, addf x offset acc))
    (fun (offset, acc) x t ->
      (offset + 1, addi x t offset acc))

let rec g env = function (* 式の仮想マシンコード生成 (caml2html: virtual_g) *)
  | Closure.Unit(_) -> Ans(Nop)
  | Closure.Int(p,i) -> Ans(Set(p,i))
  | Closure.Float(p,d) ->
      Ans(SetF(p, d))
  | Closure.Neg(p,x) -> Ans(Neg(p,x))
  | Closure.Add(p,(x, y)) -> Ans(Add(p,(x, V(y))))
  | Closure.Sub(p,(x, y)) -> Ans(Sub(p,(V(x), y)))
  | Closure.Mul(p,(x, y)) -> Ans(Mul(p,(x, y)))
  | Closure.Div(p,(x, y)) -> Ans(Div(p,(x, y)))
  | Closure.FNeg(p,x) -> Ans(FNeg(p,x))
  | Closure.FAdd(p,(x, y)) -> Ans(FAdd(p,(x, Vf(y))))
  | Closure.FSub(p,(x, y)) -> Ans(FSub(p,(Vf(x), y)))
  | Closure.FMul(p,(x, y)) -> Ans(FMul(p,(x, Vf(y))))
  | Closure.FDiv(p,(x, y)) ->
      (* fdivはfinvしてmul *)
      let a = Id.genid "f" in
      if !software_finv then
        (* sortware finvを使う *)
        Let((a, Type.Float), CallDir(p, (Id.L("min_caml_finv"), [], [y])),
            Ans(FMul(p, (x, Vf a))))
      else
        Let((a, Type.Float), FInv(p, y),
            Ans(FMul(p, (x, Vf a))))
  | Closure.CmpEq(p,(x, y)) -> 
      (match M.find x env with
         | Type.Bool | Type.Int ->
             Ans(CmpEq(p,(x, V(y))))
         | Type.Float ->
             Ans(CmpFEq(p,(x, y)))
         | _ -> Printf.eprintf "%s\n" (Type.type_str (M.find x env));failwith "equality supported only for bool, int, and float")
  | Closure.CmpLt(p,(x, y)) ->
      (match M.find x env with
         | Type.Bool | Type.Int ->
             Ans(CmpGt(p,(y, V(x))))
         | Type.Float ->
             Ans(CmpFGt(p,(y, x)))
         | _ -> Printf.eprintf "%s\n" (Type.type_str (M.find x env));failwith "inequality supported only for bool, int, and float")
  | Closure.If(p1,p2,(x, e1, e2)) ->
      Ans(If(p1,p2,(x, g env e1, g env e2)))
  | Closure.Let(_,((x, t1), e1, e2)) ->
      let e1' = g env e1 in
      let e2' = g (M.add x t1 env) e2 in
      concat e1' (x, t1) e2'
  | Closure.Var(p,x) ->
      (match M.find x env with
      | Type.Unit -> Ans(Nop)
      | _ -> Ans(Mov(p,x)))
  | Closure.MakeCls(p,((x, t), { Closure.entry = l; Closure.actual_fv = ys }, e2)) -> (* クロージャの生成 (caml2html: virtual_makecls) *)
      (* Closureのアドレスをセットしてから、自由変数の値をストア *)
      let e2' = g (M.add x t env) e2 in
      let offset, store_fv =
        expand
          (List.map (fun y -> (y, M.find y env)) ys)
          (1, e2')
          (fun y offset store_fv -> seq(Sto(p,(y, Vm(x, offset))), store_fv))
          (fun y _ offset store_fv -> seq(Sto(p,(y, Vm(x, offset))), store_fv)) in
        (* ヒープポインタ（この位置に関数アドレスが入る）をxに保存 *)
        Let((x, t), Mov(p,reg_hp),
            (* ストアした自由変数の分だけヒープポインタを進める *)
            Let((reg_hp, Type.Int), Add(p,(reg_hp, C(offset))),
                let z = Id.genid "l" in
                  (* 関数のアドレスをxの位置にストア *)
                  Let((z, Type.Int), SetL(p,(l, 0)),
                      seq(Sto(p, (z, Vm(x, 0))),
                          (* store_fv内で自由変数もストアしておわり *)
                          store_fv))))
  | Closure.AppCls(p,(x, ys)) ->
      let (int, float) = separate (List.map (fun y -> (y, M.find y env)) ys) in
      Ans(CallCls(p,(x, int, float)))
  | Closure.AppDir(p,(Id.L(x), ys)) ->
      let (int, float) = separate (List.map (fun y -> (y, M.find y env)) ys) in
      Ans(CallDir(p,(Id.L(x), int, float)))
  | Closure.Tuple(p,xs) -> (* 組の生成 (caml2html: virtual_tuple) *)
      let y = Id.genid "t" in
      let (offset, store) =
        (* 0番から順にヒープにストア *)
        expand
          (List.map (fun x -> (x, M.find x env)) xs)
          (0, Ans(Mov(p,y)))
          (fun x offset store -> seq(Sto(p,(x, Vm(y, offset))), store))
          (fun x _ offset store -> seq(Sto(p,(x, Vm(y, offset))), store)) in
        (* yにタプルのアドレスを入れる *)
        Let((y, Type.Tuple(List.map (fun x -> M.find x env) xs)), Mov(p,reg_hp),
            (* ヒープポインタを動かす *)
            Let((reg_hp, Type.Int), Add(p,(reg_hp, C(offset))),
                (* データをストアしてyを返す *)
                store))
  | Closure.LetTuple(p,(xts, y, e2)) ->
      let s = Closure.fv e2 in
      let (offset, load) =
        expand
          xts
          (0, g (M.add_list xts env) e2)
          (* 中で使わないものはロードしていない *)
          (fun x offset load ->
             if not (S.mem x s) then load else (* [XX] a little ad hoc optimization *)
               Let((x, Type.Float), Ld(p,Vm(y, offset)), load))
          (fun x t offset load ->
             if not (S.mem x s) then load else (* [XX] a little ad hoc optimization *)
               Let((x, t), Ld(p, Vm(y, offset)), load)) in
        load
  | Closure.Get(p,(x, y)) -> (* 配列の読み出し (caml2html: virtual_get) *)
      (match M.find x env with
      | Type.Array(Type.Unit) -> Ans(Nop)
      | Type.Array(_) as t->
          let a = Id.gentmp t in
            Let((a, t), Add(p, (x, V(y))),
                Ans(Ld(p, Vm(a, 0))))
      | _ -> assert false)
  | Closure.Put(p,(x, y, z)) ->
      (match M.find x env with
      | Type.Array(Type.Unit) -> Ans(Nop)
      | Type.Array(_) as t ->
          let a = Id.gentmp t in
            Let((a, t), Add(p, (x, V(y))),
                Ans(Sto(p, (z, Vm(a, 0)))))
      | _ -> assert false)
  | Closure.ExtArray(p,Id.L(x))
  | Closure.ExtTuple(p,Id.L(x)) -> Ans(SetL(p,(Id.L("min_caml_" ^ x) ,0)))
  | Closure.Array(p,(i, t, x)) ->
      (* xがi個並んだ配列を作成 *)
      let addr = Id.gentmp Type.Int in
      let rec iter j =
        if j >= i then
          (* 配列つくりおわったからreg_hpを進めておわり *)
          Let((reg_hp, Type.Int), Add(p, (reg_hp, C(i))),
              Ans(Mov(p, addr)))
        else
          (* 配列をひとつ作る *)
          let u = Id.gentmp Type.Unit in
            Let((u, Type.Unit), Sto(p, (x, Vm(addr, j))),
                iter (j+1)) in
        Let((addr, Type.Int), Mov(p, reg_hp), iter 0)
  | Closure.GlobalArray(p,(l, size, x)) ->
      (* Globalの場合も同様に作成 *)
      let rec iter j =
        if j >= size then
          (* 配列つくりおわった *)
          Ans(SetL(p,(l, 0)))
        else
          (* 配列をひとつ作る *)
          let u = Id.gentmp Type.Unit in
            Let((u, Type.Unit), Sto(p, (x, Vml(l, j))),
                iter (j+1)) in
        iter 0
  | Closure.GlobalTuple(p, (l, xs)) ->
      (* Tupleと同様に展開 *)
      let t = Id.gentmp Type.Int in
      let (offset, store) =
        expand
          (List.map (fun x -> (x, M.find x env)) xs)
          (0, Ans(Mov(p,t)))
          (fun x offset store -> seq(Sto(p,(x, Vm(t, offset))), store))
          (fun x _ offset store -> seq(Sto(p,(x, Vm(t, offset))), store)) in
        Let((t, Type.Tuple(List.map (fun x -> M.find x env) xs)), SetL(p, (l, 0)),
            store)
  | Closure.AppNative(p, ((x, t), xs)) ->
      (match x with
         | "fabs" ->
             assert (List.length xs = 1);
             Ans(FAbs(p, List.hd xs))
         | "fneg" ->
             assert (List.length xs = 1);
             Ans(FNeg(p, List.hd xs))
         | "fsqrt" ->
             assert (List.length xs = 1);
             Ans(FSqrt(p, List.hd xs))
         | "read" ->
             assert (List.length xs = 1); (* UNIT *)
             Ans(In(p))
         | "out" ->
             assert (List.length xs = 1);
             Ans(Out(p, List.hd xs))
         | "lshift" ->
             assert (List.length xs = 2);
             Ans(LShift(p, (List.hd xs, V(List.nth xs 1))))
         | "rshift" ->
             assert (List.length xs = 2);
             Ans(RShift(p, (List.hd xs, V(List.nth xs 1))))
         | "add" ->
             assert (List.length xs = 2);
             Ans(Add(p, (List.hd xs, V(List.nth xs 1))))
         | _ ->
             failwith ("unsupported native '" ^ x ^ "'"))




(* 関数の仮想マシンコード生成 (caml2html: virtual_h) *)
let h depth { Closure.pos = p; Closure.name = (Id.L(x), t); Closure.args = yts; Closure.formal_fv = zts; Closure.body = e } =
  let (int, float) = separate yts in
  let category = try (M.find x depth) mod reg_split_number with Not_found -> 0 in
  let (offset, load) =
    expand
      zts
      (* 1: クロージャのアドレスの次に自由変数が並んでいる *)
      (1, g (M.add x t (M.add_list yts (M.add_list zts M.empty))) e)
      (fun z offset load -> Let((z,Type.Float), Ld(p, Vm(reg_cl, offset)), load))
      (fun z t offset load -> Let((z, t), Ld(p, Vm(reg_cl, offset)), load)) in
    match t with
      | Type.Fun(_, t2) ->
          { name = Id.L(x); args = int; fargs = float; body = load; ret = t2; ret_reg = ""; category = category }
      | _ -> Printf.eprintf "%s\n" (Type.type_str t);  assert false

(* ネイティブ用のクロージャを生成 *)
(* XXX nativeの定義が2箇所に必要！！！ *)
let i (id, (l, t)) =
  let cl = 
    match id with
      | "fabs" | "fneg" | "fsqrt" ->
          let tmp = Id.gentmp Type.Float in
            {
              Closure.pos = Syntax.dummy_pos();
              Closure.name = (l, t);
              Closure.args = [(tmp, Type.Float)];
              Closure.formal_fv = [];
              Closure.body = Closure.AppNative(Syntax.dummy_pos(), ((id, t), [tmp]))
            }
      | "read" ->
          let tmp = Id.gentmp Type.Unit in
            {
              Closure.pos = Syntax.dummy_pos();
              Closure.name = (l, t);
              Closure.args = [(tmp, Type.Unit)];
              Closure.formal_fv = [];
              Closure.body = Closure.AppNative(Syntax.dummy_pos(), ((id, t), [tmp]))
            }
      | "out" ->
          let tmp = Id.gentmp Type.Int in
            {
              Closure.pos = Syntax.dummy_pos();
              Closure.name = (l, t);
              Closure.args = [(tmp, Type.Int)];
              Closure.formal_fv = [];
              Closure.body = Closure.AppNative(Syntax.dummy_pos(), ((id, t), [tmp]))
            }
      | "lshift" | "rshift" | "add" ->
          let tmp1 = Id.gentmp Type.Int in
          let tmp2 = Id.gentmp Type.Int in
            {
              Closure.pos = Syntax.dummy_pos();
              Closure.name = (l, t);
              Closure.args = [(tmp1, Type.Int); (tmp2, Type.Int)];
              Closure.formal_fv = [];
              Closure.body = Closure.AppNative(Syntax.dummy_pos(), ((id, t), [tmp1; tmp2]))
            }
      | _ -> failwith ("unsupported native '" ^ id ^ "'")
  in
    h M.empty cl

(* プログラム全体の仮想マシンコード生成 (caml2html: virtual_f) *)
let f (Closure.Prog(fundefs, globals, natives, e) as p) =
  let (graph, depth) = CallGraph.f p in
  let fundefs = List.map (h depth) fundefs in
  let globals = List.map (fun {Closure.name = (name, _); Closure.size = size} -> {name = name; size = size}) globals in
  let natives = List.map i natives in
  let e = g M.empty e in
  Prog(fundefs @ natives, globals, e)
