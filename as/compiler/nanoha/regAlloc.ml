open Asm 
(* リストのi番目からn個とる *)
let rec sublist i n l =
  let rec take n l m =
    match l with
      | [] -> List.rev m
      | x::l' ->
          if n = 0 then
            List.rev m
          else
            take (n-1) l' (x::m) in
  match l with
    | [] -> []
    | x::l' ->
        if i = 0 then
          take n l' []
        else
          sublist (i-1) n l'

(* 関数のレジスタ周りの情報 *)
let fun_argregs = ref M.empty
let fun_retregs = ref M.empty

let argsof x =
  try
    let (args, fargs) = M.find x !fun_argregs in
      (args@fargs)
  with Not_found -> allregs

let retof x =
  try
    M.find x !fun_retregs
  with Not_found -> regs.(0)


(* for register coalescing *)
(* [XXX] Callがあったら、そこから先は無意味というか逆効果なので追わない。
         そのために「Callがあったかどうか」を返り値の第1要素に含める。 *)
let rec target' src (dest, t) = function
  | Mov(_,x) when x = src && is_reg dest ->
      assert (t <> Type.Unit);
      (* srcの値は最終的にレジスタdestに入るので、srcはdestに割り当てたい *)
      false, [dest]
  | If(_,_,(_, e1, e2)) | IfNot(_,_,(_, e1, e2)) ->
      let c1, rs1 = target src (dest, t) e1 in
      let c2, rs2 = target src (dest, t) e2 in
      c1 && c2, rs1 @ rs2
  | CallCls(_,(x, ys, zs)) ->
      (* TODO *)
      assert false;
      true, (target_args src allregs 0 (ys @ zs) @
             (* Callclsのときはx（クロージャアドレス）はreg_clに入れないといけないので、srcはreg_clに割り当てたい *)
             if x = src then [reg_cl] else [])
  | CallDir(_,(Id.L(x), ys, zs)) ->
      (* 関数呼出なので引数の情報を得たい *)
      let argregs = argsof x in
        true, target_args src argregs 0 (ys @ zs)
  | _ -> false, []
and target src dest = function (* register targeting (caml2html: regalloc_target) *)
  | Ans(exp) -> target' src dest exp
  | Let(xt, exp, e) ->
      let c1, rs1 = target' src xt exp in
      if c1 then true, rs1 else
      let c2, rs2 = target src dest e in
      c2, rs1 @ rs2
and target_args src all n = function (* auxiliary function for Call *)
  | [] -> []
  (* 引数になっている場合は該当のレジスタに割り当てたい *)
  | y :: ys when src = y  ->
      Printf.eprintf "%s -> %s %d\n" src (List.nth all n) (List.length all);
      (List.nth all n) :: target_args src all (n + 1) ys
  | _ :: ys -> target_args src all (n + 1) ys
(* "register sourcing" (?) as opposed to register targeting *)
let rec source t = function
  | Ans(exp) -> source' t exp
  | Let(_, _, e) -> source t e
and source' t = function
  | Mov(_,x) | Neg(_,x) | FNeg (_,x) | FInv(_,x) | FSqrt(_, x) | FAbs(_,x) | Add(_,(x, C _)) | Sub(_,(C _, x)) | LShift (_,(x, C _)) | RShift (_,(x, C _))
  | CmpEq(_, (x, C _)) | CmpGt(_, (x, C _)) | CmpLt(_, (x, C _ ))
  | FAdd(_,(x, F _)) | FSub(_,(F _, x)) | FMul(_,(x, F _)) -> [x]
  | Add(_,(x, V y)) | Sub(_,(V x, y)) | CmpEq(_, (x, V(y))) | CmpGt(_, (x, V(y))) | CmpLt(_, (x, V(y)))
  | CmpFEq(_,(x, y)) | CmpFGt(_,(x, y)) | FAdd(_,(x, Vf y)) | FSub (_,(Vf x, y)) | FMul(_,(x, Vf y)) -> [x; y]
  | If(_,_,(_, e1, e2)) | IfNot(_,_,(_, e1, e2)) ->
      source t e1 @ source t e2
  | CallCls _ | CallDir _ -> (match t with Type.Unit -> [] | _ -> [regs.(0)])
  | _ -> []
           
(* 関数が破壊するレジスタを列挙 *)
let broken_regs fenv args ret_reg e =
  let rec bg_exp fenv = function
    | If(_,_,(x, e1, e2)) | IfNot(_,_,(x, e1, e2)) when S.mem x allregsset -> x :: remove_and_uniq S.empty (bg fenv e1 @ bg fenv e2)
    | If(_,_,(x, e1, e2)) | IfNot(_,_,(x, e1, e2)) -> remove_and_uniq S.empty (bg fenv e1 @ bg fenv e2)
    | CallCls(_) ->
        (* XXX 何がこわれるか分からない！ *)
        allregs
    | CallDir(_,(Id.L(x), _, _)) ->
        (* xが壊すやつも含む *)
        (try
           let ggg = M.find x fenv in
             S.elements ggg
         with Not_found ->
           (* 外部関数は何を壊すかわからない！！！ *)
           allregs)
    | _ -> []
  and bg fenv = function
    | Ans(exp) -> bg_exp fenv exp
    | Let((x,_), exp, e2) ->
        if S.mem x allregsset then
          x :: bg_exp fenv exp @ bg fenv e2
        else
          bg_exp fenv exp @ bg fenv e2 in
    (* 引数レジスタは破壊されるであろう！！！！ *)
  let args = (if is_reg ret_reg then [ret_reg] else []) @ args in
    S.of_list (args @ bg fenv e)


(* この変数が死ぬまでに関数呼び出しにより破壊されるレジスタを求める *)
let rec destroy_until_die x = function
  | Ans exp ->
      (match exp with
         | If(_,_,(_,e1,e2))
         | IfNot(_,_,(_,e1,e2)) ->
             S.union (destroy_until_die x e1) (destroy_until_die x e2)
         | _ ->
             S.empty)
  | Let(_, exp, e) ->
      (* expに関数呼び出しが含まれるか？ *)
      (match exp with
         | CallCls(_) ->
             (* クロージャはなにか分からないから全部破壊される可能性がある *)
             let f = fv e in
               (if List.mem x f then
                  allregsset
                else
                  S.empty)
         | CallDir(_,(Id.L(y), _, _)) ->
             (* yを呼び出す *)
             (if List.mem x (fv e) then
                (* まだxはいる *)
                (try
                   S.union (M.find y (!fregenv)) (destroy_until_die x e)
                 with Not_found ->
                   (* 再帰かな *)
                   allregsset)
              else
                (* この先はもういない *)
                  S.empty)
         | If(_,_,(_,e1,e2))
         | IfNot(_,_,(_,e1,e2)) ->
             (* この中に関数呼び出しがあるかも? *)
             S.union (S.union (destroy_until_die x e1) (destroy_until_die x e2)) (destroy_until_die x e)
         | _ ->
             (* 関数呼び出しはなかった *)
             destroy_until_die x e)

(* 式中の関数呼び出しで破壊されるレジスタは全て求める *)
let rec destroy_all fenv = function
  | Ans exp ->
      (match exp with
         | If(_,_,(_,e1,e2))
         | IfNot(_,_,(_,e1,e2)) ->
             S.union (destroy_all fenv e1) (destroy_all fenv e2)
         | CallCls(_) ->
             allregsset
         | CallDir(_,(Id.L(y), _, _)) ->
             (try
                M.find y fenv
              with Not_found ->
                (* 再帰かな *)
                allregsset)
         | _ ->
             S.empty)
  | Let(_, exp, e) ->
      (* expに関数呼び出しが含まれるか？ *)
      (match exp with
         | CallCls(_) ->
             (* クロージャはなにか分からないから全部破壊される可能性がある *)
             allregsset
         | CallDir(_,(Id.L(y), _, _)) ->
             (* yを呼び出す *)
             (try
                S.union (M.find y fenv) (destroy_all fenv e)
              with Not_found ->
                (* 再帰かな *)
                allregsset)
         | If(_,_,(_,e1,e2))
         | IfNot(_,_,(_,e1,e2)) ->
             (* この中に関数呼び出しがあるかも? *)
             S.union (S.union (destroy_all fenv e1) (destroy_all fenv e2)) (destroy_all fenv e)
         | _ ->
             (* 関数呼び出しはなかった *)
             destroy_all fenv e)


type alloc_result = (* allocにおいてspillingがあったかどうかを表すデータ型 *)
  | Alloc of Id.t (* allocated register *)
  | Spill of Id.t (* spilled variable *)
let rec alloc cont regenv polluted x t prefer =
  (* allocate a register or spill a variable *)
  assert (not (M.mem x regenv));
  let all =
    match t with
    | Type.Unit -> [] (* dummy *)
    | _ -> allregs in
  if all = [] then (assert (t = Type.Unit); Alloc("%unit")) else (* [XX] ad hoc *)
  if is_reg x then Alloc(x) else
  let free = fv cont in
  try
    let live = (* 生きているレジスタ *)
      List.fold_left
        (fun live y ->
	  if is_reg y then S.add y live else
          try S.add (M.find y regenv) live
          with Not_found -> live)
        S.empty
        free in
    (* 使いたいレジスタをえらぶ *)
    (* 関数を超える変数はできれば新しいレジスタを割り当てる *)
    let dud = destroy_until_die x cont in
    let catr =
      (if S.is_empty dud then
         (* xは関数を超えて生存しない。一度使われたレジスタを優先 *)
         polluted
       else
         (* xは関数を超えるからそれと被らないのを優先 *)
         remove_and_uniq dud allregs) in

    let r = (* 候補のうち生きてないレジスタを探す *)
      List.find
        (fun r -> not (S.mem r live))
        (prefer @ catr @ all) in
    (* Format.eprintf "allocated %s to %s@." x r; *)
    Alloc(r)
  with Not_found ->
    Format.eprintf "register allocation failed for %s@." x;
    let y =
      (* spillするやつを選ぶ *)
      List.find
        (fun y ->
	  not (is_reg y) &&
          try List.mem (M.find y regenv) all
          with Not_found -> false)
        (List.rev free) in
    Format.eprintf "spilling %s from %s@." y (M.find y regenv);
    Spill(y)

(* auxiliary function for g and g'_and_restore *)
let add x r regenv =
  if is_reg x then (assert (x = r); regenv) else
  M.add x r regenv

(* auxiliary functions for g' *)
exception NoReg of Id.t * Type.t
(* xに割り当たっているレジスタを探す *)
let find x t regenv =
  if is_reg x then x else
  try M.find x regenv
  with Not_found -> raise (NoReg(x, t))
let find' x' regenv =
  match x' with
  | V(x) -> V(find x Type.Int regenv)
  | c -> c
let find'f x' regenv =
  match x' with
    | Vf(x) -> Vf(find x Type.Float regenv)
    | c -> c

(* dest: この命令列の結果が入る変数(xt)  cont: 後にくる命令列*)
let rec g dest cont regenv fenv polluted = function (* 命令列のレジスタ割り当て (caml2html: regalloc_g) *)
  | Ans(exp) -> g'_and_restore dest cont regenv fenv polluted exp
  | Let((x, t) as xt, exp, e) ->
      assert (not (M.mem x regenv));
      (* 行われる計算はeとcont *)
      let cont' = concat e dest cont in
      let (e1', regenv1) = g'_and_restore xt cont' regenv fenv polluted exp in
      let (_call, targets) = target x dest cont' in
      let sources = source t e1' in
      (* レジスタ間のmovよりメモリを介するswapのほうが問題なので、sourcesよりtargetsを優先 *)
      let prefer = List.filter
                     (fun r -> S.mem r allregsset)
                     (targets @ sources) in
        (match alloc cont' regenv1 polluted x t prefer with
           | Spill(y) ->
               (* yをspillすることになった(rをもらう) *)
               let r = M.find y regenv1 in
               let regenv1' = add x r (M.remove y regenv1) in
               let polluted' = if List.mem r polluted then polluted else r::polluted in
               let (e2', regenv2) = g dest cont regenv1' fenv polluted' e in
               let save =
                 (* XXX ついにdummy_posが!!!!!!!!!!!!!!!! *)
                 try Save(Syntax.dummy_pos(), (M.find y regenv, y))
                 with Not_found -> Nop in	    
                 (seq(save, concat e1' (r, t) e2'), regenv2)
           | Alloc(r) ->
               Format.eprintf "Allocated %s -> %s\n" x r;
               let polluted' = if (not (S.mem r allregsset)) || List.mem r polluted then polluted else r::polluted in
               let (e2', regenv2) = g dest cont (add x r regenv1) fenv polluted' e in
                 (concat e1' (r, t) e2', regenv2))
and g'_and_restore dest cont regenv fenv polluted exp = (* 使用される変数をスタックからレジスタへRestore (caml2html: regalloc_unspill) *)
  try g' dest cont regenv fenv polluted exp
  with NoReg(x, t) ->
    ((* Format.eprintf "restoring %s@." x; *)
     g dest cont regenv fenv polluted (Let((x, t), Restore(Syntax.dummy_pos(), x), Ans(exp))))
and g' dest cont regenv fenv polluted = function (* 各命令のレジスタ割り当て (caml2html: regalloc_gprime) *)
  | Nop | Set _ | SetF _ | SetL _ | In _ | Halt _ | Comment _ | Restore _ as exp -> (Ans(exp), regenv)
  | Mul(p,_) | Div(p,_) -> assert false
  | Mov(p,x) -> (Ans(Mov(p,find x Type.Int regenv)), regenv)
  | Neg(p,x) -> (Ans(Neg(p,find x Type.Int regenv)), regenv)
  | Add(p,(x, y')) -> (Ans(Add(p,(find x Type.Int regenv, find' y' regenv))), regenv)
  | Sub(p,(x', y)) -> (Ans(Sub(p,(find' x' regenv, find y Type.Int regenv))), regenv)
  | LShift(p,(x, y')) -> (Ans(LShift(p,(find x Type.Int regenv, find' y' regenv))), regenv)
  | RShift(p,(x, y')) -> (Ans(RShift(p,(find x Type.Int regenv, find' y' regenv))), regenv)
  | CmpEq(p,(x, y')) -> (Ans(CmpEq(p,(find x Type.Int regenv, find' y' regenv))), regenv)
  | CmpGt(p,(x, y')) -> (Ans(CmpGt(p,(find x Type.Int regenv, find' y' regenv))), regenv)
  | CmpLt(p,(x, y')) -> (Ans(CmpLt(p,(find x Type.Int regenv, find' y' regenv))), regenv)
  | CmpFEq(p,(x, y)) -> (Ans(CmpFEq(p,(find x Type.Float regenv, find y Type.Float regenv))), regenv)
  | CmpFGt(p,(x, y)) -> (Ans(CmpFGt(p,(find x Type.Float regenv, find y Type.Float regenv))), regenv)
  | Ld(p, Vm(x, i)) -> (Ans(Ld(p,Vm(find x Type.Int regenv, i))), regenv)
  | Ld(p, Vml(l, i)) as exp -> (Ans(exp), regenv)
  | Ld(p, I _) as exp -> (Ans(exp), regenv)
  | Sto(p,(x, Vm(y, i))) -> (Ans(Sto(p,(find x Type.Int regenv, Vm(find y Type.Int regenv, i)))), regenv)
  | Sto(p,(x, Vml(l, i))) -> (Ans(Sto(p,(find x Type.Int regenv, Vml(l, i))))), regenv
  | Sto(p,(x, I(i))) -> (Ans(Sto(p,(find x Type.Int regenv, I(i))))), regenv
  | FAdd(p,(x, y')) -> (Ans(FAdd(p,(find x Type.Float regenv, find'f y' regenv))), regenv)
  | FSub(p,(x', y)) -> (Ans(FSub(p,(find'f x' regenv, find y Type.Float regenv))), regenv)
  | FMul(p,(x, y')) -> (Ans(FMul(p,(find x Type.Float regenv, find'f y' regenv))), regenv)
  | FNeg(p,x) -> (Ans(FNeg(p,find x Type.Float regenv)), regenv)
  | FInv(p,x) -> (Ans(FInv(p,find x Type.Float regenv)), regenv)
  | FSqrt(p,x) -> (Ans(FSqrt(p,find x Type.Float regenv)), regenv)
  | FAbs(p,x) -> (Ans(FAbs(p,find x Type.Float regenv)), regenv)
  | Out(p,x) -> (Ans(Out(p,find x Type.Int regenv)), regenv)
  | If(p1,p2,(x, e1, e2)) -> g'_if dest cont regenv fenv polluted exp (fun e1' e2' -> If(p1,p2,(find x Type.Int regenv, e1', e2'))) e1 e2
  | IfNot(p1,p2,(x, e1, e2)) -> g'_if dest cont regenv fenv polluted exp (fun e1' e2' -> IfNot(p1,p2,(find x Type.Int regenv, e1', e2'))) e1 e2
  | CallCls(p,(x, ys, zs)) as exp ->
      (* XXX clsはすべてのレジスタが破壊されるかも *)
      g'_call dest cont regenv allregsset exp (fun ys zs -> CallCls(p,(find x Type.Int regenv, ys, zs))) ys zs
  | CallDir(p,(Id.L(x) as l, ys, zs)) as exp ->
      (* どのレジスタが破壊されるか求めておく *)
      let brs = (try M.find x fenv with Not_found -> allregsset) in
      g'_call dest cont regenv brs exp (fun ys zs -> CallDir(p,(l, ys, zs))) ys zs
  | Save(p, (x, y)) -> assert false

and g'_if dest cont regenv fenv polluted exp constr e1 e2 = (* ifのレジスタ割り当て (caml2html: regalloc_if) *)
  let (e1', regenv1) = g dest cont regenv fenv polluted e1 in
  let (e2', regenv2) = g dest cont regenv fenv polluted e2 in
  let regenv' = (* 両方に共通のレジスタ変数だけ利用 *)
    List.fold_left
      (fun regenv' x ->
        try
	  if is_reg x then regenv' else
          let r1 = M.find x regenv1 in
          let r2 = M.find x regenv2 in
          if r1 <> r2 then regenv' else
	  M.add x r1 regenv'
        with Not_found -> regenv')
      M.empty
      (fv cont) in
  (List.fold_left
     (fun e x ->
       if x = fst dest || not (M.mem x regenv) || M.mem x regenv' then e else
         (* regenvにあるけどregenv'にはない *)
         seq(Save(Syntax.dummy_pos(), (M.find x regenv, x)), e)) (* そうでない変数は分岐直前にセーブ *)
     (Ans(constr e1' e2'))
     (fv cont),
   regenv')
and g'_call dest cont regenv brs exp constr ys zs = (* 関数呼び出しのレジスタ割り当て (caml2html: regalloc_call) *)
  (* brs: この呼出で破壊される恐れのあるレジスタ *)
  List.fold_left
     (fun (e, regenv') x ->
        (* 関数呼び出しの後でもまだ使うレジスタは退避 *)
       if x = fst dest || not (M.mem x regenv) then
         (* もう使わないから退避する必要がない *)
         (e, regenv')
       else
         let r = M.find x regenv in
           if S.mem r brs then
             (* 関数によって破壊されるからSaveの必要あり *)
             ((seq(Save(Syntax.dummy_pos(), (r, x)), e)), regenv')
           else
             (* 関数を超えるけど、破壊されないからそのままでいい *)
             (e, M.add x r regenv'))
     ((Ans(constr
	    (List.map (fun y -> find y Type.Int regenv) ys)
	    (List.map (fun z -> find z Type.Float regenv) zs))), M.empty)
     (fv cont)

let h fenv { name = Id.L(x); args = ys; fargs = zs; body = e; ret = t; ret_reg = _; category = category } = (* 関数のレジスタ割り当て (caml2html: regalloc_h) *)
  let regenv = M.add x reg_cl M.empty in (*クロージャアドレスが予約されている以外は空っぽだ！ *)
  (* 中の関数呼出で破壊されるレジスタをさがす *)
  let dsts = destroy_all (M.add x S.empty fenv) e in
  (* 引数には新しいレジスタを割り当てたい *)
  let remains = remove_and_uniq dsts allregs @ allregs in
  let (i, arg_regs, regenv) =
    (* ここで引数を追加 *)
    List.fold_left
      (fun (i, arg_regs, regenv) y ->
        let r = List.nth remains i in
        (i + 1,
	 arg_regs @ [r],
	 (assert (not (is_reg y));
	  M.add y r regenv)))
      (0, [], regenv)
      ys in
  let (i, farg_regs, regenv) =
    List.fold_left
      (fun (i, farg_regs, regenv) y ->
        let r = List.nth remains i in
        (i + 1,
	 farg_regs @ [r],
	 (assert (not (is_reg y));
	  M.add y r regenv)))
      (i, [], regenv)
      zs in
  (* a: 返り値のレジスタ *)
  let a =
    match t with
    | Type.Unit -> Id.gentmp Type.Unit
    | _ -> (*List.hd remains*) regs.(0) in
  (* pollutedは中の関数呼び出しで破壊されるレジスタ及び引数レジスタ *)
  let polluted = arg_regs @ farg_regs @ S.elements dsts in
  (* 先行して自身の情報を入れておく *)
  fun_argregs := M.add x (arg_regs, farg_regs) (!fun_argregs);
  fun_retregs := M.add x a (!fun_retregs);
  let (e', regenv') = g (a, t) (Ans(Mov(Syntax.dummy_pos(), a))) regenv fenv polluted e in
  { name = Id.L(x); args = arg_regs; fargs = farg_regs; body = e'; ret = t; ret_reg = a; category = category }

let f (Prog(fundefs, globals, e)) = (* プログラム全体のレジスタ割り当て (caml2html: regalloc_f) *)
  Format.eprintf "register allocation: may take some time (up to a few minutes, depending on the size of functions)@.";
  (* 関数のレジスタ割り当てをしながら関数が破壊するレジスタの情報を収集 *)
  let fenv = (* XXX 組み込み関数の情報 *)
    M.add "min_caml_create_array" (S.of_list ["%r1";"%r2";"%r3";"%r4";"%r5"])
               (M.singleton "min_caml_create_float_array" (S.of_list ["%r1";"%r2";"%r3";"%r4";"%r5"])) in
  fregenv := fenv;
  let (fenv, fundefs'r) = List.fold_left
                            (fun (fenv, fundefs') ({ name = Id.L(x); args = _; fargs = _; body = _; ret = _; ret_reg = _; category = _ } as fd) ->
                               let fd' = h fenv fd in
                                 fun_argregs := M.add x (fd'.args, fd'.fargs) (!fun_argregs);
                                 fun_retregs := M.add x fd'.ret_reg (!fun_retregs);
                               (* 一時的にxをemptyにする（再帰に対応） *)
                               let brs = broken_regs (M.add x S.empty fenv) (fd'.args @ fd'.fargs) fd'.ret_reg fd'.body in
                                 Printf.eprintf "%s: " x;
                                 S.iter
                                   (fun r -> Printf.eprintf "%s " r)
                                   brs;
                                 Printf.eprintf "\n";
                               let fenv' = M.add x brs fenv in
                                 fregenv := fenv';
                                 (fenv', fd'::fundefs'))
                            (fenv, [])
                            fundefs in
  let fundefs' = List.rev fundefs'r in
  let e', regenv' = g (Id.gentmp Type.Unit, Type.Unit) (Ans(Nop)) M.empty fenv [] e in
  (* 関数が破壊するレジスタの情報をグローバルに *)
  fregenv := fenv;
  Prog(fundefs', globals, e')
