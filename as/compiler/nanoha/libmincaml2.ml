let read_char = [read] in
let print_char = [out] in
let rec floor x =
  let rec floor2 x x2 =
    if x2 <= x then
      x2
    else
      x2 -. 1.0 in
  let xa = [fabs] x in
  if xa >= 8388608.0 then
    x
  else
    (* 指数部 *)
    let exp = [rshift] xa 23 in
    let onex= [lshift] exp 23 in
    (* unbiased *)
    let exp2 = exp - 127 in
    if exp2 < 0 then
      floor2 x 0.0
    else
      (* 仮数部 *)
      let body = [rshift] ([lshift] xa 9) 9 in
      (* body bits cut *)
      let exp3 = 23 - exp2 in
      (* cut 仮数部 *)
      let body2 = [lshift] ([rshift] body exp3) exp3 in
      let xa2 = [add] onex body2 in
      let sign = [lshift] ([rshift] x 31) 31 in
        floor2 x ([add] xa2 sign) in
let rec int_of_float x =
  let x = floor x in
  (* unbiased *)
  let exp = ([rshift] ([lshift] x 1) 24) - 150 in
  let body = [rshift] ([lshift] x 9) 9 in
  let body2 = [add] body 8388608 in
  if x < 0.0 then
    -([lshift] body2 exp)
  else
    [lshift] body2 exp in

let rec float_of_int x =
  let rec float_of_int2 x sign =
    if x < 8388608 then
      (let x2 = [add] x 1258291200 in
         [add] (x2 -. 8388608.0) sign)
    else
      let rec float_of_int3 x sign m n =
        if x >= m then
          float_of_int3 x sign ([lshift] m 1) (n + 1)
        else
          (*let x2 = [rshift] ([rshift] x (n-32)) 9 in*)
          let x2 = [rshift] x (n-23) in
          let exp = [lshift] (n+127) 23 in
            [add] x ([add] sign exp) in
        float_of_int3 x sign 2 0 in

  if x = 0 then
    0.0
  else
    let sign = [lshift] ([rshift] x 31) 31 in
    if x <= 0 then
      float_of_int2 (-x) sign
    else
      float_of_int2 x sign in

noinline let rec read_float _ =
  let rec read_float3 c u =
    let char = read_char() in
      if char = 32 then
        c
      else if char = 10 then
        c
      else if char = 13 then
        c
      else if char = 9 then
        c
      else if 48 <= char then
        if char <= 57 then
          (* num *)
          read_float3 (c +. u *. (float_of_int (char - 48))) (u *. 0.1)
        else
            c
      else
            c in
  let rec read_float2 c =
    let char = read_char() in
      if char = 32 then
        c
      else if char = 10 then
        c
      else if char = 13 then
        c
      else if char = 9 then
        c
      else if char = 46 then
        (* '.' *)
        read_float3 c 0.1
      else if 48 <= char then
        if char <= 57 then
          (* num *)
          read_float2 (c *. 10.0 +. (float_of_int (char - 48)))
        else
            c
      else
            c in
  let char = read_char() in
    if char = 32 then
      read_float()
    else if char = 10 then
      read_float()
    else if char = 13 then
      read_float()
    else if char = 9 then
      read_float()
    else if char = 46 then
      read_float3 0.0 0.1
    else if char = 45 then
      -.(read_float2 0.0)
    else if 48 <= char then
      if char <= 57 then
        (* num *)
        read_float2 (float_of_int (char - 48))
      else
          0.0
      else
          0.0 in

noinline let rec print_int n =
  let rec print_int3 n u uc c =
    if n < (uc + u) then
      (print_char c;
       (* div10 *)
       let u3 = ([rshift] u 1) + ([rshift] u 2) in
       let u3 = u3 + ([rshift] u3 4) in
       let u3 = u3 + ([rshift] u3 8) in
       let u3 = u3 + ([rshift] u3 16) in
       let u3 = [rshift] u3 3 in
       let r = u - ([rshift] (([rshift] u3 2) + u3) 1) in
       let u3 = u3 + (if r > 9 then 1 else 0) in
         if u3 = 0 then ()
         else
           print_int3 (n - uc) u3 0 48)
    else
      print_int3 n u (uc+u) (c+1) in
  let rec print_int2 n u =
    let u2 = ([lshift] u 3) + ([lshift] u 1) in
      if n < u2 then
        print_int3 n u 0 48
      else
        print_int2 n u2 in
  if n < 0 then
    print_char 45;
    print_int2 (-n) 1
  else
    print_int2 n 1
      in
let rec print_newline _ =
  print_char 10
    in

noinline let rec read_int _ =
  let rec read_int2 v s =
    let c = read_char() in
      if c >= 48 then
        (if c <= 57 then
           (let v2 = ([lshift] v 3) + ([lshift] v 1) in
              read_int2 (v2+c-48) s)
         else
           (if s = 0 then
              v
            else
              -v))
      else
        (if s = 0 then
           v
         else
           -v) in
  let c = read_char() in
    if c >= 48 then
      if c <= 57 then
        read_int2 (c-48) 0
      else
        0
    else if c = 32 then
      read_int()
    else if c = 10 then
      read_int()
    else if c = 13 then
      read_int()
    else if c = 9 then
      read_int()
    else if c = 45 then
      read_int2 0 1 
    else
      0
  in

let rec cos_kernel x sgn =
  let x2 = x *. x in
  let x4 = x2 *. x2 in
  let x6 = x2 *. x4 in
  let b = 1.0 -. 0.5*.x2 +. 0.04166368*.x4 -. 0.0013695068*.x6 in
    [add] b sgn in
let rec sin_kernel x sgn =
  let x2 = x *. x in
  let x3 = x *. x2 in
  let x5 = x2 *. x3 in
  let x7 = x2 *. x5 in
  let b = x -. 0.16666668*.x3 +. 0.008332824*.x5 -. 0.00019587841*.x7 in
    [add] b sgn in

noinline let rec cos x =
  let rec cos4 x pi pi2 pi05 sgn =
    (* normalize to [0, pi/2] *)
    if x >= pi05 then
      sin_kernel (x -. pi05) ([add] sgn (-2147483648))
    else
      cos_kernel x sgn in
  let rec cos3 x pi pi2 pi05 =
    (* normalize to [0, pi] *)
    if x >= pi then
      cos4 (x -. pi) pi pi2 pi05 (-2147483648)
    else
      cos4 x pi pi2 pi05 0 in
  let rec cos2 x pi pi2 pi05 =
    (* normalize to [0, 2pi] *)
    if x >= pi2 then
      cos2 (x -. pi2) pi pi2 pi05
    else
      cos3 x pi pi2 pi05 in
  cos2 ([fabs] x) 3.1415926536 6.28318531 1.5707963268
  in
noinline let rec sin x =
  let rec sin4 x pi pi2 pi05 sgn =
    if x >= pi05 then
      cos_kernel (x -. pi05) sgn
    else
      sin_kernel x sgn in
  let rec sin3 x pi pi2 pi05 sgn =
    if x >= pi then
      sin4 (x -. pi) pi pi2 pi05 ([add] sgn (-2147483648))
    else
      sin4 x pi pi2 pi05 sgn in
  let rec sin2 x pi pi2 pi05 sgn =
    if x >= pi2 then
      sin2 (x -. pi2) pi pi2 pi05 sgn
    else
      sin3 x pi pi2 pi05 sgn in
  sin2 ([fabs] x) 3.1415926536 6.28318531 1.5707963268 ([lshift] ([rshift] x 31) 31)
in

let rec atan_kernel x sgn b sgn2 =
  (* Taylor *)
  let x2 = x *. x in
  let x3 = x *. x2 in
  let x4 = x2 *. x2 in
  let x5 = x2 *. x3 in
  let x7 = x3 *. x4 in
  let x9 = x4 *. x5 in
  let x11= x4 *. x7 in
  let x13= x4 *. x9 in
  let body = x -. 0.3333333*.x3 +. 0.2*.x5 -. 0.142857142*.x7 +. 0.111111104*.x9 -.0.08976446*.x11 +. 0.060035485*.x13 in
  let body2 = [add] body sgn2 in
  let body3 = b +. body2 in
    [add] body3 sgn in
let rec atan x =
  let sgn = ([lshift] ([rshift] x 31) 31) in
  let xa = [fabs] x in
  if xa < 0.4375 then
    atan_kernel xa sgn 0.0 0
  else
    if xa < 2.4375 then
      let pi025 = 0.78539816 in (* PI/4 *)
        atan_kernel ((xa -. 1.0) /. (xa +. 1.0)) sgn pi025 0
    else
      let pi05 = 1.5707963268 in (* PI/2 *)
        atan_kernel (1.0 /. xa) sgn pi05 (-2147483648)
                                   in
  ()


