open Asm

external gethi : float -> int32 = "gethi"
external getlo : float -> int32 = "getlo"

(* Syntax.pos_tをコメント文字列にする *)
let posstr (p : Syntax.pos_t) =
  if p.Syntax.s.Syntax.line < 0 then
    (* it's dummy *)
    ""
  else
      Printf.sprintf "# %s" (Syntax.pos_str p)
(* リストの先頭からn個とる *)
let rec listtake n = function
  | [] -> []
  | _ when n<=0 -> []
  | x::xs -> x::listtake (n-1) xs

(* タプルどうしのor *)
let or2 (a,b) (c,d) = (a || c, b || d)

(* 命令列がsaveを含むか（Callも暗黙のうちにsaveすると考える） *)
let rec has_save e =
  let rec has_save' = function
    | Nop | Add(_) | Sub(_) | LShift(_) | RShift(_) | CmpEq(_) | CmpGt(_) | CmpLt(_) | CmpFEq(_) | CmpFGt(_) | FAdd(_) | FSub(_) | FMul(_) | FNeg(_) | FInv(_) | FSqrt(_) | FAbs(_) | Ld(_) | Sto(_) | Out(_) | In(_) | Halt(_) | Comment(_)
    | Mul(_) | Div(_) | Set(_) | SetF(_) | SetL(_) | Mov(_) | Neg(_) | Restore(_) ->
        false
    | If(_,_,(_,e1,e2)) | IfNot(_,_,(_,e1,e2)) ->
        has_save e1 || has_save e2
    | Save(_)
    | CallCls(_) | CallDir(_) -> true in
    match e with
      | Ans e1 -> has_save' e1
      | Let(_,e1,e') ->
          has_save' e1 || has_save e'

(* 関数の引数と返り値レジスタ *)
let fun_regs = ref M.empty

let stackset = ref S.empty (* すでにSaveされた変数の集合 (caml2html: emit_stackset) *)
let stackmap = ref [] (* Saveされた変数の、スタックにおける位置 (caml2html: emit_stackmap) *)

let stack_depth_max = ref 0 (* スタックの深さの最大値 *)
(* スタック変数のprefix *)
let stackvar_prefix = "min_caml_stackvar_"

let stacksize () = List.length !stackmap

let save x =
  stackset := S.add x !stackset;
  (if not (List.mem x !stackmap) then
     stackmap := !stackmap @ [x];
   (if stacksize() > !stack_depth_max then
      stack_depth_max := stacksize()))
let locate x =
  let rec loc = function
    | [] -> []
    | y :: zs when x = y -> 0 :: List.map succ (loc zs)
    | y :: zs -> List.map succ (loc zs) in
  loc !stackmap
let offset x = List.hd (locate x)

let pp_id_or_imm = function
  | V(x) -> x
  | C(i) -> string_of_int i

(* 関数呼び出しのために引数を並べ替える(register shuffling) (caml2html: emit_shuffle) *)
let rec shuffle sw xys =
  (* remove identical moves *)
  let _, xys = List.partition (fun (x, y) -> x = y) xys in
  (* find acyclic moves *)
  match List.partition (fun (_, y) -> List.mem_assoc y xys) xys with
  | [], [] -> []
  | (x, y) :: xys, [] -> (* no acyclic moves; resolve a cyclic move *)
      (y, sw) :: (x, y) :: shuffle sw (List.map
					 (function
					   | (y', z) when y = y' -> (sw, z)
					   | yz -> yz)
					 xys)
  | xys, acyc -> acyc @ shuffle sw xys

type dest = Tail of Id.t * Id.t * bool * bool | NonTail of Id.t * Id.t * bool * bool (* 末尾かどうかを表すデータ型 (caml2html: emit_dest) *)
(* Tail (関数名, 関数の返り値レジスタ，リンクレジスタが保存されたか, スタックポインタが掘られたか)
 * NonTail (関数名, レジスタ名, リンクレジスタ保存済か, スタックポインタが掘られたか) *)



(* 返り値：　命令列が関数呼び出しを含んでいたか *)
let rec g oc = function (* 命令列のアセンブリ生成 (caml2html: emit_g) *)
  | dest, Ans(exp) -> g' oc (dest, exp)
  | dest, Let((x, t), exp, e) ->
      (match dest with
         | Tail(f, r, hc, sp) ->
             let (hc2, sp2) = g' oc (NonTail(f, x, hc, sp), exp) in
               or2 (g oc (Tail(f, r, hc || hc2, sp || sp2), e)) (hc2, sp2)
         | NonTail(f, y, hc, sp) ->
             let (hc2, sp2) = g' oc (NonTail(f, x, hc, sp), exp) in
               or2 (g oc (NonTail(f, y, hc || hc2, sp || sp2), e)) (hc2, sp2))

and g' oc = function (* 各命令のアセンブリ生成 (caml2html: emit_gprime) *)
  (* 末尾でなかったら計算結果をdestにセット (caml2html: emit_nontail) *)
  | NonTail(_), Nop -> (false,false)
  | NonTail(_,x,_,_), Add(p,(y, z')) ->
      (match z' with
         | C(i) ->
             (* TODO: 14bit即値の範囲チェック *)
             assert (-0x2000 <= i && i < 0x2000);
             Printf.fprintf oc "\taddi\t%s, %s, %d\t\t\t%s\n" x y i (posstr p)
         | V(z) ->
             Printf.fprintf oc "\tadd\t%s, %s, %s\t\t\t%s\n" x y z (posstr p));
      (false, false)
  | NonTail(_,x,_,_), Sub(p,(y', z)) ->
      (match y' with
         | C(i) ->
             (* TODO: 14bit即値の範囲チェック *)
             assert (-0x2000 < i && i <= 0x2000);
             Printf.fprintf oc "\tsubi\t%s, %d, %s\t\t\t%s\n" x i z (posstr p)
         | V(y) ->
             Printf.fprintf oc "\tsub\t%s, %s, %s\t\t\t%s\n" x y z (posstr p));
      (false, false)
  | NonTail(_,x,_,_), LShift(p,(y, z')) ->
      (match z' with
         | C(i) ->
             (* 6bit -32は許さないので注意 *)
             assert (-0x20 < i && i < 0x20);
             Printf.fprintf oc "\tlshifti\t%s, %s, %d\t\t\t%s\n" x y i (posstr p)
         | V(z) ->
             Printf.fprintf oc "\tlshift\t%s, %s, %s\t\t\t%s\n" x y z (posstr p));
      (false,false)
  | NonTail(_,x,_,_), RShift(p,(y, z')) ->
      (match z' with
         | C(i) ->
             (* 6bit -32は許さないので注意 *)
             assert (-0x20 < i && i < 0x20);
             Printf.fprintf oc "\trshifti\t%s, %s, %d\t\t\t%s\n" x y i (posstr p)
         | V(z) ->
             Printf.fprintf oc "\trshift\t%s, %s, %s\t\t\t%s\n" x y z (posstr p));
      (false,false)
  | NonTail(_,x,_,_), CmpEq(p,(y, z')) ->
      (match z' with
         | C(i) ->
             assert (-0x2000 <= i && i < 0x2000);
             Printf.fprintf oc "\tcmpeqi\t%s, %s, %d\t\t\t%s\n" x y i (posstr p)
         | V(z) ->
             Printf.fprintf oc "\tcmpeq\t%s, %s, %s\t\t\t%s\n" x y z (posstr p));
      (false,false)
  | NonTail(_,x,_,_), CmpGt(p,(y, z')) ->
      (match z' with
         | C(i) ->
             assert (-0x2000 <= i && i < 0x2000);
             Printf.fprintf oc "\tcmpgti\t%s, %s, %d\t\t\t%s\n" x y i (posstr p)
         | V(z) ->
             Printf.fprintf oc "\tcmpgt\t%s, %s, %s\t\t\t%s\n" x y z (posstr p));
      (false,false)

  | NonTail(_,x,_,_), CmpLt(p,(y, z')) ->
      (match z' with
         | C(i) ->
             assert (-0x2000 <= i && i < 0x2000);
             Printf.fprintf oc "\tcmplti\t%s, %s, %d\t\t\t%s\n" x y i (posstr p)
         | V(z) ->
             Printf.fprintf oc "\tcmplt\t%s, %s, %s\t\t\t%s\n" x y z (posstr p));
      (false,false)
  | NonTail(_,x,_,_), CmpFEq(p,(y, z)) ->
      Printf.fprintf oc "\tcmpfeq\t%s, %s, %s\t\t\t%s\n" x y z (posstr p);
      (false,false)
  | NonTail(_,x,_,_), CmpFGt(p,(y, z)) ->
      Printf.fprintf oc "\tcmpfgt\t%s, %s, %s\t\t\t%s\n" x y z (posstr p);
      (false,false)
  | NonTail(_,x,_,_), FAdd(p,(y, z')) ->
      (match z' with
         | F(i) ->
             let f = Int32.bits_of_float i in
               assert(Int32.logand (Int32.of_int 0x3ffff) f = Int32.zero);
               let i2 = Int32.shift_right_logical f 18 in
                 Printf.fprintf oc "\tfaddi\t%s, %s, %d\t\t\t%s\n" x y (Int32.to_int i2) (posstr p)
         | Vf(z) ->
             Printf.fprintf oc "\tfadd\t%s, %s, %s\t\t\t%s\n" x y z (posstr p));
      (false,false)
  | NonTail(_,x,_,_), FSub(p,(y', z)) ->
      (match y' with
         | F(i) ->
             let f = Int32.bits_of_float i in
               assert(Int32.logand (Int32.of_int 0x3ffff) f = Int32.zero);
               let i2 = Int32.shift_right_logical f 18 in
                 Printf.fprintf oc "\tfsubi\t%s, %d, %s\t\t\t%s\n" x (Int32.to_int i2) z (posstr p)
         | Vf(y) ->
             Printf.fprintf oc "\tfsub\t%s, %s, %s\t\t\t%s\n" x y z (posstr p));
      (false,false)
  | NonTail(_,x,_,_), FMul(p,(y, z')) ->
      (match z' with
         | F(i) ->
             let f = Int32.bits_of_float i in
               assert(Int32.logand (Int32.of_int 0x3ffff) f = Int32.zero);
               let i2 = Int32.shift_right_logical f 18 in
                 Printf.fprintf oc "\tfmuli\t%s, %s, %d\t\t\t%s\n" x y (Int32.to_int i2) (posstr p)
         | Vf(z) ->
             Printf.fprintf oc "\tfmul\t%s, %s, %s\t\t\t%s\n" x y z (posstr p));
      (false,false)
  | NonTail(_,x,_,_), FNeg(p,y) ->
      Printf.fprintf oc "\tfneg\t%s, %s\t\t\t%s\n" x y (posstr p);
      (false,false)
  | NonTail(_,x,_,_), FInv(p, y) ->
      Printf.fprintf oc "\tfinv\t%s, %s\t\t\t%s\n" x y (posstr p);
      (false,false)
  | NonTail(_,x,_,_), FSqrt(p, y) ->
      Printf.fprintf oc "\tfsqrt\t%s, %s\t\t\t%s\n" x y (posstr p);
      (false,false)
  | NonTail(_,x,_,_), FAbs(p, y) ->
      Printf.fprintf oc "\tfabs\t%s, %s\t\t\t%s\n" x y (posstr p);
      (false,false)
  | NonTail(_,x,_,_), Ld(p, m) ->
      (match m with
         | Vm(y, i) ->
             (* 14bit *)
             assert (-0x2000 <= i && i < 0x2000);
             Printf.fprintf oc "\tld\t%s, %d(%s)\t\t\t%s\n" x i y (posstr p)
         | Vml(Id.L(l), i) ->
             (* label and imm *)
             if i = 0 then
               Printf.fprintf oc "\tld\t%s, %s\t%s\n" x l (posstr p)
             else
               Printf.fprintf oc "\tld\t%s, &ADD(%s, %d)\t%s\n" x l i (posstr p)
         | I(i) ->
             (* 20bit *)
             assert (-0x80000 <= i && i < 0x80000);
             Printf.fprintf oc "\tld\t%s, %d\t\t\t%s\n" x i (posstr p));
      (false,false)
  | NonTail(_), Sto(p,(x, m)) ->
      (match m with
         | Vm(y, i) ->
             (* 14bit *)
             assert (-0x2000 <= i && i < 0x2000);
             Printf.fprintf oc "\tsto\t%s, %d(%s)\t\t\t%s\n" x i y (posstr p)
         | Vml(Id.L(l), i) ->
             if i = 0 then
               Printf.fprintf oc "\tsto\t%s, %s\t%s\n" x l (posstr p)
             else
               Printf.fprintf oc "\tsto\t%s, &ADD(%s, %d)\t%s\n" x l i (posstr p)
         | I(i) ->
             (* 20bit *)
             assert (-0x80000 <= i && i < 0x80000);
             Printf.fprintf oc "\tsto\t%s, %d\t\t\t%s\n" x i (posstr p));
      (false,false)
  | NonTail(_), Out(p, x) ->
      Printf.fprintf oc "\tout\t%s\t\t\t%s\n" x (posstr p);
      (false,false)
  | NonTail(_,x,_,_), In(p) ->
      Printf.fprintf oc "\tin\t%s\t\t\t%s\n" x (posstr p);
      (false,false)
  | NonTail(_), Halt(p) ->
      Printf.fprintf oc "\thalt\t\t\t%s" (posstr p);
      (false,false)
  | NonTail(_), Mul(p,_) -> assert false
  | NonTail(_), Div(p,_) -> assert false
  | NonTail(_,x,_,_), Set(p,i) ->
      (* 即値の代入 *)
      (if -0x8000 <= i && i < 0x8000 then
        (* 20bitで表すことができる *)
        Printf.fprintf oc "\tmovi\t%s, %d\t\t\t%s\n" x i (posstr p)
      else
          (* 下位12bitが0なら *)
          if (i land 0xFFF) = 0 then
            Printf.fprintf oc "\tmovhiz\t%s, %d\t\t\t%s\n" x ((i lsr 12) land 0xFFFFF) (posstr p)
          else
            (* そうでない *)
            (Printf.fprintf oc "\tmovhiz\t%s, %d\t\t\t%s\n" x ((i lsr 12) land 0xFFFFF) (posstr p);
             Printf.fprintf oc "\taddi\t%s, %s, %d\t%s\n" x x (i land 0xFFF) (posstr p)));
      (false,false)
  | NonTail(_,x,_,_), SetF(p,i) ->
      (* 浮動小数点数の即値 *)
      let f = Int32.bits_of_float i in
        if (Int32.logand f (Int32.of_int 0xFFF)) = Int32.zero then
          (* 下位12bitsが0 *)
          Printf.fprintf oc "\tmovhiz\t%s, %d\t\t%s\n" x (Int32.to_int (Int32.shift_right_logical f 12)) (posstr p)
        else
            if (Int32.logand f (Int32.of_int 0xfff00000)) = Int32.zero then
              (* 下位20bitsしかない *)
              Printf.fprintf oc "\tmovi\t%s, %d\t\t%s\n" x (Int32.to_int f) (posstr p)
            else
                (Printf.fprintf oc "\tmovhiz\t%s, %d\t\t%s\n" x (Int32.to_int (Int32.shift_right_logical f 12)) (posstr p);
                 Printf.fprintf oc "\taddi\t%s, %s, %d\t%s\n" x x (Int32.to_int (Int32.logand f (Int32.of_int 0xfff))) (posstr p));
        (false,false)
  | NonTail(_,x,_,_), SetL(p,(Id.L(y), i)) ->
      if i = 0 then
        Printf.fprintf oc "\tmovi\t%s, %s\t\t\t%s\n" x y (posstr p)
      else
        Printf.fprintf oc "\tmovi\t%s, &ADD(%s, %d)\t\t\t%s\n" x y i (posstr p);
      (false,false)
  | NonTail(_,x,_,_), Mov(p,y) ->
      if x <> y then Printf.fprintf oc "\tmov\t%s, %s\t\t\t%s\n" x y (posstr p);
      (false,false)
  | NonTail(_,x,_,_), Neg(p,y) ->
      Printf.fprintf oc "\tneg\t%s, %s\t\t\t%s\n" x y (posstr p);
      (false,false)
  | NonTail(_), Comment(p,s) -> Printf.fprintf oc "\t# %s\t\t\t%s\n" s (posstr p); (false,false)
  (* 退避の仮想命令の実装 (caml2html: emit_save) *)
  | NonTail(f,_,_,sp_dig), Save(p,(x, y)) when List.mem x allregs && not (S.mem y !stackset) ->
      (* 変数yをレジスタxからスタックへ *)
      save y;
      (* まだスタックポインタを掘ってなかったら掘る *)
      if not sp_dig then
        g'_sp_dig oc f;
      Printf.fprintf oc "\tsto\t%s, %d(%s)\t\t\t%s\n" x (-(offset y)) reg_sp (posstr p);
      (false,true)
  | NonTail(_), Save(p,(x, y)) -> assert (S.mem y !stackset); (false,false)
  (* 復帰の仮想命令の実装 (caml2html: emit_restore) *)
  | NonTail(_,x,_,sp_dig), Restore(p,y) when List.mem x allregs ->
      assert sp_dig;
      Printf.fprintf oc "\tld\t%s, %d(%s)\t\t\t%s\n" x (-(offset y)) reg_sp (posstr p);
      (false,false)
  (* 末尾だったら計算結果を第一レジスタにセットしてret (caml2html: emit_tailret) *)
  | NonTail(_,x,_,_), Restore(_,_) -> Printf.eprintf "!!! %s\n" x; assert false
  | Tail(f, _, lnk_restore, sp_dig), (Nop | Sto _ | Out _ | Halt _ | Comment _ | Save _ as exp) ->
      (* 計算結果がない *)
      let (hc2, sp2) = g' oc (NonTail(f, Id.gentmp Type.Unit, lnk_restore, sp_dig), exp) in
      g'_ret oc f (lnk_restore || hc2) (sp_dig || sp2);
      (false,false)
  | Tail(f, r, lnk_restore, sp_dig), (Add _ | Sub _ | LShift _ | RShift _ | CmpEq _ | CmpGt _ | CmpLt _ | CmpFEq _ | CmpFGt _ | FAdd _ | FSub _ | FMul _ | FNeg _ | FInv _ | FSqrt _ | FAbs _ | Ld _ | In _ | Mul _ | Div _ | Set _ | SetF _ | SetL _ | Mov _ | Neg _ as exp) ->
      let (hc2, sp2) = g' oc (NonTail(f, r, lnk_restore, sp_dig), exp) in
        (* どうせfalseだろうけど *)
        g'_ret oc f (lnk_restore || hc2) (sp_dig || sp2);
      (false, false)
  | Tail(f, r, lnk_restore, sp_dig), (Restore(p,x) as exp) ->
      ignore (match locate x with
                | [i] -> g' oc (NonTail(f, r, lnk_restore, sp_dig), exp)
                | _ -> assert false);
      g'_ret oc f lnk_restore sp_dig;
      (false, false)
  | Tail(_) as dest , If(p1,p2,(x, e1, e2)) ->
      g'_tail_if oc p1 x dest e1 e2 "jmp" "jmpz"
  | Tail(_) as dest, IfNot(p1,p2,(x, e1, e2)) ->
      g'_tail_if oc p1 x dest e1 e2 "jmpz" "jmp"
  | NonTail(f, z, lnk_restore, sp_dig) as dest, If(p1,p2,(x, e1, e2)) ->
      g'_non_tail_if oc p1 x (f,z,lnk_restore,sp_dig) e1 e2 "jmp" "jmpz"
  | NonTail(f, z, lnk_restore, sp_dig) as dest, IfNot(p1,p2,(x, e1, e2)) ->
      g'_non_tail_if oc p1 x (f,z,lnk_restore,sp_dig) e1 e2 "jmpz" "jmp"
  (* 関数呼び出しの仮想命令の実装 (caml2html: emit_call) *)
  | Tail(f, _, lnk_restore, sp_dig), CallCls(p,(x, ys, zs)) -> (* 末尾呼び出し (caml2html: emit_tailcall) *)
      (* TODO XXX クロージャの場合はスタックに積んであれする *)
      assert false;
      g'_args oc p [(x, reg_cl)] "" ys zs;
      g'_lnk_restore oc lnk_restore;
      g'_sp_restore oc f sp_dig;
      Printf.fprintf oc "\tjmpu\t%s\t\t\t%s\n" reg_cl (posstr p);
      (true,true)
  | Tail(f, r, lnk_restore, sp_dig), (CallDir(p,(Id.L(x), ys, zs)) as exp) -> (* 末尾呼び出し *)
      (* 関数の返り値がどのレジスタに入るか調べる *)
      let x_ret = RegAlloc.retof x in
        (if r = x_ret then
           (* 返り値レジスタが一緒なので末尾呼出できる *)
           (g'_args oc p [] x ys zs;
            g'_lnk_restore oc lnk_restore;
            g'_sp_restore oc f sp_dig;
            Printf.fprintf oc "\tjmpui\t%s\t\t\t%s\n" x (posstr p))
         else
             (* そうでないなら中継が必要 *)
             (ignore (g' oc (NonTail(f, r, lnk_restore, sp_dig), exp));
              g'_ret oc f true true));
      (true, true)
  | NonTail(f, a, lnk_restore, sp_dig), CallCls(p,(x, ys, zs)) ->
      assert false;
      g'_args oc p [(x, reg_cl)] "" ys zs;
      (* リターンアドレスを保存 *)
      Printf.fprintf oc "\tmovi\t%s, &PC(3)\t\t\t%s\n" reg_lnk (posstr p);
      (* 関数のアドレスを得る *)
      (* XXX ad hocだが引数が59個無い限り壊れないと思う *)
      Printf.fprintf oc "\tld\t%s, (%s)\t\t\t%s\n" "%r59" reg_cl (posstr p);
      (* 関数呼び出し *)
      Printf.fprintf oc "\tjmpu\t%s\t\t\t%s\n" "%r59" (posstr p);
      if List.mem a allregs && a <> regs.(0) then
        (* 返り値を目的のレジスタに入れる *)
        Printf.fprintf oc "\tmov\t%s, %s\t\t\t%s\n" a regs.(0) (posstr p);
      (true,true)
  | NonTail(f, a, lnk_restore, sp_dig), CallDir(p,(Id.L(x), ys, zs)) ->
      (* 自由変数のない関数の呼び出し *)
      if not sp_dig then
        g'_sp_dig oc f;
      if not lnk_restore then
        g'_lnk_save oc;
      let x_ret = RegAlloc.retof x in
      g'_args oc p [] x ys zs;
      Printf.fprintf oc "\tcall\t%s, %s\t\t\t%s\n" reg_lnk x (posstr p);
      if List.mem a allregs && a <> x_ret then
        Printf.fprintf oc "\tmov\t%s, %s\t\t\t%s\n" a x_ret (posstr p);
      (true, true)
and g'_tail_if oc p cond dest e1 e2 _ bn =
  (* tail ifをつくる *)
  let b_else = Id.genid ("else") in
  Printf.fprintf oc "\t%si\t %s, %s\t\t\t%s\n" bn cond b_else (posstr p);
  let stackset_back = !stackset in
  let hc1,sp1 = g oc (dest, e1) in
  Printf.fprintf oc "%s:\t\t\t\t%s\n\n" b_else (posstr p);
  stackset := stackset_back;
  let hc2,sp2 = g oc (dest, e2) in
    (hc1 || hc2, sp1 || sp2)
and g'_non_tail_if oc p cond (f,dest,lnk_restore,sp_dig) e1 e2 _ bn =
  (* non-tail ifの出力 *)
  (* e1とe2の一方にcallがあったら仕方ないからlnkをsave *)
  let sp_dig' =
    if (not sp_dig) && (has_save e1 || has_save e2) then
      (g'_sp_dig oc f;
       true)
    else
      sp_dig in
  let lnk_restore' =
    if (not lnk_restore) && (has_call e1 || has_call e2) then
      (g'_lnk_save oc;
       true)
    else
      lnk_restore in
  let b_else = Id.genid ("else") in
  let b_cont = Id.genid ("if_cont") in
  Printf.fprintf oc "\t%si\t%s, %s\t\t\t%s\n" bn cond b_else (posstr p);
  let stackset_back = !stackset in
  let hc1,sp1 = g oc (NonTail(f, dest, lnk_restore', sp_dig'), e1) in
  let stackset1 = !stackset in
  Printf.fprintf oc "\tjmpui\t%s\t\t\t%s\n" b_cont (posstr p);
  Printf.fprintf oc "%s:\t\t\t%s\n" b_else (posstr p);
  stackset := stackset_back;
  let hc2,sp2 = g oc (NonTail(f, dest, lnk_restore', sp_dig'), e2) in
  Printf.fprintf oc "%s:\t\t\t%s\n" b_cont (posstr p);
  let stackset2 = !stackset in
  stackset := S.inter stackset1 stackset2;
  (hc1 || hc2, sp1 || sp2)
and g'_args oc p x_reg_cl x ys zs =
  assert ((List.length ys + List.length zs) <= Array.length regs - List.length x_reg_cl);
  (* cyclic movesのために一時的にあれする場所 *)
  let sw = Printf.sprintf "%d(%s)" (stacksize ()) reg_sp in
  (* 呼び出し対象の関数の引数とか *)
  let argregs = try
    (let (args, fargs, _) = M.find x (!fun_regs) in (args@fargs))
  with Not_found ->
    (* 外部関数はわからないからr1からにする *) listtake (List.length ys + List.length zs) allregs in
  (* レジスタにいれるべきやつ（引数とか）をまとめる *)
  let yrs = List.combine (ys@zs) argregs in
  List.iter
    (function
       | (y, r) when y="_SW_" ->
           Printf.fprintf oc "\tld\t%s, %s\t\t\t%s\n" r sw (posstr p)
       | (y, r) when r="_SW_" ->
           Printf.fprintf oc "\tsto\t%s, %s\t\t\t%s\n" y sw (posstr p)
       | (y, r) ->
           Printf.fprintf oc "\tmov\t%s, %s\t\t\t%s\n" r y (posstr p))
    (shuffle "_SW_" yrs)
(* 必要ならリンクレジスタを戻してからjmp *)
and g'_lnk_restore oc lnk_restore =
  if lnk_restore then
    (Printf.fprintf oc "\tld\t%s, (%s)\n" reg_lnk reg_sp);
and g'_sp_restore oc f sp_dig =
  if sp_dig then
    Printf.fprintf oc "\taddi\t%s, %s, &MUL(-1, &VAR(%s))\n" reg_sp reg_sp (stackvar_prefix ^ f);
and g'_ret oc f lnk_restore sp_dig =
  g'_lnk_restore oc lnk_restore;
  g'_sp_restore oc f sp_dig;
  Printf.fprintf oc "\tjmpu\t%s\n" reg_lnk
and g'_lnk_save oc =
  Printf.fprintf oc "\tsto\t%s, (%s)\n" reg_lnk reg_sp
and g'_sp_dig oc f =
  Printf.fprintf oc "\taddi\t%s, %s, &VAR(%s)\n" reg_sp reg_sp (stackvar_prefix ^ f)

let h oc { name = Id.L(x); args = _; fargs = _; body = e; ret = _; ret_reg = r } =
  Printf.fprintf oc ".globl %s\n" x;
  Printf.fprintf oc "%s:\n" x;

  (* あらかじめスタックを掘っておく *)
  (*Printf.fprintf oc "\taddi\t%s, %s, &VAR(%s)\n" reg_sp reg_sp (stackvar_prefix ^ x);*)
  stackset := S.empty;
  stackmap := [];
  stack_depth_max := 0;
  let hc = has_call e in
  if hc then
    (* 関数呼び出しがあるからリンクレジスタを退避するスペースを確保
    * （実際の退避は必要になるまで遅延） *)
    save reg_lnk;
     (*Printf.fprintf oc "\tsto\t%s, (%s)\n" reg_lnk reg_sp);*)
    
  ignore(g oc (Tail(x, r, false, false), e));
  Printf.fprintf oc ".define %s, %d\n" (stackvar_prefix ^ x) (!stack_depth_max)

let f oc (Prog(fundefs, globals, e)) =
  Format.eprintf "generating assembly...@.";
  (match globals with
     | [] -> ()
     | _ ->
         (* globalsに何かあれば.dataセクションを出力する *)
         Printf.fprintf oc ".data\n";
         List.iter (fun {name = Id.L(l); size = size} ->
                      Printf.fprintf oc ".globl %s\n" l;
                      Printf.fprintf oc "%s:\n" l;
                      Printf.fprintf oc "\t.space %d\n" size) globals;
         Printf.fprintf oc "\n");

  Printf.fprintf oc ".text\n";
  (* 関数の引数と返り値のレジスタを共有する *)
  List.iter (fun { name = Id.L(x); args = args; fargs = fargs; ret_reg = r} ->
               fun_regs := M.add x (args, fargs, r) (!fun_regs)) fundefs;
  (* mainの出力 *)
  let mcs = Id.genid "min_caml_start" in
  if e <> Ans Nop then
    (* Nopだけなら何も出力しない *)
    (Printf.fprintf oc ".globl %s\n" mcs;
     Printf.fprintf oc ".main %s\n" mcs;
     Printf.fprintf oc "%s:\n" mcs;
     Printf.fprintf oc "\taddi\t%s, %s, &VAR(%s)\n" reg_sp reg_sp (stackvar_prefix ^ "_main");
  let _ = g oc (NonTail("_main",regs.(0), false,false), e) in
    Printf.fprintf oc ".define %s, %d\n" (stackvar_prefix ^ "_main") (!stack_depth_max);
    (* 必要ならhaltを出力する命令 *)
    Printf.fprintf oc "\t.end\n");
  (* 関数たち *)
  stackset := S.empty;
  stackmap := [];
  stack_depth_max := 0;
  List.iter (fun fundef -> h oc fundef) fundefs
