export let rec fless x y = x < y in
export let rec fispos x = x > 0.0 in
export let rec fisneg x = x < 0.0 in
export let rec fiszero x = (x = 0.0) in
export let rec fhalf x = x *. 0.5 in
export let rec fsqr x = x *. x in
export let rec sqrt x =
  let xh = x *. 0.5 in
  let x1 = x /. 2.0 +. 0.5 in
  let x2 = x1 /. 2.0 +. xh /. x1 in
  let x3 = x2 /. 2.0 +. xh /. x2 in
  let x4 = x3 /. 2.0 +. xh /. x3 in
  let x5 = x4 /. 2.0 +. xh /. x4 in
    x5 in
export let rec read_int _ =
  let rec read_int2 c =
    let char = read_char() in
      if char = 32 then
        c
      else if char = 10 then
        c
      else if char = 13 then
        c
      else if char = 9 then
        c
      else if 48 <= char then
        if char <= 57 then
          (* num *)
          read_int2 (c*10 + (char - 48))
        else
            c
      else
            c in
  let char = read_char() in
    if char = 32 then
      read_int()
    else if char = 10 then
      read_int()
    else if char = 13 then
      read_int()
    else if char = 9 then
      read_int()
    else if 48 <= char then
      if char <= 57 then
        (* num *)
        read_int2 (char - 48)
      else
          0
      else
          0 in
export let rec read_float _ =
  let rec read_float3 u c =
    let char = read_char() in
      if char = 32 then
        c
      else if char = 10 then
        c
      else if char = 13 then
        c
      else if char = 9 then
        c
      else if 48 <= char then
        if char <= 57 then
          (* num *)
          read_float3 (u *. 0.1) (c *. 10.0 +. u *. (float_of_int (char - 48)))
        else
            c
      else
            c in
  let rec read_float2 c =
    let char = read_char() in
      if char = 32 then
        c
      else if char = 10 then
        c
      else if char = 13 then
        c
      else if char = 9 then
        c
      else if char = 46 then
        (* '.' *)
        read_float3 0.1 c
      else if 48 <= char then
        if char <= 57 then
          (* num *)
          read_float2 (c *. 10.0 +. (float_of_int (char - 48)))
        else
            c
      else
            c in
  let char = read_char() in
    if char = 32 then
      read_float()
    else if char = 10 then
      read_float()
    else if char = 13 then
      read_float()
    else if char = 9 then
      read_float()
    else if 48 <= char then
      if char <= 57 then
        (* num *)
        read_float2 (float_of_int (char - 48))
      else
          0.0
      else
          0.0 in
  ()
