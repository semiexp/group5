open Asm

type memimm = MISum of Id.t * int | MIImm of int | MISumf of Id.t * float | MIImmf of float | MILabel of Id.l * int

(* addまたはmovを出力する補助関数 *)
let addimm p x i =
  if i=0 then
    Mov(p, x)
  else
    Add(p, (x, C i))

let addimmf p x i =
  if i=0.0 then
    Mov(p, x)
  else
    FAdd(p, (x, F i))

(* floatが上位14bitのみで表されるか *)
let isfloatimm i =
  let f = Int32.bits_of_float i in
    Int32.logand f (Int32.of_int 0x3ffff) = Int32.zero

(* intが14bitにおさまるか *)
let isintimm i =
  -0x2000 <= i && i < 0x2000

let rec g env = function (* 命令列の即値最適化 (caml2html: simm13_g) *)
  | Ans(exp) ->
      (* かけざんとわりざんを処理 *)
      (match (muldiv env exp) with
         | None ->
             (* なにもない *)
             g' env exp
         | Some e ->
             g env e)
  | Let((x, t) as xt, exp, e) -> (*concat (g' env exp) xt (g env e)*)
      (match (muldiv env exp) with
         | Some e2 ->
             (* かけざんわりざんを処理してやり直す *)
             g env (concat e2 xt e)
         | None ->
             let exp' = g' env exp in
             if not (is_reg x) then
               let rec resolve_exp e1 (x, t) env f =
                 match e1 with
                   | Ans(Set(p, i)) ->
                       (* 変数xの中身はiであることが判明した *)
                       let e' = f (M.add x (MIImm i) env) in
                         if List.mem x (fv e') then Let((x, t), Set(p, i), e') else e'
                   | Ans(SetF(p, i)) ->
                       let e' = f (M.add x (MIImmf i) env) in
                         if List.mem x (fv e') then Let((x, t), SetF(p, i), e') else e'
                   | Ans(SetL(p, (l, i))) ->
                       let e' = f (M.add x (MILabel(l, i)) env) in
                         if List.mem x (fv e') || is_reg x then Let((x, t), SetL(p, (l, i)), e') else e'
                   | Ans(Add(p, (y, C i))) when not (is_reg y) ->
                       (* 変数の中身が値+即値 *)
                       let e' = f (M.add x (MISum (y, i)) env) in
                         if List.mem x (fv e') || is_reg x then
                           Let((x, t), Add(p, (y, C i)), e')
                         else
                           (
                             Printf.eprintf "deleting add %s <- %s + %d.\n" x y i;
                             e')
                   | Ans(Mov(p, y)) when not (is_reg y) ->
                       let env' = (match t with
                                     | Type.Float -> M.add x (MISumf(y, 0.0)) env
                                     | Type.Int -> M.add x (MISum(y, 0)) env
                                     | _ -> M.add x (MISum (y, 0)) env) in
                       let e' = f env' in
                         if List.mem x (fv e') || is_reg x then
                           Let((x, t), Mov(p, y), e')
                         else
                           (Printf.eprintf "deleting mov %s <- %s.\n" x y;
                            e')
                   | Ans(exp) ->
                       Let((x, t), exp, f env)
                   | Let(yt, exp', e2) ->
                       (* Let(yt, exp', resolve_exp e2 (x, t) env f) in *)
                       resolve_exp (Ans(exp')) yt env (fun env' -> resolve_exp e2 (x, t) env' f) in
                 resolve_exp exp' xt env (fun env -> g env e)
             else
               concat exp' xt (g env e))
and g' env = function (* 各命令の即値最適化 (caml2html: simm13_gprime) *)
  | Add(p,(x, V(y))) as exp when M.mem x env && M.mem y env ->
      (match (M.find x env, M.find y env) with
         | (MIImm i, MIImm j) ->
             Ans(Set(p, i+j))
         | (MIImm i, MISum(z, j)) | (MISum(z, i), MIImm j) when isintimm (i+j) ->
             Ans(addimm p z (i+j))
         | (MIImm i, MILabel(l, j)) | (MILabel(l, i), MIImm j) ->
             Ans(SetL(p, (l, i+j)))
         | (MISum(x', i), MISum(y', j)) when isintimm (i+j) ->
             (* x + y = (x' + i) + (y' + j) = (x' + y') + (i + j) *)
             let tmp = Id.gentmp Type.Int in
               Let((tmp, Type.Int),
                   Add(p,(x', V(y'))),
                   Ans(Add(p,(tmp, C(i+j)))))
         | (_, MISum(y', j)) ->
             let tmp = Id.gentmp Type.Int in
               Let((tmp, Type.Int),
                   Add(p,(x, V(y'))),
                   Ans(Add(p,(tmp, C(j)))))
         | (MISum(x', i), _) ->
             let tmp = Id.gentmp Type.Int in
               Let((tmp, Type.Int),
                   Add(p,(x', V(y))),
                   Ans(Add(p,(tmp, C(i)))))

         | (MISum(x', 0), MISum(y', 0)) ->
             Ans(Add(p, (x', V(y'))))
         | (MISum(x', 0), _) ->
             Ans(Add(p, (x', V(y))))
         | (_, MISum(y', 0)) ->
             Ans(Add(p, (x, V(y'))))
         | _ -> Ans(exp))
  | Add(p,(x, V(y))) as exp when M.mem y env ->
      (match M.find y env with
         | MIImm i when isintimm i ->
             Ans(addimm p x i)
         | MISum(y', 0) ->
             Ans(Add(p, (x, V(y'))))
         | _ -> Ans(exp))
  | Add(p,(x, V(y))) as exp when M.mem x env ->
      (match M.find x env with
         | MIImm i when isintimm i ->
             Ans(addimm p y i)
         | MISum(x', 0) ->
             Ans(Add(p, (x', V(y))))
         | _ -> Ans(exp))
  | Add(p,(x, C(i))) as exp when M.mem x env ->
      (match M.find x env with
         | MIImm j ->
             Ans(Set(p, i+j))
         | MISum(y, j) when isintimm (i+j) ->
             Ans(addimm p y (i+j))
         | MILabel(l, j) ->
             Ans(SetL(p, (l, i+j)))
         | _ -> Ans(exp))
  | Sub(p,(V(x), y)) as exp when M.mem x env && M.mem y env ->
      (match (M.find x env, M.find y env) with
         | (MIImm i, MIImm j) ->
             Ans(Set(p, i-j))
         | (MISum(z, i), MIImm j) when isintimm (i-j) ->
             Ans(addimm p z (i-j))
         | (MIImm i, MISum (z, j)) when isintimm (i-j) ->
             Ans(Sub(p,(C(i-j), z)))
         | (MIImm i, _) when isintimm i ->
             Ans(Sub(p,(C(i), y)))
         | (_, MIImm j) when isintimm j ->
             Ans(addimm p x (-j))
         | (MISum(x', 0), MISum(y', 0)) ->
             Ans(Sub(p,(V(x'), y')))
         | (MISum(x', 0), _) ->
             Ans(Sub(p,(V(x'), y)))
         | (_, MISum(y', 0)) ->
             Ans(Sub(p,(V(x), y')))
         | _ -> Ans(exp))
  | Sub(p,(V(x), y)) as exp when M.mem x env ->
      (match M.find x env with
         | MIImm i when isintimm i -> 
             Ans(Sub(p,(C(i), y)))
         | MISum(x', 0) ->
             Ans(Sub(p,(V(x'), y)))
         | _ -> Ans(exp))
  | Sub(p,(V(x), y)) as exp when M.mem y env ->
      (match M.find y env with
         | MIImm i when isintimm (-i) ->
             Ans(addimm p x (-i))
         | MISum(y', 0) ->
             Ans(Sub(p,(V(x), y')))
         | _ -> Ans(exp))
  | LShift(p,(x,V(y))) as exp when M.mem x env && M.mem y env ->
      (match (M.find x env, M.find y env) with
        | (MIImm i, MIImm j) ->
            Ans(Set(p, i lsl (j mod 32)))
        | (MISum(x', 0), MIImm j) when isintimm j ->
            Ans(LShift(p, (x', C(j))))
        | (_, MIImm j) when isintimm j ->
            Ans(LShift(p, (x, C(j))))
        | (MISum(x', 0), MISum(y', 0)) ->
            Ans(LShift(p, (x', V(y'))))
        | (MISum(x', 0), _) ->
            Ans(LShift(p, (x', V(y))))
        | (_, MISum(y', 0)) ->
            Ans(LShift(p, (x, V(y'))))
        | _ -> Ans(exp))
  | LShift(p,(x,V(y))) as exp when M.mem y env ->
      (match M.find y env with
         | MIImm i when isintimm i ->
             if i=0 then
               Ans(Mov(p, x))
             else
               Ans(LShift(p,(x, C(i))))
         | MISum(y', 0) ->
             Ans(LShift(p,(x,V(y'))))
         | _ -> Ans(exp))
  | RShift(p,(x,V(y))) as exp when M.mem x env && M.mem y env ->
      (match (M.find x env, M.find y env) with
        | (MIImm i, MIImm j) ->
            Ans(Set(p, i lsr (j mod 32)))
        | (MISum(x', 0), MIImm j) when isintimm j ->
            Ans(RShift(p, (x', C(j))))
        | (_, MIImm j) when isintimm j ->
            Ans(RShift(p, (x, C(j))))
        | (MISum(x', 0), MISum(y', 0)) ->
            Ans(RShift(p, (x', V(y'))))
        | (MISum(x', 0), _) ->
            Ans(RShift(p, (x', V(y))))
        | (_, MISum(y', 0)) ->
            Ans(RShift(p, (x, V(y'))))
        | _ -> Ans(exp))
  | RShift(p,(x,V(y))) as exp when M.mem y env ->
      (match M.find y env with
         | MIImm i when isintimm i ->
             if i=0 then
               Ans(Mov(p, x))
             else
               Ans(RShift(p,(x, C(i))))
         | MISum(y', 0) ->
             Ans(RShift(p,(x,V(y'))))
         | _ -> Ans(exp))
  | CmpEq(p,(x,V(y))) as exp when M.mem x env && M.mem y env ->
      (match (M.find x env, M.find y env) with
         | (MIImm i, MIImm j) ->
             Ans(Set(p, if i = j then 1 else 0))
         | (MISum(z, i), MIImm j)
         | (MIImm j, MISum(z, i)) when isintimm (j-i) ->
             Ans(CmpEq(p,(z, C(j-i))))
         | (MIImm i, MISum(y', 0)) when isintimm i ->
             Ans(CmpEq(p, (y', C(i))))
         | (MIImm i, _) when isintimm i ->
             Ans(CmpEq(p, (y, C(i))))
         | (MISum(x', 0), MIImm j) when isintimm j ->
             Ans(CmpEq(p, (x', C(j))))
         | (_, MIImm j) when isintimm j ->
             Ans(CmpEq(p, (x, C(j))))
         | (MISum(x',0), MISum(y',0)) ->
             Ans(CmpEq(p, (x', V(y'))))
         | (MISum(x',0), _) ->
             Ans(CmpEq(p, (x', V(y))))
         | (_,MISum(y',0)) ->
             Ans(CmpEq(p, (x, V(y'))))
         | _ -> Ans(exp))
  | CmpEq(p,(x,V(y))) as exp when M.mem x env ->
      (match M.find x env with
         | MIImm i when isintimm i ->
             Ans(CmpEq(p,(y, C(i))))
         | MISum(x',0) ->
             Ans(CmpEq(p,(x',V(y))))
         | _ -> Ans(exp))
  | CmpEq(p,(x,V(y))) as exp when M.mem y env ->
      (match M.find y env with
         | MIImm i when isintimm i ->
             Ans(CmpEq(p,(x, C(i))))
         | MISum(y',0) ->
             Ans(CmpEq(p,(x, V(y'))))
         | _ -> Ans(exp))
  | CmpEq(p,(x,C(i))) as exp when M.mem x env ->
      (match M.find x env with
         | MISum(y, j) when isintimm (i-j)->
             Ans(CmpEq(p, (y, C(i-j))))
         | _ -> Ans(exp))
  | CmpGt(p,(x,V(y))) as exp when M.mem x env && M.mem y env ->
      (match (M.find x env, M.find y env) with
         | (MIImm i, MIImm j) ->
             Ans(Set(p, if i > j then 1 else 0))
         | (MISum(z, i), MIImm j) when isintimm (j-i) ->
             Ans(CmpGt(p,(z, C(j-i))))
         | (MIImm j, MISum(z, i)) when isintimm (j-i) ->
             Ans(CmpLt(p,(z, C(j-i))))
         | (MIImm i, MISum(y', 0)) when isintimm i ->
             Ans(CmpLt(p, (y', C(i))))
         | (MIImm i, _) when isintimm i ->
             Ans(CmpLt(p, (y, C(i))))
         | (MISum(x', 0), MIImm j) when isintimm j ->
             Ans(CmpGt(p, (x', C(j))))
         | (_, MIImm j) when isintimm j ->
             Ans(CmpGt(p, (x, C(j))))
         | (MISum(x',0), MISum(y',0)) ->
             Ans(CmpGt(p, (x', V(y'))))
         | (MISum(x',0), _) ->
             Ans(CmpGt(p, (x', V(y))))
         | (_,MISum(y',0)) ->
             Ans(CmpGt(p, (x, V(y'))))
         | _ -> Ans(exp))
  | CmpGt(p,(x,V(y))) as exp when M.mem x env ->
      (match M.find x env with
         | MIImm i when isintimm i ->
             Ans(CmpLt(p,(y, C(i))))
         | MISum(x',0) ->
             Ans(CmpGt(p,(x',V(y))))
         | _ -> Ans(exp))
  | CmpGt(p,(x,V(y))) as exp when M.mem y env ->
      (match M.find y env with
         | MIImm i when isintimm i ->
             Ans(CmpGt(p,(x, C(i))))
         | MISum(y',0) ->
             Ans(CmpGt(p,(x, V(y'))))
         | _ -> Ans(exp))
  (*| CmpGt(p,(x,C(i))) as exp when M.mem x env ->
      (match M.find x env with
         | MISum(y, j) ->
             Ans(CmpGt(p, (y, C(i - j))))
         | _ -> Ans(exp))*)
  | CmpLt(p,(x,V(y))) as exp when M.mem x env && M.mem y env ->
      (match (M.find x env, M.find y env) with
         | (MIImm i, MIImm j) ->
             Ans(Set(p, if i < j then 1 else 0))
         | (MISum(z, i), MIImm j) when isintimm (j-i) ->
             Ans(CmpLt(p,(z, C(j-i))))
         | (MIImm j, MISum(z, i)) when isintimm (j-i) ->
             Ans(CmpGt(p,(z, C(j-i))))
         | (MIImm i, MISum(y', 0)) when isintimm i ->
             Ans(CmpGt(p, (y', C(i))))
         | (MIImm i, _) when isintimm i ->
             Ans(CmpGt(p, (y, C(i))))
         | (MISum(x', 0), MIImm j) when isintimm j ->
             Ans(CmpLt(p, (x', C(j))))
         | (_, MIImm j) when isintimm j ->
             Ans(CmpLt(p, (x, C(j))))
         | (MISum(x',0), MISum(y',0)) ->
             Ans(CmpLt(p, (x', V(y'))))
         | (MISum(x',0), _) ->
             Ans(CmpLt(p, (x', V(y))))
         | (_,MISum(y',0)) ->
             Ans(CmpLt(p, (x, V(y'))))
         | _ -> Ans(exp))
  | CmpLt(p,(x,V(y))) as exp when M.mem x env ->
      (match M.find x env with
         | MIImm i when isintimm i ->
             Ans(CmpGt(p,(y, C(i))))
         | MISum(x',0) ->
             Ans(CmpLt(p,(x',V(y))))
         | _ -> Ans(exp))
  | CmpLt(p,(x,V(y))) as exp when M.mem y env ->
      (match M.find y env with
         | MIImm i when isintimm i ->
             Ans(CmpLt(p,(x, C(i))))
         | MISum(y',0) ->
             Ans(CmpLt(p,(x, V(y'))))
         | _ -> Ans(exp))
  (*| CmpLt(p,(x,C(i))) as exp when M.mem x env ->
      (match M.find x env with
         | MISum(y, j) ->
             Ans(CmpLt(p, (y, C(i - j))))
         | _ -> Ans(exp))*)
  | CmpFEq(p, (x, y)) as exp when M.mem x env && M.mem y env ->
      (match (M.find x env, M.find y env) with
         | (a, b) when a = b ->
             Ans(Set(p, 1))
         | (MISumf(x', 0.0), MISumf(y', 0.0)) ->
             Ans(CmpFEq(p, (x', y')))
         | (MISumf(x',0.0), _) ->
             Ans(CmpFEq(p, (x', y)))
         | (_, MISumf(y',0.0)) ->
             Ans(CmpFEq(p, (x, y')))
         | _ -> Ans(exp))
  | CmpFEq(p, (x, y)) as exp when M.mem y env ->
      (match M.find y env with
         | MIImmf 0.0 ->
             Ans(CmpFEq(p, (x, reg_zero)))
         | MISumf(y', 0.0) ->
             Ans(CmpFEq(p, (x, y')))
         | _ -> Ans(exp))
  | CmpFEq(p, (x, y)) as exp when M.mem x env ->
      (match M.find x env with
         | MIImmf 0.0 ->
             Ans(CmpFEq(p, (reg_zero, y)))
         | MISumf(x', 0.0) ->
             Ans(CmpFEq(p, (x', y)))
         | _ -> Ans(exp))
  | CmpFGt(p, (x, y)) as exp when M.mem x env && M.mem y env ->
      (match (M.find x env, M.find y env) with
         | (MIImmf 0.0, MISumf(y', 0.0)) ->
             Ans(CmpFGt(p, (reg_zero, y'))) 
         | (MIImmf 0.0, _) ->
             Ans(CmpFGt(p, (reg_zero, y))) 
         | (MISumf(x', 0.0), MIImmf 0.0) ->
             Ans(CmpFGt(p, (x', reg_zero))) 
         | (_, MIImmf 0.0) ->
             Ans(CmpFGt(p, (x, reg_zero))) 
         | (MISumf(x', 0.0), MISumf(y', 0.0)) ->
             Ans(CmpFGt(p, (x', y')))
         | (MISumf(x', 0.0), _) ->
             Ans(CmpFGt(p, (x', y)))
         | (_, MISumf(y', 0.0)) ->
             Ans(CmpFGt(p, (x, y')))
         | _ -> Ans(exp))
  | CmpFGt(p, (x, y)) as exp when M.mem y env ->
      (match M.find y env with
         | MIImmf 0.0 ->
             Ans(CmpFGt(p, (x, reg_zero)))
         | MISumf(y', 0.0) ->
             Ans(CmpFGt(p, (x, y')))
         | _ -> Ans(exp))
  | CmpFGt(p, (x, y)) as exp when M.mem x env ->
      (match M.find x env with
         | MIImmf 0.0 ->
             Ans(CmpFGt(p, (reg_zero, y)))
         | MISumf(x', 0.0) ->
             Ans(CmpFGt(p, (x', y)))
         | _ -> Ans(exp))
  | FAdd(p, (x, Vf(y))) as exp when M.mem x env && M.mem y env ->
      (match (M.find x env, M.find y env) with
         | (MIImmf i, MIImmf j) ->
             Ans(SetF(p, i +. j))
         | (MIImmf i, MISumf(z, j)) | (MISumf(z, i), MIImmf j) when isfloatimm (i +. j) ->
             Ans(addimmf p z (i +. j))
         | (MISumf(x', 0.0), MISumf(y', 0.0)) ->
             Ans(FAdd(p, (x', Vf(y'))))
         | (MISumf(x', 0.0), _) ->
             Ans(FAdd(p, (x', Vf(y))))
         | (_, MISumf(y', 0.0)) ->
             Ans(FAdd(p, (x, Vf(y'))))
         | _ -> Ans(exp))
  | FAdd(p,(x, Vf(y))) as exp when M.mem y env ->
      (match M.find y env with
         | MIImmf i when isfloatimm i ->
             Ans(addimmf p x i)
         | MISumf(y', 0.0) ->
             Ans(FAdd(p, (x, Vf(y'))))
         | _ -> Ans(exp))
  | FAdd(p,(x, Vf(y))) as exp when M.mem x env ->
      (match M.find x env with
         | MIImmf i when isfloatimm i ->
             Ans(addimmf p y i)
         | MISumf(x', 0.0) ->
             Ans(FAdd(p, (x', Vf(y))))
         | _ -> Ans(exp))
  | FAdd(p,(x, F(i))) as exp when M.mem x env ->
      (match M.find x env with
         | MIImmf j ->
             Ans(SetF(p, i +. j))
         | MISumf(y, j) when isfloatimm (i +. j) ->
             Ans(addimmf p y (i +. j))
         | _ -> Ans(exp))
  | FSub(p,(Vf(x), y)) as exp when M.mem x env && M.mem y env ->
      (match (M.find x env, M.find y env) with
         | (MIImmf i, MIImmf j) ->
             Ans(SetF(p, (i -. j)))
         | (MISumf(z, i), MIImmf j) when isfloatimm (i-.j) ->
             Ans(addimmf p x (i-.j))
         | (MIImmf i, MISumf(z, j)) when isfloatimm (i+.j) ->
             Ans(FSub(p,(F(i+.j), z)))
         | (MISumf(x', 0.0), MISumf(y', 0.0)) ->
             Ans(FSub(p,(Vf(x'), y')))
         | (MISumf(x', 0.0), _) ->
             Ans(FSub(p,(Vf(x'), y)))
         | (_, MISumf(y', 0.0)) ->
             Ans(FSub(p,(Vf(x), y')))
         | _ -> Ans(exp))
  | FSub(p,(Vf(x), y)) as exp when M.mem x env ->
      (match M.find x env with
         | MIImmf i when isfloatimm i -> 
             Ans(FSub(p,(F(i), y)))
         | MISumf(x',0.0) ->
             Ans(FSub(p,(Vf(x'), y)))
         | _ -> Ans(exp))
  | FSub(p,(Vf(x), y)) as exp when M.mem y env ->
      (match M.find y env with
         | MIImmf i when isfloatimm (-.i) ->
             Ans(addimmf p x (-.i))
         | MISumf(y',0.0) ->
             Ans(FSub(p,(Vf(x),y')))
         | _ -> Ans(exp))
  | FMul(p,(x, Vf(y))) as exp when M.mem x env && M.mem y env ->
      (match (M.find x env, M.find y env) with
         | (MIImmf i, MIImmf j) ->
             Ans(SetF(p, i *. j))
         | (MIImmf 1.0, MISumf(y', 0.0)) ->
             Ans(Mov(p, y'))
         | (MIImmf 1.0, _) ->
             Ans(Mov(p, y))
         | (MISumf(x', 0.0), MIImmf 1.0) ->
             Ans(Mov(p, x'))
         | (_, MIImmf 1.0) ->
             Ans(Mov(p, x))
         | (MISumf(x',0.0), MISumf(y',0.0)) ->
             Ans(FMul(p,(x', Vf(y'))))
         | (MISumf(x',0.0), _) ->
             Ans(FMul(p,(x', Vf(y))))
         | (_, MISumf(y',0.0)) ->
             Ans(FMul(p,(x, Vf(y'))))
         | _ -> Ans(exp))
  | FMul(p, (x, Vf(y))) as exp when M.mem x env ->
      (match M.find x env with
         | MIImmf 1.0 ->
             Ans(Mov(p, y))
         | MIImmf i when isfloatimm i ->
             Ans(FMul(p, (y, F(i))))
         | MISumf(x', 0.0) ->
             Ans(FMul(p, (x', Vf(y))))
         | _ -> Ans(exp))
  | FMul(p, (x, Vf(y))) as exp when M.mem y env ->
      (match M.find y env with
         | MIImmf 1.0 ->
             Ans(Mov(p, x))
         | MIImmf i when isfloatimm i ->
             Ans(FMul(p, (x, F(i))))
         | MISumf(y', 0.0) ->
             Ans(FMul(p, (x, Vf(y'))))
         | _ -> Ans(exp))
  | FMul(p, (x, F(i))) as exp when M.mem x env ->
      (match M.find x env with
         | MIImmf j ->
             Ans(SetF(p, i *. j))
         | MISumf(x', 0.0) when i=1.0 ->
             Ans(Mov(p, x'))
         | MISumf(x', 0.0) ->
             Ans(FMul(p, (x', F(i))))
         | _ when i = 1.0 ->
             Ans(Mov(p, x))
         | _ -> Ans(exp))
  | FMul(p, (x, F(1.0))) ->
      Ans(Mov(p, x))
  | FNeg(p, x) as exp when M.mem x env ->
      (match M.find x env with
         | MIImmf j ->
             Ans(SetF(p, -.j))
         | MISumf(x', 0.0) ->
             Ans(FNeg(p, x'))
         | _ -> Ans(exp))
  | FInv(p, x) as exp when M.mem x env ->
      (match M.find x env with
         | MIImmf j ->
             Ans(SetF(p, 1.0 /. j))
         | MISumf(x', 0.0) ->
             Ans(FInv(p, x'))
         | _ -> Ans(exp))
  | FSqrt(p, x) as exp when M.mem x env ->
      (match M.find x env with
         | MIImmf j ->
             Ans(SetF(p, sqrt j))
         | MISumf(x', 0.0) ->
             Ans(FSqrt(p, x'))
         | _ -> Ans(exp))
  | FAbs(p, x) as exp when M.mem x env ->
      (match M.find x env with
         | MIImmf j ->
             Ans(SetF(p, abs_float j))
         | MISumf(x', 0.0) ->
             Ans(FAbs(p, x'))
         | _ -> Ans(exp))
  | Ld(p,Vm(x, i)) as exp when M.mem x env ->
      (match M.find x env with
         | MIImm j ->
             Ans(Ld(p, I(i + j)))
         | MISum(y, j) ->
             Ans(Ld(p, Vm(y, i + j)))
         | MILabel(l, j) ->
             Ans(Ld(p, Vml(l, i + j)))
         | _ ->
             Ans(exp))
  | Sto(p, (x, Vm(y, i))) when M.mem y env ->
      let x' = (try
                  (match M.find x env with
                     | MIImm 0 | MIImmf 0.0 -> reg_zero
                     | MISum(x', 0) | MISumf(x', 0.0) -> x'
                     | _ -> x)
                with Not_found -> x) in
      (match M.find y env with
         | MIImm j ->
             Ans(Sto(p, (x', I(i + j))))
         | MISum(z, j) ->
             Ans(Sto(p, (x', Vm(z, i + j))))
         | MILabel(l, j) ->
             Ans(Sto(p, (x', Vml(l, i + j))))
         | _ -> Ans(Sto(p, (x', Vm(y, i)))))
  | Sto(p, (x, y)) as exp when M.mem x env ->
      (match M.find x env with
         | MIImm 0 | MIImmf 0.0 ->
             Ans(Sto(p, (reg_zero, y)))
         | MISum(x', 0) | MISumf(x', 0.0) ->
             Ans(Sto(p, (x', y)))
         | _ -> Ans(exp))
  | Out(p, x) as exp when M.mem x env ->
      (match M.find x env with
         | MISum(x', 0) ->
             Ans(Out(p, x'))
         | _ -> Ans(exp))
  | Mov(p, x) as exp when M.mem x env ->
      (match M.find x env with
         | MISum(x', 0) | MISumf(x', 0.0) ->
             Ans(Mov(p, x'))
         | _ -> Ans(exp))

  | If(p1,p2,(x, e1, e2)) when M.mem x env ->
      (match M.find x env with
         | MIImm 0 -> g env e2
         | MIImm _ -> g env e1
         | MISum(x', 0) -> Ans(If(p1, p2, (x', g env e1, g env e2)))
         | _ -> Ans(If(p1, p2, (x, g env e1, g env e2))))
  | IfNot(p1,p2,(x, e1, e2)) when M.mem x env ->
      (match M.find x env with
         | MIImm 0 -> g env e1
         | MIImm _ -> g env e2
         | MISum(x', 0) -> Ans(If(p1, p2, (x', g env e1, g env e2)))
         | _ -> Ans(IfNot(p1, p2, (x, g env e1, g env e2))))
  | If(p1,p2,(x, e1, e2)) -> Ans(If(p1,p2,(x, g env e1, g env e2)))
  | IfNot(p1,p2,(x, e1, e2)) -> Ans(IfNot(p1,p2,(x, g env e1, g env e2)))
  | CallDir(p, (x, ys, zs)) ->
      let ys' = List.map
                  (fun y ->
                     if M.mem y env then
                       (match M.find y env with
                          | MISum(y', 0) | MISumf(y', 0.0) ->
                              y'
                          | _ -> y)
                     else
                       y)
                  ys in
      let zs' = List.map
                  (fun z ->
                     if M.mem z env then
                       (match M.find z env with
                          | MISum(z', 0) | MISumf(z', 0.0) ->
                              z'
                          | _ -> z)
                     else
                       z)
                  zs in
        Ans(CallDir(p, (x, ys', zs')))
  | Mul _ | Div _ -> assert false
  | e -> Ans(e)

           (*
and execmul p x c =
  let rec b x y c f =
    if y = 0 then
      f
    else
      if (y land 1) = 1 then
        b x (y lsr 1) (c+1) (fun t -> 
                             let t1 = Id.gentmp Type.Int in
                             let t2 = Id.gentmp Type.Int in
                               Let((t1, Type.Int), (if c=0 then Mov(p,x) else LShift(p, (x, C(c)))),
                                   Let((t2, Type.Int), Add(p, (t, V(t1))),
                                       f t2)))
      else
        b x (y lsr 1) (c+1) f in
    if c < 0 then
      (* 負の数をかけたい気分だ *)
      let c' = -c in
      (* x'の絶対値をとって計算してもどす *)
      let x' = Id.gentmp Type.Int in
      let t = Id.gentmp Type.Int in
      let t2 = Id.gentmp Type.Int in
        Let((t, Type.Int), Set(p, 0),
            Let((t2, Type.Int), CmpGt(p, (x, C(0))),
                Ans(IfNot(p, p, (t2,
                                (* xが負だったときは正にもどして計算してそのまま *)
                                Let((x', Type.Int), Neg(p, x),
                                    b x' c' 0 (fun t -> Ans(Mov(p, t))) t),
                                (* xが正だったときは計算したあと負にする *)
                                b x c' 0 (fun t -> Ans(Neg(p, t))) t)))))
        else
          (* 正の数をかける *)
          let x' = Id.gentmp Type.Int in
          let t = Id.gentmp Type.Int in
          let t2= Id.gentmp Type.Int in
            Let((t, Type.Int), Set(p, 0),
                Let((t2, Type.Int), CmpLt(p, (x, C(0))),
                    Ans(If(p, p, (t2,
                                    (* xが負だったときは正にもどして計算して負に戻す *)
                                    Let((x', Type.Int), Neg(p, x),
                                        b x' c 0 (fun t -> Ans(Neg(p, t))) t),
                                    (* xが正だったときはそのまま計算 *)
                                    b x c 0 (fun t -> Ans(Mov(p, t))) t)))))
            *)
and execmul p x c =
  (* 変数を全部つくる *)
  let rec expn vs c' f =
    if (c' land 1) = c' then
      addn (List.rev vs) (abs c) None f
    else
      let v = List.hd vs in
      let v' = Id.gentmp Type.Int in
        expn (v'::vs) (c' lsr 1) (fun e ->
                                 f (Let((v', Type.Int), Add(p, (v, V(v))), e)))
  and addn vsr c v f =
    (* 変数列から必要なものを全部たしていく *)
    Printf.eprintf "addn %s %d\n" x c;
    if c = 0 then
      (match v with
         | None -> assert false (* XXX *)
         | Some v' ->
             (v', f))
    else
      if (c land 1) = 0 then
        addn (List.tl vsr) (c lsr 1) v f
      else
        (match v with
           | None ->
               addn (List.tl vsr) (c lsr 1) (Some (List.hd vsr)) f
           | Some v2 ->
               let v' = Id.gentmp Type.Int in
               let vh = List.hd vsr in
                 addn (List.tl vsr) (c lsr 1) (Some v')
                   (fun e ->
                      f (Let((v', Type.Int), Add(p, (v2, V(vh))), e)))) in
    if c = 0 then
      Ans(Set(p, 0))
    else if c > 0 then
      (let (v, f) = expn [x] c (fun e -> e) in
        f (Ans(Mov(p, v))))
    else
      (let (v, f) = expn [x] (-c) (fun e -> e) in
        f (Ans(Neg(p, v))))



and execdiv p x c =
  (* 右シフトで演算可能なもののみOK *)
  if c = 0 then failwith "0 div"
  else
    let c' = abs c in
    let rec sfr c n =
      let c' = c lsr 1 in
        if (c land 1) = 1 then
          (if c' <> 0 then
             (* 右シフトで演算可能でない *)
             failwith "This div is not executable using rshift."
           else
               n)
        else
          sfr c' (n+1) in
    let rshiftnum = sfr c' 0 in
    let x1 = Id.gentmp Type.Int in
    let x2 = Id.gentmp Type.Int in
    let t1 = Id.gentmp Type.Int in
      if c < 0 then
        (* cが負 *)
        Let((t1, Type.Int), CmpLt(p, (x, C(0))),
            Ans(If(p, p, (t1,
                             (* xが負なら正になおしてから *)
                             Let((x1, Type.Int), Neg(p, x),
                                 Ans(RShift(p, (x1, C(rshiftnum))))),
                             (* xが正なら割った後に負にする *)
                             Let((x2, Type.Int), RShift(p, (x, C(rshiftnum))),
                                 Ans(Neg(p, x2)))))))
      else
          (* cが正 *)
          Let((t1, Type.Int), CmpLt(p, (x, C(0))),
              Ans(If(p, p, (t1,
                              (* xが負なら正になおしてから *)
                              Let((x1, Type.Int), Neg(p, x),
                                  Let((x2, Type.Int), RShift(p, (x1, C(rshiftnum))),
                                      Ans(Neg(p, x2)))),
                              (* xが正ならそのままわる *)
                              Ans(RShift(p, (x, C(rshiftnum))))))))
and muldiv env = function
  | Mul(p,(x, y)) when M.mem x env && M.mem y env -> 
      (match (M.find x env, M.find y env) with
         | (MIImm i, MIImm j) ->
             Some( Ans(Set(p, i*j)))
         | (_, MIImm i) ->
             Some(execmul p x i)
         | (MIImm i, _) ->
             Some(execmul p y i)
         | _ -> failwith "Mul var * var is not supported")
  | Mul(p,(x, y)) when M.mem y env -> 
      (match M.find y env with
         | MIImm i ->
             Some(execmul p x i)
         | _ -> failwith "Mul var * var is not supported")
  | Mul(p,(x, y)) when M.mem x env ->
      (match M.find x env with
         | MIImm i ->
             Some(execmul p y i)
         | _ -> failwith "Mul var * var is not supported")
  | Div(p,(x, y)) when M.mem x env && M.mem y env -> 
      (match (M.find x env, M.find y env) with
         | (MIImm i, MIImm j) ->
             Some(Ans(Set(p, i / j)))
         | (_, MIImm i) ->
             Some(execdiv p x i)
         | _ -> failwith "Mul var * var is not supported")
  | Div(p,(x, y)) when M.mem y env ->
      (match M.find y env with
         | MIImm i ->
             Some(execdiv p x i)
         | _ -> failwith "Div _ / var is not supported")
  | Mul(p,_) -> failwith "Mul var * var is not supported"
  | Div(p,_) -> failwith "Div _ / var is not supported"
  | _ -> None

let h { name = l; args = xs; fargs = ys; body = e; ret = t; ret_reg = r; category = category } = (* トップレベル関数の即値最適化 *)
  { name = l; args = xs; fargs = ys; body = g M.empty e; ret = t; ret_reg = r; category = category }

let f (Prog(fundefs, globals, e)) = (* プログラム全体の即値最適化 *)
  Prog(List.map h fundefs, globals, g M.empty e)
