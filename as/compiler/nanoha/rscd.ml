open Asm
(* レジスタ割り当て後のスケジューリング *)
(* 関数呼び出し時のあれを低減することが目的 *)

(* 動かしたらいけないもの（簡易的） *)
let has_effect = function
  | Ld _
  | Sto _
  | If _
  | IfNot _
  | CallCls _
  | CallDir _
  | Save _ -> true
  | _ -> false

(* リストの何番目にあるか調べる *)
let rec index_of' x i l =
  match l with
    | [] -> raise Not_found
    | y::l' ->
        if x = y then
          (* i番目に見つけた *)
          i
        else
          index_of' x (i+1) l'
let index_of x l = index_of' x 0 l
(* リストのi番目をxで置き換える non tail-recursive *)
let rec replace_list i x l =
  if i < 0 then l else
    match l with
      | [] -> []
      | y::l' ->
          if i = 0 then
            x::l'
          else
            y::replace_list (i-1) x l'
(* CallDirまたはCallClsの引数を書き換える *)
let replace_args ys zs = function
  | CallCls(p, (x, _, _)) -> CallCls(p, (x, ys, zs))
  | CallDir(p, (x, _, _)) -> CallDir(p, (x, ys, zs))
  | _ -> assert false

  (* 式のあれ *)
(*let rec g env = function
  (* env: 出番待ちのレジスタとその定義 *)
  | Ans(exp) -> Ans(g' env exp)
  | Let((x, t), exp, e) ->
      let exp' = g' env exp in
      let eff = has_effect' exp' in
      let (env', e') = if eff then
        (env, Let((x, t), exp', g env e))
      else
        (let env' = M.add x (t, exp', S.of_list (fv (Ans exp'))) env in
           (env', g env' e)) in
      let fvs = S.of_list (fv e') in
      (* 定義が消化されるの待ちでここで出す必要があるもの *)
      let e'' = M.fold (fun y (t,e1,_) e' ->
                          if S.mem y fvs then
                            (* yが使用されるのでyの定義をここにいれる *)
                            Let((y, t), e1, e')
                          else
                            (* yは使用されないので無視 *)
                              e') env' e' in
        e''*)
(* (x, t) = exp をeに挿入（遅延して） *)
let rec uninterchangable exp exp2 =
  (* expとexp2が交換可能でないならtrue *)
  match exp, exp2 with
    | (CallCls(_), _)
    | (_, CallCls(_))
    | (CallDir(_), _)
    | (_, CallDir(_))
    | (If(_), _)
    | (_, If(_))
    | (IfNot(_), _)
    | (_, IfNot(_))
    | (Ld(_), Sto(_))
    | (Sto(_), Ld(_))
    | (In(_), _)
    | (_, In(_))
    | (Out(_), _)
    | (_, Out(_)) -> true
    | _ -> false
let rec insert (x, t) exp = function
  | Ans(_) as e ->
      Let((x, t), exp, e)
  | Let((y, _) as yt, exp2, e2) as e ->
      (* expがexp2より後に行ってもいいか判定 *)
      (* exp2でxが使われている場合はだめ *)
      let f = fv (Ans exp2) in
      (* xとyが同じレジスタである場合もだめ *)
      let f' = y :: f in
      (* expでyが使われていてもだめ *)
      let f2 = fv (Ans exp) in
        if List.mem x f' || List.mem y f2 || uninterchangable exp exp2 then
          (* xへの代入はここに配置する *)
          let default = Let((x, t), exp, e) in
            if is_reg x then
              (match exp2 with
                 | CallCls(_, (a, zs, ws)) ->
                     (* TODO *)
                     assert false;
                 | CallDir(_, (Id.L(a), zs, ws)) ->
                     (* 関数にぶつかったときはレジスタを変更できるかも *)
                     (* xがクロージャではなく、引数である *)
                     let zws = zs@ws in
                       if a <> x && List.mem x zws then
                         (* xが最終的に何番のレジスタに割り当てられるか調べる *)
                         (let i = index_of x zws in
                          let argregs = RegAlloc.argsof a in
                          let final_reg = List.nth argregs i in
                            (* final_regに割り当ててあげたいけど、このレジスタが既に別のレジスタに割り当たっていたら無理 *)
                            if a = final_reg || List.mem final_reg zws then
                              (* 無理だった *)
                              default
                            else
                              (* あと、関数より先で参照されていたら書き換えるのは大変だからやらない *)
                              let br = (try
                                          M.find a (!fregenv)
                                        with Not_found -> allregsset) in
                                if not (S.mem x br) && List.mem x (fv e2) then
                                  (* 無理だった *)
                                  default
                                else
                                    (* final_regに割り当てることができる *)
                                    (* 引数のi番目をfinal_regに書き換える *)
                                    (Printf.eprintf "rscd: %s -> %s behind call for %s\n" x final_reg a;
                                    let ln = List.length zs in
                                    let zs' = replace_list i final_reg zs in
                                    let ws' = replace_list (i-ln) final_reg ws in
                                      Let((final_reg, t), exp,
                                          Let(yt, replace_args zs' ws' exp2, e2))))
                       else
                         default
                 | _ ->
                     if x = y && not (has_effect exp) && not (List.mem x f) then
                       (* xの定義は副作用を持たないしyで書き換えられるから不要定義だ *)
                       e
                     else
                       default)
            else
              default
        else
          (* 先に進む *)
          Let(yt, exp2, insert (x, t) exp e2)
let rec g env = function
  | Ans(exp) -> Ans(g' env exp)
  | Let((x, t) as xt, exp, e) ->
      let exp' = g' env exp in
      let e' = g env e in
        (* exp'をなるべく遅延する *)
        insert xt exp' e'

and g' env = function
  | If(p1,p2,(x,e1,e2)) ->
      If(p1,p2,(x, g env e1, g env e2))
  | IfNot(p1,p2,(x,e1,e2)) ->
      IfNot(p1,p2,(x, g env e1, g env e2))
  | exp -> exp


let h {name = name; args = ys; fargs = zs; body = e; ret = ret; ret_reg = ret_reg; category = category} =
  let e' = g M.empty e in
    {name = name; args = ys; fargs = zs; body = e'; ret = ret; ret_reg = ret_reg; category = category}


let f (Prog(fundefs, globals, e)) =
  let fundefs' = List.map h fundefs in
  let e' = g M.empty e in
    Prog(fundefs', globals, e')

let f x = x
