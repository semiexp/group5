(* starlightの何かだ *)

type id_or_imm = V of Id.t | C of int
type id_or_immf = Vf of Id.t | F of float
type id_or_immm = Vm of Id.t * int | Vml of Id.l * int | I of int
type t = (* 命令の列 *)
  | Ans of exp
  | Let of (Id.t * Type.t) * exp * t
and exp =
  | Nop
  | Add of Syntax.pos_t * (Id.t * id_or_imm)
  | Sub of Syntax.pos_t * (id_or_imm * Id.t)
  | LShift of Syntax.pos_t * (Id.t * id_or_imm)
  | RShift of Syntax.pos_t * (Id.t * id_or_imm)
  | CmpEq of Syntax.pos_t * (Id.t * id_or_imm)
  | CmpGt of Syntax.pos_t * (Id.t * id_or_imm)
  | CmpLt of Syntax.pos_t * (Id.t * id_or_imm)
  | CmpFEq of Syntax.pos_t * (Id.t * Id.t)
  | CmpFGt of Syntax.pos_t * (Id.t * Id.t)
  | FAdd of Syntax.pos_t * (Id.t * id_or_immf)
  | FSub of Syntax.pos_t * (id_or_immf * Id.t)
  | FMul of Syntax.pos_t * (Id.t * id_or_immf)
  | FNeg of Syntax.pos_t * Id.t
  | FInv of Syntax.pos_t * Id.t
  | FSqrt of Syntax.pos_t * Id.t
  | FAbs of Syntax.pos_t * Id.t
  | Ld of Syntax.pos_t * id_or_immm
  | Sto of Syntax.pos_t * (Id.t * id_or_immm)
  | Out of Syntax.pos_t * Id.t
  | In of Syntax.pos_t
  | Halt of Syntax.pos_t
  | Comment of Syntax.pos_t * string
  (* virtual instructions *)
  | Mul of Syntax.pos_t * (Id.t * Id.t)
  | Div of Syntax.pos_t * (Id.t * Id.t)
  | Set of Syntax.pos_t * int
  | SetF of Syntax.pos_t * float
  | SetL of Syntax.pos_t * (Id.l * int)
  | Mov of Syntax.pos_t * Id.t
  | Neg of Syntax.pos_t * Id.t
  | If of Syntax.pos_t * Syntax.pos_t * (Id.t * t * t)
  | IfNot of Syntax.pos_t * Syntax.pos_t * (Id.t * t * t)
  (* closure address, integer arguments, and float arguments *)
  | CallCls of Syntax.pos_t * (Id.t * Id.t list * Id.t list)
  | CallDir of Syntax.pos_t * (Id.l * Id.t list * Id.t list)
  | Save of Syntax.pos_t * (Id.t * Id.t) (* レジスタ変数の値をスタック変数へ保存 (caml2html: sparcasm_save) *)
  | Restore of Syntax.pos_t * Id.t (* スタック変数から値を復元 (caml2html: sparcasm_restore) *)
type fundef = { name : Id.l; args : Id.t list; fargs : Id.t list; body : t; ret : Type.t; ret_reg: Id.t; category : int }
type global = { name : Id.l; size: int }
(* プログラム全体 = トップレベル関数 + グローバル変数データ + メインの式 (caml2html: sparcasm_prog) *)
type prog = Prog of fundef list * global list * t

let seq(e1, e2) = Let((Id.gentmp Type.Unit, Type.Unit), e1, e2)

let regs = Array.init 59 (fun i -> Printf.sprintf "%%r%d" (i+1))
let allregs = Array.to_list regs
let allregsset = S.of_list allregs
let reg_zero = "%r0"
let reg_lnk= "%r60" (* link register *)
let reg_cl = "%r61" (* closure address (caml2html: sparcasm_regcl) *)
let reg_hp = "%r62"       (* heap pointer *)
let reg_sp = "%r63"       (* stack pointer *)

(* レジスタセットの分割数 *)
let reg_split_number = 7
(* 1つのレジスタセットのレジスタの数 *)
let reg_split_length = 8
 
(* 各種解析データ *)
let fregenv = ref M.empty

(*
let reg_sw = regs.(Array.length regs - 1) (* temporary for swap *)
let reg_fsw = fregs.(Array.length fregs - 1) (* temporary for swap *)
*)
let is_reg x = (x <> "" && x.[0] = '%')

(* super-tenuki *)
let rec remove_and_uniq xs = function
  | [] -> []
  | x :: ys when S.mem x xs -> remove_and_uniq xs ys
  | x :: ys -> x :: remove_and_uniq (S.add x xs) ys

(* free variables in the order of use (for spilling) (caml2html: sparcasm_fv) *)
let fv_id_or_imm = function V(x) -> [x] | _ -> []
let fv_id_or_immf = function Vf(x) -> [x] | _ -> []
let rec fv_exp = function
  | Nop | Comment(_) | Restore(_) | In(_) | Halt(_) | Set(_) | SetF(_) | SetL(_)
  | Ld(_,I(_)) | Ld(_, Vml(_)) -> []
  | Save(_,(x, _)) | Out(_,x) | FInv(_,x) | FSqrt(_,x) | FAbs(_,x) | Ld(_,Vm(x,_)) | Mov(_,x) | Neg(_,x) | FNeg(_,x) | Sto(_,(x, I(_))) | Sto(_,(x, Vml(_))) -> [x]
  | Add(_,(x, y')) | Sub(_,(y', x)) | LShift(_,(x,y')) | RShift(_,(x,y')) 
  | CmpEq(_, (x, y')) | CmpGt(_, (x, y')) | CmpLt(_, (x, y')) -> x :: fv_id_or_imm y'
  | FAdd(_,(x, y')) | FSub(_,(y', x)) | FMul(_,(x, y')) -> x :: fv_id_or_immf y'
  | CmpFEq(_, (x, y)) | CmpFGt(_, (x, y))
  | Sto(_, (x, Vm(y, _)))
  | Mul(_,(x, y)) | Div(_,(x, y)) -> [x; y]
  | If(_,_,(x, e1, e2)) | IfNot(_,_,(x, e1, e2)) -> x :: remove_and_uniq S.empty (fv e1 @ fv e2) (* uniq here just for efficiency *)
  | CallCls(_,(x, ys, zs)) -> x :: ys @ zs
  | CallDir(_,(_, ys, zs)) -> ys @ zs
and fv = function
  | Ans(exp) -> fv_exp exp
  | Let((x, t), exp, e) ->
      fv_exp exp @ remove_and_uniq (S.singleton x) (fv e)
let fv e = remove_and_uniq S.empty (fv e)

let rec concat e1 xt e2 =
  match e1 with
  | Ans(exp) -> Let(xt, exp, e2)
  | Let(yt, exp, e1') -> Let(yt, exp, concat e1' xt e2)

let align i = (if i mod 8 = 0 then i else i + 4)

(* 命令列が関数呼び出しを含むか *)
let rec has_call e =
  let rec has_call' = function
    | Nop | Add(_) | Sub(_) | LShift(_) | RShift(_) | CmpEq(_) | CmpGt(_) | CmpLt(_) | CmpFEq(_) | CmpFGt(_) | FAdd(_) | FSub(_) | FMul(_) | FNeg(_) | FInv(_) | FSqrt(_) | FAbs(_) | Ld(_) | Sto(_) | Out(_) | In(_) | Halt(_) | Comment(_)
    | Mul(_) | Div(_) | Set(_) | SetF(_) | SetL(_) | Mov(_) | Neg(_) | Save(_) | Restore(_) ->
        false
    | If(_,_,(_,e1,e2)) | IfNot(_,_,(_,e1,e2)) ->
        has_call e1 || has_call e2
    | CallCls(_) | CallDir(_) -> true in
    match e with
      | Ans e1 -> has_call' e1
      | Let(_,e1,e') ->
          has_call' e1 || has_call e'

let rec join gl l =
  match l with
    | [] -> ""
    | x::[] -> x
    | x::l' -> x ^ gl ^ join gl l'
let tree p =
  let rec f = function
    | Ans exp -> f' exp
    | Let((x,t), exp, e) -> ("LET " ^ x, [Ans exp; e])
  and f' = function
    | Nop -> ("NOP", [])
    | Add(_,(x,C(i))) -> ("ADD " ^ x ^ ", C(" ^ string_of_int i ^ ")", [])
    | Add(_,(x,V(y))) -> ("ADD " ^ x ^ ", " ^ y,  [])
    | Sub(_,(C(i),y)) -> ("SUB C(" ^ string_of_int i ^ "), " ^ y, [])
    | Sub(_,(V(x),y)) -> ("SUB " ^ x ^ ", " ^ y,  [])
    | LShift(_,(x,C(i))) -> ("LSHIFT " ^ x ^ ", C(" ^ string_of_int i ^ ")", [])
    | LShift(_,(x,V(y))) -> ("LSHIFT " ^ x ^ ", " ^ y,  [])
    | RShift(_,(x,C(i))) -> ("RSHIFT " ^ x ^ ", C(" ^ string_of_int i ^ ")", [])
    | RShift(_,(x,V(y))) -> ("RSHIFT " ^ x ^ ", " ^ y,  [])
    | CmpEq(_,(x,C(i))) -> ("CMPEQ " ^ x ^ ", C(" ^ string_of_int i ^ ")", [])
    | CmpEq(_,(x,V(y))) -> ("CMPEQ " ^ x ^ ", " ^ y , [])
    | CmpGt(_,(x,C(i))) -> ("CMPGT " ^ x ^ ", C(" ^ string_of_int i ^ ")", [])
    | CmpGt(_,(x,V(y))) -> ("CMPGT " ^ x ^ ", " ^ y , [])
    | CmpLt(_,(x,C(i))) -> ("CMPLT " ^ x ^ ", C(" ^ string_of_int i ^ ")", [])
    | CmpLt(_,(x,V(y))) -> ("CMPLT " ^ x ^ ", " ^ y , [])
    | CmpFEq(_,(x,y)) -> ("CMPFEQ " ^ x ^ ", " ^ y , [])
    | CmpFGt(_,(x,y)) -> ("CMPFGT " ^ x ^ ", " ^ y , [])
    | FAdd(_,(x,F(i))) -> ("FADD " ^ x ^ ", F(" ^ string_of_float i ^ ")", [])
    | FAdd(_,(x,Vf(y))) -> ("FADD " ^ x ^ ", " ^ y,  [])
    | FSub(_,(F(i),y)) -> ("FSUB F(" ^ string_of_float i ^ "), " ^ y, [])
    | FSub(_,(Vf(x),y)) -> ("FSUB " ^ x ^ ", " ^ y,  [])
    | FMul(_,(x,Vf(y))) -> ("FMUL " ^ x ^ ", " ^ y,  [])
    | FMul(_,(x,F(i))) -> ("FMUL " ^ x ^ ", " ^ string_of_float i,  [])
    | FNeg(_,x) -> ("FNEG " ^ x,  [])
    | FInv(_,x) -> ("FINV " ^ x,  [])
    | FSqrt(_,x) -> ("FSQRT " ^ x,  [])
    | FAbs(_,x) -> ("FABS " ^ x,  [])
    | Ld(_,Vm(x,i)) -> ("LD " ^ string_of_int i ^ "(" ^ x ^ ")",  [])
    | Ld(_,I(i)) -> ("LD " ^ string_of_int i,  [])
    | Ld(_,Vml(Id.L(l),i)) -> ("LD " ^ string_of_int i ^ "L(" ^ l ^ ")",  [])
    | Sto(_,(x,Vm(y,i))) -> ("STO " ^ x ^ ", " ^ string_of_int i ^ "(" ^ y ^ ")",  [])
    | Sto(_,(x,I(i))) -> ("STO " ^ x ^ ", " ^ string_of_int i,  [])
    | Sto(_,(x,Vml(Id.L(l),i))) -> ("STO " ^ x ^ ", " ^ string_of_int i ^ "(" ^ l ^ ")",  [])
    | Out(_,x) -> ("OUT " ^ x,  [])
    | In(_) -> ("IN",  [])
    | Halt(_) -> ("HALT",  [])
    | Comment(_,x) -> ("COMMENT " ^ x,  [])
    | Mul(_,(x,y)) -> ("MUL " ^ x ^ ", " ^ y,  [])
    | Div(_,(x,y)) -> ("DIV " ^ x ^ ", " ^ y,  [])
    | Set(_,i) -> ("SET " ^ string_of_int i, [])
    | SetF(_,i) -> ("SETF " ^ string_of_float i, [])
    | SetL(_,(Id.L(l), i)) -> ("SETL L(" ^ l ^ " + " ^ string_of_int i ^ ")", [])
    | Mov(_,x) -> ("MOV " ^ x, [])
    | Neg(_,x) -> ("NEG " ^ x, [])
    | If(_,_,(x, e1, e2)) -> ("IF " ^ x,  [e1; e2])
    | IfNot(_,_,(x, e1, e2)) -> ("IFNOT " ^ x,  [e1; e2])
    | CallCls(_,(x,ys,zs)) -> ("CALLCLS " ^ x ^ " " ^ join ", " ys ^ " " ^ join ", " zs,  [])
    | CallDir(_,(Id.L(x),ys,zs)) -> ("CALLDIR L(" ^ x ^ ") " ^ join ", " ys ^ " " ^ join ", " zs,  [])
    | Save(_,(x, y)) -> ("SAVE " ^ x ^ ", " ^ y,  [])
    | Restore(_,x) -> ("RESTORE " ^ x,  []) in
  let rec g = function
    | Ans exp -> g' exp
    | Let _  -> ""
  and g' = function
    | Nop -> ""
    | Add(p,_) | Sub(p,_) | LShift(p,_) | RShift(p,_)
    | CmpEq(p,_) | CmpGt(p,_) | CmpLt(p,_) | CmpFEq(p,_) | CmpFGt(p,_)
    | FAdd(p,_) | FSub(p,_)
    | FMul(p,_) | FNeg(p,_) | FInv(p,_) | FSqrt(p,_) | FAbs(p,_) | Ld(p,_) | Sto(p,_)
    | Out(p,_) | In(p) | Halt(p) | Comment(p,_) | Mul(p,_) | Div(p,_) | Set(p,_) | SetF(p,_) | SetL(p,_)
    | Mov(p,_) | Neg(p,_) | If(p,_,_) | IfNot(p,_,_)
    | CallCls(p,_) | CallDir(p,_)
    | Save(p,_) | Restore(p,_) -> Syntax.pos_str p in
  let Prog(fs,_,e) = p in
  let buf = Buffer.create 1024 in
    List.iter (fun (fd:fundef) ->
                 let Id.L(id) = fd.name in
                   Buffer.add_string buf ("  FUNC L("^id^") " ^ join " " fd.args ^ "\n");
                   Buffer.add_string buf ((Tree.make f g "    " fd.body))) fs;
    Buffer.add_string buf (Tree.make f g "  " e);
    Buffer.contents buf

