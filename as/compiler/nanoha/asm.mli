type id_or_imm = V of Id.t | C of int
type id_or_immf = Vf of Id.t | F of float
type id_or_immm = Vm of Id.t * int | Vml of Id.l * int | I of int
type t = (* 命令の列 *)
  | Ans of exp
  | Let of (Id.t * Type.t) * exp * t
and exp =
  | Nop
  | Add of Syntax.pos_t * (Id.t * id_or_imm)
  | Sub of Syntax.pos_t * (id_or_imm * Id.t)
  | LShift of Syntax.pos_t * (Id.t * id_or_imm)
  | RShift of Syntax.pos_t * (Id.t * id_or_imm)
  | CmpEq of Syntax.pos_t * (Id.t * id_or_imm)
  | CmpGt of Syntax.pos_t * (Id.t * id_or_imm)
  | CmpLt of Syntax.pos_t * (Id.t * id_or_imm)
  | CmpFEq of Syntax.pos_t * (Id.t * Id.t)
  | CmpFGt of Syntax.pos_t * (Id.t * Id.t)
  | FAdd of Syntax.pos_t * (Id.t * id_or_immf)
  | FSub of Syntax.pos_t * (id_or_immf * Id.t)
  | FMul of Syntax.pos_t * (Id.t * id_or_immf)
  | FNeg of Syntax.pos_t * Id.t
  | FInv of Syntax.pos_t * Id.t
  | FSqrt of Syntax.pos_t * Id.t
  | FAbs of Syntax.pos_t * Id.t
  | Ld of Syntax.pos_t * id_or_immm
  | Sto of Syntax.pos_t * (Id.t * id_or_immm)
  | Out of Syntax.pos_t * Id.t
  | In of Syntax.pos_t
  | Halt of Syntax.pos_t
  | Comment of Syntax.pos_t * string
  (* virtual instructions *)
  | Mul of Syntax.pos_t * (Id.t * Id.t)
  | Div of Syntax.pos_t * (Id.t * Id.t)
  | Set of Syntax.pos_t * int
  | SetF of Syntax.pos_t * float
  | SetL of Syntax.pos_t * (Id.l * int)
  | Mov of Syntax.pos_t * Id.t
  | Neg of Syntax.pos_t * Id.t
  | If of Syntax.pos_t * Syntax.pos_t * (Id.t * t * t)
  | IfNot of Syntax.pos_t * Syntax.pos_t * (Id.t * t * t)
  (* closure address, integer arguments, and float arguments *)
  | CallCls of Syntax.pos_t * (Id.t * Id.t list * Id.t list)
  | CallDir of Syntax.pos_t * (Id.l * Id.t list * Id.t list)
  | Save of Syntax.pos_t * (Id.t * Id.t) (* レジスタ変数の値をスタック変数へ保存 (caml2html: sparcasm_save) *)
  | Restore of Syntax.pos_t * Id.t (* スタック変数から値を復元 (caml2html: sparcasm_restore) *)
type fundef = { name : Id.l; args : Id.t list; fargs : Id.t list; body : t; ret : Type.t; ret_reg : Id.t; category : int }
type global = { name : Id.l; size: int }
type prog = Prog of fundef list * global list * t

val has_call : t -> bool
val seq : exp * t -> t (* shorthand of Let for unit *)

val regs : Id.t array
val allregs : Id.t list
val allregsset : S.t
val reg_zero : Id.t
val reg_lnk: Id.t
val reg_cl : Id.t
val reg_hp : Id.t
val reg_sp : Id.t
val is_reg : Id.t -> bool

val remove_and_uniq : S.t -> Id.t list -> Id.t list

val reg_split_number : int
val reg_split_length : int

val fregenv : (S.t M.t) ref

val fv : t -> Id.t list
val concat : t -> Id.t * Type.t -> t -> t

val align : int -> int

val join : string -> string list -> string
val tree : prog -> string
