(* 不要引数除去 *)
open KNormal

let rec g env = function
  (* env: 関数名 -> (使用される引数のリスト, 変換後の型 *)
  | If(p1, p2, (x, e1, e2)) ->
      If(p1, p2, (x, g env e1, g env e2))
  | Let(p, ex, ((x, t), e1, e2)) ->
      let e1' = g env e1 in
      let env', t' =
        (match e1' with
           | Var(_, y) ->
               (try
                  let (ns, t') = M.find y env in
                    (M.add y (ns, t') env, t')
                with Not_found -> (env, t))
           | _ -> (env, t)) in
        Let(p, ex, ((x, t'), e1', g env' e2))
  | LetRec(p, ex, ({ name = (x, t); args = yts; body = e1}, e2)) ->
      (* TODO XXX exportedな関数に不要引数があったら？ *)
      (* 関数定義 *)
      let e1' = g env e1 in
      let fvs = fv e1' in
      (* 引数をひとつずつ不要かどうか確かめる *)
      let yts'ib = List.mapi (fun i (y, t) ->
                               (i, (y, t), S.mem y fvs)) yts in
      (* 使われる引数だけ残す *)
      let yts'ib' =List.filter (fun (_,_,tf) -> tf) yts'ib in
      (* もともとの引数と使われる引数の対応 *)
      let ns = List.map (fun (i,_,_) -> i) yts'ib' in
      let yts' = List.map (fun (_,yt,_) -> yt) yts'ib' in
        (* メッセージのために使われていないやつを列挙 *)
      let unuseds = List.map (fun (_,(y, _),_) -> y) (List.filter (fun (_,_,tf) -> not tf) yts'ib) in
      (* 型を再構成 *)
        (match t with
           | Type.Fun(_, rt) ->
               let t' = Type.Fun(List.map snd yts', rt) in
               let env' = M.add x (ns, t') env in
                 (if unuseds <> [] then Printf.eprintf "reducing argument(s) %s of %s.\n" (join ", " unuseds) x);
                 LetRec(p, ex, ({ name = (x, t'); args = yts'; body = e1'}, g env' e2))
           | _ -> assert false)
  | LetTuple(p, (xts, y, e)) ->
      (* TODO XXX タプルに関数が入っていたら？ *)
      LetTuple(p, (xts, y, g env e))
  | App(p, (x, ys)) as exp ->
      (* 関数適用のときはいらない引数を除外する *)
      (try
         let (ns, t) = M.find x env in
         let rec take ys ns h f =
           match ns with
             | [] -> f []
             | i::ns' ->
                 if i = h then
                   (* これを取る *)
                   (let y = List.hd ys in
                    let tl= List.tl ys in
                      take tl ns' (h+1) (fun l -> f (y::l)))
                 else
                     take (List.tl ys) ns (h+1) f in
         (* 必要な引数だけ残した *)
         let ys' = take ys ns 0 (fun l -> l) in
           (if ys' <> ys then Printf.eprintf "Argument(s) to %s is reduced to %s\n" x (join ", " ys'));
           App(p, (x, ys'))
       with Not_found -> exp)
  | exp -> exp








let f = g M.empty
