(* Global Value Numbering *)
open KNormal

(* intキーのmap *)
module IM = Map.Make(struct
                       type t = int
                       let compare: int -> int -> int = compare
                     end)
module FM = Map.Make(struct
                       type t = float
                       let compare: float -> float -> int = compare
                     end)
module IM2 = Map.Make(struct
                       type t = int * int
                       let compare (a,b) (c,d) =
                         if a = c then
                           b - d
                         else
                           a - c
                     end)

(* タプル用 *)
module ILM = Map.Make(struct
                        type t = int list
                        let compare : t -> t -> int = compare
                      end)

(* グローバルな番号 *)
let nextid = ref 1

(* nenvの実体 *)
type nenv = {
  int: int IM.t;
  float: int FM.t;
  var: int M.t;
  neg: int IM.t;
  add: int IM2.t;
  sub: int IM2.t;
  mul: int IM2.t;
  div: int IM2.t;
  fneg: int IM.t;
  fadd: int IM2.t;
  fsub: int IM2.t;
  fmul: int IM2.t;
  fdiv: int IM2.t;
  cmpeq: int IM2.t;
  cmplt: int IM2.t;
  tuple: int ILM.t;
}

(* empty nenv *)
let nempty = {
    int   = IM.empty;
    float = FM.empty;
    var   = M.empty;

    neg = IM.empty;
    add = IM2.empty;
    sub = IM2.empty;
    mul = IM2.empty;
    div = IM2.empty;
    fneg= IM.empty;
    fadd= IM2.empty;
    fsub= IM2.empty;
    fmul= IM2.empty;
    fdiv= IM2.empty;
    cmpeq= IM2.empty;
    cmplt= IM2.empty;
    tuple= ILM.empty
  }

(* 対応する値を探してなければ追加する *)
let findornewIM x m =
  try (IM.find x m, m)
  with Not_found ->
    let n = !nextid in
      nextid := n+1;
      (n, IM.add x n m)

let findornewFM x m =
  try (FM.find x m, m)
  with Not_found ->
    let n = !nextid in
      nextid := n+1;
      (n, FM.add x n m)

let findornewM x m =
  try (M.find x m, m)
  with Not_found ->
    let n = !nextid in
      nextid := n+1;
      (n, M.add x n m)

let findornewIM2 x m =
  try (IM2.find x m, m)
  with Not_found ->
    let n = !nextid in
      nextid := n+1;
      (n, IM2.add x n m)

let findornewIM2cm (x, y) m =
  (* xとyが交換可能なやつ *)
  let t = if x < y then (x, y) else (y, x) in
  try (IM2.find t m, m)
  with Not_found ->
    let n = !nextid in
      nextid := n+1;
      (n, IM2.add t n m)

let findornewILM ns m =
  try (ILM.find ns m, m)
  with Not_found ->
    let n = !nextid in
      nextid := n+1;
      (n, ILM.add ns n m)


let rec g venv nenv = function
  (* venv: 番号に対応する変数 *)
  (* nenv: 式に対して割り当てられた番号 *)
  | Let(p, ls, ((x, t), e1, e2)) ->
      let e1' = g venv nenv e1 in
      (* xに対して値を割り当てたい *)
      let n, nenv', isimm = (match e1' with
                               | Int(_,i) ->
                                   let n, m' = findornewIM i nenv.int in
                                     n, {nenv with int = m'}, true
                               | Float(_,i) ->
                                   let n, m' = findornewFM i nenv.float in
                                     n, {nenv with float = m'}, true
                               | Var(_,y) ->
                                   let n, m' = findornewM y nenv.var in
                                     n, {nenv with var = m'}, false
                               | Neg(_,y) ->
                                   (* まずyの番号を探す *)
                                   let yn, var' = findornewM y nenv.var in
                                   let n, neg' = findornewIM yn nenv.neg in
                                     n, {nenv with var = var'; neg = neg'}, false
                               | Add(_,(y,z)) ->
                                   let yn, var' = findornewM y nenv.var in
                                   let zn, var' = findornewM z var' in
                                   let n, add'  = findornewIM2cm (yn, zn) nenv.add in
                                     n, {nenv with var = var'; add = add'}, false
                               | Sub(_,(y,z)) ->
                                   let yn, var' = findornewM y nenv.var in
                                   let zn, var' = findornewM z var' in
                                   let n, sub'  = findornewIM2 (yn, zn) nenv.sub in
                                     n, {nenv with var = var'; sub = sub'}, false
                               | Mul(_,(y,z)) ->
                                   let yn, var' = findornewM y nenv.var in
                                   let zn, var' = findornewM z var' in
                                   let n, mul'  = findornewIM2cm (yn, zn) nenv.mul in
                                     n, {nenv with var = var'; mul = mul'}, false
                               | Div(_,(y,z)) ->
                                   let yn, var' = findornewM y nenv.var in
                                   let zn, var' = findornewM z var' in
                                   let n, div'  = findornewIM2 (yn, zn) nenv.div in
                                     n, {nenv with var = var'; div = div'}, false
                               | FNeg(_,y) ->
                                   let yn, var' = findornewM y nenv.var in
                                   let n, fneg' = findornewIM yn nenv.fneg in
                                     n, {nenv with var = var'; fneg = fneg'}, false
                               | FAdd(_,(y,z)) ->
                                   let yn, var' = findornewM y nenv.var in
                                   let zn, var' = findornewM z var' in
                                   let n, fadd'  = findornewIM2cm (yn, zn) nenv.fadd in
                                     n, {nenv with var = var'; fadd = fadd'}, false
                               | FSub(_,(y,z)) ->
                                   let yn, var' = findornewM y nenv.var in
                                   let zn, var' = findornewM z var' in
                                   let n, fsub'  = findornewIM2 (yn, zn) nenv.fsub in
                                     n, {nenv with var = var'; fsub = fsub'}, false
                               | FMul(_,(y,z)) ->
                                   let yn, var' = findornewM y nenv.var in
                                   let zn, var' = findornewM z var' in
                                   let n, fmul'  = findornewIM2cm (yn, zn) nenv.fmul in
                                     n, {nenv with var = var'; fmul = fmul'}, false
                               | FDiv(_,(y,z)) ->
                                   let yn, var' = findornewM y nenv.var in
                                   let zn, var' = findornewM z var' in
                                   let n, fdiv'  = findornewIM2 (yn, zn) nenv.fdiv in
                                     n, {nenv with var = var'; fdiv = fdiv'}, false
                               | CmpEq(_,(y,z)) ->
                                   let yn, var' = findornewM y nenv.var in
                                   let zn, var' = findornewM z var' in
                                   let n, cmpeq' = findornewIM2cm (yn, zn) nenv.cmpeq in
                                     n, {nenv with var = var'; cmpeq = cmpeq'}, false
                               | CmpLt(_,(y,z)) ->
                                   let yn, var' = findornewM y nenv.var in
                                   let zn, var' = findornewM z var' in
                                   let n, cmplt' = findornewIM2 (yn, zn) nenv.cmplt in
                                     n, {nenv with var = var'; cmplt = cmplt'}, false
                               | Tuple(_, ys) ->
                                   (* 全部番号をアレする *)
                                   let yns, var' = List.fold_right
                                                     (fun y (yns, var) ->
                                                        let yn, var' = findornewM y var in
                                                          (yn::yns, var'))
                                                     ys
                                                     ([], nenv.var) in
                                   let n, tuple' = findornewILM yns nenv.tuple in
                                     n, {nenv with var = var'; tuple = tuple'}, false

                               | _ ->
                                   (* ほかはよくわからんから新しい変数にしておく *)
                                   let n, m' = findornewM x nenv.var in
                                     n, {nenv with var = m'}, true) in
      (* この値に対応する変数がすでに存在していたらそれにする *)
      let nenv'' = {nenv' with var = M.add x n nenv.var} in
      let e1'', venv' =
        if isimm then
          (* 即値とかの場合は変数で置き換えたくない *)
          e1', venv
        else
          (try
             let v = IM.find n venv in
               (* xはじつは変数vと同じ値だ *)
               Printf.eprintf "detected %s = %s\n" x v;
               Var(p, v), venv
           with Not_found ->
             (* xと同じ意味の既存の変数はなかった *)
             e1', IM.add n x venv) in
      let e2' = g venv' nenv'' e2 in
        Let(p, ls, ((x, t), e1'', e2'))
  | If(p1, p2, (x, e1, e2)) ->
      If(p1, p2, (x, g venv nenv e1, g venv nenv e2))
  | LetRec(p, ls, ({body = e1} as fd, e2)) ->
      LetRec(p, ls, ({fd with body = g IM.empty nempty e1}, g venv nenv e2))
  | LetTuple(p, (xts, y, e2)) ->
      LetTuple(p, (xts, y, g venv nenv e2))
  | exp -> exp




let f exp =
  nextid := 1;
  g IM.empty nempty exp

