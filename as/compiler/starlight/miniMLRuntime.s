.text
.globl min_caml_fless
min_caml_fless:
	cmp	%r2, %r1			# line 1, column 27-32
	jmp	p, else.233			# line 1, column 27-32
	movi	%r1, 0			# line 1, column 27-32
	jmp	%r60
else.233:				# line 1, column 27-32

	movi	%r1, 1			# line 1, column 27-32
	jmp	%r60
.globl min_caml_fispos
min_caml_fispos:
	movi	%r2, 0			# line 2, column 30-33
	fcmp	%r1, %r2			# line 2, column 26-33
	jmp	p, else.234			# line 2, column 26-33
	movi	%r1, 0			# line 2, column 26-33
	jmp	%r60
else.234:				# line 2, column 26-33

	movi	%r1, 1			# line 2, column 26-33
	jmp	%r60
.globl min_caml_fisneg
min_caml_fisneg:
	movi	%r2, 0			# line 3, column 30-33
	fcmp	%r2, %r1			# line 3, column 26-33
	jmp	p, else.235			# line 3, column 26-33
	movi	%r1, 0			# line 3, column 26-33
	jmp	%r60
else.235:				# line 3, column 26-33

	movi	%r1, 1			# line 3, column 26-33
	jmp	%r60
.globl min_caml_fiszero
min_caml_fiszero:
	movi	%r2, 0			# line 4, column 32-35
	fcmp	%r1, %r2			# line 4, column 27-36
	jmp	!z, else.236			# line 4, column 27-36
	movi	%r1, 1			# line 4, column 27-36
	jmp	%r60
else.236:				# line 4, column 27-36

	movi	%r1, 0			# line 4, column 27-36
	jmp	%r60
.globl min_caml_fhalf
min_caml_fhalf:
	movhiz	%r2, 16128			# line 5, column 30-33
	fmul	%r1, %r1, %r2			# line 5, column 25-33
	jmp	%r60
.globl min_caml_fsqr
min_caml_fsqr:
	fmul	%r1, %r1, %r1			# line 6, column 24-30
	jmp	%r60
.globl min_caml_sqrt
min_caml_sqrt:
	movhiz	%r2, 16128			# line 8, column 16-19
	fmul	%r2, %r1, %r2			# line 8, column 11-19
	movhiz	%r3, 16384			# line 9, column 16-19
	finv	%r3, %r3			# line 9, column 11-19
	fmul	%r1, %r1, %r3			# line 9, column 11-19
	movhiz	%r3, 16128			# line 9, column 23-26
	fadd	%r1, %r1, %r3			# line 9, column 11-26
	movhiz	%r3, 16384			# line 10, column 17-20
	finv	%r3, %r3			# line 10, column 11-20
	fmul	%r3, %r1, %r3			# line 10, column 11-20
	finv	%r1, %r1			# line 10, column 24-32
	fmul	%r1, %r2, %r1			# line 10, column 24-32
	fadd	%r3, %r3, %r1			# line 10, column 11-32
	movhiz	%r1, 16384			# line 11, column 17-20
	finv	%r1, %r1			# line 11, column 11-20
	fmul	%r1, %r3, %r1			# line 11, column 11-20
	finv	%r3, %r3			# line 11, column 24-32
	fmul	%r3, %r2, %r3			# line 11, column 24-32
	fadd	%r1, %r1, %r3			# line 11, column 11-32
	movhiz	%r3, 16384			# line 12, column 17-20
	finv	%r3, %r3			# line 12, column 11-20
	fmul	%r3, %r1, %r3			# line 12, column 11-20
	finv	%r1, %r1			# line 12, column 24-32
	fmul	%r1, %r2, %r1			# line 12, column 24-32
	fadd	%r3, %r3, %r1			# line 12, column 11-32
	movhiz	%r1, 16384			# line 13, column 17-20
	finv	%r1, %r1			# line 13, column 11-20
	fmul	%r1, %r3, %r1			# line 13, column 11-20
	finv	%r3, %r3			# line 13, column 24-32
	fmul	%r2, %r2, %r3			# line 13, column 24-32
	fadd	%r1, %r1, %r2			# line 13, column 11-32
	jmp	%r60
.globl read_int2.157
read_int2.157:
	sto	%r1, 0(%r63)			
	sto	%r60, 1(%r63)			# line 17, column 15-26
	addi	%r63, %r63, 2			# line 17, column 15-26
	call	%r60, min_caml_read_char			# line 17, column 15-26
	addi	%r63, %r63, -2			# line 17, column 15-26
	ld	%r60, 1(%r63)			# line 17, column 15-26
	cmpi	%r1, 32			# line 18, column 9-18
	jmp	!z, else.237			# line 18-33
	ld	%r1, 0(%r63)			
	jmp	%r60
else.237:				# line 18-33

	cmpi	%r1, 10			# line 20, column 14-23
	jmp	!z, else.238			# line 20-33
	ld	%r1, 0(%r63)			
	jmp	%r60
else.238:				# line 20-33

	cmpi	%r1, 13			# line 22, column 14-23
	jmp	!z, else.239			# line 22-33
	ld	%r1, 0(%r63)			
	jmp	%r60
else.239:				# line 22-33

	cmpi	%r1, 9			# line 24, column 14-22
	jmp	!z, else.240			# line 24-33
	ld	%r1, 0(%r63)			
	jmp	%r60
else.240:				# line 24-33

	cmpi	%r1, 48			# line 26, column 14-24
	jmp	n, else.241			# line 26-33
	cmpi	%r1, 57			# line 27, column 11-21
	jmp	p, else.242			# line 27-31
	movi	%r2, 0			# line 29, column 21-25
	ld	%r3, 0(%r63)			
	cmpi	%r3, 0			# line 29, column 21-25
	jmp	p, else.243			# line 29, column 21-25
	sub	%r3, %r0, %r3			# line 29, column 21-25
	lshifti	%r4, %r3, 3			# line 29, column 21-25
	add	%r2, %r2, %r4			# line 29, column 21-25
	lshifti	%r3, %r3, 1			# line 29, column 21-25
	add	%r2, %r2, %r3			# line 29, column 21-25
	sub	%r2, %r0, %r2			# line 29, column 21-25
	jmp	if_cont.244			# line 29, column 21-25
else.243:			# line 29, column 21-25
	lshifti	%r4, %r3, 3			# line 29, column 21-25
	add	%r2, %r2, %r4			# line 29, column 21-25
	lshifti	%r3, %r3, 1			# line 29, column 21-25
	add	%r2, %r2, %r3			# line 29, column 21-25
if_cont.244:			# line 29, column 21-25
	addi	%r1, %r1, -48			# line 29, column 28-39
	add	%r1, %r2, %r1			# line 29, column 20-40
	jmp	read_int2.157			# line 29, column 10-40
else.242:				# line 27-31

	ld	%r1, 0(%r63)			
	jmp	%r60
else.241:				# line 26-33

	ld	%r1, 0(%r63)			
	jmp	%r60
.globl min_caml_read_int
min_caml_read_int:
	sto	%r60, 0(%r63)			# line 34, column 13-24
	addi	%r63, %r63, 1			# line 34, column 13-24
	call	%r60, min_caml_read_char			# line 34, column 13-24
	addi	%r63, %r63, -1			# line 34, column 13-24
	ld	%r60, 0(%r63)			# line 34, column 13-24
	cmpi	%r1, 32			# line 35, column 7-16
	jmp	!z, else.245			# line 35-50
	jmp	min_caml_read_int			# line 36, column 6-16
else.245:				# line 35-50

	cmpi	%r1, 10			# line 37, column 12-21
	jmp	!z, else.246			# line 37-50
	jmp	min_caml_read_int			# line 38, column 6-16
else.246:				# line 37-50

	cmpi	%r1, 13			# line 39, column 12-21
	jmp	!z, else.247			# line 39-50
	jmp	min_caml_read_int			# line 40, column 6-16
else.247:				# line 39-50

	cmpi	%r1, 9			# line 41, column 12-20
	jmp	!z, else.248			# line 41-50
	jmp	min_caml_read_int			# line 42, column 6-16
else.248:				# line 41-50

	cmpi	%r1, 48			# line 43, column 12-22
	jmp	n, else.249			# line 43-50
	cmpi	%r1, 57			# line 44, column 9-19
	jmp	p, else.250			# line 44-48
	addi	%r1, %r1, -48			# line 46, column 18-29
	jmp	read_int2.157			# line 46, column 8-29
else.250:				# line 44-48

	movi	%r1, 0			# line 48, column 10-11
	jmp	%r60
else.249:				# line 43-50

	movi	%r1, 0			# line 50, column 10-11
	jmp	%r60
