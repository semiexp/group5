open Asm

external gethi : float -> int32 = "gethi"
external getlo : float -> int32 = "getlo"

(* Syntax.pos_tをコメント文字列にする *)
let posstr (p : Syntax.pos_t) =
  if p.Syntax.s.Syntax.line < 0 then
    (* it's dummy *)
    ""
  else
      Printf.sprintf "# %s" (Syntax.pos_str p)



let stackset = ref S.empty (* すでにSaveされた変数の集合 (caml2html: emit_stackset) *)
let stackmap = ref [] (* Saveされた変数の、スタックにおける位置 (caml2html: emit_stackmap) *)

let save x =
  stackset := S.add x !stackset;
  if not (List.mem x !stackmap) then
    stackmap := !stackmap @ [x]
let locate x =
  let rec loc = function
    | [] -> []
    | y :: zs when x = y -> 0 :: List.map succ (loc zs)
    | y :: zs -> List.map succ (loc zs) in
  loc !stackmap
let offset x = List.hd (locate x)
let stacksize () = List.length !stackmap

let pp_id_or_imm = function
  | V(x) -> x
  | C(i) -> string_of_int i

(* 関数呼び出しのために引数を並べ替える(register shuffling) (caml2html: emit_shuffle) *)
let rec shuffle sw xys =
  (* remove identical moves *)
  let _, xys = List.partition (fun (x, y) -> x = y) xys in
  (* find acyclic moves *)
  match List.partition (fun (_, y) -> List.mem_assoc y xys) xys with
  | [], [] -> []
  | (x, y) :: xys, [] -> (* no acyclic moves; resolve a cyclic move *)
      (y, sw) :: (x, y) :: shuffle sw (List.map
					 (function
					   | (y', z) when y = y' -> (sw, z)
					   | yz -> yz)
					 xys)
  | xys, acyc -> acyc @ shuffle sw xys

(* cmpを出力 *)
let emit_cmp oc x y' p =
  match y' with
    | C(i) ->
        assert (-0x8000 <= i && i < 0x8000);
        Printf.fprintf oc "\tcmpi\t%s, %d\t\t\t%s\n" x i (posstr p)
    | V(y) ->
        Printf.fprintf oc "\tcmp\t%s, %s\t\t\t%s\n" x y (posstr p)


(* Tailのboolパラメータ：リンクレジスタのリストアが必要かどうか *)
type dest = Tail of bool | NonTail of Id.t (* 末尾かどうかを表すデータ型 (caml2html: emit_dest) *)


let rec g oc = function (* 命令列のアセンブリ生成 (caml2html: emit_g) *)
  | dest, Ans(exp) -> g' oc (dest, exp)
  | dest, Let((x, t), exp, e) ->
      g' oc (NonTail(x), exp);
      g oc (dest, e)

and g' oc = function (* 各命令のアセンブリ生成 (caml2html: emit_gprime) *)
  (* 末尾でなかったら計算結果をdestにセット (caml2html: emit_nontail) *)
  | NonTail(_), Nop -> ()
  | NonTail(x), Add(p,(y, z')) ->
      (match z' with
         | C(i) ->
             (* TODO: 14bit即値の範囲チェック *)
             assert (-0x2000 <= i && i < 0x2000);
             Printf.fprintf oc "\taddi\t%s, %s, %d\t\t\t%s\n" x y i (posstr p)
         | V(z) ->
             Printf.fprintf oc "\tadd\t%s, %s, %s\t\t\t%s\n" x y z (posstr p))
  | NonTail(x), Sub(p,(y, z')) ->
      (match z' with
         | C(i) ->
             (* TODO: 14bit即値の範囲チェック *)
             assert (-0x2000 < i && i <= 0x2000);
             Printf.fprintf oc "\taddi\t%s, %s, %d\t\t\t%s\n" x y (~-i) (posstr p)
         | V(z) ->
             Printf.fprintf oc "\tsub\t%s, %s, %s\t\t\t%s\n" x y z (posstr p))
  | NonTail(x), LShift(p,(y, z')) ->
      (match z' with
         | C(i) ->
             (* 6bit -32は許さないので注意 *)
             assert (-0x20 < i && i < 0x20);
             Printf.fprintf oc "\tlshifti\t%s, %s, %d\t\t\t%s\n" x y i (posstr p)
         | V(z) ->
             Printf.fprintf oc "\tlshift\t%s, %s, %s\t\t\t%s\n" x y z (posstr p))
  | NonTail(x), RShift(p,(y, z')) ->
      (match z' with
         | C(i) ->
             (* 6bit -32は許さないので注意 *)
             assert (-0x20 < i && i < 0x20);
             Printf.fprintf oc "\trshifti\t%s, %s, %d\t\t\t%s\n" x y i (posstr p)
         | V(z) ->
             Printf.fprintf oc "\trshift\t%s, %s, %s\t\t\t%s\n" x y z (posstr p))
  | NonTail(x), FAdd(p,(y, z)) ->
      Printf.fprintf oc "\tfadd\t%s, %s, %s\t\t\t%s\n" x y z (posstr p)
  | NonTail(x), FSub(p,(y, z)) ->
      Printf.fprintf oc "\tfsub\t%s, %s, %s\t\t\t%s\n" x y z (posstr p)
  | NonTail(x), FMul(p,(y, z)) ->
      Printf.fprintf oc "\tfmul\t%s, %s, %s\t\t\t%s\n" x y z (posstr p)
  | NonTail(x), FInv(p, y) ->
      Printf.fprintf oc "\tfinv\t%s, %s\t\t\t%s\n" x y (posstr p)
  | NonTail(x), FAbs(p, y) ->
      Printf.fprintf oc "\tfabs\t%s, %s\t\t\t%s\n" x y (posstr p)
  | NonTail(x), Ld(p,(y, i)) ->
      (* 14bit *)
      assert (-0x2000 <= i && i < 0x2000);
      if i = 0 then
        Printf.fprintf oc "\tld\t%s, (%s)\t\t\t%s\n" x y (posstr p)
      else
        Printf.fprintf oc "\tld\t%s, %d(%s)\t\t\t%s\n" x i y (posstr p)
  | NonTail(x), Ld2(p,(y, z, i)) ->
      (* 8bit *)
      assert (-0x80 <= i && i < 0x80);
      if i = 0 then
        Printf.fprintf oc "\tld\t%s, (%s + %s)\t\t\t%s\n" x y z (posstr p)
      else
        Printf.fprintf oc "\tld\t%s, %d(%s + %s)\t\t\t%s\n" x i y z (posstr p)
  | NonTail(_), Sto(p,(x, (y, i))) ->
      (* 14bit *)
      assert (-0x2000 <= i && i < 0x2000);
      if i = 0 then
        Printf.fprintf oc "\tsto\t%s, (%s)\t\t\t%s\n" x y (posstr p)
      else
        Printf.fprintf oc "\tsto\t%s, %d(%s)\t\t\t%s\n" x i y (posstr p)
  | NonTail(_), Sto2(p,(x, (y, z, i))) ->
      (* 8bit *)
      assert (-0x80 <= i && i < 0x80);
      if i = 0 then
        Printf.fprintf oc "\tsto\t%s, (%s + %s)\t\t\t%s\n" x y z (posstr p)
      else
        Printf.fprintf oc "\tsto\t%s, %d(%s + %s)\t\t\t%s\n" x i y z (posstr p)
  | NonTail(_), Out(p, x) ->
      Printf.fprintf oc "\tout\t%s\t\t\t%s\n" x (posstr p)
  | NonTail(x), In(p) ->
      Printf.fprintf oc "\tin\t%s\t\t\t%s\n" x (posstr p)
  | NonTail(_), Halt(p) ->
      Printf.fprintf oc "\thalt\t\t\t%s" (posstr p)
  | NonTail(_), Mul(p,_) -> assert false
  | NonTail(_), Div(p,_) -> assert false
  | NonTail(x), Set(p,i) ->
      (* 即値の代入 *)
      (if -0x8000 <= i && i < 0x8000 then
        (* 20bitで表すことができる *)
        Printf.fprintf oc "\tmovi\t%s, %d\t\t\t%s\n" x i (posstr p)
      else
          (* 下位16bitが0なら *)
          if (i land 0xFFFF) = 0 then
            Printf.fprintf oc "\tmovhiz\t%s, %d\t\t\t%s\n" x ((i lsr 16) land 0xFFFF) (posstr p)
          else
            (* そうでない *)
            (Printf.fprintf oc "\tmovi\t%s, %d\t\t\t%s\n" x (i land 0xFFFF) (posstr p);
             Printf.fprintf oc "\tmovhi\t%s, %d\t\t\t%s\n" x ((i lsr 16) land 0xFFFF) (posstr p)))
  | NonTail(x), SetL(p,Id.L(y)) -> Printf.fprintf oc "\tmovi\t%s, %s\t\t\t%s\n" x y (posstr p)
  | NonTail(x), Mov(p,y) ->
      if x <> y then Printf.fprintf oc "\tadd\t%s, %%r0, %s\t\t\t%s\n" x y (posstr p)
  | NonTail(x), Neg(p,y) ->
      Printf.fprintf oc "\tsub\t%s, %%r0, %s\t\t\t%s\n" x y (posstr p)
  | NonTail(x), FNeg(p,y) ->
      Printf.fprintf oc "\tfsub\t%s, %%r0, %s\t\t\t%s\n" x y (posstr p)
  | NonTail(_), Comment(p,s) -> Printf.fprintf oc "\t# %s\t\t\t%s\n" s (posstr p)
  (* 退避の仮想命令の実装 (caml2html: emit_save) *)
  | NonTail(_), Save(p,(x, y)) when List.mem x allregs && not (S.mem y !stackset) ->
      (* 変数yをレジスタxからスタックへ *)
      save y;
      Printf.fprintf oc "\tsto\t%s, %d(%s)\t\t\t%s\n" x (offset y) reg_sp (posstr p)
  | NonTail(_), Save(p,(x, y)) -> assert (S.mem y !stackset); ()
  (* 復帰の仮想命令の実装 (caml2html: emit_restore) *)
  | NonTail(x), Restore(p,y) when List.mem x allregs ->
      Printf.fprintf oc "\tld\t%s, %d(%s)\t\t\t%s\n" x (offset y) reg_sp (posstr p)
  (* 末尾だったら計算結果を第一レジスタにセットしてret (caml2html: emit_tailret) *)
  | NonTail(_), Restore(_,_) -> assert false
  | Tail(lnk_restore), (Nop | Sto _ | Sto2 _ | Out _ | Halt _ | Comment _ | Save _ as exp) ->
      (* 計算結果がない *)
      g' oc (NonTail(Id.gentmp Type.Unit), exp);
      g'_ret oc lnk_restore;
  | Tail(lnk_restore), (Add _ | Sub _ | LShift _ | RShift _ | FAdd _ | FSub _ | FMul _ | FInv _ | FAbs _ | Ld _ | Ld2 _ | In _ | Mul _ | Div _ | Set _ | SetL _ | Mov _ | Neg _ | FNeg _ as exp) ->
      g' oc (NonTail(regs.(0)), exp);
      g'_ret oc lnk_restore;
  | Tail(lnk_restore), (Restore(p,x) as exp) ->
      (match locate x with
      | [i] -> g' oc (NonTail(regs.(0)), exp)
      | _ -> assert false);
      g'_ret oc lnk_restore;
  | Tail(lnk_restore), IfEq(p1,p2,(x, y', e1, e2)) ->
      emit_cmp oc x y' p2;
      g'_tail_if oc p1 lnk_restore e1 e2 "z" "!z"
  | Tail(lnk_restore), IfLE(p1,p2,(x, y', e1, e2)) ->
      emit_cmp oc x y' p2;
      g'_tail_if oc p1 lnk_restore e1 e2 "!p" "p"
  | Tail(lnk_restore), IfGE(p1,p2,(x, y', e1, e2)) ->
      emit_cmp oc x y' p2;
      g'_tail_if oc p1 lnk_restore e1 e2 "!n" "n"
  | Tail(lnk_restore), IfFEq(p1,p2,(x, y, e1, e2)) ->
      Printf.fprintf oc "\tfcmp\t%s, %s\t\t\t%s\n" x y (posstr p2);
      g'_tail_if oc p1 lnk_restore e1 e2 "z" "!z"
  | Tail(lnk_restore), IfFLE(p1,p2,(x, y, e1, e2)) ->
      Printf.fprintf oc "\tfcmp\t%s, %s\t\t\t%s\n" x y (posstr p2);
      g'_tail_if oc p1 lnk_restore e1 e2 "!p" "p"
  | NonTail(z), IfEq(p1,p2,(x, y', e1, e2)) ->
      emit_cmp oc x y' p2;
      g'_non_tail_if oc p1 (NonTail(z)) e1 e2 "z" "!z"
  | NonTail(z), IfLE(p1,p2,(x, y', e1, e2)) ->
      emit_cmp oc x y' p2;
      g'_non_tail_if oc p1 (NonTail(z)) e1 e2 "!p" "p"
  | NonTail(z), IfGE(p1,p2,(x, y', e1, e2)) ->
      emit_cmp oc x y' p2;
      g'_non_tail_if oc p1 (NonTail(z)) e1 e2 "!n" "n"
  | NonTail(z), IfFEq(p1,p2,(x, y, e1, e2)) ->
      Printf.fprintf oc "\tfcmp\t%s, %s\t%s\n" x y (posstr p2);
      g'_non_tail_if oc p1 (NonTail(z)) e1 e2 "z" "!z"
  | NonTail(z), IfFLE(p1,p2,(x, y, e1, e2)) ->
      Printf.fprintf oc "\tfcmp\t%s, %s\t%s\n" x y (posstr p2);
      g'_non_tail_if oc p1 (NonTail(z)) e1 e2 "!p" "p"
  (* 関数呼び出しの仮想命令の実装 (caml2html: emit_call) *)
  | Tail(lnk_restore), CallCls(p,(x, ys, zs)) -> (* 末尾呼び出し (caml2html: emit_tailcall) *)
      g'_args oc p [(x, reg_cl)] ys zs;
      g'_lnk_restore oc lnk_restore;
      Printf.fprintf oc "\tjmp\t%s\t\t\t%s\n" reg_cl (posstr p);
  | Tail(lnk_restore), CallDir(p,(Id.L(x), ys, zs)) -> (* 末尾呼び出し *)
      g'_args oc p [] ys zs;
      g'_lnk_restore oc lnk_restore;
      Printf.fprintf oc "\tjmp\t%s\t\t\t%s\n" x (posstr p);
  | NonTail(a), CallCls(p,(x, ys, zs)) ->
      g'_args oc p [(x, reg_cl)] ys zs;
      let ss = stacksize () in
      if ss > 0 then
        (* スタックポインタを進める *)
        Printf.fprintf oc "\taddi\t%s, %s, %d\t\t\t%s\n" reg_sp reg_sp ss (posstr p);
      (* リターンアドレスを保存 *)
      Printf.fprintf oc "\tmovi\t%s, &PC(3)\t\t\t%s\n" reg_lnk (posstr p);
      (* 関数のアドレスを得る *)
      (* XXX ad hocだが引数が59個無い限り壊れないと思う *)
      Printf.fprintf oc "\tld\t%s, (%s)\t\t\t%s\n" "%r59" reg_cl (posstr p);
      (* 関数呼び出し *)
      Printf.fprintf oc "\tjmp\t%s\t\t\t%s\n" "%r59" (posstr p);
      if ss > 0 then
        (* スタックポインタ戻す *)
        Printf.fprintf oc "\taddi\t%s, %s, %d\t\t\t%s\n" reg_sp reg_sp (~-ss) (posstr p);
      if List.mem a allregs && a <> regs.(0) then
        (* 返り値を目的のレジスタに入れる *)
        Printf.fprintf oc "\tmov\t%s, %s\t\t\t%s\n" a regs.(0) (posstr p)
  | NonTail(a), CallDir(p,(Id.L(x), ys, zs)) ->
      (* 自由変数のない関数の呼び出し *)
      g'_args oc p [] ys zs;
      let ss = stacksize () in
      if ss > 0 then
        Printf.fprintf oc "\taddi\t%s, %s, %d\t\t\t%s\n" reg_sp reg_sp ss (posstr p);
      Printf.fprintf oc "\tcall\t%s, %s\t\t\t%s\n" reg_lnk x (posstr p);
      if ss > 0 then
        Printf.fprintf oc "\taddi\t%s, %s, %d\t\t\t%s\n" reg_sp reg_sp (~-ss) (posstr p);
      if List.mem a allregs && a <> regs.(0) then
        Printf.fprintf oc "\tmov\t%s, %s\t\t\t%s\n" a regs.(0) (posstr p)
and g'_tail_if oc p lnk_restore e1 e2 _ bn =
  (* tail ifをつくる *)
  let b_else = Id.genid ("else") in
  Printf.fprintf oc "\tjmp\t%s, %s\t\t\t%s\n" bn b_else (posstr p);
  let stackset_back = !stackset in
  g oc (Tail(lnk_restore), e1);
  Printf.fprintf oc "%s:\t\t\t\t%s\n\n" b_else (posstr p);
  stackset := stackset_back;
  g oc (Tail(lnk_restore), e2)
and g'_non_tail_if oc p dest e1 e2 b bn =
  (* non-tail ifの出力 *)
  let b_else = Id.genid ("else") in
  let b_cont = Id.genid ("if_cont") in
  Printf.fprintf oc "\tjmp\t%s, %s\t\t\t%s\n" bn b_else (posstr p);
  let stackset_back = !stackset in
  g oc (dest, e1);
  let stackset1 = !stackset in
  Printf.fprintf oc "\tjmp\t%s\t\t\t%s\n" b_cont (posstr p);
  Printf.fprintf oc "%s:\t\t\t%s\n" b_else (posstr p);
  stackset := stackset_back;
  g oc (dest, e2);
  Printf.fprintf oc "%s:\t\t\t%s\n" b_cont (posstr p);
  let stackset2 = !stackset in
  stackset := S.inter stackset1 stackset2
and g'_args oc p x_reg_cl ys zs =
  assert ((List.length ys + List.length zs) <= Array.length regs - List.length x_reg_cl);
  (* cyclic movesのために一時的にあれする場所 *)
  let sw = Printf.sprintf "%d(%s)" (stacksize ()) reg_sp in
  (* レジスタにいれるべきやつ（引数とか）をまとめる *)
  let (i, yrs) =
    List.fold_left
      (fun (i, yrs) y -> (i + 1, (y, regs.(i)) :: yrs))
      (0, x_reg_cl)
      (ys@zs) in
  List.iter
    (function
       | (y, r) when y="_SW_" ->
           Printf.fprintf oc "\tld\t%s, %s\t\t\t%s\n" r sw (posstr p)
       | (y, r) when r="_SW_" ->
           Printf.fprintf oc "\tsto\t%s, %s\t\t\t%s\n" y sw (posstr p)
       | (y, r) ->
           Printf.fprintf oc "\tmov\t%s, %s\t\t\t%s\n" r y (posstr p))
    (shuffle "_SW_" yrs)
(* 必要ならリンクレジスタを戻してからjmp *)
and g'_lnk_restore oc lnk_restore =
  if lnk_restore then
    (Printf.fprintf oc "\taddi\t%s, %s, -1\n" reg_sp reg_sp;
     Printf.fprintf oc "\tld\t%s, (%s)\n" reg_lnk reg_sp);
and g'_ret oc lnk_restore =
  g'_lnk_restore oc lnk_restore;
  Printf.fprintf oc "\tjmp\t%s\n" reg_lnk

let h oc { name = Id.L(x); args = _; fargs = _; body = e; ret = _ } =
  Printf.fprintf oc ".globl %s\n" x;
  Printf.fprintf oc "%s:\n" x;
  let hc = has_call e in
  if hc then
    (* 関数呼び出しがあるからリンクレジスタを退避 *)
    (Printf.fprintf oc "\tsto\t%s, (%s)\n" reg_lnk reg_sp;
    Printf.fprintf oc "\taddi\t%s, %s, 1\n" reg_sp reg_sp);
  stackset := S.empty;
  stackmap := [];
  g oc (Tail hc, e)

let f oc (Prog(fundefs, globals, e)) =
  Format.eprintf "generating assembly...@.";
  (match globals with
     | [] -> ()
     | _ ->
         (* globalsに何かあれば.dataセクションを出力する *)
         Printf.fprintf oc ".data\n";
         List.iter (fun {name = Id.L(l); size = size} ->
                      Printf.fprintf oc ".globl %s\n" l;
                      Printf.fprintf oc "%s:\n" l;
                      Printf.fprintf oc "\t.space %d\n" size) globals;
         Printf.fprintf oc "\n");

  Printf.fprintf oc ".text\n";
  (* mainの出力 *)
  let mcs = Id.genid "min_caml_start" in
  if e <> Ans Nop then
    (* Nopだけなら何も出力しない *)
    (Printf.fprintf oc ".globl %s\n" mcs;
     Printf.fprintf oc ".main %s\n" mcs;
     Printf.fprintf oc "%s:\n" mcs;
     g oc (NonTail(regs.(0)), e);
     (* 必要ならhaltを出力する命令 *)
     Printf.fprintf oc "\t.end\n");


  (* 関数たち *)
  stackset := S.empty;
  stackmap := [];
  List.iter (fun fundef -> h oc fundef) fundefs
