(* starlightの何かだ *)

type id_or_imm = V of Id.t | C of int
type t = (* 命令の列 *)
  | Ans of exp
  | Let of (Id.t * Type.t) * exp * t
and exp =
  | Nop
  | Add of Syntax.pos_t * (Id.t * id_or_imm)
  | Sub of Syntax.pos_t * (Id.t * id_or_imm)
  | LShift of Syntax.pos_t * (Id.t * id_or_imm)
  | RShift of Syntax.pos_t * (Id.t * id_or_imm)
  | FAdd of Syntax.pos_t * (Id.t * Id.t)
  | FSub of Syntax.pos_t * (Id.t * Id.t)
  | FMul of Syntax.pos_t * (Id.t * Id.t)
  | FInv of Syntax.pos_t * Id.t
  | FAbs of Syntax.pos_t * Id.t
  | Ld of Syntax.pos_t * (Id.t * int)
  | Ld2 of Syntax.pos_t * (Id.t * Id.t * int)
  | Sto of Syntax.pos_t * (Id.t * (Id.t * int))
  | Sto2 of Syntax.pos_t * (Id.t * (Id.t * Id.t * int))
  | Out of Syntax.pos_t * Id.t
  | In of Syntax.pos_t
  | Halt of Syntax.pos_t
  | Comment of Syntax.pos_t * string
  (* virtual instructions *)
  | Mul of Syntax.pos_t * (Id.t * Id.t)
  | Div of Syntax.pos_t * (Id.t * Id.t)
  | Set of Syntax.pos_t * int
  | SetL of Syntax.pos_t * Id.l
  | Mov of Syntax.pos_t * Id.t
  | Neg of Syntax.pos_t * Id.t
  | FNeg of Syntax.pos_t * Id.t
  | IfEq of Syntax.pos_t * Syntax.pos_t * (Id.t * id_or_imm * t * t)
  | IfLE of Syntax.pos_t * Syntax.pos_t * (Id.t * id_or_imm * t * t)
  | IfGE of Syntax.pos_t * Syntax.pos_t * (Id.t * id_or_imm * t * t) (* 左右対称ではないので必要 *)
  | IfFEq of Syntax.pos_t * Syntax.pos_t * (Id.t * Id.t * t * t)
  | IfFLE of Syntax.pos_t * Syntax.pos_t * (Id.t * Id.t * t * t)
  (* closure address, integer arguments, and float arguments *)
  | CallCls of Syntax.pos_t * (Id.t * Id.t list * Id.t list)
  | CallDir of Syntax.pos_t * (Id.l * Id.t list * Id.t list)
  | Save of Syntax.pos_t * (Id.t * Id.t) (* レジスタ変数の値をスタック変数へ保存 (caml2html: sparcasm_save) *)
  | Restore of Syntax.pos_t * Id.t (* スタック変数から値を復元 (caml2html: sparcasm_restore) *)
type fundef = { name : Id.l; args : Id.t list; fargs : Id.t list; body : t; ret : Type.t }
type global = { name : Id.l; size: int }
(* プログラム全体 = トップレベル関数 + グローバル変数データ + メインの式 (caml2html: sparcasm_prog) *)
type prog = Prog of fundef list * global list * t

let seq(e1, e2) = Let((Id.gentmp Type.Unit, Type.Unit), e1, e2)

let regs = Array.init 61 (fun i -> Printf.sprintf "%%r%d" (i+1))
let allregs = Array.to_list regs
let reg_lnk= "%r60" (* link register *)
let reg_cl = "%r61" (* closure address (caml2html: sparcasm_regcl) *)
let reg_hp = "%r62"       (* heap pointer *)
let reg_sp = "%r63"       (* stack pointer *)
 

(*
let reg_sw = regs.(Array.length regs - 1) (* temporary for swap *)
let reg_fsw = fregs.(Array.length fregs - 1) (* temporary for swap *)
*)
let is_reg x = (x.[0] = '%')

(* super-tenuki *)
let rec remove_and_uniq xs = function
  | [] -> []
  | x :: ys when S.mem x xs -> remove_and_uniq xs ys
  | x :: ys -> x :: remove_and_uniq (S.add x xs) ys

(* free variables in the order of use (for spilling) (caml2html: sparcasm_fv) *)
let fv_id_or_imm = function V(x) -> [x] | _ -> []
let rec fv_exp = function
  | Nop | Comment(_) | Restore(_) | In(_) | Halt(_) | Set(_) | SetL(_) -> []
  | Save(_,(x, _)) | Out(_,x) | FInv(_,x) | FAbs(_,x) | Ld(_,(x,_)) | Mov(_,x) | Neg(_,x) | FNeg(_,x) -> [x]
  | Add(_,(x, y')) | Sub(_,(x, y')) | LShift(_,(x,y')) | RShift(_,(x,y')) -> x :: fv_id_or_imm y'
  | Mul(_,(x, y)) | Div(_,(x, y)) | FAdd(_,(x, y)) | FSub(_,(x, y)) | FMul(_,(x, y))
  | Ld2(_,(x,y,_)) | Sto(_,(x,(y,_))) -> [x; y]
  | Sto2(_,(x,(y,z,_))) -> [x; y; z]
  | IfEq(_,_,(x, y', e1, e2)) | IfLE(_,_,(x, y', e1, e2)) | IfGE(_,_,(x, y', e1, e2)) -> x :: fv_id_or_imm y' @ remove_and_uniq S.empty (fv e1 @ fv e2) (* uniq here just for efficiency *)
  | IfFEq(_,_,(x, y, e1, e2)) | IfFLE(_,_,(x, y, e1, e2)) -> x :: y :: remove_and_uniq S.empty (fv e1 @ fv e2) (* uniq here just for efficiency *)
  | CallCls(_,(x, ys, zs)) -> x :: ys @ zs
  | CallDir(_,(_, ys, zs)) -> ys @ zs
and fv = function
  | Ans(exp) -> fv_exp exp
  | Let((x, t), exp, e) ->
      fv_exp exp @ remove_and_uniq (S.singleton x) (fv e)
let fv e = remove_and_uniq S.empty (fv e)

let rec concat e1 xt e2 =
  match e1 with
  | Ans(exp) -> Let(xt, exp, e2)
  | Let(yt, exp, e1') -> Let(yt, exp, concat e1' xt e2)

let align i = (if i mod 8 = 0 then i else i + 4)

(* 命令列が関数呼び出しを含むか *)
let rec has_call e =
  let rec has_call' = function
    | Nop | Add(_) | Sub(_) | LShift(_) | RShift(_) | FAdd(_) | FSub(_) | FMul(_) | FInv(_) | FAbs(_) | Ld(_) | Ld2(_) | Sto(_) | Sto2(_) | Out(_) | In(_) | Halt(_) | Comment(_)
    | Mul(_) | Div(_) | Set(_) | SetL(_) | Mov(_) | Neg(_) | FNeg(_) | Save(_) | Restore(_) ->
        false
    | IfEq(_,_,(_,_,e1,e2)) | IfLE(_,_,(_,_,e1,e2)) | IfGE(_,_,(_,_,e1,e2)) | IfFEq(_,_,(_,_,e1,e2)) | IfFLE(_,_,(_,_,e1,e2)) ->
        has_call e1 || has_call e2
    | CallCls(_) | CallDir(_) -> true in
    match e with
      | Ans e1 -> has_call' e1
      | Let(_,e1,e') ->
          has_call' e1 || has_call e'

let rec join gl l =
  match l with
    | [] -> ""
    | x::[] -> x
    | x::l' -> x ^ gl ^ join gl l'
let tree p =
  let rec f = function
    | Ans exp -> f' exp
    | Let((x,t), exp, e) -> ("LET " ^ x, [Ans exp; e])
  and f' = function
    | Nop -> ("NOP", [])
    | Add(_,(x,C(i))) -> ("ADD " ^ x ^ ", C(" ^ string_of_int i ^ ")", [])
    | Add(_,(x,V(y))) -> ("ADD " ^ x ^ ", " ^ y,  [])
    | Sub(_,(x,C(i))) -> ("SUB " ^ x ^ ", C(" ^ string_of_int i ^ ")", [])
    | Sub(_,(x,V(y))) -> ("SUB " ^ x ^ ", " ^ y,  [])
    | LShift(_,(x,C(i))) -> ("LSHIFT " ^ x ^ ", C(" ^ string_of_int i ^ ")", [])
    | LShift(_,(x,V(y))) -> ("LSHIFT " ^ x ^ ", " ^ y,  [])
    | RShift(_,(x,C(i))) -> ("RSHIFT " ^ x ^ ", C(" ^ string_of_int i ^ ")", [])
    | RShift(_,(x,V(y))) -> ("RSHIFT " ^ x ^ ", " ^ y,  [])
    | FAdd(_,(x,y)) -> ("FADD " ^ x ^ ", " ^ y,  [])
    | FSub(_,(x,y)) -> ("FSUB " ^ x ^ ", " ^ y,  [])
    | FMul(_,(x,y)) -> ("FMUL " ^ x ^ ", " ^ y,  [])
    | FInv(_,x) -> ("FINV " ^ x,  [])
    | FAbs(_,x) -> ("FABS " ^ x,  [])
    | Ld(_,(x,i)) -> ("LD " ^ string_of_int i ^ "(" ^ x ^ ")",  [])
    | Ld2(_,(x,y,i)) -> ("LD " ^ string_of_int i ^ "(" ^ x ^ " + " ^ y ^ ")",  [])
    | Sto(_,(x,(y,i))) -> ("STO " ^ x ^ ", " ^ string_of_int i ^ "(" ^ y ^ ")",  [])
    | Sto2(_,(x,(y,z,i))) -> ("STO2 " ^ x ^ "," ^  string_of_int i ^ "(" ^ y ^ " + " ^ z ^ ")",  [])
    | Out(_,x) -> ("OUT " ^ x,  [])
    | In(_) -> ("IN",  [])
    | Halt(_) -> ("HALT",  [])
    | Comment(_,x) -> ("COMMENT " ^ x,  [])
    | Mul(_,(x,y)) -> ("MUL " ^ x ^ ", " ^ y,  [])
    | Div(_,(x,y)) -> ("DIV " ^ x ^ ", " ^ y,  [])
    | Set(_,i) -> ("SET " ^ string_of_int i, [])
    | SetL(_,Id.L(l)) -> ("SETL L(" ^ l ^ ")", [])
    | Mov(_,x) -> ("MOV " ^ x, [])
    | Neg(_,x) -> ("NEG " ^ x, [])
    | FNeg(_,x) -> ("FNEG " ^ x,  [])
    | IfEq(_,_,(x,C(i), e1, e2)) -> ("IFEQ " ^ x ^ ", C(" ^ string_of_int i ^ ")",  [e1; e2])
    | IfEq(_,_,(x,V(y), e1, e2)) -> ("IFEQ " ^ x ^ ", " ^ y,  [e1; e2])
    | IfLE(_,_,(x,C(i), e1, e2)) -> ("IFLE " ^ x ^ ", C(" ^ string_of_int i ^ ")",  [e1; e2])
    | IfLE(_,_,(x,V(y), e1, e2)) -> ("IFLE " ^ x ^ ", " ^ y,  [e1; e2])
    | IfGE(_,_,(x,C(i), e1, e2)) -> ("IFGE " ^ x ^ ", C(" ^ string_of_int i ^ ")",  [e1; e2])
    | IfGE(_,_,(x,V(y), e1, e2)) -> ("IFGE " ^ x ^ ", " ^ y,  [e1; e2])
    | IfFEq(_,_,(x,y, e1, e2)) -> ("IFFEQ " ^ x ^ ", " ^ y,  [e1; e2])
    | IfFLE(_,_,(x,y, e1, e2)) -> ("IFFLE " ^ x ^ ", " ^ y,  [e1; e2])
    | CallCls(_,(x,ys,zs)) -> ("CALLCLS " ^ x ^ " " ^ join ", " ys ^ " " ^ join ", " zs,  [])
    | CallDir(_,(Id.L(x),ys,zs)) -> ("CALLDIR L(" ^ x ^ ") " ^ join ", " ys ^ " " ^ join ", " zs,  [])
    | Save(_,(x, y)) -> ("SAVE " ^ x ^ ", " ^ y,  [])
    | Restore(_,x) -> ("RESTORE " ^ x,  []) in
  let rec g = function
    | Ans exp -> g' exp
    | Let _  -> ""
  and g' = function
    | Nop -> ""
    | Add(p,_) | Sub(p,_) | LShift(p,_) | RShift(p,_) | FAdd(p,_) | FSub(p,_)
    | FMul(p,_) | FInv(p,_) | FAbs(p,_) | Ld(p,_) | Ld2(p,_) | Sto(p,_) | Sto2(p,_)
    | Out(p,_) | In(p) | Halt(p) | Comment(p,_) | Mul(p,_) | Div(p,_) | Set(p,_) | SetL(p,_)
    | Mov(p,_) | Neg(p,_) | FNeg(p,_) | IfEq(p,_,_) | IfLE(p,_,_) | IfGE(p,_,_)
    | IfFEq(p,_,_) | IfFLE(p,_,_) | CallCls(p,_) | CallDir(p,_)
    | Save(p,_) | Restore(p,_) -> Syntax.pos_str p in
  let Prog(fs,_,e) = p in
  let buf = Buffer.create 1024 in
    List.iter (fun (fd:fundef) ->
                 let Id.L(id) = fd.name in
                   Buffer.add_string buf ("  FUNC L("^id^") " ^ join " " fd.args ^ "\n");
                   Buffer.add_string buf ((Tree.make f g "    " fd.body))) fs;
    Buffer.add_string buf (Tree.make f g "  " e);
    Buffer.contents buf

