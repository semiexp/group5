type id_or_imm = V of Id.t | C of int
type t = (* 命令の列 *)
  | Ans of exp
  | Let of (Id.t * Type.t) * exp * t
and exp =
  | Nop
  | Add of Syntax.pos_t * (Id.t * id_or_imm)
  | Sub of Syntax.pos_t * (Id.t * id_or_imm)
  | LShift of Syntax.pos_t * (Id.t * id_or_imm)
  | RShift of Syntax.pos_t * (Id.t * id_or_imm)
  | FAdd of Syntax.pos_t * (Id.t * Id.t)
  | FSub of Syntax.pos_t * (Id.t * Id.t)
  | FMul of Syntax.pos_t * (Id.t * Id.t)
  | FInv of Syntax.pos_t * Id.t
  | FAbs of Syntax.pos_t * Id.t
  | Ld of Syntax.pos_t * (Id.t * int)
  | Ld2 of Syntax.pos_t * (Id.t * Id.t * int)
  | Sto of Syntax.pos_t * (Id.t * (Id.t * int))
  | Sto2 of Syntax.pos_t * (Id.t * (Id.t * Id.t * int))
  | Out of Syntax.pos_t * Id.t
  | In of Syntax.pos_t
  | Halt of Syntax.pos_t
  | Comment of Syntax.pos_t * string
  (* virtual instructions *)
  | Mul of Syntax.pos_t * (Id.t * Id.t)
  | Div of Syntax.pos_t * (Id.t * Id.t)
  | Set of Syntax.pos_t * int
  | SetL of Syntax.pos_t * Id.l
  | Mov of Syntax.pos_t * Id.t
  | Neg of Syntax.pos_t * Id.t
  | FNeg of Syntax.pos_t * Id.t
  | IfEq of Syntax.pos_t * Syntax.pos_t * (Id.t * id_or_imm * t * t)
  | IfLE of Syntax.pos_t * Syntax.pos_t * (Id.t * id_or_imm * t * t)
  | IfGE of Syntax.pos_t * Syntax.pos_t * (Id.t * id_or_imm * t * t) (* 左右対称ではないので必要 *)
  | IfFEq of Syntax.pos_t * Syntax.pos_t * (Id.t * Id.t * t * t)
  | IfFLE of Syntax.pos_t * Syntax.pos_t * (Id.t * Id.t * t * t)
  (* closure address, integer arguments, and float arguments *)
  | CallCls of Syntax.pos_t * (Id.t * Id.t list * Id.t list)
  | CallDir of Syntax.pos_t * (Id.l * Id.t list * Id.t list)
  | Save of Syntax.pos_t * (Id.t * Id.t) (* レジスタ変数の値をスタック変数へ保存 (caml2html: sparcasm_save) *)
  | Restore of Syntax.pos_t * Id.t (* スタック変数から値を復元 (caml2html: sparcasm_restore) *)
type fundef = { name : Id.l; args : Id.t list; fargs : Id.t list; body : t; ret : Type.t }
type global = { name : Id.l; size: int }
type prog = Prog of fundef list * global list * t

val has_call : t -> bool
val seq : exp * t -> t (* shorthand of Let for unit *)

val regs : Id.t array
val allregs : Id.t list
val reg_lnk: Id.t
val reg_cl : Id.t
val reg_hp : Id.t
val reg_sp : Id.t
val is_reg : Id.t -> bool

val fv : t -> Id.t list
val concat : t -> Id.t * Type.t -> t -> t

val align : int -> int

val tree : prog -> string
