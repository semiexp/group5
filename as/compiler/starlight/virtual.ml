(* translation into assembly with infinite number of virtual registers *)

let software_finv = ref false

open Asm


(*let data = ref [] (* 浮動小数点数の定数テーブル (caml2html: virtual_data) *)*)

let classify xts ini addf addi =
  List.fold_left
    (fun acc (x, t) ->
      match t with
      | Type.Unit -> acc
      | Type.Float -> addf acc x
      | _ -> addi acc x t)
    ini
    xts

let separate xts =
  classify
    xts
    ([], [])
    (fun (int, float) x -> (int, float @ [x]))
    (fun (int, float) x _ -> (int @ [x], float))

let expand xts ini addf addi =
  classify
    xts
    ini
    (fun (offset, acc) x ->
      (offset + 1, addf x offset acc))
    (fun (offset, acc) x t ->
      (offset + 1, addi x t offset acc))

let rec g env = function (* 式の仮想マシンコード生成 (caml2html: virtual_g) *)
  | Closure.Unit(_) -> Ans(Nop)
  | Closure.Int(p,i) -> Ans(Set(p,i))
  | Closure.Float(p,d) ->
      (* IEEE 754 *)
      Ans(Set(p, Int32.to_int(Int32.bits_of_float(d))))
  | Closure.Neg(p,x) -> Ans(Neg(p,x))
  | Closure.Add(p,(x, y)) -> Ans(Add(p,(x, V(y))))
  | Closure.Sub(p,(x, y)) -> Ans(Sub(p,(x, V(y))))
  | Closure.Mul(p,(x, y)) -> Ans(Mul(p,(x, y)))
  | Closure.Div(p,(x, y)) -> Ans(Div(p,(x, y)))
  | Closure.FNeg(p,x) -> Ans(FNeg(p,x))
  | Closure.FAdd(p,(x, y)) -> Ans(FAdd(p,(x, y)))
  | Closure.FSub(p,(x, y)) -> Ans(FSub(p,(x, y)))
  | Closure.FMul(p,(x, y)) -> Ans(FMul(p,(x, y)))
  | Closure.FDiv(p,(x, y)) ->
      (* fdivはfinvしてmul *)
      let a = Id.genid "f" in
      if !software_finv then
        (* sortware finvを使う *)
        Let((a, Type.Float), CallDir(p, (Id.L("min_caml_finv"), [], [y])),
            Ans(FMul(p, (x, a))))
      else
        Let((a, Type.Float), FInv(p, y),
            Ans(FMul(p, (x, a))))
  | Closure.IfEq(p1,p2,(x, y, e1, e2)) ->
      (match M.find x env with
      | Type.Bool | Type.Int -> Ans(IfEq(p1,p2,(x, V(y), g env e1, g env e2)))
      | Type.Float -> Ans(IfFEq(p1,p2,(x, y, g env e1, g env e2)))
      | _ -> failwith "equality supported only for bool, int, and float")
  | Closure.IfLE(p1,p2,(x, y, e1, e2)) ->
      (match M.find x env with
      | Type.Bool | Type.Int -> Ans(IfLE(p1,p2,(x, V(y), g env e1, g env e2)))
      | Type.Float -> Ans(IfFLE(p1,p2,(x, y, g env e1, g env e2)))
      | _ -> failwith "inequality supported only for bool, int, and float")
  | Closure.Let(_,((x, t1), e1, e2)) ->
      let e1' = g env e1 in
      let e2' = g (M.add x t1 env) e2 in
      concat e1' (x, t1) e2'
  | Closure.Var(p,x) ->
      (match M.find x env with
      | Type.Unit -> Ans(Nop)
      | _ -> Ans(Mov(p,x)))
  | Closure.MakeCls(p,((x, t), { Closure.entry = l; Closure.actual_fv = ys }, e2)) -> (* クロージャの生成 (caml2html: virtual_makecls) *)
      (* Closureのアドレスをセットしてから、自由変数の値をストア *)
      let e2' = g (M.add x t env) e2 in
      let offset, store_fv =
        expand
          (List.map (fun y -> (y, M.find y env)) ys)
          (1, e2')
          (fun y offset store_fv -> seq(Sto(p,(y, (x, offset))), store_fv))
          (fun y _ offset store_fv -> seq(Sto(p,(y, (x, offset))), store_fv)) in
        (* ヒープポインタ（この位置に関数アドレスが入る）をxに保存 *)
        Let((x, t), Mov(p,reg_hp),
            (* ストアした自由変数の分だけヒープポインタを進める *)
            Let((reg_hp, Type.Int), Add(p,(reg_hp, C(offset))),
                let z = Id.genid "l" in
                  (* 関数のアドレスをxの位置にストア *)
                  Let((z, Type.Int), SetL(p,l),
                      seq(Sto(p, (z, (x, 0))),
                          (* store_fv内で自由変数もストアしておわり *)
                          store_fv))))
  | Closure.AppCls(p,(x, ys)) ->
      let (int, float) = separate (List.map (fun y -> (y, M.find y env)) ys) in
      Ans(CallCls(p,(x, int, float)))
  | Closure.AppDir(p,(Id.L(x), ys)) ->
      let (int, float) = separate (List.map (fun y -> (y, M.find y env)) ys) in
      Ans(CallDir(p,(Id.L(x), int, float)))
  | Closure.Tuple(p,xs) -> (* 組の生成 (caml2html: virtual_tuple) *)
      let y = Id.genid "t" in
      let (offset, store) =
        (* 0番から順にヒープにストア *)
        expand
          (List.map (fun x -> (x, M.find x env)) xs)
          (0, Ans(Mov(p,y)))
          (fun x offset store -> seq(Sto(p,(x, (y, offset))), store))
          (fun x _ offset store -> seq(Sto(p,(x, (y, offset))), store)) in
        (* yにタプルのアドレスを入れる *)
        Let((y, Type.Tuple(List.map (fun x -> M.find x env) xs)), Mov(p,reg_hp),
            (* ヒープポインタを動かす *)
            Let((reg_hp, Type.Int), Add(p,(reg_hp, C(offset))),
                (* データをストアしてyを返す *)
                store))
  | Closure.LetTuple(p,(xts, y, e2)) ->
      let s = Closure.fv e2 in
      let (offset, load) =
        expand
          xts
          (0, g (M.add_list xts env) e2)
          (* 中で使わないものはロードしていない *)
          (fun x offset load ->
             if not (S.mem x s) then load else (* [XX] a little ad hoc optimization *)
               Let((x, Type.Float), Ld(p,(y, offset)), load))
          (fun x t offset load ->
             if not (S.mem x s) then load else (* [XX] a little ad hoc optimization *)
               Let((x, t), Ld(p, (y, offset)), load)) in
        load
  | Closure.Get(p,(x, y)) -> (* 配列の読み出し (caml2html: virtual_get) *)
      (match M.find x env with
      | Type.Array(Type.Unit) -> Ans(Nop)
      | Type.Array(_) -> Ans(Ld2(p,(x, y, 0)))
      | _ -> assert false)
  | Closure.Put(p,(x, y, z)) ->
      (match M.find x env with
      | Type.Array(Type.Unit) -> Ans(Nop)
      | Type.Array(_) -> Ans(Sto2(p,(z, (x, y, 0))))
      | _ -> assert false)
  | Closure.ExtArray(p,Id.L(x))
  | Closure.ExtTuple(p,Id.L(x)) -> Ans(SetL(p,Id.L("min_caml_" ^ x)))
  | Closure.GlobalArray(p,(l, size, y)) ->
      (* それ用の組み込み関数を使って配列を生成する *)
      let t1 = Id.gentmp Type.Int in
      let t2 = Id.gentmp Type.Int in
      Let((t1, Type.Int), SetL(p, l),
          Let((t2, Type.Int), Set(p, size),
              Ans(CallDir(p, (Id.L("min_caml_create_array_here"), [t1; t2; y], [])))))
  | Closure.GlobalTuple(p, (l, xs)) ->
      (* Tupleと同様に展開 *)
      let t = Id.gentmp Type.Int in
      let (offset, store) =
        expand
          (List.map (fun x -> (x, M.find x env)) xs)
          (0, Ans(Mov(p,t)))
          (fun x offset store -> seq(Sto(p,(x, (t, offset))), store))
          (fun x _ offset store -> seq(Sto(p,(x, (t, offset))), store)) in
        Let((t, Type.Tuple(List.map (fun x -> M.find x env) xs)), SetL(p, l),
            store)
  | Closure.AppNative(p, ((x, t), xs)) ->
      (match x with
         | "fabs" ->
             assert (List.length xs = 1);
             Ans(FAbs(p, List.hd xs))
         | "fneg" ->
             assert (List.length xs = 1);
             Ans(FNeg(p, List.hd xs))
         | "read" ->
             assert (List.length xs = 1); (* UNIT *)
             Ans(In(p))
         | "out" ->
             assert (List.length xs = 1);
             Ans(Out(p, List.hd xs))
         | _ ->
             failwith ("unsupported native '" ^ x ^ "'"))




(* 関数の仮想マシンコード生成 (caml2html: virtual_h) *)
let h { Closure.pos = p; Closure.name = (Id.L(x), t); Closure.args = yts; Closure.formal_fv = zts; Closure.body = e } =
  let (int, float) = separate yts in
  let (offset, load) =
    expand
      zts
      (* 1: クロージャのアドレスの次に自由変数が並んでいる *)
      (1, g (M.add x t (M.add_list yts (M.add_list zts M.empty))) e)
      (fun z offset load -> Let((z,Type.Float), Ld(p, (reg_cl, offset)), load))
      (fun z t offset load -> Let((z, t), Ld(p,(reg_cl, offset)), load)) in
    match t with
      | Type.Fun(_, t2) ->
          { name = Id.L(x); args = int; fargs = float; body = load; ret = t2 }
      | _ -> Printf.eprintf "%s\n" (Type.type_str t);  assert false

(* ネイティブ用のクロージャを生成 *)
(* XXX nativeの定義が2箇所に必要！！！ *)
let i (id, (l, t)) =
  let cl = 
    match id with
      | "fabs" | "fneg" ->
          let tmp = Id.gentmp Type.Float in
            {
              Closure.pos = Syntax.dummy_pos();
              Closure.name = (l, t);
              Closure.args = [(tmp, Type.Float)];
              Closure.formal_fv = [];
              Closure.body = Closure.AppNative(Syntax.dummy_pos(), ((id, t), [tmp]))
            }
      | "read" ->
          let tmp = Id.gentmp Type.Unit in
            {
              Closure.pos = Syntax.dummy_pos();
              Closure.name = (l, t);
              Closure.args = [(tmp, Type.Unit)];
              Closure.formal_fv = [];
              Closure.body = Closure.AppNative(Syntax.dummy_pos(), ((id, t), [tmp]))
            }
      | "out" ->
          let tmp = Id.gentmp Type.Int in
            {
              Closure.pos = Syntax.dummy_pos();
              Closure.name = (l, t);
              Closure.args = [(tmp, Type.Int)];
              Closure.formal_fv = [];
              Closure.body = Closure.AppNative(Syntax.dummy_pos(), ((id, t), [tmp]))
            }
      | _ -> failwith ("unsupported native '" ^ id ^ "'")
  in
    h cl

(* プログラム全体の仮想マシンコード生成 (caml2html: virtual_f) *)
let f (Closure.Prog(fundefs, globals, natives, e)) =
  let fundefs = List.map h fundefs in
  let globals = List.map (fun {Closure.name = (name, _); Closure.size = size} -> {name = name; size = size}) globals in
  let natives = List.map i natives in
  let e = g M.empty e in
  Prog(fundefs @ natives, globals, e)
