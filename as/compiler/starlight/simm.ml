open Asm

let rec g env = function (* 命令列の即値最適化 (caml2html: simm13_g) *)
  | Ans(exp) -> g' env exp
  | Let((x, t), Set(p,i), e) ->
      (* 変数xの中身はiであることが判明した *)
      let e' = g (M.add x i env) e in
      if List.mem x (fv e') then Let((x, t), Set(p,i), e') else
      ((* xは使われていないので消してしまう *)
       e')
  | Let(xt, exp, e) -> concat (g' env exp) xt (g env e)
and g' env = function (* 各命令の即値最適化 (caml2html: simm13_gprime) *)
  (* TODO 即値が入りきらないほど大きかったら? *)
  | Add(p,(x, V(y))) when M.mem y env -> Ans(Add(p,(x, C(M.find y env))))
  | Add(p,(x, V(y))) when M.mem x env -> Ans(Add(p,(y, C(M.find x env))))
  | Sub(p,(x, V(y))) when M.mem y env -> Ans(Sub(p,(x, C(M.find y env))))
  | Mul(p,(x, y)) when M.mem y env -> 
      execmul p x (M.find y env)
  | Mul(p,(x, y)) when M.mem x env ->
      execmul p y (M.find x env)
  | Div(p,(x, y)) when M.mem y env ->
      execdiv p x (M.find y env)
  | Mul(p,(x, y)) -> failwith "Mul var * var is not supported"
  | Div(p,_) -> failwith "Div _ / var is not supported"
  | Ld2(p,(x,y,i)) when M.mem y env ->
      Ans(Ld(p, (x, M.find y env + i)))
  | Ld2(p,(x,y,i)) when M.mem x env ->
      Ans(Ld(p, (y, M.find x env + i)))
  | Sto2(p,(x,(y,z,i))) when M.mem z env ->
      Ans(Sto(p, (x, (y, M.find z env + i))))
  | Sto2(p,(x,(y,z,i))) when M.mem y env ->
      Ans(Sto(p, (x, (z, M.find y env + i))))
  | IfEq(p1,p2,(x, V(y), e1, e2)) when M.mem y env -> Ans(IfEq(p1,p2,(x, C(M.find y env), g env e1, g env e2)))
  | IfLE(p1,p2,(x, V(y), e1, e2)) when M.mem y env -> Ans(IfLE(p1,p2,(x, C(M.find y env), g env e1, g env e2)))
  | IfGE(p1,p2,(x, V(y), e1, e2)) when M.mem y env -> Ans(IfGE(p1,p2,(x, C(M.find y env), g env e1, g env e2)))
  | IfEq(p1,p2,(x, V(y), e1, e2)) when M.mem x env -> Ans(IfEq(p1,p2,(y, C(M.find x env), g env e1, g env e2)))
  | IfLE(p1,p2,(x, V(y), e1, e2)) when M.mem x env -> Ans(IfGE(p1,p2,(y, C(M.find x env), g env e1, g env e2)))
  | IfGE(p1,p2,(x, V(y), e1, e2)) when M.mem x env -> Ans(IfLE(p1,p2,(y, C(M.find x env), g env e1, g env e2)))
  | IfEq(p1,p2,(x, y', e1, e2)) -> Ans(IfEq(p1,p2,(x, y', g env e1, g env e2)))
  | IfLE(p1,p2,(x, y', e1, e2)) -> Ans(IfLE(p1,p2,(x, y', g env e1, g env e2)))
  | IfGE(p1,p2,(x, y', e1, e2)) -> Ans(IfGE(p1,p2,(x, y', g env e1, g env e2)))
  | IfFEq(p1,p2,(x, y, e1, e2)) -> Ans(IfFEq(p1,p2,(x, y, g env e1, g env e2)))
  | IfFLE(p1,p2,(x, y, e1, e2)) -> Ans(IfFLE(p1,p2,(x, y, g env e1, g env e2)))
  | e -> Ans(e)

and execmul p x c =
  let rec b x y c f =
    if y = 0 then
      f
    else
      if (y land 1) = 1 then
        b x (y lsr 1) (c+1) (fun t -> 
                             let t1 = Id.gentmp Type.Int in
                             let t2 = Id.gentmp Type.Int in
                               Let((t1, Type.Int), (if c=0 then Mov(p,x) else LShift(p, (x, C(c)))),
                                   Let((t2, Type.Int), Add(p, (t, V(t1))),
                                       f t2)))
      else
        b x (y lsr 1) (c+1) f in
    if c < 0 then
      (* 負の数をかけたい気分だ *)
      let c' = -c in
      (* x'の絶対値をとって計算してもどす *)
      let x' = Id.gentmp Type.Int in
      let t = Id.gentmp Type.Int in
        Let((t, Type.Int), Set(p, 0),
            Ans(IfLE(p, p, (x, C(0), 
                            (* xが負だったときは正にもどして計算してそのまま *)
                            Let((x', Type.Int), Neg(p, x),
                                b x' c' 0 (fun t -> Ans(Mov(p, t))) t),
                                (* xが正だったときは計算したあと負にする *)
                                b x c' 0 (fun t -> Ans(Neg(p, t))) t))))
        else
          (* 正の数をかける *)
          let x' = Id.gentmp Type.Int in
          let t = Id.gentmp Type.Int in
            Let((t, Type.Int), Set(p, 0),
                Ans(IfLE(p, p, (x, C(0), 
                                (* xが負だったときは正にもどして計算して負に戻す *)
                                Let((x', Type.Int), Neg(p, x),
                                    b x' c 0 (fun t -> Ans(Neg(p, t))) t),
                                    (* xが正だったときはそのまま計算 *)
                                    b x c 0 (fun t -> Ans(Mov(p, t))) t))))

and execdiv p x c =
  (* 右シフトで演算可能なもののみOK *)
  if c = 0 then failwith "0 div"
  else
    let c' = abs c in
    let rec sfr c n =
      let c' = c lsr 1 in
        if (c land 1) = 1 then
          (if c' <> 0 then
             (* 右シフトで演算可能でない *)
             failwith "This div is not executable using rshift."
           else
               n)
        else
          sfr c' (n+1) in
    let rshiftnum = sfr c' 0 in
    let x1 = Id.gentmp Type.Int in
    let x2 = Id.gentmp Type.Int in
      if c < 0 then
        (* cが負 *)
        Ans(IfLE(p, p, (x, C(0),
                        (* xが負なら正になおしてから *)
                        Let((x1, Type.Int), Neg(p, x),
                            Ans(RShift(p, (x1, C(rshiftnum))))),
                        (* xが正なら割った後に負にする *)
                        Let((x2, Type.Int), RShift(p, (x, C(rshiftnum))),
                            Ans(Neg(p, x2))))))
      else
          (* cが正 *)
          Ans(IfLE(p, p, (x, C(0),
                          (* xが負なら正になおしてから *)
                          Let((x1, Type.Int), Neg(p, x),
                              Let((x2, Type.Int), RShift(p, (x1, C(rshiftnum))),
                                  Ans(Neg(p, x2)))),
                          (* xが正ならそのままわる *)
                          Ans(RShift(p, (x, C(rshiftnum)))))))


let h { name = l; args = xs; fargs = ys; body = e; ret = t } = (* トップレベル関数の即値最適化 *)
  { name = l; args = xs; fargs = ys; body = g M.empty e; ret = t }

let f (Prog(fundefs, globals, e)) = (* プログラム全体の即値最適化 *)
  Prog(List.map h fundefs, globals, g M.empty e)
