open Closure
(* callグラフを作る *)

(* Id.t * id.tのSet *)
module STT = Set.Make(struct
                        type t = Id.t * Id.t
                        let compare : t -> t -> int = compare
                      end)


(* 
 * depth: 関数のあれ
 * env: 変数がclosureである場合の候補
 * num: 現在の自分の番号
 * callees: この式から呼び出される可能性がある関数の集合
 * 返り値: (callees', num', cls)
 * cls: この式がclosureである場合の候補
 *)
let rec g  depth env num callees = function
  | If(_,_,(_,e1,e2)) ->
      let callees'1, num'1, cls'1 = g depth env num callees e1 in
      let callees'2, num'2, cls'2 = g depth env num callees e2 in
      (* calleesは合併する *)
      let callees' = S.union callees'1 callees'2 in
      (* numは大きいほうを採用 *)
      let num' = max num'1 num'2 in
      (* clsも合併する *)
      let cls' = S.union cls'1 cls'2 in
        (callees', num', cls')
  | Let(_,((x, t), e1, e2)) ->
      let callees', num', cls' = g depth env num callees e1 in
      let env' =
        if cls' <> S.empty then
          (* クロージャがある *)
          M.add x cls' env
        else
          env in
      let num' = max num num' in
        g depth env' num' callees' e2
  | Var(_, x) ->
      (* clsが伝播する *)
      (try
         let cls = M.find x env in
           (callees, num, cls)
       with Not_found -> (callees, num, S.empty))
  | MakeCls(_,((x, t), { entry = Id.L(y) }, e2)) ->
      (* clsを作成 *)
      let cls = S.singleton y in
      let env' = M.add x cls env in
        g depth env' num callees e2
  | AppCls(_,(x, ys)) ->
      (* xはクロージャ *)
      (* クロージャの返り値はわからん！！！ *)
      (* すでに定義されている関数全てかも *)
      let cls = S.of_list
                  (List.map
                     (fun (y, _) -> y)
                     (M.bindings depth)) in
      (* xの候補 *)
      let s = (try
                 M.find x env
               with Not_found -> cls) in
      (* 呼ばれる可能性があるものをcalleesに追加 *)
      let callees' = S.union callees s in
      (* 関数を呼んでいるので番号を更新 *)
      let num' = S.fold
                   (fun y num' ->
                      try
                        let n = M.find y depth in
                          max (n+1) num'
                      with Not_found -> num') s num in
        (callees', num', cls)
  | AppDir(_, (Id.L(x), ys)) ->
      (* トップレベル関数を直で呼んでる *)
      let callees' = S.add x callees in
      let cls = S.of_list
                  (List.map
                     (fun (y, _) -> y)
                     (M.bindings depth)) in
      let num' =
        (try
           let n = M.find x depth in
             max (n+1) num
         with Not_found -> num) in
        (callees', num', cls)
  | LetTuple(_,(xts, y, e2)) ->
      (* タプルの中身がクロージャかもしれない *)
      let env' = List.fold_left
                   (fun env' (x, t) ->
                      match t with
                        | Type.Fun _ ->
                            (* xはクロージャである *)
                            M.add
                              x
                              (S.of_list
                                 (List.map
                                    (fun (y, _) -> y)
                                    (M.bindings depth)))
                              env'
                        | _ -> env')
                   env
                   xts in
        g depth env' num callees e2
  | _ ->
      (* 終点 *)
      (* XXX GetやAppNativeがクロージャを返した場合は？ *)
      (callees, num, S.empty)



let f (Prog(fundefs, globals, natives, e)) =
  (* callグラフを作る *)
  (* fundefsは先に定義されたものほど左 *)
  let (graph, depth) = List.fold_left
                         (fun (graph, depth) { name = (Id.L(x), _); body = e} ->
                            let (callees, num, _) = g depth M.empty 0 S.empty e in
                            (* 呼ばれる可能性があるものはgraphに追加 *)
                              Printf.eprintf "%s depth: %d\n" x num;
                            let graph' = STT.union
                                           graph
                                           (List.fold_left
                                              (fun gr y -> Printf.eprintf "call: %s -> %s\n" x y;STT.add (x, y) gr)
                                              STT.empty
                                              (S.elements callees)) in
                            let depth' = M.add x num depth in
                              (graph', depth'))
                         (STT.empty, M.empty)
                         fundefs in
    STT.iter
      (fun (x, y) ->
         Printf.eprintf "%s -> %s\n" x y)
      graph;
    M.iter
      (fun x n ->
         Printf.eprintf "%s: depth %d\n" x n)
      depth;
    (graph, depth)

