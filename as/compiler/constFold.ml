open KNormal

let memi x env =
  try (match M.find x env with Int(_) -> true | _ -> false)
  with Not_found -> false
let memf x env =
  try (match M.find x env with Float(_) -> true | _ -> false)
  with Not_found -> false
let memt x env =
  try (match M.find x env with Tuple(_) -> true | _ -> false)
  with Not_found -> false

let findi x env = (match M.find x env with Int(_,i) -> i | _ -> raise Not_found)
let findf x env = (match M.find x env with Float(_,d) -> d | _ -> raise Not_found)
let findt x env = (match M.find x env with Tuple(_,ys) -> ys | _ -> raise Not_found)

let rec g env = function (* 定数畳み込みルーチン本体 (caml2html: constfold_g) *)
  | Var(p,x) when memi x env -> Int(p,findi x env)
  (* | Var(x) when memf x env -> Float(findf x env) *)
  (* | Var(x) when memt x env -> Tuple(findt x env) *)
  | Neg(p,x) when memi x env -> Int(p,-(findi x env))
  | Add(p,(x, y)) when memi x env && memi y env -> Int(p,findi x env + findi y env) (* 足し算のケース (caml2html: constfold_add) *)
  | Sub(p,(x, y)) when memi x env && memi y env -> Int(p,findi x env - findi y env)
  | Mul(p,(x, y)) when memi x env && memi y env -> Int(p,findi x env * findi y env)
  | Div(p,(x, y)) when memi x env && memi y env -> Int(p,findi x env / findi y env)
  | FNeg(p,x) when memf x env -> Float(p,-.(findf x env))
  | FAdd(p,(x, y)) when memf x env && memf y env -> Float(p,findf x env +. findf y env)
  | FSub(p,(x, y)) when memf x env && memf y env -> Float(p,findf x env -. findf y env)
  | FMul(p,(x, y)) when memf x env && memf y env -> Float(p,findf x env *. findf y env)
  | FDiv(p,(x, y)) when memf x env && memf y env -> Float(p,findf x env /. findf y env)
  | CmpEq(p,(x, y)) when memi x env && memi y env -> Int(p, if findi x env = findi y env then 1 else 0)
  | CmpLt(p,(x, y)) when memi x env && memi y env -> Int(p, if findi x env < findi y env then 1 else 0)
  | If(_,_,(x, e1, e2)) when memi x env -> if findi x env <> 0 then g env e1 else g env e2
  | If(p1,p2,(x, e1, e2)) -> If(p1,p2,(x, g env e1, g env e2))
  | Let(p,ls,((x, t), e1, e2)) -> (* letのケース (caml2html: constfold_let) *)
      let e1' = g env e1 in
      let e2' = g (M.add x e1' env) e2 in
      Let(p,ls,((x, t), e1', e2'))
  | LetRec(p,ls,({ name = x; args = ys; body = e1 }, e2)) ->
      LetRec(p,ls,({ name = x; args = ys; body = g env e1 }, g env e2))
  | LetTuple(p,(xts, y, e)) when memt y env ->
      List.fold_left2
        (fun e' xt z -> Let(p,SLS.ls_default,(xt, Var(p,z), e'))) (* TODO: Varのpがタプル全体になってる *)
        (g env e)
        xts
        (findt y env)
  | LetTuple(p,(xts, y, e)) -> LetTuple(p,(xts, y, g env e))
  | e -> e

let f = g M.empty
