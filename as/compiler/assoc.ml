(* flatten let-bindings (just for prettier printing) *)

open KNormal

let rec f = function (* ネストしたletの簡約 (caml2html: assoc_f) *)
  | If(p1,p2,(x, e1, e2)) -> If(p1,p2,(x, f e1, f e2))
  | Let(p,ex,(xt, e1, e2)) -> (* letの場合 (caml2html: assoc_let) *)
      let rec insert = function
        | Let(p',ex',(yt, e3, e4)) -> Let(p',ex',(yt, e3, insert e4))
        | LetRec(p',ex',(fundefs, e)) -> LetRec(p',ex',(fundefs, insert e))
        | LetTuple(p',(yts, z, e)) -> LetTuple(p',(yts, z, insert e))
        | e -> Let(p,ex,(xt, e, f e2)) in
        insert (f e1)
  | LetRec(p,ex,({ name = xt; args = yts; body = e1 }, e2)) ->
      LetRec(p,ex,({ name = xt; args = yts; body = f e1 }, f e2))
  | LetTuple(p,(xts, y, e)) -> LetTuple(p,(xts, y, f e))
  | e -> e
