%{
(* parserが利用する変数、関数、型などの定義 *)
open Syntax
let addtyp x = (x, Type.gentyp ())
let addls st exp =
  let st' =
    match st with
      | "export" -> SLS.Exported
      | "noinline" -> SLS.NoInline
      | _ -> failwith "foo" in
  match exp with
    | Let(p, ls, pr) ->
       Let(p, SLS.add st' ls, pr)
    | LetRec(p, ls, pr) ->
       LetRec(p, SLS.add st' ls, pr)
    | _ -> failwith "foo"

(* week1-q3: 現在のposをオブジェクト化 *)
let pos () =
  let sp = Parsing.symbol_start_pos() in
  let ep = Parsing.symbol_end_pos() in
  let mk {Lexing.pos_fname = fname; Lexing.pos_lnum = line; Lexing.pos_bol = bol; Lexing.pos_cnum = cnum} = ({
          fname = fname;
    line  = line;
    column = cnum - bol
  } : Syntax.pospos) in
  ({ s = mk sp; 
     e = mk ep} : Syntax.pos_t)

%}

/* (* 字句を表すデータ型の定義 (caml2html: parser_token) *) */
%token <bool> BOOL
%token <int> INT
%token <float> FLOAT
%token NOT
%token MINUS
%token PLUS
%token AST
%token SLASH
%token MINUS_DOT
%token PLUS_DOT
%token AST_DOT
%token SLASH_DOT
%token EQUAL
%token LESS_GREATER
%token LESS_EQUAL
%token GREATER_EQUAL
%token LESS
%token GREATER
%token IF
%token THEN
%token ELSE
%token <Id.t> IDENT
%token LET
%token IN
%token REC
%token <Id.t> LETSTATUS
%token COMMA
%token ARRAY_CREATE
%token DOT
%token LESS_MINUS
%token SEMICOLON
%token LPAREN
%token RPAREN
%token LKPAREN
%token RKPAREN
%token EOF

/* (* 優先順位とassociativityの定義（低い方から高い方へ） (caml2html: parser_prior) *) */
%right prec_let
%right SEMICOLON
%right prec_if
%right LESS_MINUS
%left COMMA
%left EQUAL LESS_GREATER LESS GREATER LESS_EQUAL GREATER_EQUAL
%left PLUS MINUS PLUS_DOT MINUS_DOT
%left AST SLASH AST_DOT SLASH_DOT
%right prec_unary_minus
%left prec_app
%left DOT

/* (* 開始記号の定義 *) */
%type <Syntax.t> exp
%start exp

%%

simple_exp: /* (* 括弧をつけなくても関数の引数になれる式 (caml2html: parser_simple) *) */
| LPAREN exp RPAREN
    { set_pos $2 (pos()) }
| LPAREN RPAREN
    { Unit(pos()) }
| BOOL
    { Bool(pos(), $1) }
| INT
    { Int(pos(), $1) }
| FLOAT
    { Float(pos(), $1) }
| IDENT
    { Var(pos(), $1) }
| simple_exp DOT LPAREN exp RPAREN
    { Get(pos(),($1, $4)) }

exp: /* (* 一般の式 (caml2html: parser_exp) *) */
| simple_exp
    { $1 }
| NOT exp
    %prec prec_app
    { Not(pos(), $2) }
| MINUS exp
    %prec prec_unary_minus
    { match $2 with
    | Float(p,f) -> Float(p,-.f) (* -1.23などは型エラーではないので別扱い *)
    | e -> Neg(pos(), e) }
| exp PLUS exp /* (* 足し算を構文解析するルール (caml2html: parser_add) *) */
    { Add(pos(), ($1, $3)) }
| exp MINUS exp
    { Sub(pos(), ($1, $3)) }
| exp AST exp
    { Mul(pos(), ($1, $3)) }
| exp SLASH exp
    { Div(pos(), ($1, $3)) }
| exp EQUAL exp
    { Eq(pos(), ($1, $3)) }
| exp LESS_GREATER exp
    {
      let p = pos()
      in Not(p, Eq(p, ($1,$3)))
    }
| exp LESS exp
    {
      let p = pos()
      in Lt(p, ($1,$3))
    }
| exp GREATER exp
    {
      let p = pos()
      in Lt(p, ($3,$1))
    }
| exp LESS_EQUAL exp
    {
      let p = pos()
      in Not(p, Lt(p, ($3, $1)))
    }
| exp GREATER_EQUAL exp
    {
      let p = pos()
      in Not(p, Lt(p, ($1, $3)))
    }
| IF exp THEN exp ELSE exp
    %prec prec_if
    { If(pos(), ($2, $4, $6)) }
| MINUS_DOT exp
    %prec prec_unary_minus
    { FNeg(pos(), $2) }
| exp PLUS_DOT exp
    { FAdd(pos(), ($1, $3)) }
| exp MINUS_DOT exp
    { FSub(pos(), ($1, $3)) }
| exp AST_DOT exp
    { FMul(pos(), ($1, $3)) }
| exp SLASH_DOT exp
    { FDiv(pos(), ($1, $3)) }
| letexp
    { $1 }
| exp actual_args
    %prec prec_app
    { App(pos(), ($1, $2)) }
| elems
    { Tuple(pos(), $1) }
| LET LPAREN pat RPAREN EQUAL exp IN exp
    { LetTuple(pos(), ($3, $6, $8)) }
| simple_exp DOT LPAREN exp RPAREN LESS_MINUS exp
    { Put(pos(), ($1, $4, $7)) }
| exp SEMICOLON exp
    { Let(pos(), SLS.ls_default, ((Id.gentmp Type.Unit, Type.Unit), $1, $3)) }
| exp SEMICOLON
    { $1 }
| ARRAY_CREATE simple_exp simple_exp
    %prec prec_app
    { Array(pos(), ($2, $3)) }
| LKPAREN IDENT RKPAREN
    { Native(pos(), addtyp $2) }
| error
    { failwith
        (let {
                Lexing.pos_lnum = s_lnum;
                Lexing.pos_cnum = s_cnum;
                Lexing.pos_bol = s_bol
             } = Parsing.symbol_start_pos () in
        let {
                Lexing.pos_lnum = e_lnum;
                Lexing.pos_cnum = e_cnum;
                Lexing.pos_bol = e_bol
             } = Parsing.symbol_end_pos () in
        (Printf.sprintf "parse error near (%d, %d)-(%d, %d)"
           s_lnum (s_cnum - s_bol)
           e_lnum (e_cnum - e_bol)
           )) }

letexp:
| LET IDENT EQUAL exp IN exp
    %prec prec_let
    { Let(pos(), SLS.ls_default, (addtyp $2, $4, $6)) }
    /* (* boolに代入するアホがいるので *) */
| LET BOOL EQUAL exp IN exp
    %prec prec_let
    { Let(pos(), SLS.ls_default, (addtyp (Id.gentmp Type.Bool), $4, $6)) }
| LET REC fundef IN exp
    %prec prec_let
    { LetRec(pos(), SLS.ls_default, ($3, $5)) }
| LETSTATUS letexp
    %prec prec_let
    { addls $1 $2 }

fundef:
| IDENT formal_args EQUAL exp
    { { pos = pos(); name = addtyp $1; args = $2; body = $4 } }

formal_args:
| IDENT formal_args
    { addtyp $1 :: $2 }
| IDENT
    { [addtyp $1] }

actual_args:
| actual_args simple_exp
    %prec prec_app
    { $1 @ [$2] }
| simple_exp
    %prec prec_app
    { [$1] }

elems:
| elems COMMA exp
    { $1 @ [$3] }
| exp COMMA exp
    { [$1; $3] }

pat:
| pat COMMA IDENT
    { $1 @ [addtyp $3] }
| IDENT COMMA IDENT
    { [addtyp $1; addtyp $3] }
