{
(* lexerが利用する変数、関数、型などの定義 *)
open Parser
open Type

}

(* 正規表現の略記 *)
let space = [' ' '\t']
let digit = ['0'-'9']
let lower = ['a'-'z']
let upper = ['A'-'Z']

rule token = parse
| space+
    { token lexbuf }
| ("\r\n" | "\r" | "\n")
    { Lexing.new_line lexbuf; (* 改行をハンドル *)
      token lexbuf }
| "(*"
    { comment lexbuf; (* ネストしたコメントのためのトリック *)
      token lexbuf }
| '('
    { LPAREN }
| ')'
    { RPAREN }
| "true"
    { BOOL(true) }
| "false"
    { BOOL(false) }
| "not"
    { NOT }
| digit+ (* 整数を字句解析するルール (caml2html: lexer_int) *)
    { INT(int_of_string (Lexing.lexeme lexbuf)) }
| digit+ ('.' digit*)? (['e' 'E'] ['+' '-']? digit+)?
    { FLOAT(float_of_string (Lexing.lexeme lexbuf)) }
| '-' (* -.より後回しにしなくても良い? 最長一致? *)
    { MINUS }
| '+' (* +.より後回しにしなくても良い? 最長一致? *)
    { PLUS }
| '*'
    { AST }
| '/'
    { SLASH }
| "-."
    { MINUS_DOT }
| "+."
    { PLUS_DOT }
| "*."
    { AST_DOT }
| "/."
    { SLASH_DOT }
| '='
    { EQUAL }
| "<>"
    { LESS_GREATER }
| "<="
    { LESS_EQUAL }
| ">="
    { GREATER_EQUAL }
| '<'
    { LESS }
| '>'
    { GREATER }
| "if"
    { IF }
| "then"
    { THEN }
| "else"
    { ELSE }
| "let"
    { LET }
| "in"
    { IN }
| "rec"
    { REC }
| "export"
    { LETSTATUS("export") }
| "noinline"
    { LETSTATUS("noinline") }
| ','
    { COMMA }
| '_'
    { IDENT(Id.gentmp Type.Unit) }
| "Array.create" (* [XX] ad hoc *)
    { ARRAY_CREATE }
| "create_array" (* XXX XXX ad hoc *)
    { ARRAY_CREATE }
| '.'
    { DOT }
| "<-"
    { LESS_MINUS }
| ';'
    { SEMICOLON }
| '['
    { LKPAREN }
| ']'
    { RKPAREN }
| eof
    { EOF }
| lower (digit|lower|upper|'_')* (* 他の「予約語」より後でないといけない *)
    { IDENT(Lexing.lexeme lexbuf) }
| _
    { failwith
        (let {
                Lexing.pos_lnum = s_lnum;
                Lexing.pos_cnum = s_cnum;
                Lexing.pos_bol = s_bol
             } = Parsing.symbol_start_pos () in
        let {
                Lexing.pos_lnum = e_lnum;
                Lexing.pos_cnum = e_cnum;
                Lexing.pos_bol = e_bol
             } = Parsing.symbol_end_pos () in
	(Printf.sprintf "unknown token %s near (%d, %d)-(%d, %d)"
	   (Lexing.lexeme lexbuf)
           s_lnum (s_cnum - s_bol)
           e_lnum (e_cnum - e_bol)
	   )) }
and comment = parse
| ("\r\n" | "\r" | "\n")
    { Lexing.new_line lexbuf; (* ここでも改行をハンドル *)
      comment lexbuf }
| "*)"
    { () }
| "(*"
    { comment lexbuf;
      comment lexbuf }
| eof
    { Format.eprintf "warning: unterminated comment@." }
| _
    { comment lexbuf }
