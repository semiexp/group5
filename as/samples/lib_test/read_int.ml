(* this test requires miniMLRuntime.minca.ml *)

let rec main _ =
  let c = read_int() in
    print_int c;
    print_newline();
    if c = 0 then ()
    else main () in
  main ()

