let rec repeat c i =
  if i>0 then
    (print_char c;
     repeat c (i-1))
  else () in
let rec f v =
  if v < 10.0001 then
    let floorv = floor v in
    let n = int_of_float (floorv *. 10.0) in
      (repeat 42 n;
       repeat 32 (100-n);
        print_int (int_of_float (v *. 10000000.0));
        print_char 32;
        print_int (int_of_float (floorv *. 100.0));
       print_newline();
       f (v +. 0.000001))
      else
        () in
  f (9.9999)




