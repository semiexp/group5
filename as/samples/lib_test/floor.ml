let rec repeat c i =
  if i>0 then
    (print_char c;
     repeat c (i-1))
  else () in
let rec f v =
  if v < 1.5 then
    let floorv = floor v in
    let n = int_of_float (floorv *. 20.0) + 50 in
      (repeat 42 n;
       repeat 32 (75-n);
        print_int (int_of_float (v *. 100.0));
        print_char 32;
        print_int (int_of_float (floorv *. 100.0));
       print_newline();
       f (v +. 0.01))
      else
        () in
  f (-1.5)



