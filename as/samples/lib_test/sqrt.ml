let rec repeat c i =
  if i>0 then
    (print_char c;
     repeat c (i-1))
  else () in
let rec f v =
  if v < 100.0 then
    let sqrtv = sqrt v in
    let n = int_of_float (sqrtv *. 10.0) in
      (repeat 42 n;
       repeat 32 (100-n);
        print_int (int_of_float (v *. 100.0));
        print_char 32;
        print_int (int_of_float (sqrtv *. 100.0));
       print_newline();
       f (v +. 1.0))
      else
        () in
  f (0.0)




