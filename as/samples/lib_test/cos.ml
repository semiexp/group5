let rec repeat c i =
  if i>0 then
    (print_char c;
     repeat c (i-1))
  else () in
let rec f v =
  if v < 15.7 then
    let cosv = cos v in
    let n = int_of_float (cosv *. 30.0) + 30 in
      (repeat 42 n;
       repeat 32 (60-n);
        print_int (int_of_float (v *. 100.0));
        print_char 32;
        print_int (int_of_float (cosv *. 100.0));
       print_newline();
       f (v +. 0.1))
      else
        () in
  f (-12.7)



