let rec f v =
  if v < 20.0 then
    (print_int (int_of_float v);
     print_newline();
     f (v +. 0.5))
  else
    () in
  (f (-20.0);
   print_newline());
  let rec g v =
    if v < 1000000000.0 then
      (print_int (int_of_float v);
       print_newline();
       g (v *. 10.0))
    else
      () in
    g 1.1234567




