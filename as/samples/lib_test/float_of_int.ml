let rec f v =
  if v < 20 then
    (print_int v;
     print_char 32;
     print_int (int_of_float ((float_of_int v) *. 0.5));
     print_newline();
     f (v+1))
  else
    () in
  f (-20)
