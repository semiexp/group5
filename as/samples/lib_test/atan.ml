let rec repeat c i =
  if i>0 then
    (print_char c;
     repeat c (i-1))
  else () in
let rec f v =
  if v < 12.7 then
    let atanv = atan v in
    let n = int_of_float (atanv *. 20.0) + 40 in
      (repeat 42 n;
       repeat 32 (75-n);
        print_int (int_of_float (v *. 100.0));
        print_char 32;
        print_int (int_of_float (atanv *. 100.0));
       print_newline();
       f (v +. 0.1))
      else
        () in
  f (-12.7)




