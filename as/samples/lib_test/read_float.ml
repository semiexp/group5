(* this test requires miniMLRuntime.minca.ml *)

let rec main _ =
  let c = read_float() in
    print_int (int_of_float (c*.10000.0));
    print_newline();
    if c = 0.0 then ()
    else main () in
  main ()


