let rec a n =
  if n > 8 then ()
  else
    (out32 (float_of_int n);
     a (n+1)) in
  a (-5)
