let rec id x = x in
let a = id 8 in
let b = id (-14) in
let c = a * 4 in
let d = 4 * b in
let e = (-5) * a in
let f = b * (-3) in
let g = b * 0 in
  print_int c; print_newline();
  print_int d; print_newline();
  print_int e; print_newline();
  print_int f; print_newline();
  print_int g; print_newline();
