let rec fib n =
  let rec fib2 n a b =
    if n<=1 then
      a
    else
      fib2 (n-1) (a+b) a in
    fib2 n 1 0 in
  print_int (fib 10000); print_newline()
