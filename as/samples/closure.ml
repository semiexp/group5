let rec f x =
  let rec addx y = x + y in addx in
let g = f 5 in
  print_int(g 10); print_newline()

