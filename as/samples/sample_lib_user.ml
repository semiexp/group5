let x = global_array.(0) + global_array.(2) in
  global_array.(4) <- 20;
  print_int 100;
  print_newline();
  print_int_twice x;
  print_newline();
  global_array.(4) <- global_array.(4) + global_array.(0);
  print_int global_array.(4);
  print_newline();
