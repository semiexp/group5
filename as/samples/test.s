# 1 to 100 sum
.text
.globl main
.main main
main:
	movi %r1,0
	movi %r2,1
	movi %r3,100
label1:
	add %r1,%r1,%r2
	addi %r2,%r2,1
	cmpgt %r10, %r2, %r3
	jmpi %r10,end
	jmpui label1
end:
	out r1
	rshifti r3,r1,8
	out r3
	halt
	.end
