/* なにか */
export const REGISTER_NUMBER = 64; /* 2^nでないとこわれる！！！！！ */

interface BisonPos {
    first_column: number;
    first_line: number;
    last_column: number;
    last_line: number;
}

// globalなやつはあまり好きではないけど仕方ない
let current_filename:string = "";
export function setCurrentFilename(filename:string):void{
    current_filename = filename;
}

//position
export class Pos implements BisonPos{
    public filename:string;
    public first_column:number;
    public first_line:number;
    public last_column:number;
    public last_line:number;
    constructor(obj:BisonPos){
        this.filename = current_filename;
        this.first_column = obj.first_column;
        this.first_line = obj.first_line;
        this.last_column = obj.last_column;
        this.last_line = obj.last_line;
    }

    static merge(pos1:BisonPos,pos2:BisonPos):Pos{
        let first_column:number, first_line:number, last_column:number, last_line:number;
        if((pos1.first_line < pos2.first_line) || (pos1.first_line===pos2.first_line && pos1.first_column <= pos2.first_column)){
            first_line = pos1.first_line;
            first_column = pos1.first_column;
        }else{
            first_line = pos2.first_line;
            first_column = pos2.first_column;
        }
        if((pos1.last_line < pos2.last_line) || (pos1.last_line===pos2.last_line && pos1.last_column <= pos2.last_column)){
            last_line = pos2.last_line;
            last_column = pos2.last_column;
        }else{
            last_line = pos1.last_line;
            last_column = pos1.last_column;
        }
        return new Pos({
            first_column,
            first_line,
            last_column,
            last_line
        });
    }
    static dummy():Pos{
        return new Pos({
            first_column: -1,
            first_line: -1,
            last_column: -1,
            last_line: -1
        });
    }
}
//Error with positions
export class AsmError extends Error{
    constructor(message:string,public pos:Pos){
        super(message);
        this.message = message;
    }
}
//ast objects
export abstract class Expression{
    //アセンブリ文字列を生成するやつ
    abstract toAssembly():string;
    abstract clone():Expression;
}
//文字列（レジスタ名？）か数値
export abstract class Ident{
    public pos:Pos;
    constructor(pos:Pos){
        this.pos=pos;
    }
    //文字列にする
    abstract toString():string;
    //identifierの解釈
    abstract asRegister():number;
    //即値の値を生成
    abstract rawImmediate(state:EmitState,bits:number,signed:boolean):number;
    //即値を返す
    public asImmediate(state:EmitState,bits:number,signed:boolean = false):number{
        //即値が範囲内か調べる
        let min:number, max:number, v=this.rawImmediate(state, bits, signed);
        if(signed===true){
            //符号有だ
            min = -Math.pow(2,bits-1);
            max = Math.pow(2,bits-1)-1;
        }else{
            //符号無し
            min = 0;
            max = Math.pow(2,bits)-1;
        }
        if(v < min || max < v){
            //範囲をオーバーしてるぞ？？？？
            throw new AsmError(v+" is not in the range of "+(signed ? "signed" : "unsigned")+" "+ bits+" bits integer.", this.pos);
        }
        //bitsのビット数にあれする
        if(signed===true && v<0){
            return Math.pow(2,bits)-Math.abs(v);
        }else{
            return v;
        }
    }
}
//Identifier
export class Identifier extends Ident{
    public value:string;
    constructor(value:string,pos:Pos){
        super(pos);
        this.value=value.toLowerCase();
    }
    public toString():string{
        return this.value;
    }
    public asRegister():number{
        let r = this.value.match(/^[\w\%]*?(\d+)$/);
        if(r==null){
            throw new AsmError("Cannot parse "+ this.value + " as a register.", this.pos);
        }
        let num = Number(r[1]);
        if(num<0 || REGISTER_NUMBER<=num){
            throw new AsmError(this.value+" is not a valid register number.", this.pos);
        }
        return num;
    }
    public rawImmediate(state:EmitState,bits:number,signed:boolean):number{
        //ラベル名だったらOK
        let l=state.table[this.value];
        if(l==null){
            throw new AsmError("Cannot parse "+this.value+" as an immediate", this.pos);
        }else{
            return l.position;
        }
    }
}
export class Num extends Ident{
    public value:number;
    constructor(private str:string,pos:Pos){
        super(pos);
        this.value=this.getNumber();
    }
    private getNumber():number{
        let str=this.str;
        let r = str.match(/^([\+\-]?)0x(\d+)$/i);
        if(r){
            //前置0xリテラル
            return parseInt(r[1]+r[2],16);
        }
        if(r = str.match(/^([\+\-]?)0o(\d+)$/i)){
            //0o-prefixed literal
            return parseInt(r[1]+r[2],8);
        }
        if(r = str.match(/^([\+\-]?)0b([01]+)$/i)){
            return parseInt(r[1]+r[2],2);
        }
        if(r = str.match(/^([\+\-]?\d+)x$/i)){
            return parseInt(r[1],16);
        }
        if(r = str.match(/^([\+\-]?\d+)o$/i)){
            return parseInt(r[1],8);
        }
        if(r = str.match(/^([\+\-]?\d+)b$/i)){
            return parseInt(r[1],2);
        }
        let v = Number(str);
        if(isFinite(v)){
            return v;
        }else{
            return null;
        }
    }
    public toString():string{
        return this.str;
    }
    public asRegister():number{
        let num = this.value;
        if(num<0 || REGISTER_NUMBER<=num){
            throw new AsmError(this.value+" is not a valid register number", this.pos);
        }
        return num;
    }
    public rawImmediate(state:EmitState,bits:number,signed:boolean):number{
        return this.value;
    }
}
export abstract class MemAddr extends Ident{
    //アドレッシング
    public asRegister():number{
        throw new AsmError("Memory indicator cannot be read as a register number.",this.pos);
    }
    public rawImmediate(state:EmitState,bits:number,signed:boolean):number{
        throw new AsmError("Memory indicator cannot be read as an immediate value.",this.pos);
    }
}
export class MemAddr1 extends MemAddr{
    constructor(public reg:Ident,public displacement:Num,pos:Pos){
        super(pos);
    }
    toString():string{
        if(this.displacement.value===0){
            return "("+this.reg.toString()+")";
        }else{
            return this.displacement.toString()+"("+this.reg.toString()+")";
        }
    }
}
export class MemAddr2 extends MemAddr{
    constructor(public reg1:Ident,public reg2:Ident,public displacement:Num,pos:Pos){
        super(pos);
    }
    toString():string{
        if(this.displacement.value===0){
            return "("+this.reg1.toString()+" + "+this.reg2.toString()+")";
        }else{
            return this.displacement.toString()+"("+this.reg1.toString()+" + "+this.reg2.toString()+")";
        }
    }
}

//condition registerの否定条件ででてくるやつ
export class Neg extends Ident{
    constructor(public cr:Identifier,pos:Pos){
        super(pos);
    }
    public toString(){
        return "!"+this.cr.toString();
    }
    public asRegister():number{
        throw new AsmError("Negative conditional indicator cannot be read as a register number.",this.pos);
    }
    public rawImmediate(state:EmitState,bits:number,signed:boolean):number{
        throw new AsmError("Negative conditional indicator cannot be read as an immediate value.",this.pos);
    }
}

//マクロ
export class Macro extends Ident{
    constructor(public name:Identifier, public args:Array<Ident>, pos:Pos){
        super(pos);
    }
    public toString(){
        return "&"+this.name.toString()+"("+this.args.map((arg)=>arg.toString()).join(", ")+")";
    }
    public asRegister():number{
        throw new AsmError("The "+this.name.toString()+" macro cannot generate register.", this.pos);
    }
    public rawImmediate(state:EmitState,bits:number, signed:boolean){
        let name=this.name.toString();
        if(name==="pc"){
            //PC macro（現在のPCの位置のあれをあれする
            if(this.args.length!==1){
                throw new AsmError("The "+name+" macro must have 1 argument.", this.pos);
            }
            let offset:number = this.args[0].rawImmediate(state, bits, true);
            //返り値
            return state.index + offset;
        }else if(name==="add"){
            //ADD macro（即値の足し算を計算する）
            return this.args.reduce((prev, curr)=>{
                return prev + curr.rawImmediate(state, bits, true);
            }, 0);
        }else if(name==="var"){
            //VAR macro
            if(this.args.length!==1){
                throw new AsmError("The "+name+" macro must have 1 argument.", this.pos);
            }
            const vname = this.args[0].toString();
            if(!(vname in state.variables)){
                throw new AsmError("ReferenceError: "+vname, this.pos);
            }
            return state.variables[vname].rawImmediate(state, bits, signed);
        }else if(name==="mul"){
            //MUL macro（かけざん）
            return this.args.reduce((prev, curr)=>{
                return prev * curr.rawImmediate(state, bits, true);
            }, 1);
        }
    }
}


export class Instruction extends Expression{
    public programIndex:number; //何番地か
    constructor(public name:Ident, public args:Array<Ident>){
        super();
        this.programIndex = -1;
    }

    //引数の数をたしかめる
    public ensureArgs(length:number){
        if(length !== this.args.length){
            throw new AsmError(this.name.toString()+" must have "+length+" operand(s)",this.name.pos);
        }
    }
    public ensureArgsm(length:Array<number>){
        if(length.every(n => this.args.length!==n)){
            throw new AsmError(this.name.toString()+" must have "+length.join(" or ")+" operand(s)",this.name.pos);
        }
    }

    public toAssembly():string{
        const pr = this.programIndex >= 0 ? `\t\t# ${this.programIndex}` : "";
        return "\t"+this.name.toString()+"\t"+this.args.map(i=>i.toString()).join(", ")+pr+"\n";
    }
    public clone():Instruction{
        const result = new Instruction(this.name, this.args);
        result.programIndex = this.programIndex;
        return result;
    }
}

export class Label extends Expression{
    constructor(public name:Identifier){
        super();
    }
    public toAssembly():string{
        return this.name.toString()+":\n";
    }
    public clone():Label{
        return new Label(this.name);
    }
}

export class Processing extends Expression{
    constructor(public name:Identifier, public args:Array<Ident>){
        super();
    }
    public toAssembly():string{
        return "."+this.name.toString()+"\t"+this.args.map(i=>i.toString()).join(", ")+"\n";
    }
    public clone():Processing{
        return new Processing(this.name, this.args);
    }
}

export class Nop extends Expression{
    constructor(){
        super();
    }
    public toAssembly():string{
        return "";
    }
    public clone():Nop{
        return new Nop();
    }
}

export type Expressions = Array<Expression>;

// ラベルのテーブル
export interface LabelTable {
    [label:string]:{
        //ラベルがグローバルかどうか
        globl: boolean;
        //ラベルの位置（AST上）
        index: number;
        //ラベルの位置（プログラム上）
        position: number;  //20bit
        //アセンブリ上の位置
        aspos: Pos;
    };
}

//変数テーブル
export interface VariableTable {
    [variable:string]: Ident;
}
//emit中の状態
export interface EmitState {
    //ラベル情報
    table: LabelTable;
    //変数情報
    variables: VariableTable;
    //位置
    index: number;
}

