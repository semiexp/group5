import {Expressions} from './ast';
export var parser:{
    parse(input:string): Expressions;
};
