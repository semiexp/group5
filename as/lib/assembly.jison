%lex

%%
(?:\r\n|\r|\n)	return 'NEWLINE'
' '|\t		/* skip whitespace */
'#'.*			/* skip comment */
'%'?[a-zA-Z_][\w\.]*		return 'IDENT'
(?:[+-]?(?:0[xX][0-9a-fA-F]+|0[oO][0-7]+|0[bB][01]+|[0-9a-fA-F]+[xX]|[0-7]+[oO]|[01][bB]|[0-9]+))		return 'NUMBER'
','		return ','
'('		return '('
')'		return ')'
'&'		return '&'
'.'		return '.'
':'		return ':'
'+'		return '+'
'!'		return '!'
<<EOF>>		return 'EOF'
.		return 'INVALID'

/lex

%start file

%%

file
	: expressions EOF { return $1 }
	;

expressions
	: expression { $$ = [$1] }
	| expressions expression { $$ = $1.concat($2) }
	;

expression
	: instruction NEWLINE { $$ = $1 }
	| label NEWLINE { $$ = $1 }
	| processing NEWLINE { $$ = $1 }
	| NEWLINE {$$ = new Nop() }
	;

instruction
	: identifier { $$ = new Instruction($1,[]) }
	| identifier args { $$ = new Instruction($1,$2) }
	;

args
	: identifier { $$ = [$1] }
	| args ',' identifier { $$ = $1.concat($3) }
	;

label
	: IDENT ':' { $$ = new Label(new Identifier($1,new Pos(@1))) }
	;

processing
	: '.' IDENT { $$ = new Processing($2,[]) }
	| '.' IDENT args { $$ = new Processing($2, $3) }
	;

identifier
	/* memory addressing */
	: NUMBER '(' identifier ')' { $$ = new MemAddr1($3, new Num($1,new Pos(@1)), Pos.merge(@1,@4)) }
	| NUMBER '(' identifier '+' identifier ')' { $$ = new MemAddr2($3, $5, new Num($1), Pos.merge(@1,@6)) }
	| '(' identifier ')' { $$ = new MemAddr1($2, new Num("0",null), Pos.merge(@1,@3)) }
	| '(' identifier '+' identifier ')' { $$ = new MemAddr2($2, $4, new Num("0",null), Pos.merge(@1,@5)) }
	/* condition */
	| '!' IDENT { $$ = new Neg(new Identifier($2,new Pos(@2)), Pos.merge(@1,@2)) }
	/* macro */
	| '&' IDENT '(' args ')' { $$ = new Macro(new Identifier($2, new Pos(@2)), $4, Pos.merge(@1,@5)) }
	| IDENT { $$ = new Identifier($1,new Pos(@1)) }
	| NUMBER { $$ = new Num($1,new Pos(@1)) }
	;
	
%%
/* むりやり展開 */
var ast=require('./ast');
for(var key in ast){
	this[key]=ast[key];
}

