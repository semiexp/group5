///<reference path="./node.d.ts" />
import minimist = require('minimist');
import fs = require('fs');
import path = require('path');
import {Expressions, Instruction, Label, Processing, Identifier, Pos, AsmError, LabelTable, VariableTable, setCurrentFilename} from './ast';
import * as emit from './emit';
import {parser} from './parser';

var p=require('../package.json');

//読み込んだデータ
export interface FileData{
    filename:string;
    data:string;
}
//入力を読む
export function read(files:Array<string>,callback:(datas:Array<FileData>)=>void):void{
    let length=files.length;
    if(length===0){
        //stdinから読まないといけないぞ
        let result="";
        let stdin=process.stdin;
        stdin.resume();
        stdin.setEncoding('utf8');
        stdin.on('data',(chunk)=>{
            result+=chunk;
        });
        stdin.on('end',()=>{
            callback([{
                filename: "(stdin)",
                data: result
            }]);
        });
        return;
    }else{
        ((files)=>{
            let result=[]
            let load = (i)=>{
                fs.readFile(files[i],{
                    encoding: 'utf8'
                },(err,data)=>{
                    if(err){
                        throw err;
                    }
                    result.push({
                        filename: files[i],
                        data
                    });
                    if(length <= i+1){
                        callback(result);
                    }else{
                        load(i+1);
                    }
                });
            };
            load(0);
        })(files);
    }
}

//ファイルの中身をみてオブジェクトファイルを吐く
export function make(emitter:emit.Emitter, datas:Array<FileData>, preprocessf:boolean, optimizer:emit.Optimizer):string|Buffer{
    try{
        //順番にパースしてつなげる
        let exps:Expressions = [], table:LabelTable, variables: VariableTable, size:number;
        for(let i=0;i<datas.length;i++){
            setCurrentFilename(datas[i].filename);
            let es = parser.parse(datas[i].data);
            exps=exps.concat(es);
        }
        //プリプロセス
        ({exps, table, variables, size} = preprocess(exps, optimizer));

        if(preprocessf){
            //プリプロセス結果を出力すればいい
            return exps.map(e=>e.toAssembly()).join("");
        }

        //バッファを作る
        let buf = new Buffer(4+size*4);
        //残りのバッファサイズを書き込む
        buf.writeUInt32LE(size,0);
        //Bufferにwriteする
        for(let i=0,index=4,pc=0,l=exps.length;i<l;i++){
            let e = exps[i];
            let num = emitter(e, {
                table,
                variables,
                index: pc
            });
            if(num!=null){
                buf.writeUInt32LE(num>>>0,index);
                pc++;
                index+=4;
            }
        }
        return buf;
    }catch(e){
        if(e instanceof AsmError){
            showError(datas,e);
            process.exit(1);
        }else{
            throw e;
        }
    }
};

//プリプロセス
export function preprocess(exps: Expressions, optimizer: emit.Optimizer):{
    exps: Expressions;
    table: LabelTable;
    variables: VariableTable;
    size: number;
}{
    //dataとtextに分ける
    let {data, text, datasize, textsize} = partition(exps);

    //textを処理
    let {table, names, main, size, freeexp} = makeLabelTable(text);

    //グローバルなラベルでセクション分け
    let secs = labelSections(text, table);

    //mainを先頭に持ってくる
    secs = sortText(secs, main);

    //再構成
    text = secs.reduce((acc, {name, exps})=>{
        return acc.concat(exps);
    },[]);
    //再捜査
    ({table, names, main, size, freeexp} = makeLabelTable(text));

    //dataラベルの情報を追加
    addDataLabel(data, size, table);

    //変数を走査
    const variables = makeVariableTable(text);

    if(optimizer != null){
        //最適化を行う
        text = optimize(text, optimizer);
        ({table, names, main, size, freeexp} = makeLabelTable(text));
        addDataLabel(data, size, table);
    }

    //命令に番地の情報を付加
    addIndex(text);

    return {
        exps: data.concat(text),
        table,
        variables,
        size
    };
}

//走査してdataとtextに分ける
export function partition(exps: Expressions){
    let data:Expressions = [
        new Processing(new Identifier("data", Pos.dummy()), [])
    ], text:Expressions = [
        new Processing(new Identifier("text", Pos.dummy()), [])
    ];
    let datasize=0, textsize=0;
    let textmode = true;   //false -> data, true -> text
    for(let i=0, l=exps.length; i<l;i++){
        const e = exps[i];
        if(e instanceof Processing){
            let pname = e.name.toString();
            //処理命令
            if(pname==="data"){
                //モードきりかえ
                textmode = false;
            }else if(pname==="text"){
                textmode = true;
            }else if(pname==="space"){
                if(e.args.length!==1){
                    throw new AsmError("The space PI requires exact 1 argument", e.name.pos);
                }
                if(textmode===true){
                    // TODO
                    throw new AsmError("The space PI is not supported in text section", e.name.pos);
                }
                //TODO
                let size = Number(e.args[0].toString());
                if(!isFinite(size)){
                    throw new AsmError("Number is expected", e.args[0].pos);
                }
                datasize += size;
                data.push(e);
            }else{
                //ほかの処理命令はそのまま（でもtextに振り分けたりするかも）
                if(pname==="define" || textmode){
                    text.push(e);
                }else{
                    data.push(e);
                }
            }
        }else if(e instanceof Instruction){
            if(textmode===false){
                throw new AsmError("Any instruction is forbidden in data section", e.name.pos);
            }
            text.push(e);
            textsize++;
        }else if(e instanceof Label){
            if(textmode){
                text.push(e);
            }else{
                data.push(e);
            }
        }
    }
    return {
        data, text,
        datasize, textsize
    };
}
//走査してラベルの位置を得る（mainのラベルの名前もあれば返す）
export function makeLabelTable(exps:Expressions):{
    //ラベルの位置のあれ
    table: LabelTable;
    //ラベル一覧（順番に）
    names: Array<string>;
    //メイン指定のあるラベル
    main: Array<string>;
    //プログラムサイズ
    size: number;
    //最初のラベルより前にプログラムがあったかどうか
    freeexp: boolean;
}{

    const result:LabelTable = {}, names:Array<string>=[];
    let position=0;    //プログラム中の位置
    let main=[];  //mainラベル
    for(let i=0,l=exps.length;i<l;i++){
        const e = exps[i];
        if(e instanceof Label){
            //ラベルを登録する
            const lname=e.name.toString();
            if(result[lname]==null){
                //新発見
                names.push(lname);
                result[lname] = {
                    globl: false,
                    index: i,
                    position,
                    aspos: e.name.pos
                };
            }else{
                if(result[lname].position != null){
                    //すでにあるラベルをまた見つけてしまった
                    throw new AsmError("Duplicate label.", e.name.pos);
                }
                //すでに登録済
                result[lname].index = i;
                result[lname].position = position;
                result[lname].aspos = e.name.pos;
            }
        }else if(e instanceof Processing){
            let pname=e.name.toString();
            if(pname==="globl"){
                //ラベルのグローバル性を示すあれだ
                if(e.args.length!==1){
                    throw new AsmError("The globl PI requires exact 1 argument", e.name.pos);
                }
                let lname=e.args[0].toString();
                if(result[lname]==null){
                    //先に宣言
                    names.push(lname);
                    result[lname] = {
                        globl: true,
                        index: null,
                        position: null,
                        aspos: e.name.pos
                    };
                }else{
                    result[lname].globl = true;
                }
            }else if(pname==="main"){
                //main宣言をみつけた
                if(e.args.length!==1){
                    throw new AsmError("The main PI requires exact 1 argument", e.name.pos);
                }
                let lb = e.args[0].toString();
                if(main.indexOf(lb) <0){
                    main.push(lb);
                }
            }else if(pname==="space"){
                //プログラムサイズを増加させる
                let size = Number(e.args[0].toString());
                position += size;
            }
        }else if(e instanceof Instruction){
            position++;
        }
    }
    //妥当性検査
    for(let key in result){
        if(result[key].position==null){
            //globl宣言だけあって見つからない
            throw new AsmError("The label "+ key +" is not found.", result[key].aspos);
        }
    }
    //mainが存在するかも
    if(main.some((lb)=>{return result[lb]==null || result[lb].position==null})){
        throw new Error("The main label does not exist");
    }
    //freeexpが存在するか？（0番の位置にgloblなラベルが存在すればない）
    let freeexp=true;
    for(let i=0; i<names.length;i++){
        let l=result[names[i]];
        if(l.position>0){
            break;
        }else if(l.globl===true){
            freeexp=false;
            break;
        }
    }
    return {
        table: result,
        names,
        main,
        size: position,
        freeexp
    };
}

//グローバルなラベルごとに分ける
export function labelSections(exps:Expressions, table:LabelTable):Array<{
    name: string;
    exps: Expressions;
}>{
    let result = [];
    let current = {
        name: null,
        exps: []
    };
    for(let i=0, l=exps.length;i<l;i++){
        const e = exps[i];
        if(e instanceof Label){
            const lname = e.name.toString();
            const entry = table[lname];
            if(entry && entry.globl){
                //グローバルなラベルだ
                result.push(current);
                current = {
                    name: lname,
                    exps: []
                };
            }
            current.exps.push(e);
        }else if(e instanceof Processing){
            current.exps.push(e);
        }else{
            current.exps.push(e);
        }
    }
    result.push(current);
    return result;
}

interface Sec{
    name:string;
    exps:Expressions;
}

//mainを先頭に持ってくる
function sortText(secs:Array<Sec>, main:Array<string>):Array<{
    name:string;
    exps:Expressions;
}>{
    //非破壊的
    secs = secs.concat([]);
    //先頭
    let ts:Array<Sec> = [];
    for(let i=0; i<secs.length;i++){
        if(!secs[i].name || main.indexOf(secs[i].name)>=0){
            ts.push(secs[i]);
            secs.splice(i,1);
            i--;
        }
    }
    //mainの最後から走査して.endを発見する
    let flg = false;
    for(let i=ts.length-1; i>=0; i--){
        let exps = ts[i].exps;
        for(let j=exps.length-1; j>=0; j--){
            let e = exps[j];
            if(e instanceof Processing && e.name.toString()==="end"){
                //end PIだ
                if(flg===false){
                    //最初にみつけた.endなのでhaltに置き換える
                    exps[j] = new Instruction(
                        new Identifier("halt", e.name.pos),
                        []
                    );
                    flg=true;
                }else{
                    //この.endはいらない
                    exps.splice(j,1);
                }
            }
        }
    }
    return ts.concat(secs);
}

//dataラベルの情報を追加する
function addDataLabel(exps:Expressions, textsize:number, table:LabelTable):void{
    //data領域のスタート地点を決める
    //max text size: 32K = 100000 0000000000 = 0x8000
    const start = Math.ceil(textsize / 0x8000) * 0x8000;
    let {table:table2, names, size} = makeLabelTable(exps);

    names.forEach((lb)=>{
        const t2e = table2[lb];
        table[lb] = {
            globl: t2e.globl,
            index: t2e.index,
            position: start + t2e.position,
            aspos: t2e.aspos
        };
    });
}

//最適化
function optimize(text:Expressions, optimizer:emit.Optimizer):Expressions{
    let {table} = makeLabelTable(text);
    let variables = makeVariableTable(text);
    //個別のあれ
    const result:Expressions = [];
    let index=0;
    for(let i=0, l=text.length; i<l; i++){
        const e = text[i];
        const e2 = optimizer.single(e, {
            table,
            variables,
            index
        });
        if(e2 != null){
            result.push(e2);
            if(e2 instanceof Instruction){
                index++;
            }
        }
    }
    text = result;
    
    //全体のあれ
    let flag:boolean = true;
    while(flag){
        let flag2:boolean;
        flag = false;
        for(const func of optimizer.text){
            ({table} = makeLabelTable(text));
            variables = makeVariableTable(text);
            const old_text = text;
            ([flag2, text] = func(text, table, variables));
            flag = flag || flag2;
            /*
            if(flag2){
                console.log("a %s", (func as any).name);
                for(let e of old_text){
                    process.stdout.write(e.toAssembly());
                }
                console.log("----------");
                for(let e of text){
                    process.stdout.write(e.toAssembly());
                }
            }
            */
        }
    }
    return text;
}

//番地の情報を付加
function addIndex(text:Expressions):void{
    let index = 0;
    for(let i=0, l=text.length; i<l; i++){
        let e=text[i];
        if(e instanceof Instruction){
            e.programIndex = index++;
        }
    }
}

//変数テーブルをつくる
function makeVariableTable(text:Expressions):VariableTable{
    const result:VariableTable = {};
    for(let i=0, l=text.length; i<l; i++){
        const e=text[i];
        if(e instanceof Processing && e.name.toString()==="define"){
            //"define" preprocessing instruction
            if(e.args.length!==2){
                throw new AsmError("define PI must have 2 arguments", e.name.pos);
            }
            const vname = e.args[0].toString();
            if(vname in result){
                throw new AsmError("Duplicate variable.", e.name.pos);
            }
            result[vname] = e.args[1];
        }
    }
    return result;
}

function showError(datas:Array<FileData>, e:AsmError):void{
    const message:string = e.message, pos:Pos = e.pos;
    if(pos==null){
        //???
        throw new Error(message);
    }
    //該当ソースファイルを見つける
    let source = datas.filter((o)=>o.filename===pos.filename)[0].data;
    //ソースを行にわける
    const line_num = pos.first_line, column = pos.first_column;
    const lines = source.split(/\r\n|\r|\n/g, line_num+1);
    const line = lines[line_num-1].replace("\t"," ");
    const r = line.match(/^\u0020*/);
    const spw = r ? r[0].length : 0;    //最初のスペース

    //エラーメッセージを表示
    console.error("ERROR(%s, line %d, column %d): %s", pos.filename, line_num, column, message);
    //該当行を表示
    console.error("    "+line.slice(spw));
    if(pos.first_line===pos.last_line)
    //カーソルを表示
    console.error(repeat(" ",column - spw + 4)+repeat("^",pos.last_column - column));
    console.error("");

    function repeat(str:string,num:number):string{
        return (<any>str).repeat(num);
    }
}

export function main():void{
    //main routine
    let args = process.argv.slice(2);
    const argv = minimist(args,{
        string: ['out','isa','O'],
        boolean: ['help','version'],
        alias: {
            h: 'help',
            o: 'out',
            v: 'version'
        }
    });
    if(argv.help===true){
        //helpを要求された
        help();
        return;
    }
    if(argv.version===true){
        version();
        return;
    }
    //実行ファイル名からアーキテクチャを推論する
    const exe_isa = /starlight/.test(path.basename(process.argv[1])) ? "starlight" : "nanoha";
    const isa = argv.isa || exe_isa;
    //emitter
    const {emitter, optimizer} = getisa(isa, argv);
    //validation
    if(argv.out===""){
        console.error("ERROR: output file is not specified");
        process.exit(1);
        return;
    }
    //処理する
    read(argv._,(files)=>{
        core(emitter, files, argv.out, false, argv.basic===true ? null : optimizer);
    });
}
export function core(emitter: emit.Emitter, datas:Array<FileData>,outfile:string,preprocess:boolean,optimizer: emit.Optimizer):void{
    //bufを得る
    let buf = make(emitter, datas, preprocess, optimizer);
    //書く
    let out;
    if(outfile){
        //ファイルに書き込む
        out=fs.createWriteStream(outfile, {
            flags: 'w',
            defaultEncoding: "string"===typeof buf ? "utf8" : "binary",
            mode: 0o666
        });
    }else{
        out=process.stdout;
    }
    //書き込んで終わり
    if(out!==process.stdout){
        out.end(buf);
    }else{
        //stdoutは閉じてはいけない
        out.write(buf);
    }
}

export function getisa(isa:string, argv:any):{emitter: emit.Emitter; optimizer: emit.Optimizer}{
    /*if(isa==="starlight"){
        return {
            emitter: emit.expression.starlight,
            optimizer: emit.optimizer.starlight
        };
    }else */if(isa==="nanoha"){
        return {
            emitter: emit.expression.nanoha,
            optimizer: emit.optimizer.nanoha.get(argv)
        };
    }else{
        throw new Error("Unknown isa '"+isa+"'");
    }
}

function help():void{
    //help message
    console.log("USAGE: %s [FILE1 FILE2 ...] [options]",p.name)
    console.log("\tAssembles from files or stdin.\n");
    console.log("options:");
    console.log("\t-h, --help: Show this message");
    console.log("\t-o, --out: Stream the result to specified file instead of stdout");
    console.log("\t    --isa: starlight or nanoha (defalts to starlight)");
    console.log("\t      -O0: Do not optimize");
    console.log("\t      -O1: Do not increase instruction number");
    console.log("\t      -O2: full optimization");
}

function version():void{
    console.log("%s v%s",p.name,p.version);
}
