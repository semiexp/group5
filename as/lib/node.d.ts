declare class Buffer{
    constructor(size:number);
    public writeUInt32LE(value:number,offset:number,noAssert?:boolean):void;
}
declare class Promise<T>{
    constructor(callback:(resolve:(result:T)=>void, reject:(e:Error)=>void)=>void);
    then<U>(callback:(T)=>(U|Promise<U>)):Promise<U>;
    catch(callback:(Error)=>void):Promise<T>;

    static resolve<T>(r:T|Promise<T>):Promise<T>;
    static reject<T>(e:Error):Promise<T>;
    static all<T>(iterable:Array<T|Promise<T>>):Promise<Array<T>>;
    static race<T>(iterable:Array<T|Promise<T>>):Promise<T>;
}

declare class WeakMap<K,V>{
    get(key: K, defaultValue?: V):V;
    set(key: K, value: V):void;
    has(key: K):boolean;
    delete(key: K):void;
    clear():void;
}

declare class Set<T>{
    size:number;
    add(value:T):Set<T>;
    clear():void;
    delete(value:T):boolean;
    has(value:T):boolean;
}

declare class Map<K,V>{
    size:number;
    set(key:K, value:V):Map<K,V>;
    clear():void;
    delete(key:K):boolean;
    has(key:K):boolean;
    get(key:K):V;
}



declare var process:{
    argv:Array<string>;
    exit:(code:number)=>void;
    cwd:()=>string;
    stdin:any;  //Stream
    stdout:any;
    env: any;
};
declare var __dirname:string;
declare var require:(path:string)=>any;

interface ObjectConstructor {
    assign(target: any, ...sources: any[]): any;
}

declare module "minimist"{
    function processArgs(args:Array<string>,opts?:{
        string?: string | Array<string>;
        boolean?: boolean | string | Array<string>;
        alias?: any;
        default?: any;
        stopEarny?: boolean;
        '--'?: boolean;
        unknown?: (arg:string)=>boolean;
    }):any;
    export = processArgs;
}
declare module "fs"{
    export function readFile(filename:string,options:any,callback:(err:any,data:string)=>void):void;
    export function writeFile(filename:string,data:string,callback:(err:any,data:string)=>void):void;
    export function createWriteStream(path:string,options:any):any;
}
declare module "path"{
    export function basename(path:string):string;
    export function extname(path:string):string;
    export function join(...paths:string[]):string;
    export function resolve(...paths:string[]):string;
}
declare module "child_process"{
    export function spawn(command:string,args:Array<string>,options?:{
        cwd?:string;
        env?:any;
        stdio?:string|Array<string>;
        detached?:boolean;
        uid?:number;
        gid?:number;
    }):any /* ChildProcess */;
    export function execFile(file:string,args:Array<string>,callback:(error:Error,stdout:string,stderr:string)=>void):void;
}
declare module "del"{
    function _m(pattern:string|Array<String>,options?:any):Promise<Array<string>>;
    export = _m;
}
declare module "native-promise-only"{
    export = Promise;
}
