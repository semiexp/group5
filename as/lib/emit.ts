/* emitter */
import {Expression, Expressions, Pos, Instruction, Label, Processing, Nop, Ident, Identifier, MemAddr1, MemAddr2, Neg, AsmError, LabelTable, VariableTable, EmitState} from './ast';


/* 即値をパースする */
function parseImmediate(bits:number,ident:string):number{
    let n=Number(ident);
    if(!isFinite(n)){
        //即値ではない
        return null;
    }else{
        return n & (Math.pow(2,bits)-1);
    }
}

export type Emitter = (e:Expression, state:EmitState)=>number;
export interface Optimizer {
    single(e:Expression, state:EmitState):Expression;
    text: Array<(text:Expressions, table:LabelTable, variables:VariableTable)=>[boolean, Expressions]>;
}

/* 1命令をemitする */
export module expression{
    export function starlight(e:Expression, state:EmitState): number{
        let table:LabelTable = state.table;
        if(e instanceof Instruction){
            //instruction
            let name = e.name.toString();
            if(name==="add"){
                e.ensureArgs(3);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister(), r=e.args[2].asRegister();
                return (p<<20) | (q<<14) | (r<<8);
            }else if(name==="addi"){
                e.ensureArgs(3);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister(), i=e.args[2].asImmediate(state, 14,true);
                return (0b000001 << 26) | (p<<20) | (q<<14) | i;
            }else if(name==="sub"){
                e.ensureArgs(3);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister(), r=e.args[2].asRegister();
                return (0b000010 << 26) | (p<<20) | (q<<14) | (r<<8);
            }else if(name==="lshift" || name==="lshifti"){
                e.ensureArgs(3);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister(), r:number;
                let im=false;
                if(name==="lshifti"){
                    //即値モード
                    im=true;
                    r=e.args[2].asImmediate(state, 6,true);
                }else{
                    try{
                        r=e.args[2].asRegister();
                    }catch(e){
                        //レジスタではない
                        im=true;
                        r=e.args[2].asImmediate(state, 6,true);
                    }
                }
                if(im===true){
                    //即値左shiftだ
                    return (0b000101 << 26) | (p<<20) | (q<<14) | (r << 8);
                }else{
                    //レジスタだ
                    return (0b000100 << 26) | (p<<20) | (q<<14) | (r << 8);
                }
            }else if(name==="rshift" || name==="rshifti"){
                e.ensureArgs(3);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister(), r:number;
                let im=false;
                if(name==="rshifti"){
                    //即値モード
                    im=true;
                    r=e.args[2].asImmediate(state, 6,true);
                }else{
                    try{
                        r=e.args[2].asRegister();
                    }catch(e){
                        //レジスタではない
                        im=true;
                        r=e.args[2].asImmediate(state, 6,true);
                    }
                }
                if(im===true){
                    //即値右shiftだ
                    return (0b000111 << 26) | (p<<20) | (q<<14) | (r << 8);
                }else{
                    //レジスタだ
                    return (0b000110 << 26) | (p<<20) | (q<<14) | (r << 8);
                }
            }else if(name==="movi"){
                e.ensureArgs(2);
                let p=e.args[0].asRegister(), q=e.args[1].asImmediate(state, 20,true);
                return (0b001000 << 26) | (p<<20) | q;
            }else if(name==="movhi"){
                e.ensureArgs(2);
                let p=e.args[0].asRegister(), q=e.args[1].asImmediate(state, 16);
                return (0b001010 << 26) | (p<<20) | (q<<4);
            }else if(name==="movhiz"){
                e.ensureArgs(2);
                let p=e.args[0].asRegister(), q=e.args[1].asImmediate(state, 16);
                return (0b001011 << 26) | (p<<20) | (q<<4);
            }else if(name==="cmp"){
                e.ensureArgs(2);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister();
                return (0b001100 << 26) | (p<<20) | (q<<14);
            }else if(name==="cmpi"){
                e.ensureArgs(2);
                let p=e.args[0].asRegister(), q=e.args[1].asImmediate(state, 20,true);
                return (0b001101 << 26) | (p<<20) | q;
            }else if(name==="fcmp"){
                e.ensureArgs(2);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister();
                return (0b011110 << 26) | (p<<20) | (q<<14);
            }else if(name==="fadd"){
                e.ensureArgs(3);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister(), r=e.args[2].asRegister();
                return (0b010000 << 26) | (p<<20) | (q<<14) | (r<<8);
            }else if(name==="fsub"){
                e.ensureArgs(3);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister(), r=e.args[2].asRegister();
                return (0b010001 << 26) | (p<<20) | (q<<14) | (r<<8);
            }else if(name==="fmul"){
                e.ensureArgs(3);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister(), r=e.args[2].asRegister();
                return (0b010010 << 26) | (p<<20) | (q<<14) | (r<<8);
            }else if(name==="finv"){
                e.ensureArgs(2);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister();
                return (0b010011 << 26) | (p<<20) | (q<<14);
            }else if(name==="fabs"){
                e.ensureArgs(2);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister();
                return (0b011111 << 26) | (p<<20) | (q<<14);
            }else if(name==="ld"){
                e.ensureArgs(2);
                let p=e.args[0].asRegister();
                let a=e.args[1];
                if(a instanceof MemAddr1){
                    let q=a.reg.asRegister();
                    let i=a.displacement.asImmediate(state, 14,true);
                    return (0b100000 << 26) | (p<<20) | (q<<14) | i;
                }else if(a instanceof MemAddr2){
                    let q=a.reg1.asRegister();
                    let r=a.reg2.asRegister();
                    let i=a.displacement.asImmediate(state, 8,true);
                    return (0b100001 << 26) | (p<<20) | (q<<14) | (r<<8) | i;
                }else{
                    throw new AsmError("Invalid argument to ld.",a.pos);
                }
            }else if(name==="sto"){
                e.ensureArgs(2);
                let p=e.args[0].asRegister();
                let a=e.args[1];
                if(a instanceof MemAddr1){
                    let q=a.reg.asRegister();
                    let i=a.displacement.asImmediate(state, 14,true);
                    return (0b100100 << 26) | (p<<20) | (q<<14) | i;
                }else if(a instanceof MemAddr2){
                    let q=a.reg1.asRegister();
                    let r=a.reg2.asRegister();
                    let i=a.displacement.asImmediate(state, 8,true);
                    return (0b100101 << 26) | (p<<20) | (q<<14) | (r<<8) | i;
                }else{
                    throw new AsmError("Invalid argument to ld.",a.pos);
                }
            }else if(name==="jmp" || name==="jmpi"){
                e.ensureArgsm([1,2]);
                let cbits:number=0b000000, dest:Ident;
                if(e.args.length===2){
                    //条件ありジャンプ
                    let c=e.args[0];
                    dest=e.args[1];
                    if(c instanceof Identifier){
                        let crname = c.value;
                        if(crname==="z" || crname==="zero"){
                            //zero=1
                            cbits = 0b000011;
                        }else if(crname==="p" || crname==="pos"){
                            //pos=1
                            cbits = 0b000101;
                        }else if(crname==="n" || crname==="neg"){
                            //neg=1
                            cbits = 0b000111;
                        }else if(crname==="u"){
                            //u
                            cbits = 0b000000;
                        }else{
                            throw new AsmError("Invalid jmp condition.",c.pos);
                        }
                    }else if(c instanceof Neg){
                        let crname = c.cr.value;
                        if(crname==="z" || crname==="zero"){
                            //zero=0
                            cbits = 0b000010;
                        }else if(crname==="p" || crname==="pos"){
                            //pos=0
                            cbits = 0b000100;
                        }else if(crname==="n" || crname==="neg"){
                            //neg=0
                            cbits = 0b000110;
                        }else{
                            throw new AsmError("Invalid jmp condition.",c.pos);
                        }
                    }else{
                        throw new AsmError("Invalid jmp condition.",c.pos);
                    }
                }else{
                    dest=e.args[0];
                }
                //conditionを作ったからdestを処理
                let im=false, q:number;
                if(name==="jmpi"){
                    im=true;
                    q=dest.asImmediate(state, 20);
                }else{
                    //ラベルがあるか調べる
                    if(dest.toString() in table){
                        //あった
                        im=true;
                        q=table[dest.toString()].position;
                    }else{
                        //レジスタをトライ
                        try{
                            q=dest.asRegister();
                        }catch(e){
                            im=true;
                            q=dest.asImmediate(state, 20);
                        }
                    }
                }
                if(im===true){
                    return (0b110001<<26) | (cbits<<20) | q;
                }else{
                    return (0b110000<<26) | (cbits<<20) | (q<<14);
                }
            }else if(name==="call"){
                e.ensureArgs(2);
                let p=e.args[0].asRegister(), dest=e.args[1], i:number;
                if(dest.toString() in table){
                    //ラベルだ
                    i = table[dest.toString()].position;
                }else{
                    //即値だ
                    i = dest.asImmediate(state, 20);
                }
                return (0b110100 << 26) | (p<<20) | i;
            }else if(name==="out"){
                e.ensureArgs(1);
                let p=e.args[0].asRegister();
                return (0b111100 << 26) | (p<<20);
            }else if(name==="in"){
                e.ensureArgs(1);
                let p=e.args[0].asRegister();
                return (0b111101 << 26) | (p<<20);
            }else if(name==="halt"){
                e.ensureArgs(0);
                return (0b111111 << 26);
                //alternatives
            }else if(name==="nop"){
                e.ensureArgs(0);
                return 0;   //add r0,r0,r0
            }else if(name==="mov"){
                e.ensureArgs(2);
                //mov r1,r2はadd r1,r0,r2と等価
                let p=e.args[0].asRegister(), q=e.args[1].asRegister();
                return (0b000000 << 26) | (p<<20) | (0<<14) | (q<<8);
            }else if(name==="neg"){
                e.ensureArgs(2);
                //neg r1,r2はsub r1,r0,r2と等価
                let p=e.args[0].asRegister(), q=e.args[1].asRegister();
                return (0b000010 << 26) | (p<<20) | (0<<14) | (q<<8);
            }else if(name==="fneg"){
                e.ensureArgs(2);
                //fneg r1,r2はfsub r1,r0,r2と等価
                let p=e.args[0].asRegister(), q=e.args[1].asRegister();
                return (0b010001 << 26) | (p<<20) | (0<<14) | (q<<8);
            }else{
                throw new AsmError("Unrecognized instruction.",e.name.pos);
            }
        }else if(e instanceof Label){
            //この段階では無視
            return null;
        }else if(e instanceof Processing){
            return null;
        }else if(e instanceof Nop){
            return null;
        }else{
            throw new AsmError("Invalid Expression",null);
        }
    }
    export function nanoha(e:Expression, state:EmitState): number{
        let table:LabelTable = state.table;
        if(e instanceof Instruction){
            //instruction
            let name = e.name.toString();
            if(name==="add"){
                e.ensureArgs(3);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister(), r=e.args[2].asRegister();
                return (p<<20) | (q<<14) | (r<<8);
            }else if(name==="addi"){
                e.ensureArgs(3);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister(), i=e.args[2].asImmediate(state, 14,true);
                return (0b000001 << 26) | (p<<20) | (q<<14) | i;
            }else if(name==="sub"){
                //sub x, y, z:   x <- y - z
                let p=e.args[0].asRegister(), q=e.args[1].asRegister(), r=e.args[2].asRegister();
                e.ensureArgs(3);
                return (0b000010 << 26) | (p<<20) | (r<<14) | (q<<8);
            }else if(name==="subi"){
                e.ensureArgs(3);
                let p=e.args[0].asRegister(), i=e.args[1].asImmediate(state, 14, true), q=e.args[2].asRegister();
                return (0b000011 << 26) | (p<<20) | (q<<14) | i;
            }else if(name==="rshift"){
                e.ensureArgs(3);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister(), r=e.args[2].asRegister();
                return (0b000100 << 26) | (p<<20) | (q<<14) | (r<<8);
            }else if(name==="rshifti"){
                e.ensureArgs(3);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister(), i=e.args[2].asImmediate(state, 14,true);
                return (0b000101 << 26) | (p<<20) | (q<<14) | i;
            }else if(name==="lshift"){
                e.ensureArgs(3);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister(), r=e.args[2].asRegister();
                return (0b000110 << 26) | (p<<20) | (q<<14) | (r<<8);
            }else if(name==="lshifti"){
                e.ensureArgs(3);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister(), i=e.args[2].asImmediate(state, 14,true);
                return (0b000111 << 26) | (p<<20) | (q<<14) | i;
            }else if(name==="cmpeq"){
                e.ensureArgs(3);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister(), r=e.args[2].asRegister();
                return (0b001000 << 26) | (p<<20) | (q<<14) | (r<<8);
            }else if(name==="cmpeqi"){
                e.ensureArgs(3);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister(), i=e.args[2].asImmediate(state, 14,true);
                return (0b001001 << 26) | (p<<20) | (q<<14) | i;
            }else if(name==="cmpgt"){
                e.ensureArgs(3);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister(), r=e.args[2].asRegister();
                return (0b001010 << 26) | (p<<20) | (q<<14) | (r<<8);
            }else if(name==="cmpgti"){
                e.ensureArgs(3);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister(), i=e.args[2].asImmediate(state, 14,true);
                return (0b001011 << 26) | (p<<20) | (q<<14) | i;
            }else if(name==="cmplt"){
                e.ensureArgs(3);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister(), r=e.args[2].asRegister();
                return (0b001100 << 26) | (p<<20) | (q<<14) | (r<<8);
            }else if(name==="cmplti"){
                e.ensureArgs(3);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister(), i=e.args[2].asImmediate(state, 14,true);
                return (0b001101 << 26) | (p<<20) | (q<<14) | i;
            }else if(name==="cmpfeq"){
                e.ensureArgs(3);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister(), r=e.args[2].asRegister();
                return (0b001110 << 26) | (p<<20) | (q<<14) | (r<<8);
            }else if(name==="cmpfeqi"){
                e.ensureArgs(3);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister(), i=e.args[2].asImmediate(state, 14,true);
                return (0b001111 << 26) | (p<<20) | (q<<14) | i;
            }else if(name==="cmpfgt"){
                e.ensureArgs(3);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister(), r=e.args[2].asRegister();
                return (0b010000 << 26) | (p<<20) | (q<<14) | (r<<8);
            }else if(name==="cmpfgti"){
                e.ensureArgs(3);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister(), i=e.args[2].asImmediate(state, 14,true);
                return (0b010001 << 26) | (p<<20) | (q<<14) | i;
            }else if(name==="fneg"){
                e.ensureArgs(2);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister();
                return (0b010011 << 26) | (p<<20) | (q<<14);
            }else if(name==="fabs"){
                e.ensureArgs(2);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister();
                return (0b010101 << 26) | (p<<20) | (q<<14);
            }else if(name==="movi"){
                e.ensureArgs(2);
                let p=e.args[0].asRegister(), q=e.args[1].asImmediate(state, 20,true);
                return (0b010110 << 26) | (p<<20) | q;
            }else if(name==="movhiz"){
                e.ensureArgs(2);
                let p=e.args[0].asRegister(), q=e.args[1].asImmediate(state, 20);
                return (0b010111 << 26) | (p<<20) | q;
            }else if(name==="fadd"){
                e.ensureArgs(3);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister(), r=e.args[2].asRegister();
                return (0b011000 << 26) | (p<<20) | (q<<14) | (r<<8);
            }else if(name==="faddi"){
                e.ensureArgs(3);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister(), i=e.args[2].asImmediate(state, 14,false);
                return (0b011001 << 26) | (p<<20) | (q<<14) | i;
            }else if(name==="fsub"){
                e.ensureArgs(3);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister(), r=e.args[2].asRegister();
                return (0b011010 << 26) | (p<<20) | (r<<14) | (q<<8);
            }else if(name==="fsubi"){
                e.ensureArgs(3);
                let p=e.args[0].asRegister(), i=e.args[1].asImmediate(state, 14, false), q=e.args[2].asRegister();
                return (0b011011 << 26) | (p<<20) | (q<<14) | i;
            }else if(name==="fmul"){
                e.ensureArgs(3);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister(), r=e.args[2].asRegister();
                return (0b011100 << 26) | (p<<20) | (q<<14) | (r<<8);
            }else if(name==="fmuli"){
                e.ensureArgs(3);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister(), i=e.args[2].asImmediate(state, 14,false);
                return (0b011101 << 26) | (p<<20) | (q<<14) | i;
            }else if(name==="finv"){
                e.ensureArgs(2);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister();
                return (0b011110 << 26) | (p<<20) | (q<<14);
            }else if(name==="fsqrt"){
                e.ensureArgs(2);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister();
                return (0b011111 << 26) | (p<<20) | (q<<14);
            }else if(name==="ld"){
                e.ensureArgs(2);
                let p=e.args[0].asRegister();
                let a=e.args[1];
                if(a instanceof MemAddr1){
                    let q=a.reg.asRegister();
                    let i=a.displacement.asImmediate(state, 14,true);
                    return (0b100001 << 26) | (p<<20) | (q<<14) | i;
                }else{
                    let i = a.asImmediate(state, 20, false);
                    return (0b100000 << 26) | (p<<20) | i;
                }
            }else if(name==="sto"){
                e.ensureArgs(2);
                let p=e.args[0].asRegister();
                let a=e.args[1];
                if(a instanceof MemAddr1){
                    let q=a.reg.asRegister();
                    let i=a.displacement.asImmediate(state, 14,true);
                    return (0b100101 << 26) | (p<<20) | (q<<14) | i;
                }else{
                    let i = a.asImmediate(state, 20, false);
                    return (0b100100 << 26) | (p<<20) | i;
                }
            }else if(name==="jmpz"){
                e.ensureArgs(2);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister();
                return (0b110000 << 26) | (p<<20) | (q<<14);
            }else if(name==="jmpzi"){
                e.ensureArgs(2);
                let p=e.args[0].asRegister(), i=e.args[1].asImmediate(state, 20, false);
                return (0b110001 << 26) | (p<<20) | i;
            }else if(name==="jmp"){
                e.ensureArgs(2);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister();
                return (0b110010 << 26) | (p<<20) | (q<<14);
            }else if(name==="jmpi"){
                e.ensureArgs(2);
                let p=e.args[0].asRegister(), i=e.args[1].asImmediate(state, 20, false);
                return (0b110011 << 26) | (p<<20) | i;
            }else if(name==="call"){
                e.ensureArgs(2);
                let p=e.args[0].asRegister(), i=e.args[1].asImmediate(state, 20, false);
                return (0b110100 << 26) | (p<<20) | i;
            }else if(name==="out"){
                e.ensureArgs(1);
                let p=e.args[0].asRegister();
                return (0b111100 << 26) | (p<<20);
            }else if(name==="in"){
                e.ensureArgs(1);
                let p=e.args[0].asRegister();
                return (0b111101 << 26) | (p<<20);
            }else if(name==="cpuid"){
                e.ensureArgs(2);
                let p=e.args[0].asRegister(), i=e.args[1].asImmediate(state, 20, false);
                return (0b111110 << 26) | (p<<20) | i;
            }else if(name==="halt"){
                e.ensureArgs(0);
                return (0b111111 << 26);
            //alternatives
            }else if(name==="nop"){
                e.ensureArgs(0);
                return 0;   //add r0,r0,r0
            }else if(name==="mov"){
                //mov r1,r2はadd r1,r0,r2と等価
                e.ensureArgs(2);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister();
                return (0b000000 << 26) | (p<<20) | (0<<14) | (q<<8);
            }else if(name==="neg"){
                //neg r1,r2はsub r1,r0,r2と等価
                e.ensureArgs(2);
                let p=e.args[0].asRegister(), q=e.args[1].asRegister();
                return (0b000010 << 26) | (p<<20) | (q<<14) | (0<<8);
            }else if(name==="jmpu"){
                //jump unconditionally
                e.ensureArgs(1);
                let p=e.args[0].asRegister();
                return (0b110000 << 26) | (0b000000<<20) | (p<<14);
            }else if(name==="jmpui"){
                e.ensureArgs(1);
                let i=e.args[0].asImmediate(state, 20, false);
                return (0b110001 << 26) | (0b000000<<20) | i;
            }else{
                throw new AsmError("Unrecognized instruction.",e.name.pos);
            }
        }else if(e instanceof Label){
            //この段階では無視
            return null;
        }else if(e instanceof Processing){
            return null;
        }else if(e instanceof Nop){
            return null;
        }else{
            throw new AsmError("Invalid Expression",null);
        }
    }
}

/* 最適化 */
export namespace optimizer{
    /*
    export namespace starlight{
        export function single(e:Expression, state:EmitState):Expression{
            return e;
        }
        export var text = [];
    }*/
    export namespace nanoha{
        function single(e:Expression, state:EmitState):Expression{
            /* 最適化 */
            if(e instanceof Instruction){
                const name = e.name.toString();

                if(name==="addi"){
                    //addiに最適化の可能性あり
                    e.ensureArgs(3);
                    const p=e.args[0].asRegister(), q=e.args[1].asRegister(), i=e.args[2].asImmediate(state, 14,true);
                    if(p===q && i===0){
                        //これはいらない
                        return null;
                    }
                }
            }
            return e;
        }
        export function get(argv:any):Optimizer{
            if(argv.O==="0"){
                return {
                    single: (e,_)=>e,
                    text: []
                };
            }else if(argv.O==="1"){
                return {
                    single,
                    text: [mergeLabel, directJump, jmpAfterJmp, unreachable, basicBlock2, jmpCut, /*jmpAfterCall*/]
                };
            }
            return {
                single,
                text: [mergeLabel, directJump, jmpAfterJmp, unreachable, basicBlock, jmpCut/*, jmpAfterCall*/]
            };
        }
        //最適化1：二重ジャンプをなくす
        function directJump(text:Expressions, table: LabelTable, variables:VariableTable):[boolean, Expressions]{
            //二重ジャンプをなくす
            const destTable = {};
            const labels = Object.getOwnPropertyNames(table);
            for(let i=0, l=labels.length; i<l; i++){
                const label = labels[i];
                const {index} = table[label];
                const [,e]=nextInstruction(text, index);
                if(e != null){
                    const name = e.name.toString();
                    //ラベルから即ジャンプかも
                    if(name==="jmpu"){
                        destTable[label] = {
                            imm: false,
                            target: e.args
                        };
                    }else if(name==="jmpui"){
                        destTable[label] = {
                            imm: true,
                            target: e.args
                        };
                    }
                }
            }
            //命令の置換
            const repl = new WeakMap<Expression, Expression>();
            //ジャンプ命令を走査
            let flag = false;
            for(let i=0, l=text.length; i<l; i++){
                const e = text[i];
                if(e instanceof Instruction){
                    const name = e.name.toString();
                    if(name==="jmpzi" || name==="jmpi"){
                        //ジャンプ命令発見（条件つき）
                        e.ensureArgs(2);
                        const cond = e.args[0];
                        let label = e.args[1].toString();
                        let imm = null, target;
                        //連続飛び先をたどる
                        while(destTable[label] != null){
                            ({imm, target} = destTable[label]);
                            label = target[0].toString();
                        }
                        if(imm == null){
                            //そのままだった
                            continue;
                        }
                        //飛び先が変わった
                        repl.set(e,
                            new Instruction(
                                new Identifier(imm===true ? name : (name==="jmpzi" ? "jmpz" : "jmp"), e.name.pos),
                                [cond].concat(target)));
                        flag = true;

                    }else if(name==="jmpui"){
                        //条件なし
                        e.ensureArgs(1);
                        let label = e.args[0].toString();
                        let imm = null, target;
                        //連続飛び先をたどる
                        while(destTable[label] != null){
                            ({imm, target} = destTable[label]);
                            label = target[0].toString();
                        }
                        if(imm == null){
                            //そのままだった
                            continue;
                        }
                        //飛び先が変わった
                        repl.set(e,
                            new Instruction(
                                new Identifier(imm===true ? name : "jmpu", e.name.pos),
                                target.concat([])));
                    }
                }
            }
            return [flag, runReplace(text, repl)];
        }
        //最適化2：callの直後のjmpを最適化
        function jmpAfterCall(text:Expressions):[boolean,Expressions]{
            // @target pattern:
            // call %r60, A
            // jmpui B
            //
            // @result pattern:
            // movi %r60, B
            // jmpui A

            const repl = new WeakMap<Expression, Expression>();
            let flag = false;
            //callを走査
            for(let i=0, l=text.length; i<l; i++){
                const e=text[i];
                if(e instanceof Instruction){
                    const name = e.name.toString();
                    if(name==="call"){
                        e.ensureArgs(2);
                        const lnk = e.args[0];
                        const label = e.args[1].toString();
                        //次がjmp命令か
                        const [,e2] = nextInstruction(text, i+1, false);
                        if(e2 != null && e2.name.toString()==="jmpui"){
                            e2.ensureArgs(1);
                            const label2 = e2.args[0].toString();
                            //条件を満たした
                            repl.set(e,
                                new Instruction(
                                    new Identifier("movi", e.name.pos),
                                    [lnk, e2.args[0]]
                                ));
                            repl.set(e2,
                                new Instruction(
                                    new Identifier("jmpui", e2.name.pos),
                                    [e.args[1]]
                                ));
                            flag = true;
                        }
                    }
                }
            }
            return [flag, runReplace(text, repl)];
        }

        function jmpAfterJmp(text:Expressions):[boolean, Expressions]{
            // @target pattern:
            // jmpi R, A
            // jmpui? B
            // A:
            //
            // @result pattern:
            // jmpzi? R, B
            // A:
            const repl = new WeakMap<Expression, Expression>();
            let flag = false;
            for(let i=0, l=text.length; i<l; i++){
                const e=text[i];
                if(e instanceof Instruction){
                    const name = e.name.toString();
                    if(name==="jmpi" || name==="jmpzi"){
                        e.ensureArgs(2);
                        const cond = e.args[0];
                        const label = e.args[1].toString();
                        //直後にjmpuかjmpuiがあれば
                        const [j,e2] = nextInstruction(text, i+1, false);
                        if(e2 != null){
                            const name2 = e2.name.toString();
                            if(name2==="jmpu" || name2==="jmpui"){
                                e2.ensureArgs(1);
                                const target = e2.args;
                                //次の命令がラベルになっていれば消せる
                                if(findSiblingLabel(label, j+1)){
                                    //最適化可能なパターンだ
                                    repl.set(e,
                                        new Instruction(
                                            new Identifier(
                                                name==="jmpi" ?
                                                    (name2==="jmpu" ? "jmpz" : "jmpzi") :
                                                    (name2==="jmpu" ? "jmp" : "jmpi"),
                                                e.name.pos),
                                            [cond].concat(target)));
                                    repl.set(e2, null);
                                    flag = true;
                                }
                            }
                        }
                    }
                }
            }
            return [flag, runReplace(text, repl)];

            function findSiblingLabel(label:string, index:number){
                for(const l=text.length;index < l;index++){
                    const e=text[index];
                    if(e instanceof Instruction){
                        //見つからなかった
                        return false;
                    }
                    if(e instanceof Label && e.name.toString()===label){
                        //見つかった
                        return true;
                    }
                }
                return false;
            }
        }
        function unreachable(text:Expressions, table:LabelTable):[boolean, Expressions]{
            //到達不可能な命令たちは除去する
            //まず命令が参照しているラベルを列挙（jmp系, movi）
            const labels = referencedLabels(text);
            //到達不可能な命令は消していく
            const repl = new WeakMap<Expression, Expression>();
            let alive = true;
            let flag = false;
            const l = text.length;
            for(let i=0; i<l; i++){
                const e = text[i];
                if(e instanceof Label){
                    const label = e.name.toString();
                    if(labels.has(label)){
                        //到達可能ラベルなので生き返る
                        alive = true;
                    }else if(table[label]==null || table[label].globl===false){
                        //このラベルは参照されていないから消す
                        repl.set(e, null);
                        flag = true;
                    }
                }else if(e instanceof Instruction){
                    if(alive===false){
                        //この命令には到達しない
                        repl.set(e, null);
                        flag = true;
                    }
                    const name = e.name.toString();
                    if(name==="jmpu" || name==="jmpui" || name==="halt"){
                        //無条件ジャンプだ
                        alive = false;
                    }
                }
            }
            return [flag, runReplace(text, repl)];
        }
        function basicBlock(text: Expressions, table:LabelTable):[boolean, Expressions]{
            text = text.concat([]);
            const labels = new Set<string>();
            const COPY_LEN_MAX = 10;    //何命令以下なら複製するか

            //jmpuiに参照されているやつを探す
            const l = text.length;
            for(let i=0; i<l; i++){
                const e = text[i];
                if(e instanceof Instruction){
                    const name = e.name.toString();
                    if(name==="jmpui"){
                        e.ensureArgs(1);
                        let a2 = e.args[0];
                        if(a2 instanceof Identifier){
                            const a2name = a2.toString();
                            labels.add(a2name);
                        }
                    }
                }
            }

            //1が入ってるやつが最適化対象
            bigLoop: while(true){
                let starting = true;    //ジャンプ以外で到達できない場所だ
                let active:string = null;   //最適化中のラベル
                let active_starting:boolean = false;    //このラベルはstartingだったか
                let active_had_label:boolean = false;
                let kept:Expressions = [];
                for(let i=0; i<l; i++){
                    const e = text[i];
                    if(e instanceof Label){
                        const name = e.name.toString();
                        if(/*starting && */labels.has(name) && (table[name]==null || table[name].globl===false)){
                            //ここから最適化可能
                            active = name;
                            active_starting = starting;
                            kept = [e];
                        }else{
                            active = null;
                        }
                        starting = false;
                    }else if(e instanceof Instruction){
                        const name = e.name.toString();
                        if(name==="jmpu" || name==="jmpui" || name==="halt"){
                            //ブロックの終端
                            if(active != null){
                                //最適化完了
                                if(name==="jmpui" && e.args[0] instanceof Identifier && e.args[0].toString()===active){
                                    // 無限ループを最適化するのはまずい
                                    labels.delete(active);
                                    continue bigLoop;
                                }
                                kept.push(e);
                                if(active_starting===false && kept.length > COPY_LEN_MAX){
                                    //長すぎる
                                    labels.delete(active);
                                    continue bigLoop;
                                }
                                break;
                            }else{
                                starting = true;
                            }
                        }else if(active != null){
                            kept.push(e);
                        }
                    }
                }
                if(active != null){
                    //最適化ができるところを見つけた
                    for(let i=0; i<l; i++){
                        const e=text[i];
                        if(e instanceof Instruction && e.name.toString()==="jmpui" && e.args[0] instanceof Identifier){
                            const lbl = e.args[0].toString();
                            if(active===lbl){
                                //ここに挿入したい
                                const repl = new WeakMap<Expression, Expression>();
                                if(active_starting){
                                    //ここに来るやつがなければ除去していい
                                    for(const e of kept){
                                        repl.set(e, null);
                                    }
                                }else{
                                    //除去しないときはラベルはコピーしない
                                    kept = kept.slice(1);
                                }
                                const kept2 = kept.map(e => e.clone());
                                text.splice(i, 1, ...kept2);
                                text = runReplace(text, repl);
                                return [true, text];
                            }
                        }
                    }
                }
                break;
            }
            return [false, text];
        }
        function basicBlock2(text: Expressions, table:LabelTable):[boolean, Expressions]{
            //コードの複製はしない
            text = text.concat([]);
            const labels = new Set<string>();

            //jmpuiに参照されているやつを探す
            const l = text.length;
            for(let i=0; i<l; i++){
                const e = text[i];
                if(e instanceof Instruction){
                    const name = e.name.toString();
                    if(name==="jmpui"){
                        e.ensureArgs(1);
                        let a2 = e.args[0];
                        if(a2 instanceof Identifier){
                            const a2name = a2.toString();
                            labels.add(a2name);
                        }
                    }
                }
            }

            //1が入ってるやつが最適化対象
            bigLoop: while(true){
                let starting = true;    //ジャンプ以外で到達できない場所だ
                let active:string = null;   //最適化中のラベル
                let active_starting:boolean = false;    //このラベルはstartingだったか
                let active_had_label:boolean = false;
                let kept:Expressions = [];
                for(let i=0; i<l; i++){
                    const e = text[i];
                    if(e instanceof Label){
                        const name = e.name.toString();
                        if(starting && labels.has(name) && (table[name]==null || table[name].globl===false)){
                            //ここから最適化可能
                            active = name;
                            active_starting = starting;
                            kept = [e];
                        }else{
                            active = null;
                        }
                        starting = false;
                    }else if(e instanceof Instruction){
                        const name = e.name.toString();
                        if(name==="jmpu" || name==="jmpui" || name==="halt"){
                            //ブロックの終端
                            if(active != null){
                                //最適化完了
                                if(name==="jmpui" && e.args[0] instanceof Identifier && e.args[0].toString()===active){
                                    // 無限ループを最適化するのはまずい
                                    labels.delete(active);
                                    continue bigLoop;
                                }
                                kept.push(e);
                                break;
                            }else{
                                starting = true;
                            }
                        }else if(active != null){
                            kept.push(e);
                        }
                    }
                }
                if(active != null){
                    //最適化ができるところを見つけた
                    for(let i=0; i<l; i++){
                        const e=text[i];
                        if(e instanceof Instruction && e.name.toString()==="jmpui" && e.args[0] instanceof Identifier){
                            const lbl = e.args[0].toString();
                            if(active===lbl){
                                //ここに挿入したい
                                const repl = new WeakMap<Expression, Expression>();
                                if(active_starting){
                                    //ここに来るやつがなければ除去していい
                                    for(const e of kept){
                                        repl.set(e, null);
                                    }
                                }else{
                                    //除去しないときはラベルはコピーしない
                                    kept = kept.slice(1);
                                }
                                const kept2 = kept.map(e => e.clone());
                                text.splice(i, 1, ...kept2);
                                text = runReplace(text, repl);
                                return [true, text];
                            }
                        }
                    }
                }
                break;
            }
            return [false, text];
        }
        function jmpCut(text:Expressions):[boolean, Expressions]{
            // @target pattern:
            // jmpui A
            // A:
            //
            // @result pattern:
            // A:
            const repl = new WeakMap<Expression, Expression>();
            let flag = false;
            let current_e:Expression = null;
            let current_l:string = null;
            for(let i=0, l=text.length; i<l; i++){
                const e=text[i];
                if(e instanceof Instruction){
                    const name = e.name.toString();
                    if(name==="jmpui"){
                        e.ensureArgs(1);
                        //ラベルを登録
                        current_e = e;
                        current_l = e.args[0].toString();
                    }else{
                        current_e = null;
                        current_l = null;
                    }
                }else if(e instanceof Label){
                    const name = e.name.toString();
                    if(current_l === name){
                        //直後ジャンプをみつけた
                        repl.set(current_e, null);
                        flag = true;
                    }
                }
            }
            return [flag, runReplace(text, repl)];
        }
        function mergeLabel(text:Expressions, table:LabelTable):[boolean, Expressions]{
            //同じ位置を示すラベルはひとつにまとめる
            const labels = new Map<string,string>();
            const repl = new WeakMap<Expression, Expression>();
            let flag = false;
            const l = text.length;
            let current_l:string = null;
            for(let e of text){
                if(e instanceof Label){
                    const name = e.name.toString();
                    if(current_l == null){
                        //これにする
                        current_l = name;
                    }else if(table[name]==null || table[name].globl===false){
                        //この位置はすでにラベルがある
                        labels.set(name,current_l);
                        flag = true;
                        repl.set(e, null);
                    }
                }else if(e instanceof Instruction){
                    current_l = null;
                }
            }
            //ラベルを置き換える
            for(let e of text){
                if(e instanceof Instruction){
                    const name = e.name.toString();
                    switch(name){
                        case "jmpi":
                        case "jmpzi":
                        case "call":
                        case "movi":
                            e.ensureArgs(2);
                            let a1 = e.args[1];
                            if(a1 instanceof Identifier){
                                //これがラベルだ
                                const lname = a1.toString();
                                if(labels.has(lname)){
                                    //ラベルの置き換えあった
                                    flag = true;
                                    repl.set(e, new Instruction(
                                                e.name,
                                                [e.args[0], new Identifier(labels.get(lname), a1.pos)]));
                                }
                            }
                            break;
                        case "jmpui":
                            e.ensureArgs(1);
                            let a2 = e.args[0];
                            if(a2 instanceof Identifier){
                                const lname = a2.toString();
                                if(labels.has(lname)){
                                    flag = true;
                                    repl.set(e, new Instruction(
                                                e.name,
                                                [new Identifier(labels.get(lname), a2.pos)]));
                                }
                            }
                            break;
                    }
                }
            }
            return [flag, runReplace(text, repl)];
        }

        //次の命令を探す
        //overLabel: falseのときはラベルを超えられない
        function nextInstruction(text: Expressions, index:number, overLabel:boolean = true):[number, Instruction]{
            const l=text.length;
            while(index<l){
                const e=text[index];
                if(!overLabel && e instanceof Label){
                    return [null, null];
                }
                if(e instanceof Instruction){
                    return [index, e];
                }
                index++;
            }
            return [null, null];
        }
        //命令を置換
        function runReplace(text:Expressions, repl: WeakMap<Expression, Expression>):Expressions{
            const result: Expressions = [];
            for(let e of text){
                if(repl.has(e)){
                    let e2 = repl.get(e);
                    if(e2 != null){
                        result.push(e2);
                    }
                }else{
                    result.push(e);
                }
                
            }
            return result;
        }
        //命令に参照されるラベルを列挙
        function referencedLabels(text:Expressions):Set<string>{
            const labels = new Set<string>();
            const l = text.length;
            for(let i=0; i<l; i++){
                const e = text[i];
                if(e instanceof Instruction){
                    const name = e.name.toString();
                    switch(name){
                        case "jmpi":
                        case "jmpzi":
                        case "call":
                        case "movi":
                            e.ensureArgs(2);
                            let a1 = e.args[1];
                            if(a1 instanceof Identifier){
                                labels.add(a1.toString());
                            }
                            break;
                        case "jmpui":
                            e.ensureArgs(1);
                            let a2 = e.args[0];
                            if(a2 instanceof Identifier){
                                labels.add(a2.toString());
                            }
                            break;
                    }
                }
            }
            return labels;
        }
    }
}
