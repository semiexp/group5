///<reference path="./node.d.ts" />
import minimist = require('minimist');
import fs = require('fs');
import path = require('path');
import child_process = require('child_process');
import Promise = require('native-promise-only');
import del = require('del');
var p=require('../package.json');

import {read, core, getisa, FileData} from './main';

//starlightcの実装
export function main(){
    let args = process.argv.slice(2);
    let argv = minimist(args,{
        string: ['out','isa','mincaml','inline','O'],
        boolean: ['help','version','s','verbose','VERBOSE','software-finv','preprocess','nolib','basic'],
        alias: {
            h: 'help',
            o: 'out',
            v: 'version',
            m: 'mincaml',
            p: 'preprocess'
        }
    });
    if(argv.help===true){
        help();
        return;
    }
    if(argv.version===true){
        version();
        return;
    }
    //実行ファイル名からアーキテクチャを推論する
    const exe_isa = /starlight/.test(path.basename(process.argv[1])) ? "starlight" : "nanoha";
    const isa = argv.isa || exe_isa;
    if(isa!=="nanoha"){
        console.error("ERROR: unknown isa '%s'", isa);
        process.exit(1);
    }
    let files = argv._;

    if(files.length===0){
        //ファイルが指定されていないぞ
        console.error("ERROR: at least one file is needed.");
        process.exit(1);
    }
    if(argv.preprocess===true && argv.s==true){
        console.error("ERROR: -s and -p options cannot be both used.");
        process.exit(1);
    }
    if(argv.preprocess===true && !argv.out){
        console.error("ERROR: --out option is required to use --preprocess option.");
        process.exit(1);
    }
    //min-camlの場所
    const mincaml = argv.mincaml ? path.resolve(argv.mincaml) : path.join(__dirname,"..","dist",isa);

    //mlライブラリを追加？
    if(argv["nolib"]!==true){
        files = [path.resolve(mincaml, "libmincaml2.ml")].concat(files);
    }

    //min-camlに全部投げる
    filePartition(files).then((fileps)=>{
        return Promise.all(fileps.map(({ext, files})=>{
            return new Promise<{filename: string; tmp: boolean}>((resolve,reject)=>{
                if(ext===".s"){
                    //.sファイルはすでにコンパイルされているから素通り
                    resolve({
                        filename: files[0],
                        tmp: false
                    });
                    return;
                }else if(ext!==".ml"){
                    reject(new Error("ERROR: "+files[0]+" cannot be compiled; its extension is not .ml"));
                    return;
                }
                //min-camlにわたすファイルは拡張子を撮る
                const bs = files.map(file => file.slice(0, -ext.length));
                //アセンブリファイルは最後のやつに
                const asf = bs[bs.length-1]+".s";

                //min-camlへのオプション
                const mcoption = bs.concat([]);
                if(argv["software-finv"]===true){
                    //min-camlにソフトウェアfinvをつかってもらう
                    mcoption.push("-sortware-finv");
                }
                if(argv["VERBOSE"]===true){
                    mcoption.push("-verbose");
                }
                const verbose = argv.verbose || argv.VERBOSE;
                if(argv.inline){
                    mcoption.push("-inline",argv.inline);
                }
                const mincamlp = child_process.spawn(path.resolve(mincaml, "min-caml"), mcoption, {
                    stdio: ['ignore', 'ignore', 'pipe'],
                    env: Object.assign({}, process.env, {
                        OCAMLRUNPARAM: 'b'
                    })
                });
                let errbuf = "";
                mincamlp.on("error",(err)=>{
                    reject(err);
                });
                mincamlp.on("close",(code)=>{
                    if(code !== 0){
                        //エラー終了
                        process.stdout.write(errbuf);
                        reject(new Error("min-caml exited in error"));
                    }else{
                        //実行終了
                        resolve({
                            filename: asf,
                            tmp: true
                        });
                    }
                });
                mincamlp.stderr.on("data",(data)=>{
                    errbuf += data;
                    if(verbose){
                        //出力してあげる
                        process.stdout.write(data);
                    }
                });
            });
        }))
    })
    .then((arr)=>{
        //全部アセンブリファイルになった
        if(argv.s===true && !argv.out){
            //アセンブリを求められているのでこれで終わり
            return;
        }
        //出力されたアセンブリファイルを読んで消す
        Promise.all(arr.map(({filename, tmp})=>{
            return new Promise<FileData>((resolve, reject)=>{
                fs.readFile(filename, {encoding: "utf8"}, (err,data)=>{
                    if(err){
                        reject(err);
                        return;
                    }
                    if(tmp){
                        //一時ファイルなので読んだら消す
                        del(filename, {force:true}).then((paths)=>{
                            resolve({filename, data});
                        }).catch(reject);
                    }else{
                        resolve({filename, data});
                    }
                });
            });
        }))
        .then((arr)=>{
            //ライブラリファイルも読んで追加してあげる
            if(argv["nolib"]===true){
                //いらなかった
                return arr;
            }
            const lib = path.resolve(mincaml, "libmincaml.S");
            return new Promise((resolve, reject)=>{
                fs.readFile(lib, {encoding: "utf8"}, (err,data)=>{
                    if(err){
                        reject(err);
                        return;
                    }
                    resolve([{
                        filename: lib,
                        data
                    }].concat(arr));
                });
            });
        })
        .then((arr)=>{
            if(argv.s===true){
                //1つのファイルにまとめてあげる
                const result = arr.map(({data})=>{return data}).join("\n");
                return new Promise((resolve, reject)=>{
                    fs.writeFile(argv.out, result, (err)=>{
                        if(err){
                            reject(err);
                            return;
                        }
                        resolve(true);
                    });
                });
            }
            // 実行形式ファイルを出力する
            // ファイル名
            const lf = files[files.length-1];
            const lfext = path.extname(lf);
            const output = argv.out || files[files.length-1].slice(0,-lfext.length)+".out";
            const {emitter, optimizer} = getisa(isa, argv);
            core(emitter, arr,output, argv.preprocess===true, argv.basic===true ? null : optimizer);
        })
        .catch((err)=>{
            console.error(err);
            console.error(err.stack);
            throw err;
        });
    })
    .catch((err)=>{
        console.error(err);
        console.error(err.stack);
        throw err;
    });

    //.sと.mlの入力を順番にわける
    function filePartition(files:Array<string>):Promise<Array<{ext:string;files:Array<string>}>>{
        let result=[];
        let obj=null;
        let last_ext:string = null;
        for(let i=0, l=files.length; i<l;i++){
            let file = files[i], ext = path.extname(file);
            if(ext!==".ml" || ext!==last_ext){
                //新しい系列だ
                obj = {
                    ext,
                    files: []
                };
                result.push(obj);
                last_ext = ext;
            }
            obj.files.push(file);
        }
        return Promise.resolve(result);
    }

}


function help():void{
    //help message
    console.log("USAGE: starlightc FILE1 [options]");
    console.log("\tStarlight compiler.\n");
    console.log("options:");
    console.log("\t-h, --help      : Show this message");
    console.log("\t-o, --out       : output filename.");
    console.log("\t-m, --mincaml   : Path to min-caml (if you want to use custom min-caml)");
    console.log("\t-s              : Stop after the compilation stage to generate assembly file.");
    console.log("\t-p, --preprocess: Stop after the preprocess stage.");
    console.log("\t    --verbose   : Verbose output.");
    console.log("\t    --VERVOSE   : More verbose output.");
    console.log("\t--sortware-finv : Use software finv instead of hardware finv.");
    console.log("\t    --nolib     : Do not include libmincaml.S. Use this to compile preprocessed assembly file.");
    console.log("\t    --isa       : starlight or nanoha. Defaults to starlight.");
    console.log("\t-O0             : Do not optimize");
    console.log("\t-O1             : Do not increase instruction number");
    console.log("\t-O2             : full optimization");
    console.log("");
    console.log("compiler options:");
    console.log("\t--inline        : Limitation size of function inlining.");
}

function version():void{
    console.log("%s v%s",p.name,p.version);
}
