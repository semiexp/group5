assert = require 'assert'
main = require '../dist/main'
emitter = require('../dist/emit').expression.nanoha
optimizer = require('../dist/emit').optimizer.nanoha.get({"O":"2"})

make = (data)->
    main.make emitter, [{
        filename: "(test)"
        data: data
    }], false, optimizer

makewithnooptimize = (data)->
    main.make emitter, [{
        filename: "(test)"
        data: data
    }], false, null

#32bit LE buf
buf32 = (arr)->
    result = new Buffer(arr.length*4)
    for v,i in arr
        result.writeUInt32LE v,i*4
    result

#pretty 2 string
b = (str)->
    result = 0
    for c in str.split ""
        if c=="0"
            result <<= 1
        else if c=="1"
            result = (result << 1) | 1
    result>>>0


describe 'single instruction',->
    it 'add',->
        assert.deepEqual make("add %r5, %r12, %r29\n"),  buf32([1,b("000000 000101 001100 011101 00000000")])
    it 'addi',->
        assert.deepEqual make("addi %r9, %r10, 0x410\n"), buf32([1,b("000001 001001 001010 000100-00010000")])
    it 'sub',->
        assert.deepEqual make("sub %r1, %r1, %r2\n"), buf32([1,b("000010 000001 000010 000001 00000000")])
    it 'subi',->
        assert.deepEqual make("subi %r1, 0o300, %r2\n"), buf32([1,b("000011 000001 000010 000000-11000000")])
    it 'rshift',->
        assert.deepEqual make("rshift %r30, %r5, %r2\n"), buf32([1,b("000100 011110 000101 000010 00000000")])
    it 'rshifti',->
        assert.deepEqual make("rshifti %r2, %r10, -2\n"), buf32([1,b("000101 000010 001010 111111-11111110")])
    it 'lshift',->
        assert.deepEqual make("lshift %r2, %r54, %r4\n"), buf32([1,b("000110 000010 110110 000100 00000000")])
    it 'lshifti',->
        assert.deepEqual make("lshifti %r2, %r1, 4\n"), buf32([1,b("000111 000010 000001 000000-00000100")])
    it 'cmpeq',->
        assert.deepEqual make("cmpeq %r2, %r10, %r12\n"), buf32([1,b("001000 000010 001010 001100 00000000")])
    it 'cmpeqi',->
        assert.deepEqual make("cmpeqi %r2, %r7, 1\n"), buf32([1,b("001001 000010 000111 000000-00000001")])
    it 'cmpgt',->
        assert.deepEqual make("cmpgt %r10, %r0, %r59\n"), buf32([1,b("001010 001010 000000 111011 00000000")])
    it 'cmpgti',->
        assert.deepEqual make("cmpgti %r8, %r2, -1\n"), buf32([1,b("001011 001000 000010 111111-11111111")])
    it 'cmplt',->
        assert.deepEqual make("cmplt %r10, %r0, %r59\n"), buf32([1,b("001100 001010 000000 111011 00000000")])
    it 'cmplti',->
        assert.deepEqual make("cmplti %r8, %r2, -1\n"), buf32([1,b("001101 001000 000010 111111-11111111")])
    it 'cmpfeq',->
        assert.deepEqual make("cmpfeq %r2, %r10, %r12\n"), buf32([1,b("001110 000010 001010 001100 00000000")])
    it 'cmpfgt',->
        assert.deepEqual make("cmpfgt %r2, %r10, %r12\n"), buf32([1,b("010000 000010 001010 001100 00000000")])
    it 'fneg',->
        assert.deepEqual make("fneg %r1, %r1\n"), buf32([1,b("010011 000001 000001 000000-00000000")])
    it 'fabs',->
        assert.deepEqual make("fabs %r5, %r1\n"), buf32([1,b("010101 000101 000001 000000-00000000")])
    it 'movi',->
        assert.deepEqual make("movi %r2, 0b111100101\n"), buf32([1,b("010110 000010 0000-00000001-11100101")])
    it 'movhiz',->
        assert.deepEqual make("movhiz %r31, 73145o\n"), buf32([1,b("010111 011111 00000-111-011-001-100-101")])

    it 'fadd',->
        assert.deepEqual make("fadd %r12,%r8, %r50\n"), buf32([1,b("011000 001100 001000 110010 00000000")])
    it 'faddi',->
        assert.deepEqual make("faddi %r12,%r1, 0x2fc0\n"), buf32([1,b("011001 001100 000001 101111-11000000")])
    it 'fsub',->
        assert.deepEqual make("fsub %r12,%r8, %r50\n"), buf32([1,b("011010 001100 110010 001000 00000000")])
    it 'fsubi',->
        assert.deepEqual make("fsubi %r12, 0x3000, %r1\n"), buf32([1,b("011011 001100 000001 110000-00000000")])
    it 'fmul',->
        assert.deepEqual make("fmul %r12,%r8, %r50\n"), buf32([1,b("011100 001100 001000 110010 00000000")])
    it 'fmuli',->
        assert.deepEqual make("fmuli %r12,%r1, 0x2fc0\n"), buf32([1,b("011101 001100 000001 101111-11000000")])
    it 'finv',->
        assert.deepEqual make("finv %r1, %r5\t\n"), buf32([1,b("011110 000001 000101 000000-00000000")])
    it 'fsqrt',->
        assert.deepEqual make("fsqrt %r13, %r29  \n"), buf32([1,b("011111 001101 011101 000000-00000000")])
    describe 'ld',->
        it 'ld (register)',->
            assert.deepEqual make("ld %r10, (%r5)\n"), buf32([1,b("100001 001010 000101 000000-00000000")])
        it 'ld (imm)',->
            assert.deepEqual make("ld %r10, 0x13000\n"), buf32([1,b("100000 001010 0001-00110000-00000000")])
        it 'ld (register + imm)',->
            assert.deepEqual make("ld %r10, -4(%r5)\n"), buf32([1,b("100001 001010 000101 111111-11111100")])
    describe 'sto',->
        it 'sto (register)',->
            assert.deepEqual make("sto %r10, (%r5)\n"), buf32([1,b("100101 001010 000101 000000-00000000")])
        it 'sto (imm)',->
            assert.deepEqual make("sto %r10, 0x13000\n"), buf32([1,b("100100 001010 0001-00110000-00000000")])
        it 'sto (register + imm)',->
            assert.deepEqual make("sto %r10, -4(%r5)\n"), buf32([1,b("100101 001010 000101 111111-11111100")])
    it 'jmpz',->
        assert.deepEqual make("jmpz %r5, %r60\n"), buf32([1,b("110000 000101 111100 000000-00000000")])
    it 'jmpzi',->
        assert.deepEqual make("jmpzi %r5, 0x35941\n"), buf32([1,b("110001 000101 0011-01011001-01000001")])
    it 'jmp',->
        assert.deepEqual make("jmp %r5, %r60\n"), buf32([1,b("110010 000101 111100 000000-00000000")])
    it 'jmpi',->
        assert.deepEqual make("jmpi %r5, 0x35941\n"), buf32([1,b("110011 000101 0011-01011001-01000001")])
    it 'call',->
        assert.deepEqual make("call %r1, 0x12300 \n"), buf32([1,b("110100 000001 0001-00100011-00000000")])
    it 'out',->
        assert.deepEqual make("out %r3\n"), buf32([1,b("111100 000011 0000-00000000-00000000")])
    it 'in',->
        assert.deepEqual make("in %r4\n"), buf32([1,b("111101 000100 0000-00000000-00000000")])
    it 'cpuid',->
        assert.deepEqual make("cpuid %r3, 2\n"), buf32([1,b("111110 000011 0000-00000000-00000010")])
    it 'halt',->
        assert.deepEqual make("halt\n"), buf32([1,b("111111 000000-0000-00000000-00000000")])
    describe 'alternatives',->
        it 'nop',->
            #nopはadd r0,r0,r0と等価
            assert.deepEqual make("nop\n"), buf32([1,0])
        it 'mov',->
            #mov r1, r2はadd r1, r0, r2と等価
            assert.deepEqual make("mov r1, r2\n"), buf32([1,b("000000 000001 000000 000010 00000000")])
        it 'neg',->
            #neg r1, r2はsub r1, r0, r2と等価
            assert.deepEqual make("neg r3, r4\n"), buf32([1,b("000010 000011 000100 000000 00000000")])
        it 'jmpu',->
            #jmpuはjmpz %r0と等価
            it 'jmpu',->
                assert.deepEqual make("jmpu %r60\n"), buf32([1,b("110000 000000 111100 000000-00000000")])
            it 'jmpui',->
                assert.deepEqual make("jmpui 0x35941\n"), buf32([1,b("110001 000000 0011-01011001-01000001")])

describe "macros",->
    describe 'PC',->
        it 'basic functionality',->
            assert.deepEqual makewithnooptimize("halt\njmpui &PC(2)\n"), buf32([2, b("111111 000000-0000-00000000-00000000"), b("110001 000000 0000-00000000-00000011")])
        it 'with label rotation',->
            assert.deepEqual makewithnooptimize(""".globl foo.3
            foo.3:
                add %r1, %r1, %r2
                jmpui &PC(-2)

            .globl main
            .main main
            main:
                call %r60, foo.3
            local.label:
                halt\n
            """), buf32([4, b("110100 111100 0000-00000000-00000010"), b("111111 000000-0000-00000000-00000000"), b("000000 000001 000001 000010 00000000"), b("110001 000000 0000-00000000-00000001")])
    describe 'ADD',->
        it 'basic functionality',->
            assert.deepEqual make("movi\t%r1, &ADD(1, 2, 0b11, 0x4)\n"), buf32([1, b("010110 000001 0000-00000000-00001010")])
        it 'with pc macro', ->
            assert.deepEqual make("nop\nmovi\t%r1, &ADD(&PC(0), 2)\n"), buf32([2, 0, b("010110 000001 0000-00000000-00000011")])
        it 'with label', ->
            assert.deepEqual make(""".data
            .globl data1
            data1:
                .space 12
            .globl data2
            data2:
                .space 4
            .globl data3
            data3:
                .space 4
            .text
            .globl main
            .main main
            main:
                movi %r1, &ADD(data3, 1)\n"""), buf32([1, b("010110 000001 0000-10000000-00010001")])
    describe 'MUL',->
        it 'basic functionality',->
            assert.deepEqual make("movi\t%r1, &MUL(1, -2, 0b11)\n"), buf32([1, b("010110 000001 1111-11111111-11111010")])
    describe 'VAR',->
        it 'basic functionality',->
            assert.deepEqual make(""".text
            .globl main
            .main main
            main:
                movi %r1, &VAR(var1)
            .define var1, 0x3a\n"""), buf32([1, b("010110 000001 0000-00000000-00111010")])
        it 'with mul macro',->
            assert.deepEqual make(""".text
            .globl main
            .main main
            main:
                movi %r1, &MUL(-1, &VAR(var1))
            .define var1, 5\n"""), buf32([1, b("010110 000001 1111-11111111-11111011")])
        it 'recursive macro',->
            assert.deepEqual make(""".text
            .define var1, 10
            .globl main
            .main main
            main:
                movi %r1, &VAR(var2)
            .define var2, &ADD(&VAR(var1), 6)\n"""), buf32([1, b("010110 000001 0000-00000000-00010000")])

describe 'optimization',->
    describe '0-increment deletion',->
        it 'basic',->
            assert.deepEqual make("""addi %r4, %r4, 0
                addi %r3, %r3, 3\n"""), buf32([1, b("000001 000011 000011 000000-00000011")])
        it 'with VAR macro',->
            assert.deepEqual make("""addi %r4, %r4, &MUL(-1, &VAR(var1))
            addi %r3, %r3, 3
            .define var1, 0\n"""), buf32([1, b("000001 000011 000011 000000-00000011")])
    describe 'jmp shortcut',->
        it 'basic jmpui',->
            assert.deepEqual make("""jmpui label1
            label1:
                jmpui label2
            label1.5:
                movi %r1, label1.5
            label2:
                movi %r0, label1\n"""), make("""jmpui label2
            label1:
                jmpui label2
            label1.5:
                movi %r1, label1.5
            label2:
                movi %r0, label1\n""")
        it 'basic jmpi',->
            assert.deepEqual make("""jmpi %r3, label1
            nop
            label1:
                jmpui label2
            label1.5:
                movi %r1, label1.5
            label2:
                nop\n"""), make("""jmpi %r3, label2
            nop
            label1:
                jmpui label2
            label1.5:
                movi %r1, label1.5
            label2:
                nop\n""")
        it 'basic jmpzi',->
            assert.deepEqual make("""jmpzi %r2, label1
            nop
            label1:
                jmpui label2
            label2:
                nop\n"""), make("""jmpzi %r2, label2
            nop
            label1:
                jmpui label2
            label2:
                nop\n""")
        it 'jmpi last dest is jmpu',->
            assert.deepEqual make("""jmpi %r3, label1
            label0:
            nop
            label1:
                jmpu %r60
            label2:
                movi %r2, label0\n"""), make("""jmp %r3, %r60
            label0:
            nop
            label1:
                jmpu %r60
            label2:
                movi %r2, label0\n""")


        it 'jmpzi last dest is jmpu',->
            assert.deepEqual make("""jmpzi %r3, label1
            nop
            label1:
                jmpu %r60
                nop\n"""), make("""jmpz %r3, %r60
            nop
            label1:
                jmpu %r60
                nop\n""")
    describe 'jmpui-after-call',->
        it 'basic',->
            assert.deepEqual make("""add %r3, %r4, %r5
            call %r60, label1
            jmpui label2
            label3:
            nop
            jmpui label3
            label1:
            cmpeq %r5, %r10, %r20
            .globl label2
            label2:
            nop
            halt\n"""), make("""add %r3, %r4, %r5
            movi %r60, label2
            jmpui label1
            label3:
            nop
            jmpui label3
            label1:
            cmpeq %r5, %r10, %r20
            .globl label2
            label2:
            nop
            halt\n""")
    describe 'reverse-jmp',->
        it 'jmpi -> jmpzi',->
            assert.deepEqual make("""cmpeq %r8, %r3, %r4
            jmpi %r8, label1
            jmpui label2
            label1:
            add %r5, %r5, %r5
            label2:
            nop
            movi %r5, label1\n"""), makewithnooptimize("""cmpeq %r8, %r3, %r4
            jmpzi %r8, label2
            label1:
            add %r5, %r5, %r5
            label2:
            nop
            movi %r5, label1\n""")
        it 'jmpzi -> jmpi',->
            assert.deepEqual make("""cmpeq %r8, %r3, %r4
            jmpzi %r8, label1
            jmpui label2
            label1:
            add %r5, %r5, %r5
            label2:
            nop
            movi %r5, label1\n"""), makewithnooptimize("""cmpeq %r8, %r3, %r4
            jmpi %r8, label2
            label1:
            add %r5, %r5, %r5
            label2:
            nop
            movi %r5, label1\n""")
        it 'jmpi -> jmpz',->
            assert.deepEqual make("""cmpeq %r8, %r3, %r4
            jmpi %r8, label1
            jmpu %r60
            label1:
            add %r5, %r5, %r5
            label2:
            nop
            movi %r5, label1\n"""), makewithnooptimize("""cmpeq %r8, %r3, %r4
            jmpz %r8, %r60
            label1:
            add %r5, %r5, %r5
            label2:
            nop
            movi %r5, label1\n""")
        it 'jmpzi -> jmp',->
            assert.deepEqual make("""cmpeq %r8, %r3, %r4
            jmpzi %r8, label1
            jmpu %r60
            label1:
            add %r5, %r5, %r5
            label2:
            nop
            movi %r5, label1\n"""), makewithnooptimize("""cmpeq %r8, %r3, %r4
            jmp %r8, %r60
            label1:
            add %r5, %r5, %r5
            label2:
            nop
            movi %r5, label1\n""")
        it 'do not delete labeled jmp',->
            assert.deepEqual make("""cmpeq %r8, %r3, %r4
            jmpi %r8, label1
            label0:
            jmpui label2
            label1:
            add %r5, %r5, %r5
            label2:
            movi %r20, label0
            movi %r5, label1\n"""), make("""cmpeq %r8, %r3, %r4
            jmpi %r8, label1
            label0:
            jmpui label2
            label1:
            add %r5, %r5, %r5
            label2:
            movi %r20, label0
            movi %r5, label1\n""")
    describe 'remove unreachable instructions',->
        it 'basic',->
            assert.deepEqual make("""cmpeq %r8, %r3, %r4
            jmpui label1
            label0:
            add %r6, %r30, %r25
            label2:
            sub %r3, %r5, %r8
            label1:
            movi %r30, label2\n"""), make("""cmpeq %r8, %r3, %r4
            jmpui label1
            label2:
            sub %r3, %r5, %r8
            label1:
            movi %r30, label2\n""")
    describe 'basic block rotation',->
        it 'starting',->
            assert.deepEqual make("""cmpeq %r8, %r3, %r4
            jmpui label1
            label0:
            add %r6, %r30, %r25
            jmpui label0
            label1:
            sub %r3, %r0, %r10
            rshifti %r5, %r3, 1
            jmpui label0\n"""), make("""cmpeq %r8, %r3, %r4
            sub %r3, %r0, %r10
            rshifti %r5, %r3, 1
            jmpui label0
            label0:
            add %r6, %r30, %r25
            jmpui label0\n""")
        it 'after jmp',->
            assert.deepEqual make("""cmpeq %r8, %r3, %r4
            jmpzi %r5, label1
            movi %r1, 1
            jmpui label2
            label1:
            rshifti %r5, %r3, 1
            jmpui label3
            label2:
            sub %r3, %r0, %r10
            jmpui label1
            label3:
            fneg %r3, %r3
            halt\n"""), make("""cmpeq %r8, %r3, %r4
            jmpzi %r5, label1
            movi %r1, 1
            sub %r3, %r0, %r10
            label1:
            rshifti %r5, %r3, 1
            label3:
            fneg %r3, %r3
            halt
            label2:
            sub %r3, %r0, %r10
            jmpui label1\n""")
        it 'non-starting',->
            assert.deepEqual make("""cmpeq %r8, %r3, %r4
            jmpzi %r5, label1
            movi %r1, 1
            jmpui label2
            label1:
            rshifti %r5, %r3, 1
            label2:
            sub %r3, %r0, %r10
            jmpui label1
            label3:
            fneg %r3, %r3
            movi %r4, label3
            halt\n"""), make("""cmpeq %r8, %r3, %r4
            jmpzi %r5, label1
            movi %r1, 1
            sub %r3, %r0, %r10
            jmpui label1
            label1:
            rshifti %r5, %r3, 1
            label2:
            sub %r3, %r0, %r10
            jmpui label1
            label3:
            fneg %r3, %r3
            movi %r4, label3
            halt\n""")
