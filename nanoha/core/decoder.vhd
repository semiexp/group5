library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

use work.common.all;

entity Decoder is
	Port (
		clk : in std_logic;
		dec_in : in decoder_in_t;
		dec_out : out decoder_out_t
	 );
end Decoder;

architecture rtl of Decoder is
	type decoded_data is record
		forbidden : std_logic;
		op_alu, op_in, op_out, op_fadd, op_fmul, op_finv, op_fsqrt, op_mem, op_jmp, op_call, op_halt: std_logic;
		unit_op : unit_op_t;
		value1, value2, value3 : vtvalue_t;
		rsv_reg : register_idx_t;
		rsv_reg_enable : std_logic;
	end record;
	constant dec_zero : decoded_data := (
		value1 => '0' & x"00000000",
		value2 => '0' & x"00000000",
		value3 => '0' & x"00000000",
		unit_op => "0000",
		rsv_reg => "000000",
		others => '0'
	);
	function decode_instruction (instruction : instruction_t; pcounter : std_logic_vector(19 downto 0); pred_success : value_t; pred_failure : value_t) return decoded_data is
	variable ret : decoded_data;
	variable sgn : std_logic;
	variable reg1, reg2, reg3 : register_idx_t;
	variable opcode : std_logic_vector(5 downto 0);
	variable imm20, imm14 : vtvalue_t;
	begin
		ret := dec_zero;
		opcode := instruction(31 downto 26);
		reg1 := instruction(25 downto 20);
		reg2 := instruction(19 downto 14);
		reg3 := instruction(13 downto 8);

		sgn := instruction(19);
		imm20 := '0' & 
				sgn & sgn & sgn & sgn & sgn & sgn &
				sgn & sgn & sgn & sgn & sgn & sgn &
				instruction(19 downto 0);
		sgn := instruction(13);
		imm14 := '0' & 
				sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & 
				sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn & sgn &
				instruction(13 downto 0);

		if opcode = "010110" then
			ret.rsv_reg := reg1;
			ret.rsv_reg_enable := '1';
			ret.op_alu := '1';
			ret.unit_op := alu_op_add;
			
			ret.value1 := '0' & x"00000000";
			ret.value2 := imm20;
		elsif opcode = "010111" then
			ret.rsv_reg := reg1;
			ret.rsv_reg_enable := '1';
			ret.op_alu := '1';
			ret.unit_op := alu_op_add;
			
			ret.value1 := '0' & x"00000000";
			ret.value2 := '0' & instruction(19 downto 0) & x"000";
		elsif opcode(5 downto 3) = "000" or opcode(5 downto 3) = "001" or opcode(5 downto 3) = "010" then
			ret.rsv_reg := reg1;
			ret.rsv_reg_enable := '1';
			ret.op_alu := '1';
			ret.unit_op := opcode(4 downto 1);
			
			ret.value1 := "100" & x"000000" & reg2;
			if opcode(0) = '0' then
				ret.value2 := "100" & x"000000" & reg3;
			else
				ret.value2 := imm14;
			end if;
		end if;
		
		if opcode = "111101" then
			-- in
			ret.rsv_reg := reg1;
			ret.rsv_reg_enable := '1';
			ret.op_alu := '1';
			ret.op_in := '1';
			ret.value1 := '0' & x"00000000";
			ret.value2 := '0' & x"00000000";
			ret.unit_op := alu_op_add;
		end if;
		
		if opcode = "111100" then
			-- out
			ret.value1 := "100" & x"000000" & reg1;
			ret.op_out := '1';
		end if;
		
		if opcode(5 downto 2) = "0110" then
			-- fadd
			ret.rsv_reg := reg1;
			ret.rsv_reg_enable := '1';
			ret.value1 := "100" & x"000000" & reg2;
			
			if opcode(0) = '0' then
				ret.value2 := "100" & x"000000" & reg3;
			else
				ret.value2 := "0" & instruction(13 downto 0) & "00" & x"0000";
			end if;
			
			ret.unit_op := "000" & opcode(1);
			ret.op_fadd := '1';
		end if;
		
		if opcode(5 downto 1) = "01110" then
			-- fmul
			ret.rsv_reg := reg1;
			ret.rsv_reg_enable := '1';
			ret.value1 := "100" & x"000000" & reg2;
			if opcode(0) = '0' then
				ret.value2 := "100" & x"000000" & reg3;
			else
				ret.value2 := "0" & instruction(13 downto 0) & "00" & x"0000";
			end if;

			ret.op_fmul := '1';
		end if;

		if opcode = "011110" then
			-- finv
			ret.rsv_reg := reg1;
			ret.rsv_reg_enable := '1';
			ret.value1 := "100" & x"000000" & reg2;
			ret.op_finv := '1';
		end if;

		if opcode = "011111" then
			-- fsqrt
			ret.rsv_reg := reg1;
			ret.rsv_reg_enable := '1';
			ret.value1 := "100" & x"000000" & reg2;
			ret.op_fsqrt := '1';
		end if;

		if opcode(5 downto 1) = "10000" then
			-- ld
			ret.op_mem := '1';
			ret.rsv_reg := reg1;
			ret.rsv_reg_enable := '1';
			ret.value1 := '0' & x"00000000";
			ret.unit_op := "0000";
			
			if opcode(0) = '0' then
				ret.value2 := '0' & x"000" & instruction(19 downto 0);
				ret.value3 := '0' & x"00000000";
			else
				ret.value2 := "100" & x"000000" & reg2;
				ret.value3 := imm14;
			end if;
		end if;

		if opcode(5 downto 1) = "10010" then
			-- sto
			ret.op_mem := '1';
			ret.rsv_reg := "000000";
			ret.value1 := "100" & x"000000" & reg1;
			ret.unit_op := "0001";

			if opcode(0) = '0' then
				ret.value2 := '0' & x"000" & instruction(19 downto 0);
				ret.value3 := '0' & x"00000000";
			else
				ret.value2 := "100" & x"000000" & reg2;
				ret.value3 := imm14;
			end if;
		end if;

		if opcode(5 downto 2) = "1100" then
			-- jmp
			ret.op_jmp := '1';
			ret.rsv_reg := "000000";
			ret.value1 := "100" & x"000000" & reg1;
			ret.unit_op := "00" & (not opcode(0)) & opcode(1);
			
			if opcode(0) = '0' then
				ret.value2 := "100" & x"000000" & reg2;
			else
				ret.value2 := '0' & x"000" & instruction(19 downto 0);
			end if;
			ret.value3 := '0' & x"000" & (pcounter + x"00001");
		end if;
		
		if opcode = "110100" then
			-- call is not expected!! (call should be processed in the IF phase)
			ret.op_call := '1';
		end if;
		
		if opcode = "111110" then
			ret.rsv_reg := reg1;
			ret.rsv_reg_enable := '1';
			ret.op_alu := '1';
			ret.unit_op := alu_op_add;
			
			ret.value1 := '0' & x"00000000";
			if instruction(2 downto 0) = "000" then
				ret.value2 := '0' & pred_success;
			else
				ret.value2 := '0' & pred_failure;
			end if;
		end if;
		
		if opcode = "111111" then
			ret.op_halt := '1';
		else
			ret.op_halt := '0';
		end if;

		return ret;
	end decode_instruction;
	
	type rend_tmp_t is array(0 to (spec_size - 1)) of rob_index_t;
	type spec_tag_tmp_t is array(0 to (spec_size - 1)) of spec_tag_t;
	type rob_spec_t is array(0 to (rob_size - 1)) of spec_tag_t;
	type register_rsv_tmp_t is array(0 to (spec_size - 1)) of register_rsv_t;
	
	type reg_type is record
		-- for internal signals (register file, ROB)
		reservation1 : std_logic;
		rsv_reg1  : register_idx_t;
		rsv_dest1 : rob_index_t;
		rsv_spec1 : spec_tag_t;
		reservation2 : std_logic;
		rsv_reg2  : register_idx_t;
		rsv_dest2 : rob_index_t;
		rsv_spec2 : spec_tag_t;
		
		writeback1 : std_logic;
		reg_writeback_idx1 : rob_index_t;
		reg_writeback_target1 : register_idx_t;
		reg_writeback_value1 : value_t;
		reg_writeback_upd_vt1 : std_logic;
		writeback2 : std_logic;
		reg_writeback_idx2 : rob_index_t;
		reg_writeback_target2 : register_idx_t;
		reg_writeback_value2 : value_t;
		reg_writeback_upd_vt2 : std_logic;
		
		-- for output
		for_alu1, for_alu2, for_fadd, for_fmul, for_finv, for_fsqrt : decoder_to_afu_unit_t;
		for_mem : decoder_to_mem_unit_t;
		for_jmp : decoder_to_jmp_unit_t;
		for_out : decoder_to_out_unit_t;
		
		is_halt : std_logic;
		
		rtop, rend : rob_index_t;
		
		rob_top0, rob_top1 : vtvalue_t;
		rob_target_top0, rob_target_top1 : register_idx_t;
		rob_spec_top0, rob_spec_top1 : spec_tag_t;
		
		-- speculative
		spec_tag : std_logic_vector((spec_size - 1) downto 0);
		rend_tmp : rend_tmp_t;
		spec_tag_tmp : spec_tag_tmp_t;
		save_target, spec_success_id : spec_id_t;
		spec_addr : std_logic_vector(19 downto 0);
		pred_history : pred_history_t;
		pred_previous_value : std_logic_vector(1 downto 0);
		call_stack_idx : std_logic_vector((call_stack_width - 1) downto 0);
		wait_empty_rob : std_logic;
		clear_rsv : std_logic;
		
		-- cpuid counter
		pred_success_count : value_t;
		pred_failure_count : value_t;
	end record;
	constant rzero : reg_type := (
		reservation1 => '0',
		rsv_reg1 => "000000",
		rsv_dest1 => "0000",
		rsv_spec1 => "00",
		reservation2 => '0',
		rsv_reg2 => "000000",
		rsv_dest2 => "0000",
		rsv_spec2 => "00",
		writeback1 => '0',
		reg_writeback_idx1 => "0000",
		reg_writeback_target1 => "000000",
		reg_writeback_value1 => x"00000000",
		reg_writeback_upd_vt1 => '0',
		writeback2 => '0',
		reg_writeback_idx2 => "0000",
		reg_writeback_target2 => "000000",
		reg_writeback_value2 => x"00000000",
		reg_writeback_upd_vt2 => '0',
		
		for_alu1 => decoder_to_afu_unit_zero,
		for_alu2 => decoder_to_afu_unit_zero,
		for_fadd => decoder_to_afu_unit_zero,
		for_fmul => decoder_to_afu_unit_zero,
		for_finv => decoder_to_afu_unit_zero,
		for_fsqrt => decoder_to_afu_unit_zero,
		for_mem => decoder_to_mem_unit_zero,
		for_jmp => decoder_to_jmp_unit_zero,
		for_out => decoder_to_out_unit_zero,
		
		is_halt => '0',
		rtop => "0000",
		rend => "0000",
		
		rob_top0 => (others => '0'),
		rob_top1 => (others => '0'),
		rob_target_top0 => (others => '0'),
		rob_target_top1 => (others => '0'),
		rob_spec_top0 => (others => '0'),
		rob_spec_top1 => (others => '0'),
		
		spec_tag => "00",
		rend_tmp => (others => "0000"),
		spec_tag_tmp => (others => "00"),
		save_target => spec_size,
		spec_success_id => spec_size,
		spec_addr => x"00000",
		pred_history => (others => '0'),
		pred_previous_value => "00",
		call_stack_idx => (others => '0'),
		wait_empty_rob => '0',
		clear_rsv => '0',
		
		pred_success_count => x"00000000",
		pred_failure_count => x"00000000"
	);
	
	signal r, rin : reg_type := rzero;
	signal reg1 : register_t := (others => x"00000000");
	signal reg2 : register_t := (others => x"ffffffff");
	signal reg_rsv : register_rsv_t := (others => "00000");
	
	attribute RAM_STYLE : string;
	attribute EQUIVALENT_REGISTER_REMOVAL : string;
	attribute RAM_STYLE of reg1 : signal is "distributed";
	attribute RAM_STYLE of reg2 : signal is "distributed";
	attribute EQUIVALENT_REGISTER_REMOVAL of reg1 : signal is "NO";
	attribute EQUIVALENT_REGISTER_REMOVAL of reg2 : signal is "NO";
	
	signal rob : rob_value_t := (others => '0' & x"00000000");
	signal rob_target : rob_target_t := (others => "000000");
	signal rob_spec : rob_spec_t := (others => "00");
	
	impure function instantiate_value (in_value : vtvalue_t; flg_reg2 : std_logic) return vtvalue_t is
	variable value1 : vtvalue_t;
	begin
		value1 := in_value;
		if value1(32) = '1' then
			if reg_rsv(conv_integer(value1(5 downto 0)))(rob_index_size) = '1' then
				value1 := '1' & x"0000000" & reg_rsv(conv_integer(value1(5 downto 0)))((rob_index_size - 1) downto 0);
				
				if is_real(rob(conv_integer(value1(3 downto 0)))) = '1' then
					value1 := rob(conv_integer(value1(3 downto 0)));
				else
					value1 := retrive_cdb(dec_in.cdb, value1);
				end if;
			else
				if flg_reg2 = '1' then
					value1 := '0' & (reg2(conv_integer(value1(5 downto 0))) xor x"ffffffff");
				else
					value1 := '0' & reg1(conv_integer(value1(5 downto 0)));
				end if;
			end if;
		end if;
		return value1;
	end instantiate_value;
	
	impure function is_forbidden (data : decoded_data; spec_tag : spec_tag_t; inst_id : integer) return std_logic is
	variable ret : std_logic;
	begin
		ret := data.forbidden;
		if inst_id = 1 then
			if data.op_alu = '1' and (dec_in.alu1_acceptable = '0' and dec_in.alu2_acceptable = '0') then
				ret := '1';
			end if;
		end if;
		if inst_id = 2 then
			if data.op_alu = '1' and dec_in.alu2_acceptable = '0' then
				ret := '1';
			end if;
		end if;
		if data.op_fadd = '1' and dec_in.fadd_acceptable = '0' then
			ret := '1';
		end if;
		if data.op_fmul = '1' and dec_in.fmul_acceptable = '0' then
			ret := '1';
		end if;
		if data.op_finv = '1' and dec_in.finv_acceptable = '0' then
			ret := '1';
		end if;
		if data.op_fsqrt = '1' and dec_in.fsqrt_acceptable = '0' then
			ret := '1';
		end if;
		if data.op_out = '1' and dec_in.ounit_acceptable = '0' then
			ret := '1';
		end if;
		if data.op_mem = '1' and dec_in.mem_acceptable = '0' then
			ret := '1';
		end if;
		if data.op_jmp = '1' and (dec_in.jmp_acceptable = '0' or spec_tag = "11") then
			ret := '1';
		end if;
		return ret;
	end is_forbidden;
begin
	process (r, dec_in, reg1, reg2, reg_rsv, rob, rob_target, rob_spec)
		variable v : reg_type;
		
		variable addr_enable : std_logic;
		variable branch_addr : std_logic_vector(19 downto 0);
		
		variable is_input : std_logic;
		
		variable decode_result1, decode_result2 : decoded_data;
	begin
		v := r;
		
		v.rsv_reg1 := "000000";
		v.rsv_dest1 := "0000";
		v.rsv_spec1 := "00";
		v.rsv_reg2 := "000000";
		v.rsv_dest2 := "0000";
		v.rsv_spec2 := "00";
		v.save_target := spec_size;
		v.spec_success_id := spec_size;
		v.spec_addr := dec_in.speculate_addr;
		
		is_input := '0';
		
		decode_result1 := decode_instruction(dec_in.instruction1, dec_in.pc, r.pred_success_count, r.pred_failure_count);
		decode_result2 := decode_instruction(dec_in.instruction2, dec_in.pc + x"00001", r.pred_success_count, r.pred_failure_count);
		
		v.pred_history := dec_in.pred_history;
		v.pred_previous_value := dec_in.pred_previous_value;
		v.call_stack_idx := dec_in.call_stack_idx;
		
		if decode_result1.op_in = '1' then
			decode_result1.value1 := '0' & x"000000" & dec_in.input_data;
			is_input := '1';
		end if;
		if decode_result1.op_halt = '1' then
			v.is_halt := '1';
		else
			v.is_halt := '0';
		end if;
		
		-- translate virtual values (if possible)
		--if decode_result1.value1(32) = '1' then
		--	if reg_rsv(conv_integer(decode_result1.value1(5 downto 0)))(rob_index_size) = '1' then
		--		decode_result1.value1 := '1' & x"0000000" & reg_rsv(conv_integer(decode_result1.value1(5 downto 0)))((rob_index_size - 1) downto 0);
		--		
		--		if is_real(rob(conv_integer(decode_result1.value1(3 downto 0)))) = '1' then
		--			decode_result1.value1 := rob(conv_integer(decode_result1.value1(3 downto 0)));
		--		else
		--			decode_result1.value1 := retrive_cdb(dec_in.cdb, decode_result1.value1);
		--		end if;
		--	else
		--		decode_result1.value1 := '0' & reg1(conv_integer(decode_result1.value1(5 downto 0)));
		--	end if;
		--end if;
		decode_result1.value1 := instantiate_value(decode_result1.value1, '0');
		decode_result1.value2 := instantiate_value(decode_result1.value2, '0');

		-- handle dependency
		if decode_result1.rsv_reg_enable = '1' and decode_result1.rsv_reg /= "000000" and decode_result2.value1(32) = '1' and decode_result2.value1((register_index_size - 1) downto 0) = decode_result1.rsv_reg then
			decode_result2.value1 := '1' & x"0000000" & v.rend;
		else
			decode_result2.value1 := instantiate_value(decode_result2.value1, '1');
		end if;
		if decode_result1.rsv_reg_enable = '1' and decode_result1.rsv_reg /= "000000" and decode_result2.value2(32) = '1' and decode_result2.value2((register_index_size - 1) downto 0) = decode_result1.rsv_reg then
			decode_result2.value2 := '1' & x"0000000" & v.rend;
		else
			decode_result2.value2 := instantiate_value(decode_result2.value2, '1');
		end if;

		-- executable?
		decode_result1.forbidden := (not dec_in.inst_enable1) or is_forbidden(decode_result1, v.spec_tag, 1);
		if is_input = '1' and dec_in.input_queue_empty = '1' then
			decode_result1.forbidden := '1';
		end if;
		if v.wait_empty_rob = '1' then
			decode_result1.forbidden := '1';
		end if;
		if (is_input = '1' or v.is_halt = '1' or decode_result1.op_out = '1') and v.spec_tag /= "00" then
			-- super tenuki
			decode_result1.forbidden := '1';
		end if;
		
		decode_result2.forbidden := (not dec_in.inst_enable2) or is_forbidden(decode_result2, v.spec_tag, 2);
		if decode_result1.op_alu = '1' and decode_result2.op_alu = '1' and dec_in.alu1_acceptable = '0' then
			decode_result2.forbidden := '1';
		end if;
		if decode_result1.op_in = '1' or decode_result1.op_out = '1' or decode_result1.op_halt = '1' or decode_result1.op_jmp = '1' then
			decode_result2.forbidden := '1';
		end if;
		if decode_result2.op_in = '1' or decode_result2.op_out = '1' or decode_result2.op_halt = '1' or decode_result2.op_jmp = '1' or decode_result2.op_call = '1' then
			decode_result2.forbidden := '1';
		end if;
		if ((decode_result1.op_fadd and decode_result2.op_fadd) or (decode_result1.op_fmul and decode_result2.op_fmul)
			or (decode_result1.op_finv and decode_result2.op_finv) or (decode_result1.op_fsqrt and decode_result2.op_fsqrt) or (decode_result1.op_mem and decode_result2.op_mem)) = '1' then
			-- structual hazard
			decode_result2.forbidden := '1';
		end if;

		if dec_in.spec_failure = '1' then
			decode_result1.forbidden := '1';
			decode_result2.forbidden := '1';
			v.rend := v.rend_tmp(dec_in.spec_id);
			v.spec_tag := v.spec_tag_tmp(dec_in.spec_id);
			v.wait_empty_rob := '1';

			v.pred_failure_count := v.pred_failure_count + x"00000001";
		end if;
		
		if dec_in.spec_success = '1' then
			v.spec_success_id := dec_in.spec_id;
			v.spec_tag(dec_in.spec_id) := '0';
			for i in 0 to (spec_size - 1) loop
				v.spec_tag_tmp(i)(dec_in.spec_id) := '0';
			end loop;
			
			v.pred_success_count := v.pred_success_count + x"00000001";
		end if;
		
		-- check stall caused by full ROB
		if (v.rend + "0001") = v.rtop then
			decode_result1.forbidden := '1';
		end if;
		if (v.rend + "0010") = v.rtop then
			decode_result2.forbidden := '1';
		end if;
		
		decode_result2.forbidden := decode_result2.forbidden or decode_result1.forbidden;
		
		if decode_result1.forbidden = '1' then
			decode_result1.op_alu := '0';
			decode_result1.op_fadd := '0';
			decode_result1.op_fmul := '0';
			decode_result1.op_finv := '0';
			decode_result1.op_fsqrt := '0';
			decode_result1.op_out := '0';
			decode_result1.op_mem := '0';
			decode_result1.op_jmp := '0';
			v.is_halt := '0';
			is_input := '0';
		end if;
		if decode_result2.forbidden = '1' then
			decode_result2.op_alu := '0';
			decode_result2.op_fadd := '0';
			decode_result2.op_fmul := '0';
			decode_result2.op_finv := '0';
			decode_result2.op_fsqrt := '0';
			decode_result2.op_out := '0';
			decode_result2.op_mem := '0';
			decode_result2.op_jmp := '0';
		end if;
				
		addr_enable := '0';
		branch_addr := x"00000";
		
		dec_out.is_branch <= decode_result1.op_jmp;
		
		if decode_result1.op_jmp = '1' then
			if v.spec_tag(0) = '0' then
				v.save_target := 0;
			elsif v.spec_tag(1) = '0' then
				v.save_target := 1;
			end if;
			v.spec_tag_tmp(v.save_target) := v.spec_tag;
			v.spec_tag(v.save_target) := '1';
			v.rend_tmp(v.save_target) := v.rend;
		end if;
		
		assert rob(conv_integer(v.rtop)) = v.rob_top0 report "rob_top0 unmatch";
		
		-- register write back
		v.writeback1 := '0';
		if v.rtop /= v.rend then
			if is_real(v.rob_top0) = '1' and v.rob_spec_top0 = "00" then
				v.writeback1 := '1';
				v.reg_writeback_idx1 := v.rtop;
				v.reg_writeback_target1 := v.rob_target_top0;
				v.reg_writeback_value1 := get_value(v.rob_top0);
				
				if reg_rsv(conv_integer(v.reg_writeback_target1))((rob_index_size - 1) downto 0) = v.rtop
					and (decode_result1.forbidden = '1' or decode_result1.rsv_reg /= v.reg_writeback_target1)
					and (decode_result2.forbidden = '1' or decode_result2.rsv_reg /= v.reg_writeback_target1) then
					v.reg_writeback_upd_vt1 := '1';
				else
					v.reg_writeback_upd_vt1 := '0';
				end if;
			end if;
		end if;
		
		v.writeback2 := '0';
		if v.rtop + "0001" /= v.rend then
			if is_real(v.rob_top1) = '1' and v.rob_spec_top1 = "00" then
				v.writeback2 := '1';
				v.reg_writeback_idx2 := v.rtop + "0001";
				v.reg_writeback_target2 := v.rob_target_top1;
				v.reg_writeback_value2 := get_value(v.rob_top1);
				
				if reg_rsv(conv_integer(v.reg_writeback_target2))((rob_index_size - 1) downto 0) = v.rtop + "0001"
					and (decode_result1.forbidden = '1' or decode_result1.rsv_reg /= v.reg_writeback_target2)
					and (decode_result2.forbidden = '1' or decode_result2.rsv_reg /= v.reg_writeback_target2) then
					v.reg_writeback_upd_vt2 := '1';
				else
					v.reg_writeback_upd_vt2 := '0';
				end if;
			end if;
		end if;

		v.writeback2 := v.writeback1 and v.writeback2;
		if v.writeback2 = '1' then
			v.rtop := v.rtop + "0010";
		elsif v.writeback1 = '1' then
			v.rtop := v.rtop + "0001";
		end if;
		
		if v.writeback2 = '1' and v.reg_writeback_target1 = v.reg_writeback_target2 then
			v.writeback1 := '0';
		end if;
		
		v.clear_rsv := '0';
		if v.wait_empty_rob = '1' and v.rtop = v.rend then
			v.wait_empty_rob := '0';
			v.clear_rsv := '1';
		end if;

		-- for all kind of instructions which require register reservation
		v.reservation1 := dec_in.inst_enable1 and decode_result1.rsv_reg_enable and (not decode_result1.forbidden);
		if v.reservation1 = '1' then
			if decode_result1.rsv_reg = "000000" then
				v.reservation1 := '0';
				decode_result1.op_alu := '0';
				decode_result1.op_fadd := '0';
				decode_result1.op_fmul := '0';
				decode_result1.op_finv := '0';
				decode_result1.op_fsqrt := '0';
				decode_result1.op_mem := '0';
			else
				v.rsv_reg1 := decode_result1.rsv_reg;
				v.rsv_dest1 := v.rend;
				v.rend := v.rend + "0001";
			end if;
		end if;
		v.rsv_spec1 := v.spec_tag;

		v.reservation2 := dec_in.inst_enable2 and decode_result2.rsv_reg_enable and (not decode_result2.forbidden);
		if v.reservation2 = '1' then
			if decode_result2.rsv_reg = "000000" then
				v.reservation2 := '0';
				decode_result2.op_alu := '0';
				decode_result2.op_fadd := '0';
				decode_result2.op_fmul := '0';
				decode_result2.op_finv := '0';
				decode_result2.op_fsqrt := '0';
				decode_result2.op_mem := '0';
			else
				v.rsv_reg2 := decode_result2.rsv_reg;
				v.rsv_dest2 := v.rend;
				v.rend := v.rend + "0001";
			end if;
		end if;
		v.rsv_spec2 := v.spec_tag;
		
		if dec_in.alu1_acceptable = '1' then
			v.for_alu1 := (
				enable => decode_result1.op_alu, value1 => decode_result1.value1, value2 => decode_result1.value2,
				destination => v.rsv_dest1, operation => decode_result1.unit_op, spec_tag => v.rsv_spec1
			);

			v.for_alu2 := (
				enable => decode_result2.op_alu, value1 => decode_result2.value1, value2 => decode_result2.value2,
				destination => v.rsv_dest2, operation => decode_result2.unit_op, spec_tag => v.rsv_spec2
			);
		elsif decode_result1.op_alu = '1' then
			v.for_alu1 := (
				enable => '0', value1 => decode_result1.value1, value2 => decode_result1.value2,
				destination => v.rsv_dest1, operation => decode_result1.unit_op, spec_tag => v.rsv_spec1
			);

			v.for_alu2 := (
				enable => decode_result1.op_alu, value1 => decode_result1.value1, value2 => decode_result1.value2,
				destination => v.rsv_dest1, operation => decode_result1.unit_op, spec_tag => v.rsv_spec1
			);
		else
			v.for_alu1 := (
				enable => '0', value1 => decode_result1.value1, value2 => decode_result1.value2,
				destination => v.rsv_dest1, operation => decode_result1.unit_op, spec_tag => v.rsv_spec1
			);

			v.for_alu2 := (
				enable => decode_result2.op_alu, value1 => decode_result2.value1, value2 => decode_result2.value2,
				destination => v.rsv_dest2, operation => decode_result2.unit_op, spec_tag => v.rsv_spec2
			);
		end if;
		
		if decode_result1.op_fadd = '1' then
			v.for_fadd := (
				enable => decode_result1.op_fadd, value1 => decode_result1.value1, value2 => decode_result1.value2,
				destination => v.rsv_dest1, operation => decode_result1.unit_op, spec_tag => v.rsv_spec1
			);
		else
			v.for_fadd := (
				enable => decode_result2.op_fadd, value1 => decode_result2.value1, value2 => decode_result2.value2,
				destination => v.rsv_dest2, operation => decode_result2.unit_op, spec_tag => v.rsv_spec2
			);
		end if;
		
		if decode_result1.op_fmul = '1' then
			v.for_fmul := (
				enable => decode_result1.op_fmul, value1 => decode_result1.value1, value2 => decode_result1.value2,
				destination => v.rsv_dest1, operation => decode_result1.unit_op, spec_tag => v.rsv_spec1
			);
		else
			v.for_fmul := (
				enable => decode_result2.op_fmul, value1 => decode_result2.value1, value2 => decode_result2.value2,
				destination => v.rsv_dest2, operation => decode_result2.unit_op, spec_tag => v.rsv_spec2
			);
		end if;
		
		if decode_result1.op_finv = '1' then
			v.for_finv := (
				enable => decode_result1.op_finv, value1 => decode_result1.value1, value2 => decode_result1.value2,
				destination => v.rsv_dest1, operation => decode_result1.unit_op, spec_tag => v.rsv_spec1
			);
		else
			v.for_finv := (
				enable => decode_result2.op_finv, value1 => decode_result2.value1, value2 => decode_result2.value2,
				destination => v.rsv_dest2, operation => decode_result2.unit_op, spec_tag => v.rsv_spec2
			);
		end if;
		
		if decode_result1.op_fsqrt = '1' then
			v.for_fsqrt := (
				enable => decode_result1.op_fsqrt, value1 => decode_result1.value1, value2 => decode_result1.value2,
				destination => v.rsv_dest1, operation => decode_result1.unit_op, spec_tag => v.rsv_spec1
			);
		else
			v.for_fsqrt := (
				enable => decode_result2.op_fsqrt, value1 => decode_result2.value1, value2 => decode_result2.value2,
				destination => v.rsv_dest2, operation => decode_result2.unit_op, spec_tag => v.rsv_spec2
			);
		end if;
		
		v.for_out := (
			enable => decode_result1.op_out, value1 => decode_result1.value1
		);
		
		if decode_result1.op_mem = '1' then
			v.for_mem := (
				enable => decode_result1.op_mem, addr => decode_result1.value2, wvalue => decode_result1.value1,
				ofs => decode_result1.value3, destination => v.rsv_dest1, is_write => decode_result1.unit_op(0),
				spec_tag => v.rsv_spec1
			);
		else
			v.for_mem := (
				enable => decode_result2.op_mem, addr => decode_result2.value2, wvalue => decode_result2.value1,
				ofs => decode_result2.value3, destination => v.rsv_dest2, is_write => decode_result2.unit_op(0),
				spec_tag => v.rsv_spec2
			);
		end if;
		
		v.for_jmp := (
			enable => decode_result1.op_jmp, cond => decode_result1.value1, addr1 => decode_result1.value2, addr2 => decode_result1.value3, 
			spec_addr => v.spec_addr,
			spec_id => v.save_target,
			non_zero => decode_result1.unit_op(0),
			is_indirect => decode_result1.unit_op(1),
			spec_tag => v.rsv_spec1,
			pred_history => v.pred_history,
			pred_previous_value => v.pred_previous_value,
			call_stack_idx => v.call_stack_idx
		);
		
		v.rob_top0 := rob(conv_integer(v.rtop));
		v.rob_top1 := rob(conv_integer(v.rtop + "0001"));
		v.rob_spec_top0 := rob_spec(conv_integer(v.rtop));
		v.rob_spec_top1 := rob_spec(conv_integer(v.rtop + "0001"));
		v.rob_target_top0 := rob_target(conv_integer(v.rtop));
		v.rob_target_top1 := rob_target(conv_integer(v.rtop + "0001"));
		if v.spec_success_id /= spec_size then
			v.rob_spec_top0(v.spec_success_id) := '0';
			v.rob_spec_top1(v.spec_success_id) := '0';
		end if;
		
		if v.reservation1 = '1' then
			if v.rtop = v.rsv_dest1 then
				v.rob_top0 := '1' & x"00000000";
			end if;
			if (v.rtop + "0001") = v.rsv_dest1 then
				v.rob_top1 := '1' & x"00000000";
			end if;
		end if;
		if v.reservation2 = '1' then
			if v.rtop = v.rsv_dest2 then
				v.rob_top0 := '1' & x"00000000";
			end if;
			if (v.rtop + "0001") = v.rsv_dest2 then
				v.rob_top1 := '1' & x"00000000";
			end if;
		end if;
		if dec_in.cdb.enable(0) = '1' then
			if v.rtop = dec_in.cdb.destination(0) then
				v.rob_top0 := '0' & dec_in.cdb.data(0);
			end if;
			if (v.rtop + "0001") = dec_in.cdb.destination(0) then
				v.rob_top1 := '0' & dec_in.cdb.data(0);
			end if;
		end if;
		if dec_in.cdb.enable(1) = '1' then
			if v.rtop = dec_in.cdb.destination(1) then
				v.rob_top0 := '0' & dec_in.cdb.data(1);
			end if;
			if (v.rtop + "0001") = dec_in.cdb.destination(1) then
				v.rob_top1 := '0' & dec_in.cdb.data(1);
			end if;
		end if;		
		
		rin <= v;
		
		dec_out.alu1_out <= r.for_alu1;
		dec_out.alu2_out <= r.for_alu2;
		dec_out.fadd_out <= r.for_fadd;
		dec_out.fmul_out <= r.for_fmul;
		dec_out.finv_out <= r.for_finv;
		dec_out.fsqrt_out <= r.for_fsqrt;
		dec_out.mem_out <= r.for_mem;
		dec_out.jmp_out <= r.for_jmp;
		dec_out.ounit_out <= r.for_out;
		
		dec_out.is_halt <= v.is_halt;
--		dec_out.is_stall <= decode_result1.forbidden; -- v.stall and (not dec_in.spec_failure);
		dec_out.inst_accept1 <= not decode_result1.forbidden;
		dec_out.inst_accept2 <= not decode_result2.forbidden;
		dec_out.addr_enable <= addr_enable;
		dec_out.branch_addr <= branch_addr;
		dec_out.input_queue_pop <= is_input;
	end process;
	
	process (clk)
		variable cdb : cdb_t;
		variable target_reg : register_idx_t;
		
		variable rob_spec_tmp : rob_spec_t;
	begin
		if rising_edge(clk) then
			r <= rin;
			rob_spec_tmp := rob_spec;
			
			cdb := dec_in.cdb;
			
			if rin.spec_success_id /= spec_size then
				for i in 0 to (rob_size - 1) loop
					rob_spec_tmp(i)(rin.spec_success_id) := '0';
				end loop;
			end if;
			
			if rin.reservation1 = '1' then
				-- write to ROB (issue an instruction)
				rob(conv_integer(rin.rsv_dest1)) <= '1' & x"00000000";
				rob_target(conv_integer(rin.rsv_dest1)) <= rin.rsv_reg1;
				rob_spec_tmp(conv_integer(rin.rsv_dest1)) := rin.rsv_spec1;
				
				-- reserve register
				if rin.reservation2 = '0' or rin.rsv_reg1 /= rin.rsv_reg2 then
					reg_rsv(conv_integer(rin.rsv_reg1)) <= '1' & rin.rsv_dest1;
				end if;
			end if;
			if rin.reservation2 = '1' then
				-- write to ROB (issue an instruction)
				rob(conv_integer(rin.rsv_dest2)) <= '1' & x"00000000";
				rob_target(conv_integer(rin.rsv_dest2)) <= rin.rsv_reg2;
				rob_spec_tmp(conv_integer(rin.rsv_dest2)) := rin.rsv_spec2;
				
				-- reserve register
				reg_rsv(conv_integer(rin.rsv_reg2)) <= '1' & rin.rsv_dest2;
			end if;
			
			-- write back to register file
			if rin.writeback1 = '1' then
				reg1(conv_integer(rin.reg_writeback_target1)) <= rin.reg_writeback_value1;
				reg2(conv_integer(rin.reg_writeback_target1)) <= rin.reg_writeback_value1 xor x"ffffffff";
			end if;
			if rin.writeback2 = '1' then
				reg1(conv_integer(rin.reg_writeback_target2)) <= rin.reg_writeback_value2;
				reg2(conv_integer(rin.reg_writeback_target2)) <= rin.reg_writeback_value2 xor x"ffffffff";
			end if;
			
			if rin.clear_rsv = '1' then
				reg_rsv <= (others => "00000");
			else
				if rin.writeback1 = '1' and rin.reg_writeback_upd_vt1 = '1' then
					reg_rsv(conv_integer(rin.reg_writeback_target1)) <= "00000";
				end if;
				if rin.writeback2 = '1' and rin.reg_writeback_upd_vt2 = '1' then
					reg_rsv(conv_integer(rin.reg_writeback_target2)) <= "00000";
				end if;
			end if;
			
			-- write to ROB (completed instructions)
			if cdb.enable(0) = '1' then
				rob(conv_integer(cdb.destination(0))) <= '0' & cdb.data(0);
			end if;
			if cdb.enable(1) = '1' then
				rob(conv_integer(cdb.destination(1))) <= '0' & cdb.data(1);
			end if;
			
			rob_spec <= rob_spec_tmp;
		end if;
	end process;
end;
