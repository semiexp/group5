library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

use work.common.all;

entity JmpUnit is
	Port (
		clk : in std_logic;
		unit_in : in jmp_unit_in_t;
		unit_out : out jmp_unit_out_t
	 );
end JmpUnit;

-- addr2 is always (PC + 1) !!

architecture rtl of JmpUnit is
	type values_t is array(0 to (spec_size - 1)) of vtvalue_t;
	type spec_addr_t is array(0 to (spec_size - 1)) of std_logic_vector(19 downto 0);
	type spec_tags_t is array(0 to (spec_size - 1)) of spec_tag_t;
	type pred_histories_t is array(0 to (spec_size - 1)) of pred_history_t;
	type stack_idx_t is array(0 to (spec_size - 1)) of std_logic_vector((call_stack_width - 1) downto 0);
	type prev_values_t is array(0 to (spec_size - 1)) of std_logic_vector(1 downto 0);
	
	constant pred_writeback_buffer_size : integer := 4;
	type buffer_spec_t is array(0 to (pred_writeback_buffer_size - 1)) of spec_tag_t;
	type buffer_idx_t is array(0 to (pred_writeback_buffer_size - 1)) of std_logic_vector((pred_table_width - 1) downto 0);
	type buffer_value_t is  array(0 to (pred_writeback_buffer_size - 1)) of std_logic_vector(1 downto 0);
	subtype buffer_busy_t is std_logic_vector(0 to (pred_writeback_buffer_size - 1));

	type reg_type is record
		-- reservation station
		working : std_logic_vector(0 to (spec_size - 1));
		
		cond, addr1, addr2 : values_t;
		spec_addr : spec_addr_t;
		spec_tags : spec_tags_t;
		non_zero : std_logic_vector(0 to (spec_size - 1));
		indirect : std_logic_vector(0 to (spec_size - 1));
		pred_histories : pred_histories_t;
		stack_idx : stack_idx_t;
		prev_values : prev_values_t;
		
		-- buffer
		buffer_spec : buffer_spec_t;
		buffer_idx : buffer_idx_t;
		buffer_value : buffer_value_t;
		buffer_busy : buffer_busy_t;
		
		writeback_idx : std_logic_vector((pred_table_width - 1) downto 0);
		writeback_enable : std_logic;
		writeback_value : std_logic_vector(1 downto 0);
		
		-- in next clk
		spec_success, spec_failure : std_logic;
		spec_broadcast_id : spec_id_t;
		call_stack_idx : std_logic_vector((call_stack_width - 1) downto 0);
		branch_addr : std_logic_vector(19 downto 0);
		pred_history_rollback : pred_history_t;
		
		-- 2 clks after
		spec_success2, spec_failure2 : std_logic;
		spec_broadcast_id2 : spec_id_t;
	end record;
	
	constant rzero : reg_type := (
		working => (others => '0'),
		cond => (others => '0' & x"00000000"),
		addr1 => (others => '0' & x"00000000"),
		addr2 => (others => '0' & x"00000000"),
		spec_addr => (others => x"00000"),
		spec_tags => (others => "00"),
		non_zero => (others => '0'),
		indirect => (others => '0'),
		pred_histories => (others => (others => '0')),
		stack_idx => (others => (others => '0')),
		prev_values => (others => "00"),
		
		buffer_spec => (others => (others => '0')),
		buffer_idx => (others => (others => '0')),
		buffer_value => (others => "00"),
		buffer_busy => (others => '0'),
		
		writeback_idx => (others => '0'),
		writeback_value => "00",
		writeback_enable => '0',
		
		spec_success => '0',
		spec_failure => '0',
		spec_broadcast_id => spec_size,
		call_stack_idx => (others => '0'),
		branch_addr => (others => '0'),
		pred_history_rollback => (others => '0'),
		
		spec_success2 => '0',
		spec_failure2 => '0',
		spec_broadcast_id2 => spec_size
	);
	
	signal r, rin : reg_type := rzero;
	
begin
	process (r, unit_in)
		variable v : reg_type;
		
		variable branch_addr : std_logic_vector(19 downto 0);
		variable pred_history_rollback : pred_history_t;
		variable b_stack_idx : std_logic_vector((call_stack_width - 1) downto 0);
		variable b_indirect : std_logic;
		variable addr_enable : std_logic;
		variable complete_spec_tag : spec_tag_t;
		variable complete_pc : std_logic_vector(19 downto 0);
		variable complete_prev_value : std_logic_vector(1 downto 0);
		variable branch_taken : std_logic;
		
		variable i : spec_id_t;
	begin
		v := r;
		
		v.spec_success2 := v.spec_success;
		v.spec_failure2 := v.spec_failure;
		v.spec_broadcast_id2 := v.spec_broadcast_id;
		
		complete_pc := x"00000";
		
		-- process buffer
		for i in 0 to (pred_writeback_buffer_size - 1) loop
			if r.spec_success = '1' then
				v.buffer_spec(i)(r.spec_broadcast_id) := '0';
			end if;
			
			if r.spec_failure = '1' and v.buffer_spec(i)(r.spec_broadcast_id) = '1' then
				v.buffer_busy(i) := '0';
			end if;
		end loop;
		
		v.writeback_idx := (others => '0');
		v.writeback_value := "00";
		v.writeback_enable := '0';
		
		for i in 0 to (pred_writeback_buffer_size - 1) loop
			if r.buffer_spec(i) = "00" and v.buffer_busy(i) = '1' then
				v.buffer_busy(i) := '0';
				v.writeback_idx := v.buffer_idx(i);
				v.writeback_value := v.buffer_value(i);
				v.writeback_enable := '1';
				exit;
			end if;
		end loop;
		
		if unit_in.input.enable = '1' then
			i := unit_in.input.spec_id;
			
			v.working(i) := '1';
			v.cond(i) := unit_in.input.cond;
			v.addr1(i) := unit_in.input.addr1;
			v.addr2(i) := unit_in.input.addr2;
			v.spec_addr(i) := unit_in.input.spec_addr;
			v.spec_tags(i) := unit_in.input.spec_tag;
			v.non_zero(i) := unit_in.input.non_zero;
			v.indirect(i) := unit_in.input.is_indirect;
			v.pred_histories(i) := unit_in.input.pred_history;
			v.stack_idx(i) := unit_in.input.call_stack_idx;
			v.prev_values(i) := unit_in.input.pred_previous_value;
		end if;
		
		if v.working = "11" then
			unit_out.acceptable <= '0';
		else
			unit_out.acceptable <= '1';
		end if;
		
		for i in 0 to (spec_size - 1) loop
			if r.spec_success = '1' then
				v.spec_tags(i)(r.spec_broadcast_id) := '0';
			end if;
			
			if r.spec_failure = '1' and v.spec_tags(i)(r.spec_broadcast_id) = '1' then
				v.working(i) := '0';
			end if;

			if r.spec_success2 = '1' then
				v.spec_tags(i)(r.spec_broadcast_id2) := '0';
			end if;
			
			if r.spec_failure2 = '1' and v.spec_tags(i)(r.spec_broadcast_id2) = '1' then
				v.working(i) := '0';
			end if;
		end loop;
		
		for i in 0 to (spec_size - 1) loop
			v.cond(i) := retrive_cdb(unit_in.cdb, v.cond(i));
			v.addr1(i) := retrive_cdb(unit_in.cdb, v.addr1(i));
		end loop;
		
		branch_addr := x"00000";
		pred_history_rollback := (others => '0');
		complete_spec_tag := (others => '0');
		b_stack_idx := (others => '0');
		b_indirect := '0';
		addr_enable := '0';
		branch_taken := '0';
		complete_prev_value := "00";
		
		v.spec_success := '0';
		v.spec_failure := '0';
		v.spec_broadcast_id := spec_size;
		
		for i in 0 to (spec_size - 1) loop
			if v.working(i) = '1' and is_real(v.cond(i)) = '1' and is_real(v.addr1(i)) = '1' then
				if v.non_zero(i) = '0' then
					if get_value(v.cond(i)) = x"00000000" then
						branch_addr := v.addr1(i)(19 downto 0);
						branch_taken := '1';
					else
						branch_addr := v.addr2(i)(19 downto 0);
						branch_taken := '0';
					end if;
				else
					if get_value(v.cond(i)) = x"00000000" then
						branch_addr := v.addr2(i)(19 downto 0);
						branch_taken := '0';
					else
						branch_addr := v.addr1(i)(19 downto 0);
						branch_taken := '1';
					end if;
				end if;
				
				pred_history_rollback := v.pred_histories(i);
				b_stack_idx := v.stack_idx(i);
				b_indirect := v.indirect(i);
				complete_spec_tag := v.spec_tags(i);
				complete_pc := v.addr2(i)(19 downto 0) - x"00001";
				complete_prev_value := v.prev_values(i);
				
				if b_indirect = '1' and branch_taken = '1' then
					b_stack_idx := b_stack_idx - "0001";
				end if;
				
				addr_enable := '1';
				v.working(i) := '0';

				if branch_addr = v.spec_addr(i) then
					v.spec_failure := '0';
					v.spec_success := '1';
					v.spec_broadcast_id := i;
				else
					v.spec_failure := '1';
					v.spec_success := '0';
					v.spec_broadcast_id := i;
				end if;
				exit;
			end if;			
		end loop;
		
		if v.spec_failure = '1' or v.spec_success = '1' then
			for i in 0 to (pred_writeback_buffer_size - 1) loop
				if v.buffer_busy(i) = '0' then
					v.buffer_spec(i) := complete_spec_tag;
					v.buffer_spec(i)(v.spec_broadcast_id) := '0';
					v.buffer_idx(i) := pred_table_index(complete_pc, pred_history_rollback);
					v.buffer_value(i) := satulated_update(complete_prev_value, branch_taken);
					v.buffer_busy(i) := '1';
					exit;
				end if;
			end loop;
		end if;
		
		if b_indirect = '1' then
			v.pred_history_rollback := pred_history_rollback;
		else
			v.pred_history_rollback := branch_taken & pred_history_rollback((pred_history_size - 1) downto 1);
		end if;
		v.call_stack_idx := b_stack_idx;
		v.branch_addr := branch_addr;
		
		rin <= v;

		unit_out.addr_enable <= r.spec_failure;
		unit_out.call_stack_idx <= r.call_stack_idx;
		unit_out.branch_addr <= r.branch_addr;
		unit_out.pred_history_rollback <= r.pred_history_rollback;

		unit_out.spec_success <= r.spec_success;
		unit_out.spec_failure <= r.spec_failure;
		unit_out.spec_id <= r.spec_broadcast_id;
		
		unit_out.writeback_idx <= r.writeback_idx;
		unit_out.writeback_value <= r.writeback_value;
		unit_out.writeback_enable <= r.writeback_enable;
	end process;
	
	process (clk)
	begin
		if rising_edge(clk) then
			r <= rin;
		end if;
	end process;
end;
