library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

use work.common.all;

-- assume that write operations can be unconditionally executed	
entity MemoryController2 is
	Port (
		CLK : in  std_logic;
		
		unit_in : in memct_in_t;
		unit_out : out memct_out_t;
		
		XWA : out std_logic;
		ZD : inout std_logic_vector(31 downto 0);
		ZA : out std_logic_vector(19 downto 0)
	 );
end MemoryController2;

architecture struct of MemoryController2 is
	-- set associative
	constant cache_ways : integer := 4;
	constant cache_depth : integer := 10;
	constant cache_size : integer := 1024;
	
	type cache_tag_way_t is array(0 to (cache_size - 1)) of std_logic_vector((19 - cache_depth) downto 0);
	type cache_data_way_t is array(0 to (cache_size - 1)) of value_t;
	
	type cache_tag_t is array(0 to (cache_ways - 1)) of cache_tag_way_t;
	type cache_data_t is array(0 to (cache_ways - 1)) of cache_data_way_t;

	type cache_tag_fetched_t is array(0 to (cache_ways - 1)) of std_logic_vector((19 - cache_depth) downto 0);
	type cache_data_fetched_t is array(0 to (cache_ways - 1)) of value_t;

	type rd_addr_t is array(0 to 4) of std_logic_vector(19 downto 0);
	type spec_tags_t is array(0 to 4) of spec_tag_t;
	type dest_t is array(0 to 4) of rob_index_t;
	subtype cache_way_id_t is integer range 0 to cache_ways;
	
	type reg_type is record
		reading1, reading2, reading3 : std_logic;
		wr_data1, wr_data2, wr_data3 : value_t;
		fetching : std_logic_vector(0 to 4);
		prefetch : std_logic_vector(0 to 4);
		rd_addr : rd_addr_t;
		spec_tags : spec_tags_t;
		dest : dest_t;
		RR : cache_way_id_t;
		
	-- cache writeback
		write_way1, write_way2 : cache_way_id_t;
		write_index1, write_index2 : std_logic_vector((cache_depth - 1) downto 0);
		write_tag1, write_tag2 : std_logic_vector((19 - cache_depth) downto 0);
		write_data1, write_data2 : value_t;
		
	-- recent write buffer
		recent_val1, recent_val2, recent_val3, recent_val4 : value_t;
		recent_addr1, recent_addr2, recent_addr3, recent_addr4 : std_logic_vector(19 downto 0);
		
	-- recent write to cache (avoid inconsistency)
		recent_write_addr1, recent_write_addr2, recent_write_addr3 : std_logic_vector(19 downto 0);
		recent_write_enable1, recent_write_enable2, recent_write_enable3 : std_logic;

		recent_read_addr1, recent_read_addr2, recent_read_addr3, recent_read_addr4: std_logic_vector(19 downto 0);
		recent_read_enable1, recent_read_enable2, recent_read_enable3, recent_read_enable4 : std_logic;
	end record;
	
	constant rzero : reg_type := (
		reading1 => '1',
		reading2 => '1',
		reading3 => '1',
		wr_data1 => x"00000000",
		wr_data2 => x"00000000",
		wr_data3 => x"00000000",
		fetching => "00000",
		prefetch => "00000",
		rd_addr => (others => x"00000"),
		spec_tags => (others => "00"),
		dest => (others => "0000"),
		RR => 0,
		
		write_way1 => 4,
		write_way2 => 4,
		write_index1 => (others => '0'),
		write_index2 => (others => '0'),
		write_tag1 => (others => '0'),
		write_tag2 => (others => '0'),
		write_data1 => x"00000000",
		write_data2 => x"00000000",
		
		recent_val1 => x"00000000",
		recent_val2 => x"00000000",
		recent_val3 => x"00000000",
		recent_val4 => x"00000000",
		recent_addr1 => x"00000",
		recent_addr2 => x"00000",
		recent_addr3 => x"00000",
		recent_addr4 => x"00000",
		
		recent_write_addr1 => x"00000",
		recent_write_addr2 => x"00000",
		recent_write_addr3 => x"00000",
		recent_write_enable1 => '0',
		recent_write_enable2 => '0',
		recent_write_enable3 => '0',
		
		recent_read_addr1 => x"00000",
		recent_read_addr2 => x"00000",
		recent_read_addr3 => x"00000",
		recent_read_addr4 => x"00000",
		recent_read_enable1 => '0',
		recent_read_enable2 => '0',
		recent_read_enable3 => '0',
		recent_read_enable4 => '0'
	);
		
	signal ZD_latch : std_logic_vector(31 downto 0) := x"00000000";
	
	signal r, rin : reg_type := rzero;
	
	signal cache_tag : cache_tag_t := (
		(others => "0000000000"),
		(others => "0000000001"),
		(others => "0000000010"),
		(others => "0000000011")
	);
	signal cache_data : cache_data_t := (others => (others => x"00000000"));
	
	signal cache_tag_fetched, cache_tag_fetched_base : cache_tag_fetched_t;
	signal cache_data_fetched, cache_data_fetched_base : cache_data_fetched_t;
	
begin
	process (r, unit_in, ZD, ZD_latch, cache_tag, cache_data, cache_tag_fetched, cache_data_fetched, cache_tag_fetched_base, cache_data_fetched_base)
	variable tag : std_logic_vector((19 - cache_depth) downto 0);
	variable index : std_logic_vector((cache_depth - 1) downto 0);
	variable v : reg_type;
	
	variable cache_found : std_logic;
	variable cache_found_data : value_t;
	
	begin
		v := r;
		
		v.reading3 := v.reading2;
		v.reading2 := v.reading1;
		v.reading1 := not unit_in.write_enable;
		v.wr_data3 := v.wr_data2;
		v.wr_data2 := v.wr_data1;
		v.wr_data1 := unit_in.write_data;
		
		v.fetching(1 to 4) := v.fetching(0 to 3);
		v.fetching(0) := unit_in.read_enable;
		v.prefetch(1 to 4) := v.prefetch(0 to 3);
		v.prefetch(0) := unit_in.is_prefetch;
		v.spec_tags(1 to 4) := v.spec_tags(0 to 3);
		v.spec_tags(0) := unit_in.read_spec_tag;
		v.rd_addr(1 to 4) := v.rd_addr(0 to 3);
		v.rd_addr(0) := unit_in.address;
		v.dest(1 to 4) := v.dest(0 to 3);
		v.dest(0) := unit_in.read_destination;
		
		v.write_way1 := 4;
		v.write_index1 := (others => '0');
		v.write_tag1 := (others => '0');
		v.write_data1 := x"00000000";
		v.write_way2 := 4;
		v.write_index2 := (others => '0');
		v.write_tag2 := (others => '0');
		v.write_data2 := x"00000000";
		
		-- handle spec tags
		for i in 0 to 4 loop
			if unit_in.spec_success = '1' then
				v.spec_tags(i)(unit_in.spec_id) := '0';
			end if;
			if unit_in.spec_failure = '1' and v.spec_tags(i)(unit_in.spec_id) = '1' then
				v.fetching(i) := '0';
			end if;
		end loop;
		
		v.recent_write_addr3 := v.recent_write_addr2;
		v.recent_write_addr2 := v.recent_write_addr1;
		v.recent_write_addr1 := x"00000";
		v.recent_write_enable3 := v.recent_write_enable2;
		v.recent_write_enable2 := v.recent_write_enable1;
		v.recent_write_enable1 := '0';

		v.recent_read_addr4 := v.recent_read_addr3;
		v.recent_read_addr3 := v.recent_read_addr2;
		v.recent_read_addr2 := v.recent_read_addr1;
		v.recent_read_addr1 := x"00000";
		v.recent_read_enable4 := v.recent_read_enable3;
		v.recent_read_enable3 := v.recent_read_enable2;
		v.recent_read_enable2 := v.recent_read_enable1;
		v.recent_read_enable1 := '0';

		if v.reading2 = '0' then
			-- write
			-- check whether it should write to the cache
			
			for i in 0 to (cache_ways - 1) loop
				if cache_tag_fetched(i) = v.rd_addr(1)(19 downto cache_depth) then
					v.write_way2 := i;
					v.write_index2 := v.rd_addr(1)((cache_depth - 1) downto 0);
					v.write_tag2 := v.rd_addr(1)(19 downto cache_depth);
					v.write_data2 := v.wr_data2;
					exit;
				end if;
			end loop;
			
			v.recent_addr4 := v.recent_addr3;
			v.recent_val4 := v.recent_val3;
			v.recent_addr3 := v.recent_addr2;
			v.recent_val3 := v.recent_val2;
			v.recent_addr2 := v.recent_addr1;
			v.recent_val2 := v.recent_val1;
			v.recent_addr1 := v.rd_addr(1);
			v.recent_val1 := v.wr_data2;
			
			v.recent_write_addr1 := v.rd_addr(1);
			v.recent_write_enable1 := '1';
		end if;
		
		cache_found := '0';
		cache_found_data := x"00000000";
		
		if v.fetching(1) = '1' then
			if v.rd_addr(1) = v.recent_addr1 then
				cache_found := '1';
				cache_found_data := v.recent_val1;
			elsif v.rd_addr(1) = v.recent_addr2 then
				cache_found := '1';
				cache_found_data := v.recent_val2;
			elsif v.rd_addr(1) = v.recent_addr3 then
				cache_found := '1';
				cache_found_data := v.recent_val3;
			elsif v.rd_addr(1) = v.recent_addr4 then
				cache_found := '1';
				cache_found_data := v.recent_val4;
			else
				for i in 0 to 3 loop
					if v.rd_addr(1)(19 downto cache_depth) = cache_tag_fetched(i) then
						cache_found := '1';
						cache_found_data := cache_data_fetched(i);
						exit;
					end if;
				end loop;
			end if;		
		end if;
		
		if cache_found = '1' then
			v.fetching(1) := '0';
		end if;
		
		unit_out.output1.enable <= cache_found and (not v.prefetch(1));
		unit_out.output1.destination <= v.dest(1);
		unit_out.output1.data <= cache_found_data;
		
		if v.fetching(4) = '1' then
			-- broadcast[2]
			-- cache miss is happening, so write back to the cache
			-- way id is not important
			v.write_way1 := v.RR;
			if v.RR = (cache_ways - 1) then
				v.RR := 0;
			else
				v.RR := v.RR + 1;
			end if;
			
			if v.recent_write_enable1 = '1' and v.recent_write_addr1 = v.rd_addr(4) then
				v.write_way1 := cache_ways;
			end if;
			if v.recent_write_enable2 = '1' and v.recent_write_addr2 = v.rd_addr(4) then
				v.write_way1 := cache_ways;
			end if;
			if v.recent_write_enable3 = '1' and v.recent_write_addr3 = v.rd_addr(4) then
				v.write_way1 := cache_ways;
			end if;
			
			-- avoid duplicated element
			if v.recent_read_enable1 = '1' and v.recent_read_addr1 = v.rd_addr(4) then
				v.write_way1 := cache_ways;
			end if;
			if v.recent_read_enable2 = '1' and v.recent_read_addr2 = v.rd_addr(4) then
				v.write_way1 := cache_ways;
			end if;
			if v.recent_read_enable3 = '1' and v.recent_read_addr3 = v.rd_addr(4) then
				v.write_way1 := cache_ways;
			end if;
			if v.recent_read_enable4 = '1' and v.recent_read_addr4 = v.rd_addr(4) then
				v.write_way1 := cache_ways;
			end if;
			
			v.write_index1 := v.rd_addr(4)((cache_depth - 1) downto 0);
			v.write_tag1 := v.rd_addr(4)(19 downto cache_depth);
			v.write_data1 := ZD_latch;
			
			v.recent_read_addr1 := v.rd_addr(4);
			v.recent_read_enable1 := '1';
		else
		end if;
		
		unit_out.output2.enable <= v.fetching(3) and (not v.prefetch(3));
		unit_out.output2.destination <= v.dest(3);
		unit_out.output2.data <= ZD;
		
		if v.write_way1 = v.write_way2 and v.write_way1 /= 4 then
			-- avoid collision
			v.write_way1 := 3 - v.write_way1;
		end if;
		
		rin <= v;
		
		ZA <= r.rd_addr(0);
		XWA <= r.reading1;
		
		if r.reading3 = '1' then
			ZD <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
		else
			ZD <= r.wr_data3;
		end if;
		
		for i in 0 to (cache_ways - 1) loop
			if i = r.write_way1 and r.write_index1 = r.rd_addr(0)((cache_depth - 1) downto 0) then
				cache_tag_fetched(i) <= r.write_tag1;
				cache_data_fetched(i) <= r.write_data1;
			elsif i = r.write_way2 and r.write_index2 = r.rd_addr(0)((cache_depth - 1) downto 0) then
				cache_tag_fetched(i) <= r.write_tag2;
				cache_data_fetched(i) <= r.write_data2;
			else
				cache_tag_fetched(i) <= cache_tag_fetched_base(i);
				cache_data_fetched(i) <= cache_data_fetched_base(i);
			end if;
		end loop;
	end process;
	
	process(CLK)
	begin
		if rising_edge(CLK) then
			r <= rin;

			ZD_latch <= ZD;
			
			-- useful because rd_addr(0) stores the address even if the operation is write
			for i in 0 to (cache_ways - 1) loop
				cache_tag_fetched_base(i) <= cache_tag(i)(conv_integer(rin.rd_addr(0)((cache_depth - 1) downto 0)));
				cache_data_fetched_base(i) <= cache_data(i)(conv_integer(rin.rd_addr(0)((cache_depth - 1) downto 0)));
			end loop;
			
			-- write to cache
			for i in 0 to (cache_ways - 1) loop
				if i = rin.write_way1 then
					cache_tag(i)(conv_integer(rin.write_index1)) <= rin.write_tag1;
					cache_data(i)(conv_integer(rin.write_index1)) <= rin.write_data1;
				elsif i = rin.write_way2 then
					cache_tag(i)(conv_integer(rin.write_index2)) <= rin.write_tag2;
					cache_data(i)(conv_integer(rin.write_index2)) <= rin.write_data2;
				end if;
			end loop;
		end if;
	end process;
end;
