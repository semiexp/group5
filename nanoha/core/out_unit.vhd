library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

use work.common.all;

entity OutUnit is
	Port (
		clk : in std_logic;
		unit_in : in out_unit_in_t;
		unit_out : out out_unit_out_t
	 );
end OutUnit;

architecture rtl of OutUnit is
	type reg_type is record
		-- reservation station
		value1 : vtvalue_t;
		working : std_logic;
	end record;
	
	constant rzero : reg_type := (
		value1 => '0' & x"00000000",
		working => '0'
	);
	
	signal r, rin : reg_type := rzero;

begin
	process (r, unit_in)
		variable v : reg_type;
	begin
		v := r;
		
		unit_out.acceptable <= (not v.working) and (not unit_in.input.enable);
		
		if v.working = '1' then
			v.value1 := retrive_cdb(unit_in.cdb, v.value1);
		elsif unit_in.input.enable = '1' then
			v.value1 := retrive_cdb(unit_in.cdb, unit_in.input.value1);
			v.working := '1';
		end if;
		
		if v.working = '1' and is_real(v.value1) = '1' and unit_in.write_busy = '0' then
			unit_out.write_enable <= '1';
			unit_out.write_data <= v.value1(7 downto 0);
			v.working := '0';
		else
			unit_out.write_enable <= '0';
			unit_out.write_data <= "00000000";
		end if;
		
		rin <= v;
	end process;
	
	process (clk)
	begin
		if rising_edge(clk) then
			r <= rin;
		end if;
	end process;
end;
