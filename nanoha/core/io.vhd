library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity RS232C is
	generic (
		read_magic : std_logic_vector(15 downto 0) := x"0238";
		write_magic : std_logic_vector(15 downto 0) := x"0237"
	);
	Port (
		CLK : in  std_logic;
		RS_TX : out std_logic;
		RS_RX : in std_logic;

		-- FPGA -> PC
		write_busy : out std_logic;
		write_enable : in std_logic;
		write_data : in std_logic_vector(7 downto 0);
		
		-- PC -> FPGA
		read_empty : out std_logic;
		read_pop : in std_logic;
		read_data : out std_logic_vector(7 downto 0)
	 );
end RS232C;

architecture struct of RS232C is
	component fifo8
	Port (
	clk : in std_logic;
	wr_en : in std_logic;
	rd_en : in std_logic;
	din : in std_logic_vector(7 downto 0);
	dout : out std_logic_vector(7 downto 0);
	full : out std_logic;
	empty : out std_logic
	);
	end component;
	
	attribute RAM_STYLE : string;

	type write_fifo_t is array(0 to 4095) of std_logic_vector(7 downto 0);
	
	signal writing, write_awake: std_logic := '0';
	signal write_buf, fifo_v : std_logic_vector(7 downto 0) := x"00";
	signal write_fifo : write_fifo_t := (others => x"00");
	attribute RAM_STYLE of write_fifo : signal is "block";
	signal write_fifo_top, write_fifo_end : std_logic_vector(11 downto 0) := "000000000000";
	signal write_state : std_logic_vector(3 downto 0) := (others => '0');
	signal write_counter : std_logic_vector(15 downto 0) := (others => '0');
	signal output : std_logic := '1';
	
	signal reading : std_logic := '0';
	signal read_buf : std_logic_vector(7 downto 0) := (others => '0');
	signal read_state : std_logic_vector(3 downto 0) := (others => '0');
	signal read_counter : std_logic_vector(15 downto 0) := (others => '0');
	signal input : std_logic := '1';
	
	signal buf_wren, buf_rden, buf_full, buf_empty : std_logic := '0';
	signal buf_din, buf_dout : std_logic_vector(7 downto 0) := (others => '0');
	
begin
	write_busy <= '1' when (write_fifo_end + "000000000001" = write_fifo_top) else '0';
	RS_TX <= output;

	read_empty <= buf_empty;
	buf_rden <= read_pop;
	read_data <= buf_dout;
	
	in_buf : fifo8 port map (
		clk => CLK,
		wr_en => buf_wren,
		rd_en => buf_rden,
		din => buf_din,
		dout => buf_dout,
		full => buf_full,
		empty => buf_empty
	);

	process(CLK)
	begin
		if rising_edge(CLK) then
			-- write part
			if write_enable = '1' then
				write_fifo(conv_integer(write_fifo_end)) <= write_data;
				write_fifo_end <= write_fifo_end + "000000000001";
			end if;
			
			fifo_v <= write_fifo(conv_integer(write_fifo_top));
			if writing = '0' and write_awake = '0' and write_fifo_top /= write_fifo_end then
				write_fifo_top <= write_fifo_top + "000000000001";
				write_awake <= '1';
			end if;
			
			if write_awake = '1' then
				write_awake <= '0';
				
				write_buf <= fifo_v;
				writing <= '1';
				write_state <= "0000";
				write_counter <= write_magic;
			end if;
			
			if writing = '1' then
				if write_counter = x"0000" then
					write_state <= write_state + x"1";
					write_counter <= write_magic;
					
					if write_state = x"0" then
						output <= '0';
					elsif write_state = x"a" then
						output <= '1';
						writing <= '0';
					else
						output <= write_buf(0);
						write_buf <= '1' & write_buf(7 downto 1);
					end if;
				else
					write_counter <= write_counter - x"0001";
				end if;
			end if;
			
			-- read part
			input <= RS_RX; -- buffering		
			if reading = '1' and read_counter = x"0000" and read_state = x"8" then
				buf_wren <= '1';
				buf_din <= read_buf;
			else
				buf_wren <= '0';
			end if;
			
			if reading = '1' then
				if read_counter = x"0000" then
					read_state <= read_state + x"1";
					read_counter <= read_magic;
					
					if read_state = x"8" then
						reading <= '0';
					else
						read_buf <= input & read_buf(7 downto 1);
						read_state <= read_state + x"1";
					end if;
				else
					read_counter <= read_counter - x"0001";
				end if;
			elsif input = '0' then
				reading <= '1';
				read_state <= "0000";
				read_counter <= read_magic + ('0' & read_magic(15 downto 1));
			end if;
		end if;
	end process;
end;
