library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

use work.common.all;

entity FPUUnit is
	generic (
		use_fmul : in std_logic := '0';
		use_finv : in std_logic := '0';
		use_fsqrt : in std_logic := '0';
		rsv_size : in integer
	);
	Port (
		clk : in std_logic;
		unit_in : in afu_unit_in_t;
		unit_out : out afu_unit_out_t
	 );
end FPUUnit;

architecture rtl of FPUUnit is
	component FAdd is Port(
	input1 :  in STD_LOGIC_VECTOR (31 downto 0);
	input2 :  in STD_LOGIC_VECTOR (31 downto 0);
	output : out STD_LOGIC_VECTOR (31 downto 0);
	clk    :  in STD_LOGIC);
    end component;
	component FMul is Port(
	input1 :  in STD_LOGIC_VECTOR (31 downto 0);
	input2 :  in STD_LOGIC_VECTOR (31 downto 0);
	output : out STD_LOGIC_VECTOR (31 downto 0);
	clk    :  in STD_LOGIC);
    end component;
	component FInv is Port(
	input  :  in STD_LOGIC_VECTOR (31 downto 0);
	output : out STD_LOGIC_VECTOR (31 downto 0);
	clk    :  in STD_LOGIC);
    end component;
	component FSqrt is Port(
	input  :  in STD_LOGIC_VECTOR (31 downto 0);
	output : out STD_LOGIC_VECTOR (31 downto 0);
	clk    :  in STD_LOGIC);
    end component;
	
	type values_t is array(0 to (rsv_size - 1)) of vtvalue_t;
	type dest_t is array(0 to (rsv_size - 1)) of rob_index_t;
	subtype state_t is std_logic_vector(1 downto 0);
	type all_state_t is array(0 to (rsv_size - 1)) of state_t;
	type all_ops_t is array(0 to (rsv_size - 1)) of unit_op_t;
	type spec_tags_t is array(0 to (rsv_size - 1)) of spec_tag_t;
	subtype rs_index is integer range 0 to rsv_size;
	
	constant rs_empty : state_t := "00";
	constant rs_waiting : state_t := "01";
	constant rs_running : state_t := "10";
	constant rs_completed : state_t := "11";
	
	type reg_type is record
		-- reservation station
		value1, value2 : values_t;
		dest : dest_t;
		ops : all_ops_t;
		spec_tags : spec_tags_t;
		status : all_state_t;
		
		running1, running2, running3 : rs_index;
		fpu_in1, fpu_in2 : value_t;
	end record;
	
	constant rzero : reg_type := (
		value1 => (others => '0' & x"00000000"),
		value2 => (others => '0' & x"00000000"),
		dest => (others => "0000"),
		ops => (others => "0000"),
		spec_tags => (others => "00"),
		status => (others => rs_empty),
		running1 => rsv_size,
		running2 => rsv_size,
		running3 => rsv_size,
		fpu_in1 => x"00000000",
		fpu_in2 => x"00000000"
	);
	
	signal r, rin : reg_type := rzero;
	signal input1, input2, output, output2, output_fadd, output_fmul, output_finv, output_fsqrt : std_logic_vector(31 downto 0) := (others => '0');
	
begin
	fadd_unit : FAdd 
		port map (
		input1 => input1,
		input2 => input2,
		output => output_fadd,
		clk => clk
		);
	fmul_unit : FMul 
		port map (
		input1 => input1,
		input2 => input2,
		output => output_fmul,
		clk => clk
		);
	finv_unit : FInv 
		port map (
		input => input1,
		output => output_finv,
		clk => clk
		);
	fsqrt_unit : FSqrt
		port map (
		input => input1,
		output => output_fsqrt,
		clk => clk
		);
	output <= 
		output_fmul when use_fmul = '1' else
		output_finv when use_finv = '1' else
		output_fsqrt when use_fsqrt = '1' else
		output_fadd;
	
	process (r, unit_in, output)
		variable v : reg_type;
		variable n_value1, n_value2 : vtvalue_t;
		variable n_status : state_t;
		variable n_storage : rs_index;
		variable empty_loc : integer;
		
		variable perform_target : rs_index;
		variable fpu_input1, fpu_input2 : value_t;
		
		variable output_enable : std_logic;
		variable output_index : rs_index;
		
		variable real_enable : std_logic;
	begin		
		v := r;
		
		n_storage := 0;
		empty_loc := 0;
		for i in 0 to (rsv_size - 1) loop
			if v.status(i) = rs_empty then
				n_storage := i;
				empty_loc := empty_loc + 1;
			end if;
		end loop;
		
		real_enable := unit_in.input.enable;
		if unit_in.spec_failure = '1' and unit_in.input.spec_tag(unit_in.spec_id) = '1' then
			real_enable := '0';
		end if;
		
		if real_enable = '0' then
			empty_loc := empty_loc + 1;
		end if;

		-- decide acceptance
		if empty_loc >= 2 then
			unit_out.acceptable <= '1';
		else
			unit_out.acceptable <= '0';
		end if;
		
		for i in 0 to (rsv_size - 1) loop
			v.value1(i) := retrive_cdb(unit_in.cdb, v.value1(i));
			v.value2(i) := retrive_cdb(unit_in.cdb, v.value2(i));
			if unit_in.spec_success = '1' then
				v.spec_tags(i)(unit_in.spec_id) := '0';
			end if;		
		end loop;
		
		for i in 0 to (rsv_size - 1) loop
			if unit_in.spec_failure = '1' and v.spec_tags(i)(unit_in.spec_id) = '1' then
				v.status(i) := rs_empty;
				if i = v.running1 then
					v.running1 := rsv_size;
				end if;
				if i = v.running2 then
					v.running2 := rsv_size;
				end if;
				if i = v.running3 then
					v.running3 := rsv_size;
				end if;
			end if;
		end loop;
		
		n_value1 := retrive_cdb(unit_in.cdb, unit_in.input.value1);
		n_value2 := retrive_cdb(unit_in.cdb, unit_in.input.value2);
		
		if real_enable = '1' then
			n_status := rs_waiting;
		else
			n_status := rs_empty;
		end if;
		
		fpu_input1 := x"00000000";
		fpu_input2 := x"00000000";
		perform_target := rsv_size;
		for i in 0 to (rsv_size - 1) loop
			if v.status(i) = rs_waiting and is_real(v.value1(i)) = '1' and is_real(v.value2(i)) = '1' then
				v.status(i) := rs_running;
				perform_target := i;
				fpu_input1 := (v.ops(i)(0) xor get_value(v.value1(i))(31)) & get_value(v.value1(i))(30 downto 0);
				fpu_input2 := get_value(v.value2(i));
				exit;
			end if;
		end loop;
		
		if perform_target = rsv_size and n_status = rs_waiting and is_real(n_value1) = '1' and is_real(n_value2) = '1' then
			n_status := rs_running;
			perform_target := n_storage;
			fpu_input1 := (unit_in.input.operation(0) xor get_value(n_value1)(31)) & get_value(n_value1)(30 downto 0);
			fpu_input2 := get_value(n_value2);
		end if;
		
		v.fpu_in1 := fpu_input1;
		v.fpu_in2 := fpu_input2;
		
		if use_fmul = '1' or use_finv = '1' then
			if v.running2 /= rsv_size then
				if v.status(v.running2) = rs_running then
					v.status(v.running2) := rs_completed;
					v.value1(v.running2) := '0' & output;
				end if;
			end if;
		elsif use_fsqrt = '1' then
			if v.running3 /= rsv_size then
				if v.status(v.running3) = rs_running then
					v.status(v.running3) := rs_completed;
					v.value1(v.running3) := '0' & output2;
				end if;
			end if;
		else
			if v.running3 /= rsv_size then
				if v.status(v.running3) = rs_running then
					v.status(v.running3) := rs_completed;
					v.value1(v.running3) := '0' & output;
				end if;
			end if;
		end if;
		
		output_enable := '0';
		output_index := 0;
		
		for i in 0 to (rsv_size - 1) loop
			if v.status(i) = rs_completed then
				output_enable := '1';
				output_index := i;
				exit;
			end if;
		end loop;
		
		if output_enable = '1' then
			unit_out.output.destination <= v.dest(output_index);
			unit_out.output.data <= get_value(v.value1(output_index));
			unit_out.output.enable <= '1';
		else
			unit_out.output.destination <= "0000";
			unit_out.output.data <= x"00000000";
			unit_out.output.enable <= '0';
		end if;
		
		if unit_in.accepted = '1' then
			v.status(output_index) := rs_empty;
		end if;
		
		if real_enable = '1' then
			v.status(n_storage) := n_status;
			v.value1(n_storage) := n_value1;
			v.value2(n_storage) := n_value2;
			v.dest(n_storage) := unit_in.input.destination;
			v.ops(n_storage) := unit_in.input.operation;
			v.spec_tags(n_storage) := unit_in.input.spec_tag;
			if unit_in.spec_success = '1' then
				v.spec_tags(n_storage)(unit_in.spec_id) := '0';
			end if;
		end if;
		
		v.running3 := v.running2;
		v.running2 := v.running1;
		v.running1 := perform_target;
		
		rin <= v;
		
		input1 <= r.fpu_in1;
		input2 <= r.fpu_in2;
	end process;
	
	process (clk)
	begin
		if rising_edge(clk) then
			r <= rin;
			output2 <= output;
		end if;
	end process;
end;
