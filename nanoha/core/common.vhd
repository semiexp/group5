library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

package common is
	constant n_register : integer := 64;
	constant register_index_size : integer := 6;
	constant rob_size : integer := 16;
	constant rob_index_size : integer := 4;
	constant cdb_width : integer := 2;
	constant spec_size : integer := 2;
	
	constant pred_history_size : integer := 15;
	constant pred_table_width : integer := 15;
	constant pred_table_size : integer := 32768;
	
	constant call_stack_size : integer := 16;
	constant call_stack_width : integer := 4;

	subtype instruction_t is std_logic_vector(31 downto 0);
	subtype value_t is std_logic_vector(31 downto 0);
	subtype vtvalue_t is std_logic_vector(32 downto 0); -- representing a value which may be virtual

	subtype register_idx_t is std_logic_vector((register_index_size - 1) downto 0);
	subtype rob_index_t is std_logic_vector((rob_index_size - 1) downto 0);
	subtype rob_index_with_vt_t is std_logic_vector(rob_index_size downto 0);
	subtype rob_location_t is integer range 0 to (rob_size - 1);
	
	subtype spec_tag_t is std_logic_vector((spec_size - 1) downto 0);
	subtype spec_id_t is integer range 0 to spec_size;
	subtype pred_history_t is std_logic_vector((pred_history_size - 1) downto 0);
	
	subtype unit_op_t is std_logic_vector(3 downto 0);
	constant alu_op_add    : unit_op_t := "0000";
	constant alu_op_sub    : unit_op_t := "0001";
	constant alu_op_rshift : unit_op_t := "0010";
	constant alu_op_lshift : unit_op_t := "0011";
	constant alu_op_cmpeq  : unit_op_t := "0100";
	constant alu_op_cmpgt  : unit_op_t := "0101";
	constant alu_op_cmplt  : unit_op_t := "0110";
	constant alu_op_cmpfeq : unit_op_t := "0111";
	constant alu_op_cmpfgt : unit_op_t := "1000";
	constant alu_op_fneg   : unit_op_t := "1001";
	constant alu_op_fabs   : unit_op_t := "1010";

	type register_t is array(0 to (n_register - 1)) of value_t;
	type register_rsv_t is array(0 to (n_register - 1)) of rob_index_with_vt_t;
	subtype register_is_virtual_t is std_logic_vector(0 to (n_register - 1));
	
	type rob_value_t is array(0 to (rob_size - 1)) of vtvalue_t;
	type rob_target_t is array(0 to (rob_size - 1)) of register_idx_t;
	
	type cdb_destination_t is array(0 to (cdb_width - 1)) of rob_index_t;
	type cdb_value_t is array(0 to (cdb_width - 1)) of value_t;
	subtype cdb_enable_t is std_logic_vector(0 to (cdb_width - 1));

	type call_stack_t is array(0 to (call_stack_size - 1)) of std_logic_vector(19 downto 0);
	
	type cdb_t is record
		destination : cdb_destination_t;
		data : cdb_value_t;
		enable : cdb_enable_t;
	end record;
	
	type ifetch_in_t is record
		cpu_running : std_logic;
		
		pmem_write_enable : std_logic;
		pmem_write_addr : std_logic_vector(14 downto 0);
		pmem_write_data : instruction_t;
		
		pred_write_enable : std_logic;
		pred_write_addr : std_logic_vector((pred_table_width - 1) downto 0);
		pred_write_data : std_logic_vector(1 downto 0);
		
		reset : std_logic;
		
		addr_enable : std_logic;
		branch_addr : std_logic_vector(19 downto 0);
		pred_history_rollback : pred_history_t;
	end record;
	
	type ifetch_out_t is record
		inst_enable1, inst_enable2 : std_logic;
		instruction1, instruction2 : instruction_t;
		decoder_pc : std_logic_vector(19 downto 0);
		decoder_pred_history : std_logic_vector((pred_history_size - 1) downto 0);
	end record;
	
	-- decoder -> alu / fpu
	type decoder_to_afu_unit_t is record
		enable : std_logic;
		value1, value2 : vtvalue_t;
		destination : rob_index_t;
		operation : unit_op_t;
		spec_tag : spec_tag_t;
	end record;
	
	constant decoder_to_afu_unit_zero : decoder_to_afu_unit_t := (
		enable => '0',
		value1 => (others => '0'),
		value2 => (others => '0'),
		destination => (others => '0'),
		operation => (others => '0'),
		spec_tag => (others => '0')
	);
	
	-- decoder -> memory unit
	type decoder_to_mem_unit_t is record
		enable : std_logic;
		addr, wvalue, ofs : vtvalue_t;
		destination : rob_index_t;
		is_write : std_logic;
		spec_tag : spec_tag_t;
	end record;
	
	constant decoder_to_mem_unit_zero : decoder_to_mem_unit_t := (
		enable => '0',
		addr => (others => '0'),
		wvalue => (others => '0'),
		ofs => (others => '0'),
		destination => (others => '0'),
		is_write => '0',
		spec_tag => (others => '0')
	);

	-- decoder -> jmp unit
	type decoder_to_jmp_unit_t is record
		enable : std_logic;
		cond, addr1, addr2 : vtvalue_t;
		spec_addr : std_logic_vector(19 downto 0);
		spec_id : spec_id_t;
		non_zero : std_logic;
		is_indirect : std_logic;
		spec_tag : spec_tag_t;
		pred_history : pred_history_t;
		pred_previous_value : std_logic_vector(1 downto 0);
		call_stack_idx : std_logic_vector((call_stack_width - 1) downto 0);
	end record;
	
	constant decoder_to_jmp_unit_zero : decoder_to_jmp_unit_t := (
		enable => '0',
		cond => (others => '0'),
		addr1 => (others => '0'),
		addr2 => (others => '0'),
		spec_addr => (others => '0'),
		spec_id => spec_size,
		non_zero => '0',
		is_indirect => '0',
		spec_tag => (others => '0'),
		pred_history => (others => '0'),
		pred_previous_value => (others => '0'),
		call_stack_idx => (others => '0')
	);

	-- decoder -> outunit
	type decoder_to_out_unit_t is record
		enable : std_logic;
		value1 : vtvalue_t;
	end record;
	
	constant decoder_to_out_unit_zero : decoder_to_out_unit_t := (
		enable => '0',
		value1 => (others => '0')
	);

	-- (any unit) -> arbiter
	type unit_to_arbiter_t is record
		destination : rob_index_t;
		data : value_t;
		enable : std_logic;
	end record;
	
	type decoder_in_t is record
		-- from instruction fetch
		inst_enable1, inst_enable2 : std_logic;
		instruction1, instruction2 : instruction_t;
		pc : std_logic_vector(19 downto 0);
		speculate_addr : std_logic_vector(19 downto 0);
		pred_history : pred_history_t;
		pred_previous_value : std_logic_vector(1 downto 0);
		call_stack_idx : std_logic_vector((call_stack_width - 1) downto 0);
		
		-- from each unit
		alu1_acceptable : std_logic;
		alu2_acceptable : std_logic;
		fadd_acceptable : std_logic;
		fmul_acceptable : std_logic;
		finv_acceptable : std_logic;
		fsqrt_acceptable : std_logic;
		mem_acceptable : std_logic;
		ounit_acceptable : std_logic;
		jmp_acceptable : std_logic;
		
		-- from CDB
		cdb : cdb_t;
		
		-- from I/O
		input_queue_empty : std_logic;
		input_data : std_logic_vector(7 downto 0);
		
		-- from jmp
		spec_success : std_logic;
		spec_failure : std_logic;
		spec_id : spec_id_t;
	end record;
	
	type decoder_out_t is record
		-- to each unit
		alu1_out : decoder_to_afu_unit_t;
		alu2_out : decoder_to_afu_unit_t;
		fadd_out : decoder_to_afu_unit_t;
		fmul_out : decoder_to_afu_unit_t;
		finv_out : decoder_to_afu_unit_t;
		fsqrt_out : decoder_to_afu_unit_t;
		mem_out : decoder_to_mem_unit_t;
		jmp_out : decoder_to_jmp_unit_t;
		ounit_out : decoder_to_out_unit_t;
		
		-- to instruction fetch
		is_halt : std_logic;
		-- is_stall : std_logic;
		inst_accept1, inst_accept2 : std_logic;
		is_branch : std_logic;
		
		addr_enable : std_logic;
		branch_addr : std_logic_vector(19 downto 0);
		
		-- to I/O
		input_queue_pop : std_logic;
	end record;
	
	type memct_in_t is record
		address : std_logic_vector(19 downto 0);
		
		write_enable : std_logic;
		write_data : std_logic_vector(31 downto 0);
		
		read_enable : std_logic;
		read_spec_tag : spec_tag_t;
		read_destination : rob_index_t;
		is_prefetch : std_logic;
	
		-- from jmp
		spec_success : std_logic;
		spec_failure : std_logic;
		spec_id : spec_id_t;
	end record;

	type memct_out_t is record
		output1, output2 : unit_to_arbiter_t;
	end record;

	type arbiter_in_t is record
		-- from each unit
		alu1_in : unit_to_arbiter_t;
		alu2_in : unit_to_arbiter_t;
		fadd_in : unit_to_arbiter_t;
		fmul_in : unit_to_arbiter_t;
		finv_in : unit_to_arbiter_t;
		fsqrt_in : unit_to_arbiter_t;
		mem1_in : unit_to_arbiter_t;
		mem2_in : unit_to_arbiter_t;
	end record;
	
	type arbiter_out_t is record
		alu1_accepted : std_logic;
		alu2_accepted : std_logic;
		fadd_accepted : std_logic;
		fmul_accepted : std_logic;
		finv_accepted : std_logic;
		fsqrt_accepted : std_logic;
		
		cdb : cdb_t;
	end record;
	
	type afu_unit_in_t is record
		-- from decoder
		input : decoder_to_afu_unit_t;
		
		-- from arbiter
		accepted : std_logic;
		
		-- from CDB
		cdb : cdb_t;

		-- from jmp
		spec_success : std_logic;
		spec_failure : std_logic;
		spec_id : spec_id_t;
	end record;
	
	type afu_unit_out_t is record
		-- to decoder
		acceptable : std_logic;
		
		-- to arbiter
		output : unit_to_arbiter_t;
	end record;
	
	type mem_unit_in_t is record
		-- from decoder
		input : decoder_to_mem_unit_t;
		
		-- from CDB
		cdb : cdb_t;

		-- from jmp
		spec_success : std_logic;
		spec_failure : std_logic;
		spec_id : spec_id_t;
	end record;
	
	type mem_unit_out_t is record
		-- to decoder
		acceptable : std_logic;
		
		-- to memory controller
		address : std_logic_vector(19 downto 0);
		write_enable, read_enable : std_logic;
		write_data : value_t;
		read_dest : rob_index_t;
		read_spec_tag : spec_tag_t;
		is_prefetch : std_logic;
	end record;
	
	type jmp_unit_in_t is record
		-- from decoder
		input : decoder_to_jmp_unit_t;
		
		-- from CDB
		cdb : cdb_t;
	end record;
	
	type jmp_unit_out_t is record
		-- to decoder
		acceptable : std_logic;
		
		spec_success : std_logic;
		spec_failure : std_logic;
		spec_id : spec_id_t;
		
		writeback_idx : std_logic_vector((pred_table_width - 1) downto 0);
		writeback_value : std_logic_vector(1 downto 0);
		writeback_enable : std_logic;
		
		-- to IF
		branch_addr : std_logic_vector(19 downto 0);
		pred_history_rollback : pred_history_t;
		call_stack_idx : std_logic_vector((call_stack_width - 1) downto 0);
		addr_enable : std_logic;
		
	end record;
	
	type out_unit_in_t is record
		-- from decoder
		input : decoder_to_out_unit_t;
		
		-- from CDB
		cdb : cdb_t;
		
		-- from I/O unit
		write_busy : std_logic;
	end record;
	
	type out_unit_out_t is record
		-- to decoder
		acceptable : std_logic;
		
		-- to I/O unit
		write_enable : std_logic;
		write_data : std_logic_vector(7 downto 0);
	end record;
	
	function retrive_cdb(cdb : cdb_t; v : vtvalue_t) return vtvalue_t;
	function is_virtual(v : vtvalue_t) return std_logic;
	function is_real(v : vtvalue_t) return std_logic;
	function get_value(v : vtvalue_t) return value_t;

	subtype piyo is std_logic_vector((pred_table_width - 1) downto 0);
	subtype hoge is std_logic_vector(1 downto 0);
	function pred_table_index(pc : std_logic_vector(19 downto 0); history : pred_history_t) return piyo;
	function satulated_update(prev : std_logic_vector(1 downto 0); taken : std_logic) return hoge;
	
	function perform_ALU(value1 : value_t; value2 : value_t; op : unit_op_t) return value_t;
end;

package body common is
	function retrive_cdb(cdb : cdb_t; v : vtvalue_t) return vtvalue_t is
		variable ret : vtvalue_t;
	begin
		ret := v;
		
		if v(32) = '1' then
			if v(3 downto 0) = cdb.destination(0) and cdb.enable(0) = '1' then
				ret := '0' & cdb.data(0);
			elsif v(3 downto 0) = cdb.destination(1) and cdb.enable(1) = '1' then
				ret := '0' & cdb.data(1);
			end if;
		end if;
		
		return ret;
	end retrive_cdb;

	function is_virtual(v : vtvalue_t) return std_logic is
	begin
		return v(32);
	end is_virtual;

	function is_real(v : vtvalue_t) return std_logic is
	begin
		return (not v(32));
	end is_real;

	function get_value(v : vtvalue_t) return value_t is
	begin
		return v(31 downto 0);
	end get_value;

	function pred_table_index(pc : std_logic_vector(19 downto 0); history : pred_history_t) return piyo is
	begin
		return pc(14 downto 0) xor history; --(pc(14 downto 4) xor history) & pc(3 downto 0);
	end pred_table_index;
	
	function satulated_update(prev : std_logic_vector(1 downto 0); taken : std_logic) return hoge is
	variable ret : std_logic_vector(1 downto 0);
	begin
		case prev & taken is
		when "000" => ret := "00";
		when "001" => ret := "01";
		when "010" => ret := "00";
		when "011" => ret := "10";
		when "100" => ret := "01";
		when "101" => ret := "11";
		when "110" => ret := "10";
		when "111" => ret := "11";
		when others => ret := "00";
		end case;
		return ret;
	end satulated_update;

	function perform_ALU(value1 : value_t; value2 : value_t; op : unit_op_t) return value_t is
		variable ret : value_t;
		variable neg_value2 : value_t;
		variable cmp_value : std_logic_vector(32 downto 0);
	begin
		ret := x"00000000";
		cmp_value := (value1(31) & value1) - (value2(31) & value2);
		
		case op is
		when alu_op_add =>
			ret := value1 + value2;
		when alu_op_sub =>
			ret := value2 - value1;
		when alu_op_lshift =>
			if value2(31) = '1' then
				neg_value2 := x"00000000" - value2;
				ret := shr(value1, neg_value2(4 downto 0));
			else
				ret := shl(value1, value2(4 downto 0));
			end if;
		when alu_op_rshift =>
			if value2(31) = '1' then
				neg_value2 := x"00000000" - value2;
				ret := shl(value1, neg_value2(4 downto 0));
			else
				ret := shr(value1, value2(4 downto 0));
			end if;
		when alu_op_cmpeq =>
			if value1 = value2 then
				ret := x"00000001";
			else
				ret := x"00000000";
			end if;
		when alu_op_cmpgt =>
			if cmp_value(32) = '0' and cmp_value /= '0' & x"00000000" then
				ret := x"00000001";
			else
				ret := x"00000000";
			end if;
		when alu_op_cmplt =>
			if cmp_value(32) = '1' then
				ret := x"00000001";
			else
				ret := x"00000000";
			end if;
		when alu_op_cmpfeq =>
			if value1 = value2 or (value1(30 downto 0) = "000" & x"0000000" and value2(30 downto 0) = "000" & x"0000000") then
				ret := x"00000001";
			else
				ret := x"00000000";
			end if;
		when alu_op_cmpfgt =>
			if value1(31) = '0' and value2(31) = '1' and (value1(30 downto 0) /= "000" & x"0000000" or value2(30 downto 0) /= "000" & x"0000000") then
				ret := x"00000001";
			elsif value1(31) = '0' and value2(31) = '0' and value1 > value2 then
				ret := x"00000001";
			elsif value1(31) = '1' and value2(31) = '1' and ('0' & value1(30 downto 0)) < ('0' & value2(30 downto 0)) then
				ret := x"00000001";
			else
				ret := x"00000000";
			end if;
		when alu_op_fneg =>
			ret := (not value1(31)) & value1(30 downto 0);
		when alu_op_fabs =>
			ret := '0' & value1(30 downto 0);
		when others =>
			ret := x"00000000";
		end case;
		
		return ret;
	end perform_ALU;
end common;

