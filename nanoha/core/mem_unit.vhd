library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

use work.common.all;

entity MemUnit is
	Port (
		clk : in std_logic;
		unit_in : in mem_unit_in_t;
		unit_out : out mem_unit_out_t
	 );
end MemUnit;

architecture rtl of MemUnit is
	constant rsv_size : integer := 6;
	
	type values_t is array(0 to (rsv_size - 1)) of vtvalue_t;
	type dest_t is array(0 to (rsv_size - 1)) of rob_index_t;
	subtype state_t is std_logic;
	subtype rs_index is integer range 0 to rsv_size;
	type spec_tags_t is array(0 to (rsv_size - 1)) of spec_tag_t;
	
	subtype prefetch_count_t is integer range 0 to 4;
	
	constant rs_waiting : state_t := '0';
	constant rs_running : state_t := '1';
	
	type reg_type is record
		-- reservation station
		addr, ofs, wvalue : values_t;
		dest : dest_t;
		is_write : std_logic_vector(0 to (rsv_size - 1));
		spec_tags : spec_tags_t;
		status : state_t;
		
		rs_size : rs_index;
		terminated : std_logic;
		
		prefetch_index : std_logic_vector(19 downto 0);
		prefetch_count : prefetch_count_t;
	end record;
	
	constant rzero : reg_type := (
		addr => (others => '0' & x"00000000"),
		ofs => (others => '0' & x"00000000"),
		wvalue => (others => '0' & x"00000000"),
		dest => (others => "0000"),
		is_write => (others => '0'),
		spec_tags => (others => "00"),
		status => rs_waiting,
		rs_size => 0,
		terminated => '0',
		
		prefetch_index => (others => '0'),
		prefetch_count => 0
	);
	
	signal r, rin : reg_type := rzero;
	
begin
	process (r, unit_in)
		variable v : reg_type;
		
		variable n_addr, n_wvalue : vtvalue_t;
		
		variable wenable, renable : std_logic;
		variable mc_addr : std_logic_vector(19 downto 0);
		variable wdata : value_t;
		variable rspec_tag : spec_tag_t;
		variable rdest : rob_index_t;
		
		variable rsv_pop : std_logic;
		
		variable output_enable : std_logic;
		variable output_dest : rob_index_t;
		variable output_data : value_t;
		
		variable trim_idx : rs_index;
		
		variable is_prefetch : std_logic;
	begin
		v := r;
		
		-- decide acceptance
		if v.rs_size <= (rsv_size - 2) or (v.rs_size = (rsv_size - 1) and unit_in.input.enable = '0') then
			unit_out.acceptable <= '1';
		else
			unit_out.acceptable <= '0';
		end if;
		
		for i in 0 to (rsv_size - 1) loop
			v.addr(i) := retrive_cdb(unit_in.cdb, v.addr(i));
			v.wvalue(i) := retrive_cdb(unit_in.cdb, v.wvalue(i));	
		end loop;
		
		if unit_in.input.enable = '1' and v.rs_size < rsv_size then
			v.addr(v.rs_size) := retrive_cdb(unit_in.cdb, unit_in.input.addr);
			v.wvalue(v.rs_size) := retrive_cdb(unit_in.cdb, unit_in.input.wvalue);
			v.ofs(v.rs_size) := unit_in.input.ofs;
			v.is_write(v.rs_size) := unit_in.input.is_write;
			v.dest(v.rs_size) := unit_in.input.destination;
			v.spec_tags(v.rs_size) := unit_in.input.spec_tag;
			v.rs_size := v.rs_size + 1;
		end if;
		
		for i in 0 to (rsv_size - 1) loop
			if unit_in.spec_success = '1' then
				v.spec_tags(i)(unit_in.spec_id) := '0';
			end if;
		end loop;
		
		-- erase failed ops
		trim_idx := 0;
		for i in 0 to (rsv_size - 1) loop
			if unit_in.spec_failure = '1' and v.spec_tags(i)(unit_in.spec_id) = '1' then
				exit;
			elsif i < v.rs_size then
			--	v.addr(trim_idx) := v.addr(i);
			--	v.wvalue(trim_idx) := v.wvalue(i);
			--	v.ofs(trim_idx) := v.ofs(i);
			--	v.is_write(trim_idx) := v.is_write(i);
			--	v.dest(trim_idx) := v.dest(i);
			--	v.spec_tags(trim_idx) := v.spec_tags(i);
				trim_idx := trim_idx + 1;
			end if;
		end loop;
		v.rs_size := trim_idx;
		
		wenable := '0';
		renable := '0';
		mc_addr := x"00000";
		wdata := x"00000000";
		rspec_tag := "00";
		rdest := "0000";
		is_prefetch := '0';
		
		rsv_pop := '0';
		if v.rs_size > 0 and (v.is_write(0) = '0' or v.spec_tags(0) = "00") and is_real(v.addr(0)) = '1' and is_real(v.wvalue(0)) = '1' and v.status = rs_waiting then
			if v.is_write(0) = '1' then
				-- perform write
				wenable := '1';
				mc_addr := v.addr(0)(19 downto 0) + v.ofs(0)(19 downto 0);
				wdata := get_value(v.wvalue(0));
				rsv_pop := '1';
			else
				renable := '1';
				mc_addr := v.addr(0)(19 downto 0) + v.ofs(0)(19 downto 0);
				rspec_tag := v.spec_tags(0);
				rdest := v.dest(0);
				rsv_pop := '1';
			end if;
		end if;
		
		if rsv_pop = '1' then
			for i in 0 to (rsv_size - 2) loop
				v.addr(i) := v.addr(i + 1);
				v.wvalue(i) := v.wvalue(i + 1);
				v.ofs(i) := v.ofs(i + 1);
				v.is_write(i) := v.is_write(i +  1);
				v.dest(i) := v.dest(i + 1);
				v.spec_tags(i) := v.spec_tags(i + 1);
			end loop;
			v.addr(rsv_size - 1) := '0' & x"00000000";
			v.wvalue(rsv_size - 1) := '0' & x"00000000";
			v.ofs(rsv_size - 1) := '0' & x"00000000";
			v.is_write(rsv_size - 1) := '0';
			v.dest(rsv_size - 1) := "0000";
			v.spec_tags(rsv_size - 1) := "00";
			if v.rs_size > 0 then
				v.rs_size := v.rs_size - 1;
			end if;
			v.terminated := '0';
		end if;
		
		if renable = '1' then
			v.prefetch_index := mc_addr;
			v.prefetch_count := 3;
		elsif wenable = '0' and renable = '0' and v.prefetch_count /= 0 then
			v.prefetch_index := v.prefetch_index + x"00001";
			v.prefetch_count := v.prefetch_count - 1;
			renable := '1';
			mc_addr := v.prefetch_index;
			is_prefetch := '1';
		end if;
		
		rin <= v;

		unit_out.write_enable <= wenable;
		unit_out.read_enable <= renable;
		unit_out.address <= mc_addr;
		unit_out.write_data <= wdata;
		unit_out.read_spec_tag <= rspec_tag;
		unit_out.read_dest <= rdest;
		unit_out.is_prefetch <= is_prefetch;
		
	end process;
	
	process (clk)
	begin
		if rising_edge(clk) then
			r <= rin;
		end if;
	end process;
end;
