library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_arith.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

use work.common.all;

entity RaisingHeart is
	generic (
		read_magic : std_logic_vector(15 downto 0) := x"02f0";
		write_magic : std_logic_vector(15 downto 0) := x"02ff";
		simulation_mode : std_logic := '0'
	);
	Port (
		MCLK1 : in  std_logic;
		RS_TX : out std_logic;
		RS_RX : in std_logic;

		XE1, E2A, XE3 : out std_logic;
		XGA, XZCKE, ADVA, XLBO, ZZA, XFT : out std_logic;
		XWA : out std_logic;
		XZBE : out std_logic_vector(3 downto 0);
		ZD : inout std_logic_vector(31 downto 0);
		ZA : out std_logic_vector(19 downto 0);
		ZCLKMA : out std_logic_vector(1 downto 0)
	 );
end RaisingHeart;

ARCHITECTURE struct OF RaisingHeart IS
	component clockacc
	port (
	CLKIN_IN        : in    std_logic; 
	RST_IN          : in    std_logic; 
	CLKFX_OUT       : out   std_logic; 
	CLKIN_IBUFG_OUT : out   std_logic; 
	CLK0_OUT        : out   std_logic; 
	LOCKED_OUT      : out   std_logic);
	end component;

	component RS232C is
	generic (
		read_magic : std_logic_vector(15 downto 0) := x"0238";
		write_magic : std_logic_vector(15 downto 0) := x"0237"
	);
	Port (
		CLK : in  std_logic;
		RS_TX : out std_logic;
		RS_RX : in std_logic;

		-- FPGA -> PC
		write_busy : out std_logic;
		write_enable : in std_logic;
		write_data : in std_logic_vector(7 downto 0);
		
		-- PC -> FPGA
		read_empty : out std_logic;
		read_pop : in std_logic;
		read_data : out std_logic_vector(7 downto 0)
	 );
	end component;

	component MemoryController2 is
	Port (
		CLK : in  std_logic;
		
		unit_in : in memct_in_t;
		unit_out : out memct_out_t;
		
		XWA : out std_logic;
		ZD : inout std_logic_vector(31 downto 0);
		ZA : out std_logic_vector(19 downto 0)
	 );
	end component;

	component Decoder is
	Port (
		clk : in std_logic;
		dec_in : in decoder_in_t;
		dec_out : out decoder_out_t
	);
	end component;

	component Arbiter is
	Port (
		clk : in std_logic;
		unit_in : in arbiter_in_t;
		unit_out : out arbiter_out_t
	);
	end component;
	
	component ALUUnit is
	Port (
		clk : in std_logic;
		unit_in : in afu_unit_in_t;
		unit_out : out afu_unit_out_t
	);
	end component;

	component MemUnit is
	Port (
		clk : in std_logic;
		unit_in : in mem_unit_in_t;
		unit_out : out mem_unit_out_t
	);
	end component;

	component JmpUnit is
	Port (
		clk : in std_logic;
		unit_in : in jmp_unit_in_t;
		unit_out : out jmp_unit_out_t
	);
	end component;

	component FPUUnit is
	generic (
		use_fmul : in std_logic;
		use_finv : in std_logic;
		use_fsqrt : in std_logic;
		rsv_size : in integer
	);
	Port (
		clk : in std_logic;
		unit_in : in afu_unit_in_t;
		unit_out : out afu_unit_out_t
	);
	end component;

	component OutUnit is
	Port (
		clk : in std_logic;
		unit_in : in out_unit_in_t;
		unit_out : out out_unit_out_t
	);
	end component;
	
-- I/O related variables
	signal wbusy, wenable, rempty, rpop, rpop_starter : std_logic := '0';
	signal wdata, rdata : std_logic_vector(7 downto 0);
	
-- Program Loader
	signal rd_mode : std_logic_vector(2 downto 0) := (others => '0');
	signal program_size : std_logic_vector(23 downto 0) := (others => '0');
	signal program_read_pos : std_logic_vector(23 downto 0) := (others => '0');
	signal program_code : std_logic_vector(23 downto 0) := (others => '0');

-- CPU variables
	signal cpu_awake_pre2, cpu_running, inst_enable1, inst_enable2, decoder_stall : std_logic := '0';
	signal cpu_awake_pre1 : std_logic := simulation_mode;
	signal pcounter, pcounter_plus_one : std_logic_vector(19 downto 0) := x"00000";
	signal decoder_pc : std_logic_vector(19 downto 0) := x"00000";
	signal instruction1, instruction2 : std_logic_vector(31 downto 0) := x"00000000";
	signal pmem_addr0, pmem_addr1 : std_logic_vector(13 downto 0) := (others => '0');
	
	type program_memory_sub_t is array(0 to 16383) of std_logic_vector(31 downto 0);
	type program_memory_t is array(0 to 1) of program_memory_sub_t;
	type pred_table_t is array(0 to (pred_table_size - 1)) of std_logic_vector(1 downto 0);
	
	impure function set_initial_sub (program_file : in string) return program_memory_t is
	type BIN is file of character;
	file RFile : BIN open read_mode is program_file;
	variable pmem : program_memory_t := (others => (others => x"00000000"));
	variable loc : std_logic_vector(14 downto 0);
	variable read_char : character;
	variable word : std_logic_vector(31 downto 0);
	begin
		-- ignore the header
		read(RFile, read_char);
		read(RFile, read_char);
		read(RFile, read_char);
		read(RFile, read_char);
		
		loc := (others => '0');
		while (endfile(RFile) = FALSE) loop
			read(RFile, read_char);
			word( 7 downto  0) := conv_std_logic_vector(character'pos(read_char), 8);
			read(RFile, read_char);
			word(15 downto  8) := conv_std_logic_vector(character'pos(read_char), 8);
			read(RFile, read_char);
			word(23 downto 16) := conv_std_logic_vector(character'pos(read_char), 8);
			read(RFile, read_char);
			word(31 downto 24) := conv_std_logic_vector(character'pos(read_char), 8);
			
			pmem(conv_integer(loc(0)))(conv_integer(loc(14 downto 1))) := word;
			loc := loc + "000000000000001";
		end loop;
		return pmem;
	end function;
	impure function set_initial	(RamFileName : in string) return program_memory_t is
	variable pmem : program_memory_t := (others => (others => x"00000000"));
	begin
		if simulation_mode = '1' then
			pmem := set_initial_sub(RamFileName);
		end if;
		return pmem;
	end function;

	signal program_memory : program_memory_t := set_initial("program.bin");
	signal pred_table : pred_table_t := (others => "01");
	
	signal acc_rst, acc_clock, acc_dummy1, acc_dummy2, acc_dummy3 : std_logic := '0';
	
	signal CDB : cdb_t;
	
	signal mc_in : memct_in_t;
	signal mc_out : memct_out_t;
	signal decoder_in : decoder_in_t;
	signal decoder_out : decoder_out_t;
	signal arbiter_in : arbiter_in_t;
	signal arbiter_out : arbiter_out_t;
	signal alu1_in, alu2_in, fadd_in, fmul_in, finv_in, fsqrt_in : afu_unit_in_t;
	signal alu1_out, alu2_out, fadd_out, fmul_out, finv_out, fsqrt_out : afu_unit_out_t;
	signal mem_in : mem_unit_in_t;
	signal mem_out : mem_unit_out_t;
	signal jmp_in : jmp_unit_in_t;
	signal jmp_out : jmp_unit_out_t;
	signal ounit_in : out_unit_in_t;
	signal ounit_out : out_unit_out_t;
	
-- branch variables
	signal is_branch, addr_enable, waiting_addr : std_logic := '0';
	signal spec_addr : std_logic_vector(19 downto 0) := x"00000";
	
	signal decoder_pred_history, pred_history : pred_history_t := (others => '0');
	signal decoder_pred_previous_value : std_logic_vector(1 downto 0) := "00";
	signal pred_table_addr : std_logic_vector((pred_table_width - 1) downto 0);
	signal decoder_call_stack_idx : std_logic_vector((call_stack_width - 1) downto 0) := (others => '0');
	
	constant call_stack_size : integer := 16;
	constant call_stack_width : integer := 4;
	type call_stack_t is array(0 to (call_stack_size - 1)) of std_logic_vector(19 downto 0);
	
	signal call_stack : call_stack_t := (others => x"00000");
	signal call_stack_idx : std_logic_vector((call_stack_width - 1) downto 0) := (others => '0');
	signal call_stack_top : std_logic_vector(19 downto 0) := (others => '0');

	--alias clock is MCLK1; -- for 66.66MHz
	alias clock is acc_clock; -- for overclock
BEGIN
	acc: clockacc port map (
		CLKIN_IN => MCLK1,
		RST_IN => acc_rst,
		CLKFX_OUT => acc_clock,
		CLKIN_IBUFG_OUT => acc_dummy1,
		CLK0_OUT => acc_dummy2,
		LOCKED_OUT => acc_dummy3
	);

	io_unit : RS232C
		generic map (
		read_magic => read_magic,
		write_magic => write_magic
		)
		PORT MAP (
		CLK => clock,
		RS_TX => RS_TX,
		RS_RX => RS_RX,
		write_busy => wbusy,
		write_enable => wenable,
		write_data => wdata,
		read_empty => rempty,
		read_pop => rpop,
		read_data => rdata);

	mem_unit : MemoryController2
		port map (
		CLK => clock,
		
		unit_in => mc_in,
		unit_out => mc_out,

		XWA => XWA,
		ZD => ZD,
		ZA => ZA
		);

	dec : Decoder
		port map (
		clk => clock,
		dec_in => decoder_in,
		dec_out => decoder_out
		);

	arb : Arbiter
		port map (
		clk => clock,
		unit_in => arbiter_in,
		unit_out => arbiter_out
		);

	alu1 : ALUUnit
		port map (
		clk => clock,
		unit_in => alu1_in,
		unit_out => alu1_out
		);

	alu2 : ALUUnit
		port map (
		clk => clock,
		unit_in => alu2_in,
		unit_out => alu2_out
		);

	fadd : FPUUnit
		generic map (
		use_fmul => '0',
		use_finv => '0',
		use_fsqrt => '0',
		rsv_size => 4
		)
		port map (
		clk => clock,
		unit_in => fadd_in,
		unit_out => fadd_out
		);

	fmul : FPUUnit
		generic map (
		use_fmul => '1',
		use_finv => '0',
		use_fsqrt => '0',
		rsv_size => 4
		)
		port map (
		clk => clock,
		unit_in => fmul_in,
		unit_out => fmul_out
		);

	finv : FPUUnit
		generic map (
		use_fmul => '0',
		use_finv => '1',
		use_fsqrt => '0',
		rsv_size => 4
		)
		port map (
		clk => clock,
		unit_in => finv_in,
		unit_out => finv_out
		);

	fsqrt : FPUUnit
		generic map (
		use_fmul => '0',
		use_finv => '0',
		use_fsqrt => '1',
		rsv_size => 4
		)
		port map (
		clk => clock,
		unit_in => fsqrt_in,
		unit_out => fsqrt_out
		);
	
	mem : MemUnit
		port map (
		clk => clock,
		unit_in => mem_in,
		unit_out => mem_out
		);

	jmp : JmpUnit
		port map (
		clk => clock,
		unit_in => jmp_in,
		unit_out => jmp_out
		);

	ounit : OutUnit
		port map (
		clk => clock,
		unit_in => ounit_in,
		unit_out => ounit_out
		);

	XE1 <= '0';
	E2A <= '1';
	XE3 <= '0';
	XGA <= '0';
	XZCKE <= '0';
	ADVA <= '0';
	XLBO <= '1';
	ZZA <= '0';
	XFT <= '1';
	XZBE <= "0000";
	ZCLKMA(0) <= clock;
	ZCLKMA(1) <= clock;
	
	decoder_in.inst_enable1 <= inst_enable1;
	decoder_in.inst_enable2 <= inst_enable2;
	decoder_in.instruction1 <= instruction1;
	decoder_in.instruction2 <= instruction2;
	decoder_in.pc <= decoder_pc;
	decoder_in.speculate_addr <= spec_addr;
	decoder_in.pred_history <= decoder_pred_history;
	decoder_in.pred_previous_value <= decoder_pred_previous_value;
	decoder_in.call_stack_idx <= decoder_call_stack_idx;
	decoder_in.cdb <= CDB;
	decoder_in.alu1_acceptable <= alu1_out.acceptable;
	decoder_in.alu2_acceptable <= alu2_out.acceptable;
	decoder_in.fadd_acceptable <= fadd_out.acceptable;
	decoder_in.fmul_acceptable <= fmul_out.acceptable;
	decoder_in.finv_acceptable <= finv_out.acceptable;
	decoder_in.fsqrt_acceptable <= fsqrt_out.acceptable;
	decoder_in.ounit_acceptable <= ounit_out.acceptable;
	decoder_in.mem_acceptable <= mem_out.acceptable;
	decoder_in.jmp_acceptable <= jmp_out.acceptable;
	decoder_in.input_queue_empty <= rempty;
	decoder_in.input_data <= rdata;
	decoder_in.spec_success <= jmp_out.spec_success;
	decoder_in.spec_failure <= jmp_out.spec_failure;
	decoder_in.spec_id <= jmp_out.spec_id;
	
	mc_in.address <= mem_out.address;
	mc_in.write_enable <= mem_out.write_enable;
	mc_in.read_enable <= mem_out.read_enable;
	mc_in.write_data <= mem_out.write_data;
	mc_in.read_spec_tag <= mem_out.read_spec_tag;
	mc_in.read_destination <= mem_out.read_dest;
	mc_in.is_prefetch <= mem_out.is_prefetch;
	
	mc_in.spec_success <= jmp_out.spec_success;
	mc_in.spec_failure <= jmp_out.spec_failure;
	mc_in.spec_id <= jmp_out.spec_id;

	alu1_in.input <= decoder_out.alu1_out;
	alu1_in.accepted <= arbiter_out.alu1_accepted;
	alu1_in.cdb <= CDB;
	alu1_in.spec_success <= jmp_out.spec_success;
	alu1_in.spec_failure <= jmp_out.spec_failure;
	alu1_in.spec_id <= jmp_out.spec_id;
	
	alu2_in.input <= decoder_out.alu2_out;
	alu2_in.accepted <= arbiter_out.alu2_accepted;
	alu2_in.cdb <= CDB;
	alu2_in.spec_success <= jmp_out.spec_success;
	alu2_in.spec_failure <= jmp_out.spec_failure;
	alu2_in.spec_id <= jmp_out.spec_id;

	fadd_in.input <= decoder_out.fadd_out;
	fadd_in.accepted <= arbiter_out.fadd_accepted;
	fadd_in.cdb <= CDB;
	fadd_in.spec_success <= jmp_out.spec_success;
	fadd_in.spec_failure <= jmp_out.spec_failure;
	fadd_in.spec_id <= jmp_out.spec_id;

	fmul_in.input <= decoder_out.fmul_out;
	fmul_in.accepted <= arbiter_out.fmul_accepted;
	fmul_in.cdb <= CDB;
	fmul_in.spec_success <= jmp_out.spec_success;
	fmul_in.spec_failure <= jmp_out.spec_failure;
	fmul_in.spec_id <= jmp_out.spec_id;

	finv_in.input <= decoder_out.finv_out;
	finv_in.accepted <= arbiter_out.finv_accepted;
	finv_in.cdb <= CDB;
	finv_in.spec_success <= jmp_out.spec_success;
	finv_in.spec_failure <= jmp_out.spec_failure;
	finv_in.spec_id <= jmp_out.spec_id;

	fsqrt_in.input <= decoder_out.fsqrt_out;
	fsqrt_in.accepted <= arbiter_out.fsqrt_accepted;
	fsqrt_in.cdb <= CDB;
	fsqrt_in.spec_success <= jmp_out.spec_success;
	fsqrt_in.spec_failure <= jmp_out.spec_failure;
	fsqrt_in.spec_id <= jmp_out.spec_id;

	mem_in.input <= decoder_out.mem_out;
	mem_in.cdb <= cdb;
	mem_in.spec_success <= jmp_out.spec_success;
	mem_in.spec_failure <= jmp_out.spec_failure;
	mem_in.spec_id <= jmp_out.spec_id;

	jmp_in.input <= decoder_out.jmp_out;
	jmp_in.cdb <= cdb;
	
	ounit_in.input <= decoder_out.ounit_out;
	ounit_in.cdb <= CDB;
	ounit_in.write_busy <= wbusy;
	
	wenable <= ounit_out.write_enable;
	wdata <= ounit_out.write_data;
	
	arbiter_in.alu1_in <= alu1_out.output;
	arbiter_in.alu2_in <= alu2_out.output;
	arbiter_in.fadd_in <= fadd_out.output;
	arbiter_in.fmul_in <= fmul_out.output;
	arbiter_in.finv_in <= finv_out.output;
	arbiter_in.fsqrt_in <= fsqrt_out.output;
	arbiter_in.mem1_in <= mc_out.output1;
	arbiter_in.mem2_in <= mc_out.output2;
	
	CDB <= arbiter_out.cdb;
	
	rpop <= rpop_starter or decoder_out.input_queue_pop;
	
	is_branch <= decoder_out.is_branch;
	addr_enable <= jmp_out.addr_enable or decoder_out.addr_enable;
	
	process(clock)
	variable inst_tmp1, inst_tmp2 : std_logic_vector(31 downto 0);
	variable predict_result : std_logic_vector(1 downto 0);
	
	variable pcounter_tmp : std_logic_vector(19 downto 0);
	variable pred_history_tmp : std_logic_vector((pred_history_size - 1) downto 0);
	
	begin
		if rising_edge(clock) then
			pcounter_tmp := pcounter;
			pred_history_tmp := pred_history;
			
			-- CPU starter
			if cpu_running = '0' then
				if rempty = '0' and rpop_starter = '0' and cpu_awake_pre1 = '0' then
					if rd_mode <= "010" then
						rd_mode <= rd_mode + 1;
						program_size <= rdata & program_size(23 downto 8);
					elsif rd_mode = "011" then
						rd_mode <= rd_mode + 1;
					elsif rd_mode = "100" then
						program_code(7 downto 0) <= rdata;
						rd_mode <= "101";
					elsif rd_mode = "101" then
						program_code(15 downto 8) <= rdata;
						rd_mode <= "110";
					elsif rd_mode = "110" then
						program_code(23 downto 16) <= rdata;
						rd_mode <= "111";
					elsif rd_mode = "111" then
						rd_mode <= "100";
						
						program_read_pos <= program_read_pos + 1;
						
						if program_read_pos(23 downto 15) = "000000000" then
							if program_read_pos(0) = '0' then
								program_memory(0)(conv_integer(program_read_pos(14 downto 1))) <= rdata & program_code(23 downto 0);
							else
								program_memory(1)(conv_integer(program_read_pos(14 downto 1))) <= rdata & program_code(23 downto 0);
							end if;
						end if;
						
						if program_read_pos + 1 = program_size then
							cpu_awake_pre1 <= '1';
							pcounter_tmp := x"00000";
						end if;
					end if;
					
					rpop_starter <= '1';
				else
					-- mem_reading <= '1';
					rpop_starter <= '0';
				end if;
			end if;
					
			if cpu_awake_pre1 = '1' then
				cpu_awake_pre1 <= '0';
				cpu_awake_pre2 <= '1';
			end if;
			if cpu_awake_pre2 = '1' then
				cpu_awake_pre2 <= '0';
				cpu_running <= '1';
			end if;

			predict_result := pred_table(conv_integer(pred_table_addr));
			-- predict_result := pred_table(conv_integer(pred_table_index(pcounter, pred_history)));
			
			-- instruction fetch stage
			if cpu_running = '1' and decoder_out.is_halt = '0' and jmp_out.addr_enable = '0' then
				if pcounter(0) = '0' then
					inst_tmp1 := program_memory(0)(conv_integer(pmem_addr0));
					inst_tmp2 := program_memory(1)(conv_integer(pmem_addr1));			
				else
					inst_tmp2 := program_memory(0)(conv_integer(pmem_addr0));
					inst_tmp1 := program_memory(1)(conv_integer(pmem_addr1));
				end if;
				
				if inst_enable1 = '0' or decoder_out.inst_accept2 = '1' or (decoder_out.inst_accept1 = '1' and inst_enable2 = '0') then
					-- N out of N instructions were issued
					-- check next 2 instructions
					inst_enable1 <= '1';
					if inst_tmp1(31 downto 29) = "110" or (inst_tmp2(31 downto 29) = "110" and inst_tmp2(31 downto 20) /= "110001000000" and inst_tmp2(31 downto 26) /= "110100") then
						inst_enable2 <= '0';
					else
						inst_enable2 <= '1';
					end if;
					
					if inst_tmp1(31 downto 26) = "110100" then
						-- call -> movi
						pcounter_tmp := inst_tmp1(19 downto 0);
						spec_addr <= inst_tmp1(19 downto 0);
						call_stack(conv_integer(call_stack_idx)) <= pcounter + x"00001";
						call_stack_top <= pcounter + x"00001";
						call_stack_idx <= call_stack_idx + "0001";
						instruction1 <= "010110" & inst_tmp1(25 downto 20) & (pcounter + x"00001"); -- movi
					elsif inst_tmp1(31 downto 20) = "110001000000" then
						-- jmpui (jmpzi r0) -> add r0, r0, r0
						pcounter_tmp := inst_tmp1(19 downto 0);
						spec_addr <= inst_tmp1(19 downto 0);
						instruction1 <= x"00000000"; -- add r0, r0, r0
					elsif inst_tmp1(31 downto 26) = "110001" or inst_tmp1(31 downto 26) = "110011" then
						-- conditional immediate branch
						-- branch prediction
						
						if predict_result(1) = '1' then
							-- taken
							pcounter_tmp := inst_tmp1(19 downto 0);
							spec_addr <= inst_tmp1(19 downto 0);
						else
							pcounter_tmp := pcounter + x"00001";
							spec_addr <= pcounter + x"00001";
						end if;
						
						pred_history_tmp := predict_result(1) & pred_history_tmp((pred_history_size - 1) downto 1);
						instruction1 <= inst_tmp1;
						decoder_pred_previous_value <= predict_result;
					elsif inst_tmp1(31 downto 26) = "110000" or inst_tmp1(31 downto 26) = "110010" then
						if inst_tmp1(31 downto 20) = "110000000000" then
							pcounter_tmp := call_stack_top; --call_stack(conv_integer(call_stack_idx - "0001"));
							call_stack_top <= call_stack(conv_integer(call_stack_idx - "0010"));
							spec_addr <= call_stack_top; --call_stack(conv_integer(call_stack_idx - "0001"));
							call_stack_idx <= call_stack_idx - "0001";
						else
							pcounter_tmp := pcounter + x"00001";
							spec_addr <= pcounter + x"00001";
						end if;
						instruction1 <= inst_tmp1;
					elsif inst_tmp2(31 downto 26) = "110100" then
						-- call -> movi (2nd instruction)
						pcounter_tmp := inst_tmp2(19 downto 0);
						spec_addr <= inst_tmp2(19 downto 0);
						call_stack(conv_integer(call_stack_idx)) <= pcounter + x"00002";
						call_stack_top <= pcounter + x"00002";
						call_stack_idx <= call_stack_idx + "0001";
						instruction1 <= inst_tmp1;
						instruction2 <= "010110" & inst_tmp2(25 downto 20) & (pcounter + x"00002"); -- movi
					elsif inst_tmp2(31 downto 20) = "110001000000" then
						-- jmpui (jmpzi r0) -> nop = add r0, r0, r0 (2nd instruction)
						pcounter_tmp := inst_tmp2(19 downto 0);
						spec_addr <= inst_tmp2(19 downto 0);
						instruction1 <= inst_tmp1;
						instruction2 <= x"00000000"; -- add r0, r0, r0
					elsif inst_tmp1(31 downto 29) = "110" or inst_tmp2(31 downto 29) = "110" then
						pcounter_tmp := pcounter + x"00001";
						spec_addr <= pcounter + x"00001";
						instruction1 <= inst_tmp1;
					else
						pcounter_tmp := pcounter + x"00002";
						spec_addr <= pcounter + x"00002";
						instruction1 <= inst_tmp1;
						instruction2 <= inst_tmp2;
					end if;

					decoder_pc <= pcounter;
					decoder_pred_history <= pred_history;
					decoder_call_stack_idx <= call_stack_idx;
				elsif decoder_out.inst_accept1 = '1' then
					-- 2 instructions were given but only 1 was issued
					-- instruction1 <= instruction2;
					instruction2 <= inst_tmp1;
					
					if instruction2(31 downto 26) = "110100" then
						-- call -> movi
						instruction1 <= "010110" & instruction2(25 downto 20) & pcounter; -- movi
					elsif instruction2(31 downto 20) = "110001000000" then
						-- jmpui (jmpzi r0) -> add r0, r0, r0
						instruction1 <= x"00000000"; -- add r0, r0, r0
					else
						instruction1 <= instruction2;
					end if;
					
					inst_enable1 <= '1';
					
					if inst_tmp1(31 downto 29) = "110" then
						inst_enable2 <= '0';
						pcounter_tmp := pcounter;
					else
						inst_enable2 <= '1';
						pcounter_tmp := pcounter + x"00001";
					end if;

					decoder_pc <= pcounter - x"00001";
					decoder_pred_history <= pred_history;
					decoder_call_stack_idx <= call_stack_idx;
				else
					-- do nothing
				end if;
			else
				inst_enable1 <= '0';
				inst_enable2 <= '0';
			end if;

			if jmp_out.addr_enable = '1' then
				pcounter_tmp := jmp_out.branch_addr;
				pred_history_tmp := jmp_out.pred_history_rollback;
				call_stack_idx <= jmp_out.call_stack_idx;
			end if;
			
			if decoder_out.is_halt = '1' then
				cpu_running <= '0';
				rd_mode <= "000";
				program_read_pos <= x"000000";
				pcounter_tmp := x"00000";
			end if;
			
			if jmp_out.writeback_enable = '1' then
				pred_table(conv_integer(jmp_out.writeback_idx)) <= jmp_out.writeback_value;
			end if;
			
			pcounter <= pcounter_tmp;
			pcounter_plus_one <= pcounter_tmp + x"00001";
			pred_history <= pred_history_tmp;
			pred_table_addr <= pred_table_index(pcounter_tmp, pred_history_tmp);
			
			if pcounter_tmp(0) = '0' then
				pmem_addr0 <= pcounter_tmp(14 downto 1);
				pmem_addr1 <= pcounter_tmp(14 downto 1);
			else
				pmem_addr0 <= pcounter_tmp(14 downto 1) + "00000000000001";
				pmem_addr1 <= pcounter_tmp(14 downto 1);
			end if;
		end if;
	end process;
END;
