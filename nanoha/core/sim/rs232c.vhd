library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

use ieee.std_logic_textio.all;
use std.textio.all;

entity RS232C_Unit is
	generic (
		cycle : TIME := 0.10417 ms
	);
	Port (
		RS_TX : in std_logic;
		RS_RX : out std_logic
	);

end RS232C_Unit;

architecture RTL of RS232C_Unit is
	type BIN is file of character;
	file RFile : BIN open read_mode is "asm.out";
	file WFile : BIN open write_mode is "output.txt";

	signal cur : std_logic_vector(28 downto 0);
	signal aux : std_logic_vector(26 downto 2);
	signal sr : std_logic_vector(20 downto 0);
	signal counter : std_logic_vector(4 downto 0);
	
	type RAM is array (0 to 1048575) of std_logic_vector(31 downto 0);
	signal values : RAM;
	
	signal rd1, rd2 : std_logic := '0';
	signal adr1, adr2 : std_logic_vector(19 downto 0) := (others => '0');

begin
	process
	variable read_char : character;
	variable read_data : std_logic_vector(7 downto 0);
	
	begin
		-- write
		RS_RX <= '1';
		while (endfile(RFile) = FALSE) loop
			read(RFile, read_char);
			read_data := conv_std_logic_vector(character'pos(read_char), 8);
			-- baud rate : 9600
			RS_RX <= '0';
			wait for cycle;
			RS_RX <= read_data(0);
			wait for cycle;
			RS_RX <= read_data(1);
			wait for cycle;
			RS_RX <= read_data(2);
			wait for cycle;
			RS_RX <= read_data(3);
			wait for cycle;
			RS_RX <= read_data(4);
			wait for cycle;
			RS_RX <= read_data(5);
			wait for cycle;
			RS_RX <= read_data(6);
			wait for cycle;
			RS_RX <= read_data(7);
			wait for cycle;
			RS_RX <= '1';
			wait for cycle;
		end loop;
		wait;
	end process;
	
	process
	variable write_char : character;
	variable write_data : std_logic_vector(7 downto 0);
	
	begin
		-- write
		wait until RS_TX'event and RS_TX = '0';
		
		wait for cycle;
		wait for cycle * 0.5;
		
		write_data(0) := RS_TX; wait for cycle;
		write_data(1) := RS_TX; wait for cycle;
		write_data(2) := RS_TX; wait for cycle;
		write_data(3) := RS_TX; wait for cycle;
		write_data(4) := RS_TX; wait for cycle;
		write_data(5) := RS_TX; wait for cycle;
		write_data(6) := RS_TX; wait for cycle;
		write_data(7) := RS_TX; wait for cycle;
		
		write_char := character'val(conv_integer(write_data));
		write(WFile, write_char);
	end process;
end RTL;

