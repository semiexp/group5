LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.all;

ENTITY CPUSim IS
END CPUSim;

ARCHITECTURE testbench OF CPUSim IS
	COMPONENT RaisingHeart
	generic (
		read_magic : std_logic_vector(15 downto 0);
		write_magic : std_logic_vector(15 downto 0);
		simulation_mode : std_logic
	);
	Port (
		MCLK1 : in  std_logic;
		RS_TX : out std_logic;
		RS_RX : in std_logic;

		XE1, E2A, XE3 : out std_logic;
		XGA, XZCKE, ADVA, XLBO, ZZA, XFT : out std_logic;
		XWA : out std_logic;
		XZBE : out std_logic_vector(3 downto 0);
		ZD : inout std_logic_vector(31 downto 0);
		ZA : out std_logic_vector(19 downto 0);
		ZCLKMA : out std_logic_vector(1 downto 0)
	);
	END COMPONENT;

	COMPONENT SRAM_Model
	Port (
		MCLK1 : in std_logic;
		XE1, E2A, XE3 : in std_logic;
		XGA, XZCKE, ADVA, XLBO, ZZA, XFT : in std_logic;
		XWA : in std_logic;
		XZBE : in std_logic_vector(3 downto 0);
		ZD : inout std_logic_vector(31 downto 0);
		ZA : in std_logic_vector(19 downto 0);
		ZCLKMA : in std_logic_vector(1 downto 0)
	);
	END COMPONENT;

	COMPONENT RS232C_Unit
	generic (
		cycle : TIME
	);
	Port (
		RS_TX : in std_logic;
		RS_RX : out std_logic
	);
	end component;
	
  --Inputs
	signal tb_MCLK1, tb_RS_TX, tb_RS_RX, tb_XE1, tb_E2A, tb_XE3, tb_XGA, tb_XZCKE, tb_ADVA, tb_XLBO, tb_ZZA, tb_XFT, tb_XWA : std_logic;
	signal tb_XZBE : std_logic_vector(3 downto 0);
	signal tb_ZD : std_logic_vector(31 downto 0);
	signal tb_ZA : std_logic_vector(19 downto 0);
	signal tb_ZCLKMA : std_logic_vector(1 downto 0);

  --Simulation
	signal simclk : std_logic := '0';
BEGIN
	cpucore : RaisingHeart
		generic map (
		read_magic => x"0013",
		write_magic => x"0013",
		simulation_mode => '1'
		)
		PORT MAP (
		MCLK1 => simclk,
		RS_TX => tb_RS_TX,
		RS_RX => tb_RS_RX,
		XE1 => tb_XE1,
		E2A => tb_E2A,
		XE3 => tb_XE3,
		XGA => tb_XGA,
		XZCKE => tb_XZCKE,
		ADVA => tb_ADVA,
		XLBO => tb_XLBO,
		ZZA => tb_ZZA,
		XFT => tb_XFT,
		XWA => tb_XWA,
		XZBE => tb_XZBE,
		ZD => tb_ZD,
		ZA => tb_ZA,
		ZCLKMA => tb_ZCLKMA);

	sram : SRAM_Model PORT MAP (
		MCLK1 => simclk,
		XE1 => tb_XE1,
		E2A => tb_E2A,
		XE3 => tb_XE3,
		XGA => tb_XGA,
		XZCKE => tb_XZCKE,
		ADVA => tb_ADVA,
		XLBO => tb_XLBO,
		ZZA => tb_ZZA,
		XFT => tb_XFT,
		XWA => tb_XWA,
		XZBE => tb_XZBE,
		ZD => tb_ZD,
		ZA => tb_ZA,
		ZCLKMA => tb_ZCLKMA);

	rs232c : RS232C_Unit
		generic map (
		cycle => 300 ns
		)
		PORT MAP (
		RS_TX => tb_RS_TX,
		RS_RX => tb_RS_RX);

	-- generate clock for the simulation (200/3 MHz)
	clockgen : process
	begin
		simclk <= '0';
		wait for 7 ns;
		simclk <= '1';
		wait for 8 ns;
	end process;
END;
