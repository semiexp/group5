(* open MiniMLRuntime;; *)

(**************** グローバル変数の宣言 ****************)

(* オブジェクトの個数 *)
export let n_objects = create_array 1 0 in

(* オブジェクトのデータを入れるベクトル（最大60個）*)
export let objects = 
  let dummy = create_array 0 0.0 in
  create_array 60 (0, 0, 0, 0, dummy, dummy, false, dummy, dummy, dummy, dummy) in

(* Screen の中心座標 *)
export let screen = create_array 3 0.0 in
(* 視点の座標 *)
export let viewpoint = create_array 3 0.0 in
(* 光源方向ベクトル (単位ベクトル) *)
export let light = create_array 3 0.0 in
(* 鏡面ハイライト強度 (標準=255) *)
export let beam = create_array 1 255.0 in
(* AND ネットワークを保持 *)
export let and_net = create_array 50 (create_array 1 (-1)) in
(* OR ネットワークを保持 *)
export let or_net = create_array 1 (create_array 1 (and_net.(0))) in

(* 以下、交差判定ルーチンの返り値格納用 *)
(* solver の交点 の t の値 *)
export let solver_dist = create_array 1 0.0 in
(* 交点の直方体表面での方向 *)
export let intsec_rectside = create_array 1 0 in
(* 発見した交点の最小の t *)
export let tmin = create_array 1 (1000000000.0) in
(* 交点の座標 *)
export let intersection_point = create_array 3 0.0 in
(* 衝突したオブジェクト番号 *)
export let intersected_object_id = create_array 1 0 in
(* 法線ベクトル *)
export let nvector = create_array 3 0.0 in
(* 交点の色 *)
export let texture_color = create_array 3 0.0 in

(* 計算中の間接受光強度を保持 *)
export let diffuse_ray = create_array 3 0.0 in
(* スクリーン上の点の明るさ *)
export let rgb = create_array 3 0.0 in

(* 画像サイズ *)
export let image_size = create_array 2 0 in
(* 画像の中心 = 画像サイズの半分 *)
export let image_center = create_array 2 0 in
(* 3次元上のピクセル間隔 *)
export let scan_pitch = create_array 1 0.0 in

(* judge_intersectionに与える光線始点 *)
export let startp = create_array 3 0.0 in
(* judge_intersection_fastに与える光線始点 *)
export let startp_fast = create_array 3 0.0 in

(* 画面上のx,y,z軸の3次元空間上の方向 *)
export let screenx_dir = create_array 3 0.0 in
export let screeny_dir = create_array 3 0.0 in
export let screenz_dir = create_array 3 0.0 in

(* 直接光追跡で使う光方向ベクトル *)
export let ptrace_dirvec  = create_array 3 0.0 in

(* 間接光サンプリングに使う方向ベクトル *)
export let dirvecs = 
  let dummyf = create_array 0 0.0 in
  let dummyff = create_array 0 dummyf in
  let dummy_vs = create_array 0 (dummyf, dummyff) in
  create_array 5 dummy_vs in

(* 光源光の前処理済み方向ベクトル *)
export let light_dirvec =
  let dummyf2 = create_array 0 0.0 in
  let v3 = create_array 3 0.0 in
  let consts = create_array 60 dummyf2 in
  (v3, consts) in

(* 鏡平面の反射情報 *)
export let reflections =
  let dummyf3 = create_array 0 0.0 in
  let dummyff3 = create_array 0 dummyf3 in
  let dummydv = (dummyf3, dummyff3) in
  create_array 180 (0, dummydv, 0.0) in

(* reflectionsの有効な要素数 *) 

export let n_reflections = create_array 1 0 in
  ()
