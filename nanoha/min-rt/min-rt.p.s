.data	
.globl	min_caml_n_objects
min_caml_n_objects:
.space	1
.globl	min_caml_objects
min_caml_objects:
.space	60
.globl	min_caml_screen
min_caml_screen:
.space	3
.globl	min_caml_viewpoint
min_caml_viewpoint:
.space	3
.globl	min_caml_light
min_caml_light:
.space	3
.globl	min_caml_beam
min_caml_beam:
.space	1
.globl	min_caml_and_net
min_caml_and_net:
.space	50
.globl	min_caml_or_net
min_caml_or_net:
.space	1
.globl	min_caml_solver_dist
min_caml_solver_dist:
.space	1
.globl	min_caml_intsec_rectside
min_caml_intsec_rectside:
.space	1
.globl	min_caml_tmin
min_caml_tmin:
.space	1
.globl	min_caml_intersection_point
min_caml_intersection_point:
.space	3
.globl	min_caml_intersected_object_id
min_caml_intersected_object_id:
.space	1
.globl	min_caml_nvector
min_caml_nvector:
.space	3
.globl	min_caml_texture_color
min_caml_texture_color:
.space	3
.globl	min_caml_diffuse_ray
min_caml_diffuse_ray:
.space	3
.globl	min_caml_rgb
min_caml_rgb:
.space	3
.globl	min_caml_image_size
min_caml_image_size:
.space	2
.globl	min_caml_image_center
min_caml_image_center:
.space	2
.globl	min_caml_scan_pitch
min_caml_scan_pitch:
.space	1
.globl	min_caml_startp
min_caml_startp:
.space	3
.globl	min_caml_startp_fast
min_caml_startp_fast:
.space	3
.globl	min_caml_screenx_dir
min_caml_screenx_dir:
.space	3
.globl	min_caml_screeny_dir
min_caml_screeny_dir:
.space	3
.globl	min_caml_screenz_dir
min_caml_screenz_dir:
.space	3
.globl	min_caml_ptrace_dirvec
min_caml_ptrace_dirvec:
.space	3
.globl	min_caml_dirvecs
min_caml_dirvecs:
.space	5
.globl	min_caml_light_dirvec
min_caml_light_dirvec:
.space	2
.globl	min_caml_reflections
min_caml_reflections:
.space	180
.globl	min_caml_n_reflections
min_caml_n_reflections:
.space	1
.text	
.globl	min_caml_finv
min_caml_lib_start:
	movhiz	%r62, 0x10		# 0
	movhiz	%r63, 0x06		# 1
.globl	min_caml_start.15784
.main	min_caml_start.15784
min_caml_start.15784:
	movi	%r3, 0		# 2
	movi	%r1, min_caml_n_objects		# 3
	movi	%r2, 1		# 4
	call	%r60, min_caml_create_array_here		# 5
	movi	%r2, 0		# 6
	movi	%r1, 0		# 7
	call	%r60, min_caml_create_float_array		# 8
	movi	%r2, 0		# 9
	movi	%r3, 0		# 10
	movi	%r4, 0		# 11
	movi	%r5, 0		# 12
	movi	%r6, 0		# 13
	mov	%r7, %r62		# 14
	addi	%r62, %r62, 11		# 15
	sto	%r1, 10(%r7)		# 16
	sto	%r1, 9(%r7)		# 17
	sto	%r1, 8(%r7)		# 18
	sto	%r1, 7(%r7)		# 19
	sto	%r6, 6(%r7)		# 20
	sto	%r1, 5(%r7)		# 21
	sto	%r1, 4(%r7)		# 22
	sto	%r5, 3(%r7)		# 23
	sto	%r4, 2(%r7)		# 24
	sto	%r3, 1(%r7)		# 25
	sto	%r2, (%r7)		# 26
	mov	%r3, %r7		# 27
	movi	%r1, min_caml_objects		# 28
	movi	%r2, 60		# 29
	call	%r60, min_caml_create_array_here		# 30
	movi	%r3, 0		# 31
	movi	%r1, min_caml_screen		# 32
	movi	%r2, 3		# 33
	call	%r60, min_caml_create_array_here		# 34
	movi	%r3, 0		# 35
	movi	%r1, min_caml_viewpoint		# 36
	movi	%r2, 3		# 37
	call	%r60, min_caml_create_array_here		# 38
	movi	%r3, 0		# 39
	movi	%r1, min_caml_light		# 40
	movi	%r2, 3		# 41
	call	%r60, min_caml_create_array_here		# 42
	movhiz	%r3, 276464		# 43
	movi	%r1, min_caml_beam		# 44
	movi	%r2, 1		# 45
	call	%r60, min_caml_create_array_here		# 46
	movi	%r2, -1		# 47
	movi	%r1, 1		# 48
	call	%r60, min_caml_create_array		# 49
	mov	%r3, %r1		# 50
	movi	%r1, min_caml_and_net		# 51
	movi	%r2, 50		# 52
	call	%r60, min_caml_create_array_here		# 53
	movi	%r1, min_caml_and_net		# 54
	addi	%r1, %r1, 0		# 55
	ld	%r2, (%r1)		# 56
	movi	%r1, 1		# 57
	call	%r60, min_caml_create_array		# 58
	mov	%r3, %r1		# 59
	movi	%r1, min_caml_or_net		# 60
	movi	%r2, 1		# 61
	call	%r60, min_caml_create_array_here		# 62
	movi	%r3, 0		# 63
	movi	%r1, min_caml_solver_dist		# 64
	movi	%r2, 1		# 65
	call	%r60, min_caml_create_array_here		# 66
	movi	%r3, 0		# 67
	movi	%r1, min_caml_intsec_rectside		# 68
	movi	%r2, 1		# 69
	call	%r60, min_caml_create_array_here		# 70
	movhiz	%r3, 321254		# 71
	addi	%r3, %r3, 2856		# 72
	movi	%r1, min_caml_tmin		# 73
	movi	%r2, 1		# 74
	call	%r60, min_caml_create_array_here		# 75
	movi	%r3, 0		# 76
	movi	%r1, min_caml_intersection_point		# 77
	movi	%r2, 3		# 78
	call	%r60, min_caml_create_array_here		# 79
	movi	%r3, 0		# 80
	movi	%r1, min_caml_intersected_object_id		# 81
	movi	%r2, 1		# 82
	call	%r60, min_caml_create_array_here		# 83
	movi	%r3, 0		# 84
	movi	%r1, min_caml_nvector		# 85
	movi	%r2, 3		# 86
	call	%r60, min_caml_create_array_here		# 87
	movi	%r3, 0		# 88
	movi	%r1, min_caml_texture_color		# 89
	movi	%r2, 3		# 90
	call	%r60, min_caml_create_array_here		# 91
	movi	%r3, 0		# 92
	movi	%r1, min_caml_diffuse_ray		# 93
	movi	%r2, 3		# 94
	call	%r60, min_caml_create_array_here		# 95
	movi	%r3, 0		# 96
	movi	%r1, min_caml_rgb		# 97
	movi	%r2, 3		# 98
	call	%r60, min_caml_create_array_here		# 99
	movi	%r3, 0		# 100
	movi	%r1, min_caml_image_size		# 101
	movi	%r2, 2		# 102
	call	%r60, min_caml_create_array_here		# 103
	movi	%r3, 0		# 104
	movi	%r1, min_caml_image_center		# 105
	movi	%r2, 2		# 106
	call	%r60, min_caml_create_array_here		# 107
	movi	%r3, 0		# 108
	movi	%r1, min_caml_scan_pitch		# 109
	movi	%r2, 1		# 110
	call	%r60, min_caml_create_array_here		# 111
	movi	%r3, 0		# 112
	movi	%r1, min_caml_startp		# 113
	movi	%r2, 3		# 114
	call	%r60, min_caml_create_array_here		# 115
	movi	%r3, 0		# 116
	movi	%r1, min_caml_startp_fast		# 117
	movi	%r2, 3		# 118
	call	%r60, min_caml_create_array_here		# 119
	movi	%r3, 0		# 120
	movi	%r1, min_caml_screenx_dir		# 121
	movi	%r2, 3		# 122
	call	%r60, min_caml_create_array_here		# 123
	movi	%r3, 0		# 124
	movi	%r1, min_caml_screeny_dir		# 125
	movi	%r2, 3		# 126
	call	%r60, min_caml_create_array_here		# 127
	movi	%r3, 0		# 128
	movi	%r1, min_caml_screenz_dir		# 129
	movi	%r2, 3		# 130
	call	%r60, min_caml_create_array_here		# 131
	movi	%r3, 0		# 132
	movi	%r1, min_caml_ptrace_dirvec		# 133
	movi	%r2, 3		# 134
	call	%r60, min_caml_create_array_here		# 135
	movi	%r2, 0		# 136
	movi	%r1, 0		# 137
	call	%r60, min_caml_create_float_array		# 138
	mov	%r2, %r1		# 139
	movi	%r1, 0		# 140
	sto	%r2, (%r63)		# 141
	addi	%r63, %r63, 1		# 142
	call	%r60, min_caml_create_array		# 143
	addi	%r63, %r63, -1		# 144
	mov	%r2, %r62		# 145
	addi	%r62, %r62, 2		# 146
	sto	%r1, 1(%r2)		# 147
	ld	%r1, (%r63)		# 148
	sto	%r1, (%r2)		# 149
	movi	%r1, 0		# 150
	addi	%r63, %r63, 1		# 151
	call	%r60, min_caml_create_array		# 152
	addi	%r63, %r63, -1		# 153
	mov	%r3, %r1		# 154
	movi	%r1, min_caml_dirvecs		# 155
	movi	%r2, 5		# 156
	addi	%r63, %r63, 1		# 157
	call	%r60, min_caml_create_array_here		# 158
	addi	%r63, %r63, -1		# 159
	movi	%r2, 0		# 160
	movi	%r1, 0		# 161
	addi	%r63, %r63, 1		# 162
	call	%r60, min_caml_create_float_array		# 163
	addi	%r63, %r63, -1		# 164
	movi	%r2, 0		# 165
	movi	%r3, 3		# 166
	sto	%r1, 1(%r63)		# 167
	mov	%r1, %r3		# 168
	addi	%r63, %r63, 2		# 169
	call	%r60, min_caml_create_float_array		# 170
	addi	%r63, %r63, -2		# 171
	movi	%r2, 60		# 172
	ld	%r3, 1(%r63)		# 173
	sto	%r1, 2(%r63)		# 174
	mov	%r1, %r2		# 175
	mov	%r2, %r3		# 176
	addi	%r63, %r63, 3		# 177
	call	%r60, min_caml_create_array		# 178
	addi	%r63, %r63, -3		# 179
	movi	%r2, min_caml_light_dirvec		# 180
	sto	%r1, 1(%r2)		# 181
	ld	%r1, 2(%r63)		# 182
	sto	%r1, (%r2)		# 183
	movi	%r2, 0		# 184
	movi	%r1, 0		# 185
	addi	%r63, %r63, 3		# 186
	call	%r60, min_caml_create_float_array		# 187
	addi	%r63, %r63, -3		# 188
	mov	%r2, %r1		# 189
	movi	%r1, 0		# 190
	sto	%r2, 3(%r63)		# 191
	addi	%r63, %r63, 4		# 192
	call	%r60, min_caml_create_array		# 193
	addi	%r63, %r63, -4		# 194
	mov	%r2, %r62		# 195
	addi	%r62, %r62, 2		# 196
	sto	%r1, 1(%r2)		# 197
	ld	%r1, 3(%r63)		# 198
	sto	%r1, (%r2)		# 199
	movi	%r1, 0		# 200
	movi	%r3, 0		# 201
	mov	%r4, %r62		# 202
	addi	%r62, %r62, 3		# 203
	sto	%r3, 2(%r4)		# 204
	sto	%r2, 1(%r4)		# 205
	sto	%r1, (%r4)		# 206
	mov	%r3, %r4		# 207
	movi	%r1, min_caml_reflections		# 208
	movi	%r2, 180		# 209
	addi	%r63, %r63, 4		# 210
	call	%r60, min_caml_create_array_here		# 211
	addi	%r63, %r63, -4		# 212
	movi	%r3, 0		# 213
	movi	%r1, min_caml_n_reflections		# 214
	movi	%r2, 1		# 215
	addi	%r63, %r63, 4		# 216
	call	%r60, min_caml_create_array_here		# 217
	addi	%r63, %r63, -4		# 218
	movi	%r1, 128		# 219
	movi	%r2, 128		# 220
	addi	%r63, %r63, 4		# 221
	call	%r60, rt.2943		# 222
	addi	%r63, %r63, -4		# 223
	halt			# 224
.globl	read_float3.6192
min_caml_finv:
	rshifti	r2, r1, 23		# 225
	lshifti	r3, r2, 23		# 226
	cmpeq	r4, r1, r3		# 227
	jmpzi	r4, finv_normal		# 228
	movhiz	r3, 0x7f000		# 229
	sub	r1, r3, r1		# 230
	jmpu	r60		# 231
finv_normal:
	sub	r4, r1, r3		# 232
	movhiz	r5, 0x3f800		# 233
	add	r5, r5, r4		# 234
	movhiz	r6, 0x40000		# 235
	movhiz	r7, 0x3f000		# 236
	fmul	r8, r5, r7		# 237
	fsub	r8, r6, r8		# 238
	fmul	r7, r7, r8		# 239
	fmul	r8, r5, r7		# 240
	fsub	r8, r6, r8		# 241
	fmul	r7, r7, r8		# 242
	fmul	r8, r5, r7		# 243
	fsub	r8, r6, r8		# 244
	fmul	r7, r7, r8		# 245
	fmul	r8, r5, r7		# 246
	fsub	r8, r6, r8		# 247
	fmul	r7, r7, r8		# 248
	fmul	r8, r5, r7		# 249
	fsub	r8, r6, r8		# 250
	fmul	r7, r7, r8		# 251
	lshifti	r7, r7, 9		# 252
	rshifti	r7, r7, 9		# 253
	movhiz	r4, 0x7e800		# 254
	sub	r1, r4, r3		# 255
	add	r1, r1, r7		# 256
	jmpu	r60		# 257
.global	min_caml_floor
min_caml_floor:
	fabs	%r2, %r1		# 258
	movhiz	%r3, 0x4b000		# 259
	cmpfgt	%r4, %r3, %r2		# 260
	jmpi	%r4, min_caml_floor.1		# 261
	jmpu	%r60		# 262
min_caml_floor.1:
	rshifti	%r4, %r2, 23		# 263
	lshifti	%r9, %r4, 23		# 264
	addi	%r6, %r4, -127		# 265
	cmplti	%r10, %r6, 0		# 266
	jmpi	%r10, min_caml_floor.4		# 267
	lshifti	%r5, %r2, 9		# 268
	rshifti	%r5, %r5, 9		# 269
	movi	%r7, 23		# 270
	sub	%r8, %r7, %r6		# 271
	rshift	%r5, %r5, %r8		# 272
	lshift	%r5, %r5, %r8		# 273
	add	%r2, %r9, %r5		# 274
	rshifti	%r8, %r1, 31		# 275
	lshifti	%r8, %r8, 31		# 276
	add	%r2, %r2, %r8		# 277
min_caml_floor.2:
	cmpfgt	%r3, %r2, %r1		# 278
	jmpzi	%r3, min_caml_floor.3		# 279
	movhiz	%r3, 0x3f800		# 280
	fsub	%r1, %r2, %r3		# 281
	jmpu	%r60		# 282
min_caml_floor.3:
	mov	%r1, %r2		# 283
	jmpu	%r60		# 284
min_caml_floor.4:
	movi	%r2, 0		# 285
	jmpui	min_caml_floor.2		# 286
.globl	min_caml_float_of_int
min_caml_float_of_int:
	jmpz	%r1, %r60		# 287
	rshifti	%r5, %r1, 31		# 288
	lshifti	%r5, %r5, 31		# 289
	cmpgti	%r2, %r1, 0		# 290
	jmpi	%r2, min_caml_float_of_int.1		# 291
	neg	%r1, %r1		# 292
min_caml_float_of_int.1:
	movhiz	%r2, 0x800		# 293
	cmplt	%r3, %r1, %r2		# 294
	jmpzi	%r3, min_caml_float_of_int.2		# 295
	movhiz	%r3, 0x4b000		# 296
	add	%r1, %r1, %r3		# 297
	fsub	%r1, %r1, %r3		# 298
	add	%r1, %r1, %r5		# 299
	jmpu	%r60		# 300
min_caml_float_of_int.2:
	movi	%r2, 2		# 301
	movi	%r3, 0		# 302
min_caml_float_of_int.3:
	cmplt	%r10, %r1, %r2		# 303
	jmpi	%r10, min_caml_float_of_int.4		# 304
	lshifti	%r2, %r2, 1		# 305
	addi	%r3, %r3, 1		# 306
	jmpui	min_caml_float_of_int.3		# 307
min_caml_float_of_int.4:
	addi	%r4, %r3, -32		# 308
	rshift	%r7, %r1, %r4		# 309
	rshifti	%r7, %r7, 9		# 310
	addi	%r6, %r3, 127		# 311
	lshifti	%r6, %r6, 23		# 312
	add	%r1, %r5, %r6		# 313
	add	%r1, %r1, %r7		# 314
	jmpu	%r60		# 315
.globl	min_caml_int_of_float
min_caml_int_of_float:
	sto	%r60, (%r63)		# 316
	addi	%r63, %r63, 1		# 317
	call	%r60, min_caml_floor		# 318
	addi	%r63, %r63, -1		# 319
	ld	%r60, (%r63)		# 320
	lshifti	%r2, %r1, 1		# 321
	rshifti	%r2, %r2, 24		# 322
	addi	%r2, %r2, -150		# 323
	lshifti	%r3, %r1, 9		# 324
	rshifti	%r3, %r3, 9		# 325
	movhiz	%r4, 0x800		# 326
	add	%r3, %r3, %r4		# 327
	cmpfgt	%r10, %r0, %r1		# 328
	jmpi	%r10, min_caml_int_of_float.1		# 329
	lshift	%r1, %r3, %r2		# 330
	jmpu	%r60		# 331
min_caml_int_of_float.1:
	lshift	%r2, %r3, %r2		# 332
	neg	%r1, %r2		# 333
	jmpu	%r60		# 334
.globl	min_caml_cos
min_caml_cos:
	fabs	%r1, %r1		# 335
	movi	%r2, 0		# 336
	movhiz	%r10, 0x40490		# 337
	addi	%r10, %r10, 0x0fdb		# 338
	movhiz	%r11, 0x40c90		# 339
	addi	%r11, %r11, 0x0fdb		# 340
	movhiz	%r12, 0x3fc90		# 341
	addi	%r12, %r12, 0x0fdb		# 342
	movhiz	%r13, 0x80000		# 343
min_caml_cos.1:
	cmpfgt	%r30, %r11, %r1		# 344
	jmpi	%r30, min_caml_cos.2		# 345
	fsub	%r1, %r1, %r11		# 346
	jmpui	min_caml_cos.1		# 347
min_caml_cos.2:
	cmpfgt	%r30, %r10, %r1		# 348
	jmpi	%r30, min_caml_cos.3		# 349
	fsub	%r1, %r1, %r10		# 350
	mov	%r2, %r13		# 351
min_caml_cos.3:
	cmpfgt	%r30, %r12, %r1		# 352
	jmpi	%r30, min_caml_cos.100		# 353
	add	%r2, %r2, %r13		# 354
	fsub	%r1, %r1, %r12		# 355
	jmpui	min_caml_sin.100		# 356
.globl	min_caml_sin
min_caml_sin:
	rshifti	%r2, %r1, 31		# 357
	lshifti	%r2, %r2, 31		# 358
	fabs	%r1, %r1		# 359
	movhiz	%r10, 0x40490		# 360
	addi	%r10, %r10, 0x0fdb		# 361
	movhiz	%r11, 0x40c90		# 362
	addi	%r11, %r11, 0x0fdb		# 363
	movhiz	%r12, 0x3fc90		# 364
	addi	%r12, %r12, 0x0fdb		# 365
	movhiz	%r13, 0x80000		# 366
min_caml_sin.1:
	cmpfgt	%r30, %r11, %r1		# 367
	jmpi	%r30, min_caml_sin.2		# 368
	fsub	%r1, %r1, %r11		# 369
	jmpui	min_caml_sin.1		# 370
min_caml_sin.2:
	cmpfgt	%r30, %r10, %r1		# 371
	jmpi	%r30, min_caml_sin.3		# 372
	fsub	%r1, %r1, %r10		# 373
	add	%r2, %r2, %r13		# 374
min_caml_sin.3:
	cmpfgt	%r30, %r12, %r1		# 375
	jmpi	%r30, min_caml_sin.100		# 376
	fsub	%r1, %r1, %r12		# 377
	jmpui	min_caml_cos.100		# 378
min_caml_sin.100:
	fmul	%r40, %r1, %r1		# 379
	fmul	%r3, %r40, %r1		# 380
	fmul	%r5, %r3, %r40		# 381
	fmul	%r7, %r5, %r40		# 382
	movhiz	%r4, 0xbe2a0		# 383
	movi	%r49, 0xaaac		# 384
	add	%r4, %r4, %r49		# 385
	fmul	%r4, %r4, %r3		# 386
	fadd	%r1, %r1, %r4		# 387
	movhiz	%r6, 0x3c080		# 388
	movi	%r49, 0x8666		# 389
	add	%r6, %r6, %r49		# 390
	fmul	%r6, %r6, %r5		# 391
	fadd	%r1, %r1, %r6		# 392
	movhiz	%r8, 0xb94d0		# 393
	movi	%r49, 0x64b6		# 394
	add	%r8, %r8, %r49		# 395
	fmul	%r8, %r8, %r7		# 396
	fadd	%r1, %r1, %r8		# 397
	add	%r1, %r1, %r2		# 398
	jmpu	%r60		# 399
min_caml_cos.100:
	fmul	%r40, %r1, %r1		# 400
	fmul	%r4, %r40, %r40		# 401
	fmul	%r6, %r4, %r40		# 402
	movhiz	%r1, 0x3f800		# 403
	movhiz	%r3, 0xbf000		# 404
	fmul	%r3, %r3, %r40		# 405
	fadd	%r1, %r1, %r3		# 406
	movhiz	%r5, 0x3d2a0		# 407
	movi	%r49, 0xa789		# 408
	add	%r5, %r5, %r49		# 409
	fmul	%r5, %r5, %r4		# 410
	fadd	%r1, %r1, %r5		# 411
	movhiz	%r7, 0xbab30		# 412
	movi	%r49, 0x8106		# 413
	add	%r7, %r7, %r49		# 414
	fmul	%r7, %r7, %r6		# 415
	fadd	%r1, %r1, %r7		# 416
	add	%r1, %r1, %r2		# 417
	jmpu	%r60		# 418
.globl	min_caml_atan
min_caml_atan:
	rshifti	%r2, %r1, 31		# 419
	lshifti	%r2, %r2, 31		# 420
	fabs	%r1, %r1		# 421
	movhiz	%r10, 0x3ee00		# 422
	cmpfgt	%r30, %r10, %r1		# 423
	movi	%r3, 0		# 424
	movhiz	%r4, 0x3f800		# 425
	jmpi	%r30, min_caml_atan.10		# 426
	movhiz	%r10, 0x401c0		# 427
	cmpfgt	%r30, %r1, %r10		# 428
	jmpi	%r30, min_caml_atan.1		# 429
	movhiz	%r12, 0x3f800		# 430
	fadd	%r5, %r1, %r12		# 431
	finv	%r5, %r5		# 432
	movhiz	%r3, 0x3f490		# 433
	addi	%r3, %r3, 0x0fdb		# 434
	fsub	%r4, %r1, %r12		# 435
	fmul	%r1, %r4, %r5		# 436
	movhiz	%r4, 0x3f800		# 437
	jmpui	min_caml_atan.10		# 438
min_caml_atan.1:
	finv	%r1, %r1		# 439
	movhiz	%r3, 0x3fc90		# 440
	addi	%r3, %r3, 0x0fdb		# 441
	movhiz	%r4, 0xbf800		# 442
	jmpui	min_caml_atan.10		# 443
min_caml_atan.10:
	fmul	%r22, %r1, %r1		# 444
	fmul	%r23, %r1, %r22		# 445
	fmul	%r24, %r22, %r22		# 446
	fmul	%r25, %r22, %r23		# 447
	fmul	%r27, %r23, %r24		# 448
	fmul	%r29, %r24, %r25		# 449
	fmul	%r31, %r24, %r27		# 450
	fmul	%r33, %r24, %r29		# 451
	movhiz	%r40, 0xbeaa0		# 452
	movi	%r49, 0xaaaa		# 453
	add	%r40, %r40, %r49		# 454
	fmul	%r41, %r40, %r23		# 455
	fadd	%r1, %r1, %r41		# 456
	movhiz	%r40, 0x3e4c0		# 457
	movi	%r49, 0xcccd		# 458
	add	%r40, %r40, %r49		# 459
	fmul	%r41, %r40, %r25		# 460
	fadd	%r1, %r1, %r41		# 461
	movhiz	%r40, 0xbe120		# 462
	movi	%r49, 0x4925		# 463
	add	%r40, %r40, %r49		# 464
	fmul	%r41, %r40, %r27		# 465
	fadd	%r1, %r1, %r41		# 466
	movhiz	%r40, 0x3de30		# 467
	movi	%r49, 0x8e38		# 468
	add	%r40, %r40, %r49		# 469
	fmul	%r41, %r40, %r29		# 470
	fadd	%r1, %r1, %r41		# 471
	movhiz	%r40, 0xbdb70		# 472
	movi	%r49, 0xd66e		# 473
	add	%r40, %r40, %r49		# 474
	fmul	%r41, %r40, %r31		# 475
	fadd	%r1, %r1, %r41		# 476
	movhiz	%r40, 0x3d750		# 477
	movi	%r49, 0xe7e5		# 478
	add	%r40, %r40, %r49		# 479
	fmul	%r41, %r40, %r33		# 480
	fadd	%r1, %r1, %r41		# 481
	fmul	%r1, %r1, %r4		# 482
	fadd	%r1, %r1, %r3		# 483
	add	%r1, %r1, %r2		# 484
	jmpu	%r60		# 485
.globl	min_caml_print_int
min_caml_print_int:
	cmplti	%r2, %r1, 0		# 486
	jmpzi	%r2, min_caml_print_int.1		# 487
	movi	%r2, 0x2d		# 488
	out	%r2		# 489
	neg	%r1, %r1		# 490
min_caml_print_int.1:
	movi	%r2, 1		# 491
	movi	%r3, 1		# 492
min_caml_print_int.2:
	lshifti	%r4, %r3, 3		# 493
	lshifti	%r5, %r3, 1		# 494
	add	%r6, %r4, %r5		# 495
	cmplt	%r10, %r1, %r6		# 496
	jmpi	%r10, min_caml_print_int.3		# 497
	addi	%r2, %r2, 1		# 498
	mov	%r3, %r6		# 499
	jmpui	min_caml_print_int.2		# 500
min_caml_print_int.3:
	movi	%r3, 1		# 501
	movi	%r4, 1		# 502
	movi	%r7, 0		# 503
	movi	%r8, 0		# 504
min_caml_print_int.4:
	cmplt	%r10, %r4, %r2		# 505
	jmpzi	%r10, min_caml_print_int.5		# 506
	lshifti	%r5, %r3, 3		# 507
	lshifti	%r6, %r3, 1		# 508
	addi	%r4, %r4, 1		# 509
	add	%r3, %r5, %r6		# 510
	jmpui	min_caml_print_int.4		# 511
min_caml_print_int.5:
	add	%r9, %r8, %r3		# 512
	cmplt	%r10, %r1, %r9		# 513
	jmpzi	%r10, min_caml_print_int.6		# 514
	addi	%r21, %r7, 0x30		# 515
	out	%r21		# 516
	cmpgti	%r10, %r2, 1		# 517
	jmpz	%r10, %r60		# 518
	sub	%r1, %r1, %r8		# 519
	addi	%r2, %r2, -1		# 520
	jmpui	min_caml_print_int.3		# 521
min_caml_print_int.6:
	mov	%r8, %r9		# 522
	addi	%r7, %r7, 1		# 523
	jmpui	min_caml_print_int.5		# 524
.globl	min_caml_print_newline
min_caml_print_newline:
	movi	%r3, 10		# 525
	out	%r3		# 526
	jmpu	%r60		# 527
.globl	min_caml_read_int
min_caml_read_int:
	in	%r1		# 528
	movi	%r20, 0		# 529
	cmpgti	%r30, %r1, 0x30		# 530
	jmpi	%r30, min_caml_read_int.1		# 531
	cmpeqi	%r30, %r1, 0x20		# 532
	jmpi	%r30, min_caml_read_int		# 533
	cmpeqi	%r30, %r1, 0x0a		# 534
	jmpi	%r30, min_caml_read_int		# 535
	cmpeqi	%r30, %r1, 0x0d		# 536
	jmpi	%r30, min_caml_read_int		# 537
	cmpeqi	%r30, %r1, 9		# 538
	jmpi	%r30, min_caml_read_int		# 539
	cmpeqi	%r31, %r1, 0x2d		# 540
min_caml_read_int.0.5:
	movi	%r1, 0		# 541
	jmpz	%r31, %r60		# 542
	movi	%r20, 1		# 543
	jmpui	min_caml_read_int.2		# 544
min_caml_read_int.1:
	cmpgti	%r31, %r1, 0x39		# 545
	jmpi	%r31, min_caml_read_int.0.5		# 546
	addi	%r1, %r1, -48		# 547
min_caml_read_int.2:
	in	%r2		# 548
	cmplti	%r30, %r2, 0x30		# 549
	jmpi	%r30, min_caml_read_int.3		# 550
	cmpgti	%r30, %r2, 0x39		# 551
	jmpi	%r30, min_caml_read_int.3		# 552
	addi	%r2, %r2, -0x30		# 553
	lshifti	%r3, %r1, 3		# 554
	lshifti	%r4, %r1, 1		# 555
	add	%r3, %r3, %r4		# 556
	add	%r1, %r3, %r2		# 557
	jmpui	min_caml_read_int.2		# 558
min_caml_read_int.3:
	jmpz	%r20, %r60		# 559
	neg	%r1, %r1		# 560
	jmpu	%r60		# 561
.globl	min_caml_create_array_here
min_caml_create_array_here:
	movi	%r4, 0		# 562
min_caml_create_array_here.1:
	cmplt	%r30, %r4, %r2		# 563
	jmpz	%r30, %r60		# 564
	add	%r5, %r1, %r4		# 565
	sto	%r3, (%r5)		# 566
	addi	%r4, %r4, 1		# 567
	jmpui	min_caml_create_array_here.1		# 568
.globl	min_caml_create_float_array
.globl	min_caml_create_array
min_caml_create_float_array:
min_caml_create_array:
	mov	%r3, %r1		# 569
	movi	%r4, 0		# 570
	mov	%r1, %r62		# 571
	add	%r62, %r62, %r3		# 572
min_caml_create_array.1:
	cmplt	%r30, %r4, %r3		# 573
	jmpz	%r30, %r60		# 574
	add	%r5, %r1, %r4		# 575
	sto	%r2, (%r5)		# 576
	addi	%r4, %r4, 1		# 577
	jmpui	min_caml_create_array.1		# 578
.globl	min_caml_out32
min_caml_out32:
	rshifti	%r2, %r1, 24		# 579
	out	%r2		# 580
	rshifti	%r2, %r1, 16		# 581
	out	%r2		# 582
	rshifti	%r2, %r1, 8		# 583
	out	%r2		# 584
	out	%r1		# 585
	jmpu	%r60		# 586
.globl	min_caml_lib_start
.main	min_caml_lib_start
read_float3.6192:
	sto	%r60, (%r63)		# 587
	addi	%r63, %r63, 1		# 588
	in	%r3		# 589
	cmpeqi	%r4, %r3, 32		# 590
	jmpzi	%r4, else.15785		# 591
	mov	%r1, %r2		# 592
	addi	%r63, %r63, -1		# 593
	ld	%r60, (%r63)		# 594
	jmpu	%r60		# 595
else.15785:
	cmpeqi	%r4, %r3, 10		# 596
	jmpzi	%r4, else.15786		# 597
	mov	%r1, %r2		# 598
	addi	%r63, %r63, -1		# 599
	ld	%r60, (%r63)		# 600
	jmpu	%r60		# 601
else.15786:
	cmpeqi	%r4, %r3, 13		# 602
	jmpzi	%r4, else.15787		# 603
	mov	%r1, %r2		# 604
	addi	%r63, %r63, -1		# 605
	ld	%r60, (%r63)		# 606
	jmpu	%r60		# 607
else.15787:
	cmpeqi	%r4, %r3, 9		# 608
	jmpzi	%r4, else.15788		# 609
	mov	%r1, %r2		# 610
	addi	%r63, %r63, -1		# 611
	ld	%r60, (%r63)		# 612
	jmpu	%r60		# 613
else.15788:
	movi	%r4, 48		# 614
	cmpgt	%r4, %r4, %r3		# 615
	jmpi	%r4, else.15789		# 616
	cmpgti	%r4, %r3, 57		# 617
	jmpi	%r4, else.15790		# 618
	movhiz	%r4, 253132		# 619
	addi	%r4, %r4, 3277		# 620
	fmul	%r4, %r1, %r4		# 621
	addi	%r3, %r3, -48		# 622
	sto	%r4, (%r63)		# 623
	sto	%r2, 1(%r63)		# 624
	sto	%r1, 2(%r63)		# 625
	mov	%r1, %r3		# 626
	addi	%r63, %r63, 3		# 627
	call	%r60, min_caml_float_of_int		# 628
	addi	%r63, %r63, -3		# 629
	ld	%r2, 2(%r63)		# 630
	fmul	%r2, %r2, %r1		# 631
	ld	%r1, 1(%r63)		# 632
	fadd	%r1, %r1, %r2		# 633
	in	%r2		# 634
	cmpeqi	%r3, %r2, 32		# 635
	jmpzi	%r3, else.15791		# 636
	addi	%r63, %r63, -1		# 637
	ld	%r60, (%r63)		# 638
	jmpu	%r60		# 639
else.15791:
	cmpeqi	%r3, %r2, 10		# 640
	jmpzi	%r3, else.15792		# 641
	addi	%r63, %r63, -1		# 642
	ld	%r60, (%r63)		# 643
	jmpu	%r60		# 644
else.15792:
	cmpeqi	%r3, %r2, 13		# 645
	jmpzi	%r3, else.15793		# 646
	addi	%r63, %r63, -1		# 647
	ld	%r60, (%r63)		# 648
	jmpu	%r60		# 649
else.15793:
	cmpeqi	%r3, %r2, 9		# 650
	jmpzi	%r3, else.15794		# 651
	addi	%r63, %r63, -1		# 652
	ld	%r60, (%r63)		# 653
	jmpu	%r60		# 654
else.15794:
	movi	%r3, 48		# 655
	cmpgt	%r3, %r3, %r2		# 656
	jmpi	%r3, else.15795		# 657
	cmpgti	%r3, %r2, 57		# 658
	jmpi	%r3, else.15796		# 659
	movhiz	%r3, 253132		# 660
	addi	%r3, %r3, 3277		# 661
	ld	%r4, (%r63)		# 662
	fmul	%r3, %r4, %r3		# 663
	addi	%r2, %r2, -48		# 664
	sto	%r3, 3(%r63)		# 665
	sto	%r1, 4(%r63)		# 666
	mov	%r1, %r2		# 667
	addi	%r63, %r63, 5		# 668
	call	%r60, min_caml_float_of_int		# 669
	addi	%r63, %r63, -5		# 670
	ld	%r2, (%r63)		# 671
	fmul	%r2, %r2, %r1		# 672
	ld	%r1, 4(%r63)		# 673
	fadd	%r2, %r1, %r2		# 674
	ld	%r1, 3(%r63)		# 675
	addi	%r63, %r63, -1		# 676
	ld	%r60, (%r63)		# 677
	jmpui	read_float3.6192		# 678
else.15796:
	addi	%r63, %r63, -1		# 679
	ld	%r60, (%r63)		# 680
	jmpu	%r60		# 681
else.15795:
	addi	%r63, %r63, -1		# 682
	ld	%r60, (%r63)		# 683
	jmpu	%r60		# 684
else.15790:
	mov	%r1, %r2		# 685
	addi	%r63, %r63, -1		# 686
	ld	%r60, (%r63)		# 687
	jmpu	%r60		# 688
else.15789:
	mov	%r1, %r2		# 689
	addi	%r63, %r63, -1		# 690
	ld	%r60, (%r63)		# 691
	jmpu	%r60		# 692
.globl	read_float2.6195
read_float2.6195:
	sto	%r60, (%r63)		# 693
	addi	%r63, %r63, 1		# 694
	in	%r2		# 695
	cmpeqi	%r3, %r2, 32		# 696
	jmpzi	%r3, else.15797		# 697
	addi	%r63, %r63, -1		# 698
	ld	%r60, (%r63)		# 699
	jmpu	%r60		# 700
else.15797:
	cmpeqi	%r3, %r2, 10		# 701
	jmpzi	%r3, else.15798		# 702
	addi	%r63, %r63, -1		# 703
	ld	%r60, (%r63)		# 704
	jmpu	%r60		# 705
else.15798:
	cmpeqi	%r3, %r2, 13		# 706
	jmpzi	%r3, else.15799		# 707
	addi	%r63, %r63, -1		# 708
	ld	%r60, (%r63)		# 709
	jmpu	%r60		# 710
else.15799:
	cmpeqi	%r3, %r2, 9		# 711
	jmpzi	%r3, else.15800		# 712
	addi	%r63, %r63, -1		# 713
	ld	%r60, (%r63)		# 714
	jmpu	%r60		# 715
else.15800:
	cmpeqi	%r3, %r2, 46		# 716
	jmpzi	%r3, else.15801		# 717
	movhiz	%r2, 253132		# 718
	addi	%r2, %r2, 3277		# 719
	in	%r3		# 720
	cmpeqi	%r4, %r3, 32		# 721
	jmpzi	%r4, else.15802		# 722
	addi	%r63, %r63, -1		# 723
	ld	%r60, (%r63)		# 724
	jmpu	%r60		# 725
else.15802:
	cmpeqi	%r4, %r3, 10		# 726
	jmpzi	%r4, else.15803		# 727
	addi	%r63, %r63, -1		# 728
	ld	%r60, (%r63)		# 729
	jmpu	%r60		# 730
else.15803:
	cmpeqi	%r4, %r3, 13		# 731
	jmpzi	%r4, else.15804		# 732
	addi	%r63, %r63, -1		# 733
	ld	%r60, (%r63)		# 734
	jmpu	%r60		# 735
else.15804:
	cmpeqi	%r4, %r3, 9		# 736
	jmpzi	%r4, else.15805		# 737
	addi	%r63, %r63, -1		# 738
	ld	%r60, (%r63)		# 739
	jmpu	%r60		# 740
else.15805:
	movi	%r4, 48		# 741
	cmpgt	%r4, %r4, %r3		# 742
	jmpi	%r4, else.15806		# 743
	cmpgti	%r4, %r3, 57		# 744
	jmpi	%r4, else.15807		# 745
	movhiz	%r4, 246333		# 746
	addi	%r4, %r4, 1802		# 747
	addi	%r3, %r3, -48		# 748
	sto	%r4, (%r63)		# 749
	sto	%r1, 1(%r63)		# 750
	sto	%r2, 2(%r63)		# 751
	mov	%r1, %r3		# 752
	addi	%r63, %r63, 3		# 753
	call	%r60, min_caml_float_of_int		# 754
	addi	%r63, %r63, -3		# 755
	ld	%r2, 2(%r63)		# 756
	fmul	%r2, %r2, %r1		# 757
	ld	%r1, 1(%r63)		# 758
	fadd	%r2, %r1, %r2		# 759
	ld	%r1, (%r63)		# 760
	addi	%r63, %r63, -1		# 761
	ld	%r60, (%r63)		# 762
	jmpui	read_float3.6192		# 763
else.15807:
	addi	%r63, %r63, -1		# 764
	ld	%r60, (%r63)		# 765
	jmpu	%r60		# 766
else.15806:
	addi	%r63, %r63, -1		# 767
	ld	%r60, (%r63)		# 768
	jmpu	%r60		# 769
else.15801:
	movi	%r3, 48		# 770
	cmpgt	%r3, %r3, %r2		# 771
	jmpi	%r3, else.15808		# 772
	cmpgti	%r3, %r2, 57		# 773
	jmpi	%r3, else.15809		# 774
	movhiz	%r3, 266752		# 775
	fmul	%r1, %r1, %r3		# 776
	addi	%r2, %r2, -48		# 777
	sto	%r1, 3(%r63)		# 778
	mov	%r1, %r2		# 779
	addi	%r63, %r63, 4		# 780
	call	%r60, min_caml_float_of_int		# 781
	addi	%r63, %r63, -4		# 782
	ld	%r2, 3(%r63)		# 783
	fadd	%r1, %r2, %r1		# 784
	in	%r2		# 785
	cmpeqi	%r3, %r2, 32		# 786
	jmpzi	%r3, else.15810		# 787
	addi	%r63, %r63, -1		# 788
	ld	%r60, (%r63)		# 789
	jmpu	%r60		# 790
else.15810:
	cmpeqi	%r3, %r2, 10		# 791
	jmpzi	%r3, else.15811		# 792
	addi	%r63, %r63, -1		# 793
	ld	%r60, (%r63)		# 794
	jmpu	%r60		# 795
else.15811:
	cmpeqi	%r3, %r2, 13		# 796
	jmpzi	%r3, else.15812		# 797
	addi	%r63, %r63, -1		# 798
	ld	%r60, (%r63)		# 799
	jmpu	%r60		# 800
else.15812:
	cmpeqi	%r3, %r2, 9		# 801
	jmpzi	%r3, else.15813		# 802
	addi	%r63, %r63, -1		# 803
	ld	%r60, (%r63)		# 804
	jmpu	%r60		# 805
else.15813:
	cmpeqi	%r3, %r2, 46		# 806
	jmpzi	%r3, else.15814		# 807
	movhiz	%r2, 253132		# 808
	addi	%r2, %r2, 3277		# 809
	sto	%r2, 4(%r63)		# 810
	mov	%r2, %r1		# 811
	ld	%r1, 4(%r63)		# 812
	addi	%r63, %r63, -1		# 813
	ld	%r60, (%r63)		# 814
	jmpui	read_float3.6192		# 815
else.15814:
	movi	%r3, 48		# 816
	cmpgt	%r3, %r3, %r2		# 817
	jmpi	%r3, else.15815		# 818
	cmpgti	%r3, %r2, 57		# 819
	jmpi	%r3, else.15816		# 820
	movhiz	%r3, 266752		# 821
	fmul	%r1, %r1, %r3		# 822
	addi	%r2, %r2, -48		# 823
	sto	%r1, 4(%r63)		# 824
	mov	%r1, %r2		# 825
	addi	%r63, %r63, 5		# 826
	call	%r60, min_caml_float_of_int		# 827
	addi	%r63, %r63, -5		# 828
	ld	%r2, 4(%r63)		# 829
	fadd	%r1, %r2, %r1		# 830
	addi	%r63, %r63, -1		# 831
	ld	%r60, (%r63)		# 832
	jmpui	read_float2.6195		# 833
else.15816:
	addi	%r63, %r63, -1		# 834
	ld	%r60, (%r63)		# 835
	jmpu	%r60		# 836
else.15815:
	addi	%r63, %r63, -1		# 837
	ld	%r60, (%r63)		# 838
	jmpu	%r60		# 839
else.15809:
	addi	%r63, %r63, -1		# 840
	ld	%r60, (%r63)		# 841
	jmpu	%r60		# 842
else.15808:
	addi	%r63, %r63, -1		# 843
	ld	%r60, (%r63)		# 844
	jmpu	%r60		# 845
.globl	read_float.2433
read_float.2433:
	sto	%r60, (%r63)		# 846
	addi	%r63, %r63, 1		# 847
	in	%r1		# 848
	cmpeqi	%r2, %r1, 32		# 849
	jmpzi	%r2, else.15817		# 850
	addi	%r63, %r63, -1		# 851
	ld	%r60, (%r63)		# 852
	jmpui	read_float.2433		# 853
else.15817:
	cmpeqi	%r2, %r1, 10		# 854
	jmpzi	%r2, else.15818		# 855
	addi	%r63, %r63, -1		# 856
	ld	%r60, (%r63)		# 857
	jmpui	read_float.2433		# 858
else.15818:
	cmpeqi	%r2, %r1, 13		# 859
	jmpzi	%r2, else.15819		# 860
	addi	%r63, %r63, -1		# 861
	ld	%r60, (%r63)		# 862
	jmpui	read_float.2433		# 863
else.15819:
	cmpeqi	%r2, %r1, 9		# 864
	jmpzi	%r2, else.15820		# 865
	addi	%r63, %r63, -1		# 866
	ld	%r60, (%r63)		# 867
	jmpui	read_float.2433		# 868
else.15820:
	cmpeqi	%r2, %r1, 46		# 869
	jmpzi	%r2, else.15821		# 870
	movhiz	%r1, 253132		# 871
	addi	%r1, %r1, 3277		# 872
	movi	%r2, 0		# 873
	in	%r3		# 874
	cmpeqi	%r4, %r3, 32		# 875
	jmpzi	%r4, else.15822		# 876
	mov	%r1, %r2		# 877
	addi	%r63, %r63, -1		# 878
	ld	%r60, (%r63)		# 879
	jmpu	%r60		# 880
else.15822:
	cmpeqi	%r4, %r3, 10		# 881
	jmpzi	%r4, else.15823		# 882
	mov	%r1, %r2		# 883
	addi	%r63, %r63, -1		# 884
	ld	%r60, (%r63)		# 885
	jmpu	%r60		# 886
else.15823:
	cmpeqi	%r4, %r3, 13		# 887
	jmpzi	%r4, else.15824		# 888
	mov	%r1, %r2		# 889
	addi	%r63, %r63, -1		# 890
	ld	%r60, (%r63)		# 891
	jmpu	%r60		# 892
else.15824:
	cmpeqi	%r4, %r3, 9		# 893
	jmpzi	%r4, else.15825		# 894
	mov	%r1, %r2		# 895
	addi	%r63, %r63, -1		# 896
	ld	%r60, (%r63)		# 897
	jmpu	%r60		# 898
else.15825:
	movi	%r4, 48		# 899
	cmpgt	%r4, %r4, %r3		# 900
	jmpi	%r4, else.15826		# 901
	cmpgti	%r4, %r3, 57		# 902
	jmpi	%r4, else.15827		# 903
	movhiz	%r4, 246333		# 904
	addi	%r4, %r4, 1802		# 905
	addi	%r3, %r3, -48		# 906
	sto	%r4, (%r63)		# 907
	sto	%r2, 1(%r63)		# 908
	sto	%r1, 2(%r63)		# 909
	mov	%r1, %r3		# 910
	addi	%r63, %r63, 3		# 911
	call	%r60, min_caml_float_of_int		# 912
	addi	%r63, %r63, -3		# 913
	ld	%r2, 2(%r63)		# 914
	fmul	%r2, %r2, %r1		# 915
	ld	%r1, 1(%r63)		# 916
	fadd	%r2, %r1, %r2		# 917
	ld	%r1, (%r63)		# 918
	addi	%r63, %r63, -1		# 919
	ld	%r60, (%r63)		# 920
	jmpui	read_float3.6192		# 921
else.15827:
	mov	%r1, %r2		# 922
	addi	%r63, %r63, -1		# 923
	ld	%r60, (%r63)		# 924
	jmpu	%r60		# 925
else.15826:
	mov	%r1, %r2		# 926
	addi	%r63, %r63, -1		# 927
	ld	%r60, (%r63)		# 928
	jmpu	%r60		# 929
else.15821:
	cmpeqi	%r2, %r1, 45		# 930
	jmpzi	%r2, else.15828		# 931
	movi	%r2, 0		# 932
	in	%r1		# 933
	cmpeqi	%r3, %r1, 32		# 934
	jmpzi	%r3, else.15829		# 935
	jmpui	if_cont.15830		# 936
else.15829:
	cmpeqi	%r3, %r1, 10		# 937
	jmpzi	%r3, else.15831		# 938
	jmpui	if_cont.15832		# 939
else.15831:
	cmpeqi	%r3, %r1, 13		# 940
	jmpzi	%r3, else.15833		# 941
	jmpui	if_cont.15834		# 942
else.15833:
	cmpeqi	%r3, %r1, 9		# 943
	jmpzi	%r3, else.15835		# 944
	jmpui	if_cont.15836		# 945
else.15835:
	cmpeqi	%r3, %r1, 46		# 946
	jmpzi	%r3, else.15837		# 947
	movhiz	%r1, 253132		# 948
	addi	%r1, %r1, 3277		# 949
	addi	%r63, %r63, 3		# 950
	call	%r60, read_float3.6192		# 951
	addi	%r63, %r63, -3		# 952
	mov	%r2, %r1		# 953
	jmpui	if_cont.15838		# 954
else.15837:
	movi	%r3, 48		# 955
	cmpgt	%r3, %r3, %r1		# 956
	jmpi	%r3, else.15839		# 957
	cmpgti	%r3, %r1, 57		# 958
	jmpi	%r3, else.15841		# 959
	movi	%r2, 0		# 960
	addi	%r1, %r1, -48		# 961
	sto	%r2, 3(%r63)		# 962
	addi	%r63, %r63, 4		# 963
	call	%r60, min_caml_float_of_int		# 964
	addi	%r63, %r63, -4		# 965
	ld	%r2, 3(%r63)		# 966
	fadd	%r1, %r2, %r1		# 967
	addi	%r63, %r63, 4		# 968
	call	%r60, read_float2.6195		# 969
	addi	%r63, %r63, -4		# 970
	mov	%r2, %r1		# 971
	jmpui	if_cont.15842		# 972
else.15841:
if_cont.15842:
	jmpui	if_cont.15840		# 973
else.15839:
if_cont.15840:
if_cont.15838:
if_cont.15836:
if_cont.15834:
if_cont.15832:
if_cont.15830:
	fneg	%r1, %r2		# 974
	addi	%r63, %r63, -1		# 975
	ld	%r60, (%r63)		# 976
	jmpu	%r60		# 977
else.15828:
	movi	%r2, 48		# 978
	cmpgt	%r2, %r2, %r1		# 979
	jmpi	%r2, else.15843		# 980
	cmpgti	%r2, %r1, 57		# 981
	jmpi	%r2, else.15844		# 982
	addi	%r1, %r1, -48		# 983
	addi	%r63, %r63, 4		# 984
	call	%r60, min_caml_float_of_int		# 985
	addi	%r63, %r63, -4		# 986
	in	%r2		# 987
	cmpeqi	%r3, %r2, 32		# 988
	jmpzi	%r3, else.15845		# 989
	addi	%r63, %r63, -1		# 990
	ld	%r60, (%r63)		# 991
	jmpu	%r60		# 992
else.15845:
	cmpeqi	%r3, %r2, 10		# 993
	jmpzi	%r3, else.15846		# 994
	addi	%r63, %r63, -1		# 995
	ld	%r60, (%r63)		# 996
	jmpu	%r60		# 997
else.15846:
	cmpeqi	%r3, %r2, 13		# 998
	jmpzi	%r3, else.15847		# 999
	addi	%r63, %r63, -1		# 1000
	ld	%r60, (%r63)		# 1001
	jmpu	%r60		# 1002
else.15847:
	cmpeqi	%r3, %r2, 9		# 1003
	jmpzi	%r3, else.15848		# 1004
	addi	%r63, %r63, -1		# 1005
	ld	%r60, (%r63)		# 1006
	jmpu	%r60		# 1007
else.15848:
	cmpeqi	%r3, %r2, 46		# 1008
	jmpzi	%r3, else.15849		# 1009
	movhiz	%r2, 253132		# 1010
	addi	%r2, %r2, 3277		# 1011
	sto	%r2, 4(%r63)		# 1012
	mov	%r2, %r1		# 1013
	ld	%r1, 4(%r63)		# 1014
	addi	%r63, %r63, -1		# 1015
	ld	%r60, (%r63)		# 1016
	jmpui	read_float3.6192		# 1017
else.15849:
	movi	%r3, 48		# 1018
	cmpgt	%r3, %r3, %r2		# 1019
	jmpi	%r3, else.15850		# 1020
	cmpgti	%r3, %r2, 57		# 1021
	jmpi	%r3, else.15851		# 1022
	movhiz	%r3, 266752		# 1023
	fmul	%r1, %r1, %r3		# 1024
	addi	%r2, %r2, -48		# 1025
	sto	%r1, 4(%r63)		# 1026
	mov	%r1, %r2		# 1027
	addi	%r63, %r63, 5		# 1028
	call	%r60, min_caml_float_of_int		# 1029
	addi	%r63, %r63, -5		# 1030
	ld	%r2, 4(%r63)		# 1031
	fadd	%r1, %r2, %r1		# 1032
	addi	%r63, %r63, -1		# 1033
	ld	%r60, (%r63)		# 1034
	jmpui	read_float2.6195		# 1035
else.15851:
	addi	%r63, %r63, -1		# 1036
	ld	%r60, (%r63)		# 1037
	jmpu	%r60		# 1038
else.15850:
	addi	%r63, %r63, -1		# 1039
	ld	%r60, (%r63)		# 1040
	jmpu	%r60		# 1041
else.15844:
	movi	%r1, 0		# 1042
	addi	%r63, %r63, -1		# 1043
	ld	%r60, (%r63)		# 1044
	jmpu	%r60		# 1045
else.15843:
	movi	%r1, 0		# 1046
	addi	%r63, %r63, -1		# 1047
	ld	%r60, (%r63)		# 1048
	jmpu	%r60		# 1049
.globl	vecunit_sgn.2488
vecunit_sgn.2488:
	addi	%r3, %r1, 0		# 1050
	ld	%r3, (%r3)		# 1051
	fmul	%r3, %r3, %r3		# 1052
	addi	%r4, %r1, 1		# 1053
	ld	%r4, (%r4)		# 1054
	fmul	%r4, %r4, %r4		# 1055
	fadd	%r3, %r3, %r4		# 1056
	addi	%r4, %r1, 2		# 1057
	ld	%r4, (%r4)		# 1058
	fmul	%r4, %r4, %r4		# 1059
	fadd	%r3, %r3, %r4		# 1060
	fsqrt	%r3, %r3		# 1061
	movi	%r4, 0		# 1062
	cmpfeq	%r4, %r3, %r4		# 1063
	jmpzi	%r4, else.15852		# 1064
	movhiz	%r2, 260096		# 1065
	jmpui	if_cont.15853		# 1066
else.15852:
	cmpeqi	%r2, %r2, 0		# 1067
	jmpzi	%r2, else.15854		# 1068
	movhiz	%r2, 260096		# 1069
	finv	%r3, %r3		# 1070
	fmul	%r2, %r2, %r3		# 1071
	jmpui	if_cont.15855		# 1072
else.15854:
	movhiz	%r2, 784384		# 1073
	finv	%r3, %r3		# 1074
	fmul	%r2, %r2, %r3		# 1075
if_cont.15855:
if_cont.15853:
	addi	%r3, %r1, 0		# 1076
	ld	%r3, (%r3)		# 1077
	fmul	%r3, %r3, %r2		# 1078
	addi	%r4, %r1, 0		# 1079
	sto	%r3, (%r4)		# 1080
	addi	%r3, %r1, 1		# 1081
	ld	%r3, (%r3)		# 1082
	fmul	%r3, %r3, %r2		# 1083
	addi	%r4, %r1, 1		# 1084
	sto	%r3, (%r4)		# 1085
	addi	%r3, %r1, 2		# 1086
	ld	%r3, (%r3)		# 1087
	fmul	%r3, %r3, %r2		# 1088
	addi	%r1, %r1, 2		# 1089
	sto	%r3, (%r1)		# 1090
	jmpu	%r60		# 1091
.globl	vecaccumv.2512
vecaccumv.2512:
	addi	%r4, %r1, 0		# 1092
	ld	%r4, (%r4)		# 1093
	addi	%r5, %r2, 0		# 1094
	ld	%r5, (%r5)		# 1095
	addi	%r6, %r3, 0		# 1096
	ld	%r6, (%r6)		# 1097
	fmul	%r5, %r5, %r6		# 1098
	fadd	%r4, %r4, %r5		# 1099
	addi	%r5, %r1, 0		# 1100
	sto	%r4, (%r5)		# 1101
	addi	%r4, %r1, 1		# 1102
	ld	%r4, (%r4)		# 1103
	addi	%r5, %r2, 1		# 1104
	ld	%r5, (%r5)		# 1105
	addi	%r6, %r3, 1		# 1106
	ld	%r6, (%r6)		# 1107
	fmul	%r5, %r5, %r6		# 1108
	fadd	%r4, %r4, %r5		# 1109
	addi	%r5, %r1, 1		# 1110
	sto	%r4, (%r5)		# 1111
	addi	%r4, %r1, 2		# 1112
	ld	%r4, (%r4)		# 1113
	addi	%r2, %r2, 2		# 1114
	ld	%r2, (%r2)		# 1115
	addi	%r3, %r3, 2		# 1116
	ld	%r3, (%r3)		# 1117
	fmul	%r2, %r2, %r3		# 1118
	fadd	%r4, %r4, %r2		# 1119
	addi	%r1, %r1, 2		# 1120
	sto	%r4, (%r1)		# 1121
	jmpu	%r60		# 1122
.globl	read_screen_settings.2589
read_screen_settings.2589:
	sto	%r60, (%r63)		# 1123
	addi	%r63, %r63, 1		# 1124
	movi	%r1, min_caml_screen		# 1125
	sto	%r1, (%r63)		# 1126
	addi	%r63, %r63, 1		# 1127
	call	%r60, read_float.2433		# 1128
	addi	%r63, %r63, -1		# 1129
	ld	%r2, (%r63)		# 1130
	addi	%r2, %r2, 0		# 1131
	sto	%r1, (%r2)		# 1132
	movi	%r1, min_caml_screen		# 1133
	sto	%r1, 1(%r63)		# 1134
	addi	%r63, %r63, 2		# 1135
	call	%r60, read_float.2433		# 1136
	addi	%r63, %r63, -2		# 1137
	ld	%r2, 1(%r63)		# 1138
	addi	%r2, %r2, 1		# 1139
	sto	%r1, (%r2)		# 1140
	movi	%r1, min_caml_screen		# 1141
	sto	%r1, 2(%r63)		# 1142
	addi	%r63, %r63, 3		# 1143
	call	%r60, read_float.2433		# 1144
	addi	%r63, %r63, -3		# 1145
	ld	%r2, 2(%r63)		# 1146
	addi	%r2, %r2, 2		# 1147
	sto	%r1, (%r2)		# 1148
	addi	%r63, %r63, 3		# 1149
	call	%r60, read_float.2433		# 1150
	addi	%r63, %r63, -3		# 1151
	movhiz	%r2, 248047		# 1152
	addi	%r2, %r2, 2613		# 1153
	fmul	%r1, %r1, %r2		# 1154
	sto	%r1, 3(%r63)		# 1155
	addi	%r63, %r63, 4		# 1156
	call	%r60, min_caml_cos		# 1157
	addi	%r63, %r63, -4		# 1158
	ld	%r2, 3(%r63)		# 1159
	sto	%r1, 4(%r63)		# 1160
	mov	%r1, %r2		# 1161
	addi	%r63, %r63, 5		# 1162
	call	%r60, min_caml_sin		# 1163
	addi	%r63, %r63, -5		# 1164
	sto	%r1, 5(%r63)		# 1165
	addi	%r63, %r63, 6		# 1166
	call	%r60, read_float.2433		# 1167
	addi	%r63, %r63, -6		# 1168
	movhiz	%r2, 248047		# 1169
	addi	%r2, %r2, 2613		# 1170
	fmul	%r1, %r1, %r2		# 1171
	sto	%r1, 6(%r63)		# 1172
	addi	%r63, %r63, 7		# 1173
	call	%r60, min_caml_cos		# 1174
	addi	%r63, %r63, -7		# 1175
	ld	%r2, 6(%r63)		# 1176
	sto	%r1, 7(%r63)		# 1177
	mov	%r1, %r2		# 1178
	addi	%r63, %r63, 8		# 1179
	call	%r60, min_caml_sin		# 1180
	addi	%r63, %r63, -8		# 1181
	movi	%r2, min_caml_screenz_dir		# 1182
	ld	%r3, 4(%r63)		# 1183
	fmul	%r4, %r3, %r1		# 1184
	movhiz	%r5, 275584		# 1185
	fmul	%r4, %r4, %r5		# 1186
	addi	%r2, %r2, 0		# 1187
	sto	%r4, (%r2)		# 1188
	movi	%r2, min_caml_screenz_dir		# 1189
	movhiz	%r4, 799872		# 1190
	ld	%r5, 5(%r63)		# 1191
	fmul	%r4, %r5, %r4		# 1192
	addi	%r2, %r2, 1		# 1193
	sto	%r4, (%r2)		# 1194
	movi	%r2, min_caml_screenz_dir		# 1195
	ld	%r4, 7(%r63)		# 1196
	fmul	%r6, %r3, %r4		# 1197
	movhiz	%r7, 275584		# 1198
	fmul	%r6, %r6, %r7		# 1199
	addi	%r2, %r2, 2		# 1200
	sto	%r6, (%r2)		# 1201
	movi	%r2, min_caml_screenx_dir		# 1202
	addi	%r2, %r2, 0		# 1203
	sto	%r4, (%r2)		# 1204
	movi	%r2, min_caml_screenx_dir		# 1205
	movi	%r6, 0		# 1206
	addi	%r2, %r2, 1		# 1207
	sto	%r6, (%r2)		# 1208
	movi	%r2, min_caml_screenx_dir		# 1209
	fneg	%r6, %r1		# 1210
	addi	%r2, %r2, 2		# 1211
	sto	%r6, (%r2)		# 1212
	movi	%r2, min_caml_screeny_dir		# 1213
	fneg	%r6, %r5		# 1214
	fmul	%r6, %r6, %r1		# 1215
	addi	%r2, %r2, 0		# 1216
	sto	%r6, (%r2)		# 1217
	movi	%r1, min_caml_screeny_dir		# 1218
	fneg	%r3, %r3		# 1219
	addi	%r1, %r1, 1		# 1220
	sto	%r3, (%r1)		# 1221
	movi	%r1, min_caml_screeny_dir		# 1222
	fneg	%r5, %r5		# 1223
	fmul	%r5, %r5, %r4		# 1224
	addi	%r1, %r1, 2		# 1225
	sto	%r5, (%r1)		# 1226
	movi	%r1, min_caml_viewpoint		# 1227
	movi	%r2, min_caml_screen		# 1228
	addi	%r2, %r2, 0		# 1229
	ld	%r2, (%r2)		# 1230
	movi	%r3, min_caml_screenz_dir		# 1231
	addi	%r3, %r3, 0		# 1232
	ld	%r3, (%r3)		# 1233
	fsub	%r2, %r2, %r3		# 1234
	addi	%r1, %r1, 0		# 1235
	sto	%r2, (%r1)		# 1236
	movi	%r1, min_caml_viewpoint		# 1237
	movi	%r2, min_caml_screen		# 1238
	addi	%r2, %r2, 1		# 1239
	ld	%r2, (%r2)		# 1240
	movi	%r3, min_caml_screenz_dir		# 1241
	addi	%r3, %r3, 1		# 1242
	ld	%r3, (%r3)		# 1243
	fsub	%r2, %r2, %r3		# 1244
	addi	%r1, %r1, 1		# 1245
	sto	%r2, (%r1)		# 1246
	movi	%r1, min_caml_viewpoint		# 1247
	movi	%r2, min_caml_screen		# 1248
	addi	%r2, %r2, 2		# 1249
	ld	%r2, (%r2)		# 1250
	movi	%r3, min_caml_screenz_dir		# 1251
	addi	%r3, %r3, 2		# 1252
	ld	%r3, (%r3)		# 1253
	fsub	%r2, %r2, %r3		# 1254
	addi	%r1, %r1, 2		# 1255
	sto	%r2, (%r1)		# 1256
	addi	%r63, %r63, -1		# 1257
	ld	%r60, (%r63)		# 1258
	jmpu	%r60		# 1259
.globl	read_light.2591
read_light.2591:
	sto	%r60, (%r63)		# 1260
	addi	%r63, %r63, 1		# 1261
	call	%r60, min_caml_read_int		# 1262
	call	%r60, read_float.2433		# 1263
	movhiz	%r2, 248047		# 1264
	addi	%r2, %r2, 2613		# 1265
	fmul	%r1, %r1, %r2		# 1266
	sto	%r1, (%r63)		# 1267
	addi	%r63, %r63, 1		# 1268
	call	%r60, min_caml_sin		# 1269
	addi	%r63, %r63, -1		# 1270
	movi	%r2, min_caml_light		# 1271
	fneg	%r1, %r1		# 1272
	addi	%r2, %r2, 1		# 1273
	sto	%r1, (%r2)		# 1274
	addi	%r63, %r63, 1		# 1275
	call	%r60, read_float.2433		# 1276
	addi	%r63, %r63, -1		# 1277
	movhiz	%r2, 248047		# 1278
	addi	%r2, %r2, 2613		# 1279
	fmul	%r1, %r1, %r2		# 1280
	ld	%r2, (%r63)		# 1281
	sto	%r1, 1(%r63)		# 1282
	mov	%r1, %r2		# 1283
	addi	%r63, %r63, 2		# 1284
	call	%r60, min_caml_cos		# 1285
	addi	%r63, %r63, -2		# 1286
	ld	%r2, 1(%r63)		# 1287
	sto	%r1, 2(%r63)		# 1288
	mov	%r1, %r2		# 1289
	addi	%r63, %r63, 3		# 1290
	call	%r60, min_caml_sin		# 1291
	addi	%r63, %r63, -3		# 1292
	movi	%r2, min_caml_light		# 1293
	ld	%r3, 2(%r63)		# 1294
	fmul	%r1, %r3, %r1		# 1295
	addi	%r2, %r2, 0		# 1296
	sto	%r1, (%r2)		# 1297
	ld	%r1, 1(%r63)		# 1298
	addi	%r63, %r63, 3		# 1299
	call	%r60, min_caml_cos		# 1300
	addi	%r63, %r63, -3		# 1301
	movi	%r2, min_caml_light		# 1302
	ld	%r3, 2(%r63)		# 1303
	fmul	%r3, %r3, %r1		# 1304
	addi	%r2, %r2, 2		# 1305
	sto	%r3, (%r2)		# 1306
	movi	%r1, min_caml_beam		# 1307
	sto	%r1, 3(%r63)		# 1308
	addi	%r63, %r63, 4		# 1309
	call	%r60, read_float.2433		# 1310
	addi	%r63, %r63, -4		# 1311
	ld	%r2, 3(%r63)		# 1312
	addi	%r2, %r2, 0		# 1313
	sto	%r1, (%r2)		# 1314
	addi	%r63, %r63, -1		# 1315
	ld	%r60, (%r63)		# 1316
	jmpu	%r60		# 1317
.globl	rotate_quadratic_matrix.2593
rotate_quadratic_matrix.2593:
	sto	%r60, (%r63)		# 1318
	addi	%r63, %r63, 1		# 1319
	addi	%r3, %r2, 0		# 1320
	ld	%r3, (%r3)		# 1321
	sto	%r1, (%r63)		# 1322
	sto	%r2, 1(%r63)		# 1323
	mov	%r1, %r3		# 1324
	addi	%r63, %r63, 2		# 1325
	call	%r60, min_caml_cos		# 1326
	addi	%r63, %r63, -2		# 1327
	ld	%r2, 1(%r63)		# 1328
	addi	%r3, %r2, 0		# 1329
	ld	%r3, (%r3)		# 1330
	sto	%r1, 2(%r63)		# 1331
	mov	%r1, %r3		# 1332
	addi	%r63, %r63, 3		# 1333
	call	%r60, min_caml_sin		# 1334
	addi	%r63, %r63, -3		# 1335
	ld	%r2, 1(%r63)		# 1336
	addi	%r3, %r2, 1		# 1337
	ld	%r3, (%r3)		# 1338
	sto	%r1, 3(%r63)		# 1339
	mov	%r1, %r3		# 1340
	addi	%r63, %r63, 4		# 1341
	call	%r60, min_caml_cos		# 1342
	addi	%r63, %r63, -4		# 1343
	ld	%r2, 1(%r63)		# 1344
	addi	%r3, %r2, 1		# 1345
	ld	%r3, (%r3)		# 1346
	sto	%r1, 4(%r63)		# 1347
	mov	%r1, %r3		# 1348
	addi	%r63, %r63, 5		# 1349
	call	%r60, min_caml_sin		# 1350
	addi	%r63, %r63, -5		# 1351
	ld	%r2, 1(%r63)		# 1352
	addi	%r3, %r2, 2		# 1353
	ld	%r3, (%r3)		# 1354
	sto	%r1, 5(%r63)		# 1355
	mov	%r1, %r3		# 1356
	addi	%r63, %r63, 6		# 1357
	call	%r60, min_caml_cos		# 1358
	addi	%r63, %r63, -6		# 1359
	ld	%r2, 1(%r63)		# 1360
	addi	%r3, %r2, 2		# 1361
	ld	%r3, (%r3)		# 1362
	sto	%r1, 6(%r63)		# 1363
	mov	%r1, %r3		# 1364
	addi	%r63, %r63, 7		# 1365
	call	%r60, min_caml_sin		# 1366
	addi	%r63, %r63, -7		# 1367
	ld	%r2, 6(%r63)		# 1368
	ld	%r3, 4(%r63)		# 1369
	fmul	%r4, %r3, %r2		# 1370
	ld	%r5, 5(%r63)		# 1371
	ld	%r6, 3(%r63)		# 1372
	fmul	%r7, %r6, %r5		# 1373
	fmul	%r7, %r7, %r2		# 1374
	ld	%r8, 2(%r63)		# 1375
	fmul	%r9, %r8, %r1		# 1376
	fsub	%r7, %r7, %r9		# 1377
	fmul	%r9, %r8, %r5		# 1378
	fmul	%r9, %r9, %r2		# 1379
	fmul	%r10, %r6, %r1		# 1380
	fadd	%r9, %r9, %r10		# 1381
	fmul	%r10, %r3, %r1		# 1382
	fmul	%r11, %r6, %r5		# 1383
	fmul	%r11, %r11, %r1		# 1384
	fmul	%r12, %r8, %r2		# 1385
	fadd	%r11, %r11, %r12		# 1386
	fmul	%r12, %r8, %r5		# 1387
	fmul	%r12, %r12, %r1		# 1388
	fmul	%r2, %r6, %r2		# 1389
	fsub	%r12, %r12, %r2		# 1390
	fneg	%r5, %r5		# 1391
	fmul	%r6, %r6, %r3		# 1392
	fmul	%r8, %r8, %r3		# 1393
	ld	%r1, (%r63)		# 1394
	addi	%r2, %r1, 0		# 1395
	ld	%r2, (%r2)		# 1396
	addi	%r3, %r1, 1		# 1397
	ld	%r3, (%r3)		# 1398
	addi	%r13, %r1, 2		# 1399
	ld	%r13, (%r13)		# 1400
	fmul	%r14, %r4, %r4		# 1401
	fmul	%r14, %r2, %r14		# 1402
	fmul	%r15, %r10, %r10		# 1403
	fmul	%r15, %r3, %r15		# 1404
	fadd	%r14, %r14, %r15		# 1405
	fmul	%r15, %r5, %r5		# 1406
	fmul	%r15, %r13, %r15		# 1407
	fadd	%r14, %r14, %r15		# 1408
	addi	%r15, %r1, 0		# 1409
	sto	%r14, (%r15)		# 1410
	fmul	%r14, %r7, %r7		# 1411
	fmul	%r14, %r2, %r14		# 1412
	fmul	%r15, %r11, %r11		# 1413
	fmul	%r15, %r3, %r15		# 1414
	fadd	%r14, %r14, %r15		# 1415
	fmul	%r15, %r6, %r6		# 1416
	fmul	%r15, %r13, %r15		# 1417
	fadd	%r14, %r14, %r15		# 1418
	addi	%r15, %r1, 1		# 1419
	sto	%r14, (%r15)		# 1420
	fmul	%r14, %r9, %r9		# 1421
	fmul	%r14, %r2, %r14		# 1422
	fmul	%r15, %r12, %r12		# 1423
	fmul	%r15, %r3, %r15		# 1424
	fadd	%r14, %r14, %r15		# 1425
	fmul	%r15, %r8, %r8		# 1426
	fmul	%r15, %r13, %r15		# 1427
	fadd	%r14, %r14, %r15		# 1428
	addi	%r1, %r1, 2		# 1429
	sto	%r14, (%r1)		# 1430
	movhiz	%r1, 262144		# 1431
	fmul	%r14, %r2, %r7		# 1432
	fmul	%r14, %r14, %r9		# 1433
	fmul	%r15, %r3, %r11		# 1434
	fmul	%r15, %r15, %r12		# 1435
	fadd	%r14, %r14, %r15		# 1436
	fmul	%r15, %r13, %r6		# 1437
	fmul	%r15, %r15, %r8		# 1438
	fadd	%r14, %r14, %r15		# 1439
	fmul	%r1, %r1, %r14		# 1440
	ld	%r14, 1(%r63)		# 1441
	addi	%r15, %r14, 0		# 1442
	sto	%r1, (%r15)		# 1443
	movhiz	%r1, 262144		# 1444
	fmul	%r15, %r2, %r4		# 1445
	fmul	%r15, %r15, %r9		# 1446
	fmul	%r9, %r3, %r10		# 1447
	fmul	%r9, %r9, %r12		# 1448
	fadd	%r15, %r15, %r9		# 1449
	fmul	%r9, %r13, %r5		# 1450
	fmul	%r9, %r9, %r8		# 1451
	fadd	%r15, %r15, %r9		# 1452
	fmul	%r1, %r1, %r15		# 1453
	addi	%r8, %r14, 1		# 1454
	sto	%r1, (%r8)		# 1455
	movhiz	%r1, 262144		# 1456
	fmul	%r2, %r2, %r4		# 1457
	fmul	%r2, %r2, %r7		# 1458
	fmul	%r3, %r3, %r10		# 1459
	fmul	%r3, %r3, %r11		# 1460
	fadd	%r2, %r2, %r3		# 1461
	fmul	%r13, %r13, %r5		# 1462
	fmul	%r13, %r13, %r6		# 1463
	fadd	%r2, %r2, %r13		# 1464
	fmul	%r1, %r1, %r2		# 1465
	addi	%r14, %r14, 2		# 1466
	sto	%r1, (%r14)		# 1467
	addi	%r63, %r63, -1		# 1468
	ld	%r60, (%r63)		# 1469
	jmpu	%r60		# 1470
.globl	read_nth_object.2596
read_nth_object.2596:
	sto	%r60, (%r63)		# 1471
	addi	%r63, %r63, 1		# 1472
	sto	%r1, (%r63)		# 1473
	addi	%r63, %r63, 1		# 1474
	call	%r60, min_caml_read_int		# 1475
	addi	%r63, %r63, -1		# 1476
	cmpeqi	%r2, %r1, -1		# 1477
	jmpzi	%r2, else.15861		# 1478
	movi	%r1, 0		# 1479
	addi	%r63, %r63, -1		# 1480
	ld	%r60, (%r63)		# 1481
	jmpu	%r60		# 1482
else.15861:
	sto	%r1, 1(%r63)		# 1483
	addi	%r63, %r63, 2		# 1484
	call	%r60, min_caml_read_int		# 1485
	addi	%r63, %r63, -2		# 1486
	sto	%r1, 2(%r63)		# 1487
	addi	%r63, %r63, 3		# 1488
	call	%r60, min_caml_read_int		# 1489
	addi	%r63, %r63, -3		# 1490
	sto	%r1, 3(%r63)		# 1491
	addi	%r63, %r63, 4		# 1492
	call	%r60, min_caml_read_int		# 1493
	addi	%r63, %r63, -4		# 1494
	movi	%r2, 0		# 1495
	movi	%r3, 3		# 1496
	sto	%r1, 4(%r63)		# 1497
	mov	%r1, %r3		# 1498
	addi	%r63, %r63, 5		# 1499
	call	%r60, min_caml_create_float_array		# 1500
	addi	%r63, %r63, -5		# 1501
	sto	%r1, 5(%r63)		# 1502
	addi	%r63, %r63, 6		# 1503
	call	%r60, read_float.2433		# 1504
	addi	%r63, %r63, -6		# 1505
	ld	%r2, 5(%r63)		# 1506
	addi	%r3, %r2, 0		# 1507
	sto	%r1, (%r3)		# 1508
	addi	%r63, %r63, 6		# 1509
	call	%r60, read_float.2433		# 1510
	addi	%r63, %r63, -6		# 1511
	ld	%r2, 5(%r63)		# 1512
	addi	%r3, %r2, 1		# 1513
	sto	%r1, (%r3)		# 1514
	addi	%r63, %r63, 6		# 1515
	call	%r60, read_float.2433		# 1516
	addi	%r63, %r63, -6		# 1517
	ld	%r2, 5(%r63)		# 1518
	addi	%r3, %r2, 2		# 1519
	sto	%r1, (%r3)		# 1520
	movi	%r1, 0		# 1521
	movi	%r3, 3		# 1522
	mov	%r2, %r1		# 1523
	mov	%r1, %r3		# 1524
	addi	%r63, %r63, 6		# 1525
	call	%r60, min_caml_create_float_array		# 1526
	addi	%r63, %r63, -6		# 1527
	sto	%r1, 6(%r63)		# 1528
	addi	%r63, %r63, 7		# 1529
	call	%r60, read_float.2433		# 1530
	addi	%r63, %r63, -7		# 1531
	ld	%r2, 6(%r63)		# 1532
	addi	%r3, %r2, 0		# 1533
	sto	%r1, (%r3)		# 1534
	addi	%r63, %r63, 7		# 1535
	call	%r60, read_float.2433		# 1536
	addi	%r63, %r63, -7		# 1537
	ld	%r2, 6(%r63)		# 1538
	addi	%r3, %r2, 1		# 1539
	sto	%r1, (%r3)		# 1540
	addi	%r63, %r63, 7		# 1541
	call	%r60, read_float.2433		# 1542
	addi	%r63, %r63, -7		# 1543
	ld	%r2, 6(%r63)		# 1544
	addi	%r3, %r2, 2		# 1545
	sto	%r1, (%r3)		# 1546
	addi	%r63, %r63, 7		# 1547
	call	%r60, read_float.2433		# 1548
	addi	%r63, %r63, -7		# 1549
	movi	%r2, 0		# 1550
	cmpfgt	%r3, %r2, %r1		# 1551
	jmpi	%r3, else.15862		# 1552
	movi	%r3, 0		# 1553
	jmpui	if_cont.15863		# 1554
else.15862:
	movi	%r3, 1		# 1555
if_cont.15863:
	movi	%r4, 0		# 1556
	movi	%r5, 2		# 1557
	sto	%r1, 7(%r63)		# 1558
	sto	%r2, 8(%r63)		# 1559
	sto	%r3, 9(%r63)		# 1560
	mov	%r2, %r4		# 1561
	mov	%r1, %r5		# 1562
	addi	%r63, %r63, 10		# 1563
	call	%r60, min_caml_create_float_array		# 1564
	addi	%r63, %r63, -10		# 1565
	sto	%r1, 10(%r63)		# 1566
	addi	%r63, %r63, 11		# 1567
	call	%r60, read_float.2433		# 1568
	addi	%r63, %r63, -11		# 1569
	ld	%r2, 10(%r63)		# 1570
	addi	%r3, %r2, 0		# 1571
	sto	%r1, (%r3)		# 1572
	addi	%r63, %r63, 11		# 1573
	call	%r60, read_float.2433		# 1574
	addi	%r63, %r63, -11		# 1575
	ld	%r2, 10(%r63)		# 1576
	addi	%r3, %r2, 1		# 1577
	sto	%r1, (%r3)		# 1578
	movi	%r1, 0		# 1579
	movi	%r3, 3		# 1580
	mov	%r2, %r1		# 1581
	mov	%r1, %r3		# 1582
	addi	%r63, %r63, 11		# 1583
	call	%r60, min_caml_create_float_array		# 1584
	addi	%r63, %r63, -11		# 1585
	sto	%r1, 11(%r63)		# 1586
	addi	%r63, %r63, 12		# 1587
	call	%r60, read_float.2433		# 1588
	addi	%r63, %r63, -12		# 1589
	ld	%r2, 11(%r63)		# 1590
	addi	%r3, %r2, 0		# 1591
	sto	%r1, (%r3)		# 1592
	addi	%r63, %r63, 12		# 1593
	call	%r60, read_float.2433		# 1594
	addi	%r63, %r63, -12		# 1595
	ld	%r2, 11(%r63)		# 1596
	addi	%r3, %r2, 1		# 1597
	sto	%r1, (%r3)		# 1598
	addi	%r63, %r63, 12		# 1599
	call	%r60, read_float.2433		# 1600
	addi	%r63, %r63, -12		# 1601
	ld	%r2, 11(%r63)		# 1602
	addi	%r3, %r2, 2		# 1603
	sto	%r1, (%r3)		# 1604
	movi	%r1, 0		# 1605
	movi	%r3, 3		# 1606
	mov	%r2, %r1		# 1607
	mov	%r1, %r3		# 1608
	addi	%r63, %r63, 12		# 1609
	call	%r60, min_caml_create_float_array		# 1610
	addi	%r63, %r63, -12		# 1611
	ld	%r2, 4(%r63)		# 1612
	cmpeqi	%r3, %r2, 0		# 1613
	sto	%r1, 12(%r63)		# 1614
	jmpzi	%r3, else.15864		# 1615
	jmpui	if_cont.15865		# 1616
else.15864:
	addi	%r63, %r63, 13		# 1617
	call	%r60, read_float.2433		# 1618
	addi	%r63, %r63, -13		# 1619
	movhiz	%r2, 248047		# 1620
	addi	%r2, %r2, 2613		# 1621
	fmul	%r1, %r1, %r2		# 1622
	ld	%r2, 12(%r63)		# 1623
	addi	%r3, %r2, 0		# 1624
	sto	%r1, (%r3)		# 1625
	addi	%r63, %r63, 13		# 1626
	call	%r60, read_float.2433		# 1627
	addi	%r63, %r63, -13		# 1628
	movhiz	%r2, 248047		# 1629
	addi	%r2, %r2, 2613		# 1630
	fmul	%r1, %r1, %r2		# 1631
	ld	%r2, 12(%r63)		# 1632
	addi	%r3, %r2, 1		# 1633
	sto	%r1, (%r3)		# 1634
	addi	%r63, %r63, 13		# 1635
	call	%r60, read_float.2433		# 1636
	addi	%r63, %r63, -13		# 1637
	movhiz	%r2, 248047		# 1638
	addi	%r2, %r2, 2613		# 1639
	fmul	%r1, %r1, %r2		# 1640
	ld	%r2, 12(%r63)		# 1641
	addi	%r3, %r2, 2		# 1642
	sto	%r1, (%r3)		# 1643
if_cont.15865:
	ld	%r1, 2(%r63)		# 1644
	cmpeqi	%r2, %r1, 2		# 1645
	jmpzi	%r2, else.15866		# 1646
	movi	%r2, 1		# 1647
	jmpui	if_cont.15867		# 1648
else.15866:
	ld	%r2, 9(%r63)		# 1649
if_cont.15867:
	movi	%r3, 0		# 1650
	movi	%r4, 4		# 1651
	sto	%r2, 13(%r63)		# 1652
	mov	%r2, %r3		# 1653
	mov	%r1, %r4		# 1654
	addi	%r63, %r63, 14		# 1655
	call	%r60, min_caml_create_float_array		# 1656
	addi	%r63, %r63, -14		# 1657
	mov	%r2, %r62		# 1658
	addi	%r62, %r62, 11		# 1659
	sto	%r1, 10(%r2)		# 1660
	ld	%r1, 12(%r63)		# 1661
	sto	%r1, 9(%r2)		# 1662
	ld	%r3, 11(%r63)		# 1663
	sto	%r3, 8(%r2)		# 1664
	ld	%r3, 10(%r63)		# 1665
	sto	%r3, 7(%r2)		# 1666
	ld	%r3, 13(%r63)		# 1667
	sto	%r3, 6(%r2)		# 1668
	ld	%r3, 6(%r63)		# 1669
	sto	%r3, 5(%r2)		# 1670
	ld	%r3, 5(%r63)		# 1671
	sto	%r3, 4(%r2)		# 1672
	ld	%r4, 4(%r63)		# 1673
	sto	%r4, 3(%r2)		# 1674
	ld	%r5, 3(%r63)		# 1675
	sto	%r5, 2(%r2)		# 1676
	ld	%r5, 2(%r63)		# 1677
	sto	%r5, 1(%r2)		# 1678
	ld	%r6, 1(%r63)		# 1679
	sto	%r6, (%r2)		# 1680
	movi	%r6, min_caml_objects		# 1681
	ld	%r7, (%r63)		# 1682
	add	%r6, %r6, %r7		# 1683
	sto	%r2, (%r6)		# 1684
	cmpeqi	%r2, %r5, 3		# 1685
	jmpzi	%r2, else.15868		# 1686
	addi	%r2, %r3, 0		# 1687
	ld	%r2, (%r2)		# 1688
	movi	%r5, 0		# 1689
	cmpfeq	%r5, %r2, %r5		# 1690
	jmpzi	%r5, else.15870		# 1691
	movi	%r5, 0		# 1692
	jmpui	if_cont.15871		# 1693
else.15870:
	movi	%r5, 0		# 1694
	cmpfeq	%r5, %r2, %r5		# 1695
	jmpzi	%r5, else.15872		# 1696
	movi	%r5, 0		# 1697
	jmpui	if_cont.15873		# 1698
else.15872:
	movi	%r5, 0		# 1699
	cmpfgt	%r5, %r2, %r5		# 1700
	jmpi	%r5, else.15874		# 1701
	movhiz	%r5, 784384		# 1702
	jmpui	if_cont.15875		# 1703
else.15874:
	movhiz	%r5, 260096		# 1704
if_cont.15875:
if_cont.15873:
	fmul	%r2, %r2, %r2		# 1705
	finv	%r2, %r2		# 1706
	fmul	%r5, %r5, %r2		# 1707
if_cont.15871:
	addi	%r2, %r3, 0		# 1708
	sto	%r5, (%r2)		# 1709
	addi	%r2, %r3, 1		# 1710
	ld	%r2, (%r2)		# 1711
	movi	%r5, 0		# 1712
	cmpfeq	%r5, %r2, %r5		# 1713
	jmpzi	%r5, else.15876		# 1714
	movi	%r5, 0		# 1715
	jmpui	if_cont.15877		# 1716
else.15876:
	movi	%r5, 0		# 1717
	cmpfeq	%r5, %r2, %r5		# 1718
	jmpzi	%r5, else.15878		# 1719
	movi	%r5, 0		# 1720
	jmpui	if_cont.15879		# 1721
else.15878:
	movi	%r5, 0		# 1722
	cmpfgt	%r5, %r2, %r5		# 1723
	jmpi	%r5, else.15880		# 1724
	movhiz	%r5, 784384		# 1725
	jmpui	if_cont.15881		# 1726
else.15880:
	movhiz	%r5, 260096		# 1727
if_cont.15881:
if_cont.15879:
	fmul	%r2, %r2, %r2		# 1728
	finv	%r2, %r2		# 1729
	fmul	%r5, %r5, %r2		# 1730
if_cont.15877:
	addi	%r2, %r3, 1		# 1731
	sto	%r5, (%r2)		# 1732
	addi	%r2, %r3, 2		# 1733
	ld	%r2, (%r2)		# 1734
	movi	%r5, 0		# 1735
	cmpfeq	%r5, %r2, %r5		# 1736
	jmpzi	%r5, else.15882		# 1737
	movi	%r5, 0		# 1738
	jmpui	if_cont.15883		# 1739
else.15882:
	movi	%r5, 0		# 1740
	cmpfeq	%r5, %r2, %r5		# 1741
	jmpzi	%r5, else.15884		# 1742
	movi	%r5, 0		# 1743
	jmpui	if_cont.15885		# 1744
else.15884:
	movi	%r5, 0		# 1745
	cmpfgt	%r5, %r2, %r5		# 1746
	jmpi	%r5, else.15886		# 1747
	movhiz	%r5, 784384		# 1748
	jmpui	if_cont.15887		# 1749
else.15886:
	movhiz	%r5, 260096		# 1750
if_cont.15887:
if_cont.15885:
	fmul	%r2, %r2, %r2		# 1751
	finv	%r2, %r2		# 1752
	fmul	%r5, %r5, %r2		# 1753
if_cont.15883:
	addi	%r2, %r3, 2		# 1754
	sto	%r5, (%r2)		# 1755
	jmpui	if_cont.15869		# 1756
else.15868:
	cmpeqi	%r5, %r5, 2		# 1757
	jmpzi	%r5, else.15888		# 1758
	ld	%r2, 7(%r63)		# 1759
	ld	%r5, 8(%r63)		# 1760
	cmpfgt	%r5, %r5, %r2		# 1761
	jmpi	%r5, else.15890		# 1762
	movi	%r2, 1		# 1763
	jmpui	if_cont.15891		# 1764
else.15890:
	movi	%r2, 0		# 1765
if_cont.15891:
	mov	%r1, %r3		# 1766
	addi	%r63, %r63, 14		# 1767
	call	%r60, vecunit_sgn.2488		# 1768
	addi	%r63, %r63, -14		# 1769
	jmpui	if_cont.15889		# 1770
else.15888:
if_cont.15889:
if_cont.15869:
	ld	%r1, 4(%r63)		# 1771
	cmpeqi	%r1, %r1, 0		# 1772
	jmpzi	%r1, else.15892		# 1773
	jmpui	if_cont.15893		# 1774
else.15892:
	ld	%r1, 5(%r63)		# 1775
	ld	%r2, 12(%r63)		# 1776
	addi	%r63, %r63, 14		# 1777
	call	%r60, rotate_quadratic_matrix.2593		# 1778
	addi	%r63, %r63, -14		# 1779
if_cont.15893:
	movi	%r1, 1		# 1780
	addi	%r63, %r63, -1		# 1781
	ld	%r60, (%r63)		# 1782
	jmpu	%r60		# 1783
.globl	read_object.2598
read_object.2598:
	sto	%r60, (%r63)		# 1784
	addi	%r63, %r63, 1		# 1785
	movi	%r2, 60		# 1786
	cmpgt	%r2, %r2, %r1		# 1787
	jmpi	%r2, else.15894		# 1788
	addi	%r63, %r63, -1		# 1789
	ld	%r60, (%r63)		# 1790
	jmpu	%r60		# 1791
else.15894:
	sto	%r1, (%r63)		# 1792
	addi	%r63, %r63, 1		# 1793
	call	%r60, read_nth_object.2596		# 1794
	addi	%r63, %r63, -1		# 1795
	cmpeqi	%r1, %r1, 0		# 1796
	jmpzi	%r1, else.15896		# 1797
	movi	%r1, min_caml_n_objects		# 1798
	addi	%r1, %r1, 0		# 1799
	ld	%r2, (%r63)		# 1800
	sto	%r2, (%r1)		# 1801
	addi	%r63, %r63, -1		# 1802
	ld	%r60, (%r63)		# 1803
	jmpu	%r60		# 1804
else.15896:
	ld	%r1, (%r63)		# 1805
	addi	%r1, %r1, 1		# 1806
	movi	%r2, 60		# 1807
	cmpgt	%r2, %r2, %r1		# 1808
	jmpi	%r2, else.15898		# 1809
	addi	%r63, %r63, -1		# 1810
	ld	%r60, (%r63)		# 1811
	jmpu	%r60		# 1812
else.15898:
	sto	%r1, 1(%r63)		# 1813
	addi	%r63, %r63, 2		# 1814
	call	%r60, read_nth_object.2596		# 1815
	addi	%r63, %r63, -2		# 1816
	cmpeqi	%r1, %r1, 0		# 1817
	jmpzi	%r1, else.15900		# 1818
	movi	%r1, min_caml_n_objects		# 1819
	addi	%r1, %r1, 0		# 1820
	ld	%r2, 1(%r63)		# 1821
	sto	%r2, (%r1)		# 1822
	addi	%r63, %r63, -1		# 1823
	ld	%r60, (%r63)		# 1824
	jmpu	%r60		# 1825
else.15900:
	ld	%r1, 1(%r63)		# 1826
	addi	%r1, %r1, 1		# 1827
	movi	%r2, 60		# 1828
	cmpgt	%r2, %r2, %r1		# 1829
	jmpi	%r2, else.15902		# 1830
	addi	%r63, %r63, -1		# 1831
	ld	%r60, (%r63)		# 1832
	jmpu	%r60		# 1833
else.15902:
	sto	%r1, 2(%r63)		# 1834
	addi	%r63, %r63, 3		# 1835
	call	%r60, read_nth_object.2596		# 1836
	addi	%r63, %r63, -3		# 1837
	cmpeqi	%r1, %r1, 0		# 1838
	jmpzi	%r1, else.15904		# 1839
	movi	%r1, min_caml_n_objects		# 1840
	addi	%r1, %r1, 0		# 1841
	ld	%r2, 2(%r63)		# 1842
	sto	%r2, (%r1)		# 1843
	addi	%r63, %r63, -1		# 1844
	ld	%r60, (%r63)		# 1845
	jmpu	%r60		# 1846
else.15904:
	ld	%r1, 2(%r63)		# 1847
	addi	%r1, %r1, 1		# 1848
	movi	%r2, 60		# 1849
	cmpgt	%r2, %r2, %r1		# 1850
	jmpi	%r2, else.15906		# 1851
	addi	%r63, %r63, -1		# 1852
	ld	%r60, (%r63)		# 1853
	jmpu	%r60		# 1854
else.15906:
	sto	%r1, 3(%r63)		# 1855
	addi	%r63, %r63, 4		# 1856
	call	%r60, read_nth_object.2596		# 1857
	addi	%r63, %r63, -4		# 1858
	cmpeqi	%r1, %r1, 0		# 1859
	jmpzi	%r1, else.15908		# 1860
	movi	%r1, min_caml_n_objects		# 1861
	addi	%r1, %r1, 0		# 1862
	ld	%r2, 3(%r63)		# 1863
	sto	%r2, (%r1)		# 1864
	addi	%r63, %r63, -1		# 1865
	ld	%r60, (%r63)		# 1866
	jmpu	%r60		# 1867
else.15908:
	ld	%r1, 3(%r63)		# 1868
	addi	%r1, %r1, 1		# 1869
	addi	%r63, %r63, -1		# 1870
	ld	%r60, (%r63)		# 1871
	jmpui	read_object.2598		# 1872
.globl	read_net_item.2602
read_net_item.2602:
	sto	%r60, (%r63)		# 1873
	addi	%r63, %r63, 1		# 1874
	sto	%r1, (%r63)		# 1875
	addi	%r63, %r63, 1		# 1876
	call	%r60, min_caml_read_int		# 1877
	addi	%r63, %r63, -1		# 1878
	cmpeqi	%r2, %r1, -1		# 1879
	jmpzi	%r2, else.15910		# 1880
	ld	%r1, (%r63)		# 1881
	addi	%r1, %r1, 1		# 1882
	movi	%r2, -1		# 1883
	addi	%r63, %r63, -1		# 1884
	ld	%r60, (%r63)		# 1885
	jmpui	min_caml_create_array		# 1886
else.15910:
	ld	%r2, (%r63)		# 1887
	addi	%r3, %r2, 1		# 1888
	sto	%r1, 1(%r63)		# 1889
	sto	%r3, 2(%r63)		# 1890
	addi	%r63, %r63, 3		# 1891
	call	%r60, min_caml_read_int		# 1892
	addi	%r63, %r63, -3		# 1893
	cmpeqi	%r2, %r1, -1		# 1894
	jmpzi	%r2, else.15911		# 1895
	ld	%r1, 2(%r63)		# 1896
	addi	%r1, %r1, 1		# 1897
	movi	%r2, -1		# 1898
	addi	%r63, %r63, 3		# 1899
	call	%r60, min_caml_create_array		# 1900
	addi	%r63, %r63, -3		# 1901
	jmpui	if_cont.15912		# 1902
else.15911:
	ld	%r2, 2(%r63)		# 1903
	addi	%r3, %r2, 1		# 1904
	sto	%r1, 3(%r63)		# 1905
	sto	%r3, 4(%r63)		# 1906
	addi	%r63, %r63, 5		# 1907
	call	%r60, min_caml_read_int		# 1908
	addi	%r63, %r63, -5		# 1909
	cmpeqi	%r2, %r1, -1		# 1910
	jmpzi	%r2, else.15913		# 1911
	ld	%r1, 4(%r63)		# 1912
	addi	%r1, %r1, 1		# 1913
	movi	%r2, -1		# 1914
	addi	%r63, %r63, 5		# 1915
	call	%r60, min_caml_create_array		# 1916
	addi	%r63, %r63, -5		# 1917
	jmpui	if_cont.15914		# 1918
else.15913:
	ld	%r2, 4(%r63)		# 1919
	addi	%r3, %r2, 1		# 1920
	sto	%r1, 5(%r63)		# 1921
	sto	%r3, 6(%r63)		# 1922
	addi	%r63, %r63, 7		# 1923
	call	%r60, min_caml_read_int		# 1924
	addi	%r63, %r63, -7		# 1925
	cmpeqi	%r2, %r1, -1		# 1926
	jmpzi	%r2, else.15915		# 1927
	ld	%r1, 6(%r63)		# 1928
	addi	%r1, %r1, 1		# 1929
	movi	%r2, -1		# 1930
	addi	%r63, %r63, 7		# 1931
	call	%r60, min_caml_create_array		# 1932
	addi	%r63, %r63, -7		# 1933
	jmpui	if_cont.15916		# 1934
else.15915:
	ld	%r2, 6(%r63)		# 1935
	addi	%r3, %r2, 1		# 1936
	sto	%r1, 7(%r63)		# 1937
	mov	%r1, %r3		# 1938
	addi	%r63, %r63, 8		# 1939
	call	%r60, read_net_item.2602		# 1940
	addi	%r63, %r63, -8		# 1941
	ld	%r2, 6(%r63)		# 1942
	add	%r2, %r1, %r2		# 1943
	ld	%r3, 7(%r63)		# 1944
	sto	%r3, (%r2)		# 1945
if_cont.15916:
	ld	%r2, 4(%r63)		# 1946
	add	%r2, %r1, %r2		# 1947
	ld	%r3, 5(%r63)		# 1948
	sto	%r3, (%r2)		# 1949
if_cont.15914:
	ld	%r2, 2(%r63)		# 1950
	add	%r2, %r1, %r2		# 1951
	ld	%r3, 3(%r63)		# 1952
	sto	%r3, (%r2)		# 1953
if_cont.15912:
	ld	%r2, (%r63)		# 1954
	add	%r2, %r1, %r2		# 1955
	ld	%r3, 1(%r63)		# 1956
	sto	%r3, (%r2)		# 1957
	addi	%r63, %r63, -1		# 1958
	ld	%r60, (%r63)		# 1959
	jmpu	%r60		# 1960
.globl	read_or_network.2604
read_or_network.2604:
	sto	%r60, (%r63)		# 1961
	addi	%r63, %r63, 1		# 1962
	sto	%r1, (%r63)		# 1963
	addi	%r63, %r63, 1		# 1964
	call	%r60, min_caml_read_int		# 1965
	addi	%r63, %r63, -1		# 1966
	cmpeqi	%r2, %r1, -1		# 1967
	jmpzi	%r2, else.15917		# 1968
	movi	%r1, 1		# 1969
	movi	%r2, -1		# 1970
	addi	%r63, %r63, 1		# 1971
	call	%r60, min_caml_create_array		# 1972
	addi	%r63, %r63, -1		# 1973
	mov	%r2, %r1		# 1974
	jmpui	if_cont.15918		# 1975
else.15917:
	sto	%r1, 1(%r63)		# 1976
	addi	%r63, %r63, 2		# 1977
	call	%r60, min_caml_read_int		# 1978
	addi	%r63, %r63, -2		# 1979
	cmpeqi	%r2, %r1, -1		# 1980
	jmpzi	%r2, else.15919		# 1981
	movi	%r1, 2		# 1982
	movi	%r2, -1		# 1983
	addi	%r63, %r63, 2		# 1984
	call	%r60, min_caml_create_array		# 1985
	addi	%r63, %r63, -2		# 1986
	jmpui	if_cont.15920		# 1987
else.15919:
	sto	%r1, 2(%r63)		# 1988
	addi	%r63, %r63, 3		# 1989
	call	%r60, min_caml_read_int		# 1990
	addi	%r63, %r63, -3		# 1991
	cmpeqi	%r2, %r1, -1		# 1992
	jmpzi	%r2, else.15921		# 1993
	movi	%r1, 3		# 1994
	movi	%r2, -1		# 1995
	addi	%r63, %r63, 3		# 1996
	call	%r60, min_caml_create_array		# 1997
	addi	%r63, %r63, -3		# 1998
	jmpui	if_cont.15922		# 1999
else.15921:
	movi	%r2, 3		# 2000
	sto	%r1, 3(%r63)		# 2001
	mov	%r1, %r2		# 2002
	addi	%r63, %r63, 4		# 2003
	call	%r60, read_net_item.2602		# 2004
	addi	%r63, %r63, -4		# 2005
	addi	%r2, %r1, 2		# 2006
	ld	%r3, 3(%r63)		# 2007
	sto	%r3, (%r2)		# 2008
if_cont.15922:
	addi	%r2, %r1, 1		# 2009
	ld	%r3, 2(%r63)		# 2010
	sto	%r3, (%r2)		# 2011
if_cont.15920:
	addi	%r2, %r1, 0		# 2012
	ld	%r3, 1(%r63)		# 2013
	sto	%r3, (%r2)		# 2014
	mov	%r2, %r1		# 2015
if_cont.15918:
	addi	%r1, %r2, 0		# 2016
	ld	%r1, (%r1)		# 2017
	cmpeqi	%r1, %r1, -1		# 2018
	jmpzi	%r1, else.15923		# 2019
	ld	%r1, (%r63)		# 2020
	addi	%r1, %r1, 1		# 2021
	addi	%r63, %r63, -1		# 2022
	ld	%r60, (%r63)		# 2023
	jmpui	min_caml_create_array		# 2024
else.15923:
	ld	%r1, (%r63)		# 2025
	addi	%r3, %r1, 1		# 2026
	sto	%r2, 4(%r63)		# 2027
	sto	%r3, 5(%r63)		# 2028
	addi	%r63, %r63, 6		# 2029
	call	%r60, min_caml_read_int		# 2030
	addi	%r63, %r63, -6		# 2031
	cmpeqi	%r2, %r1, -1		# 2032
	jmpzi	%r2, else.15924		# 2033
	movi	%r1, 1		# 2034
	movi	%r2, -1		# 2035
	addi	%r63, %r63, 6		# 2036
	call	%r60, min_caml_create_array		# 2037
	addi	%r63, %r63, -6		# 2038
	mov	%r2, %r1		# 2039
	jmpui	if_cont.15925		# 2040
else.15924:
	sto	%r1, 6(%r63)		# 2041
	addi	%r63, %r63, 7		# 2042
	call	%r60, min_caml_read_int		# 2043
	addi	%r63, %r63, -7		# 2044
	cmpeqi	%r2, %r1, -1		# 2045
	jmpzi	%r2, else.15926		# 2046
	movi	%r1, 2		# 2047
	movi	%r2, -1		# 2048
	addi	%r63, %r63, 7		# 2049
	call	%r60, min_caml_create_array		# 2050
	addi	%r63, %r63, -7		# 2051
	jmpui	if_cont.15927		# 2052
else.15926:
	movi	%r2, 2		# 2053
	sto	%r1, 7(%r63)		# 2054
	mov	%r1, %r2		# 2055
	addi	%r63, %r63, 8		# 2056
	call	%r60, read_net_item.2602		# 2057
	addi	%r63, %r63, -8		# 2058
	addi	%r2, %r1, 1		# 2059
	ld	%r3, 7(%r63)		# 2060
	sto	%r3, (%r2)		# 2061
if_cont.15927:
	addi	%r2, %r1, 0		# 2062
	ld	%r3, 6(%r63)		# 2063
	sto	%r3, (%r2)		# 2064
	mov	%r2, %r1		# 2065
if_cont.15925:
	addi	%r1, %r2, 0		# 2066
	ld	%r1, (%r1)		# 2067
	cmpeqi	%r1, %r1, -1		# 2068
	jmpzi	%r1, else.15928		# 2069
	ld	%r1, 5(%r63)		# 2070
	addi	%r1, %r1, 1		# 2071
	addi	%r63, %r63, 8		# 2072
	call	%r60, min_caml_create_array		# 2073
	addi	%r63, %r63, -8		# 2074
	jmpui	if_cont.15929		# 2075
else.15928:
	ld	%r1, 5(%r63)		# 2076
	addi	%r3, %r1, 1		# 2077
	sto	%r2, 8(%r63)		# 2078
	mov	%r1, %r3		# 2079
	addi	%r63, %r63, 9		# 2080
	call	%r60, read_or_network.2604		# 2081
	addi	%r63, %r63, -9		# 2082
	ld	%r2, 5(%r63)		# 2083
	add	%r2, %r1, %r2		# 2084
	ld	%r3, 8(%r63)		# 2085
	sto	%r3, (%r2)		# 2086
if_cont.15929:
	ld	%r2, (%r63)		# 2087
	add	%r2, %r1, %r2		# 2088
	ld	%r3, 4(%r63)		# 2089
	sto	%r3, (%r2)		# 2090
	addi	%r63, %r63, -1		# 2091
	ld	%r60, (%r63)		# 2092
	jmpu	%r60		# 2093
.globl	read_and_network.2606
read_and_network.2606:
	sto	%r60, (%r63)		# 2094
	addi	%r63, %r63, 1		# 2095
	sto	%r1, (%r63)		# 2096
	addi	%r63, %r63, 1		# 2097
	call	%r60, min_caml_read_int		# 2098
	addi	%r63, %r63, -1		# 2099
	cmpeqi	%r2, %r1, -1		# 2100
	jmpzi	%r2, else.15930		# 2101
	movi	%r1, 1		# 2102
	movi	%r2, -1		# 2103
	addi	%r63, %r63, 1		# 2104
	call	%r60, min_caml_create_array		# 2105
	addi	%r63, %r63, -1		# 2106
	jmpui	if_cont.15931		# 2107
else.15930:
	sto	%r1, 1(%r63)		# 2108
	addi	%r63, %r63, 2		# 2109
	call	%r60, min_caml_read_int		# 2110
	addi	%r63, %r63, -2		# 2111
	cmpeqi	%r2, %r1, -1		# 2112
	jmpzi	%r2, else.15932		# 2113
	movi	%r1, 2		# 2114
	movi	%r2, -1		# 2115
	addi	%r63, %r63, 2		# 2116
	call	%r60, min_caml_create_array		# 2117
	addi	%r63, %r63, -2		# 2118
	jmpui	if_cont.15933		# 2119
else.15932:
	sto	%r1, 2(%r63)		# 2120
	addi	%r63, %r63, 3		# 2121
	call	%r60, min_caml_read_int		# 2122
	addi	%r63, %r63, -3		# 2123
	cmpeqi	%r2, %r1, -1		# 2124
	jmpzi	%r2, else.15934		# 2125
	movi	%r1, 3		# 2126
	movi	%r2, -1		# 2127
	addi	%r63, %r63, 3		# 2128
	call	%r60, min_caml_create_array		# 2129
	addi	%r63, %r63, -3		# 2130
	jmpui	if_cont.15935		# 2131
else.15934:
	movi	%r2, 3		# 2132
	sto	%r1, 3(%r63)		# 2133
	mov	%r1, %r2		# 2134
	addi	%r63, %r63, 4		# 2135
	call	%r60, read_net_item.2602		# 2136
	addi	%r63, %r63, -4		# 2137
	addi	%r2, %r1, 2		# 2138
	ld	%r3, 3(%r63)		# 2139
	sto	%r3, (%r2)		# 2140
if_cont.15935:
	addi	%r2, %r1, 1		# 2141
	ld	%r3, 2(%r63)		# 2142
	sto	%r3, (%r2)		# 2143
if_cont.15933:
	addi	%r2, %r1, 0		# 2144
	ld	%r3, 1(%r63)		# 2145
	sto	%r3, (%r2)		# 2146
if_cont.15931:
	addi	%r2, %r1, 0		# 2147
	ld	%r2, (%r2)		# 2148
	cmpeqi	%r2, %r2, -1		# 2149
	jmpzi	%r2, else.15936		# 2150
	addi	%r63, %r63, -1		# 2151
	ld	%r60, (%r63)		# 2152
	jmpu	%r60		# 2153
else.15936:
	movi	%r2, min_caml_and_net		# 2154
	ld	%r3, (%r63)		# 2155
	add	%r2, %r2, %r3		# 2156
	sto	%r1, (%r2)		# 2157
	addi	%r3, %r3, 1		# 2158
	sto	%r3, 4(%r63)		# 2159
	addi	%r63, %r63, 5		# 2160
	call	%r60, min_caml_read_int		# 2161
	addi	%r63, %r63, -5		# 2162
	cmpeqi	%r2, %r1, -1		# 2163
	jmpzi	%r2, else.15938		# 2164
	movi	%r1, 1		# 2165
	movi	%r2, -1		# 2166
	addi	%r63, %r63, 5		# 2167
	call	%r60, min_caml_create_array		# 2168
	addi	%r63, %r63, -5		# 2169
	jmpui	if_cont.15939		# 2170
else.15938:
	sto	%r1, 5(%r63)		# 2171
	addi	%r63, %r63, 6		# 2172
	call	%r60, min_caml_read_int		# 2173
	addi	%r63, %r63, -6		# 2174
	cmpeqi	%r2, %r1, -1		# 2175
	jmpzi	%r2, else.15940		# 2176
	movi	%r1, 2		# 2177
	movi	%r2, -1		# 2178
	addi	%r63, %r63, 6		# 2179
	call	%r60, min_caml_create_array		# 2180
	addi	%r63, %r63, -6		# 2181
	jmpui	if_cont.15941		# 2182
else.15940:
	movi	%r2, 2		# 2183
	sto	%r1, 6(%r63)		# 2184
	mov	%r1, %r2		# 2185
	addi	%r63, %r63, 7		# 2186
	call	%r60, read_net_item.2602		# 2187
	addi	%r63, %r63, -7		# 2188
	addi	%r2, %r1, 1		# 2189
	ld	%r3, 6(%r63)		# 2190
	sto	%r3, (%r2)		# 2191
if_cont.15941:
	addi	%r2, %r1, 0		# 2192
	ld	%r3, 5(%r63)		# 2193
	sto	%r3, (%r2)		# 2194
if_cont.15939:
	addi	%r2, %r1, 0		# 2195
	ld	%r2, (%r2)		# 2196
	cmpeqi	%r2, %r2, -1		# 2197
	jmpzi	%r2, else.15942		# 2198
	addi	%r63, %r63, -1		# 2199
	ld	%r60, (%r63)		# 2200
	jmpu	%r60		# 2201
else.15942:
	movi	%r2, min_caml_and_net		# 2202
	ld	%r3, 4(%r63)		# 2203
	add	%r2, %r2, %r3		# 2204
	sto	%r1, (%r2)		# 2205
	addi	%r1, %r3, 1		# 2206
	addi	%r63, %r63, -1		# 2207
	ld	%r60, (%r63)		# 2208
	jmpui	read_and_network.2606		# 2209
.globl	solver_rect_surface.2610
solver_rect_surface.2610:
	add	%r9, %r2, %r3		# 2210
	ld	%r9, (%r9)		# 2211
	movi	%r10, 0		# 2212
	cmpfeq	%r9, %r9, %r10		# 2213
	jmpzi	%r9, else.15944		# 2214
	movi	%r1, 0		# 2215
	jmpu	%r60		# 2216
else.15944:
	ld	%r9, 4(%r1)		# 2217
	ld	%r1, 6(%r1)		# 2218
	add	%r10, %r2, %r3		# 2219
	ld	%r10, (%r10)		# 2220
	movi	%r11, 0		# 2221
	cmpfgt	%r12, %r11, %r10		# 2222
	jmpi	%r12, else.15945		# 2223
	movi	%r12, 0		# 2224
	jmpui	if_cont.15946		# 2225
else.15945:
	movi	%r12, 1		# 2226
if_cont.15946:
	cmpeqi	%r1, %r1, 0		# 2227
	jmpzi	%r1, else.15947		# 2228
	jmpui	if_cont.15948		# 2229
else.15947:
	cmpfgt	%r11, %r11, %r10		# 2230
	jmpi	%r11, else.15949		# 2231
	movi	%r12, 1		# 2232
	jmpui	if_cont.15950		# 2233
else.15949:
	movi	%r12, 0		# 2234
if_cont.15950:
if_cont.15948:
	add	%r1, %r9, %r3		# 2235
	ld	%r1, (%r1)		# 2236
	cmpeqi	%r12, %r12, 0		# 2237
	jmpzi	%r12, else.15951		# 2238
	fneg	%r1, %r1		# 2239
	jmpui	if_cont.15952		# 2240
else.15951:
if_cont.15952:
	fsub	%r1, %r1, %r6		# 2241
	add	%r3, %r2, %r3		# 2242
	ld	%r3, (%r3)		# 2243
	finv	%r3, %r3		# 2244
	fmul	%r1, %r1, %r3		# 2245
	add	%r3, %r2, %r4		# 2246
	ld	%r3, (%r3)		# 2247
	fmul	%r3, %r1, %r3		# 2248
	fadd	%r3, %r3, %r7		# 2249
	fabs	%r3, %r3		# 2250
	add	%r4, %r9, %r4		# 2251
	ld	%r4, (%r4)		# 2252
	cmpfgt	%r4, %r4, %r3		# 2253
	jmpi	%r4, else.15953		# 2254
	movi	%r1, 0		# 2255
	jmpu	%r60		# 2256
else.15953:
	add	%r2, %r2, %r5		# 2257
	ld	%r2, (%r2)		# 2258
	fmul	%r2, %r1, %r2		# 2259
	fadd	%r2, %r2, %r8		# 2260
	fabs	%r2, %r2		# 2261
	add	%r9, %r9, %r5		# 2262
	ld	%r3, (%r9)		# 2263
	cmpfgt	%r3, %r3, %r2		# 2264
	jmpi	%r3, else.15954		# 2265
	movi	%r1, 0		# 2266
	jmpu	%r60		# 2267
else.15954:
	movi	%r2, min_caml_solver_dist		# 2268
	addi	%r2, %r2, 0		# 2269
	sto	%r1, (%r2)		# 2270
	movi	%r1, 1		# 2271
	jmpu	%r60		# 2272
.globl	solver_surface.2625
solver_surface.2625:
	ld	%r1, 4(%r1)		# 2273
	addi	%r6, %r2, 0		# 2274
	ld	%r6, (%r6)		# 2275
	addi	%r7, %r1, 0		# 2276
	ld	%r7, (%r7)		# 2277
	fmul	%r6, %r6, %r7		# 2278
	addi	%r7, %r2, 1		# 2279
	ld	%r7, (%r7)		# 2280
	addi	%r8, %r1, 1		# 2281
	ld	%r8, (%r8)		# 2282
	fmul	%r7, %r7, %r8		# 2283
	fadd	%r6, %r6, %r7		# 2284
	addi	%r2, %r2, 2		# 2285
	ld	%r2, (%r2)		# 2286
	addi	%r7, %r1, 2		# 2287
	ld	%r7, (%r7)		# 2288
	fmul	%r2, %r2, %r7		# 2289
	fadd	%r6, %r6, %r2		# 2290
	movi	%r2, 0		# 2291
	cmpfgt	%r2, %r6, %r2		# 2292
	jmpi	%r2, else.15955		# 2293
	movi	%r1, 0		# 2294
	jmpu	%r60		# 2295
else.15955:
	movi	%r2, min_caml_solver_dist		# 2296
	addi	%r7, %r1, 0		# 2297
	ld	%r7, (%r7)		# 2298
	fmul	%r7, %r7, %r3		# 2299
	addi	%r3, %r1, 1		# 2300
	ld	%r3, (%r3)		# 2301
	fmul	%r3, %r3, %r4		# 2302
	fadd	%r7, %r7, %r3		# 2303
	addi	%r1, %r1, 2		# 2304
	ld	%r1, (%r1)		# 2305
	fmul	%r1, %r1, %r5		# 2306
	fadd	%r7, %r7, %r1		# 2307
	fneg	%r7, %r7		# 2308
	finv	%r6, %r6		# 2309
	fmul	%r7, %r7, %r6		# 2310
	addi	%r2, %r2, 0		# 2311
	sto	%r7, (%r2)		# 2312
	movi	%r1, 1		# 2313
	jmpu	%r60		# 2314
.globl	quadratic.2631
quadratic.2631:
	fmul	%r5, %r2, %r2		# 2315
	ld	%r6, 4(%r1)		# 2316
	addi	%r6, %r6, 0		# 2317
	ld	%r6, (%r6)		# 2318
	fmul	%r5, %r5, %r6		# 2319
	fmul	%r6, %r3, %r3		# 2320
	ld	%r7, 4(%r1)		# 2321
	addi	%r7, %r7, 1		# 2322
	ld	%r7, (%r7)		# 2323
	fmul	%r6, %r6, %r7		# 2324
	fadd	%r5, %r5, %r6		# 2325
	fmul	%r6, %r4, %r4		# 2326
	ld	%r7, 4(%r1)		# 2327
	addi	%r7, %r7, 2		# 2328
	ld	%r7, (%r7)		# 2329
	fmul	%r6, %r6, %r7		# 2330
	fadd	%r5, %r5, %r6		# 2331
	ld	%r6, 3(%r1)		# 2332
	cmpeqi	%r6, %r6, 0		# 2333
	jmpzi	%r6, else.15956		# 2334
	mov	%r1, %r5		# 2335
	jmpu	%r60		# 2336
else.15956:
	fmul	%r6, %r3, %r4		# 2337
	ld	%r7, 9(%r1)		# 2338
	addi	%r7, %r7, 0		# 2339
	ld	%r7, (%r7)		# 2340
	fmul	%r6, %r6, %r7		# 2341
	fadd	%r5, %r5, %r6		# 2342
	fmul	%r4, %r4, %r2		# 2343
	ld	%r6, 9(%r1)		# 2344
	addi	%r6, %r6, 1		# 2345
	ld	%r6, (%r6)		# 2346
	fmul	%r4, %r4, %r6		# 2347
	fadd	%r5, %r5, %r4		# 2348
	fmul	%r2, %r2, %r3		# 2349
	ld	%r1, 9(%r1)		# 2350
	addi	%r1, %r1, 2		# 2351
	ld	%r1, (%r1)		# 2352
	fmul	%r2, %r2, %r1		# 2353
	fadd	%r1, %r5, %r2		# 2354
	jmpu	%r60		# 2355
.globl	bilinear.2636
bilinear.2636:
	fmul	%r8, %r2, %r5		# 2356
	ld	%r9, 4(%r1)		# 2357
	addi	%r9, %r9, 0		# 2358
	ld	%r9, (%r9)		# 2359
	fmul	%r8, %r8, %r9		# 2360
	fmul	%r9, %r3, %r6		# 2361
	ld	%r10, 4(%r1)		# 2362
	addi	%r10, %r10, 1		# 2363
	ld	%r10, (%r10)		# 2364
	fmul	%r9, %r9, %r10		# 2365
	fadd	%r8, %r8, %r9		# 2366
	fmul	%r9, %r4, %r7		# 2367
	ld	%r10, 4(%r1)		# 2368
	addi	%r10, %r10, 2		# 2369
	ld	%r10, (%r10)		# 2370
	fmul	%r9, %r9, %r10		# 2371
	fadd	%r8, %r8, %r9		# 2372
	ld	%r9, 3(%r1)		# 2373
	cmpeqi	%r9, %r9, 0		# 2374
	jmpzi	%r9, else.15957		# 2375
	mov	%r1, %r8		# 2376
	jmpu	%r60		# 2377
else.15957:
	fmul	%r9, %r4, %r6		# 2378
	fmul	%r10, %r3, %r7		# 2379
	fadd	%r9, %r9, %r10		# 2380
	ld	%r10, 9(%r1)		# 2381
	addi	%r10, %r10, 0		# 2382
	ld	%r10, (%r10)		# 2383
	fmul	%r9, %r9, %r10		# 2384
	fmul	%r7, %r2, %r7		# 2385
	fmul	%r4, %r4, %r5		# 2386
	fadd	%r7, %r7, %r4		# 2387
	ld	%r4, 9(%r1)		# 2388
	addi	%r4, %r4, 1		# 2389
	ld	%r4, (%r4)		# 2390
	fmul	%r7, %r7, %r4		# 2391
	fadd	%r9, %r9, %r7		# 2392
	fmul	%r2, %r2, %r6		# 2393
	fmul	%r3, %r3, %r5		# 2394
	fadd	%r2, %r2, %r3		# 2395
	ld	%r1, 9(%r1)		# 2396
	addi	%r1, %r1, 2		# 2397
	ld	%r1, (%r1)		# 2398
	fmul	%r2, %r2, %r1		# 2399
	fadd	%r9, %r9, %r2		# 2400
	movhiz	%r1, 258048		# 2401
	fmul	%r9, %r9, %r1		# 2402
	fadd	%r1, %r8, %r9		# 2403
	jmpu	%r60		# 2404
.globl	solver_second.2644
solver_second.2644:
	sto	%r60, (%r63)		# 2405
	addi	%r63, %r63, 1		# 2406
	addi	%r6, %r2, 0		# 2407
	ld	%r6, (%r6)		# 2408
	addi	%r7, %r2, 1		# 2409
	ld	%r7, (%r7)		# 2410
	addi	%r8, %r2, 2		# 2411
	ld	%r8, (%r8)		# 2412
	sto	%r5, (%r63)		# 2413
	sto	%r4, 1(%r63)		# 2414
	sto	%r3, 2(%r63)		# 2415
	sto	%r1, 3(%r63)		# 2416
	sto	%r2, 4(%r63)		# 2417
	mov	%r4, %r8		# 2418
	mov	%r3, %r7		# 2419
	mov	%r2, %r6		# 2420
	addi	%r63, %r63, 5		# 2421
	call	%r60, quadratic.2631		# 2422
	addi	%r63, %r63, -5		# 2423
	movi	%r2, 0		# 2424
	cmpfeq	%r2, %r1, %r2		# 2425
	jmpzi	%r2, else.15958		# 2426
	movi	%r1, 0		# 2427
	addi	%r63, %r63, -1		# 2428
	ld	%r60, (%r63)		# 2429
	jmpu	%r60		# 2430
else.15958:
	ld	%r2, 4(%r63)		# 2431
	addi	%r3, %r2, 0		# 2432
	ld	%r3, (%r3)		# 2433
	addi	%r4, %r2, 1		# 2434
	ld	%r4, (%r4)		# 2435
	addi	%r2, %r2, 2		# 2436
	ld	%r2, (%r2)		# 2437
	ld	%r5, 2(%r63)		# 2438
	ld	%r6, 1(%r63)		# 2439
	ld	%r7, (%r63)		# 2440
	ld	%r8, 3(%r63)		# 2441
	sto	%r1, 5(%r63)		# 2442
	mov	%r1, %r8		# 2443
	sto	%r4, 6(%r63)		# 2444
	mov	%r4, %r2		# 2445
	mov	%r2, %r3		# 2446
	ld	%r3, 6(%r63)		# 2447
	addi	%r63, %r63, 6		# 2448
	call	%r60, bilinear.2636		# 2449
	addi	%r63, %r63, -6		# 2450
	ld	%r2, 2(%r63)		# 2451
	ld	%r3, 1(%r63)		# 2452
	ld	%r4, (%r63)		# 2453
	ld	%r5, 3(%r63)		# 2454
	sto	%r1, 6(%r63)		# 2455
	mov	%r1, %r5		# 2456
	addi	%r63, %r63, 7		# 2457
	call	%r60, quadratic.2631		# 2458
	addi	%r63, %r63, -7		# 2459
	ld	%r2, 3(%r63)		# 2460
	ld	%r3, 1(%r2)		# 2461
	cmpeqi	%r3, %r3, 3		# 2462
	jmpzi	%r3, else.15959		# 2463
	movhiz	%r3, 260096		# 2464
	fsub	%r1, %r1, %r3		# 2465
	jmpui	if_cont.15960		# 2466
else.15959:
if_cont.15960:
	ld	%r3, 6(%r63)		# 2467
	fmul	%r4, %r3, %r3		# 2468
	ld	%r5, 5(%r63)		# 2469
	fmul	%r1, %r5, %r1		# 2470
	fsub	%r4, %r4, %r1		# 2471
	movi	%r1, 0		# 2472
	cmpfgt	%r1, %r4, %r1		# 2473
	jmpi	%r1, else.15961		# 2474
	movi	%r1, 0		# 2475
	addi	%r63, %r63, -1		# 2476
	ld	%r60, (%r63)		# 2477
	jmpu	%r60		# 2478
else.15961:
	fsqrt	%r4, %r4		# 2479
	ld	%r1, 6(%r2)		# 2480
	cmpeqi	%r1, %r1, 0		# 2481
	jmpzi	%r1, else.15962		# 2482
	fneg	%r4, %r4		# 2483
	jmpui	if_cont.15963		# 2484
else.15962:
if_cont.15963:
	movi	%r1, min_caml_solver_dist		# 2485
	fsub	%r4, %r4, %r3		# 2486
	finv	%r5, %r5		# 2487
	fmul	%r4, %r4, %r5		# 2488
	addi	%r1, %r1, 0		# 2489
	sto	%r4, (%r1)		# 2490
	movi	%r1, 1		# 2491
	addi	%r63, %r63, -1		# 2492
	ld	%r60, (%r63)		# 2493
	jmpu	%r60		# 2494
.globl	solver_rect_fast.2654
solver_rect_fast.2654:
	addi	%r7, %r3, 0		# 2495
	ld	%r7, (%r7)		# 2496
	fsub	%r7, %r7, %r4		# 2497
	addi	%r8, %r3, 1		# 2498
	ld	%r8, (%r8)		# 2499
	fmul	%r7, %r7, %r8		# 2500
	addi	%r8, %r2, 1		# 2501
	ld	%r8, (%r8)		# 2502
	fmul	%r8, %r7, %r8		# 2503
	fadd	%r8, %r8, %r5		# 2504
	fabs	%r8, %r8		# 2505
	ld	%r9, 4(%r1)		# 2506
	addi	%r9, %r9, 1		# 2507
	ld	%r9, (%r9)		# 2508
	cmpfgt	%r9, %r9, %r8		# 2509
	jmpi	%r9, else.15964		# 2510
	movi	%r8, 0		# 2511
	jmpui	if_cont.15965		# 2512
else.15964:
	addi	%r8, %r2, 2		# 2513
	ld	%r8, (%r8)		# 2514
	fmul	%r8, %r7, %r8		# 2515
	fadd	%r8, %r8, %r6		# 2516
	fabs	%r8, %r8		# 2517
	ld	%r9, 4(%r1)		# 2518
	addi	%r9, %r9, 2		# 2519
	ld	%r9, (%r9)		# 2520
	cmpfgt	%r9, %r9, %r8		# 2521
	jmpi	%r9, else.15966		# 2522
	movi	%r8, 0		# 2523
	jmpui	if_cont.15967		# 2524
else.15966:
	addi	%r8, %r3, 1		# 2525
	ld	%r8, (%r8)		# 2526
	movi	%r9, 0		# 2527
	cmpfeq	%r8, %r8, %r9		# 2528
	jmpzi	%r8, else.15968		# 2529
	movi	%r8, 0		# 2530
	jmpui	if_cont.15969		# 2531
else.15968:
	movi	%r8, 1		# 2532
if_cont.15969:
if_cont.15967:
if_cont.15965:
	cmpeqi	%r8, %r8, 0		# 2533
	jmpzi	%r8, else.15970		# 2534
	addi	%r7, %r3, 2		# 2535
	ld	%r7, (%r7)		# 2536
	fsub	%r7, %r7, %r5		# 2537
	addi	%r8, %r3, 3		# 2538
	ld	%r8, (%r8)		# 2539
	fmul	%r7, %r7, %r8		# 2540
	addi	%r8, %r2, 0		# 2541
	ld	%r8, (%r8)		# 2542
	fmul	%r8, %r7, %r8		# 2543
	fadd	%r8, %r8, %r4		# 2544
	fabs	%r8, %r8		# 2545
	ld	%r9, 4(%r1)		# 2546
	addi	%r9, %r9, 0		# 2547
	ld	%r9, (%r9)		# 2548
	cmpfgt	%r9, %r9, %r8		# 2549
	jmpi	%r9, else.15971		# 2550
	movi	%r8, 0		# 2551
	jmpui	if_cont.15972		# 2552
else.15971:
	addi	%r8, %r2, 2		# 2553
	ld	%r8, (%r8)		# 2554
	fmul	%r8, %r7, %r8		# 2555
	fadd	%r8, %r8, %r6		# 2556
	fabs	%r8, %r8		# 2557
	ld	%r9, 4(%r1)		# 2558
	addi	%r9, %r9, 2		# 2559
	ld	%r9, (%r9)		# 2560
	cmpfgt	%r9, %r9, %r8		# 2561
	jmpi	%r9, else.15973		# 2562
	movi	%r8, 0		# 2563
	jmpui	if_cont.15974		# 2564
else.15973:
	addi	%r8, %r3, 3		# 2565
	ld	%r8, (%r8)		# 2566
	movi	%r9, 0		# 2567
	cmpfeq	%r8, %r8, %r9		# 2568
	jmpzi	%r8, else.15975		# 2569
	movi	%r8, 0		# 2570
	jmpui	if_cont.15976		# 2571
else.15975:
	movi	%r8, 1		# 2572
if_cont.15976:
if_cont.15974:
if_cont.15972:
	cmpeqi	%r8, %r8, 0		# 2573
	jmpzi	%r8, else.15977		# 2574
	addi	%r7, %r3, 4		# 2575
	ld	%r7, (%r7)		# 2576
	fsub	%r7, %r7, %r6		# 2577
	addi	%r6, %r3, 5		# 2578
	ld	%r6, (%r6)		# 2579
	fmul	%r7, %r7, %r6		# 2580
	addi	%r6, %r2, 0		# 2581
	ld	%r6, (%r6)		# 2582
	fmul	%r6, %r7, %r6		# 2583
	fadd	%r6, %r6, %r4		# 2584
	fabs	%r6, %r6		# 2585
	ld	%r4, 4(%r1)		# 2586
	addi	%r4, %r4, 0		# 2587
	ld	%r4, (%r4)		# 2588
	cmpfgt	%r4, %r4, %r6		# 2589
	jmpi	%r4, else.15978		# 2590
	movi	%r1, 0		# 2591
	jmpui	if_cont.15979		# 2592
else.15978:
	addi	%r2, %r2, 1		# 2593
	ld	%r2, (%r2)		# 2594
	fmul	%r2, %r7, %r2		# 2595
	fadd	%r2, %r2, %r5		# 2596
	fabs	%r2, %r2		# 2597
	ld	%r1, 4(%r1)		# 2598
	addi	%r1, %r1, 1		# 2599
	ld	%r1, (%r1)		# 2600
	cmpfgt	%r1, %r1, %r2		# 2601
	jmpi	%r1, else.15980		# 2602
	movi	%r1, 0		# 2603
	jmpui	if_cont.15981		# 2604
else.15980:
	addi	%r3, %r3, 5		# 2605
	ld	%r1, (%r3)		# 2606
	movi	%r2, 0		# 2607
	cmpfeq	%r1, %r1, %r2		# 2608
	jmpzi	%r1, else.15982		# 2609
	movi	%r1, 0		# 2610
	jmpui	if_cont.15983		# 2611
else.15982:
	movi	%r1, 1		# 2612
if_cont.15983:
if_cont.15981:
if_cont.15979:
	cmpeqi	%r1, %r1, 0		# 2613
	jmpzi	%r1, else.15984		# 2614
	movi	%r1, 0		# 2615
	jmpu	%r60		# 2616
else.15984:
	movi	%r1, min_caml_solver_dist		# 2617
	addi	%r1, %r1, 0		# 2618
	sto	%r7, (%r1)		# 2619
	movi	%r1, 3		# 2620
	jmpu	%r60		# 2621
else.15977:
	movi	%r1, min_caml_solver_dist		# 2622
	addi	%r1, %r1, 0		# 2623
	sto	%r7, (%r1)		# 2624
	movi	%r1, 2		# 2625
	jmpu	%r60		# 2626
else.15970:
	movi	%r1, min_caml_solver_dist		# 2627
	addi	%r1, %r1, 0		# 2628
	sto	%r7, (%r1)		# 2629
	movi	%r1, 1		# 2630
	jmpu	%r60		# 2631
.globl	solver_second_fast.2667
solver_second_fast.2667:
	sto	%r60, (%r63)		# 2632
	addi	%r63, %r63, 1		# 2633
	addi	%r6, %r2, 0		# 2634
	ld	%r6, (%r6)		# 2635
	movi	%r7, 0		# 2636
	cmpfeq	%r7, %r6, %r7		# 2637
	jmpzi	%r7, else.15985		# 2638
	movi	%r1, 0		# 2639
	addi	%r63, %r63, -1		# 2640
	ld	%r60, (%r63)		# 2641
	jmpu	%r60		# 2642
else.15985:
	addi	%r7, %r2, 1		# 2643
	ld	%r7, (%r7)		# 2644
	fmul	%r7, %r7, %r3		# 2645
	addi	%r8, %r2, 2		# 2646
	ld	%r8, (%r8)		# 2647
	fmul	%r8, %r8, %r4		# 2648
	fadd	%r7, %r7, %r8		# 2649
	addi	%r8, %r2, 3		# 2650
	ld	%r8, (%r8)		# 2651
	fmul	%r8, %r8, %r5		# 2652
	fadd	%r7, %r7, %r8		# 2653
	sto	%r2, (%r63)		# 2654
	sto	%r6, 1(%r63)		# 2655
	sto	%r7, 2(%r63)		# 2656
	sto	%r1, 3(%r63)		# 2657
	mov	%r2, %r3		# 2658
	mov	%r3, %r4		# 2659
	mov	%r4, %r5		# 2660
	addi	%r63, %r63, 4		# 2661
	call	%r60, quadratic.2631		# 2662
	addi	%r63, %r63, -4		# 2663
	ld	%r2, 3(%r63)		# 2664
	ld	%r3, 1(%r2)		# 2665
	cmpeqi	%r3, %r3, 3		# 2666
	jmpzi	%r3, else.15986		# 2667
	movhiz	%r3, 260096		# 2668
	fsub	%r1, %r1, %r3		# 2669
	jmpui	if_cont.15987		# 2670
else.15986:
if_cont.15987:
	ld	%r3, 2(%r63)		# 2671
	fmul	%r4, %r3, %r3		# 2672
	ld	%r5, 1(%r63)		# 2673
	fmul	%r5, %r5, %r1		# 2674
	fsub	%r4, %r4, %r5		# 2675
	movi	%r1, 0		# 2676
	cmpfgt	%r1, %r4, %r1		# 2677
	jmpi	%r1, else.15988		# 2678
	movi	%r1, 0		# 2679
	addi	%r63, %r63, -1		# 2680
	ld	%r60, (%r63)		# 2681
	jmpu	%r60		# 2682
else.15988:
	ld	%r1, 6(%r2)		# 2683
	cmpeqi	%r1, %r1, 0		# 2684
	jmpzi	%r1, else.15989		# 2685
	movi	%r1, min_caml_solver_dist		# 2686
	fsqrt	%r4, %r4		# 2687
	fsub	%r3, %r3, %r4		# 2688
	ld	%r2, (%r63)		# 2689
	addi	%r2, %r2, 4		# 2690
	ld	%r2, (%r2)		# 2691
	fmul	%r3, %r3, %r2		# 2692
	addi	%r1, %r1, 0		# 2693
	sto	%r3, (%r1)		# 2694
	jmpui	if_cont.15990		# 2695
else.15989:
	movi	%r1, min_caml_solver_dist		# 2696
	fsqrt	%r4, %r4		# 2697
	fadd	%r3, %r3, %r4		# 2698
	ld	%r2, (%r63)		# 2699
	addi	%r2, %r2, 4		# 2700
	ld	%r2, (%r2)		# 2701
	fmul	%r3, %r3, %r2		# 2702
	addi	%r1, %r1, 0		# 2703
	sto	%r3, (%r1)		# 2704
if_cont.15990:
	movi	%r1, 1		# 2705
	addi	%r63, %r63, -1		# 2706
	ld	%r60, (%r63)		# 2707
	jmpu	%r60		# 2708
.globl	solver_second_fast2.2684
solver_second_fast2.2684:
	addi	%r7, %r2, 0		# 2709
	ld	%r7, (%r7)		# 2710
	movi	%r8, 0		# 2711
	cmpfeq	%r8, %r7, %r8		# 2712
	jmpzi	%r8, else.15991		# 2713
	movi	%r1, 0		# 2714
	jmpu	%r60		# 2715
else.15991:
	addi	%r8, %r2, 1		# 2716
	ld	%r8, (%r8)		# 2717
	fmul	%r8, %r8, %r4		# 2718
	addi	%r4, %r2, 2		# 2719
	ld	%r4, (%r4)		# 2720
	fmul	%r4, %r4, %r5		# 2721
	fadd	%r8, %r8, %r4		# 2722
	addi	%r4, %r2, 3		# 2723
	ld	%r4, (%r4)		# 2724
	fmul	%r4, %r4, %r6		# 2725
	fadd	%r8, %r8, %r4		# 2726
	addi	%r3, %r3, 3		# 2727
	ld	%r3, (%r3)		# 2728
	fmul	%r4, %r8, %r8		# 2729
	fmul	%r7, %r7, %r3		# 2730
	fsub	%r4, %r4, %r7		# 2731
	movi	%r3, 0		# 2732
	cmpfgt	%r3, %r4, %r3		# 2733
	jmpi	%r3, else.15992		# 2734
	movi	%r1, 0		# 2735
	jmpu	%r60		# 2736
else.15992:
	ld	%r1, 6(%r1)		# 2737
	cmpeqi	%r1, %r1, 0		# 2738
	jmpzi	%r1, else.15993		# 2739
	movi	%r1, min_caml_solver_dist		# 2740
	fsqrt	%r4, %r4		# 2741
	fsub	%r8, %r8, %r4		# 2742
	addi	%r2, %r2, 4		# 2743
	ld	%r2, (%r2)		# 2744
	fmul	%r8, %r8, %r2		# 2745
	addi	%r1, %r1, 0		# 2746
	sto	%r8, (%r1)		# 2747
	jmpui	if_cont.15994		# 2748
else.15993:
	movi	%r1, min_caml_solver_dist		# 2749
	fsqrt	%r4, %r4		# 2750
	fadd	%r8, %r8, %r4		# 2751
	addi	%r2, %r2, 4		# 2752
	ld	%r2, (%r2)		# 2753
	fmul	%r8, %r8, %r2		# 2754
	addi	%r1, %r1, 0		# 2755
	sto	%r8, (%r1)		# 2756
if_cont.15994:
	movi	%r1, 1		# 2757
	jmpu	%r60		# 2758
.globl	setup_rect_table.2694
setup_rect_table.2694:
	sto	%r60, (%r63)		# 2759
	addi	%r63, %r63, 1		# 2760
	movi	%r3, 0		# 2761
	movi	%r4, 6		# 2762
	sto	%r2, (%r63)		# 2763
	sto	%r1, 1(%r63)		# 2764
	mov	%r2, %r3		# 2765
	mov	%r1, %r4		# 2766
	addi	%r63, %r63, 2		# 2767
	call	%r60, min_caml_create_float_array		# 2768
	addi	%r63, %r63, -2		# 2769
	ld	%r2, 1(%r63)		# 2770
	addi	%r3, %r2, 0		# 2771
	ld	%r3, (%r3)		# 2772
	movi	%r4, 0		# 2773
	cmpfeq	%r3, %r3, %r4		# 2774
	jmpzi	%r3, else.15995		# 2775
	movi	%r3, 0		# 2776
	addi	%r4, %r1, 1		# 2777
	sto	%r3, (%r4)		# 2778
	jmpui	if_cont.15996		# 2779
else.15995:
	ld	%r3, (%r63)		# 2780
	ld	%r4, 6(%r3)		# 2781
	addi	%r5, %r2, 0		# 2782
	ld	%r5, (%r5)		# 2783
	movi	%r6, 0		# 2784
	cmpfgt	%r7, %r6, %r5		# 2785
	jmpi	%r7, else.15997		# 2786
	movi	%r7, 0		# 2787
	jmpui	if_cont.15998		# 2788
else.15997:
	movi	%r7, 1		# 2789
if_cont.15998:
	cmpeqi	%r4, %r4, 0		# 2790
	jmpzi	%r4, else.15999		# 2791
	jmpui	if_cont.16000		# 2792
else.15999:
	cmpfgt	%r6, %r6, %r5		# 2793
	jmpi	%r6, else.16001		# 2794
	movi	%r7, 1		# 2795
	jmpui	if_cont.16002		# 2796
else.16001:
	movi	%r7, 0		# 2797
if_cont.16002:
if_cont.16000:
	ld	%r4, 4(%r3)		# 2798
	addi	%r4, %r4, 0		# 2799
	ld	%r4, (%r4)		# 2800
	cmpeqi	%r7, %r7, 0		# 2801
	jmpzi	%r7, else.16003		# 2802
	fneg	%r4, %r4		# 2803
	jmpui	if_cont.16004		# 2804
else.16003:
if_cont.16004:
	addi	%r5, %r1, 0		# 2805
	sto	%r4, (%r5)		# 2806
	movhiz	%r4, 260096		# 2807
	addi	%r5, %r2, 0		# 2808
	ld	%r5, (%r5)		# 2809
	finv	%r5, %r5		# 2810
	fmul	%r4, %r4, %r5		# 2811
	addi	%r5, %r1, 1		# 2812
	sto	%r4, (%r5)		# 2813
if_cont.15996:
	addi	%r3, %r2, 1		# 2814
	ld	%r3, (%r3)		# 2815
	movi	%r4, 0		# 2816
	cmpfeq	%r3, %r3, %r4		# 2817
	jmpzi	%r3, else.16005		# 2818
	movi	%r3, 0		# 2819
	addi	%r4, %r1, 3		# 2820
	sto	%r3, (%r4)		# 2821
	jmpui	if_cont.16006		# 2822
else.16005:
	ld	%r3, (%r63)		# 2823
	ld	%r4, 6(%r3)		# 2824
	addi	%r5, %r2, 1		# 2825
	ld	%r5, (%r5)		# 2826
	movi	%r6, 0		# 2827
	cmpfgt	%r7, %r6, %r5		# 2828
	jmpi	%r7, else.16007		# 2829
	movi	%r7, 0		# 2830
	jmpui	if_cont.16008		# 2831
else.16007:
	movi	%r7, 1		# 2832
if_cont.16008:
	cmpeqi	%r4, %r4, 0		# 2833
	jmpzi	%r4, else.16009		# 2834
	jmpui	if_cont.16010		# 2835
else.16009:
	cmpfgt	%r6, %r6, %r5		# 2836
	jmpi	%r6, else.16011		# 2837
	movi	%r7, 1		# 2838
	jmpui	if_cont.16012		# 2839
else.16011:
	movi	%r7, 0		# 2840
if_cont.16012:
if_cont.16010:
	ld	%r4, 4(%r3)		# 2841
	addi	%r4, %r4, 1		# 2842
	ld	%r4, (%r4)		# 2843
	cmpeqi	%r7, %r7, 0		# 2844
	jmpzi	%r7, else.16013		# 2845
	fneg	%r4, %r4		# 2846
	jmpui	if_cont.16014		# 2847
else.16013:
if_cont.16014:
	addi	%r5, %r1, 2		# 2848
	sto	%r4, (%r5)		# 2849
	movhiz	%r4, 260096		# 2850
	addi	%r5, %r2, 1		# 2851
	ld	%r5, (%r5)		# 2852
	finv	%r5, %r5		# 2853
	fmul	%r4, %r4, %r5		# 2854
	addi	%r5, %r1, 3		# 2855
	sto	%r4, (%r5)		# 2856
if_cont.16006:
	addi	%r3, %r2, 2		# 2857
	ld	%r3, (%r3)		# 2858
	movi	%r4, 0		# 2859
	cmpfeq	%r3, %r3, %r4		# 2860
	jmpzi	%r3, else.16015		# 2861
	movi	%r2, 0		# 2862
	addi	%r3, %r1, 5		# 2863
	sto	%r2, (%r3)		# 2864
	jmpui	if_cont.16016		# 2865
else.16015:
	ld	%r3, (%r63)		# 2866
	ld	%r4, 6(%r3)		# 2867
	addi	%r5, %r2, 2		# 2868
	ld	%r5, (%r5)		# 2869
	movi	%r6, 0		# 2870
	cmpfgt	%r7, %r6, %r5		# 2871
	jmpi	%r7, else.16017		# 2872
	movi	%r7, 0		# 2873
	jmpui	if_cont.16018		# 2874
else.16017:
	movi	%r7, 1		# 2875
if_cont.16018:
	cmpeqi	%r4, %r4, 0		# 2876
	jmpzi	%r4, else.16019		# 2877
	jmpui	if_cont.16020		# 2878
else.16019:
	cmpfgt	%r6, %r6, %r5		# 2879
	jmpi	%r6, else.16021		# 2880
	movi	%r7, 1		# 2881
	jmpui	if_cont.16022		# 2882
else.16021:
	movi	%r7, 0		# 2883
if_cont.16022:
if_cont.16020:
	ld	%r3, 4(%r3)		# 2884
	addi	%r3, %r3, 2		# 2885
	ld	%r3, (%r3)		# 2886
	cmpeqi	%r7, %r7, 0		# 2887
	jmpzi	%r7, else.16023		# 2888
	fneg	%r3, %r3		# 2889
	jmpui	if_cont.16024		# 2890
else.16023:
if_cont.16024:
	addi	%r4, %r1, 4		# 2891
	sto	%r3, (%r4)		# 2892
	movhiz	%r3, 260096		# 2893
	addi	%r2, %r2, 2		# 2894
	ld	%r2, (%r2)		# 2895
	finv	%r2, %r2		# 2896
	fmul	%r3, %r3, %r2		# 2897
	addi	%r2, %r1, 5		# 2898
	sto	%r3, (%r2)		# 2899
if_cont.16016:
	addi	%r63, %r63, -1		# 2900
	ld	%r60, (%r63)		# 2901
	jmpu	%r60		# 2902
.globl	setup_surface_table.2697
setup_surface_table.2697:
	sto	%r60, (%r63)		# 2903
	addi	%r63, %r63, 1		# 2904
	movi	%r3, 0		# 2905
	movi	%r4, 4		# 2906
	sto	%r2, (%r63)		# 2907
	sto	%r1, 1(%r63)		# 2908
	mov	%r2, %r3		# 2909
	mov	%r1, %r4		# 2910
	addi	%r63, %r63, 2		# 2911
	call	%r60, min_caml_create_float_array		# 2912
	addi	%r63, %r63, -2		# 2913
	ld	%r2, 1(%r63)		# 2914
	addi	%r3, %r2, 0		# 2915
	ld	%r3, (%r3)		# 2916
	ld	%r4, (%r63)		# 2917
	ld	%r5, 4(%r4)		# 2918
	addi	%r5, %r5, 0		# 2919
	ld	%r5, (%r5)		# 2920
	fmul	%r3, %r3, %r5		# 2921
	addi	%r5, %r2, 1		# 2922
	ld	%r5, (%r5)		# 2923
	ld	%r6, 4(%r4)		# 2924
	addi	%r6, %r6, 1		# 2925
	ld	%r6, (%r6)		# 2926
	fmul	%r5, %r5, %r6		# 2927
	fadd	%r3, %r3, %r5		# 2928
	addi	%r2, %r2, 2		# 2929
	ld	%r2, (%r2)		# 2930
	ld	%r5, 4(%r4)		# 2931
	addi	%r5, %r5, 2		# 2932
	ld	%r5, (%r5)		# 2933
	fmul	%r2, %r2, %r5		# 2934
	fadd	%r3, %r3, %r2		# 2935
	movi	%r2, 0		# 2936
	cmpfgt	%r2, %r3, %r2		# 2937
	jmpi	%r2, else.16025		# 2938
	movi	%r2, 0		# 2939
	addi	%r3, %r1, 0		# 2940
	sto	%r2, (%r3)		# 2941
	jmpui	if_cont.16026		# 2942
else.16025:
	movhiz	%r2, 784384		# 2943
	finv	%r5, %r3		# 2944
	fmul	%r2, %r2, %r5		# 2945
	addi	%r5, %r1, 0		# 2946
	sto	%r2, (%r5)		# 2947
	ld	%r2, 4(%r4)		# 2948
	addi	%r2, %r2, 0		# 2949
	ld	%r2, (%r2)		# 2950
	finv	%r5, %r3		# 2951
	fmul	%r2, %r2, %r5		# 2952
	fneg	%r2, %r2		# 2953
	addi	%r5, %r1, 1		# 2954
	sto	%r2, (%r5)		# 2955
	ld	%r2, 4(%r4)		# 2956
	addi	%r2, %r2, 1		# 2957
	ld	%r2, (%r2)		# 2958
	finv	%r5, %r3		# 2959
	fmul	%r2, %r2, %r5		# 2960
	fneg	%r2, %r2		# 2961
	addi	%r5, %r1, 2		# 2962
	sto	%r2, (%r5)		# 2963
	ld	%r2, 4(%r4)		# 2964
	addi	%r2, %r2, 2		# 2965
	ld	%r2, (%r2)		# 2966
	finv	%r3, %r3		# 2967
	fmul	%r2, %r2, %r3		# 2968
	fneg	%r2, %r2		# 2969
	addi	%r3, %r1, 3		# 2970
	sto	%r2, (%r3)		# 2971
if_cont.16026:
	addi	%r63, %r63, -1		# 2972
	ld	%r60, (%r63)		# 2973
	jmpu	%r60		# 2974
.globl	setup_second_table.2700
setup_second_table.2700:
	sto	%r60, (%r63)		# 2975
	addi	%r63, %r63, 1		# 2976
	movi	%r3, 0		# 2977
	movi	%r4, 5		# 2978
	sto	%r2, (%r63)		# 2979
	sto	%r1, 1(%r63)		# 2980
	mov	%r2, %r3		# 2981
	mov	%r1, %r4		# 2982
	addi	%r63, %r63, 2		# 2983
	call	%r60, min_caml_create_float_array		# 2984
	addi	%r63, %r63, -2		# 2985
	ld	%r2, 1(%r63)		# 2986
	addi	%r3, %r2, 0		# 2987
	ld	%r3, (%r3)		# 2988
	addi	%r4, %r2, 1		# 2989
	ld	%r4, (%r4)		# 2990
	addi	%r5, %r2, 2		# 2991
	ld	%r5, (%r5)		# 2992
	ld	%r6, (%r63)		# 2993
	sto	%r1, 2(%r63)		# 2994
	mov	%r2, %r3		# 2995
	mov	%r1, %r6		# 2996
	mov	%r3, %r4		# 2997
	mov	%r4, %r5		# 2998
	addi	%r63, %r63, 3		# 2999
	call	%r60, quadratic.2631		# 3000
	addi	%r63, %r63, -3		# 3001
	ld	%r2, 1(%r63)		# 3002
	addi	%r3, %r2, 0		# 3003
	ld	%r3, (%r3)		# 3004
	ld	%r4, (%r63)		# 3005
	ld	%r5, 4(%r4)		# 3006
	addi	%r5, %r5, 0		# 3007
	ld	%r5, (%r5)		# 3008
	fmul	%r3, %r3, %r5		# 3009
	fneg	%r3, %r3		# 3010
	addi	%r5, %r2, 1		# 3011
	ld	%r5, (%r5)		# 3012
	ld	%r6, 4(%r4)		# 3013
	addi	%r6, %r6, 1		# 3014
	ld	%r6, (%r6)		# 3015
	fmul	%r5, %r5, %r6		# 3016
	fneg	%r5, %r5		# 3017
	addi	%r6, %r2, 2		# 3018
	ld	%r6, (%r6)		# 3019
	ld	%r7, 4(%r4)		# 3020
	addi	%r7, %r7, 2		# 3021
	ld	%r7, (%r7)		# 3022
	fmul	%r6, %r6, %r7		# 3023
	fneg	%r6, %r6		# 3024
	ld	%r7, 2(%r63)		# 3025
	addi	%r8, %r7, 0		# 3026
	sto	%r1, (%r8)		# 3027
	ld	%r8, 3(%r4)		# 3028
	cmpeqi	%r8, %r8, 0		# 3029
	jmpzi	%r8, else.16027		# 3030
	addi	%r2, %r7, 1		# 3031
	sto	%r3, (%r2)		# 3032
	addi	%r2, %r7, 2		# 3033
	sto	%r5, (%r2)		# 3034
	addi	%r2, %r7, 3		# 3035
	sto	%r6, (%r2)		# 3036
	jmpui	if_cont.16028		# 3037
else.16027:
	addi	%r8, %r2, 2		# 3038
	ld	%r8, (%r8)		# 3039
	ld	%r9, 9(%r4)		# 3040
	addi	%r9, %r9, 1		# 3041
	ld	%r9, (%r9)		# 3042
	fmul	%r8, %r8, %r9		# 3043
	addi	%r9, %r2, 1		# 3044
	ld	%r9, (%r9)		# 3045
	ld	%r10, 9(%r4)		# 3046
	addi	%r10, %r10, 2		# 3047
	ld	%r10, (%r10)		# 3048
	fmul	%r9, %r9, %r10		# 3049
	fadd	%r8, %r8, %r9		# 3050
	movhiz	%r9, 258048		# 3051
	fmul	%r8, %r8, %r9		# 3052
	fsub	%r3, %r3, %r8		# 3053
	addi	%r8, %r7, 1		# 3054
	sto	%r3, (%r8)		# 3055
	addi	%r3, %r2, 2		# 3056
	ld	%r3, (%r3)		# 3057
	ld	%r8, 9(%r4)		# 3058
	addi	%r8, %r8, 0		# 3059
	ld	%r8, (%r8)		# 3060
	fmul	%r3, %r3, %r8		# 3061
	addi	%r8, %r2, 0		# 3062
	ld	%r8, (%r8)		# 3063
	ld	%r9, 9(%r4)		# 3064
	addi	%r9, %r9, 2		# 3065
	ld	%r9, (%r9)		# 3066
	fmul	%r8, %r8, %r9		# 3067
	fadd	%r3, %r3, %r8		# 3068
	movhiz	%r8, 258048		# 3069
	fmul	%r3, %r3, %r8		# 3070
	fsub	%r5, %r5, %r3		# 3071
	addi	%r3, %r7, 2		# 3072
	sto	%r5, (%r3)		# 3073
	addi	%r3, %r2, 1		# 3074
	ld	%r3, (%r3)		# 3075
	ld	%r5, 9(%r4)		# 3076
	addi	%r5, %r5, 0		# 3077
	ld	%r5, (%r5)		# 3078
	fmul	%r3, %r3, %r5		# 3079
	addi	%r2, %r2, 0		# 3080
	ld	%r2, (%r2)		# 3081
	ld	%r4, 9(%r4)		# 3082
	addi	%r4, %r4, 1		# 3083
	ld	%r4, (%r4)		# 3084
	fmul	%r2, %r2, %r4		# 3085
	fadd	%r3, %r3, %r2		# 3086
	movhiz	%r2, 258048		# 3087
	fmul	%r3, %r3, %r2		# 3088
	fsub	%r6, %r6, %r3		# 3089
	addi	%r2, %r7, 3		# 3090
	sto	%r6, (%r2)		# 3091
if_cont.16028:
	movi	%r2, 0		# 3092
	cmpfeq	%r2, %r1, %r2		# 3093
	jmpzi	%r2, else.16029		# 3094
	jmpui	if_cont.16030		# 3095
else.16029:
	movhiz	%r2, 260096		# 3096
	finv	%r1, %r1		# 3097
	fmul	%r2, %r2, %r1		# 3098
	addi	%r1, %r7, 4		# 3099
	sto	%r2, (%r1)		# 3100
if_cont.16030:
	mov	%r1, %r7		# 3101
	addi	%r63, %r63, -1		# 3102
	ld	%r60, (%r63)		# 3103
	jmpu	%r60		# 3104
.globl	iter_setup_dirvec_constants.2703
iter_setup_dirvec_constants.2703:
	sto	%r60, (%r63)		# 3105
	addi	%r63, %r63, 1		# 3106
	movi	%r3, 0		# 3107
	cmpgt	%r3, %r3, %r2		# 3108
	jmpi	%r3, else.16031		# 3109
	movi	%r3, min_caml_objects		# 3110
	add	%r3, %r3, %r2		# 3111
	ld	%r3, (%r3)		# 3112
	ld	%r4, 1(%r1)		# 3113
	ld	%r5, (%r1)		# 3114
	ld	%r6, 1(%r3)		# 3115
	cmpeqi	%r7, %r6, 1		# 3116
	sto	%r1, (%r63)		# 3117
	jmpzi	%r7, else.16032		# 3118
	sto	%r2, 1(%r63)		# 3119
	sto	%r4, 2(%r63)		# 3120
	mov	%r2, %r3		# 3121
	mov	%r1, %r5		# 3122
	addi	%r63, %r63, 3		# 3123
	call	%r60, setup_rect_table.2694		# 3124
	addi	%r63, %r63, -3		# 3125
	ld	%r2, 1(%r63)		# 3126
	ld	%r3, 2(%r63)		# 3127
	add	%r3, %r3, %r2		# 3128
	sto	%r1, (%r3)		# 3129
	jmpui	if_cont.16033		# 3130
else.16032:
	cmpeqi	%r6, %r6, 2		# 3131
	jmpzi	%r6, else.16034		# 3132
	sto	%r2, 1(%r63)		# 3133
	sto	%r4, 2(%r63)		# 3134
	mov	%r2, %r3		# 3135
	mov	%r1, %r5		# 3136
	addi	%r63, %r63, 3		# 3137
	call	%r60, setup_surface_table.2697		# 3138
	addi	%r63, %r63, -3		# 3139
	ld	%r2, 1(%r63)		# 3140
	ld	%r3, 2(%r63)		# 3141
	add	%r3, %r3, %r2		# 3142
	sto	%r1, (%r3)		# 3143
	jmpui	if_cont.16035		# 3144
else.16034:
	sto	%r2, 1(%r63)		# 3145
	sto	%r4, 2(%r63)		# 3146
	mov	%r2, %r3		# 3147
	mov	%r1, %r5		# 3148
	addi	%r63, %r63, 3		# 3149
	call	%r60, setup_second_table.2700		# 3150
	addi	%r63, %r63, -3		# 3151
	ld	%r2, 1(%r63)		# 3152
	ld	%r3, 2(%r63)		# 3153
	add	%r3, %r3, %r2		# 3154
	sto	%r1, (%r3)		# 3155
if_cont.16035:
if_cont.16033:
	addi	%r2, %r2, -1		# 3156
	movi	%r1, 0		# 3157
	cmpgt	%r1, %r1, %r2		# 3158
	jmpi	%r1, else.16036		# 3159
	movi	%r1, min_caml_objects		# 3160
	add	%r1, %r1, %r2		# 3161
	ld	%r1, (%r1)		# 3162
	ld	%r3, (%r63)		# 3163
	ld	%r4, 1(%r3)		# 3164
	ld	%r5, (%r3)		# 3165
	ld	%r6, 1(%r1)		# 3166
	cmpeqi	%r7, %r6, 1		# 3167
	jmpzi	%r7, else.16037		# 3168
	sto	%r2, 3(%r63)		# 3169
	sto	%r4, 4(%r63)		# 3170
	mov	%r2, %r1		# 3171
	mov	%r1, %r5		# 3172
	addi	%r63, %r63, 5		# 3173
	call	%r60, setup_rect_table.2694		# 3174
	addi	%r63, %r63, -5		# 3175
	ld	%r2, 3(%r63)		# 3176
	ld	%r3, 4(%r63)		# 3177
	add	%r3, %r3, %r2		# 3178
	sto	%r1, (%r3)		# 3179
	jmpui	if_cont.16038		# 3180
else.16037:
	cmpeqi	%r6, %r6, 2		# 3181
	jmpzi	%r6, else.16039		# 3182
	sto	%r2, 3(%r63)		# 3183
	sto	%r4, 4(%r63)		# 3184
	mov	%r2, %r1		# 3185
	mov	%r1, %r5		# 3186
	addi	%r63, %r63, 5		# 3187
	call	%r60, setup_surface_table.2697		# 3188
	addi	%r63, %r63, -5		# 3189
	ld	%r2, 3(%r63)		# 3190
	ld	%r3, 4(%r63)		# 3191
	add	%r3, %r3, %r2		# 3192
	sto	%r1, (%r3)		# 3193
	jmpui	if_cont.16040		# 3194
else.16039:
	sto	%r2, 3(%r63)		# 3195
	sto	%r4, 4(%r63)		# 3196
	mov	%r2, %r1		# 3197
	mov	%r1, %r5		# 3198
	addi	%r63, %r63, 5		# 3199
	call	%r60, setup_second_table.2700		# 3200
	addi	%r63, %r63, -5		# 3201
	ld	%r2, 3(%r63)		# 3202
	ld	%r3, 4(%r63)		# 3203
	add	%r3, %r3, %r2		# 3204
	sto	%r1, (%r3)		# 3205
if_cont.16040:
if_cont.16038:
	addi	%r2, %r2, -1		# 3206
	ld	%r1, (%r63)		# 3207
	addi	%r63, %r63, -1		# 3208
	ld	%r60, (%r63)		# 3209
	jmpui	iter_setup_dirvec_constants.2703		# 3210
else.16036:
	addi	%r63, %r63, -1		# 3211
	ld	%r60, (%r63)		# 3212
	jmpu	%r60		# 3213
else.16031:
	addi	%r63, %r63, -1		# 3214
	ld	%r60, (%r63)		# 3215
	jmpu	%r60		# 3216
.globl	setup_startp_constants.2708
setup_startp_constants.2708:
	sto	%r60, (%r63)		# 3217
	addi	%r63, %r63, 1		# 3218
	movi	%r3, 0		# 3219
	cmpgt	%r3, %r3, %r2		# 3220
	jmpi	%r3, else.16043		# 3221
	movi	%r3, min_caml_objects		# 3222
	add	%r3, %r3, %r2		# 3223
	ld	%r3, (%r3)		# 3224
	ld	%r4, 10(%r3)		# 3225
	ld	%r5, 1(%r3)		# 3226
	addi	%r6, %r1, 0		# 3227
	ld	%r6, (%r6)		# 3228
	ld	%r7, 5(%r3)		# 3229
	addi	%r7, %r7, 0		# 3230
	ld	%r7, (%r7)		# 3231
	fsub	%r6, %r6, %r7		# 3232
	addi	%r7, %r4, 0		# 3233
	sto	%r6, (%r7)		# 3234
	addi	%r6, %r1, 1		# 3235
	ld	%r6, (%r6)		# 3236
	ld	%r7, 5(%r3)		# 3237
	addi	%r7, %r7, 1		# 3238
	ld	%r7, (%r7)		# 3239
	fsub	%r6, %r6, %r7		# 3240
	addi	%r7, %r4, 1		# 3241
	sto	%r6, (%r7)		# 3242
	addi	%r6, %r1, 2		# 3243
	ld	%r6, (%r6)		# 3244
	ld	%r7, 5(%r3)		# 3245
	addi	%r7, %r7, 2		# 3246
	ld	%r7, (%r7)		# 3247
	fsub	%r6, %r6, %r7		# 3248
	addi	%r7, %r4, 2		# 3249
	sto	%r6, (%r7)		# 3250
	cmpeqi	%r6, %r5, 2		# 3251
	sto	%r1, (%r63)		# 3252
	sto	%r2, 1(%r63)		# 3253
	jmpzi	%r6, else.16044		# 3254
	ld	%r3, 4(%r3)		# 3255
	addi	%r5, %r4, 0		# 3256
	ld	%r5, (%r5)		# 3257
	addi	%r6, %r4, 1		# 3258
	ld	%r6, (%r6)		# 3259
	addi	%r7, %r4, 2		# 3260
	ld	%r7, (%r7)		# 3261
	addi	%r8, %r3, 0		# 3262
	ld	%r8, (%r8)		# 3263
	fmul	%r8, %r8, %r5		# 3264
	addi	%r5, %r3, 1		# 3265
	ld	%r5, (%r5)		# 3266
	fmul	%r5, %r5, %r6		# 3267
	fadd	%r8, %r8, %r5		# 3268
	addi	%r3, %r3, 2		# 3269
	ld	%r3, (%r3)		# 3270
	fmul	%r3, %r3, %r7		# 3271
	fadd	%r8, %r8, %r3		# 3272
	addi	%r4, %r4, 3		# 3273
	sto	%r8, (%r4)		# 3274
	jmpui	if_cont.16045		# 3275
else.16044:
	cmpgti	%r6, %r5, 2		# 3276
	jmpi	%r6, else.16046		# 3277
	jmpui	if_cont.16047		# 3278
else.16046:
	addi	%r6, %r4, 0		# 3279
	ld	%r6, (%r6)		# 3280
	addi	%r7, %r4, 1		# 3281
	ld	%r7, (%r7)		# 3282
	addi	%r8, %r4, 2		# 3283
	ld	%r8, (%r8)		# 3284
	sto	%r4, 2(%r63)		# 3285
	sto	%r5, 3(%r63)		# 3286
	mov	%r4, %r8		# 3287
	mov	%r2, %r6		# 3288
	mov	%r1, %r3		# 3289
	mov	%r3, %r7		# 3290
	addi	%r63, %r63, 4		# 3291
	call	%r60, quadratic.2631		# 3292
	addi	%r63, %r63, -4		# 3293
	ld	%r2, 3(%r63)		# 3294
	cmpeqi	%r2, %r2, 3		# 3295
	jmpzi	%r2, else.16048		# 3296
	movhiz	%r2, 260096		# 3297
	fsub	%r1, %r1, %r2		# 3298
	jmpui	if_cont.16049		# 3299
else.16048:
if_cont.16049:
	ld	%r2, 2(%r63)		# 3300
	addi	%r2, %r2, 3		# 3301
	sto	%r1, (%r2)		# 3302
if_cont.16047:
if_cont.16045:
	ld	%r1, 1(%r63)		# 3303
	addi	%r2, %r1, -1		# 3304
	ld	%r1, (%r63)		# 3305
	addi	%r63, %r63, -1		# 3306
	ld	%r60, (%r63)		# 3307
	jmpui	setup_startp_constants.2708		# 3308
else.16043:
	addi	%r63, %r63, -1		# 3309
	ld	%r60, (%r63)		# 3310
	jmpu	%r60		# 3311
.globl	is_outside.2728
is_outside.2728:
	sto	%r60, (%r63)		# 3312
	addi	%r63, %r63, 1		# 3313
	ld	%r5, 5(%r1)		# 3314
	addi	%r5, %r5, 0		# 3315
	ld	%r5, (%r5)		# 3316
	fsub	%r2, %r2, %r5		# 3317
	ld	%r5, 5(%r1)		# 3318
	addi	%r5, %r5, 1		# 3319
	ld	%r5, (%r5)		# 3320
	fsub	%r3, %r3, %r5		# 3321
	ld	%r5, 5(%r1)		# 3322
	addi	%r5, %r5, 2		# 3323
	ld	%r5, (%r5)		# 3324
	fsub	%r4, %r4, %r5		# 3325
	ld	%r5, 1(%r1)		# 3326
	cmpeqi	%r6, %r5, 1		# 3327
	jmpzi	%r6, else.16051		# 3328
	fabs	%r2, %r2		# 3329
	ld	%r5, 4(%r1)		# 3330
	addi	%r5, %r5, 0		# 3331
	ld	%r5, (%r5)		# 3332
	cmpfgt	%r5, %r5, %r2		# 3333
	jmpi	%r5, else.16052		# 3334
	movi	%r2, 0		# 3335
	jmpui	if_cont.16053		# 3336
else.16052:
	fabs	%r3, %r3		# 3337
	ld	%r2, 4(%r1)		# 3338
	addi	%r2, %r2, 1		# 3339
	ld	%r2, (%r2)		# 3340
	cmpfgt	%r2, %r2, %r3		# 3341
	jmpi	%r2, else.16054		# 3342
	movi	%r2, 0		# 3343
	jmpui	if_cont.16055		# 3344
else.16054:
	fabs	%r4, %r4		# 3345
	ld	%r2, 4(%r1)		# 3346
	addi	%r2, %r2, 2		# 3347
	ld	%r2, (%r2)		# 3348
	cmpfgt	%r2, %r2, %r4		# 3349
	jmpi	%r2, else.16056		# 3350
	movi	%r2, 0		# 3351
	jmpui	if_cont.16057		# 3352
else.16056:
	movi	%r2, 1		# 3353
if_cont.16057:
if_cont.16055:
if_cont.16053:
	cmpeqi	%r2, %r2, 0		# 3354
	jmpzi	%r2, else.16058		# 3355
	ld	%r1, 6(%r1)		# 3356
	cmpeqi	%r1, %r1, 0		# 3357
	jmpzi	%r1, else.16059		# 3358
	movi	%r1, 1		# 3359
	addi	%r63, %r63, -1		# 3360
	ld	%r60, (%r63)		# 3361
	jmpu	%r60		# 3362
else.16059:
	movi	%r1, 0		# 3363
	addi	%r63, %r63, -1		# 3364
	ld	%r60, (%r63)		# 3365
	jmpu	%r60		# 3366
else.16058:
	ld	%r1, 6(%r1)		# 3367
	addi	%r63, %r63, -1		# 3368
	ld	%r60, (%r63)		# 3369
	jmpu	%r60		# 3370
else.16051:
	cmpeqi	%r5, %r5, 2		# 3371
	jmpzi	%r5, else.16060		# 3372
	ld	%r5, 4(%r1)		# 3373
	addi	%r6, %r5, 0		# 3374
	ld	%r6, (%r6)		# 3375
	fmul	%r6, %r6, %r2		# 3376
	addi	%r2, %r5, 1		# 3377
	ld	%r2, (%r2)		# 3378
	fmul	%r2, %r2, %r3		# 3379
	fadd	%r6, %r6, %r2		# 3380
	addi	%r5, %r5, 2		# 3381
	ld	%r2, (%r5)		# 3382
	fmul	%r2, %r2, %r4		# 3383
	fadd	%r6, %r6, %r2		# 3384
	ld	%r1, 6(%r1)		# 3385
	movi	%r2, 0		# 3386
	cmpfgt	%r3, %r2, %r6		# 3387
	jmpi	%r3, else.16061		# 3388
	movi	%r3, 0		# 3389
	jmpui	if_cont.16062		# 3390
else.16061:
	movi	%r3, 1		# 3391
if_cont.16062:
	cmpeqi	%r1, %r1, 0		# 3392
	jmpzi	%r1, else.16063		# 3393
	jmpui	if_cont.16064		# 3394
else.16063:
	cmpfgt	%r2, %r2, %r6		# 3395
	jmpi	%r2, else.16065		# 3396
	movi	%r3, 1		# 3397
	jmpui	if_cont.16066		# 3398
else.16065:
	movi	%r3, 0		# 3399
if_cont.16066:
if_cont.16064:
	cmpeqi	%r3, %r3, 0		# 3400
	jmpzi	%r3, else.16067		# 3401
	movi	%r1, 1		# 3402
	addi	%r63, %r63, -1		# 3403
	ld	%r60, (%r63)		# 3404
	jmpu	%r60		# 3405
else.16067:
	movi	%r1, 0		# 3406
	addi	%r63, %r63, -1		# 3407
	ld	%r60, (%r63)		# 3408
	jmpu	%r60		# 3409
else.16060:
	sto	%r1, (%r63)		# 3410
	addi	%r63, %r63, 1		# 3411
	call	%r60, quadratic.2631		# 3412
	addi	%r63, %r63, -1		# 3413
	ld	%r2, (%r63)		# 3414
	ld	%r3, 1(%r2)		# 3415
	cmpeqi	%r3, %r3, 3		# 3416
	jmpzi	%r3, else.16068		# 3417
	movhiz	%r3, 260096		# 3418
	fsub	%r1, %r1, %r3		# 3419
	jmpui	if_cont.16069		# 3420
else.16068:
if_cont.16069:
	ld	%r2, 6(%r2)		# 3421
	movi	%r3, 0		# 3422
	cmpfgt	%r4, %r3, %r1		# 3423
	jmpi	%r4, else.16070		# 3424
	movi	%r4, 0		# 3425
	jmpui	if_cont.16071		# 3426
else.16070:
	movi	%r4, 1		# 3427
if_cont.16071:
	cmpeqi	%r2, %r2, 0		# 3428
	jmpzi	%r2, else.16072		# 3429
	jmpui	if_cont.16073		# 3430
else.16072:
	cmpfgt	%r3, %r3, %r1		# 3431
	jmpi	%r3, else.16074		# 3432
	movi	%r4, 1		# 3433
	jmpui	if_cont.16075		# 3434
else.16074:
	movi	%r4, 0		# 3435
if_cont.16075:
if_cont.16073:
	cmpeqi	%r4, %r4, 0		# 3436
	jmpzi	%r4, else.16076		# 3437
	movi	%r1, 1		# 3438
	addi	%r63, %r63, -1		# 3439
	ld	%r60, (%r63)		# 3440
	jmpu	%r60		# 3441
else.16076:
	movi	%r1, 0		# 3442
	addi	%r63, %r63, -1		# 3443
	ld	%r60, (%r63)		# 3444
	jmpu	%r60		# 3445
.globl	check_all_inside.2733
check_all_inside.2733:
	sto	%r60, (%r63)		# 3446
	addi	%r63, %r63, 1		# 3447
	add	%r6, %r2, %r1		# 3448
	ld	%r6, (%r6)		# 3449
	cmpeqi	%r7, %r6, -1		# 3450
	jmpzi	%r7, else.16077		# 3451
	movi	%r1, 1		# 3452
	addi	%r63, %r63, -1		# 3453
	ld	%r60, (%r63)		# 3454
	jmpu	%r60		# 3455
else.16077:
	movi	%r7, min_caml_objects		# 3456
	add	%r7, %r7, %r6		# 3457
	ld	%r6, (%r7)		# 3458
	ld	%r7, 5(%r6)		# 3459
	addi	%r7, %r7, 0		# 3460
	ld	%r7, (%r7)		# 3461
	fsub	%r7, %r3, %r7		# 3462
	ld	%r8, 5(%r6)		# 3463
	addi	%r8, %r8, 1		# 3464
	ld	%r8, (%r8)		# 3465
	fsub	%r8, %r4, %r8		# 3466
	ld	%r9, 5(%r6)		# 3467
	addi	%r9, %r9, 2		# 3468
	ld	%r9, (%r9)		# 3469
	fsub	%r9, %r5, %r9		# 3470
	ld	%r10, 1(%r6)		# 3471
	cmpeqi	%r11, %r10, 1		# 3472
	sto	%r5, (%r63)		# 3473
	sto	%r4, 1(%r63)		# 3474
	sto	%r3, 2(%r63)		# 3475
	sto	%r2, 3(%r63)		# 3476
	sto	%r1, 4(%r63)		# 3477
	jmpzi	%r11, else.16078		# 3478
	fabs	%r7, %r7		# 3479
	ld	%r10, 4(%r6)		# 3480
	addi	%r10, %r10, 0		# 3481
	ld	%r10, (%r10)		# 3482
	cmpfgt	%r10, %r10, %r7		# 3483
	jmpi	%r10, else.16080		# 3484
	movi	%r7, 0		# 3485
	jmpui	if_cont.16081		# 3486
else.16080:
	fabs	%r8, %r8		# 3487
	ld	%r7, 4(%r6)		# 3488
	addi	%r7, %r7, 1		# 3489
	ld	%r7, (%r7)		# 3490
	cmpfgt	%r7, %r7, %r8		# 3491
	jmpi	%r7, else.16082		# 3492
	movi	%r7, 0		# 3493
	jmpui	if_cont.16083		# 3494
else.16082:
	fabs	%r9, %r9		# 3495
	ld	%r7, 4(%r6)		# 3496
	addi	%r7, %r7, 2		# 3497
	ld	%r7, (%r7)		# 3498
	cmpfgt	%r7, %r7, %r9		# 3499
	jmpi	%r7, else.16084		# 3500
	movi	%r7, 0		# 3501
	jmpui	if_cont.16085		# 3502
else.16084:
	movi	%r7, 1		# 3503
if_cont.16085:
if_cont.16083:
if_cont.16081:
	cmpeqi	%r7, %r7, 0		# 3504
	jmpzi	%r7, else.16086		# 3505
	ld	%r6, 6(%r6)		# 3506
	cmpeqi	%r6, %r6, 0		# 3507
	jmpzi	%r6, else.16088		# 3508
	movi	%r6, 1		# 3509
	jmpui	if_cont.16089		# 3510
else.16088:
	movi	%r6, 0		# 3511
if_cont.16089:
	jmpui	if_cont.16087		# 3512
else.16086:
	ld	%r6, 6(%r6)		# 3513
if_cont.16087:
	jmpui	if_cont.16079		# 3514
else.16078:
	cmpeqi	%r10, %r10, 2		# 3515
	jmpzi	%r10, else.16090		# 3516
	ld	%r10, 4(%r6)		# 3517
	addi	%r11, %r10, 0		# 3518
	ld	%r11, (%r11)		# 3519
	fmul	%r11, %r11, %r7		# 3520
	addi	%r7, %r10, 1		# 3521
	ld	%r7, (%r7)		# 3522
	fmul	%r7, %r7, %r8		# 3523
	fadd	%r11, %r11, %r7		# 3524
	addi	%r10, %r10, 2		# 3525
	ld	%r7, (%r10)		# 3526
	fmul	%r7, %r7, %r9		# 3527
	fadd	%r11, %r11, %r7		# 3528
	ld	%r6, 6(%r6)		# 3529
	movi	%r7, 0		# 3530
	cmpfgt	%r8, %r7, %r11		# 3531
	jmpi	%r8, else.16092		# 3532
	movi	%r8, 0		# 3533
	jmpui	if_cont.16093		# 3534
else.16092:
	movi	%r8, 1		# 3535
if_cont.16093:
	cmpeqi	%r6, %r6, 0		# 3536
	jmpzi	%r6, else.16094		# 3537
	jmpui	if_cont.16095		# 3538
else.16094:
	cmpfgt	%r7, %r7, %r11		# 3539
	jmpi	%r7, else.16096		# 3540
	movi	%r8, 1		# 3541
	jmpui	if_cont.16097		# 3542
else.16096:
	movi	%r8, 0		# 3543
if_cont.16097:
if_cont.16095:
	cmpeqi	%r8, %r8, 0		# 3544
	jmpzi	%r8, else.16098		# 3545
	movi	%r6, 1		# 3546
	jmpui	if_cont.16099		# 3547
else.16098:
	movi	%r6, 0		# 3548
if_cont.16099:
	jmpui	if_cont.16091		# 3549
else.16090:
	sto	%r6, 5(%r63)		# 3550
	mov	%r4, %r9		# 3551
	mov	%r3, %r8		# 3552
	mov	%r2, %r7		# 3553
	mov	%r1, %r6		# 3554
	addi	%r63, %r63, 6		# 3555
	call	%r60, quadratic.2631		# 3556
	addi	%r63, %r63, -6		# 3557
	ld	%r2, 5(%r63)		# 3558
	ld	%r3, 1(%r2)		# 3559
	cmpeqi	%r3, %r3, 3		# 3560
	jmpzi	%r3, else.16100		# 3561
	movhiz	%r3, 260096		# 3562
	fsub	%r1, %r1, %r3		# 3563
	jmpui	if_cont.16101		# 3564
else.16100:
if_cont.16101:
	ld	%r2, 6(%r2)		# 3565
	movi	%r3, 0		# 3566
	cmpfgt	%r4, %r3, %r1		# 3567
	jmpi	%r4, else.16102		# 3568
	movi	%r4, 0		# 3569
	jmpui	if_cont.16103		# 3570
else.16102:
	movi	%r4, 1		# 3571
if_cont.16103:
	cmpeqi	%r2, %r2, 0		# 3572
	jmpzi	%r2, else.16104		# 3573
	jmpui	if_cont.16105		# 3574
else.16104:
	cmpfgt	%r3, %r3, %r1		# 3575
	jmpi	%r3, else.16106		# 3576
	movi	%r4, 1		# 3577
	jmpui	if_cont.16107		# 3578
else.16106:
	movi	%r4, 0		# 3579
if_cont.16107:
if_cont.16105:
	cmpeqi	%r4, %r4, 0		# 3580
	jmpzi	%r4, else.16108		# 3581
	movi	%r6, 1		# 3582
	jmpui	if_cont.16109		# 3583
else.16108:
	movi	%r6, 0		# 3584
if_cont.16109:
if_cont.16091:
if_cont.16079:
	cmpeqi	%r6, %r6, 0		# 3585
	jmpzi	%r6, else.16110		# 3586
	ld	%r1, 4(%r63)		# 3587
	addi	%r1, %r1, 1		# 3588
	ld	%r2, 3(%r63)		# 3589
	add	%r3, %r2, %r1		# 3590
	ld	%r3, (%r3)		# 3591
	cmpeqi	%r4, %r3, -1		# 3592
	jmpzi	%r4, else.16111		# 3593
	movi	%r1, 1		# 3594
	addi	%r63, %r63, -1		# 3595
	ld	%r60, (%r63)		# 3596
	jmpu	%r60		# 3597
else.16111:
	movi	%r4, min_caml_objects		# 3598
	add	%r4, %r4, %r3		# 3599
	ld	%r3, (%r4)		# 3600
	ld	%r4, 2(%r63)		# 3601
	ld	%r5, 1(%r63)		# 3602
	ld	%r6, (%r63)		# 3603
	sto	%r1, 6(%r63)		# 3604
	mov	%r2, %r4		# 3605
	mov	%r1, %r3		# 3606
	mov	%r4, %r6		# 3607
	mov	%r3, %r5		# 3608
	addi	%r63, %r63, 7		# 3609
	call	%r60, is_outside.2728		# 3610
	addi	%r63, %r63, -7		# 3611
	cmpeqi	%r1, %r1, 0		# 3612
	jmpzi	%r1, else.16112		# 3613
	ld	%r1, 6(%r63)		# 3614
	addi	%r1, %r1, 1		# 3615
	ld	%r3, 2(%r63)		# 3616
	ld	%r4, 1(%r63)		# 3617
	ld	%r5, (%r63)		# 3618
	ld	%r2, 3(%r63)		# 3619
	addi	%r63, %r63, -1		# 3620
	ld	%r60, (%r63)		# 3621
	jmpui	check_all_inside.2733		# 3622
else.16112:
	movi	%r1, 0		# 3623
	addi	%r63, %r63, -1		# 3624
	ld	%r60, (%r63)		# 3625
	jmpu	%r60		# 3626
else.16110:
	movi	%r1, 0		# 3627
	addi	%r63, %r63, -1		# 3628
	ld	%r60, (%r63)		# 3629
	jmpu	%r60		# 3630
.globl	shadow_check_and_group.2739
shadow_check_and_group.2739:
	sto	%r60, (%r63)		# 3631
	addi	%r63, %r63, 1		# 3632
	add	%r3, %r2, %r1		# 3633
	ld	%r3, (%r3)		# 3634
	cmpeqi	%r3, %r3, -1		# 3635
	jmpzi	%r3, else.16113		# 3636
	movi	%r1, 0		# 3637
	addi	%r63, %r63, -1		# 3638
	ld	%r60, (%r63)		# 3639
	jmpu	%r60		# 3640
else.16113:
	add	%r3, %r2, %r1		# 3641
	ld	%r3, (%r3)		# 3642
	movi	%r4, min_caml_light_dirvec		# 3643
	movi	%r5, min_caml_intersection_point		# 3644
	movi	%r6, min_caml_objects		# 3645
	add	%r6, %r6, %r3		# 3646
	ld	%r6, (%r6)		# 3647
	addi	%r7, %r5, 0		# 3648
	ld	%r7, (%r7)		# 3649
	ld	%r8, 5(%r6)		# 3650
	addi	%r8, %r8, 0		# 3651
	ld	%r8, (%r8)		# 3652
	fsub	%r7, %r7, %r8		# 3653
	addi	%r8, %r5, 1		# 3654
	ld	%r8, (%r8)		# 3655
	ld	%r9, 5(%r6)		# 3656
	addi	%r9, %r9, 1		# 3657
	ld	%r9, (%r9)		# 3658
	fsub	%r8, %r8, %r9		# 3659
	addi	%r5, %r5, 2		# 3660
	ld	%r5, (%r5)		# 3661
	ld	%r9, 5(%r6)		# 3662
	addi	%r9, %r9, 2		# 3663
	ld	%r9, (%r9)		# 3664
	fsub	%r5, %r5, %r9		# 3665
	ld	%r9, 1(%r4)		# 3666
	add	%r9, %r9, %r3		# 3667
	ld	%r9, (%r9)		# 3668
	ld	%r10, 1(%r6)		# 3669
	cmpeqi	%r11, %r10, 1		# 3670
	sto	%r2, (%r63)		# 3671
	sto	%r1, 1(%r63)		# 3672
	sto	%r3, 2(%r63)		# 3673
	jmpzi	%r11, else.16114		# 3674
	ld	%r4, (%r4)		# 3675
	mov	%r3, %r9		# 3676
	mov	%r2, %r4		# 3677
	mov	%r1, %r6		# 3678
	mov	%r6, %r5		# 3679
	mov	%r4, %r7		# 3680
	mov	%r5, %r8		# 3681
	addi	%r63, %r63, 3		# 3682
	call	%r60, solver_rect_fast.2654		# 3683
	addi	%r63, %r63, -3		# 3684
	jmpui	if_cont.16115		# 3685
else.16114:
	cmpeqi	%r10, %r10, 2		# 3686
	jmpzi	%r10, else.16116		# 3687
	addi	%r4, %r9, 0		# 3688
	ld	%r4, (%r4)		# 3689
	movi	%r6, 0		# 3690
	cmpfgt	%r6, %r6, %r4		# 3691
	jmpi	%r6, else.16118		# 3692
	movi	%r1, 0		# 3693
	jmpui	if_cont.16119		# 3694
else.16118:
	movi	%r4, min_caml_solver_dist		# 3695
	addi	%r6, %r9, 1		# 3696
	ld	%r6, (%r6)		# 3697
	fmul	%r6, %r6, %r7		# 3698
	addi	%r7, %r9, 2		# 3699
	ld	%r7, (%r7)		# 3700
	fmul	%r7, %r7, %r8		# 3701
	fadd	%r6, %r6, %r7		# 3702
	addi	%r9, %r9, 3		# 3703
	ld	%r7, (%r9)		# 3704
	fmul	%r7, %r7, %r5		# 3705
	fadd	%r6, %r6, %r7		# 3706
	addi	%r4, %r4, 0		# 3707
	sto	%r6, (%r4)		# 3708
	movi	%r1, 1		# 3709
if_cont.16119:
	jmpui	if_cont.16117		# 3710
else.16116:
	mov	%r4, %r8		# 3711
	mov	%r3, %r7		# 3712
	mov	%r2, %r9		# 3713
	mov	%r1, %r6		# 3714
	addi	%r63, %r63, 3		# 3715
	call	%r60, solver_second_fast.2667		# 3716
	addi	%r63, %r63, -3		# 3717
if_cont.16117:
if_cont.16115:
	movi	%r2, min_caml_solver_dist		# 3718
	addi	%r2, %r2, 0		# 3719
	ld	%r2, (%r2)		# 3720
	cmpeqi	%r1, %r1, 0		# 3721
	jmpzi	%r1, else.16120		# 3722
	movi	%r1, 0		# 3723
	jmpui	if_cont.16121		# 3724
else.16120:
	movhiz	%r1, 779468		# 3725
	addi	%r1, %r1, 3277		# 3726
	cmpfgt	%r1, %r1, %r2		# 3727
	jmpi	%r1, else.16122		# 3728
	movi	%r1, 0		# 3729
	jmpui	if_cont.16123		# 3730
else.16122:
	movi	%r1, 1		# 3731
if_cont.16123:
if_cont.16121:
	cmpeqi	%r1, %r1, 0		# 3732
	jmpzi	%r1, else.16124		# 3733
	movi	%r1, min_caml_objects		# 3734
	ld	%r2, 2(%r63)		# 3735
	add	%r1, %r1, %r2		# 3736
	ld	%r1, (%r1)		# 3737
	ld	%r1, 6(%r1)		# 3738
	cmpeqi	%r1, %r1, 0		# 3739
	jmpzi	%r1, else.16125		# 3740
	movi	%r1, 0		# 3741
	addi	%r63, %r63, -1		# 3742
	ld	%r60, (%r63)		# 3743
	jmpu	%r60		# 3744
else.16125:
	ld	%r1, 1(%r63)		# 3745
	addi	%r1, %r1, 1		# 3746
	ld	%r2, (%r63)		# 3747
	addi	%r63, %r63, -1		# 3748
	ld	%r60, (%r63)		# 3749
	jmpui	shadow_check_and_group.2739		# 3750
else.16124:
	movhiz	%r1, 246333		# 3751
	addi	%r1, %r1, 1802		# 3752
	fadd	%r2, %r2, %r1		# 3753
	movi	%r1, min_caml_light		# 3754
	addi	%r1, %r1, 0		# 3755
	ld	%r1, (%r1)		# 3756
	fmul	%r1, %r1, %r2		# 3757
	movi	%r3, min_caml_intersection_point		# 3758
	addi	%r3, %r3, 0		# 3759
	ld	%r3, (%r3)		# 3760
	fadd	%r1, %r1, %r3		# 3761
	movi	%r3, min_caml_light		# 3762
	addi	%r3, %r3, 1		# 3763
	ld	%r3, (%r3)		# 3764
	fmul	%r3, %r3, %r2		# 3765
	movi	%r4, min_caml_intersection_point		# 3766
	addi	%r4, %r4, 1		# 3767
	ld	%r4, (%r4)		# 3768
	fadd	%r3, %r3, %r4		# 3769
	movi	%r4, min_caml_light		# 3770
	addi	%r4, %r4, 2		# 3771
	ld	%r4, (%r4)		# 3772
	fmul	%r4, %r4, %r2		# 3773
	movi	%r2, min_caml_intersection_point		# 3774
	addi	%r2, %r2, 2		# 3775
	ld	%r2, (%r2)		# 3776
	fadd	%r4, %r4, %r2		# 3777
	ld	%r2, (%r63)		# 3778
	addi	%r5, %r2, 0		# 3779
	ld	%r5, (%r5)		# 3780
	cmpeqi	%r6, %r5, -1		# 3781
	jmpzi	%r6, else.16126		# 3782
	movi	%r1, 1		# 3783
	jmpui	if_cont.16127		# 3784
else.16126:
	movi	%r6, min_caml_objects		# 3785
	add	%r6, %r6, %r5		# 3786
	ld	%r5, (%r6)		# 3787
	sto	%r4, 3(%r63)		# 3788
	sto	%r3, 4(%r63)		# 3789
	sto	%r1, 5(%r63)		# 3790
	mov	%r2, %r1		# 3791
	mov	%r1, %r5		# 3792
	addi	%r63, %r63, 6		# 3793
	call	%r60, is_outside.2728		# 3794
	addi	%r63, %r63, -6		# 3795
	cmpeqi	%r1, %r1, 0		# 3796
	jmpzi	%r1, else.16128		# 3797
	movi	%r1, 1		# 3798
	ld	%r3, 5(%r63)		# 3799
	ld	%r4, 4(%r63)		# 3800
	ld	%r5, 3(%r63)		# 3801
	ld	%r2, (%r63)		# 3802
	addi	%r63, %r63, 6		# 3803
	call	%r60, check_all_inside.2733		# 3804
	addi	%r63, %r63, -6		# 3805
	jmpui	if_cont.16129		# 3806
else.16128:
	movi	%r1, 0		# 3807
if_cont.16129:
if_cont.16127:
	cmpeqi	%r1, %r1, 0		# 3808
	jmpzi	%r1, else.16130		# 3809
	ld	%r1, 1(%r63)		# 3810
	addi	%r1, %r1, 1		# 3811
	ld	%r2, (%r63)		# 3812
	addi	%r63, %r63, -1		# 3813
	ld	%r60, (%r63)		# 3814
	jmpui	shadow_check_and_group.2739		# 3815
else.16130:
	movi	%r1, 1		# 3816
	addi	%r63, %r63, -1		# 3817
	ld	%r60, (%r63)		# 3818
	jmpu	%r60		# 3819
.globl	shadow_check_one_or_group.2742
shadow_check_one_or_group.2742:
	sto	%r60, (%r63)		# 3820
	addi	%r63, %r63, 1		# 3821
	add	%r3, %r2, %r1		# 3822
	ld	%r3, (%r3)		# 3823
	cmpeqi	%r4, %r3, -1		# 3824
	jmpzi	%r4, else.16131		# 3825
	movi	%r1, 0		# 3826
	addi	%r63, %r63, -1		# 3827
	ld	%r60, (%r63)		# 3828
	jmpu	%r60		# 3829
else.16131:
	movi	%r4, min_caml_and_net		# 3830
	add	%r4, %r4, %r3		# 3831
	ld	%r3, (%r4)		# 3832
	movi	%r4, 0		# 3833
	sto	%r2, (%r63)		# 3834
	sto	%r1, 1(%r63)		# 3835
	mov	%r2, %r3		# 3836
	mov	%r1, %r4		# 3837
	addi	%r63, %r63, 2		# 3838
	call	%r60, shadow_check_and_group.2739		# 3839
	addi	%r63, %r63, -2		# 3840
	cmpeqi	%r1, %r1, 0		# 3841
	jmpzi	%r1, else.16132		# 3842
	ld	%r1, 1(%r63)		# 3843
	addi	%r1, %r1, 1		# 3844
	ld	%r2, (%r63)		# 3845
	add	%r3, %r2, %r1		# 3846
	ld	%r3, (%r3)		# 3847
	cmpeqi	%r4, %r3, -1		# 3848
	jmpzi	%r4, else.16133		# 3849
	movi	%r1, 0		# 3850
	addi	%r63, %r63, -1		# 3851
	ld	%r60, (%r63)		# 3852
	jmpu	%r60		# 3853
else.16133:
	movi	%r4, min_caml_and_net		# 3854
	add	%r4, %r4, %r3		# 3855
	ld	%r3, (%r4)		# 3856
	movi	%r4, 0		# 3857
	sto	%r1, 2(%r63)		# 3858
	mov	%r2, %r3		# 3859
	mov	%r1, %r4		# 3860
	addi	%r63, %r63, 3		# 3861
	call	%r60, shadow_check_and_group.2739		# 3862
	addi	%r63, %r63, -3		# 3863
	cmpeqi	%r1, %r1, 0		# 3864
	jmpzi	%r1, else.16134		# 3865
	ld	%r1, 2(%r63)		# 3866
	addi	%r1, %r1, 1		# 3867
	ld	%r2, (%r63)		# 3868
	add	%r3, %r2, %r1		# 3869
	ld	%r3, (%r3)		# 3870
	cmpeqi	%r4, %r3, -1		# 3871
	jmpzi	%r4, else.16135		# 3872
	movi	%r1, 0		# 3873
	addi	%r63, %r63, -1		# 3874
	ld	%r60, (%r63)		# 3875
	jmpu	%r60		# 3876
else.16135:
	movi	%r4, min_caml_and_net		# 3877
	add	%r4, %r4, %r3		# 3878
	ld	%r3, (%r4)		# 3879
	movi	%r4, 0		# 3880
	sto	%r1, 3(%r63)		# 3881
	mov	%r2, %r3		# 3882
	mov	%r1, %r4		# 3883
	addi	%r63, %r63, 4		# 3884
	call	%r60, shadow_check_and_group.2739		# 3885
	addi	%r63, %r63, -4		# 3886
	cmpeqi	%r1, %r1, 0		# 3887
	jmpzi	%r1, else.16136		# 3888
	ld	%r1, 3(%r63)		# 3889
	addi	%r1, %r1, 1		# 3890
	ld	%r2, (%r63)		# 3891
	add	%r3, %r2, %r1		# 3892
	ld	%r3, (%r3)		# 3893
	cmpeqi	%r4, %r3, -1		# 3894
	jmpzi	%r4, else.16137		# 3895
	movi	%r1, 0		# 3896
	addi	%r63, %r63, -1		# 3897
	ld	%r60, (%r63)		# 3898
	jmpu	%r60		# 3899
else.16137:
	movi	%r4, min_caml_and_net		# 3900
	add	%r4, %r4, %r3		# 3901
	ld	%r3, (%r4)		# 3902
	movi	%r4, 0		# 3903
	sto	%r1, 4(%r63)		# 3904
	mov	%r2, %r3		# 3905
	mov	%r1, %r4		# 3906
	addi	%r63, %r63, 5		# 3907
	call	%r60, shadow_check_and_group.2739		# 3908
	addi	%r63, %r63, -5		# 3909
	cmpeqi	%r1, %r1, 0		# 3910
	jmpzi	%r1, else.16138		# 3911
	ld	%r1, 4(%r63)		# 3912
	addi	%r1, %r1, 1		# 3913
	ld	%r2, (%r63)		# 3914
	addi	%r63, %r63, -1		# 3915
	ld	%r60, (%r63)		# 3916
	jmpui	shadow_check_one_or_group.2742		# 3917
else.16138:
	movi	%r1, 1		# 3918
	addi	%r63, %r63, -1		# 3919
	ld	%r60, (%r63)		# 3920
	jmpu	%r60		# 3921
else.16136:
	movi	%r1, 1		# 3922
	addi	%r63, %r63, -1		# 3923
	ld	%r60, (%r63)		# 3924
	jmpu	%r60		# 3925
else.16134:
	movi	%r1, 1		# 3926
	addi	%r63, %r63, -1		# 3927
	ld	%r60, (%r63)		# 3928
	jmpu	%r60		# 3929
else.16132:
	movi	%r1, 1		# 3930
	addi	%r63, %r63, -1		# 3931
	ld	%r60, (%r63)		# 3932
	jmpu	%r60		# 3933
.globl	shadow_check_one_or_matrix.2745
shadow_check_one_or_matrix.2745:
	sto	%r60, (%r63)		# 3934
	addi	%r63, %r63, 1		# 3935
	add	%r3, %r2, %r1		# 3936
	ld	%r3, (%r3)		# 3937
	addi	%r4, %r3, 0		# 3938
	ld	%r4, (%r4)		# 3939
	cmpeqi	%r5, %r4, -1		# 3940
	jmpzi	%r5, else.16139		# 3941
	movi	%r1, 0		# 3942
	addi	%r63, %r63, -1		# 3943
	ld	%r60, (%r63)		# 3944
	jmpu	%r60		# 3945
else.16139:
	cmpeqi	%r5, %r4, 99		# 3946
	sto	%r3, (%r63)		# 3947
	sto	%r2, 1(%r63)		# 3948
	sto	%r1, 2(%r63)		# 3949
	jmpzi	%r5, else.16140		# 3950
	movi	%r1, 1		# 3951
	jmpui	if_cont.16141		# 3952
else.16140:
	movi	%r5, min_caml_light_dirvec		# 3953
	movi	%r6, min_caml_intersection_point		# 3954
	movi	%r7, min_caml_objects		# 3955
	add	%r7, %r7, %r4		# 3956
	ld	%r7, (%r7)		# 3957
	addi	%r8, %r6, 0		# 3958
	ld	%r8, (%r8)		# 3959
	ld	%r9, 5(%r7)		# 3960
	addi	%r9, %r9, 0		# 3961
	ld	%r9, (%r9)		# 3962
	fsub	%r8, %r8, %r9		# 3963
	addi	%r9, %r6, 1		# 3964
	ld	%r9, (%r9)		# 3965
	ld	%r10, 5(%r7)		# 3966
	addi	%r10, %r10, 1		# 3967
	ld	%r10, (%r10)		# 3968
	fsub	%r9, %r9, %r10		# 3969
	addi	%r6, %r6, 2		# 3970
	ld	%r6, (%r6)		# 3971
	ld	%r10, 5(%r7)		# 3972
	addi	%r10, %r10, 2		# 3973
	ld	%r10, (%r10)		# 3974
	fsub	%r6, %r6, %r10		# 3975
	ld	%r10, 1(%r5)		# 3976
	add	%r10, %r10, %r4		# 3977
	ld	%r4, (%r10)		# 3978
	ld	%r10, 1(%r7)		# 3979
	cmpeqi	%r11, %r10, 1		# 3980
	jmpzi	%r11, else.16142		# 3981
	ld	%r5, (%r5)		# 3982
	mov	%r3, %r4		# 3983
	mov	%r2, %r5		# 3984
	mov	%r1, %r7		# 3985
	mov	%r5, %r9		# 3986
	mov	%r4, %r8		# 3987
	addi	%r63, %r63, 3		# 3988
	call	%r60, solver_rect_fast.2654		# 3989
	addi	%r63, %r63, -3		# 3990
	jmpui	if_cont.16143		# 3991
else.16142:
	cmpeqi	%r10, %r10, 2		# 3992
	jmpzi	%r10, else.16144		# 3993
	addi	%r5, %r4, 0		# 3994
	ld	%r5, (%r5)		# 3995
	movi	%r7, 0		# 3996
	cmpfgt	%r7, %r7, %r5		# 3997
	jmpi	%r7, else.16146		# 3998
	movi	%r1, 0		# 3999
	jmpui	if_cont.16147		# 4000
else.16146:
	movi	%r5, min_caml_solver_dist		# 4001
	addi	%r7, %r4, 1		# 4002
	ld	%r7, (%r7)		# 4003
	fmul	%r7, %r7, %r8		# 4004
	addi	%r8, %r4, 2		# 4005
	ld	%r8, (%r8)		# 4006
	fmul	%r8, %r8, %r9		# 4007
	fadd	%r7, %r7, %r8		# 4008
	addi	%r4, %r4, 3		# 4009
	ld	%r4, (%r4)		# 4010
	fmul	%r4, %r4, %r6		# 4011
	fadd	%r7, %r7, %r4		# 4012
	addi	%r5, %r5, 0		# 4013
	sto	%r7, (%r5)		# 4014
	movi	%r1, 1		# 4015
if_cont.16147:
	jmpui	if_cont.16145		# 4016
else.16144:
	mov	%r5, %r6		# 4017
	mov	%r3, %r8		# 4018
	mov	%r2, %r4		# 4019
	mov	%r1, %r7		# 4020
	mov	%r4, %r9		# 4021
	addi	%r63, %r63, 3		# 4022
	call	%r60, solver_second_fast.2667		# 4023
	addi	%r63, %r63, -3		# 4024
if_cont.16145:
if_cont.16143:
	cmpeqi	%r1, %r1, 0		# 4025
	jmpzi	%r1, else.16148		# 4026
	movi	%r1, 0		# 4027
	jmpui	if_cont.16149		# 4028
else.16148:
	movi	%r1, min_caml_solver_dist		# 4029
	addi	%r1, %r1, 0		# 4030
	ld	%r1, (%r1)		# 4031
	movhiz	%r2, 777420		# 4032
	addi	%r2, %r2, 3277		# 4033
	cmpfgt	%r2, %r2, %r1		# 4034
	jmpi	%r2, else.16150		# 4035
	movi	%r1, 0		# 4036
	jmpui	if_cont.16151		# 4037
else.16150:
	ld	%r1, (%r63)		# 4038
	addi	%r2, %r1, 1		# 4039
	ld	%r2, (%r2)		# 4040
	cmpeqi	%r3, %r2, -1		# 4041
	jmpzi	%r3, else.16152		# 4042
	movi	%r1, 0		# 4043
	jmpui	if_cont.16153		# 4044
else.16152:
	movi	%r3, min_caml_and_net		# 4045
	add	%r3, %r3, %r2		# 4046
	ld	%r2, (%r3)		# 4047
	movi	%r3, 0		# 4048
	mov	%r1, %r3		# 4049
	addi	%r63, %r63, 3		# 4050
	call	%r60, shadow_check_and_group.2739		# 4051
	addi	%r63, %r63, -3		# 4052
	cmpeqi	%r1, %r1, 0		# 4053
	jmpzi	%r1, else.16154		# 4054
	ld	%r1, (%r63)		# 4055
	addi	%r2, %r1, 2		# 4056
	ld	%r2, (%r2)		# 4057
	cmpeqi	%r3, %r2, -1		# 4058
	jmpzi	%r3, else.16156		# 4059
	movi	%r1, 0		# 4060
	jmpui	if_cont.16157		# 4061
else.16156:
	movi	%r3, min_caml_and_net		# 4062
	add	%r3, %r3, %r2		# 4063
	ld	%r2, (%r3)		# 4064
	movi	%r3, 0		# 4065
	mov	%r1, %r3		# 4066
	addi	%r63, %r63, 3		# 4067
	call	%r60, shadow_check_and_group.2739		# 4068
	addi	%r63, %r63, -3		# 4069
	cmpeqi	%r1, %r1, 0		# 4070
	jmpzi	%r1, else.16158		# 4071
	ld	%r1, (%r63)		# 4072
	addi	%r2, %r1, 3		# 4073
	ld	%r2, (%r2)		# 4074
	cmpeqi	%r3, %r2, -1		# 4075
	jmpzi	%r3, else.16160		# 4076
	movi	%r1, 0		# 4077
	jmpui	if_cont.16161		# 4078
else.16160:
	movi	%r3, min_caml_and_net		# 4079
	add	%r3, %r3, %r2		# 4080
	ld	%r2, (%r3)		# 4081
	movi	%r3, 0		# 4082
	mov	%r1, %r3		# 4083
	addi	%r63, %r63, 3		# 4084
	call	%r60, shadow_check_and_group.2739		# 4085
	addi	%r63, %r63, -3		# 4086
	cmpeqi	%r1, %r1, 0		# 4087
	jmpzi	%r1, else.16162		# 4088
	movi	%r1, 4		# 4089
	ld	%r2, (%r63)		# 4090
	addi	%r63, %r63, 3		# 4091
	call	%r60, shadow_check_one_or_group.2742		# 4092
	addi	%r63, %r63, -3		# 4093
	jmpui	if_cont.16163		# 4094
else.16162:
	movi	%r1, 1		# 4095
if_cont.16163:
if_cont.16161:
	jmpui	if_cont.16159		# 4096
else.16158:
	movi	%r1, 1		# 4097
if_cont.16159:
if_cont.16157:
	jmpui	if_cont.16155		# 4098
else.16154:
	movi	%r1, 1		# 4099
if_cont.16155:
if_cont.16153:
	cmpeqi	%r1, %r1, 0		# 4100
	jmpzi	%r1, else.16164		# 4101
	movi	%r1, 0		# 4102
	jmpui	if_cont.16165		# 4103
else.16164:
	movi	%r1, 1		# 4104
if_cont.16165:
if_cont.16151:
if_cont.16149:
if_cont.16141:
	cmpeqi	%r1, %r1, 0		# 4105
	jmpzi	%r1, else.16166		# 4106
	ld	%r1, 2(%r63)		# 4107
	addi	%r1, %r1, 1		# 4108
	ld	%r2, 1(%r63)		# 4109
	addi	%r63, %r63, -1		# 4110
	ld	%r60, (%r63)		# 4111
	jmpui	shadow_check_one_or_matrix.2745		# 4112
else.16166:
	ld	%r1, (%r63)		# 4113
	addi	%r2, %r1, 1		# 4114
	ld	%r2, (%r2)		# 4115
	cmpeqi	%r3, %r2, -1		# 4116
	jmpzi	%r3, else.16167		# 4117
	movi	%r1, 0		# 4118
	jmpui	if_cont.16168		# 4119
else.16167:
	movi	%r3, min_caml_and_net		# 4120
	add	%r3, %r3, %r2		# 4121
	ld	%r2, (%r3)		# 4122
	movi	%r3, 0		# 4123
	mov	%r1, %r3		# 4124
	addi	%r63, %r63, 3		# 4125
	call	%r60, shadow_check_and_group.2739		# 4126
	addi	%r63, %r63, -3		# 4127
	cmpeqi	%r1, %r1, 0		# 4128
	jmpzi	%r1, else.16169		# 4129
	ld	%r1, (%r63)		# 4130
	addi	%r2, %r1, 2		# 4131
	ld	%r2, (%r2)		# 4132
	cmpeqi	%r3, %r2, -1		# 4133
	jmpzi	%r3, else.16171		# 4134
	movi	%r1, 0		# 4135
	jmpui	if_cont.16172		# 4136
else.16171:
	movi	%r3, min_caml_and_net		# 4137
	add	%r3, %r3, %r2		# 4138
	ld	%r2, (%r3)		# 4139
	movi	%r3, 0		# 4140
	mov	%r1, %r3		# 4141
	addi	%r63, %r63, 3		# 4142
	call	%r60, shadow_check_and_group.2739		# 4143
	addi	%r63, %r63, -3		# 4144
	cmpeqi	%r1, %r1, 0		# 4145
	jmpzi	%r1, else.16173		# 4146
	ld	%r1, (%r63)		# 4147
	addi	%r2, %r1, 3		# 4148
	ld	%r2, (%r2)		# 4149
	cmpeqi	%r3, %r2, -1		# 4150
	jmpzi	%r3, else.16175		# 4151
	movi	%r1, 0		# 4152
	jmpui	if_cont.16176		# 4153
else.16175:
	movi	%r3, min_caml_and_net		# 4154
	add	%r3, %r3, %r2		# 4155
	ld	%r2, (%r3)		# 4156
	movi	%r3, 0		# 4157
	mov	%r1, %r3		# 4158
	addi	%r63, %r63, 3		# 4159
	call	%r60, shadow_check_and_group.2739		# 4160
	addi	%r63, %r63, -3		# 4161
	cmpeqi	%r1, %r1, 0		# 4162
	jmpzi	%r1, else.16177		# 4163
	movi	%r1, 4		# 4164
	ld	%r2, (%r63)		# 4165
	addi	%r63, %r63, 3		# 4166
	call	%r60, shadow_check_one_or_group.2742		# 4167
	addi	%r63, %r63, -3		# 4168
	jmpui	if_cont.16178		# 4169
else.16177:
	movi	%r1, 1		# 4170
if_cont.16178:
if_cont.16176:
	jmpui	if_cont.16174		# 4171
else.16173:
	movi	%r1, 1		# 4172
if_cont.16174:
if_cont.16172:
	jmpui	if_cont.16170		# 4173
else.16169:
	movi	%r1, 1		# 4174
if_cont.16170:
if_cont.16168:
	cmpeqi	%r1, %r1, 0		# 4175
	jmpzi	%r1, else.16179		# 4176
	ld	%r1, 2(%r63)		# 4177
	addi	%r1, %r1, 1		# 4178
	ld	%r2, 1(%r63)		# 4179
	addi	%r63, %r63, -1		# 4180
	ld	%r60, (%r63)		# 4181
	jmpui	shadow_check_one_or_matrix.2745		# 4182
else.16179:
	movi	%r1, 1		# 4183
	addi	%r63, %r63, -1		# 4184
	ld	%r60, (%r63)		# 4185
	jmpu	%r60		# 4186
.globl	solve_each_element.2748
solve_each_element.2748:
	sto	%r60, (%r63)		# 4187
	addi	%r63, %r63, 1		# 4188
	add	%r4, %r2, %r1		# 4189
	ld	%r4, (%r4)		# 4190
	cmpeqi	%r5, %r4, -1		# 4191
	jmpzi	%r5, else.16180		# 4192
	addi	%r63, %r63, -1		# 4193
	ld	%r60, (%r63)		# 4194
	jmpu	%r60		# 4195
else.16180:
	movi	%r5, min_caml_startp		# 4196
	movi	%r6, min_caml_objects		# 4197
	add	%r6, %r6, %r4		# 4198
	ld	%r6, (%r6)		# 4199
	addi	%r7, %r5, 0		# 4200
	ld	%r7, (%r7)		# 4201
	ld	%r8, 5(%r6)		# 4202
	addi	%r8, %r8, 0		# 4203
	ld	%r8, (%r8)		# 4204
	fsub	%r7, %r7, %r8		# 4205
	addi	%r8, %r5, 1		# 4206
	ld	%r8, (%r8)		# 4207
	ld	%r9, 5(%r6)		# 4208
	addi	%r9, %r9, 1		# 4209
	ld	%r9, (%r9)		# 4210
	fsub	%r8, %r8, %r9		# 4211
	addi	%r5, %r5, 2		# 4212
	ld	%r5, (%r5)		# 4213
	ld	%r9, 5(%r6)		# 4214
	addi	%r9, %r9, 2		# 4215
	ld	%r9, (%r9)		# 4216
	fsub	%r5, %r5, %r9		# 4217
	ld	%r9, 1(%r6)		# 4218
	cmpeqi	%r10, %r9, 1		# 4219
	sto	%r3, (%r63)		# 4220
	sto	%r2, 1(%r63)		# 4221
	sto	%r1, 2(%r63)		# 4222
	sto	%r4, 3(%r63)		# 4223
	jmpzi	%r10, else.16182		# 4224
	movi	%r9, 0		# 4225
	movi	%r10, 1		# 4226
	movi	%r11, 2		# 4227
	sto	%r7, 4(%r63)		# 4228
	sto	%r5, 5(%r63)		# 4229
	sto	%r8, 6(%r63)		# 4230
	sto	%r6, 7(%r63)		# 4231
	mov	%r4, %r10		# 4232
	mov	%r2, %r3		# 4233
	mov	%r1, %r6		# 4234
	mov	%r6, %r7		# 4235
	mov	%r3, %r9		# 4236
	mov	%r7, %r8		# 4237
	mov	%r8, %r5		# 4238
	mov	%r5, %r11		# 4239
	addi	%r63, %r63, 8		# 4240
	call	%r60, solver_rect_surface.2610		# 4241
	addi	%r63, %r63, -8		# 4242
	cmpeqi	%r1, %r1, 0		# 4243
	jmpzi	%r1, else.16184		# 4244
	movi	%r3, 1		# 4245
	movi	%r4, 2		# 4246
	movi	%r5, 0		# 4247
	ld	%r6, 6(%r63)		# 4248
	ld	%r7, 5(%r63)		# 4249
	ld	%r8, 4(%r63)		# 4250
	ld	%r1, 7(%r63)		# 4251
	ld	%r2, (%r63)		# 4252
	addi	%r63, %r63, 8		# 4253
	call	%r60, solver_rect_surface.2610		# 4254
	addi	%r63, %r63, -8		# 4255
	cmpeqi	%r1, %r1, 0		# 4256
	jmpzi	%r1, else.16186		# 4257
	movi	%r3, 2		# 4258
	movi	%r4, 0		# 4259
	movi	%r5, 1		# 4260
	ld	%r6, 5(%r63)		# 4261
	ld	%r7, 4(%r63)		# 4262
	ld	%r8, 6(%r63)		# 4263
	ld	%r1, 7(%r63)		# 4264
	ld	%r2, (%r63)		# 4265
	addi	%r63, %r63, 8		# 4266
	call	%r60, solver_rect_surface.2610		# 4267
	addi	%r63, %r63, -8		# 4268
	cmpeqi	%r1, %r1, 0		# 4269
	jmpzi	%r1, else.16188		# 4270
	movi	%r1, 0		# 4271
	jmpui	if_cont.16189		# 4272
else.16188:
	movi	%r1, 3		# 4273
if_cont.16189:
	jmpui	if_cont.16187		# 4274
else.16186:
	movi	%r1, 2		# 4275
if_cont.16187:
	jmpui	if_cont.16185		# 4276
else.16184:
	movi	%r1, 1		# 4277
if_cont.16185:
	jmpui	if_cont.16183		# 4278
else.16182:
	cmpeqi	%r9, %r9, 2		# 4279
	jmpzi	%r9, else.16190		# 4280
	mov	%r4, %r8		# 4281
	mov	%r2, %r3		# 4282
	mov	%r1, %r6		# 4283
	mov	%r3, %r7		# 4284
	addi	%r63, %r63, 8		# 4285
	call	%r60, solver_surface.2625		# 4286
	addi	%r63, %r63, -8		# 4287
	jmpui	if_cont.16191		# 4288
else.16190:
	mov	%r4, %r8		# 4289
	mov	%r2, %r3		# 4290
	mov	%r1, %r6		# 4291
	mov	%r3, %r7		# 4292
	addi	%r63, %r63, 8		# 4293
	call	%r60, solver_second.2644		# 4294
	addi	%r63, %r63, -8		# 4295
if_cont.16191:
if_cont.16183:
	cmpeqi	%r2, %r1, 0		# 4296
	jmpzi	%r2, else.16192		# 4297
	movi	%r1, min_caml_objects		# 4298
	ld	%r2, 3(%r63)		# 4299
	add	%r1, %r1, %r2		# 4300
	ld	%r1, (%r1)		# 4301
	ld	%r1, 6(%r1)		# 4302
	cmpeqi	%r1, %r1, 0		# 4303
	jmpzi	%r1, else.16193		# 4304
	addi	%r63, %r63, -1		# 4305
	ld	%r60, (%r63)		# 4306
	jmpu	%r60		# 4307
else.16193:
	ld	%r1, 2(%r63)		# 4308
	addi	%r1, %r1, 1		# 4309
	ld	%r2, 1(%r63)		# 4310
	ld	%r3, (%r63)		# 4311
	addi	%r63, %r63, -1		# 4312
	ld	%r60, (%r63)		# 4313
	jmpui	solve_each_element.2748		# 4314
else.16192:
	movi	%r2, min_caml_solver_dist		# 4315
	addi	%r2, %r2, 0		# 4316
	ld	%r2, (%r2)		# 4317
	movi	%r3, 0		# 4318
	cmpfgt	%r3, %r2, %r3		# 4319
	jmpi	%r3, else.16195		# 4320
	jmpui	if_cont.16196		# 4321
else.16195:
	movi	%r3, min_caml_tmin		# 4322
	addi	%r3, %r3, 0		# 4323
	ld	%r3, (%r3)		# 4324
	cmpfgt	%r3, %r3, %r2		# 4325
	jmpi	%r3, else.16197		# 4326
	jmpui	if_cont.16198		# 4327
else.16197:
	movhiz	%r3, 246333		# 4328
	addi	%r3, %r3, 1802		# 4329
	fadd	%r2, %r2, %r3		# 4330
	ld	%r3, (%r63)		# 4331
	addi	%r4, %r3, 0		# 4332
	ld	%r4, (%r4)		# 4333
	fmul	%r4, %r4, %r2		# 4334
	movi	%r5, min_caml_startp		# 4335
	addi	%r5, %r5, 0		# 4336
	ld	%r5, (%r5)		# 4337
	fadd	%r4, %r4, %r5		# 4338
	addi	%r5, %r3, 1		# 4339
	ld	%r5, (%r5)		# 4340
	fmul	%r5, %r5, %r2		# 4341
	movi	%r6, min_caml_startp		# 4342
	addi	%r6, %r6, 1		# 4343
	ld	%r6, (%r6)		# 4344
	fadd	%r5, %r5, %r6		# 4345
	addi	%r6, %r3, 2		# 4346
	ld	%r6, (%r6)		# 4347
	fmul	%r6, %r6, %r2		# 4348
	movi	%r7, min_caml_startp		# 4349
	addi	%r7, %r7, 2		# 4350
	ld	%r7, (%r7)		# 4351
	fadd	%r6, %r6, %r7		# 4352
	ld	%r7, 1(%r63)		# 4353
	addi	%r8, %r7, 0		# 4354
	ld	%r8, (%r8)		# 4355
	cmpeqi	%r9, %r8, -1		# 4356
	sto	%r1, 8(%r63)		# 4357
	sto	%r6, 9(%r63)		# 4358
	sto	%r5, 10(%r63)		# 4359
	sto	%r4, 11(%r63)		# 4360
	sto	%r2, 12(%r63)		# 4361
	jmpzi	%r9, else.16199		# 4362
	movi	%r1, 1		# 4363
	jmpui	if_cont.16200		# 4364
else.16199:
	movi	%r9, min_caml_objects		# 4365
	add	%r9, %r9, %r8		# 4366
	ld	%r8, (%r9)		# 4367
	mov	%r3, %r5		# 4368
	mov	%r2, %r4		# 4369
	mov	%r1, %r8		# 4370
	mov	%r4, %r6		# 4371
	addi	%r63, %r63, 13		# 4372
	call	%r60, is_outside.2728		# 4373
	addi	%r63, %r63, -13		# 4374
	cmpeqi	%r1, %r1, 0		# 4375
	jmpzi	%r1, else.16201		# 4376
	movi	%r1, 1		# 4377
	ld	%r3, 11(%r63)		# 4378
	ld	%r4, 10(%r63)		# 4379
	ld	%r5, 9(%r63)		# 4380
	ld	%r2, 1(%r63)		# 4381
	addi	%r63, %r63, 13		# 4382
	call	%r60, check_all_inside.2733		# 4383
	addi	%r63, %r63, -13		# 4384
	jmpui	if_cont.16202		# 4385
else.16201:
	movi	%r1, 0		# 4386
if_cont.16202:
if_cont.16200:
	cmpeqi	%r1, %r1, 0		# 4387
	jmpzi	%r1, else.16203		# 4388
	jmpui	if_cont.16204		# 4389
else.16203:
	movi	%r1, min_caml_tmin		# 4390
	addi	%r1, %r1, 0		# 4391
	ld	%r2, 12(%r63)		# 4392
	sto	%r2, (%r1)		# 4393
	movi	%r1, min_caml_intersection_point		# 4394
	addi	%r2, %r1, 0		# 4395
	ld	%r3, 11(%r63)		# 4396
	sto	%r3, (%r2)		# 4397
	addi	%r2, %r1, 1		# 4398
	ld	%r3, 10(%r63)		# 4399
	sto	%r3, (%r2)		# 4400
	addi	%r1, %r1, 2		# 4401
	ld	%r2, 9(%r63)		# 4402
	sto	%r2, (%r1)		# 4403
	movi	%r1, min_caml_intersected_object_id		# 4404
	addi	%r1, %r1, 0		# 4405
	ld	%r2, 3(%r63)		# 4406
	sto	%r2, (%r1)		# 4407
	movi	%r1, min_caml_intsec_rectside		# 4408
	addi	%r1, %r1, 0		# 4409
	ld	%r2, 8(%r63)		# 4410
	sto	%r2, (%r1)		# 4411
if_cont.16204:
if_cont.16198:
if_cont.16196:
	ld	%r1, 2(%r63)		# 4412
	addi	%r1, %r1, 1		# 4413
	ld	%r2, 1(%r63)		# 4414
	ld	%r3, (%r63)		# 4415
	addi	%r63, %r63, -1		# 4416
	ld	%r60, (%r63)		# 4417
	jmpui	solve_each_element.2748		# 4418
.globl	solve_one_or_network.2752
solve_one_or_network.2752:
	sto	%r60, (%r63)		# 4419
	addi	%r63, %r63, 1		# 4420
	add	%r4, %r2, %r1		# 4421
	ld	%r4, (%r4)		# 4422
	cmpeqi	%r5, %r4, -1		# 4423
	jmpzi	%r5, else.16205		# 4424
	addi	%r63, %r63, -1		# 4425
	ld	%r60, (%r63)		# 4426
	jmpu	%r60		# 4427
else.16205:
	movi	%r5, min_caml_and_net		# 4428
	add	%r5, %r5, %r4		# 4429
	ld	%r4, (%r5)		# 4430
	movi	%r5, 0		# 4431
	sto	%r3, (%r63)		# 4432
	sto	%r2, 1(%r63)		# 4433
	sto	%r1, 2(%r63)		# 4434
	mov	%r2, %r4		# 4435
	mov	%r1, %r5		# 4436
	addi	%r63, %r63, 3		# 4437
	call	%r60, solve_each_element.2748		# 4438
	addi	%r63, %r63, -3		# 4439
	ld	%r1, 2(%r63)		# 4440
	addi	%r1, %r1, 1		# 4441
	ld	%r2, 1(%r63)		# 4442
	add	%r3, %r2, %r1		# 4443
	ld	%r3, (%r3)		# 4444
	cmpeqi	%r4, %r3, -1		# 4445
	jmpzi	%r4, else.16207		# 4446
	addi	%r63, %r63, -1		# 4447
	ld	%r60, (%r63)		# 4448
	jmpu	%r60		# 4449
else.16207:
	movi	%r4, min_caml_and_net		# 4450
	add	%r4, %r4, %r3		# 4451
	ld	%r3, (%r4)		# 4452
	movi	%r4, 0		# 4453
	ld	%r5, (%r63)		# 4454
	sto	%r1, 3(%r63)		# 4455
	mov	%r2, %r3		# 4456
	mov	%r1, %r4		# 4457
	mov	%r3, %r5		# 4458
	addi	%r63, %r63, 4		# 4459
	call	%r60, solve_each_element.2748		# 4460
	addi	%r63, %r63, -4		# 4461
	ld	%r1, 3(%r63)		# 4462
	addi	%r1, %r1, 1		# 4463
	ld	%r2, 1(%r63)		# 4464
	add	%r3, %r2, %r1		# 4465
	ld	%r3, (%r3)		# 4466
	cmpeqi	%r4, %r3, -1		# 4467
	jmpzi	%r4, else.16209		# 4468
	addi	%r63, %r63, -1		# 4469
	ld	%r60, (%r63)		# 4470
	jmpu	%r60		# 4471
else.16209:
	movi	%r4, min_caml_and_net		# 4472
	add	%r4, %r4, %r3		# 4473
	ld	%r3, (%r4)		# 4474
	movi	%r4, 0		# 4475
	ld	%r5, (%r63)		# 4476
	sto	%r1, 4(%r63)		# 4477
	mov	%r2, %r3		# 4478
	mov	%r1, %r4		# 4479
	mov	%r3, %r5		# 4480
	addi	%r63, %r63, 5		# 4481
	call	%r60, solve_each_element.2748		# 4482
	addi	%r63, %r63, -5		# 4483
	ld	%r1, 4(%r63)		# 4484
	addi	%r1, %r1, 1		# 4485
	ld	%r2, 1(%r63)		# 4486
	add	%r3, %r2, %r1		# 4487
	ld	%r3, (%r3)		# 4488
	cmpeqi	%r4, %r3, -1		# 4489
	jmpzi	%r4, else.16211		# 4490
	addi	%r63, %r63, -1		# 4491
	ld	%r60, (%r63)		# 4492
	jmpu	%r60		# 4493
else.16211:
	movi	%r4, min_caml_and_net		# 4494
	add	%r4, %r4, %r3		# 4495
	ld	%r3, (%r4)		# 4496
	movi	%r4, 0		# 4497
	ld	%r5, (%r63)		# 4498
	sto	%r1, 5(%r63)		# 4499
	mov	%r2, %r3		# 4500
	mov	%r1, %r4		# 4501
	mov	%r3, %r5		# 4502
	addi	%r63, %r63, 6		# 4503
	call	%r60, solve_each_element.2748		# 4504
	addi	%r63, %r63, -6		# 4505
	ld	%r1, 5(%r63)		# 4506
	addi	%r1, %r1, 1		# 4507
	ld	%r2, 1(%r63)		# 4508
	ld	%r3, (%r63)		# 4509
	addi	%r63, %r63, -1		# 4510
	ld	%r60, (%r63)		# 4511
	jmpui	solve_one_or_network.2752		# 4512
.globl	trace_or_matrix.2756
trace_or_matrix.2756:
	sto	%r60, (%r63)		# 4513
	addi	%r63, %r63, 1		# 4514
	add	%r4, %r2, %r1		# 4515
	ld	%r4, (%r4)		# 4516
	addi	%r5, %r4, 0		# 4517
	ld	%r5, (%r5)		# 4518
	cmpeqi	%r6, %r5, -1		# 4519
	jmpzi	%r6, else.16213		# 4520
	addi	%r63, %r63, -1		# 4521
	ld	%r60, (%r63)		# 4522
	jmpu	%r60		# 4523
else.16213:
	cmpeqi	%r6, %r5, 99		# 4524
	sto	%r3, (%r63)		# 4525
	sto	%r2, 1(%r63)		# 4526
	sto	%r1, 2(%r63)		# 4527
	jmpzi	%r6, else.16215		# 4528
	addi	%r5, %r4, 1		# 4529
	ld	%r5, (%r5)		# 4530
	cmpeqi	%r6, %r5, -1		# 4531
	jmpzi	%r6, else.16217		# 4532
	jmpui	if_cont.16218		# 4533
else.16217:
	movi	%r6, min_caml_and_net		# 4534
	add	%r6, %r6, %r5		# 4535
	ld	%r5, (%r6)		# 4536
	movi	%r6, 0		# 4537
	sto	%r4, 3(%r63)		# 4538
	mov	%r2, %r5		# 4539
	mov	%r1, %r6		# 4540
	addi	%r63, %r63, 4		# 4541
	call	%r60, solve_each_element.2748		# 4542
	addi	%r63, %r63, -4		# 4543
	ld	%r1, 3(%r63)		# 4544
	addi	%r2, %r1, 2		# 4545
	ld	%r2, (%r2)		# 4546
	cmpeqi	%r3, %r2, -1		# 4547
	jmpzi	%r3, else.16219		# 4548
	jmpui	if_cont.16220		# 4549
else.16219:
	movi	%r3, min_caml_and_net		# 4550
	add	%r3, %r3, %r2		# 4551
	ld	%r2, (%r3)		# 4552
	movi	%r3, 0		# 4553
	ld	%r4, (%r63)		# 4554
	mov	%r1, %r3		# 4555
	mov	%r3, %r4		# 4556
	addi	%r63, %r63, 4		# 4557
	call	%r60, solve_each_element.2748		# 4558
	addi	%r63, %r63, -4		# 4559
	ld	%r1, 3(%r63)		# 4560
	addi	%r2, %r1, 3		# 4561
	ld	%r2, (%r2)		# 4562
	cmpeqi	%r3, %r2, -1		# 4563
	jmpzi	%r3, else.16221		# 4564
	jmpui	if_cont.16222		# 4565
else.16221:
	movi	%r3, min_caml_and_net		# 4566
	add	%r3, %r3, %r2		# 4567
	ld	%r2, (%r3)		# 4568
	movi	%r3, 0		# 4569
	ld	%r4, (%r63)		# 4570
	mov	%r1, %r3		# 4571
	mov	%r3, %r4		# 4572
	addi	%r63, %r63, 4		# 4573
	call	%r60, solve_each_element.2748		# 4574
	addi	%r63, %r63, -4		# 4575
	movi	%r1, 4		# 4576
	ld	%r2, 3(%r63)		# 4577
	ld	%r3, (%r63)		# 4578
	addi	%r63, %r63, 4		# 4579
	call	%r60, solve_one_or_network.2752		# 4580
	addi	%r63, %r63, -4		# 4581
if_cont.16222:
if_cont.16220:
if_cont.16218:
	jmpui	if_cont.16216		# 4582
else.16215:
	movi	%r6, min_caml_startp		# 4583
	movi	%r7, min_caml_objects		# 4584
	add	%r7, %r7, %r5		# 4585
	ld	%r5, (%r7)		# 4586
	addi	%r7, %r6, 0		# 4587
	ld	%r7, (%r7)		# 4588
	ld	%r8, 5(%r5)		# 4589
	addi	%r8, %r8, 0		# 4590
	ld	%r8, (%r8)		# 4591
	fsub	%r7, %r7, %r8		# 4592
	addi	%r8, %r6, 1		# 4593
	ld	%r8, (%r8)		# 4594
	ld	%r9, 5(%r5)		# 4595
	addi	%r9, %r9, 1		# 4596
	ld	%r9, (%r9)		# 4597
	fsub	%r8, %r8, %r9		# 4598
	addi	%r6, %r6, 2		# 4599
	ld	%r6, (%r6)		# 4600
	ld	%r9, 5(%r5)		# 4601
	addi	%r9, %r9, 2		# 4602
	ld	%r9, (%r9)		# 4603
	fsub	%r6, %r6, %r9		# 4604
	ld	%r9, 1(%r5)		# 4605
	cmpeqi	%r10, %r9, 1		# 4606
	sto	%r4, 3(%r63)		# 4607
	jmpzi	%r10, else.16223		# 4608
	movi	%r9, 0		# 4609
	movi	%r10, 1		# 4610
	movi	%r11, 2		# 4611
	sto	%r7, 4(%r63)		# 4612
	sto	%r6, 5(%r63)		# 4613
	sto	%r8, 6(%r63)		# 4614
	sto	%r5, 7(%r63)		# 4615
	mov	%r4, %r10		# 4616
	mov	%r2, %r3		# 4617
	mov	%r1, %r5		# 4618
	mov	%r5, %r11		# 4619
	mov	%r3, %r9		# 4620
	sto	%r8, 8(%r63)		# 4621
	mov	%r8, %r6		# 4622
	mov	%r6, %r7		# 4623
	ld	%r7, 8(%r63)		# 4624
	addi	%r63, %r63, 8		# 4625
	call	%r60, solver_rect_surface.2610		# 4626
	addi	%r63, %r63, -8		# 4627
	cmpeqi	%r1, %r1, 0		# 4628
	jmpzi	%r1, else.16225		# 4629
	movi	%r3, 1		# 4630
	movi	%r4, 2		# 4631
	movi	%r5, 0		# 4632
	ld	%r6, 6(%r63)		# 4633
	ld	%r7, 5(%r63)		# 4634
	ld	%r8, 4(%r63)		# 4635
	ld	%r1, 7(%r63)		# 4636
	ld	%r2, (%r63)		# 4637
	addi	%r63, %r63, 8		# 4638
	call	%r60, solver_rect_surface.2610		# 4639
	addi	%r63, %r63, -8		# 4640
	cmpeqi	%r1, %r1, 0		# 4641
	jmpzi	%r1, else.16227		# 4642
	movi	%r3, 2		# 4643
	movi	%r4, 0		# 4644
	movi	%r5, 1		# 4645
	ld	%r6, 5(%r63)		# 4646
	ld	%r7, 4(%r63)		# 4647
	ld	%r8, 6(%r63)		# 4648
	ld	%r1, 7(%r63)		# 4649
	ld	%r2, (%r63)		# 4650
	addi	%r63, %r63, 8		# 4651
	call	%r60, solver_rect_surface.2610		# 4652
	addi	%r63, %r63, -8		# 4653
	cmpeqi	%r1, %r1, 0		# 4654
	jmpzi	%r1, else.16229		# 4655
	movi	%r1, 0		# 4656
	jmpui	if_cont.16230		# 4657
else.16229:
	movi	%r1, 3		# 4658
if_cont.16230:
	jmpui	if_cont.16228		# 4659
else.16227:
	movi	%r1, 2		# 4660
if_cont.16228:
	jmpui	if_cont.16226		# 4661
else.16225:
	movi	%r1, 1		# 4662
if_cont.16226:
	jmpui	if_cont.16224		# 4663
else.16223:
	cmpeqi	%r9, %r9, 2		# 4664
	jmpzi	%r9, else.16231		# 4665
	mov	%r4, %r8		# 4666
	mov	%r2, %r3		# 4667
	mov	%r1, %r5		# 4668
	mov	%r5, %r6		# 4669
	mov	%r3, %r7		# 4670
	addi	%r63, %r63, 8		# 4671
	call	%r60, solver_surface.2625		# 4672
	addi	%r63, %r63, -8		# 4673
	jmpui	if_cont.16232		# 4674
else.16231:
	mov	%r4, %r8		# 4675
	mov	%r2, %r3		# 4676
	mov	%r1, %r5		# 4677
	mov	%r5, %r6		# 4678
	mov	%r3, %r7		# 4679
	addi	%r63, %r63, 8		# 4680
	call	%r60, solver_second.2644		# 4681
	addi	%r63, %r63, -8		# 4682
if_cont.16232:
if_cont.16224:
	cmpeqi	%r1, %r1, 0		# 4683
	jmpzi	%r1, else.16233		# 4684
	jmpui	if_cont.16234		# 4685
else.16233:
	movi	%r1, min_caml_solver_dist		# 4686
	addi	%r1, %r1, 0		# 4687
	ld	%r1, (%r1)		# 4688
	movi	%r2, min_caml_tmin		# 4689
	addi	%r2, %r2, 0		# 4690
	ld	%r2, (%r2)		# 4691
	cmpfgt	%r2, %r2, %r1		# 4692
	jmpi	%r2, else.16235		# 4693
	jmpui	if_cont.16236		# 4694
else.16235:
	ld	%r1, 3(%r63)		# 4695
	addi	%r2, %r1, 1		# 4696
	ld	%r2, (%r2)		# 4697
	cmpeqi	%r3, %r2, -1		# 4698
	jmpzi	%r3, else.16237		# 4699
	jmpui	if_cont.16238		# 4700
else.16237:
	movi	%r3, min_caml_and_net		# 4701
	add	%r3, %r3, %r2		# 4702
	ld	%r2, (%r3)		# 4703
	movi	%r3, 0		# 4704
	ld	%r4, (%r63)		# 4705
	mov	%r1, %r3		# 4706
	mov	%r3, %r4		# 4707
	addi	%r63, %r63, 8		# 4708
	call	%r60, solve_each_element.2748		# 4709
	addi	%r63, %r63, -8		# 4710
	ld	%r1, 3(%r63)		# 4711
	addi	%r2, %r1, 2		# 4712
	ld	%r2, (%r2)		# 4713
	cmpeqi	%r3, %r2, -1		# 4714
	jmpzi	%r3, else.16239		# 4715
	jmpui	if_cont.16240		# 4716
else.16239:
	movi	%r3, min_caml_and_net		# 4717
	add	%r3, %r3, %r2		# 4718
	ld	%r2, (%r3)		# 4719
	movi	%r3, 0		# 4720
	ld	%r4, (%r63)		# 4721
	mov	%r1, %r3		# 4722
	mov	%r3, %r4		# 4723
	addi	%r63, %r63, 8		# 4724
	call	%r60, solve_each_element.2748		# 4725
	addi	%r63, %r63, -8		# 4726
	ld	%r1, 3(%r63)		# 4727
	addi	%r2, %r1, 3		# 4728
	ld	%r2, (%r2)		# 4729
	cmpeqi	%r3, %r2, -1		# 4730
	jmpzi	%r3, else.16241		# 4731
	jmpui	if_cont.16242		# 4732
else.16241:
	movi	%r3, min_caml_and_net		# 4733
	add	%r3, %r3, %r2		# 4734
	ld	%r2, (%r3)		# 4735
	movi	%r3, 0		# 4736
	ld	%r4, (%r63)		# 4737
	mov	%r1, %r3		# 4738
	mov	%r3, %r4		# 4739
	addi	%r63, %r63, 8		# 4740
	call	%r60, solve_each_element.2748		# 4741
	addi	%r63, %r63, -8		# 4742
	movi	%r1, 4		# 4743
	ld	%r2, 3(%r63)		# 4744
	ld	%r3, (%r63)		# 4745
	addi	%r63, %r63, 8		# 4746
	call	%r60, solve_one_or_network.2752		# 4747
	addi	%r63, %r63, -8		# 4748
if_cont.16242:
if_cont.16240:
if_cont.16238:
if_cont.16236:
if_cont.16234:
if_cont.16216:
	ld	%r1, 2(%r63)		# 4749
	addi	%r1, %r1, 1		# 4750
	ld	%r2, 1(%r63)		# 4751
	ld	%r3, (%r63)		# 4752
	addi	%r63, %r63, -1		# 4753
	ld	%r60, (%r63)		# 4754
	jmpui	trace_or_matrix.2756		# 4755
.globl	solve_each_element_fast.2762
solve_each_element_fast.2762:
	sto	%r60, (%r63)		# 4756
	addi	%r63, %r63, 1		# 4757
	ld	%r4, (%r3)		# 4758
	add	%r5, %r2, %r1		# 4759
	ld	%r5, (%r5)		# 4760
	cmpeqi	%r6, %r5, -1		# 4761
	jmpzi	%r6, else.16243		# 4762
	addi	%r63, %r63, -1		# 4763
	ld	%r60, (%r63)		# 4764
	jmpu	%r60		# 4765
else.16243:
	movi	%r6, min_caml_objects		# 4766
	add	%r6, %r6, %r5		# 4767
	ld	%r6, (%r6)		# 4768
	ld	%r7, 10(%r6)		# 4769
	addi	%r8, %r7, 0		# 4770
	ld	%r8, (%r8)		# 4771
	addi	%r9, %r7, 1		# 4772
	ld	%r9, (%r9)		# 4773
	addi	%r10, %r7, 2		# 4774
	ld	%r10, (%r10)		# 4775
	ld	%r11, 1(%r3)		# 4776
	add	%r11, %r11, %r5		# 4777
	ld	%r11, (%r11)		# 4778
	ld	%r12, 1(%r6)		# 4779
	cmpeqi	%r13, %r12, 1		# 4780
	sto	%r4, (%r63)		# 4781
	sto	%r3, 1(%r63)		# 4782
	sto	%r2, 2(%r63)		# 4783
	sto	%r1, 3(%r63)		# 4784
	sto	%r5, 4(%r63)		# 4785
	jmpzi	%r13, else.16245		# 4786
	ld	%r7, (%r3)		# 4787
	mov	%r5, %r9		# 4788
	mov	%r4, %r8		# 4789
	mov	%r3, %r11		# 4790
	mov	%r2, %r7		# 4791
	mov	%r1, %r6		# 4792
	mov	%r6, %r10		# 4793
	addi	%r63, %r63, 5		# 4794
	call	%r60, solver_rect_fast.2654		# 4795
	addi	%r63, %r63, -5		# 4796
	jmpui	if_cont.16246		# 4797
else.16245:
	cmpeqi	%r12, %r12, 2		# 4798
	jmpzi	%r12, else.16247		# 4799
	addi	%r6, %r11, 0		# 4800
	ld	%r6, (%r6)		# 4801
	movi	%r8, 0		# 4802
	cmpfgt	%r8, %r8, %r6		# 4803
	jmpi	%r8, else.16249		# 4804
	movi	%r1, 0		# 4805
	jmpui	if_cont.16250		# 4806
else.16249:
	movi	%r6, min_caml_solver_dist		# 4807
	addi	%r11, %r11, 0		# 4808
	ld	%r8, (%r11)		# 4809
	addi	%r7, %r7, 3		# 4810
	ld	%r7, (%r7)		# 4811
	fmul	%r8, %r8, %r7		# 4812
	addi	%r6, %r6, 0		# 4813
	sto	%r8, (%r6)		# 4814
	movi	%r1, 1		# 4815
if_cont.16250:
	jmpui	if_cont.16248		# 4816
else.16247:
	mov	%r5, %r9		# 4817
	mov	%r4, %r8		# 4818
	mov	%r3, %r7		# 4819
	mov	%r2, %r11		# 4820
	mov	%r1, %r6		# 4821
	mov	%r6, %r10		# 4822
	addi	%r63, %r63, 5		# 4823
	call	%r60, solver_second_fast2.2684		# 4824
	addi	%r63, %r63, -5		# 4825
if_cont.16248:
if_cont.16246:
	cmpeqi	%r2, %r1, 0		# 4826
	jmpzi	%r2, else.16251		# 4827
	movi	%r1, min_caml_objects		# 4828
	ld	%r2, 4(%r63)		# 4829
	add	%r1, %r1, %r2		# 4830
	ld	%r1, (%r1)		# 4831
	ld	%r1, 6(%r1)		# 4832
	cmpeqi	%r1, %r1, 0		# 4833
	jmpzi	%r1, else.16252		# 4834
	addi	%r63, %r63, -1		# 4835
	ld	%r60, (%r63)		# 4836
	jmpu	%r60		# 4837
else.16252:
	ld	%r1, 3(%r63)		# 4838
	addi	%r1, %r1, 1		# 4839
	ld	%r2, 2(%r63)		# 4840
	ld	%r3, 1(%r63)		# 4841
	addi	%r63, %r63, -1		# 4842
	ld	%r60, (%r63)		# 4843
	jmpui	solve_each_element_fast.2762		# 4844
else.16251:
	movi	%r2, min_caml_solver_dist		# 4845
	addi	%r2, %r2, 0		# 4846
	ld	%r2, (%r2)		# 4847
	movi	%r3, 0		# 4848
	cmpfgt	%r3, %r2, %r3		# 4849
	jmpi	%r3, else.16254		# 4850
	jmpui	if_cont.16255		# 4851
else.16254:
	movi	%r3, min_caml_tmin		# 4852
	addi	%r3, %r3, 0		# 4853
	ld	%r3, (%r3)		# 4854
	cmpfgt	%r3, %r3, %r2		# 4855
	jmpi	%r3, else.16256		# 4856
	jmpui	if_cont.16257		# 4857
else.16256:
	movhiz	%r3, 246333		# 4858
	addi	%r3, %r3, 1802		# 4859
	fadd	%r2, %r2, %r3		# 4860
	ld	%r3, (%r63)		# 4861
	addi	%r4, %r3, 0		# 4862
	ld	%r4, (%r4)		# 4863
	fmul	%r4, %r4, %r2		# 4864
	movi	%r5, min_caml_startp_fast		# 4865
	addi	%r5, %r5, 0		# 4866
	ld	%r5, (%r5)		# 4867
	fadd	%r4, %r4, %r5		# 4868
	addi	%r5, %r3, 1		# 4869
	ld	%r5, (%r5)		# 4870
	fmul	%r5, %r5, %r2		# 4871
	movi	%r6, min_caml_startp_fast		# 4872
	addi	%r6, %r6, 1		# 4873
	ld	%r6, (%r6)		# 4874
	fadd	%r5, %r5, %r6		# 4875
	addi	%r3, %r3, 2		# 4876
	ld	%r3, (%r3)		# 4877
	fmul	%r3, %r3, %r2		# 4878
	movi	%r6, min_caml_startp_fast		# 4879
	addi	%r6, %r6, 2		# 4880
	ld	%r6, (%r6)		# 4881
	fadd	%r3, %r3, %r6		# 4882
	ld	%r6, 2(%r63)		# 4883
	addi	%r7, %r6, 0		# 4884
	ld	%r7, (%r7)		# 4885
	cmpeqi	%r8, %r7, -1		# 4886
	sto	%r1, 5(%r63)		# 4887
	sto	%r3, 6(%r63)		# 4888
	sto	%r5, 7(%r63)		# 4889
	sto	%r4, 8(%r63)		# 4890
	sto	%r2, 9(%r63)		# 4891
	jmpzi	%r8, else.16258		# 4892
	movi	%r1, 1		# 4893
	jmpui	if_cont.16259		# 4894
else.16258:
	movi	%r8, min_caml_objects		# 4895
	add	%r8, %r8, %r7		# 4896
	ld	%r7, (%r8)		# 4897
	mov	%r2, %r4		# 4898
	mov	%r1, %r7		# 4899
	mov	%r4, %r3		# 4900
	mov	%r3, %r5		# 4901
	addi	%r63, %r63, 10		# 4902
	call	%r60, is_outside.2728		# 4903
	addi	%r63, %r63, -10		# 4904
	cmpeqi	%r1, %r1, 0		# 4905
	jmpzi	%r1, else.16260		# 4906
	movi	%r1, 1		# 4907
	ld	%r3, 8(%r63)		# 4908
	ld	%r4, 7(%r63)		# 4909
	ld	%r5, 6(%r63)		# 4910
	ld	%r2, 2(%r63)		# 4911
	addi	%r63, %r63, 10		# 4912
	call	%r60, check_all_inside.2733		# 4913
	addi	%r63, %r63, -10		# 4914
	jmpui	if_cont.16261		# 4915
else.16260:
	movi	%r1, 0		# 4916
if_cont.16261:
if_cont.16259:
	cmpeqi	%r1, %r1, 0		# 4917
	jmpzi	%r1, else.16262		# 4918
	jmpui	if_cont.16263		# 4919
else.16262:
	movi	%r1, min_caml_tmin		# 4920
	addi	%r1, %r1, 0		# 4921
	ld	%r2, 9(%r63)		# 4922
	sto	%r2, (%r1)		# 4923
	movi	%r1, min_caml_intersection_point		# 4924
	addi	%r2, %r1, 0		# 4925
	ld	%r3, 8(%r63)		# 4926
	sto	%r3, (%r2)		# 4927
	addi	%r2, %r1, 1		# 4928
	ld	%r3, 7(%r63)		# 4929
	sto	%r3, (%r2)		# 4930
	addi	%r1, %r1, 2		# 4931
	ld	%r2, 6(%r63)		# 4932
	sto	%r2, (%r1)		# 4933
	movi	%r1, min_caml_intersected_object_id		# 4934
	addi	%r1, %r1, 0		# 4935
	ld	%r2, 4(%r63)		# 4936
	sto	%r2, (%r1)		# 4937
	movi	%r1, min_caml_intsec_rectside		# 4938
	addi	%r1, %r1, 0		# 4939
	ld	%r2, 5(%r63)		# 4940
	sto	%r2, (%r1)		# 4941
if_cont.16263:
if_cont.16257:
if_cont.16255:
	ld	%r1, 3(%r63)		# 4942
	addi	%r1, %r1, 1		# 4943
	ld	%r2, 2(%r63)		# 4944
	ld	%r3, 1(%r63)		# 4945
	addi	%r63, %r63, -1		# 4946
	ld	%r60, (%r63)		# 4947
	jmpui	solve_each_element_fast.2762		# 4948
.globl	solve_one_or_network_fast.2766
solve_one_or_network_fast.2766:
	sto	%r60, (%r63)		# 4949
	addi	%r63, %r63, 1		# 4950
	add	%r4, %r2, %r1		# 4951
	ld	%r4, (%r4)		# 4952
	cmpeqi	%r5, %r4, -1		# 4953
	jmpzi	%r5, else.16264		# 4954
	addi	%r63, %r63, -1		# 4955
	ld	%r60, (%r63)		# 4956
	jmpu	%r60		# 4957
else.16264:
	movi	%r5, min_caml_and_net		# 4958
	add	%r5, %r5, %r4		# 4959
	ld	%r4, (%r5)		# 4960
	movi	%r5, 0		# 4961
	sto	%r3, (%r63)		# 4962
	sto	%r2, 1(%r63)		# 4963
	sto	%r1, 2(%r63)		# 4964
	mov	%r2, %r4		# 4965
	mov	%r1, %r5		# 4966
	addi	%r63, %r63, 3		# 4967
	call	%r60, solve_each_element_fast.2762		# 4968
	addi	%r63, %r63, -3		# 4969
	ld	%r1, 2(%r63)		# 4970
	addi	%r1, %r1, 1		# 4971
	ld	%r2, 1(%r63)		# 4972
	add	%r3, %r2, %r1		# 4973
	ld	%r3, (%r3)		# 4974
	cmpeqi	%r4, %r3, -1		# 4975
	jmpzi	%r4, else.16266		# 4976
	addi	%r63, %r63, -1		# 4977
	ld	%r60, (%r63)		# 4978
	jmpu	%r60		# 4979
else.16266:
	movi	%r4, min_caml_and_net		# 4980
	add	%r4, %r4, %r3		# 4981
	ld	%r3, (%r4)		# 4982
	movi	%r4, 0		# 4983
	ld	%r5, (%r63)		# 4984
	sto	%r1, 3(%r63)		# 4985
	mov	%r2, %r3		# 4986
	mov	%r1, %r4		# 4987
	mov	%r3, %r5		# 4988
	addi	%r63, %r63, 4		# 4989
	call	%r60, solve_each_element_fast.2762		# 4990
	addi	%r63, %r63, -4		# 4991
	ld	%r1, 3(%r63)		# 4992
	addi	%r1, %r1, 1		# 4993
	ld	%r2, 1(%r63)		# 4994
	add	%r3, %r2, %r1		# 4995
	ld	%r3, (%r3)		# 4996
	cmpeqi	%r4, %r3, -1		# 4997
	jmpzi	%r4, else.16268		# 4998
	addi	%r63, %r63, -1		# 4999
	ld	%r60, (%r63)		# 5000
	jmpu	%r60		# 5001
else.16268:
	movi	%r4, min_caml_and_net		# 5002
	add	%r4, %r4, %r3		# 5003
	ld	%r3, (%r4)		# 5004
	movi	%r4, 0		# 5005
	ld	%r5, (%r63)		# 5006
	sto	%r1, 4(%r63)		# 5007
	mov	%r2, %r3		# 5008
	mov	%r1, %r4		# 5009
	mov	%r3, %r5		# 5010
	addi	%r63, %r63, 5		# 5011
	call	%r60, solve_each_element_fast.2762		# 5012
	addi	%r63, %r63, -5		# 5013
	ld	%r1, 4(%r63)		# 5014
	addi	%r1, %r1, 1		# 5015
	ld	%r2, 1(%r63)		# 5016
	add	%r3, %r2, %r1		# 5017
	ld	%r3, (%r3)		# 5018
	cmpeqi	%r4, %r3, -1		# 5019
	jmpzi	%r4, else.16270		# 5020
	addi	%r63, %r63, -1		# 5021
	ld	%r60, (%r63)		# 5022
	jmpu	%r60		# 5023
else.16270:
	movi	%r4, min_caml_and_net		# 5024
	add	%r4, %r4, %r3		# 5025
	ld	%r3, (%r4)		# 5026
	movi	%r4, 0		# 5027
	ld	%r5, (%r63)		# 5028
	sto	%r1, 5(%r63)		# 5029
	mov	%r2, %r3		# 5030
	mov	%r1, %r4		# 5031
	mov	%r3, %r5		# 5032
	addi	%r63, %r63, 6		# 5033
	call	%r60, solve_each_element_fast.2762		# 5034
	addi	%r63, %r63, -6		# 5035
	ld	%r1, 5(%r63)		# 5036
	addi	%r1, %r1, 1		# 5037
	ld	%r2, 1(%r63)		# 5038
	ld	%r3, (%r63)		# 5039
	addi	%r63, %r63, -1		# 5040
	ld	%r60, (%r63)		# 5041
	jmpui	solve_one_or_network_fast.2766		# 5042
.globl	trace_or_matrix_fast.2770
trace_or_matrix_fast.2770:
	sto	%r60, (%r63)		# 5043
	addi	%r63, %r63, 1		# 5044
	add	%r4, %r2, %r1		# 5045
	ld	%r4, (%r4)		# 5046
	addi	%r5, %r4, 0		# 5047
	ld	%r5, (%r5)		# 5048
	cmpeqi	%r6, %r5, -1		# 5049
	jmpzi	%r6, else.16272		# 5050
	addi	%r63, %r63, -1		# 5051
	ld	%r60, (%r63)		# 5052
	jmpu	%r60		# 5053
else.16272:
	cmpeqi	%r6, %r5, 99		# 5054
	sto	%r3, (%r63)		# 5055
	sto	%r2, 1(%r63)		# 5056
	sto	%r1, 2(%r63)		# 5057
	jmpzi	%r6, else.16274		# 5058
	addi	%r5, %r4, 1		# 5059
	ld	%r5, (%r5)		# 5060
	cmpeqi	%r6, %r5, -1		# 5061
	jmpzi	%r6, else.16276		# 5062
	jmpui	if_cont.16277		# 5063
else.16276:
	movi	%r6, min_caml_and_net		# 5064
	add	%r6, %r6, %r5		# 5065
	ld	%r5, (%r6)		# 5066
	movi	%r6, 0		# 5067
	sto	%r4, 3(%r63)		# 5068
	mov	%r2, %r5		# 5069
	mov	%r1, %r6		# 5070
	addi	%r63, %r63, 4		# 5071
	call	%r60, solve_each_element_fast.2762		# 5072
	addi	%r63, %r63, -4		# 5073
	ld	%r1, 3(%r63)		# 5074
	addi	%r2, %r1, 2		# 5075
	ld	%r2, (%r2)		# 5076
	cmpeqi	%r3, %r2, -1		# 5077
	jmpzi	%r3, else.16278		# 5078
	jmpui	if_cont.16279		# 5079
else.16278:
	movi	%r3, min_caml_and_net		# 5080
	add	%r3, %r3, %r2		# 5081
	ld	%r2, (%r3)		# 5082
	movi	%r3, 0		# 5083
	ld	%r4, (%r63)		# 5084
	mov	%r1, %r3		# 5085
	mov	%r3, %r4		# 5086
	addi	%r63, %r63, 4		# 5087
	call	%r60, solve_each_element_fast.2762		# 5088
	addi	%r63, %r63, -4		# 5089
	ld	%r1, 3(%r63)		# 5090
	addi	%r2, %r1, 3		# 5091
	ld	%r2, (%r2)		# 5092
	cmpeqi	%r3, %r2, -1		# 5093
	jmpzi	%r3, else.16280		# 5094
	jmpui	if_cont.16281		# 5095
else.16280:
	movi	%r3, min_caml_and_net		# 5096
	add	%r3, %r3, %r2		# 5097
	ld	%r2, (%r3)		# 5098
	movi	%r3, 0		# 5099
	ld	%r4, (%r63)		# 5100
	mov	%r1, %r3		# 5101
	mov	%r3, %r4		# 5102
	addi	%r63, %r63, 4		# 5103
	call	%r60, solve_each_element_fast.2762		# 5104
	addi	%r63, %r63, -4		# 5105
	movi	%r1, 4		# 5106
	ld	%r2, 3(%r63)		# 5107
	ld	%r3, (%r63)		# 5108
	addi	%r63, %r63, 4		# 5109
	call	%r60, solve_one_or_network_fast.2766		# 5110
	addi	%r63, %r63, -4		# 5111
if_cont.16281:
if_cont.16279:
if_cont.16277:
	jmpui	if_cont.16275		# 5112
else.16274:
	movi	%r6, min_caml_objects		# 5113
	add	%r6, %r6, %r5		# 5114
	ld	%r6, (%r6)		# 5115
	ld	%r7, 10(%r6)		# 5116
	addi	%r8, %r7, 0		# 5117
	ld	%r8, (%r8)		# 5118
	addi	%r9, %r7, 1		# 5119
	ld	%r9, (%r9)		# 5120
	addi	%r10, %r7, 2		# 5121
	ld	%r10, (%r10)		# 5122
	ld	%r11, 1(%r3)		# 5123
	add	%r11, %r11, %r5		# 5124
	ld	%r5, (%r11)		# 5125
	ld	%r11, 1(%r6)		# 5126
	cmpeqi	%r12, %r11, 1		# 5127
	sto	%r4, 3(%r63)		# 5128
	jmpzi	%r12, else.16282		# 5129
	ld	%r7, (%r3)		# 5130
	mov	%r4, %r8		# 5131
	mov	%r3, %r5		# 5132
	mov	%r2, %r7		# 5133
	mov	%r1, %r6		# 5134
	mov	%r6, %r10		# 5135
	mov	%r5, %r9		# 5136
	addi	%r63, %r63, 4		# 5137
	call	%r60, solver_rect_fast.2654		# 5138
	addi	%r63, %r63, -4		# 5139
	jmpui	if_cont.16283		# 5140
else.16282:
	cmpeqi	%r11, %r11, 2		# 5141
	jmpzi	%r11, else.16284		# 5142
	addi	%r6, %r5, 0		# 5143
	ld	%r6, (%r6)		# 5144
	movi	%r8, 0		# 5145
	cmpfgt	%r8, %r8, %r6		# 5146
	jmpi	%r8, else.16286		# 5147
	movi	%r1, 0		# 5148
	jmpui	if_cont.16287		# 5149
else.16286:
	movi	%r6, min_caml_solver_dist		# 5150
	addi	%r5, %r5, 0		# 5151
	ld	%r5, (%r5)		# 5152
	addi	%r7, %r7, 3		# 5153
	ld	%r7, (%r7)		# 5154
	fmul	%r5, %r5, %r7		# 5155
	addi	%r6, %r6, 0		# 5156
	sto	%r5, (%r6)		# 5157
	movi	%r1, 1		# 5158
if_cont.16287:
	jmpui	if_cont.16285		# 5159
else.16284:
	mov	%r4, %r8		# 5160
	mov	%r3, %r7		# 5161
	mov	%r2, %r5		# 5162
	mov	%r1, %r6		# 5163
	mov	%r6, %r10		# 5164
	mov	%r5, %r9		# 5165
	addi	%r63, %r63, 4		# 5166
	call	%r60, solver_second_fast2.2684		# 5167
	addi	%r63, %r63, -4		# 5168
if_cont.16285:
if_cont.16283:
	cmpeqi	%r1, %r1, 0		# 5169
	jmpzi	%r1, else.16288		# 5170
	jmpui	if_cont.16289		# 5171
else.16288:
	movi	%r1, min_caml_solver_dist		# 5172
	addi	%r1, %r1, 0		# 5173
	ld	%r1, (%r1)		# 5174
	movi	%r2, min_caml_tmin		# 5175
	addi	%r2, %r2, 0		# 5176
	ld	%r2, (%r2)		# 5177
	cmpfgt	%r2, %r2, %r1		# 5178
	jmpi	%r2, else.16290		# 5179
	jmpui	if_cont.16291		# 5180
else.16290:
	ld	%r1, 3(%r63)		# 5181
	addi	%r2, %r1, 1		# 5182
	ld	%r2, (%r2)		# 5183
	cmpeqi	%r3, %r2, -1		# 5184
	jmpzi	%r3, else.16292		# 5185
	jmpui	if_cont.16293		# 5186
else.16292:
	movi	%r3, min_caml_and_net		# 5187
	add	%r3, %r3, %r2		# 5188
	ld	%r2, (%r3)		# 5189
	movi	%r3, 0		# 5190
	ld	%r4, (%r63)		# 5191
	mov	%r1, %r3		# 5192
	mov	%r3, %r4		# 5193
	addi	%r63, %r63, 4		# 5194
	call	%r60, solve_each_element_fast.2762		# 5195
	addi	%r63, %r63, -4		# 5196
	ld	%r1, 3(%r63)		# 5197
	addi	%r2, %r1, 2		# 5198
	ld	%r2, (%r2)		# 5199
	cmpeqi	%r3, %r2, -1		# 5200
	jmpzi	%r3, else.16294		# 5201
	jmpui	if_cont.16295		# 5202
else.16294:
	movi	%r3, min_caml_and_net		# 5203
	add	%r3, %r3, %r2		# 5204
	ld	%r2, (%r3)		# 5205
	movi	%r3, 0		# 5206
	ld	%r4, (%r63)		# 5207
	mov	%r1, %r3		# 5208
	mov	%r3, %r4		# 5209
	addi	%r63, %r63, 4		# 5210
	call	%r60, solve_each_element_fast.2762		# 5211
	addi	%r63, %r63, -4		# 5212
	ld	%r1, 3(%r63)		# 5213
	addi	%r2, %r1, 3		# 5214
	ld	%r2, (%r2)		# 5215
	cmpeqi	%r3, %r2, -1		# 5216
	jmpzi	%r3, else.16296		# 5217
	jmpui	if_cont.16297		# 5218
else.16296:
	movi	%r3, min_caml_and_net		# 5219
	add	%r3, %r3, %r2		# 5220
	ld	%r2, (%r3)		# 5221
	movi	%r3, 0		# 5222
	ld	%r4, (%r63)		# 5223
	mov	%r1, %r3		# 5224
	mov	%r3, %r4		# 5225
	addi	%r63, %r63, 4		# 5226
	call	%r60, solve_each_element_fast.2762		# 5227
	addi	%r63, %r63, -4		# 5228
	movi	%r1, 4		# 5229
	ld	%r2, 3(%r63)		# 5230
	ld	%r3, (%r63)		# 5231
	addi	%r63, %r63, 4		# 5232
	call	%r60, solve_one_or_network_fast.2766		# 5233
	addi	%r63, %r63, -4		# 5234
if_cont.16297:
if_cont.16295:
if_cont.16293:
if_cont.16291:
if_cont.16289:
if_cont.16275:
	ld	%r1, 2(%r63)		# 5235
	addi	%r1, %r1, 1		# 5236
	ld	%r2, 1(%r63)		# 5237
	ld	%r3, (%r63)		# 5238
	addi	%r63, %r63, -1		# 5239
	ld	%r60, (%r63)		# 5240
	jmpui	trace_or_matrix_fast.2770		# 5241
.globl	get_nvector_second.2780
get_nvector_second.2780:
	sto	%r60, (%r63)		# 5242
	addi	%r63, %r63, 1		# 5243
	movi	%r2, min_caml_intersection_point		# 5244
	addi	%r2, %r2, 0		# 5245
	ld	%r2, (%r2)		# 5246
	ld	%r3, 5(%r1)		# 5247
	addi	%r3, %r3, 0		# 5248
	ld	%r3, (%r3)		# 5249
	fsub	%r2, %r2, %r3		# 5250
	movi	%r3, min_caml_intersection_point		# 5251
	addi	%r3, %r3, 1		# 5252
	ld	%r3, (%r3)		# 5253
	ld	%r4, 5(%r1)		# 5254
	addi	%r4, %r4, 1		# 5255
	ld	%r4, (%r4)		# 5256
	fsub	%r3, %r3, %r4		# 5257
	movi	%r4, min_caml_intersection_point		# 5258
	addi	%r4, %r4, 2		# 5259
	ld	%r4, (%r4)		# 5260
	ld	%r5, 5(%r1)		# 5261
	addi	%r5, %r5, 2		# 5262
	ld	%r5, (%r5)		# 5263
	fsub	%r4, %r4, %r5		# 5264
	ld	%r5, 4(%r1)		# 5265
	addi	%r5, %r5, 0		# 5266
	ld	%r5, (%r5)		# 5267
	fmul	%r5, %r2, %r5		# 5268
	ld	%r6, 4(%r1)		# 5269
	addi	%r6, %r6, 1		# 5270
	ld	%r6, (%r6)		# 5271
	fmul	%r6, %r3, %r6		# 5272
	ld	%r7, 4(%r1)		# 5273
	addi	%r7, %r7, 2		# 5274
	ld	%r7, (%r7)		# 5275
	fmul	%r7, %r4, %r7		# 5276
	ld	%r8, 3(%r1)		# 5277
	cmpeqi	%r8, %r8, 0		# 5278
	jmpzi	%r8, else.16298		# 5279
	movi	%r2, min_caml_nvector		# 5280
	addi	%r2, %r2, 0		# 5281
	sto	%r5, (%r2)		# 5282
	movi	%r2, min_caml_nvector		# 5283
	addi	%r2, %r2, 1		# 5284
	sto	%r6, (%r2)		# 5285
	movi	%r2, min_caml_nvector		# 5286
	addi	%r2, %r2, 2		# 5287
	sto	%r7, (%r2)		# 5288
	jmpui	if_cont.16299		# 5289
else.16298:
	movi	%r8, min_caml_nvector		# 5290
	ld	%r9, 9(%r1)		# 5291
	addi	%r9, %r9, 2		# 5292
	ld	%r9, (%r9)		# 5293
	fmul	%r9, %r3, %r9		# 5294
	ld	%r10, 9(%r1)		# 5295
	addi	%r10, %r10, 1		# 5296
	ld	%r10, (%r10)		# 5297
	fmul	%r10, %r4, %r10		# 5298
	fadd	%r9, %r9, %r10		# 5299
	movhiz	%r10, 258048		# 5300
	fmul	%r9, %r9, %r10		# 5301
	fadd	%r5, %r5, %r9		# 5302
	addi	%r8, %r8, 0		# 5303
	sto	%r5, (%r8)		# 5304
	movi	%r5, min_caml_nvector		# 5305
	ld	%r8, 9(%r1)		# 5306
	addi	%r8, %r8, 2		# 5307
	ld	%r8, (%r8)		# 5308
	fmul	%r8, %r2, %r8		# 5309
	ld	%r9, 9(%r1)		# 5310
	addi	%r9, %r9, 0		# 5311
	ld	%r9, (%r9)		# 5312
	fmul	%r4, %r4, %r9		# 5313
	fadd	%r8, %r8, %r4		# 5314
	movhiz	%r4, 258048		# 5315
	fmul	%r8, %r8, %r4		# 5316
	fadd	%r6, %r6, %r8		# 5317
	addi	%r5, %r5, 1		# 5318
	sto	%r6, (%r5)		# 5319
	movi	%r4, min_caml_nvector		# 5320
	ld	%r5, 9(%r1)		# 5321
	addi	%r5, %r5, 1		# 5322
	ld	%r5, (%r5)		# 5323
	fmul	%r2, %r2, %r5		# 5324
	ld	%r5, 9(%r1)		# 5325
	addi	%r5, %r5, 0		# 5326
	ld	%r5, (%r5)		# 5327
	fmul	%r3, %r3, %r5		# 5328
	fadd	%r2, %r2, %r3		# 5329
	movhiz	%r3, 258048		# 5330
	fmul	%r2, %r2, %r3		# 5331
	fadd	%r7, %r7, %r2		# 5332
	addi	%r4, %r4, 2		# 5333
	sto	%r7, (%r4)		# 5334
if_cont.16299:
	movi	%r2, min_caml_nvector		# 5335
	ld	%r1, 6(%r1)		# 5336
	sto	%r2, (%r63)		# 5337
	mov	%r2, %r1		# 5338
	ld	%r1, (%r63)		# 5339
	addi	%r63, %r63, -1		# 5340
	ld	%r60, (%r63)		# 5341
	jmpui	vecunit_sgn.2488		# 5342
.globl	utexture.2785
utexture.2785:
	sto	%r60, (%r63)		# 5343
	addi	%r63, %r63, 1		# 5344
	ld	%r3, (%r1)		# 5345
	movi	%r4, min_caml_texture_color		# 5346
	ld	%r5, 8(%r1)		# 5347
	addi	%r5, %r5, 0		# 5348
	ld	%r5, (%r5)		# 5349
	addi	%r4, %r4, 0		# 5350
	sto	%r5, (%r4)		# 5351
	movi	%r4, min_caml_texture_color		# 5352
	ld	%r5, 8(%r1)		# 5353
	addi	%r5, %r5, 1		# 5354
	ld	%r5, (%r5)		# 5355
	addi	%r4, %r4, 1		# 5356
	sto	%r5, (%r4)		# 5357
	movi	%r4, min_caml_texture_color		# 5358
	ld	%r5, 8(%r1)		# 5359
	addi	%r5, %r5, 2		# 5360
	ld	%r5, (%r5)		# 5361
	addi	%r4, %r4, 2		# 5362
	sto	%r5, (%r4)		# 5363
	cmpeqi	%r4, %r3, 1		# 5364
	jmpzi	%r4, else.16300		# 5365
	addi	%r3, %r2, 0		# 5366
	ld	%r3, (%r3)		# 5367
	ld	%r4, 5(%r1)		# 5368
	addi	%r4, %r4, 0		# 5369
	ld	%r4, (%r4)		# 5370
	fsub	%r3, %r3, %r4		# 5371
	movhiz	%r4, 251084		# 5372
	addi	%r4, %r4, 3277		# 5373
	fmul	%r4, %r3, %r4		# 5374
	sto	%r1, (%r63)		# 5375
	sto	%r2, 1(%r63)		# 5376
	sto	%r3, 2(%r63)		# 5377
	mov	%r1, %r4		# 5378
	addi	%r63, %r63, 3		# 5379
	call	%r60, min_caml_floor		# 5380
	addi	%r63, %r63, -3		# 5381
	movhiz	%r2, 268800		# 5382
	fmul	%r1, %r1, %r2		# 5383
	ld	%r2, 2(%r63)		# 5384
	fsub	%r2, %r2, %r1		# 5385
	movhiz	%r1, 266752		# 5386
	ld	%r3, 1(%r63)		# 5387
	addi	%r3, %r3, 2		# 5388
	ld	%r3, (%r3)		# 5389
	ld	%r4, (%r63)		# 5390
	ld	%r4, 5(%r4)		# 5391
	addi	%r4, %r4, 2		# 5392
	ld	%r4, (%r4)		# 5393
	fsub	%r3, %r3, %r4		# 5394
	movhiz	%r4, 251084		# 5395
	addi	%r4, %r4, 3277		# 5396
	fmul	%r4, %r3, %r4		# 5397
	sto	%r2, 3(%r63)		# 5398
	sto	%r1, 4(%r63)		# 5399
	sto	%r3, 5(%r63)		# 5400
	mov	%r1, %r4		# 5401
	addi	%r63, %r63, 6		# 5402
	call	%r60, min_caml_floor		# 5403
	addi	%r63, %r63, -6		# 5404
	movhiz	%r2, 268800		# 5405
	fmul	%r1, %r1, %r2		# 5406
	ld	%r2, 5(%r63)		# 5407
	fsub	%r2, %r2, %r1		# 5408
	movhiz	%r1, 266752		# 5409
	movi	%r3, min_caml_texture_color		# 5410
	ld	%r4, 3(%r63)		# 5411
	ld	%r5, 4(%r63)		# 5412
	cmpfgt	%r5, %r5, %r4		# 5413
	jmpi	%r5, else.16301		# 5414
	cmpfgt	%r1, %r1, %r2		# 5415
	jmpi	%r1, else.16303		# 5416
	movhiz	%r1, 276464		# 5417
	jmpui	if_cont.16304		# 5418
else.16303:
	movi	%r1, 0		# 5419
if_cont.16304:
	jmpui	if_cont.16302		# 5420
else.16301:
	cmpfgt	%r1, %r1, %r2		# 5421
	jmpi	%r1, else.16305		# 5422
	movi	%r1, 0		# 5423
	jmpui	if_cont.16306		# 5424
else.16305:
	movhiz	%r1, 276464		# 5425
if_cont.16306:
if_cont.16302:
	addi	%r3, %r3, 1		# 5426
	sto	%r1, (%r3)		# 5427
	addi	%r63, %r63, -1		# 5428
	ld	%r60, (%r63)		# 5429
	jmpu	%r60		# 5430
else.16300:
	cmpeqi	%r4, %r3, 2		# 5431
	jmpzi	%r4, else.16308		# 5432
	addi	%r2, %r2, 1		# 5433
	ld	%r1, (%r2)		# 5434
	movhiz	%r2, 256000		# 5435
	fmul	%r1, %r1, %r2		# 5436
	addi	%r63, %r63, 6		# 5437
	call	%r60, min_caml_sin		# 5438
	addi	%r63, %r63, -6		# 5439
	fmul	%r1, %r1, %r1		# 5440
	movi	%r2, min_caml_texture_color		# 5441
	movhiz	%r3, 276464		# 5442
	fmul	%r3, %r3, %r1		# 5443
	addi	%r2, %r2, 0		# 5444
	sto	%r3, (%r2)		# 5445
	movi	%r2, min_caml_texture_color		# 5446
	movhiz	%r3, 276464		# 5447
	movhiz	%r4, 260096		# 5448
	fsub	%r4, %r4, %r1		# 5449
	fmul	%r3, %r3, %r4		# 5450
	addi	%r2, %r2, 1		# 5451
	sto	%r3, (%r2)		# 5452
	addi	%r63, %r63, -1		# 5453
	ld	%r60, (%r63)		# 5454
	jmpu	%r60		# 5455
else.16308:
	cmpeqi	%r4, %r3, 3		# 5456
	jmpzi	%r4, else.16310		# 5457
	addi	%r3, %r2, 0		# 5458
	ld	%r3, (%r3)		# 5459
	ld	%r4, 5(%r1)		# 5460
	addi	%r4, %r4, 0		# 5461
	ld	%r4, (%r4)		# 5462
	fsub	%r3, %r3, %r4		# 5463
	addi	%r2, %r2, 2		# 5464
	ld	%r2, (%r2)		# 5465
	ld	%r1, 5(%r1)		# 5466
	addi	%r1, %r1, 2		# 5467
	ld	%r1, (%r1)		# 5468
	fsub	%r2, %r2, %r1		# 5469
	fmul	%r3, %r3, %r3		# 5470
	fmul	%r2, %r2, %r2		# 5471
	fadd	%r3, %r3, %r2		# 5472
	fsqrt	%r3, %r3		# 5473
	movhiz	%r1, 266752		# 5474
	finv	%r1, %r1		# 5475
	fmul	%r1, %r3, %r1		# 5476
	sto	%r1, 6(%r63)		# 5477
	addi	%r63, %r63, 7		# 5478
	call	%r60, min_caml_floor		# 5479
	addi	%r63, %r63, -7		# 5480
	ld	%r2, 6(%r63)		# 5481
	fsub	%r2, %r2, %r1		# 5482
	movhiz	%r1, 263312		# 5483
	addi	%r1, %r1, 4059		# 5484
	fmul	%r1, %r2, %r1		# 5485
	addi	%r63, %r63, 7		# 5486
	call	%r60, min_caml_cos		# 5487
	addi	%r63, %r63, -7		# 5488
	fmul	%r1, %r1, %r1		# 5489
	movi	%r2, min_caml_texture_color		# 5490
	movhiz	%r3, 276464		# 5491
	fmul	%r3, %r1, %r3		# 5492
	addi	%r2, %r2, 1		# 5493
	sto	%r3, (%r2)		# 5494
	movi	%r2, min_caml_texture_color		# 5495
	movhiz	%r3, 260096		# 5496
	fsub	%r3, %r3, %r1		# 5497
	movhiz	%r1, 276464		# 5498
	fmul	%r3, %r3, %r1		# 5499
	addi	%r2, %r2, 2		# 5500
	sto	%r3, (%r2)		# 5501
	addi	%r63, %r63, -1		# 5502
	ld	%r60, (%r63)		# 5503
	jmpu	%r60		# 5504
else.16310:
	cmpeqi	%r3, %r3, 4		# 5505
	jmpzi	%r3, else.16312		# 5506
	addi	%r3, %r2, 0		# 5507
	ld	%r3, (%r3)		# 5508
	ld	%r4, 5(%r1)		# 5509
	addi	%r4, %r4, 0		# 5510
	ld	%r4, (%r4)		# 5511
	fsub	%r3, %r3, %r4		# 5512
	ld	%r4, 4(%r1)		# 5513
	addi	%r4, %r4, 0		# 5514
	ld	%r4, (%r4)		# 5515
	fsqrt	%r4, %r4		# 5516
	fmul	%r3, %r3, %r4		# 5517
	addi	%r4, %r2, 2		# 5518
	ld	%r4, (%r4)		# 5519
	ld	%r5, 5(%r1)		# 5520
	addi	%r5, %r5, 2		# 5521
	ld	%r5, (%r5)		# 5522
	fsub	%r4, %r4, %r5		# 5523
	ld	%r5, 4(%r1)		# 5524
	addi	%r5, %r5, 2		# 5525
	ld	%r5, (%r5)		# 5526
	fsqrt	%r5, %r5		# 5527
	fmul	%r4, %r4, %r5		# 5528
	fmul	%r5, %r3, %r3		# 5529
	fmul	%r6, %r4, %r4		# 5530
	fadd	%r5, %r5, %r6		# 5531
	fabs	%r6, %r3		# 5532
	movhiz	%r7, 232731		# 5533
	addi	%r7, %r7, 1815		# 5534
	cmpfgt	%r7, %r7, %r6		# 5535
	sto	%r5, 7(%r63)		# 5536
	sto	%r1, (%r63)		# 5537
	sto	%r2, 1(%r63)		# 5538
	jmpi	%r7, else.16313		# 5539
	finv	%r3, %r3		# 5540
	fmul	%r4, %r4, %r3		# 5541
	fabs	%r4, %r4		# 5542
	mov	%r1, %r4		# 5543
	addi	%r63, %r63, 8		# 5544
	call	%r60, min_caml_atan		# 5545
	addi	%r63, %r63, -8		# 5546
	movhiz	%r2, 270080		# 5547
	fmul	%r1, %r1, %r2		# 5548
	movhiz	%r2, 263312		# 5549
	addi	%r2, %r2, 4059		# 5550
	finv	%r2, %r2		# 5551
	fmul	%r1, %r1, %r2		# 5552
	jmpui	if_cont.16314		# 5553
else.16313:
	movhiz	%r1, 268032		# 5554
if_cont.16314:
	sto	%r1, 8(%r63)		# 5555
	addi	%r63, %r63, 9		# 5556
	call	%r60, min_caml_floor		# 5557
	addi	%r63, %r63, -9		# 5558
	ld	%r2, 8(%r63)		# 5559
	fsub	%r2, %r2, %r1		# 5560
	ld	%r1, 1(%r63)		# 5561
	addi	%r1, %r1, 1		# 5562
	ld	%r1, (%r1)		# 5563
	ld	%r3, (%r63)		# 5564
	ld	%r4, 5(%r3)		# 5565
	addi	%r4, %r4, 1		# 5566
	ld	%r4, (%r4)		# 5567
	fsub	%r1, %r1, %r4		# 5568
	ld	%r3, 4(%r3)		# 5569
	addi	%r3, %r3, 1		# 5570
	ld	%r3, (%r3)		# 5571
	fsqrt	%r3, %r3		# 5572
	fmul	%r1, %r1, %r3		# 5573
	ld	%r3, 7(%r63)		# 5574
	fabs	%r4, %r3		# 5575
	movhiz	%r5, 232731		# 5576
	addi	%r5, %r5, 1815		# 5577
	cmpfgt	%r5, %r5, %r4		# 5578
	sto	%r2, 9(%r63)		# 5579
	jmpi	%r5, else.16315		# 5580
	finv	%r3, %r3		# 5581
	fmul	%r1, %r1, %r3		# 5582
	fabs	%r1, %r1		# 5583
	addi	%r63, %r63, 10		# 5584
	call	%r60, min_caml_atan		# 5585
	addi	%r63, %r63, -10		# 5586
	movhiz	%r2, 270080		# 5587
	fmul	%r1, %r1, %r2		# 5588
	movhiz	%r2, 263312		# 5589
	addi	%r2, %r2, 4059		# 5590
	finv	%r2, %r2		# 5591
	fmul	%r1, %r1, %r2		# 5592
	jmpui	if_cont.16316		# 5593
else.16315:
	movhiz	%r1, 268032		# 5594
if_cont.16316:
	sto	%r1, 10(%r63)		# 5595
	addi	%r63, %r63, 11		# 5596
	call	%r60, min_caml_floor		# 5597
	addi	%r63, %r63, -11		# 5598
	ld	%r2, 10(%r63)		# 5599
	fsub	%r2, %r2, %r1		# 5600
	movhiz	%r1, 254361		# 5601
	addi	%r1, %r1, 2458		# 5602
	movhiz	%r3, 258048		# 5603
	ld	%r4, 9(%r63)		# 5604
	fsub	%r3, %r3, %r4		# 5605
	fmul	%r3, %r3, %r3		# 5606
	fsub	%r1, %r1, %r3		# 5607
	movhiz	%r3, 258048		# 5608
	fsub	%r3, %r3, %r2		# 5609
	fmul	%r3, %r3, %r3		# 5610
	fsub	%r1, %r1, %r3		# 5611
	movi	%r2, 0		# 5612
	cmpfgt	%r2, %r2, %r1		# 5613
	jmpi	%r2, else.16317		# 5614
	jmpui	if_cont.16318		# 5615
else.16317:
	movi	%r1, 0		# 5616
if_cont.16318:
	movi	%r2, min_caml_texture_color		# 5617
	movhiz	%r3, 276464		# 5618
	fmul	%r3, %r3, %r1		# 5619
	movhiz	%r1, 256409		# 5620
	addi	%r1, %r1, 2458		# 5621
	finv	%r1, %r1		# 5622
	fmul	%r3, %r3, %r1		# 5623
	addi	%r2, %r2, 2		# 5624
	sto	%r3, (%r2)		# 5625
	addi	%r63, %r63, -1		# 5626
	ld	%r60, (%r63)		# 5627
	jmpu	%r60		# 5628
else.16312:
	addi	%r63, %r63, -1		# 5629
	ld	%r60, (%r63)		# 5630
	jmpu	%r60		# 5631
.globl	add_light.2788
add_light.2788:
	movi	%r4, 0		# 5632
	cmpfgt	%r4, %r1, %r4		# 5633
	jmpi	%r4, else.16321		# 5634
	jmpui	if_cont.16322		# 5635
else.16321:
	movi	%r4, min_caml_rgb		# 5636
	movi	%r5, min_caml_texture_color		# 5637
	addi	%r6, %r4, 0		# 5638
	ld	%r6, (%r6)		# 5639
	addi	%r7, %r5, 0		# 5640
	ld	%r7, (%r7)		# 5641
	fmul	%r7, %r1, %r7		# 5642
	fadd	%r6, %r6, %r7		# 5643
	addi	%r7, %r4, 0		# 5644
	sto	%r6, (%r7)		# 5645
	addi	%r6, %r4, 1		# 5646
	ld	%r6, (%r6)		# 5647
	addi	%r7, %r5, 1		# 5648
	ld	%r7, (%r7)		# 5649
	fmul	%r7, %r1, %r7		# 5650
	fadd	%r6, %r6, %r7		# 5651
	addi	%r7, %r4, 1		# 5652
	sto	%r6, (%r7)		# 5653
	addi	%r6, %r4, 2		# 5654
	ld	%r6, (%r6)		# 5655
	addi	%r5, %r5, 2		# 5656
	ld	%r5, (%r5)		# 5657
	fmul	%r1, %r1, %r5		# 5658
	fadd	%r6, %r6, %r1		# 5659
	addi	%r4, %r4, 2		# 5660
	sto	%r6, (%r4)		# 5661
if_cont.16322:
	movi	%r1, 0		# 5662
	cmpfgt	%r1, %r2, %r1		# 5663
	jmpi	%r1, else.16323		# 5664
	jmpu	%r60		# 5665
else.16323:
	fmul	%r2, %r2, %r2		# 5666
	fmul	%r2, %r2, %r2		# 5667
	fmul	%r2, %r2, %r3		# 5668
	movi	%r1, min_caml_rgb		# 5669
	movi	%r3, min_caml_rgb		# 5670
	addi	%r3, %r3, 0		# 5671
	ld	%r3, (%r3)		# 5672
	fadd	%r3, %r3, %r2		# 5673
	addi	%r1, %r1, 0		# 5674
	sto	%r3, (%r1)		# 5675
	movi	%r1, min_caml_rgb		# 5676
	movi	%r3, min_caml_rgb		# 5677
	addi	%r3, %r3, 1		# 5678
	ld	%r3, (%r3)		# 5679
	fadd	%r3, %r3, %r2		# 5680
	addi	%r1, %r1, 1		# 5681
	sto	%r3, (%r1)		# 5682
	movi	%r1, min_caml_rgb		# 5683
	movi	%r3, min_caml_rgb		# 5684
	addi	%r3, %r3, 2		# 5685
	ld	%r3, (%r3)		# 5686
	fadd	%r3, %r3, %r2		# 5687
	addi	%r1, %r1, 2		# 5688
	sto	%r3, (%r1)		# 5689
	jmpu	%r60		# 5690
.globl	trace_reflections.2792
trace_reflections.2792:
	sto	%r60, (%r63)		# 5691
	addi	%r63, %r63, 1		# 5692
	movi	%r5, 0		# 5693
	cmpgt	%r5, %r5, %r1		# 5694
	jmpi	%r5, else.16326		# 5695
	movi	%r5, min_caml_reflections		# 5696
	add	%r5, %r5, %r1		# 5697
	ld	%r5, (%r5)		# 5698
	ld	%r6, 1(%r5)		# 5699
	movi	%r7, min_caml_tmin		# 5700
	movhiz	%r8, 321254		# 5701
	addi	%r8, %r8, 2856		# 5702
	addi	%r7, %r7, 0		# 5703
	sto	%r8, (%r7)		# 5704
	movi	%r7, 0		# 5705
	movi	%r8, min_caml_or_net		# 5706
	addi	%r8, %r8, 0		# 5707
	ld	%r8, (%r8)		# 5708
	sto	%r1, (%r63)		# 5709
	sto	%r4, 1(%r63)		# 5710
	sto	%r2, 2(%r63)		# 5711
	sto	%r3, 3(%r63)		# 5712
	sto	%r6, 4(%r63)		# 5713
	sto	%r5, 5(%r63)		# 5714
	mov	%r3, %r6		# 5715
	mov	%r2, %r8		# 5716
	mov	%r1, %r7		# 5717
	addi	%r63, %r63, 6		# 5718
	call	%r60, trace_or_matrix_fast.2770		# 5719
	addi	%r63, %r63, -6		# 5720
	movi	%r1, min_caml_tmin		# 5721
	addi	%r1, %r1, 0		# 5722
	ld	%r1, (%r1)		# 5723
	movhiz	%r2, 777420		# 5724
	addi	%r2, %r2, 3277		# 5725
	cmpfgt	%r2, %r1, %r2		# 5726
	jmpi	%r2, else.16327		# 5727
	movi	%r1, 0		# 5728
	jmpui	if_cont.16328		# 5729
else.16327:
	movhiz	%r2, 314347		# 5730
	addi	%r2, %r2, 3104		# 5731
	cmpfgt	%r2, %r2, %r1		# 5732
	jmpi	%r2, else.16329		# 5733
	movi	%r1, 0		# 5734
	jmpui	if_cont.16330		# 5735
else.16329:
	movi	%r1, 1		# 5736
if_cont.16330:
if_cont.16328:
	cmpeqi	%r1, %r1, 0		# 5737
	jmpzi	%r1, else.16331		# 5738
	jmpui	if_cont.16332		# 5739
else.16331:
	movi	%r1, min_caml_intersected_object_id		# 5740
	addi	%r1, %r1, 0		# 5741
	ld	%r1, (%r1)		# 5742
	movi	%r2, 0		# 5743
	cmplti	%r3, %r1, 0		# 5744
	jmpzi	%r3, else.16333		# 5745
	neg	%r1, %r1		# 5746
	lshifti	%r1, %r1, 2		# 5747
	add	%r2, %r2, %r1		# 5748
	neg	%r2, %r2		# 5749
	jmpui	if_cont.16334		# 5750
else.16333:
	lshifti	%r1, %r1, 2		# 5751
	add	%r2, %r2, %r1		# 5752
if_cont.16334:
	movi	%r1, min_caml_intsec_rectside		# 5753
	addi	%r1, %r1, 0		# 5754
	ld	%r1, (%r1)		# 5755
	add	%r2, %r2, %r1		# 5756
	ld	%r1, 5(%r63)		# 5757
	ld	%r3, (%r1)		# 5758
	cmpeq	%r2, %r2, %r3		# 5759
	jmpzi	%r2, else.16335		# 5760
	movi	%r2, 0		# 5761
	movi	%r3, min_caml_or_net		# 5762
	addi	%r3, %r3, 0		# 5763
	ld	%r3, (%r3)		# 5764
	mov	%r1, %r2		# 5765
	mov	%r2, %r3		# 5766
	addi	%r63, %r63, 6		# 5767
	call	%r60, shadow_check_one_or_matrix.2745		# 5768
	addi	%r63, %r63, -6		# 5769
	cmpeqi	%r1, %r1, 0		# 5770
	jmpzi	%r1, else.16337		# 5771
	movi	%r1, min_caml_nvector		# 5772
	ld	%r2, 4(%r63)		# 5773
	ld	%r3, (%r2)		# 5774
	addi	%r4, %r1, 0		# 5775
	ld	%r4, (%r4)		# 5776
	addi	%r5, %r3, 0		# 5777
	ld	%r5, (%r5)		# 5778
	fmul	%r4, %r4, %r5		# 5779
	addi	%r5, %r1, 1		# 5780
	ld	%r5, (%r5)		# 5781
	addi	%r6, %r3, 1		# 5782
	ld	%r6, (%r6)		# 5783
	fmul	%r5, %r5, %r6		# 5784
	fadd	%r4, %r4, %r5		# 5785
	addi	%r1, %r1, 2		# 5786
	ld	%r1, (%r1)		# 5787
	addi	%r3, %r3, 2		# 5788
	ld	%r3, (%r3)		# 5789
	fmul	%r1, %r1, %r3		# 5790
	fadd	%r4, %r4, %r1		# 5791
	ld	%r1, 5(%r63)		# 5792
	ld	%r1, 2(%r1)		# 5793
	ld	%r3, 3(%r63)		# 5794
	fmul	%r5, %r1, %r3		# 5795
	fmul	%r5, %r5, %r4		# 5796
	ld	%r2, (%r2)		# 5797
	ld	%r4, 2(%r63)		# 5798
	addi	%r6, %r4, 0		# 5799
	ld	%r6, (%r6)		# 5800
	addi	%r7, %r2, 0		# 5801
	ld	%r7, (%r7)		# 5802
	fmul	%r6, %r6, %r7		# 5803
	addi	%r7, %r4, 1		# 5804
	ld	%r7, (%r7)		# 5805
	addi	%r8, %r2, 1		# 5806
	ld	%r8, (%r8)		# 5807
	fmul	%r7, %r7, %r8		# 5808
	fadd	%r6, %r6, %r7		# 5809
	addi	%r7, %r4, 2		# 5810
	ld	%r7, (%r7)		# 5811
	addi	%r2, %r2, 2		# 5812
	ld	%r2, (%r2)		# 5813
	fmul	%r7, %r7, %r2		# 5814
	fadd	%r6, %r6, %r7		# 5815
	fmul	%r2, %r1, %r6		# 5816
	ld	%r1, 1(%r63)		# 5817
	mov	%r3, %r1		# 5818
	mov	%r1, %r5		# 5819
	addi	%r63, %r63, 6		# 5820
	call	%r60, add_light.2788		# 5821
	addi	%r63, %r63, -6		# 5822
	jmpui	if_cont.16338		# 5823
else.16337:
if_cont.16338:
	jmpui	if_cont.16336		# 5824
else.16335:
if_cont.16336:
if_cont.16332:
	ld	%r1, (%r63)		# 5825
	addi	%r1, %r1, -1		# 5826
	ld	%r3, 3(%r63)		# 5827
	ld	%r4, 1(%r63)		# 5828
	ld	%r2, 2(%r63)		# 5829
	addi	%r63, %r63, -1		# 5830
	ld	%r60, (%r63)		# 5831
	jmpui	trace_reflections.2792		# 5832
else.16326:
	addi	%r63, %r63, -1		# 5833
	ld	%r60, (%r63)		# 5834
	jmpu	%r60		# 5835
.globl	trace_ray.2797
trace_ray.2797:
	sto	%r60, (%r63)		# 5836
	addi	%r63, %r63, 1		# 5837
	cmpgti	%r6, %r1, 4		# 5838
	jmpi	%r6, else.16340		# 5839
	ld	%r6, 2(%r3)		# 5840
	movi	%r7, min_caml_tmin		# 5841
	movhiz	%r8, 321254		# 5842
	addi	%r8, %r8, 2856		# 5843
	addi	%r7, %r7, 0		# 5844
	sto	%r8, (%r7)		# 5845
	movi	%r7, 0		# 5846
	movi	%r8, min_caml_or_net		# 5847
	addi	%r8, %r8, 0		# 5848
	ld	%r8, (%r8)		# 5849
	sto	%r5, (%r63)		# 5850
	sto	%r3, 1(%r63)		# 5851
	sto	%r4, 2(%r63)		# 5852
	sto	%r2, 3(%r63)		# 5853
	sto	%r1, 4(%r63)		# 5854
	sto	%r6, 5(%r63)		# 5855
	mov	%r3, %r2		# 5856
	mov	%r1, %r7		# 5857
	mov	%r2, %r8		# 5858
	addi	%r63, %r63, 6		# 5859
	call	%r60, trace_or_matrix.2756		# 5860
	addi	%r63, %r63, -6		# 5861
	movi	%r1, min_caml_tmin		# 5862
	addi	%r1, %r1, 0		# 5863
	ld	%r1, (%r1)		# 5864
	movhiz	%r2, 777420		# 5865
	addi	%r2, %r2, 3277		# 5866
	cmpfgt	%r2, %r1, %r2		# 5867
	jmpi	%r2, else.16341		# 5868
	movi	%r1, 0		# 5869
	jmpui	if_cont.16342		# 5870
else.16341:
	movhiz	%r2, 314347		# 5871
	addi	%r2, %r2, 3104		# 5872
	cmpfgt	%r2, %r2, %r1		# 5873
	jmpi	%r2, else.16343		# 5874
	movi	%r1, 0		# 5875
	jmpui	if_cont.16344		# 5876
else.16343:
	movi	%r1, 1		# 5877
if_cont.16344:
if_cont.16342:
	cmpeqi	%r1, %r1, 0		# 5878
	jmpzi	%r1, else.16345		# 5879
	movi	%r1, -1		# 5880
	ld	%r2, 4(%r63)		# 5881
	ld	%r3, 5(%r63)		# 5882
	add	%r3, %r3, %r2		# 5883
	sto	%r1, (%r3)		# 5884
	cmpeqi	%r2, %r2, 0		# 5885
	jmpzi	%r2, else.16346		# 5886
	addi	%r63, %r63, -1		# 5887
	ld	%r60, (%r63)		# 5888
	jmpu	%r60		# 5889
else.16346:
	movi	%r1, min_caml_light		# 5890
	ld	%r2, 3(%r63)		# 5891
	addi	%r3, %r2, 0		# 5892
	ld	%r3, (%r3)		# 5893
	addi	%r4, %r1, 0		# 5894
	ld	%r4, (%r4)		# 5895
	fmul	%r3, %r3, %r4		# 5896
	addi	%r4, %r2, 1		# 5897
	ld	%r4, (%r4)		# 5898
	addi	%r5, %r1, 1		# 5899
	ld	%r5, (%r5)		# 5900
	fmul	%r4, %r4, %r5		# 5901
	fadd	%r3, %r3, %r4		# 5902
	addi	%r2, %r2, 2		# 5903
	ld	%r2, (%r2)		# 5904
	addi	%r1, %r1, 2		# 5905
	ld	%r1, (%r1)		# 5906
	fmul	%r2, %r2, %r1		# 5907
	fadd	%r3, %r3, %r2		# 5908
	fneg	%r3, %r3		# 5909
	movi	%r1, 0		# 5910
	cmpfgt	%r1, %r3, %r1		# 5911
	jmpi	%r1, else.16348		# 5912
	addi	%r63, %r63, -1		# 5913
	ld	%r60, (%r63)		# 5914
	jmpu	%r60		# 5915
else.16348:
	fmul	%r1, %r3, %r3		# 5916
	fmul	%r1, %r1, %r3		# 5917
	ld	%r2, 2(%r63)		# 5918
	fmul	%r1, %r1, %r2		# 5919
	movi	%r2, min_caml_beam		# 5920
	addi	%r2, %r2, 0		# 5921
	ld	%r2, (%r2)		# 5922
	fmul	%r1, %r1, %r2		# 5923
	movi	%r2, min_caml_rgb		# 5924
	movi	%r3, min_caml_rgb		# 5925
	addi	%r3, %r3, 0		# 5926
	ld	%r3, (%r3)		# 5927
	fadd	%r3, %r3, %r1		# 5928
	addi	%r2, %r2, 0		# 5929
	sto	%r3, (%r2)		# 5930
	movi	%r2, min_caml_rgb		# 5931
	movi	%r3, min_caml_rgb		# 5932
	addi	%r3, %r3, 1		# 5933
	ld	%r3, (%r3)		# 5934
	fadd	%r3, %r3, %r1		# 5935
	addi	%r2, %r2, 1		# 5936
	sto	%r3, (%r2)		# 5937
	movi	%r2, min_caml_rgb		# 5938
	movi	%r3, min_caml_rgb		# 5939
	addi	%r3, %r3, 2		# 5940
	ld	%r3, (%r3)		# 5941
	fadd	%r3, %r3, %r1		# 5942
	addi	%r2, %r2, 2		# 5943
	sto	%r3, (%r2)		# 5944
	addi	%r63, %r63, -1		# 5945
	ld	%r60, (%r63)		# 5946
	jmpu	%r60		# 5947
else.16345:
	movi	%r1, min_caml_intersected_object_id		# 5948
	addi	%r1, %r1, 0		# 5949
	ld	%r1, (%r1)		# 5950
	movi	%r2, min_caml_objects		# 5951
	add	%r2, %r2, %r1		# 5952
	ld	%r2, (%r2)		# 5953
	ld	%r3, 2(%r2)		# 5954
	ld	%r4, 7(%r2)		# 5955
	addi	%r4, %r4, 0		# 5956
	ld	%r4, (%r4)		# 5957
	ld	%r5, 2(%r63)		# 5958
	fmul	%r4, %r4, %r5		# 5959
	ld	%r6, 1(%r2)		# 5960
	cmpeqi	%r7, %r6, 1		# 5961
	sto	%r3, 6(%r63)		# 5962
	sto	%r4, 7(%r63)		# 5963
	sto	%r1, 8(%r63)		# 5964
	sto	%r2, 9(%r63)		# 5965
	jmpzi	%r7, else.16351		# 5966
	movi	%r6, min_caml_intsec_rectside		# 5967
	addi	%r6, %r6, 0		# 5968
	ld	%r6, (%r6)		# 5969
	movi	%r7, min_caml_nvector		# 5970
	movi	%r8, 0		# 5971
	addi	%r9, %r7, 0		# 5972
	sto	%r8, (%r9)		# 5973
	addi	%r9, %r7, 1		# 5974
	sto	%r8, (%r9)		# 5975
	addi	%r7, %r7, 2		# 5976
	sto	%r8, (%r7)		# 5977
	movi	%r7, min_caml_nvector		# 5978
	addi	%r8, %r6, -1		# 5979
	addi	%r6, %r6, -1		# 5980
	ld	%r9, 3(%r63)		# 5981
	add	%r6, %r9, %r6		# 5982
	ld	%r6, (%r6)		# 5983
	movi	%r10, 0		# 5984
	cmpfeq	%r10, %r6, %r10		# 5985
	jmpzi	%r10, else.16353		# 5986
	movi	%r6, 0		# 5987
	jmpui	if_cont.16354		# 5988
else.16353:
	movi	%r10, 0		# 5989
	cmpfgt	%r6, %r6, %r10		# 5990
	jmpi	%r6, else.16355		# 5991
	movhiz	%r6, 784384		# 5992
	jmpui	if_cont.16356		# 5993
else.16355:
	movhiz	%r6, 260096		# 5994
if_cont.16356:
if_cont.16354:
	fneg	%r6, %r6		# 5995
	add	%r7, %r7, %r8		# 5996
	sto	%r6, (%r7)		# 5997
	jmpui	if_cont.16352		# 5998
else.16351:
	cmpeqi	%r6, %r6, 2		# 5999
	jmpzi	%r6, else.16357		# 6000
	movi	%r6, min_caml_nvector		# 6001
	ld	%r7, 4(%r2)		# 6002
	addi	%r7, %r7, 0		# 6003
	ld	%r7, (%r7)		# 6004
	fneg	%r7, %r7		# 6005
	addi	%r6, %r6, 0		# 6006
	sto	%r7, (%r6)		# 6007
	movi	%r6, min_caml_nvector		# 6008
	ld	%r7, 4(%r2)		# 6009
	addi	%r7, %r7, 1		# 6010
	ld	%r7, (%r7)		# 6011
	fneg	%r7, %r7		# 6012
	addi	%r6, %r6, 1		# 6013
	sto	%r7, (%r6)		# 6014
	movi	%r6, min_caml_nvector		# 6015
	ld	%r7, 4(%r2)		# 6016
	addi	%r7, %r7, 2		# 6017
	ld	%r7, (%r7)		# 6018
	fneg	%r7, %r7		# 6019
	addi	%r6, %r6, 2		# 6020
	sto	%r7, (%r6)		# 6021
	jmpui	if_cont.16358		# 6022
else.16357:
	mov	%r1, %r2		# 6023
	addi	%r63, %r63, 10		# 6024
	call	%r60, get_nvector_second.2780		# 6025
	addi	%r63, %r63, -10		# 6026
if_cont.16358:
if_cont.16352:
	movi	%r1, min_caml_startp		# 6027
	movi	%r2, min_caml_intersection_point		# 6028
	addi	%r3, %r2, 0		# 6029
	ld	%r3, (%r3)		# 6030
	addi	%r4, %r1, 0		# 6031
	sto	%r3, (%r4)		# 6032
	addi	%r3, %r2, 1		# 6033
	ld	%r3, (%r3)		# 6034
	addi	%r4, %r1, 1		# 6035
	sto	%r3, (%r4)		# 6036
	addi	%r2, %r2, 2		# 6037
	ld	%r2, (%r2)		# 6038
	addi	%r1, %r1, 2		# 6039
	sto	%r2, (%r1)		# 6040
	movi	%r2, min_caml_intersection_point		# 6041
	ld	%r1, 9(%r63)		# 6042
	addi	%r63, %r63, 10		# 6043
	call	%r60, utexture.2785		# 6044
	addi	%r63, %r63, -10		# 6045
	movi	%r1, 0		# 6046
	ld	%r2, 8(%r63)		# 6047
	cmplti	%r3, %r2, 0		# 6048
	jmpzi	%r3, else.16359		# 6049
	neg	%r2, %r2		# 6050
	lshifti	%r2, %r2, 2		# 6051
	add	%r1, %r1, %r2		# 6052
	neg	%r1, %r1		# 6053
	jmpui	if_cont.16360		# 6054
else.16359:
	lshifti	%r2, %r2, 2		# 6055
	add	%r1, %r1, %r2		# 6056
if_cont.16360:
	movi	%r2, min_caml_intsec_rectside		# 6057
	addi	%r2, %r2, 0		# 6058
	ld	%r2, (%r2)		# 6059
	add	%r1, %r1, %r2		# 6060
	ld	%r2, 4(%r63)		# 6061
	ld	%r3, 5(%r63)		# 6062
	add	%r4, %r3, %r2		# 6063
	sto	%r1, (%r4)		# 6064
	ld	%r1, 1(%r63)		# 6065
	ld	%r4, 1(%r1)		# 6066
	add	%r4, %r4, %r2		# 6067
	ld	%r4, (%r4)		# 6068
	movi	%r5, min_caml_intersection_point		# 6069
	addi	%r6, %r5, 0		# 6070
	ld	%r6, (%r6)		# 6071
	addi	%r7, %r4, 0		# 6072
	sto	%r6, (%r7)		# 6073
	addi	%r6, %r5, 1		# 6074
	ld	%r6, (%r6)		# 6075
	addi	%r7, %r4, 1		# 6076
	sto	%r6, (%r7)		# 6077
	addi	%r5, %r5, 2		# 6078
	ld	%r5, (%r5)		# 6079
	addi	%r4, %r4, 2		# 6080
	sto	%r5, (%r4)		# 6081
	ld	%r4, 3(%r1)		# 6082
	ld	%r5, 9(%r63)		# 6083
	ld	%r6, 7(%r5)		# 6084
	addi	%r6, %r6, 0		# 6085
	ld	%r6, (%r6)		# 6086
	movhiz	%r7, 258048		# 6087
	cmpfgt	%r7, %r7, %r6		# 6088
	jmpi	%r7, else.16361		# 6089
	movi	%r6, 1		# 6090
	add	%r4, %r4, %r2		# 6091
	sto	%r6, (%r4)		# 6092
	ld	%r4, 4(%r1)		# 6093
	add	%r6, %r4, %r2		# 6094
	ld	%r6, (%r6)		# 6095
	movi	%r7, min_caml_texture_color		# 6096
	addi	%r8, %r7, 0		# 6097
	ld	%r8, (%r8)		# 6098
	addi	%r9, %r6, 0		# 6099
	sto	%r8, (%r9)		# 6100
	addi	%r8, %r7, 1		# 6101
	ld	%r8, (%r8)		# 6102
	addi	%r9, %r6, 1		# 6103
	sto	%r8, (%r9)		# 6104
	addi	%r7, %r7, 2		# 6105
	ld	%r7, (%r7)		# 6106
	addi	%r6, %r6, 2		# 6107
	sto	%r7, (%r6)		# 6108
	add	%r4, %r4, %r2		# 6109
	ld	%r4, (%r4)		# 6110
	movhiz	%r6, 243712		# 6111
	ld	%r7, 7(%r63)		# 6112
	fmul	%r6, %r6, %r7		# 6113
	addi	%r8, %r4, 0		# 6114
	ld	%r8, (%r8)		# 6115
	fmul	%r8, %r8, %r6		# 6116
	addi	%r9, %r4, 0		# 6117
	sto	%r8, (%r9)		# 6118
	addi	%r8, %r4, 1		# 6119
	ld	%r8, (%r8)		# 6120
	fmul	%r8, %r8, %r6		# 6121
	addi	%r9, %r4, 1		# 6122
	sto	%r8, (%r9)		# 6123
	addi	%r8, %r4, 2		# 6124
	ld	%r8, (%r8)		# 6125
	fmul	%r8, %r8, %r6		# 6126
	addi	%r4, %r4, 2		# 6127
	sto	%r8, (%r4)		# 6128
	ld	%r4, 7(%r1)		# 6129
	add	%r4, %r4, %r2		# 6130
	ld	%r4, (%r4)		# 6131
	movi	%r6, min_caml_nvector		# 6132
	addi	%r8, %r6, 0		# 6133
	ld	%r8, (%r8)		# 6134
	addi	%r9, %r4, 0		# 6135
	sto	%r8, (%r9)		# 6136
	addi	%r8, %r6, 1		# 6137
	ld	%r8, (%r8)		# 6138
	addi	%r9, %r4, 1		# 6139
	sto	%r8, (%r9)		# 6140
	addi	%r6, %r6, 2		# 6141
	ld	%r6, (%r6)		# 6142
	addi	%r4, %r4, 2		# 6143
	sto	%r6, (%r4)		# 6144
	jmpui	if_cont.16362		# 6145
else.16361:
	movi	%r6, 0		# 6146
	add	%r4, %r4, %r2		# 6147
	sto	%r6, (%r4)		# 6148
if_cont.16362:
	movhiz	%r4, 786432		# 6149
	movi	%r6, min_caml_nvector		# 6150
	ld	%r7, 3(%r63)		# 6151
	addi	%r8, %r7, 0		# 6152
	ld	%r8, (%r8)		# 6153
	addi	%r9, %r6, 0		# 6154
	ld	%r9, (%r9)		# 6155
	fmul	%r8, %r8, %r9		# 6156
	addi	%r9, %r7, 1		# 6157
	ld	%r9, (%r9)		# 6158
	addi	%r10, %r6, 1		# 6159
	ld	%r10, (%r10)		# 6160
	fmul	%r9, %r9, %r10		# 6161
	fadd	%r8, %r8, %r9		# 6162
	addi	%r9, %r7, 2		# 6163
	ld	%r9, (%r9)		# 6164
	addi	%r6, %r6, 2		# 6165
	ld	%r6, (%r6)		# 6166
	fmul	%r9, %r9, %r6		# 6167
	fadd	%r8, %r8, %r9		# 6168
	fmul	%r4, %r4, %r8		# 6169
	movi	%r6, min_caml_nvector		# 6170
	addi	%r8, %r7, 0		# 6171
	ld	%r8, (%r8)		# 6172
	addi	%r9, %r6, 0		# 6173
	ld	%r9, (%r9)		# 6174
	fmul	%r9, %r4, %r9		# 6175
	fadd	%r8, %r8, %r9		# 6176
	addi	%r9, %r7, 0		# 6177
	sto	%r8, (%r9)		# 6178
	addi	%r8, %r7, 1		# 6179
	ld	%r8, (%r8)		# 6180
	addi	%r9, %r6, 1		# 6181
	ld	%r9, (%r9)		# 6182
	fmul	%r9, %r4, %r9		# 6183
	fadd	%r8, %r8, %r9		# 6184
	addi	%r9, %r7, 1		# 6185
	sto	%r8, (%r9)		# 6186
	addi	%r8, %r7, 2		# 6187
	ld	%r8, (%r8)		# 6188
	addi	%r6, %r6, 2		# 6189
	ld	%r6, (%r6)		# 6190
	fmul	%r4, %r4, %r6		# 6191
	fadd	%r8, %r8, %r4		# 6192
	addi	%r4, %r7, 2		# 6193
	sto	%r8, (%r4)		# 6194
	ld	%r4, 7(%r5)		# 6195
	addi	%r4, %r4, 1		# 6196
	ld	%r4, (%r4)		# 6197
	ld	%r6, 2(%r63)		# 6198
	fmul	%r4, %r6, %r4		# 6199
	movi	%r8, 0		# 6200
	movi	%r9, min_caml_or_net		# 6201
	addi	%r9, %r9, 0		# 6202
	ld	%r9, (%r9)		# 6203
	sto	%r4, 10(%r63)		# 6204
	mov	%r2, %r9		# 6205
	mov	%r1, %r8		# 6206
	addi	%r63, %r63, 11		# 6207
	call	%r60, shadow_check_one_or_matrix.2745		# 6208
	addi	%r63, %r63, -11		# 6209
	cmpeqi	%r1, %r1, 0		# 6210
	jmpzi	%r1, else.16363		# 6211
	movi	%r1, min_caml_nvector		# 6212
	movi	%r2, min_caml_light		# 6213
	addi	%r3, %r1, 0		# 6214
	ld	%r3, (%r3)		# 6215
	addi	%r4, %r2, 0		# 6216
	ld	%r4, (%r4)		# 6217
	fmul	%r3, %r3, %r4		# 6218
	addi	%r4, %r1, 1		# 6219
	ld	%r4, (%r4)		# 6220
	addi	%r5, %r2, 1		# 6221
	ld	%r5, (%r5)		# 6222
	fmul	%r4, %r4, %r5		# 6223
	fadd	%r3, %r3, %r4		# 6224
	addi	%r1, %r1, 2		# 6225
	ld	%r1, (%r1)		# 6226
	addi	%r2, %r2, 2		# 6227
	ld	%r2, (%r2)		# 6228
	fmul	%r1, %r1, %r2		# 6229
	fadd	%r3, %r3, %r1		# 6230
	fneg	%r3, %r3		# 6231
	ld	%r1, 7(%r63)		# 6232
	fmul	%r3, %r3, %r1		# 6233
	movi	%r2, min_caml_light		# 6234
	ld	%r4, 3(%r63)		# 6235
	addi	%r5, %r4, 0		# 6236
	ld	%r5, (%r5)		# 6237
	addi	%r6, %r2, 0		# 6238
	ld	%r6, (%r6)		# 6239
	fmul	%r5, %r5, %r6		# 6240
	addi	%r6, %r4, 1		# 6241
	ld	%r6, (%r6)		# 6242
	addi	%r7, %r2, 1		# 6243
	ld	%r7, (%r7)		# 6244
	fmul	%r6, %r6, %r7		# 6245
	fadd	%r5, %r5, %r6		# 6246
	addi	%r6, %r4, 2		# 6247
	ld	%r6, (%r6)		# 6248
	addi	%r2, %r2, 2		# 6249
	ld	%r2, (%r2)		# 6250
	fmul	%r6, %r6, %r2		# 6251
	fadd	%r5, %r5, %r6		# 6252
	fneg	%r2, %r5		# 6253
	ld	%r5, 10(%r63)		# 6254
	mov	%r1, %r3		# 6255
	mov	%r3, %r5		# 6256
	addi	%r63, %r63, 11		# 6257
	call	%r60, add_light.2788		# 6258
	addi	%r63, %r63, -11		# 6259
	jmpui	if_cont.16364		# 6260
else.16363:
if_cont.16364:
	movi	%r1, min_caml_intersection_point		# 6261
	movi	%r2, min_caml_startp_fast		# 6262
	addi	%r3, %r1, 0		# 6263
	ld	%r3, (%r3)		# 6264
	addi	%r4, %r2, 0		# 6265
	sto	%r3, (%r4)		# 6266
	addi	%r3, %r1, 1		# 6267
	ld	%r3, (%r3)		# 6268
	addi	%r4, %r2, 1		# 6269
	sto	%r3, (%r4)		# 6270
	addi	%r3, %r1, 2		# 6271
	ld	%r3, (%r3)		# 6272
	addi	%r2, %r2, 2		# 6273
	sto	%r3, (%r2)		# 6274
	movi	%r2, min_caml_n_objects		# 6275
	addi	%r2, %r2, 0		# 6276
	ld	%r2, (%r2)		# 6277
	addi	%r2, %r2, -1		# 6278
	addi	%r63, %r63, 11		# 6279
	call	%r60, setup_startp_constants.2708		# 6280
	addi	%r63, %r63, -11		# 6281
	movi	%r1, min_caml_n_reflections		# 6282
	addi	%r1, %r1, 0		# 6283
	ld	%r1, (%r1)		# 6284
	addi	%r1, %r1, -1		# 6285
	ld	%r3, 7(%r63)		# 6286
	ld	%r4, 10(%r63)		# 6287
	ld	%r2, 3(%r63)		# 6288
	addi	%r63, %r63, 11		# 6289
	call	%r60, trace_reflections.2792		# 6290
	addi	%r63, %r63, -11		# 6291
	movhiz	%r1, 253132		# 6292
	addi	%r1, %r1, 3277		# 6293
	ld	%r2, 2(%r63)		# 6294
	cmpfgt	%r1, %r2, %r1		# 6295
	jmpi	%r1, else.16365		# 6296
	addi	%r63, %r63, -1		# 6297
	ld	%r60, (%r63)		# 6298
	jmpu	%r60		# 6299
else.16365:
	movi	%r1, 4		# 6300
	ld	%r3, 4(%r63)		# 6301
	cmpgt	%r1, %r1, %r3		# 6302
	jmpi	%r1, else.16367		# 6303
	jmpui	if_cont.16368		# 6304
else.16367:
	addi	%r1, %r3, 1		# 6305
	movi	%r4, -1		# 6306
	ld	%r5, 5(%r63)		# 6307
	add	%r5, %r5, %r1		# 6308
	sto	%r4, (%r5)		# 6309
if_cont.16368:
	ld	%r1, 6(%r63)		# 6310
	cmpeqi	%r1, %r1, 2		# 6311
	jmpzi	%r1, else.16369		# 6312
	movhiz	%r1, 260096		# 6313
	ld	%r4, 9(%r63)		# 6314
	ld	%r4, 7(%r4)		# 6315
	addi	%r4, %r4, 0		# 6316
	ld	%r4, (%r4)		# 6317
	fsub	%r1, %r1, %r4		# 6318
	fmul	%r4, %r2, %r1		# 6319
	addi	%r1, %r3, 1		# 6320
	movi	%r2, min_caml_tmin		# 6321
	addi	%r2, %r2, 0		# 6322
	ld	%r2, (%r2)		# 6323
	ld	%r3, (%r63)		# 6324
	fadd	%r5, %r3, %r2		# 6325
	ld	%r2, 3(%r63)		# 6326
	ld	%r3, 1(%r63)		# 6327
	addi	%r63, %r63, -1		# 6328
	ld	%r60, (%r63)		# 6329
	jmpui	trace_ray.2797		# 6330
else.16369:
	addi	%r63, %r63, -1		# 6331
	ld	%r60, (%r63)		# 6332
	jmpu	%r60		# 6333
else.16340:
	addi	%r63, %r63, -1		# 6334
	ld	%r60, (%r63)		# 6335
	jmpu	%r60		# 6336
.globl	trace_diffuse_ray.2803
trace_diffuse_ray.2803:
	sto	%r60, (%r63)		# 6337
	addi	%r63, %r63, 1		# 6338
	movi	%r3, min_caml_tmin		# 6339
	movhiz	%r4, 321254		# 6340
	addi	%r4, %r4, 2856		# 6341
	addi	%r3, %r3, 0		# 6342
	sto	%r4, (%r3)		# 6343
	movi	%r3, 0		# 6344
	movi	%r4, min_caml_or_net		# 6345
	addi	%r4, %r4, 0		# 6346
	ld	%r4, (%r4)		# 6347
	sto	%r2, (%r63)		# 6348
	sto	%r1, 1(%r63)		# 6349
	mov	%r2, %r4		# 6350
	sto	%r3, 2(%r63)		# 6351
	mov	%r3, %r1		# 6352
	ld	%r1, 2(%r63)		# 6353
	addi	%r63, %r63, 2		# 6354
	call	%r60, trace_or_matrix_fast.2770		# 6355
	addi	%r63, %r63, -2		# 6356
	movi	%r1, min_caml_tmin		# 6357
	addi	%r1, %r1, 0		# 6358
	ld	%r1, (%r1)		# 6359
	movhiz	%r2, 777420		# 6360
	addi	%r2, %r2, 3277		# 6361
	cmpfgt	%r2, %r1, %r2		# 6362
	jmpi	%r2, else.16372		# 6363
	movi	%r1, 0		# 6364
	jmpui	if_cont.16373		# 6365
else.16372:
	movhiz	%r2, 314347		# 6366
	addi	%r2, %r2, 3104		# 6367
	cmpfgt	%r2, %r2, %r1		# 6368
	jmpi	%r2, else.16374		# 6369
	movi	%r1, 0		# 6370
	jmpui	if_cont.16375		# 6371
else.16374:
	movi	%r1, 1		# 6372
if_cont.16375:
if_cont.16373:
	cmpeqi	%r1, %r1, 0		# 6373
	jmpzi	%r1, else.16376		# 6374
	addi	%r63, %r63, -1		# 6375
	ld	%r60, (%r63)		# 6376
	jmpu	%r60		# 6377
else.16376:
	movi	%r1, min_caml_objects		# 6378
	movi	%r2, min_caml_intersected_object_id		# 6379
	addi	%r2, %r2, 0		# 6380
	ld	%r2, (%r2)		# 6381
	add	%r1, %r1, %r2		# 6382
	ld	%r1, (%r1)		# 6383
	ld	%r2, 1(%r63)		# 6384
	ld	%r2, (%r2)		# 6385
	ld	%r3, 1(%r1)		# 6386
	cmpeqi	%r4, %r3, 1		# 6387
	sto	%r1, 2(%r63)		# 6388
	jmpzi	%r4, else.16378		# 6389
	movi	%r3, min_caml_intsec_rectside		# 6390
	addi	%r3, %r3, 0		# 6391
	ld	%r3, (%r3)		# 6392
	movi	%r4, min_caml_nvector		# 6393
	movi	%r5, 0		# 6394
	addi	%r6, %r4, 0		# 6395
	sto	%r5, (%r6)		# 6396
	addi	%r6, %r4, 1		# 6397
	sto	%r5, (%r6)		# 6398
	addi	%r4, %r4, 2		# 6399
	sto	%r5, (%r4)		# 6400
	movi	%r4, min_caml_nvector		# 6401
	addi	%r5, %r3, -1		# 6402
	addi	%r3, %r3, -1		# 6403
	add	%r2, %r2, %r3		# 6404
	ld	%r2, (%r2)		# 6405
	movi	%r3, 0		# 6406
	cmpfeq	%r3, %r2, %r3		# 6407
	jmpzi	%r3, else.16380		# 6408
	movi	%r2, 0		# 6409
	jmpui	if_cont.16381		# 6410
else.16380:
	movi	%r3, 0		# 6411
	cmpfgt	%r2, %r2, %r3		# 6412
	jmpi	%r2, else.16382		# 6413
	movhiz	%r2, 784384		# 6414
	jmpui	if_cont.16383		# 6415
else.16382:
	movhiz	%r2, 260096		# 6416
if_cont.16383:
if_cont.16381:
	fneg	%r2, %r2		# 6417
	add	%r4, %r4, %r5		# 6418
	sto	%r2, (%r4)		# 6419
	jmpui	if_cont.16379		# 6420
else.16378:
	cmpeqi	%r3, %r3, 2		# 6421
	jmpzi	%r3, else.16384		# 6422
	movi	%r2, min_caml_nvector		# 6423
	ld	%r3, 4(%r1)		# 6424
	addi	%r3, %r3, 0		# 6425
	ld	%r3, (%r3)		# 6426
	fneg	%r3, %r3		# 6427
	addi	%r2, %r2, 0		# 6428
	sto	%r3, (%r2)		# 6429
	movi	%r2, min_caml_nvector		# 6430
	ld	%r3, 4(%r1)		# 6431
	addi	%r3, %r3, 1		# 6432
	ld	%r3, (%r3)		# 6433
	fneg	%r3, %r3		# 6434
	addi	%r2, %r2, 1		# 6435
	sto	%r3, (%r2)		# 6436
	movi	%r2, min_caml_nvector		# 6437
	ld	%r3, 4(%r1)		# 6438
	addi	%r3, %r3, 2		# 6439
	ld	%r3, (%r3)		# 6440
	fneg	%r3, %r3		# 6441
	addi	%r2, %r2, 2		# 6442
	sto	%r3, (%r2)		# 6443
	jmpui	if_cont.16385		# 6444
else.16384:
	addi	%r63, %r63, 3		# 6445
	call	%r60, get_nvector_second.2780		# 6446
	addi	%r63, %r63, -3		# 6447
if_cont.16385:
if_cont.16379:
	movi	%r2, min_caml_intersection_point		# 6448
	ld	%r1, 2(%r63)		# 6449
	addi	%r63, %r63, 3		# 6450
	call	%r60, utexture.2785		# 6451
	addi	%r63, %r63, -3		# 6452
	movi	%r1, 0		# 6453
	movi	%r2, min_caml_or_net		# 6454
	addi	%r2, %r2, 0		# 6455
	ld	%r2, (%r2)		# 6456
	addi	%r63, %r63, 3		# 6457
	call	%r60, shadow_check_one_or_matrix.2745		# 6458
	addi	%r63, %r63, -3		# 6459
	cmpeqi	%r1, %r1, 0		# 6460
	jmpzi	%r1, else.16386		# 6461
	movi	%r1, min_caml_nvector		# 6462
	movi	%r2, min_caml_light		# 6463
	addi	%r3, %r1, 0		# 6464
	ld	%r3, (%r3)		# 6465
	addi	%r4, %r2, 0		# 6466
	ld	%r4, (%r4)		# 6467
	fmul	%r3, %r3, %r4		# 6468
	addi	%r4, %r1, 1		# 6469
	ld	%r4, (%r4)		# 6470
	addi	%r5, %r2, 1		# 6471
	ld	%r5, (%r5)		# 6472
	fmul	%r4, %r4, %r5		# 6473
	fadd	%r3, %r3, %r4		# 6474
	addi	%r1, %r1, 2		# 6475
	ld	%r1, (%r1)		# 6476
	addi	%r2, %r2, 2		# 6477
	ld	%r2, (%r2)		# 6478
	fmul	%r1, %r1, %r2		# 6479
	fadd	%r3, %r3, %r1		# 6480
	fneg	%r3, %r3		# 6481
	movi	%r1, 0		# 6482
	cmpfgt	%r1, %r3, %r1		# 6483
	jmpi	%r1, else.16387		# 6484
	movi	%r3, 0		# 6485
	jmpui	if_cont.16388		# 6486
else.16387:
if_cont.16388:
	movi	%r1, min_caml_diffuse_ray		# 6487
	ld	%r2, (%r63)		# 6488
	fmul	%r2, %r2, %r3		# 6489
	ld	%r3, 2(%r63)		# 6490
	ld	%r3, 7(%r3)		# 6491
	addi	%r3, %r3, 0		# 6492
	ld	%r3, (%r3)		# 6493
	fmul	%r2, %r2, %r3		# 6494
	movi	%r3, min_caml_texture_color		# 6495
	addi	%r4, %r1, 0		# 6496
	ld	%r4, (%r4)		# 6497
	addi	%r5, %r3, 0		# 6498
	ld	%r5, (%r5)		# 6499
	fmul	%r5, %r2, %r5		# 6500
	fadd	%r4, %r4, %r5		# 6501
	addi	%r5, %r1, 0		# 6502
	sto	%r4, (%r5)		# 6503
	addi	%r4, %r1, 1		# 6504
	ld	%r4, (%r4)		# 6505
	addi	%r5, %r3, 1		# 6506
	ld	%r5, (%r5)		# 6507
	fmul	%r5, %r2, %r5		# 6508
	fadd	%r4, %r4, %r5		# 6509
	addi	%r5, %r1, 1		# 6510
	sto	%r4, (%r5)		# 6511
	addi	%r4, %r1, 2		# 6512
	ld	%r4, (%r4)		# 6513
	addi	%r3, %r3, 2		# 6514
	ld	%r3, (%r3)		# 6515
	fmul	%r2, %r2, %r3		# 6516
	fadd	%r4, %r4, %r2		# 6517
	addi	%r1, %r1, 2		# 6518
	sto	%r4, (%r1)		# 6519
	addi	%r63, %r63, -1		# 6520
	ld	%r60, (%r63)		# 6521
	jmpu	%r60		# 6522
else.16386:
	addi	%r63, %r63, -1		# 6523
	ld	%r60, (%r63)		# 6524
	jmpu	%r60		# 6525
.globl	iter_trace_diffuse_rays.2806
iter_trace_diffuse_rays.2806:
	sto	%r60, (%r63)		# 6526
	addi	%r63, %r63, 1		# 6527
	movi	%r5, 0		# 6528
	cmpgt	%r5, %r5, %r4		# 6529
	jmpi	%r5, else.16391		# 6530
	add	%r5, %r1, %r4		# 6531
	ld	%r5, (%r5)		# 6532
	ld	%r5, (%r5)		# 6533
	addi	%r6, %r5, 0		# 6534
	ld	%r6, (%r6)		# 6535
	addi	%r7, %r2, 0		# 6536
	ld	%r7, (%r7)		# 6537
	fmul	%r6, %r6, %r7		# 6538
	addi	%r7, %r5, 1		# 6539
	ld	%r7, (%r7)		# 6540
	addi	%r8, %r2, 1		# 6541
	ld	%r8, (%r8)		# 6542
	fmul	%r7, %r7, %r8		# 6543
	fadd	%r6, %r6, %r7		# 6544
	addi	%r5, %r5, 2		# 6545
	ld	%r5, (%r5)		# 6546
	addi	%r7, %r2, 2		# 6547
	ld	%r7, (%r7)		# 6548
	fmul	%r5, %r5, %r7		# 6549
	fadd	%r6, %r6, %r5		# 6550
	movi	%r5, 0		# 6551
	cmpfgt	%r5, %r5, %r6		# 6552
	sto	%r3, (%r63)		# 6553
	sto	%r2, 1(%r63)		# 6554
	sto	%r1, 2(%r63)		# 6555
	sto	%r4, 3(%r63)		# 6556
	jmpi	%r5, else.16392		# 6557
	add	%r5, %r1, %r4		# 6558
	ld	%r5, (%r5)		# 6559
	movhiz	%r7, 274784		# 6560
	finv	%r7, %r7		# 6561
	fmul	%r6, %r6, %r7		# 6562
	mov	%r2, %r6		# 6563
	mov	%r1, %r5		# 6564
	addi	%r63, %r63, 4		# 6565
	call	%r60, trace_diffuse_ray.2803		# 6566
	addi	%r63, %r63, -4		# 6567
	jmpui	if_cont.16393		# 6568
else.16392:
	addi	%r5, %r4, 1		# 6569
	add	%r5, %r1, %r5		# 6570
	ld	%r5, (%r5)		# 6571
	movhiz	%r7, 799072		# 6572
	finv	%r7, %r7		# 6573
	fmul	%r6, %r6, %r7		# 6574
	mov	%r2, %r6		# 6575
	mov	%r1, %r5		# 6576
	addi	%r63, %r63, 4		# 6577
	call	%r60, trace_diffuse_ray.2803		# 6578
	addi	%r63, %r63, -4		# 6579
if_cont.16393:
	ld	%r1, 3(%r63)		# 6580
	addi	%r1, %r1, -2		# 6581
	movi	%r2, 0		# 6582
	cmpgt	%r2, %r2, %r1		# 6583
	jmpi	%r2, else.16394		# 6584
	ld	%r2, 2(%r63)		# 6585
	add	%r3, %r2, %r1		# 6586
	ld	%r3, (%r3)		# 6587
	ld	%r3, (%r3)		# 6588
	addi	%r4, %r3, 0		# 6589
	ld	%r4, (%r4)		# 6590
	ld	%r5, 1(%r63)		# 6591
	addi	%r6, %r5, 0		# 6592
	ld	%r6, (%r6)		# 6593
	fmul	%r4, %r4, %r6		# 6594
	addi	%r6, %r3, 1		# 6595
	ld	%r6, (%r6)		# 6596
	addi	%r7, %r5, 1		# 6597
	ld	%r7, (%r7)		# 6598
	fmul	%r6, %r6, %r7		# 6599
	fadd	%r4, %r4, %r6		# 6600
	addi	%r3, %r3, 2		# 6601
	ld	%r3, (%r3)		# 6602
	addi	%r6, %r5, 2		# 6603
	ld	%r6, (%r6)		# 6604
	fmul	%r3, %r3, %r6		# 6605
	fadd	%r4, %r4, %r3		# 6606
	movi	%r3, 0		# 6607
	cmpfgt	%r3, %r3, %r4		# 6608
	sto	%r1, 4(%r63)		# 6609
	jmpi	%r3, else.16395		# 6610
	add	%r3, %r2, %r1		# 6611
	ld	%r3, (%r3)		# 6612
	movhiz	%r6, 274784		# 6613
	finv	%r6, %r6		# 6614
	fmul	%r4, %r4, %r6		# 6615
	mov	%r2, %r4		# 6616
	mov	%r1, %r3		# 6617
	addi	%r63, %r63, 5		# 6618
	call	%r60, trace_diffuse_ray.2803		# 6619
	addi	%r63, %r63, -5		# 6620
	jmpui	if_cont.16396		# 6621
else.16395:
	addi	%r3, %r1, 1		# 6622
	add	%r3, %r2, %r3		# 6623
	ld	%r3, (%r3)		# 6624
	movhiz	%r6, 799072		# 6625
	finv	%r6, %r6		# 6626
	fmul	%r4, %r4, %r6		# 6627
	mov	%r2, %r4		# 6628
	mov	%r1, %r3		# 6629
	addi	%r63, %r63, 5		# 6630
	call	%r60, trace_diffuse_ray.2803		# 6631
	addi	%r63, %r63, -5		# 6632
if_cont.16396:
	ld	%r1, 4(%r63)		# 6633
	addi	%r4, %r1, -2		# 6634
	ld	%r1, 2(%r63)		# 6635
	ld	%r2, 1(%r63)		# 6636
	ld	%r3, (%r63)		# 6637
	addi	%r63, %r63, -1		# 6638
	ld	%r60, (%r63)		# 6639
	jmpui	iter_trace_diffuse_rays.2806		# 6640
else.16394:
	addi	%r63, %r63, -1		# 6641
	ld	%r60, (%r63)		# 6642
	jmpu	%r60		# 6643
else.16391:
	addi	%r63, %r63, -1		# 6644
	ld	%r60, (%r63)		# 6645
	jmpu	%r60		# 6646
.globl	trace_diffuse_ray_80percent.2815
trace_diffuse_ray_80percent.2815:
	sto	%r60, (%r63)		# 6647
	addi	%r63, %r63, 1		# 6648
	cmpeqi	%r4, %r1, 0		# 6649
	sto	%r2, (%r63)		# 6650
	sto	%r3, 1(%r63)		# 6651
	sto	%r1, 2(%r63)		# 6652
	jmpzi	%r4, else.16399		# 6653
	jmpui	if_cont.16400		# 6654
else.16399:
	movi	%r4, min_caml_dirvecs		# 6655
	addi	%r4, %r4, 0		# 6656
	ld	%r4, (%r4)		# 6657
	movi	%r5, min_caml_startp_fast		# 6658
	addi	%r6, %r3, 0		# 6659
	ld	%r6, (%r6)		# 6660
	addi	%r7, %r5, 0		# 6661
	sto	%r6, (%r7)		# 6662
	addi	%r6, %r3, 1		# 6663
	ld	%r6, (%r6)		# 6664
	addi	%r7, %r5, 1		# 6665
	sto	%r6, (%r7)		# 6666
	addi	%r6, %r3, 2		# 6667
	ld	%r6, (%r6)		# 6668
	addi	%r5, %r5, 2		# 6669
	sto	%r6, (%r5)		# 6670
	movi	%r5, min_caml_n_objects		# 6671
	addi	%r5, %r5, 0		# 6672
	ld	%r5, (%r5)		# 6673
	addi	%r5, %r5, -1		# 6674
	sto	%r4, 3(%r63)		# 6675
	mov	%r2, %r5		# 6676
	mov	%r1, %r3		# 6677
	addi	%r63, %r63, 4		# 6678
	call	%r60, setup_startp_constants.2708		# 6679
	addi	%r63, %r63, -4		# 6680
	movi	%r4, 118		# 6681
	ld	%r1, 3(%r63)		# 6682
	ld	%r2, (%r63)		# 6683
	ld	%r3, 1(%r63)		# 6684
	addi	%r63, %r63, 4		# 6685
	call	%r60, iter_trace_diffuse_rays.2806		# 6686
	addi	%r63, %r63, -4		# 6687
if_cont.16400:
	ld	%r1, 2(%r63)		# 6688
	cmpeqi	%r2, %r1, 1		# 6689
	jmpzi	%r2, else.16401		# 6690
	jmpui	if_cont.16402		# 6691
else.16401:
	movi	%r2, min_caml_dirvecs		# 6692
	addi	%r2, %r2, 1		# 6693
	ld	%r2, (%r2)		# 6694
	movi	%r3, min_caml_startp_fast		# 6695
	ld	%r4, 1(%r63)		# 6696
	addi	%r5, %r4, 0		# 6697
	ld	%r5, (%r5)		# 6698
	addi	%r6, %r3, 0		# 6699
	sto	%r5, (%r6)		# 6700
	addi	%r5, %r4, 1		# 6701
	ld	%r5, (%r5)		# 6702
	addi	%r6, %r3, 1		# 6703
	sto	%r5, (%r6)		# 6704
	addi	%r5, %r4, 2		# 6705
	ld	%r5, (%r5)		# 6706
	addi	%r3, %r3, 2		# 6707
	sto	%r5, (%r3)		# 6708
	movi	%r3, min_caml_n_objects		# 6709
	addi	%r3, %r3, 0		# 6710
	ld	%r3, (%r3)		# 6711
	addi	%r3, %r3, -1		# 6712
	sto	%r2, 4(%r63)		# 6713
	mov	%r2, %r3		# 6714
	mov	%r1, %r4		# 6715
	addi	%r63, %r63, 5		# 6716
	call	%r60, setup_startp_constants.2708		# 6717
	addi	%r63, %r63, -5		# 6718
	movi	%r4, 118		# 6719
	ld	%r1, 4(%r63)		# 6720
	ld	%r2, (%r63)		# 6721
	ld	%r3, 1(%r63)		# 6722
	addi	%r63, %r63, 5		# 6723
	call	%r60, iter_trace_diffuse_rays.2806		# 6724
	addi	%r63, %r63, -5		# 6725
if_cont.16402:
	ld	%r1, 2(%r63)		# 6726
	cmpeqi	%r2, %r1, 2		# 6727
	jmpzi	%r2, else.16403		# 6728
	jmpui	if_cont.16404		# 6729
else.16403:
	movi	%r2, min_caml_dirvecs		# 6730
	addi	%r2, %r2, 2		# 6731
	ld	%r2, (%r2)		# 6732
	movi	%r3, min_caml_startp_fast		# 6733
	ld	%r4, 1(%r63)		# 6734
	addi	%r5, %r4, 0		# 6735
	ld	%r5, (%r5)		# 6736
	addi	%r6, %r3, 0		# 6737
	sto	%r5, (%r6)		# 6738
	addi	%r5, %r4, 1		# 6739
	ld	%r5, (%r5)		# 6740
	addi	%r6, %r3, 1		# 6741
	sto	%r5, (%r6)		# 6742
	addi	%r5, %r4, 2		# 6743
	ld	%r5, (%r5)		# 6744
	addi	%r3, %r3, 2		# 6745
	sto	%r5, (%r3)		# 6746
	movi	%r3, min_caml_n_objects		# 6747
	addi	%r3, %r3, 0		# 6748
	ld	%r3, (%r3)		# 6749
	addi	%r3, %r3, -1		# 6750
	sto	%r2, 5(%r63)		# 6751
	mov	%r2, %r3		# 6752
	mov	%r1, %r4		# 6753
	addi	%r63, %r63, 6		# 6754
	call	%r60, setup_startp_constants.2708		# 6755
	addi	%r63, %r63, -6		# 6756
	movi	%r4, 118		# 6757
	ld	%r1, 5(%r63)		# 6758
	ld	%r2, (%r63)		# 6759
	ld	%r3, 1(%r63)		# 6760
	addi	%r63, %r63, 6		# 6761
	call	%r60, iter_trace_diffuse_rays.2806		# 6762
	addi	%r63, %r63, -6		# 6763
if_cont.16404:
	ld	%r1, 2(%r63)		# 6764
	cmpeqi	%r2, %r1, 3		# 6765
	jmpzi	%r2, else.16405		# 6766
	jmpui	if_cont.16406		# 6767
else.16405:
	movi	%r2, min_caml_dirvecs		# 6768
	addi	%r2, %r2, 3		# 6769
	ld	%r2, (%r2)		# 6770
	movi	%r3, min_caml_startp_fast		# 6771
	ld	%r4, 1(%r63)		# 6772
	addi	%r5, %r4, 0		# 6773
	ld	%r5, (%r5)		# 6774
	addi	%r6, %r3, 0		# 6775
	sto	%r5, (%r6)		# 6776
	addi	%r5, %r4, 1		# 6777
	ld	%r5, (%r5)		# 6778
	addi	%r6, %r3, 1		# 6779
	sto	%r5, (%r6)		# 6780
	addi	%r5, %r4, 2		# 6781
	ld	%r5, (%r5)		# 6782
	addi	%r3, %r3, 2		# 6783
	sto	%r5, (%r3)		# 6784
	movi	%r3, min_caml_n_objects		# 6785
	addi	%r3, %r3, 0		# 6786
	ld	%r3, (%r3)		# 6787
	addi	%r3, %r3, -1		# 6788
	sto	%r2, 6(%r63)		# 6789
	mov	%r2, %r3		# 6790
	mov	%r1, %r4		# 6791
	addi	%r63, %r63, 7		# 6792
	call	%r60, setup_startp_constants.2708		# 6793
	addi	%r63, %r63, -7		# 6794
	movi	%r4, 118		# 6795
	ld	%r1, 6(%r63)		# 6796
	ld	%r2, (%r63)		# 6797
	ld	%r3, 1(%r63)		# 6798
	addi	%r63, %r63, 7		# 6799
	call	%r60, iter_trace_diffuse_rays.2806		# 6800
	addi	%r63, %r63, -7		# 6801
if_cont.16406:
	ld	%r1, 2(%r63)		# 6802
	cmpeqi	%r1, %r1, 4		# 6803
	jmpzi	%r1, else.16407		# 6804
	addi	%r63, %r63, -1		# 6805
	ld	%r60, (%r63)		# 6806
	jmpu	%r60		# 6807
else.16407:
	movi	%r1, min_caml_dirvecs		# 6808
	addi	%r1, %r1, 4		# 6809
	ld	%r1, (%r1)		# 6810
	movi	%r2, min_caml_startp_fast		# 6811
	ld	%r3, 1(%r63)		# 6812
	addi	%r4, %r3, 0		# 6813
	ld	%r4, (%r4)		# 6814
	addi	%r5, %r2, 0		# 6815
	sto	%r4, (%r5)		# 6816
	addi	%r4, %r3, 1		# 6817
	ld	%r4, (%r4)		# 6818
	addi	%r5, %r2, 1		# 6819
	sto	%r4, (%r5)		# 6820
	addi	%r4, %r3, 2		# 6821
	ld	%r4, (%r4)		# 6822
	addi	%r2, %r2, 2		# 6823
	sto	%r4, (%r2)		# 6824
	movi	%r2, min_caml_n_objects		# 6825
	addi	%r2, %r2, 0		# 6826
	ld	%r2, (%r2)		# 6827
	addi	%r2, %r2, -1		# 6828
	sto	%r1, 7(%r63)		# 6829
	mov	%r1, %r3		# 6830
	addi	%r63, %r63, 8		# 6831
	call	%r60, setup_startp_constants.2708		# 6832
	addi	%r63, %r63, -8		# 6833
	movi	%r4, 118		# 6834
	ld	%r1, 7(%r63)		# 6835
	ld	%r2, (%r63)		# 6836
	ld	%r3, 1(%r63)		# 6837
	addi	%r63, %r63, -1		# 6838
	ld	%r60, (%r63)		# 6839
	jmpui	iter_trace_diffuse_rays.2806		# 6840
.globl	calc_diffuse_using_5points.2822
calc_diffuse_using_5points.2822:
	sto	%r60, (%r63)		# 6841
	addi	%r63, %r63, 1		# 6842
	add	%r2, %r2, %r1		# 6843
	ld	%r2, (%r2)		# 6844
	ld	%r2, 5(%r2)		# 6845
	addi	%r6, %r1, -1		# 6846
	add	%r6, %r3, %r6		# 6847
	ld	%r6, (%r6)		# 6848
	ld	%r6, 5(%r6)		# 6849
	add	%r7, %r3, %r1		# 6850
	ld	%r7, (%r7)		# 6851
	ld	%r7, 5(%r7)		# 6852
	addi	%r8, %r1, 1		# 6853
	add	%r8, %r3, %r8		# 6854
	ld	%r8, (%r8)		# 6855
	ld	%r8, 5(%r8)		# 6856
	add	%r4, %r4, %r1		# 6857
	ld	%r4, (%r4)		# 6858
	ld	%r4, 5(%r4)		# 6859
	movi	%r9, min_caml_diffuse_ray		# 6860
	add	%r2, %r2, %r5		# 6861
	ld	%r2, (%r2)		# 6862
	addi	%r10, %r2, 0		# 6863
	ld	%r10, (%r10)		# 6864
	addi	%r11, %r9, 0		# 6865
	sto	%r10, (%r11)		# 6866
	addi	%r10, %r2, 1		# 6867
	ld	%r10, (%r10)		# 6868
	addi	%r11, %r9, 1		# 6869
	sto	%r10, (%r11)		# 6870
	addi	%r2, %r2, 2		# 6871
	ld	%r2, (%r2)		# 6872
	addi	%r9, %r9, 2		# 6873
	sto	%r2, (%r9)		# 6874
	movi	%r2, min_caml_diffuse_ray		# 6875
	add	%r6, %r6, %r5		# 6876
	ld	%r6, (%r6)		# 6877
	addi	%r9, %r2, 0		# 6878
	ld	%r9, (%r9)		# 6879
	addi	%r10, %r6, 0		# 6880
	ld	%r10, (%r10)		# 6881
	fadd	%r9, %r9, %r10		# 6882
	addi	%r10, %r2, 0		# 6883
	sto	%r9, (%r10)		# 6884
	addi	%r9, %r2, 1		# 6885
	ld	%r9, (%r9)		# 6886
	addi	%r10, %r6, 1		# 6887
	ld	%r10, (%r10)		# 6888
	fadd	%r9, %r9, %r10		# 6889
	addi	%r10, %r2, 1		# 6890
	sto	%r9, (%r10)		# 6891
	addi	%r9, %r2, 2		# 6892
	ld	%r9, (%r9)		# 6893
	addi	%r6, %r6, 2		# 6894
	ld	%r6, (%r6)		# 6895
	fadd	%r9, %r9, %r6		# 6896
	addi	%r2, %r2, 2		# 6897
	sto	%r9, (%r2)		# 6898
	movi	%r2, min_caml_diffuse_ray		# 6899
	add	%r7, %r7, %r5		# 6900
	ld	%r6, (%r7)		# 6901
	addi	%r7, %r2, 0		# 6902
	ld	%r7, (%r7)		# 6903
	addi	%r9, %r6, 0		# 6904
	ld	%r9, (%r9)		# 6905
	fadd	%r7, %r7, %r9		# 6906
	addi	%r9, %r2, 0		# 6907
	sto	%r7, (%r9)		# 6908
	addi	%r7, %r2, 1		# 6909
	ld	%r7, (%r7)		# 6910
	addi	%r9, %r6, 1		# 6911
	ld	%r9, (%r9)		# 6912
	fadd	%r7, %r7, %r9		# 6913
	addi	%r9, %r2, 1		# 6914
	sto	%r7, (%r9)		# 6915
	addi	%r7, %r2, 2		# 6916
	ld	%r7, (%r7)		# 6917
	addi	%r6, %r6, 2		# 6918
	ld	%r6, (%r6)		# 6919
	fadd	%r7, %r7, %r6		# 6920
	addi	%r2, %r2, 2		# 6921
	sto	%r7, (%r2)		# 6922
	movi	%r2, min_caml_diffuse_ray		# 6923
	add	%r8, %r8, %r5		# 6924
	ld	%r6, (%r8)		# 6925
	addi	%r7, %r2, 0		# 6926
	ld	%r7, (%r7)		# 6927
	addi	%r8, %r6, 0		# 6928
	ld	%r8, (%r8)		# 6929
	fadd	%r7, %r7, %r8		# 6930
	addi	%r8, %r2, 0		# 6931
	sto	%r7, (%r8)		# 6932
	addi	%r7, %r2, 1		# 6933
	ld	%r7, (%r7)		# 6934
	addi	%r8, %r6, 1		# 6935
	ld	%r8, (%r8)		# 6936
	fadd	%r7, %r7, %r8		# 6937
	addi	%r8, %r2, 1		# 6938
	sto	%r7, (%r8)		# 6939
	addi	%r7, %r2, 2		# 6940
	ld	%r7, (%r7)		# 6941
	addi	%r6, %r6, 2		# 6942
	ld	%r6, (%r6)		# 6943
	fadd	%r7, %r7, %r6		# 6944
	addi	%r2, %r2, 2		# 6945
	sto	%r7, (%r2)		# 6946
	movi	%r2, min_caml_diffuse_ray		# 6947
	add	%r4, %r4, %r5		# 6948
	ld	%r4, (%r4)		# 6949
	addi	%r6, %r2, 0		# 6950
	ld	%r6, (%r6)		# 6951
	addi	%r7, %r4, 0		# 6952
	ld	%r7, (%r7)		# 6953
	fadd	%r6, %r6, %r7		# 6954
	addi	%r7, %r2, 0		# 6955
	sto	%r6, (%r7)		# 6956
	addi	%r6, %r2, 1		# 6957
	ld	%r6, (%r6)		# 6958
	addi	%r7, %r4, 1		# 6959
	ld	%r7, (%r7)		# 6960
	fadd	%r6, %r6, %r7		# 6961
	addi	%r7, %r2, 1		# 6962
	sto	%r6, (%r7)		# 6963
	addi	%r6, %r2, 2		# 6964
	ld	%r6, (%r6)		# 6965
	addi	%r4, %r4, 2		# 6966
	ld	%r4, (%r4)		# 6967
	fadd	%r6, %r6, %r4		# 6968
	addi	%r2, %r2, 2		# 6969
	sto	%r6, (%r2)		# 6970
	add	%r3, %r3, %r1		# 6971
	ld	%r1, (%r3)		# 6972
	ld	%r1, 4(%r1)		# 6973
	movi	%r2, min_caml_rgb		# 6974
	add	%r1, %r1, %r5		# 6975
	ld	%r1, (%r1)		# 6976
	movi	%r3, min_caml_diffuse_ray		# 6977
	sto	%r2, (%r63)		# 6978
	mov	%r2, %r1		# 6979
	ld	%r1, (%r63)		# 6980
	addi	%r63, %r63, -1		# 6981
	ld	%r60, (%r63)		# 6982
	jmpui	vecaccumv.2512		# 6983
.globl	do_without_neighbors.2828
do_without_neighbors.2828:
	sto	%r60, (%r63)		# 6984
	addi	%r63, %r63, 1		# 6985
	cmpgti	%r3, %r2, 4		# 6986
	jmpi	%r3, else.16409		# 6987
	ld	%r3, 2(%r1)		# 6988
	movi	%r4, 0		# 6989
	add	%r3, %r3, %r2		# 6990
	ld	%r3, (%r3)		# 6991
	cmpgt	%r4, %r4, %r3		# 6992
	jmpi	%r4, else.16410		# 6993
	ld	%r3, 3(%r1)		# 6994
	add	%r3, %r3, %r2		# 6995
	ld	%r3, (%r3)		# 6996
	cmpeqi	%r3, %r3, 0		# 6997
	sto	%r1, (%r63)		# 6998
	sto	%r2, 1(%r63)		# 6999
	jmpzi	%r3, else.16411		# 7000
	jmpui	if_cont.16412		# 7001
else.16411:
	ld	%r3, 5(%r1)		# 7002
	ld	%r4, 7(%r1)		# 7003
	ld	%r5, 1(%r1)		# 7004
	ld	%r6, 4(%r1)		# 7005
	movi	%r7, min_caml_diffuse_ray		# 7006
	add	%r3, %r3, %r2		# 7007
	ld	%r3, (%r3)		# 7008
	addi	%r8, %r3, 0		# 7009
	ld	%r8, (%r8)		# 7010
	addi	%r9, %r7, 0		# 7011
	sto	%r8, (%r9)		# 7012
	addi	%r8, %r3, 1		# 7013
	ld	%r8, (%r8)		# 7014
	addi	%r9, %r7, 1		# 7015
	sto	%r8, (%r9)		# 7016
	addi	%r3, %r3, 2		# 7017
	ld	%r3, (%r3)		# 7018
	addi	%r7, %r7, 2		# 7019
	sto	%r3, (%r7)		# 7020
	ld	%r3, 6(%r1)		# 7021
	addi	%r3, %r3, 0		# 7022
	ld	%r3, (%r3)		# 7023
	add	%r4, %r4, %r2		# 7024
	ld	%r4, (%r4)		# 7025
	add	%r5, %r5, %r2		# 7026
	ld	%r5, (%r5)		# 7027
	sto	%r6, 2(%r63)		# 7028
	mov	%r2, %r4		# 7029
	mov	%r1, %r3		# 7030
	mov	%r3, %r5		# 7031
	addi	%r63, %r63, 3		# 7032
	call	%r60, trace_diffuse_ray_80percent.2815		# 7033
	addi	%r63, %r63, -3		# 7034
	movi	%r1, min_caml_rgb		# 7035
	ld	%r2, 1(%r63)		# 7036
	ld	%r3, 2(%r63)		# 7037
	add	%r3, %r3, %r2		# 7038
	ld	%r3, (%r3)		# 7039
	movi	%r4, min_caml_diffuse_ray		# 7040
	mov	%r2, %r3		# 7041
	mov	%r3, %r4		# 7042
	addi	%r63, %r63, 3		# 7043
	call	%r60, vecaccumv.2512		# 7044
	addi	%r63, %r63, -3		# 7045
if_cont.16412:
	ld	%r1, 1(%r63)		# 7046
	addi	%r1, %r1, 1		# 7047
	cmpgti	%r2, %r1, 4		# 7048
	jmpi	%r2, else.16413		# 7049
	ld	%r2, (%r63)		# 7050
	ld	%r3, 2(%r2)		# 7051
	movi	%r4, 0		# 7052
	add	%r3, %r3, %r1		# 7053
	ld	%r3, (%r3)		# 7054
	cmpgt	%r4, %r4, %r3		# 7055
	jmpi	%r4, else.16414		# 7056
	ld	%r3, 3(%r2)		# 7057
	add	%r3, %r3, %r1		# 7058
	ld	%r3, (%r3)		# 7059
	cmpeqi	%r3, %r3, 0		# 7060
	sto	%r1, 3(%r63)		# 7061
	jmpzi	%r3, else.16415		# 7062
	jmpui	if_cont.16416		# 7063
else.16415:
	ld	%r3, 5(%r2)		# 7064
	ld	%r4, 7(%r2)		# 7065
	ld	%r5, 1(%r2)		# 7066
	ld	%r6, 4(%r2)		# 7067
	movi	%r7, min_caml_diffuse_ray		# 7068
	add	%r3, %r3, %r1		# 7069
	ld	%r3, (%r3)		# 7070
	addi	%r8, %r3, 0		# 7071
	ld	%r8, (%r8)		# 7072
	addi	%r9, %r7, 0		# 7073
	sto	%r8, (%r9)		# 7074
	addi	%r8, %r3, 1		# 7075
	ld	%r8, (%r8)		# 7076
	addi	%r9, %r7, 1		# 7077
	sto	%r8, (%r9)		# 7078
	addi	%r3, %r3, 2		# 7079
	ld	%r3, (%r3)		# 7080
	addi	%r7, %r7, 2		# 7081
	sto	%r3, (%r7)		# 7082
	ld	%r3, 6(%r2)		# 7083
	addi	%r3, %r3, 0		# 7084
	ld	%r3, (%r3)		# 7085
	add	%r4, %r4, %r1		# 7086
	ld	%r4, (%r4)		# 7087
	add	%r5, %r5, %r1		# 7088
	ld	%r5, (%r5)		# 7089
	sto	%r6, 4(%r63)		# 7090
	mov	%r2, %r4		# 7091
	mov	%r1, %r3		# 7092
	mov	%r3, %r5		# 7093
	addi	%r63, %r63, 5		# 7094
	call	%r60, trace_diffuse_ray_80percent.2815		# 7095
	addi	%r63, %r63, -5		# 7096
	movi	%r1, min_caml_rgb		# 7097
	ld	%r2, 3(%r63)		# 7098
	ld	%r3, 4(%r63)		# 7099
	add	%r3, %r3, %r2		# 7100
	ld	%r3, (%r3)		# 7101
	movi	%r4, min_caml_diffuse_ray		# 7102
	mov	%r2, %r3		# 7103
	mov	%r3, %r4		# 7104
	addi	%r63, %r63, 5		# 7105
	call	%r60, vecaccumv.2512		# 7106
	addi	%r63, %r63, -5		# 7107
if_cont.16416:
	ld	%r1, 3(%r63)		# 7108
	addi	%r2, %r1, 1		# 7109
	ld	%r1, (%r63)		# 7110
	addi	%r63, %r63, -1		# 7111
	ld	%r60, (%r63)		# 7112
	jmpui	do_without_neighbors.2828		# 7113
else.16414:
	addi	%r63, %r63, -1		# 7114
	ld	%r60, (%r63)		# 7115
	jmpu	%r60		# 7116
else.16413:
	addi	%r63, %r63, -1		# 7117
	ld	%r60, (%r63)		# 7118
	jmpu	%r60		# 7119
else.16410:
	addi	%r63, %r63, -1		# 7120
	ld	%r60, (%r63)		# 7121
	jmpu	%r60		# 7122
else.16409:
	addi	%r63, %r63, -1		# 7123
	ld	%r60, (%r63)		# 7124
	jmpu	%r60		# 7125
.globl	try_exploit_neighbors.2844
try_exploit_neighbors.2844:
	sto	%r60, (%r63)		# 7126
	addi	%r63, %r63, 1		# 7127
	add	%r7, %r4, %r1		# 7128
	ld	%r7, (%r7)		# 7129
	cmpgti	%r8, %r6, 4		# 7130
	jmpi	%r8, else.16421		# 7131
	movi	%r8, 0		# 7132
	ld	%r9, 2(%r7)		# 7133
	add	%r9, %r9, %r6		# 7134
	ld	%r9, (%r9)		# 7135
	cmpgt	%r8, %r8, %r9		# 7136
	jmpi	%r8, else.16422		# 7137
	add	%r8, %r4, %r1		# 7138
	ld	%r8, (%r8)		# 7139
	ld	%r8, 2(%r8)		# 7140
	add	%r8, %r8, %r6		# 7141
	ld	%r8, (%r8)		# 7142
	add	%r9, %r3, %r1		# 7143
	ld	%r9, (%r9)		# 7144
	ld	%r9, 2(%r9)		# 7145
	add	%r9, %r9, %r6		# 7146
	ld	%r9, (%r9)		# 7147
	cmpeq	%r9, %r9, %r8		# 7148
	jmpzi	%r9, else.16423		# 7149
	add	%r9, %r5, %r1		# 7150
	ld	%r9, (%r9)		# 7151
	ld	%r9, 2(%r9)		# 7152
	add	%r9, %r9, %r6		# 7153
	ld	%r9, (%r9)		# 7154
	cmpeq	%r9, %r9, %r8		# 7155
	jmpzi	%r9, else.16425		# 7156
	addi	%r9, %r1, -1		# 7157
	add	%r9, %r4, %r9		# 7158
	ld	%r9, (%r9)		# 7159
	ld	%r9, 2(%r9)		# 7160
	add	%r9, %r9, %r6		# 7161
	ld	%r9, (%r9)		# 7162
	cmpeq	%r9, %r9, %r8		# 7163
	jmpzi	%r9, else.16427		# 7164
	addi	%r9, %r1, 1		# 7165
	add	%r9, %r4, %r9		# 7166
	ld	%r9, (%r9)		# 7167
	ld	%r9, 2(%r9)		# 7168
	add	%r9, %r9, %r6		# 7169
	ld	%r9, (%r9)		# 7170
	cmpeq	%r9, %r9, %r8		# 7171
	jmpzi	%r9, else.16429		# 7172
	movi	%r8, 1		# 7173
	jmpui	if_cont.16430		# 7174
else.16429:
	movi	%r8, 0		# 7175
if_cont.16430:
	jmpui	if_cont.16428		# 7176
else.16427:
	movi	%r8, 0		# 7177
if_cont.16428:
	jmpui	if_cont.16426		# 7178
else.16425:
	movi	%r8, 0		# 7179
if_cont.16426:
	jmpui	if_cont.16424		# 7180
else.16423:
	movi	%r8, 0		# 7181
if_cont.16424:
	cmpeqi	%r8, %r8, 0		# 7182
	jmpzi	%r8, else.16431		# 7183
	add	%r4, %r4, %r1		# 7184
	ld	%r1, (%r4)		# 7185
	cmpgti	%r2, %r6, 4		# 7186
	jmpi	%r2, else.16432		# 7187
	ld	%r2, 2(%r1)		# 7188
	movi	%r3, 0		# 7189
	add	%r2, %r2, %r6		# 7190
	ld	%r2, (%r2)		# 7191
	cmpgt	%r3, %r3, %r2		# 7192
	jmpi	%r3, else.16433		# 7193
	ld	%r2, 3(%r1)		# 7194
	add	%r2, %r2, %r6		# 7195
	ld	%r2, (%r2)		# 7196
	cmpeqi	%r2, %r2, 0		# 7197
	sto	%r1, (%r63)		# 7198
	sto	%r6, 1(%r63)		# 7199
	jmpzi	%r2, else.16434		# 7200
	jmpui	if_cont.16435		# 7201
else.16434:
	ld	%r2, 5(%r1)		# 7202
	ld	%r3, 7(%r1)		# 7203
	ld	%r4, 1(%r1)		# 7204
	ld	%r5, 4(%r1)		# 7205
	movi	%r7, min_caml_diffuse_ray		# 7206
	add	%r2, %r2, %r6		# 7207
	ld	%r2, (%r2)		# 7208
	addi	%r8, %r2, 0		# 7209
	ld	%r8, (%r8)		# 7210
	addi	%r9, %r7, 0		# 7211
	sto	%r8, (%r9)		# 7212
	addi	%r8, %r2, 1		# 7213
	ld	%r8, (%r8)		# 7214
	addi	%r9, %r7, 1		# 7215
	sto	%r8, (%r9)		# 7216
	addi	%r2, %r2, 2		# 7217
	ld	%r2, (%r2)		# 7218
	addi	%r7, %r7, 2		# 7219
	sto	%r2, (%r7)		# 7220
	ld	%r2, 6(%r1)		# 7221
	addi	%r2, %r2, 0		# 7222
	ld	%r2, (%r2)		# 7223
	add	%r3, %r3, %r6		# 7224
	ld	%r3, (%r3)		# 7225
	add	%r4, %r4, %r6		# 7226
	ld	%r4, (%r4)		# 7227
	sto	%r5, 2(%r63)		# 7228
	mov	%r1, %r2		# 7229
	mov	%r2, %r3		# 7230
	mov	%r3, %r4		# 7231
	addi	%r63, %r63, 3		# 7232
	call	%r60, trace_diffuse_ray_80percent.2815		# 7233
	addi	%r63, %r63, -3		# 7234
	movi	%r1, min_caml_rgb		# 7235
	ld	%r2, 1(%r63)		# 7236
	ld	%r3, 2(%r63)		# 7237
	add	%r3, %r3, %r2		# 7238
	ld	%r3, (%r3)		# 7239
	movi	%r4, min_caml_diffuse_ray		# 7240
	mov	%r2, %r3		# 7241
	mov	%r3, %r4		# 7242
	addi	%r63, %r63, 3		# 7243
	call	%r60, vecaccumv.2512		# 7244
	addi	%r63, %r63, -3		# 7245
if_cont.16435:
	ld	%r1, 1(%r63)		# 7246
	addi	%r2, %r1, 1		# 7247
	ld	%r1, (%r63)		# 7248
	addi	%r63, %r63, -1		# 7249
	ld	%r60, (%r63)		# 7250
	jmpui	do_without_neighbors.2828		# 7251
else.16433:
	addi	%r63, %r63, -1		# 7252
	ld	%r60, (%r63)		# 7253
	jmpu	%r60		# 7254
else.16432:
	addi	%r63, %r63, -1		# 7255
	ld	%r60, (%r63)		# 7256
	jmpu	%r60		# 7257
else.16431:
	ld	%r7, 3(%r7)		# 7258
	add	%r7, %r7, %r6		# 7259
	ld	%r7, (%r7)		# 7260
	cmpeqi	%r7, %r7, 0		# 7261
	sto	%r2, 3(%r63)		# 7262
	sto	%r5, 4(%r63)		# 7263
	sto	%r3, 5(%r63)		# 7264
	sto	%r1, 6(%r63)		# 7265
	sto	%r4, 7(%r63)		# 7266
	sto	%r6, 1(%r63)		# 7267
	jmpzi	%r7, else.16438		# 7268
	jmpui	if_cont.16439		# 7269
else.16438:
	mov	%r2, %r3		# 7270
	mov	%r3, %r4		# 7271
	mov	%r4, %r5		# 7272
	mov	%r5, %r6		# 7273
	addi	%r63, %r63, 8		# 7274
	call	%r60, calc_diffuse_using_5points.2822		# 7275
	addi	%r63, %r63, -8		# 7276
if_cont.16439:
	ld	%r1, 1(%r63)		# 7277
	addi	%r2, %r1, 1		# 7278
	ld	%r1, 6(%r63)		# 7279
	ld	%r3, 7(%r63)		# 7280
	add	%r4, %r3, %r1		# 7281
	ld	%r4, (%r4)		# 7282
	cmpgti	%r5, %r2, 4		# 7283
	jmpi	%r5, else.16440		# 7284
	movi	%r5, 0		# 7285
	ld	%r6, 2(%r4)		# 7286
	add	%r6, %r6, %r2		# 7287
	ld	%r6, (%r6)		# 7288
	cmpgt	%r5, %r5, %r6		# 7289
	jmpi	%r5, else.16441		# 7290
	add	%r5, %r3, %r1		# 7291
	ld	%r5, (%r5)		# 7292
	ld	%r5, 2(%r5)		# 7293
	add	%r5, %r5, %r2		# 7294
	ld	%r5, (%r5)		# 7295
	ld	%r6, 5(%r63)		# 7296
	add	%r7, %r6, %r1		# 7297
	ld	%r7, (%r7)		# 7298
	ld	%r7, 2(%r7)		# 7299
	add	%r7, %r7, %r2		# 7300
	ld	%r7, (%r7)		# 7301
	cmpeq	%r7, %r7, %r5		# 7302
	jmpzi	%r7, else.16442		# 7303
	ld	%r7, 4(%r63)		# 7304
	add	%r8, %r7, %r1		# 7305
	ld	%r8, (%r8)		# 7306
	ld	%r8, 2(%r8)		# 7307
	add	%r8, %r8, %r2		# 7308
	ld	%r8, (%r8)		# 7309
	cmpeq	%r8, %r8, %r5		# 7310
	jmpzi	%r8, else.16444		# 7311
	addi	%r8, %r1, -1		# 7312
	add	%r8, %r3, %r8		# 7313
	ld	%r8, (%r8)		# 7314
	ld	%r8, 2(%r8)		# 7315
	add	%r8, %r8, %r2		# 7316
	ld	%r8, (%r8)		# 7317
	cmpeq	%r8, %r8, %r5		# 7318
	jmpzi	%r8, else.16446		# 7319
	addi	%r8, %r1, 1		# 7320
	add	%r8, %r3, %r8		# 7321
	ld	%r8, (%r8)		# 7322
	ld	%r8, 2(%r8)		# 7323
	add	%r8, %r8, %r2		# 7324
	ld	%r8, (%r8)		# 7325
	cmpeq	%r8, %r8, %r5		# 7326
	jmpzi	%r8, else.16448		# 7327
	movi	%r5, 1		# 7328
	jmpui	if_cont.16449		# 7329
else.16448:
	movi	%r5, 0		# 7330
if_cont.16449:
	jmpui	if_cont.16447		# 7331
else.16446:
	movi	%r5, 0		# 7332
if_cont.16447:
	jmpui	if_cont.16445		# 7333
else.16444:
	movi	%r5, 0		# 7334
if_cont.16445:
	jmpui	if_cont.16443		# 7335
else.16442:
	movi	%r5, 0		# 7336
if_cont.16443:
	cmpeqi	%r5, %r5, 0		# 7337
	jmpzi	%r5, else.16450		# 7338
	add	%r3, %r3, %r1		# 7339
	ld	%r1, (%r3)		# 7340
	addi	%r63, %r63, -1		# 7341
	ld	%r60, (%r63)		# 7342
	jmpui	do_without_neighbors.2828		# 7343
else.16450:
	ld	%r4, 3(%r4)		# 7344
	add	%r4, %r4, %r2		# 7345
	ld	%r4, (%r4)		# 7346
	cmpeqi	%r4, %r4, 0		# 7347
	sto	%r2, 8(%r63)		# 7348
	jmpzi	%r4, else.16451		# 7349
	jmpui	if_cont.16452		# 7350
else.16451:
	ld	%r4, 4(%r63)		# 7351
	mov	%r5, %r2		# 7352
	mov	%r2, %r6		# 7353
	addi	%r63, %r63, 9		# 7354
	call	%r60, calc_diffuse_using_5points.2822		# 7355
	addi	%r63, %r63, -9		# 7356
if_cont.16452:
	ld	%r1, 8(%r63)		# 7357
	addi	%r6, %r1, 1		# 7358
	ld	%r1, 6(%r63)		# 7359
	ld	%r2, 3(%r63)		# 7360
	ld	%r3, 5(%r63)		# 7361
	ld	%r4, 7(%r63)		# 7362
	ld	%r5, 4(%r63)		# 7363
	addi	%r63, %r63, -1		# 7364
	ld	%r60, (%r63)		# 7365
	jmpui	try_exploit_neighbors.2844		# 7366
else.16441:
	addi	%r63, %r63, -1		# 7367
	ld	%r60, (%r63)		# 7368
	jmpu	%r60		# 7369
else.16440:
	addi	%r63, %r63, -1		# 7370
	ld	%r60, (%r63)		# 7371
	jmpu	%r60		# 7372
else.16422:
	addi	%r63, %r63, -1		# 7373
	ld	%r60, (%r63)		# 7374
	jmpu	%r60		# 7375
else.16421:
	addi	%r63, %r63, -1		# 7376
	ld	%r60, (%r63)		# 7377
	jmpu	%r60		# 7378
.globl	write_rgb.2855
write_rgb.2855:
	sto	%r60, (%r63)		# 7379
	addi	%r63, %r63, 1		# 7380
	movi	%r1, min_caml_rgb		# 7381
	addi	%r1, %r1, 0		# 7382
	ld	%r1, (%r1)		# 7383
	call	%r60, min_caml_int_of_float		# 7384
	cmpgti	%r2, %r1, 255		# 7385
	jmpi	%r2, else.16457		# 7386
	movi	%r2, 0		# 7387
	cmpgt	%r2, %r2, %r1		# 7388
	jmpi	%r2, else.16459		# 7389
	jmpui	if_cont.16460		# 7390
else.16459:
	movi	%r1, 0		# 7391
if_cont.16460:
	jmpui	if_cont.16458		# 7392
else.16457:
	movi	%r1, 255		# 7393
if_cont.16458:
	call	%r60, min_caml_print_int		# 7394
	movi	%r1, 32		# 7395
	out	%r1		# 7396
	movi	%r1, min_caml_rgb		# 7397
	addi	%r1, %r1, 1		# 7398
	ld	%r1, (%r1)		# 7399
	call	%r60, min_caml_int_of_float		# 7400
	cmpgti	%r2, %r1, 255		# 7401
	jmpi	%r2, else.16461		# 7402
	movi	%r2, 0		# 7403
	cmpgt	%r2, %r2, %r1		# 7404
	jmpi	%r2, else.16463		# 7405
	jmpui	if_cont.16464		# 7406
else.16463:
	movi	%r1, 0		# 7407
if_cont.16464:
	jmpui	if_cont.16462		# 7408
else.16461:
	movi	%r1, 255		# 7409
if_cont.16462:
	call	%r60, min_caml_print_int		# 7410
	movi	%r1, 32		# 7411
	out	%r1		# 7412
	movi	%r1, min_caml_rgb		# 7413
	addi	%r1, %r1, 2		# 7414
	ld	%r1, (%r1)		# 7415
	call	%r60, min_caml_int_of_float		# 7416
	cmpgti	%r2, %r1, 255		# 7417
	jmpi	%r2, else.16465		# 7418
	movi	%r2, 0		# 7419
	cmpgt	%r2, %r2, %r1		# 7420
	jmpi	%r2, else.16467		# 7421
	jmpui	if_cont.16468		# 7422
else.16467:
	movi	%r1, 0		# 7423
if_cont.16468:
	jmpui	if_cont.16466		# 7424
else.16465:
	movi	%r1, 255		# 7425
if_cont.16466:
	call	%r60, min_caml_print_int		# 7426
	movi	%r1, 10		# 7427
	out	%r1		# 7428
	addi	%r63, %r63, -1		# 7429
	ld	%r60, (%r63)		# 7430
	jmpu	%r60		# 7431
.globl	pretrace_diffuse_rays.2857
pretrace_diffuse_rays.2857:
	sto	%r60, (%r63)		# 7432
	addi	%r63, %r63, 1		# 7433
	cmpgti	%r3, %r2, 4		# 7434
	jmpi	%r3, else.16470		# 7435
	ld	%r3, 2(%r1)		# 7436
	add	%r3, %r3, %r2		# 7437
	ld	%r3, (%r3)		# 7438
	movi	%r4, 0		# 7439
	cmpgt	%r4, %r4, %r3		# 7440
	jmpi	%r4, else.16471		# 7441
	ld	%r3, 3(%r1)		# 7442
	add	%r3, %r3, %r2		# 7443
	ld	%r3, (%r3)		# 7444
	cmpeqi	%r3, %r3, 0		# 7445
	sto	%r2, (%r63)		# 7446
	jmpzi	%r3, else.16472		# 7447
	jmpui	if_cont.16473		# 7448
else.16472:
	ld	%r3, 6(%r1)		# 7449
	addi	%r3, %r3, 0		# 7450
	ld	%r3, (%r3)		# 7451
	movi	%r4, min_caml_diffuse_ray		# 7452
	movi	%r5, 0		# 7453
	addi	%r6, %r4, 0		# 7454
	sto	%r5, (%r6)		# 7455
	addi	%r6, %r4, 1		# 7456
	sto	%r5, (%r6)		# 7457
	addi	%r4, %r4, 2		# 7458
	sto	%r5, (%r4)		# 7459
	ld	%r4, 7(%r1)		# 7460
	ld	%r5, 1(%r1)		# 7461
	movi	%r6, min_caml_dirvecs		# 7462
	add	%r6, %r6, %r3		# 7463
	ld	%r3, (%r6)		# 7464
	add	%r4, %r4, %r2		# 7465
	ld	%r4, (%r4)		# 7466
	add	%r5, %r5, %r2		# 7467
	ld	%r5, (%r5)		# 7468
	movi	%r6, min_caml_startp_fast		# 7469
	addi	%r7, %r5, 0		# 7470
	ld	%r7, (%r7)		# 7471
	addi	%r8, %r6, 0		# 7472
	sto	%r7, (%r8)		# 7473
	addi	%r7, %r5, 1		# 7474
	ld	%r7, (%r7)		# 7475
	addi	%r8, %r6, 1		# 7476
	sto	%r7, (%r8)		# 7477
	addi	%r7, %r5, 2		# 7478
	ld	%r7, (%r7)		# 7479
	addi	%r6, %r6, 2		# 7480
	sto	%r7, (%r6)		# 7481
	movi	%r6, min_caml_n_objects		# 7482
	addi	%r6, %r6, 0		# 7483
	ld	%r6, (%r6)		# 7484
	addi	%r6, %r6, -1		# 7485
	sto	%r1, 1(%r63)		# 7486
	sto	%r5, 2(%r63)		# 7487
	sto	%r4, 3(%r63)		# 7488
	sto	%r3, 4(%r63)		# 7489
	mov	%r2, %r6		# 7490
	mov	%r1, %r5		# 7491
	addi	%r63, %r63, 5		# 7492
	call	%r60, setup_startp_constants.2708		# 7493
	addi	%r63, %r63, -5		# 7494
	movi	%r4, 118		# 7495
	ld	%r1, 4(%r63)		# 7496
	ld	%r2, 3(%r63)		# 7497
	ld	%r3, 2(%r63)		# 7498
	addi	%r63, %r63, 5		# 7499
	call	%r60, iter_trace_diffuse_rays.2806		# 7500
	addi	%r63, %r63, -5		# 7501
	ld	%r1, 1(%r63)		# 7502
	ld	%r2, 5(%r1)		# 7503
	ld	%r3, (%r63)		# 7504
	add	%r2, %r2, %r3		# 7505
	ld	%r2, (%r2)		# 7506
	movi	%r4, min_caml_diffuse_ray		# 7507
	addi	%r5, %r4, 0		# 7508
	ld	%r5, (%r5)		# 7509
	addi	%r6, %r2, 0		# 7510
	sto	%r5, (%r6)		# 7511
	addi	%r5, %r4, 1		# 7512
	ld	%r5, (%r5)		# 7513
	addi	%r6, %r2, 1		# 7514
	sto	%r5, (%r6)		# 7515
	addi	%r4, %r4, 2		# 7516
	ld	%r4, (%r4)		# 7517
	addi	%r2, %r2, 2		# 7518
	sto	%r4, (%r2)		# 7519
if_cont.16473:
	ld	%r2, (%r63)		# 7520
	addi	%r2, %r2, 1		# 7521
	addi	%r63, %r63, -1		# 7522
	ld	%r60, (%r63)		# 7523
	jmpui	pretrace_diffuse_rays.2857		# 7524
else.16471:
	addi	%r63, %r63, -1		# 7525
	ld	%r60, (%r63)		# 7526
	jmpu	%r60		# 7527
else.16470:
	addi	%r63, %r63, -1		# 7528
	ld	%r60, (%r63)		# 7529
	jmpu	%r60		# 7530
.globl	pretrace_pixels.2860
pretrace_pixels.2860:
	sto	%r60, (%r63)		# 7531
	addi	%r63, %r63, 1		# 7532
	movi	%r7, 0		# 7533
	cmpgt	%r7, %r7, %r2		# 7534
	jmpi	%r7, else.16476		# 7535
	movi	%r7, min_caml_scan_pitch		# 7536
	addi	%r7, %r7, 0		# 7537
	ld	%r7, (%r7)		# 7538
	movi	%r8, min_caml_image_center		# 7539
	addi	%r8, %r8, 0		# 7540
	ld	%r8, (%r8)		# 7541
	sub	%r8, %r2, %r8		# 7542
	sto	%r3, (%r63)		# 7543
	sto	%r2, 1(%r63)		# 7544
	sto	%r1, 2(%r63)		# 7545
	sto	%r6, 3(%r63)		# 7546
	sto	%r5, 4(%r63)		# 7547
	sto	%r4, 5(%r63)		# 7548
	sto	%r7, 6(%r63)		# 7549
	mov	%r1, %r8		# 7550
	addi	%r63, %r63, 7		# 7551
	call	%r60, min_caml_float_of_int		# 7552
	addi	%r63, %r63, -7		# 7553
	ld	%r2, 6(%r63)		# 7554
	fmul	%r2, %r2, %r1		# 7555
	movi	%r1, min_caml_ptrace_dirvec		# 7556
	movi	%r3, min_caml_screenx_dir		# 7557
	addi	%r3, %r3, 0		# 7558
	ld	%r3, (%r3)		# 7559
	fmul	%r3, %r2, %r3		# 7560
	ld	%r4, 5(%r63)		# 7561
	fadd	%r3, %r3, %r4		# 7562
	addi	%r1, %r1, 0		# 7563
	sto	%r3, (%r1)		# 7564
	movi	%r1, min_caml_ptrace_dirvec		# 7565
	movi	%r3, min_caml_screenx_dir		# 7566
	addi	%r3, %r3, 1		# 7567
	ld	%r3, (%r3)		# 7568
	fmul	%r3, %r2, %r3		# 7569
	ld	%r5, 4(%r63)		# 7570
	fadd	%r3, %r3, %r5		# 7571
	addi	%r1, %r1, 1		# 7572
	sto	%r3, (%r1)		# 7573
	movi	%r1, min_caml_ptrace_dirvec		# 7574
	movi	%r3, min_caml_screenx_dir		# 7575
	addi	%r3, %r3, 2		# 7576
	ld	%r3, (%r3)		# 7577
	fmul	%r2, %r2, %r3		# 7578
	ld	%r3, 3(%r63)		# 7579
	fadd	%r2, %r2, %r3		# 7580
	addi	%r1, %r1, 2		# 7581
	sto	%r2, (%r1)		# 7582
	movi	%r1, min_caml_ptrace_dirvec		# 7583
	movi	%r2, 0		# 7584
	addi	%r63, %r63, 7		# 7585
	call	%r60, vecunit_sgn.2488		# 7586
	addi	%r63, %r63, -7		# 7587
	movi	%r1, min_caml_rgb		# 7588
	movi	%r2, 0		# 7589
	addi	%r3, %r1, 0		# 7590
	sto	%r2, (%r3)		# 7591
	addi	%r3, %r1, 1		# 7592
	sto	%r2, (%r3)		# 7593
	addi	%r1, %r1, 2		# 7594
	sto	%r2, (%r1)		# 7595
	movi	%r1, min_caml_startp		# 7596
	movi	%r2, min_caml_viewpoint		# 7597
	addi	%r3, %r2, 0		# 7598
	ld	%r3, (%r3)		# 7599
	addi	%r4, %r1, 0		# 7600
	sto	%r3, (%r4)		# 7601
	addi	%r3, %r2, 1		# 7602
	ld	%r3, (%r3)		# 7603
	addi	%r4, %r1, 1		# 7604
	sto	%r3, (%r4)		# 7605
	addi	%r2, %r2, 2		# 7606
	ld	%r2, (%r2)		# 7607
	addi	%r1, %r1, 2		# 7608
	sto	%r2, (%r1)		# 7609
	movi	%r1, 0		# 7610
	movhiz	%r4, 260096		# 7611
	movi	%r2, min_caml_ptrace_dirvec		# 7612
	ld	%r3, 1(%r63)		# 7613
	ld	%r5, 2(%r63)		# 7614
	add	%r6, %r5, %r3		# 7615
	ld	%r6, (%r6)		# 7616
	movi	%r7, 0		# 7617
	mov	%r5, %r7		# 7618
	mov	%r3, %r6		# 7619
	addi	%r63, %r63, 7		# 7620
	call	%r60, trace_ray.2797		# 7621
	addi	%r63, %r63, -7		# 7622
	ld	%r1, 1(%r63)		# 7623
	ld	%r2, 2(%r63)		# 7624
	add	%r3, %r2, %r1		# 7625
	ld	%r3, (%r3)		# 7626
	ld	%r3, (%r3)		# 7627
	movi	%r4, min_caml_rgb		# 7628
	addi	%r5, %r4, 0		# 7629
	ld	%r5, (%r5)		# 7630
	addi	%r6, %r3, 0		# 7631
	sto	%r5, (%r6)		# 7632
	addi	%r5, %r4, 1		# 7633
	ld	%r5, (%r5)		# 7634
	addi	%r6, %r3, 1		# 7635
	sto	%r5, (%r6)		# 7636
	addi	%r4, %r4, 2		# 7637
	ld	%r4, (%r4)		# 7638
	addi	%r3, %r3, 2		# 7639
	sto	%r4, (%r3)		# 7640
	add	%r3, %r2, %r1		# 7641
	ld	%r3, (%r3)		# 7642
	ld	%r3, 6(%r3)		# 7643
	addi	%r3, %r3, 0		# 7644
	ld	%r4, (%r63)		# 7645
	sto	%r4, (%r3)		# 7646
	add	%r3, %r2, %r1		# 7647
	ld	%r3, (%r3)		# 7648
	movi	%r5, 0		# 7649
	mov	%r2, %r5		# 7650
	mov	%r1, %r3		# 7651
	addi	%r63, %r63, 7		# 7652
	call	%r60, pretrace_diffuse_rays.2857		# 7653
	addi	%r63, %r63, -7		# 7654
	ld	%r1, 1(%r63)		# 7655
	addi	%r2, %r1, -1		# 7656
	ld	%r1, (%r63)		# 7657
	addi	%r1, %r1, 1		# 7658
	movi	%r3, 5		# 7659
	cmpgt	%r3, %r3, %r1		# 7660
	jmpi	%r3, else.16477		# 7661
	addi	%r3, %r1, -5		# 7662
	jmpui	if_cont.16478		# 7663
else.16477:
	mov	%r3, %r1		# 7664
if_cont.16478:
	ld	%r4, 5(%r63)		# 7665
	ld	%r5, 4(%r63)		# 7666
	ld	%r6, 3(%r63)		# 7667
	ld	%r1, 2(%r63)		# 7668
	addi	%r63, %r63, -1		# 7669
	ld	%r60, (%r63)		# 7670
	jmpui	pretrace_pixels.2860		# 7671
else.16476:
	addi	%r63, %r63, -1		# 7672
	ld	%r60, (%r63)		# 7673
	jmpu	%r60		# 7674
.globl	pretrace_line.2867
pretrace_line.2867:
	sto	%r60, (%r63)		# 7675
	addi	%r63, %r63, 1		# 7676
	movi	%r4, min_caml_scan_pitch		# 7677
	addi	%r4, %r4, 0		# 7678
	ld	%r4, (%r4)		# 7679
	movi	%r5, min_caml_image_center		# 7680
	addi	%r5, %r5, 1		# 7681
	ld	%r5, (%r5)		# 7682
	sub	%r2, %r2, %r5		# 7683
	sto	%r3, (%r63)		# 7684
	sto	%r1, 1(%r63)		# 7685
	sto	%r4, 2(%r63)		# 7686
	mov	%r1, %r2		# 7687
	addi	%r63, %r63, 3		# 7688
	call	%r60, min_caml_float_of_int		# 7689
	addi	%r63, %r63, -3		# 7690
	ld	%r2, 2(%r63)		# 7691
	fmul	%r2, %r2, %r1		# 7692
	movi	%r1, min_caml_screeny_dir		# 7693
	addi	%r1, %r1, 0		# 7694
	ld	%r1, (%r1)		# 7695
	fmul	%r1, %r2, %r1		# 7696
	movi	%r3, min_caml_screenz_dir		# 7697
	addi	%r3, %r3, 0		# 7698
	ld	%r3, (%r3)		# 7699
	fadd	%r4, %r1, %r3		# 7700
	movi	%r1, min_caml_screeny_dir		# 7701
	addi	%r1, %r1, 1		# 7702
	ld	%r1, (%r1)		# 7703
	fmul	%r1, %r2, %r1		# 7704
	movi	%r3, min_caml_screenz_dir		# 7705
	addi	%r3, %r3, 1		# 7706
	ld	%r3, (%r3)		# 7707
	fadd	%r5, %r1, %r3		# 7708
	movi	%r1, min_caml_screeny_dir		# 7709
	addi	%r1, %r1, 2		# 7710
	ld	%r1, (%r1)		# 7711
	fmul	%r2, %r2, %r1		# 7712
	movi	%r1, min_caml_screenz_dir		# 7713
	addi	%r1, %r1, 2		# 7714
	ld	%r1, (%r1)		# 7715
	fadd	%r6, %r2, %r1		# 7716
	movi	%r1, min_caml_image_size		# 7717
	addi	%r1, %r1, 0		# 7718
	ld	%r1, (%r1)		# 7719
	addi	%r2, %r1, -1		# 7720
	ld	%r1, 1(%r63)		# 7721
	ld	%r3, (%r63)		# 7722
	addi	%r63, %r63, -1		# 7723
	ld	%r60, (%r63)		# 7724
	jmpui	pretrace_pixels.2860		# 7725
.globl	scan_pixel.2871
scan_pixel.2871:
	sto	%r60, (%r63)		# 7726
	addi	%r63, %r63, 1		# 7727
	movi	%r6, min_caml_image_size		# 7728
	addi	%r6, %r6, 0		# 7729
	ld	%r6, (%r6)		# 7730
	cmpgt	%r6, %r6, %r1		# 7731
	jmpi	%r6, else.16480		# 7732
	addi	%r63, %r63, -1		# 7733
	ld	%r60, (%r63)		# 7734
	jmpu	%r60		# 7735
else.16480:
	movi	%r6, min_caml_rgb		# 7736
	add	%r7, %r4, %r1		# 7737
	ld	%r7, (%r7)		# 7738
	ld	%r7, (%r7)		# 7739
	addi	%r8, %r7, 0		# 7740
	ld	%r8, (%r8)		# 7741
	addi	%r9, %r6, 0		# 7742
	sto	%r8, (%r9)		# 7743
	addi	%r8, %r7, 1		# 7744
	ld	%r8, (%r8)		# 7745
	addi	%r9, %r6, 1		# 7746
	sto	%r8, (%r9)		# 7747
	addi	%r7, %r7, 2		# 7748
	ld	%r7, (%r7)		# 7749
	addi	%r6, %r6, 2		# 7750
	sto	%r7, (%r6)		# 7751
	movi	%r6, min_caml_image_size		# 7752
	addi	%r6, %r6, 1		# 7753
	ld	%r6, (%r6)		# 7754
	addi	%r7, %r2, 1		# 7755
	cmpgt	%r6, %r6, %r7		# 7756
	jmpi	%r6, else.16482		# 7757
	movi	%r6, 0		# 7758
	jmpui	if_cont.16483		# 7759
else.16482:
	cmpgti	%r6, %r2, 0		# 7760
	jmpi	%r6, else.16484		# 7761
	movi	%r6, 0		# 7762
	jmpui	if_cont.16485		# 7763
else.16484:
	movi	%r6, min_caml_image_size		# 7764
	addi	%r6, %r6, 0		# 7765
	ld	%r6, (%r6)		# 7766
	addi	%r7, %r1, 1		# 7767
	cmpgt	%r6, %r6, %r7		# 7768
	jmpi	%r6, else.16486		# 7769
	movi	%r6, 0		# 7770
	jmpui	if_cont.16487		# 7771
else.16486:
	cmpgti	%r6, %r1, 0		# 7772
	jmpi	%r6, else.16488		# 7773
	movi	%r6, 0		# 7774
	jmpui	if_cont.16489		# 7775
else.16488:
	movi	%r6, 1		# 7776
if_cont.16489:
if_cont.16487:
if_cont.16485:
if_cont.16483:
	cmpeqi	%r6, %r6, 0		# 7777
	sto	%r5, (%r63)		# 7778
	sto	%r3, 1(%r63)		# 7779
	sto	%r2, 2(%r63)		# 7780
	sto	%r4, 3(%r63)		# 7781
	sto	%r1, 4(%r63)		# 7782
	jmpzi	%r6, else.16490		# 7783
	add	%r6, %r4, %r1		# 7784
	ld	%r6, (%r6)		# 7785
	ld	%r7, 2(%r6)		# 7786
	movi	%r8, 0		# 7787
	addi	%r7, %r7, 0		# 7788
	ld	%r7, (%r7)		# 7789
	cmpgt	%r8, %r8, %r7		# 7790
	jmpi	%r8, else.16492		# 7791
	ld	%r7, 3(%r6)		# 7792
	addi	%r7, %r7, 0		# 7793
	ld	%r7, (%r7)		# 7794
	cmpeqi	%r7, %r7, 0		# 7795
	sto	%r6, 5(%r63)		# 7796
	jmpzi	%r7, else.16494		# 7797
	jmpui	if_cont.16495		# 7798
else.16494:
	ld	%r7, 5(%r6)		# 7799
	ld	%r8, 7(%r6)		# 7800
	ld	%r9, 1(%r6)		# 7801
	ld	%r10, 4(%r6)		# 7802
	movi	%r11, min_caml_diffuse_ray		# 7803
	addi	%r7, %r7, 0		# 7804
	ld	%r7, (%r7)		# 7805
	addi	%r12, %r7, 0		# 7806
	ld	%r12, (%r12)		# 7807
	addi	%r13, %r11, 0		# 7808
	sto	%r12, (%r13)		# 7809
	addi	%r12, %r7, 1		# 7810
	ld	%r12, (%r12)		# 7811
	addi	%r13, %r11, 1		# 7812
	sto	%r12, (%r13)		# 7813
	addi	%r7, %r7, 2		# 7814
	ld	%r7, (%r7)		# 7815
	addi	%r11, %r11, 2		# 7816
	sto	%r7, (%r11)		# 7817
	ld	%r7, 6(%r6)		# 7818
	addi	%r7, %r7, 0		# 7819
	ld	%r7, (%r7)		# 7820
	addi	%r8, %r8, 0		# 7821
	ld	%r8, (%r8)		# 7822
	addi	%r9, %r9, 0		# 7823
	ld	%r9, (%r9)		# 7824
	sto	%r10, 6(%r63)		# 7825
	mov	%r3, %r9		# 7826
	mov	%r2, %r8		# 7827
	mov	%r1, %r7		# 7828
	addi	%r63, %r63, 7		# 7829
	call	%r60, trace_diffuse_ray_80percent.2815		# 7830
	addi	%r63, %r63, -7		# 7831
	movi	%r1, min_caml_rgb		# 7832
	ld	%r2, 6(%r63)		# 7833
	addi	%r2, %r2, 0		# 7834
	ld	%r2, (%r2)		# 7835
	movi	%r3, min_caml_diffuse_ray		# 7836
	addi	%r63, %r63, 7		# 7837
	call	%r60, vecaccumv.2512		# 7838
	addi	%r63, %r63, -7		# 7839
if_cont.16495:
	movi	%r2, 1		# 7840
	ld	%r1, 5(%r63)		# 7841
	addi	%r63, %r63, 7		# 7842
	call	%r60, do_without_neighbors.2828		# 7843
	addi	%r63, %r63, -7		# 7844
	jmpui	if_cont.16493		# 7845
else.16492:
if_cont.16493:
	jmpui	if_cont.16491		# 7846
else.16490:
	movi	%r6, 0		# 7847
	add	%r7, %r4, %r1		# 7848
	ld	%r7, (%r7)		# 7849
	movi	%r8, 0		# 7850
	ld	%r9, 2(%r7)		# 7851
	addi	%r9, %r9, 0		# 7852
	ld	%r9, (%r9)		# 7853
	cmpgt	%r8, %r8, %r9		# 7854
	jmpi	%r8, else.16496		# 7855
	add	%r8, %r4, %r1		# 7856
	ld	%r8, (%r8)		# 7857
	ld	%r8, 2(%r8)		# 7858
	addi	%r8, %r8, 0		# 7859
	ld	%r8, (%r8)		# 7860
	add	%r9, %r3, %r1		# 7861
	ld	%r9, (%r9)		# 7862
	ld	%r9, 2(%r9)		# 7863
	addi	%r9, %r9, 0		# 7864
	ld	%r9, (%r9)		# 7865
	cmpeq	%r9, %r9, %r8		# 7866
	jmpzi	%r9, else.16498		# 7867
	add	%r9, %r5, %r1		# 7868
	ld	%r9, (%r9)		# 7869
	ld	%r9, 2(%r9)		# 7870
	addi	%r9, %r9, 0		# 7871
	ld	%r9, (%r9)		# 7872
	cmpeq	%r9, %r9, %r8		# 7873
	jmpzi	%r9, else.16500		# 7874
	addi	%r9, %r1, -1		# 7875
	add	%r9, %r4, %r9		# 7876
	ld	%r9, (%r9)		# 7877
	ld	%r9, 2(%r9)		# 7878
	addi	%r9, %r9, 0		# 7879
	ld	%r9, (%r9)		# 7880
	cmpeq	%r9, %r9, %r8		# 7881
	jmpzi	%r9, else.16502		# 7882
	addi	%r9, %r1, 1		# 7883
	add	%r9, %r4, %r9		# 7884
	ld	%r9, (%r9)		# 7885
	ld	%r9, 2(%r9)		# 7886
	addi	%r9, %r9, 0		# 7887
	ld	%r9, (%r9)		# 7888
	cmpeq	%r9, %r9, %r8		# 7889
	jmpzi	%r9, else.16504		# 7890
	movi	%r8, 1		# 7891
	jmpui	if_cont.16505		# 7892
else.16504:
	movi	%r8, 0		# 7893
if_cont.16505:
	jmpui	if_cont.16503		# 7894
else.16502:
	movi	%r8, 0		# 7895
if_cont.16503:
	jmpui	if_cont.16501		# 7896
else.16500:
	movi	%r8, 0		# 7897
if_cont.16501:
	jmpui	if_cont.16499		# 7898
else.16498:
	movi	%r8, 0		# 7899
if_cont.16499:
	cmpeqi	%r8, %r8, 0		# 7900
	jmpzi	%r8, else.16506		# 7901
	add	%r7, %r4, %r1		# 7902
	ld	%r7, (%r7)		# 7903
	mov	%r2, %r6		# 7904
	mov	%r1, %r7		# 7905
	addi	%r63, %r63, 7		# 7906
	call	%r60, do_without_neighbors.2828		# 7907
	addi	%r63, %r63, -7		# 7908
	jmpui	if_cont.16507		# 7909
else.16506:
	ld	%r7, 3(%r7)		# 7910
	addi	%r7, %r7, 0		# 7911
	ld	%r7, (%r7)		# 7912
	cmpeqi	%r7, %r7, 0		# 7913
	jmpzi	%r7, else.16508		# 7914
	jmpui	if_cont.16509		# 7915
else.16508:
	mov	%r2, %r3		# 7916
	mov	%r3, %r4		# 7917
	mov	%r4, %r5		# 7918
	mov	%r5, %r6		# 7919
	addi	%r63, %r63, 7		# 7920
	call	%r60, calc_diffuse_using_5points.2822		# 7921
	addi	%r63, %r63, -7		# 7922
if_cont.16509:
	movi	%r6, 1		# 7923
	ld	%r1, 4(%r63)		# 7924
	ld	%r2, 2(%r63)		# 7925
	ld	%r3, 1(%r63)		# 7926
	ld	%r4, 3(%r63)		# 7927
	ld	%r5, (%r63)		# 7928
	addi	%r63, %r63, 7		# 7929
	call	%r60, try_exploit_neighbors.2844		# 7930
	addi	%r63, %r63, -7		# 7931
if_cont.16507:
	jmpui	if_cont.16497		# 7932
else.16496:
if_cont.16497:
if_cont.16491:
	movi	%r1, min_caml_rgb		# 7933
	addi	%r1, %r1, 0		# 7934
	ld	%r1, (%r1)		# 7935
	addi	%r63, %r63, 7		# 7936
	call	%r60, min_caml_int_of_float		# 7937
	addi	%r63, %r63, -7		# 7938
	cmpgti	%r2, %r1, 255		# 7939
	jmpi	%r2, else.16510		# 7940
	movi	%r2, 0		# 7941
	cmpgt	%r2, %r2, %r1		# 7942
	jmpi	%r2, else.16512		# 7943
	jmpui	if_cont.16513		# 7944
else.16512:
	movi	%r1, 0		# 7945
if_cont.16513:
	jmpui	if_cont.16511		# 7946
else.16510:
	movi	%r1, 255		# 7947
if_cont.16511:
	addi	%r63, %r63, 7		# 7948
	call	%r60, min_caml_print_int		# 7949
	addi	%r63, %r63, -7		# 7950
	movi	%r1, 32		# 7951
	out	%r1		# 7952
	movi	%r1, min_caml_rgb		# 7953
	addi	%r1, %r1, 1		# 7954
	ld	%r1, (%r1)		# 7955
	addi	%r63, %r63, 7		# 7956
	call	%r60, min_caml_int_of_float		# 7957
	addi	%r63, %r63, -7		# 7958
	cmpgti	%r2, %r1, 255		# 7959
	jmpi	%r2, else.16514		# 7960
	movi	%r2, 0		# 7961
	cmpgt	%r2, %r2, %r1		# 7962
	jmpi	%r2, else.16516		# 7963
	jmpui	if_cont.16517		# 7964
else.16516:
	movi	%r1, 0		# 7965
if_cont.16517:
	jmpui	if_cont.16515		# 7966
else.16514:
	movi	%r1, 255		# 7967
if_cont.16515:
	addi	%r63, %r63, 7		# 7968
	call	%r60, min_caml_print_int		# 7969
	addi	%r63, %r63, -7		# 7970
	movi	%r1, 32		# 7971
	out	%r1		# 7972
	movi	%r1, min_caml_rgb		# 7973
	addi	%r1, %r1, 2		# 7974
	ld	%r1, (%r1)		# 7975
	addi	%r63, %r63, 7		# 7976
	call	%r60, min_caml_int_of_float		# 7977
	addi	%r63, %r63, -7		# 7978
	cmpgti	%r2, %r1, 255		# 7979
	jmpi	%r2, else.16518		# 7980
	movi	%r2, 0		# 7981
	cmpgt	%r2, %r2, %r1		# 7982
	jmpi	%r2, else.16520		# 7983
	jmpui	if_cont.16521		# 7984
else.16520:
	movi	%r1, 0		# 7985
if_cont.16521:
	jmpui	if_cont.16519		# 7986
else.16518:
	movi	%r1, 255		# 7987
if_cont.16519:
	addi	%r63, %r63, 7		# 7988
	call	%r60, min_caml_print_int		# 7989
	addi	%r63, %r63, -7		# 7990
	movi	%r1, 10		# 7991
	out	%r1		# 7992
	ld	%r1, 4(%r63)		# 7993
	addi	%r1, %r1, 1		# 7994
	movi	%r2, min_caml_image_size		# 7995
	addi	%r2, %r2, 0		# 7996
	ld	%r2, (%r2)		# 7997
	cmpgt	%r2, %r2, %r1		# 7998
	jmpi	%r2, else.16522		# 7999
	addi	%r63, %r63, -1		# 8000
	ld	%r60, (%r63)		# 8001
	jmpu	%r60		# 8002
else.16522:
	movi	%r2, min_caml_rgb		# 8003
	ld	%r4, 3(%r63)		# 8004
	add	%r3, %r4, %r1		# 8005
	ld	%r3, (%r3)		# 8006
	ld	%r3, (%r3)		# 8007
	addi	%r5, %r3, 0		# 8008
	ld	%r5, (%r5)		# 8009
	addi	%r6, %r2, 0		# 8010
	sto	%r5, (%r6)		# 8011
	addi	%r5, %r3, 1		# 8012
	ld	%r5, (%r5)		# 8013
	addi	%r6, %r2, 1		# 8014
	sto	%r5, (%r6)		# 8015
	addi	%r3, %r3, 2		# 8016
	ld	%r3, (%r3)		# 8017
	addi	%r2, %r2, 2		# 8018
	sto	%r3, (%r2)		# 8019
	movi	%r2, min_caml_image_size		# 8020
	addi	%r2, %r2, 1		# 8021
	ld	%r2, (%r2)		# 8022
	ld	%r3, 2(%r63)		# 8023
	addi	%r5, %r3, 1		# 8024
	cmpgt	%r2, %r2, %r5		# 8025
	jmpi	%r2, else.16524		# 8026
	movi	%r2, 0		# 8027
	jmpui	if_cont.16525		# 8028
else.16524:
	cmpgti	%r2, %r3, 0		# 8029
	jmpi	%r2, else.16526		# 8030
	movi	%r2, 0		# 8031
	jmpui	if_cont.16527		# 8032
else.16526:
	movi	%r2, min_caml_image_size		# 8033
	addi	%r2, %r2, 0		# 8034
	ld	%r2, (%r2)		# 8035
	addi	%r5, %r1, 1		# 8036
	cmpgt	%r2, %r2, %r5		# 8037
	jmpi	%r2, else.16528		# 8038
	movi	%r2, 0		# 8039
	jmpui	if_cont.16529		# 8040
else.16528:
	cmpgti	%r2, %r1, 0		# 8041
	jmpi	%r2, else.16530		# 8042
	movi	%r2, 0		# 8043
	jmpui	if_cont.16531		# 8044
else.16530:
	movi	%r2, 1		# 8045
if_cont.16531:
if_cont.16529:
if_cont.16527:
if_cont.16525:
	cmpeqi	%r2, %r2, 0		# 8046
	sto	%r1, 7(%r63)		# 8047
	jmpzi	%r2, else.16532		# 8048
	add	%r2, %r4, %r1		# 8049
	ld	%r2, (%r2)		# 8050
	movi	%r5, 0		# 8051
	mov	%r1, %r2		# 8052
	mov	%r2, %r5		# 8053
	addi	%r63, %r63, 8		# 8054
	call	%r60, do_without_neighbors.2828		# 8055
	addi	%r63, %r63, -8		# 8056
	jmpui	if_cont.16533		# 8057
else.16532:
	movi	%r6, 0		# 8058
	ld	%r2, 1(%r63)		# 8059
	ld	%r5, (%r63)		# 8060
	sto	%r3, 8(%r63)		# 8061
	mov	%r3, %r2		# 8062
	ld	%r2, 8(%r63)		# 8063
	addi	%r63, %r63, 8		# 8064
	call	%r60, try_exploit_neighbors.2844		# 8065
	addi	%r63, %r63, -8		# 8066
if_cont.16533:
	addi	%r63, %r63, 8		# 8067
	call	%r60, write_rgb.2855		# 8068
	addi	%r63, %r63, -8		# 8069
	ld	%r1, 7(%r63)		# 8070
	addi	%r1, %r1, 1		# 8071
	ld	%r2, 2(%r63)		# 8072
	ld	%r3, 1(%r63)		# 8073
	ld	%r4, 3(%r63)		# 8074
	ld	%r5, (%r63)		# 8075
	addi	%r63, %r63, -1		# 8076
	ld	%r60, (%r63)		# 8077
	jmpui	scan_pixel.2871		# 8078
.globl	scan_line.2877
scan_line.2877:
	sto	%r60, (%r63)		# 8079
	addi	%r63, %r63, 1		# 8080
	movi	%r6, min_caml_image_size		# 8081
	addi	%r6, %r6, 1		# 8082
	ld	%r6, (%r6)		# 8083
	cmpgt	%r6, %r6, %r1		# 8084
	jmpi	%r6, else.16534		# 8085
	addi	%r63, %r63, -1		# 8086
	ld	%r60, (%r63)		# 8087
	jmpu	%r60		# 8088
else.16534:
	movi	%r6, min_caml_image_size		# 8089
	addi	%r6, %r6, 1		# 8090
	ld	%r6, (%r6)		# 8091
	addi	%r6, %r6, -1		# 8092
	cmpgt	%r6, %r6, %r1		# 8093
	sto	%r5, (%r63)		# 8094
	sto	%r4, 1(%r63)		# 8095
	sto	%r2, 2(%r63)		# 8096
	sto	%r1, 3(%r63)		# 8097
	sto	%r3, 4(%r63)		# 8098
	jmpi	%r6, else.16536		# 8099
	jmpui	if_cont.16537		# 8100
else.16536:
	addi	%r6, %r1, 1		# 8101
	mov	%r3, %r5		# 8102
	mov	%r2, %r6		# 8103
	mov	%r1, %r4		# 8104
	addi	%r63, %r63, 5		# 8105
	call	%r60, pretrace_line.2867		# 8106
	addi	%r63, %r63, -5		# 8107
if_cont.16537:
	movi	%r1, 0		# 8108
	movi	%r2, min_caml_image_size		# 8109
	addi	%r2, %r2, 0		# 8110
	ld	%r2, (%r2)		# 8111
	cmpgti	%r2, %r2, 0		# 8112
	jmpi	%r2, else.16538		# 8113
	jmpui	if_cont.16539		# 8114
else.16538:
	movi	%r2, min_caml_rgb		# 8115
	ld	%r4, 4(%r63)		# 8116
	addi	%r3, %r4, 0		# 8117
	ld	%r3, (%r3)		# 8118
	ld	%r3, (%r3)		# 8119
	addi	%r5, %r3, 0		# 8120
	ld	%r5, (%r5)		# 8121
	addi	%r6, %r2, 0		# 8122
	sto	%r5, (%r6)		# 8123
	addi	%r5, %r3, 1		# 8124
	ld	%r5, (%r5)		# 8125
	addi	%r6, %r2, 1		# 8126
	sto	%r5, (%r6)		# 8127
	addi	%r3, %r3, 2		# 8128
	ld	%r3, (%r3)		# 8129
	addi	%r2, %r2, 2		# 8130
	sto	%r3, (%r2)		# 8131
	movi	%r2, min_caml_image_size		# 8132
	addi	%r2, %r2, 1		# 8133
	ld	%r2, (%r2)		# 8134
	ld	%r3, 3(%r63)		# 8135
	addi	%r5, %r3, 1		# 8136
	cmpgt	%r2, %r2, %r5		# 8137
	jmpi	%r2, else.16540		# 8138
	movi	%r2, 0		# 8139
	jmpui	if_cont.16541		# 8140
else.16540:
	cmpgti	%r2, %r3, 0		# 8141
	jmpi	%r2, else.16542		# 8142
	movi	%r2, 0		# 8143
	jmpui	if_cont.16543		# 8144
else.16542:
	movi	%r2, min_caml_image_size		# 8145
	addi	%r2, %r2, 0		# 8146
	ld	%r2, (%r2)		# 8147
	cmpgti	%r2, %r2, 1		# 8148
	jmpi	%r2, else.16544		# 8149
	movi	%r2, 0		# 8150
	jmpui	if_cont.16545		# 8151
else.16544:
	movi	%r2, 0		# 8152
if_cont.16545:
if_cont.16543:
if_cont.16541:
	cmpeqi	%r2, %r2, 0		# 8153
	jmpzi	%r2, else.16546		# 8154
	addi	%r1, %r4, 0		# 8155
	ld	%r1, (%r1)		# 8156
	movi	%r2, 0		# 8157
	addi	%r63, %r63, 5		# 8158
	call	%r60, do_without_neighbors.2828		# 8159
	addi	%r63, %r63, -5		# 8160
	jmpui	if_cont.16547		# 8161
else.16546:
	movi	%r6, 0		# 8162
	ld	%r2, 2(%r63)		# 8163
	ld	%r5, 1(%r63)		# 8164
	sto	%r3, 5(%r63)		# 8165
	mov	%r3, %r2		# 8166
	ld	%r2, 5(%r63)		# 8167
	addi	%r63, %r63, 5		# 8168
	call	%r60, try_exploit_neighbors.2844		# 8169
	addi	%r63, %r63, -5		# 8170
if_cont.16547:
	addi	%r63, %r63, 5		# 8171
	call	%r60, write_rgb.2855		# 8172
	addi	%r63, %r63, -5		# 8173
	movi	%r1, 1		# 8174
	ld	%r2, 3(%r63)		# 8175
	ld	%r3, 2(%r63)		# 8176
	ld	%r4, 4(%r63)		# 8177
	ld	%r5, 1(%r63)		# 8178
	addi	%r63, %r63, 5		# 8179
	call	%r60, scan_pixel.2871		# 8180
	addi	%r63, %r63, -5		# 8181
if_cont.16539:
	ld	%r1, 3(%r63)		# 8182
	addi	%r2, %r1, 1		# 8183
	ld	%r1, (%r63)		# 8184
	addi	%r1, %r1, 2		# 8185
	movi	%r3, 5		# 8186
	cmpgt	%r3, %r3, %r1		# 8187
	jmpi	%r3, else.16548		# 8188
	addi	%r3, %r1, -5		# 8189
	jmpui	if_cont.16549		# 8190
else.16548:
	mov	%r3, %r1		# 8191
if_cont.16549:
	movi	%r1, min_caml_image_size		# 8192
	addi	%r1, %r1, 1		# 8193
	ld	%r1, (%r1)		# 8194
	cmpgt	%r1, %r1, %r2		# 8195
	jmpi	%r1, else.16550		# 8196
	addi	%r63, %r63, -1		# 8197
	ld	%r60, (%r63)		# 8198
	jmpu	%r60		# 8199
else.16550:
	movi	%r1, min_caml_image_size		# 8200
	addi	%r1, %r1, 1		# 8201
	ld	%r1, (%r1)		# 8202
	addi	%r1, %r1, -1		# 8203
	cmpgt	%r1, %r1, %r2		# 8204
	sto	%r3, 5(%r63)		# 8205
	sto	%r2, 6(%r63)		# 8206
	jmpi	%r1, else.16552		# 8207
	jmpui	if_cont.16553		# 8208
else.16552:
	addi	%r1, %r2, 1		# 8209
	ld	%r4, 2(%r63)		# 8210
	mov	%r2, %r1		# 8211
	mov	%r1, %r4		# 8212
	addi	%r63, %r63, 7		# 8213
	call	%r60, pretrace_line.2867		# 8214
	addi	%r63, %r63, -7		# 8215
if_cont.16553:
	movi	%r1, 0		# 8216
	ld	%r2, 6(%r63)		# 8217
	ld	%r3, 4(%r63)		# 8218
	ld	%r4, 1(%r63)		# 8219
	ld	%r5, 2(%r63)		# 8220
	addi	%r63, %r63, 7		# 8221
	call	%r60, scan_pixel.2871		# 8222
	addi	%r63, %r63, -7		# 8223
	ld	%r1, 6(%r63)		# 8224
	addi	%r1, %r1, 1		# 8225
	ld	%r2, 5(%r63)		# 8226
	addi	%r2, %r2, 2		# 8227
	movi	%r3, 5		# 8228
	cmpgt	%r3, %r3, %r2		# 8229
	jmpi	%r3, else.16554		# 8230
	addi	%r5, %r2, -5		# 8231
	jmpui	if_cont.16555		# 8232
else.16554:
	mov	%r5, %r2		# 8233
if_cont.16555:
	ld	%r2, 1(%r63)		# 8234
	ld	%r3, 2(%r63)		# 8235
	ld	%r4, 4(%r63)		# 8236
	addi	%r63, %r63, -1		# 8237
	ld	%r60, (%r63)		# 8238
	jmpui	scan_line.2877		# 8239
.globl	create_pixel.2885
create_pixel.2885:
	sto	%r60, (%r63)		# 8240
	addi	%r63, %r63, 1		# 8241
	movi	%r2, 0		# 8242
	movi	%r1, 3		# 8243
	call	%r60, min_caml_create_float_array		# 8244
	movi	%r2, 0		# 8245
	movi	%r3, 3		# 8246
	sto	%r1, (%r63)		# 8247
	mov	%r1, %r3		# 8248
	addi	%r63, %r63, 1		# 8249
	call	%r60, min_caml_create_float_array		# 8250
	addi	%r63, %r63, -1		# 8251
	mov	%r2, %r1		# 8252
	movi	%r1, 5		# 8253
	addi	%r63, %r63, 1		# 8254
	call	%r60, min_caml_create_array		# 8255
	addi	%r63, %r63, -1		# 8256
	movi	%r2, 0		# 8257
	movi	%r3, 3		# 8258
	sto	%r1, 1(%r63)		# 8259
	mov	%r1, %r3		# 8260
	addi	%r63, %r63, 2		# 8261
	call	%r60, min_caml_create_float_array		# 8262
	addi	%r63, %r63, -2		# 8263
	ld	%r2, 1(%r63)		# 8264
	addi	%r3, %r2, 1		# 8265
	sto	%r1, (%r3)		# 8266
	movi	%r1, 0		# 8267
	movi	%r3, 3		# 8268
	mov	%r2, %r1		# 8269
	mov	%r1, %r3		# 8270
	addi	%r63, %r63, 2		# 8271
	call	%r60, min_caml_create_float_array		# 8272
	addi	%r63, %r63, -2		# 8273
	ld	%r2, 1(%r63)		# 8274
	addi	%r3, %r2, 2		# 8275
	sto	%r1, (%r3)		# 8276
	movi	%r1, 0		# 8277
	movi	%r3, 3		# 8278
	mov	%r2, %r1		# 8279
	mov	%r1, %r3		# 8280
	addi	%r63, %r63, 2		# 8281
	call	%r60, min_caml_create_float_array		# 8282
	addi	%r63, %r63, -2		# 8283
	ld	%r2, 1(%r63)		# 8284
	addi	%r3, %r2, 3		# 8285
	sto	%r1, (%r3)		# 8286
	movi	%r1, 0		# 8287
	movi	%r3, 3		# 8288
	mov	%r2, %r1		# 8289
	mov	%r1, %r3		# 8290
	addi	%r63, %r63, 2		# 8291
	call	%r60, min_caml_create_float_array		# 8292
	addi	%r63, %r63, -2		# 8293
	ld	%r2, 1(%r63)		# 8294
	addi	%r3, %r2, 4		# 8295
	sto	%r1, (%r3)		# 8296
	movi	%r1, 0		# 8297
	movi	%r3, 5		# 8298
	mov	%r2, %r1		# 8299
	mov	%r1, %r3		# 8300
	addi	%r63, %r63, 2		# 8301
	call	%r60, min_caml_create_array		# 8302
	addi	%r63, %r63, -2		# 8303
	movi	%r2, 0		# 8304
	movi	%r3, 5		# 8305
	sto	%r1, 2(%r63)		# 8306
	mov	%r1, %r3		# 8307
	addi	%r63, %r63, 3		# 8308
	call	%r60, min_caml_create_array		# 8309
	addi	%r63, %r63, -3		# 8310
	movi	%r2, 0		# 8311
	movi	%r3, 3		# 8312
	sto	%r1, 3(%r63)		# 8313
	mov	%r1, %r3		# 8314
	addi	%r63, %r63, 4		# 8315
	call	%r60, min_caml_create_float_array		# 8316
	addi	%r63, %r63, -4		# 8317
	mov	%r2, %r1		# 8318
	movi	%r1, 5		# 8319
	addi	%r63, %r63, 4		# 8320
	call	%r60, min_caml_create_array		# 8321
	addi	%r63, %r63, -4		# 8322
	movi	%r2, 0		# 8323
	movi	%r3, 3		# 8324
	sto	%r1, 4(%r63)		# 8325
	mov	%r1, %r3		# 8326
	addi	%r63, %r63, 5		# 8327
	call	%r60, min_caml_create_float_array		# 8328
	addi	%r63, %r63, -5		# 8329
	ld	%r2, 4(%r63)		# 8330
	addi	%r3, %r2, 1		# 8331
	sto	%r1, (%r3)		# 8332
	movi	%r1, 0		# 8333
	movi	%r3, 3		# 8334
	mov	%r2, %r1		# 8335
	mov	%r1, %r3		# 8336
	addi	%r63, %r63, 5		# 8337
	call	%r60, min_caml_create_float_array		# 8338
	addi	%r63, %r63, -5		# 8339
	ld	%r2, 4(%r63)		# 8340
	addi	%r3, %r2, 2		# 8341
	sto	%r1, (%r3)		# 8342
	movi	%r1, 0		# 8343
	movi	%r3, 3		# 8344
	mov	%r2, %r1		# 8345
	mov	%r1, %r3		# 8346
	addi	%r63, %r63, 5		# 8347
	call	%r60, min_caml_create_float_array		# 8348
	addi	%r63, %r63, -5		# 8349
	ld	%r2, 4(%r63)		# 8350
	addi	%r3, %r2, 3		# 8351
	sto	%r1, (%r3)		# 8352
	movi	%r1, 0		# 8353
	movi	%r3, 3		# 8354
	mov	%r2, %r1		# 8355
	mov	%r1, %r3		# 8356
	addi	%r63, %r63, 5		# 8357
	call	%r60, min_caml_create_float_array		# 8358
	addi	%r63, %r63, -5		# 8359
	ld	%r2, 4(%r63)		# 8360
	addi	%r3, %r2, 4		# 8361
	sto	%r1, (%r3)		# 8362
	movi	%r1, 0		# 8363
	movi	%r3, 3		# 8364
	mov	%r2, %r1		# 8365
	mov	%r1, %r3		# 8366
	addi	%r63, %r63, 5		# 8367
	call	%r60, min_caml_create_float_array		# 8368
	addi	%r63, %r63, -5		# 8369
	mov	%r2, %r1		# 8370
	movi	%r1, 5		# 8371
	addi	%r63, %r63, 5		# 8372
	call	%r60, min_caml_create_array		# 8373
	addi	%r63, %r63, -5		# 8374
	movi	%r2, 0		# 8375
	movi	%r3, 3		# 8376
	sto	%r1, 5(%r63)		# 8377
	mov	%r1, %r3		# 8378
	addi	%r63, %r63, 6		# 8379
	call	%r60, min_caml_create_float_array		# 8380
	addi	%r63, %r63, -6		# 8381
	ld	%r2, 5(%r63)		# 8382
	addi	%r3, %r2, 1		# 8383
	sto	%r1, (%r3)		# 8384
	movi	%r1, 0		# 8385
	movi	%r3, 3		# 8386
	mov	%r2, %r1		# 8387
	mov	%r1, %r3		# 8388
	addi	%r63, %r63, 6		# 8389
	call	%r60, min_caml_create_float_array		# 8390
	addi	%r63, %r63, -6		# 8391
	ld	%r2, 5(%r63)		# 8392
	addi	%r3, %r2, 2		# 8393
	sto	%r1, (%r3)		# 8394
	movi	%r1, 0		# 8395
	movi	%r3, 3		# 8396
	mov	%r2, %r1		# 8397
	mov	%r1, %r3		# 8398
	addi	%r63, %r63, 6		# 8399
	call	%r60, min_caml_create_float_array		# 8400
	addi	%r63, %r63, -6		# 8401
	ld	%r2, 5(%r63)		# 8402
	addi	%r3, %r2, 3		# 8403
	sto	%r1, (%r3)		# 8404
	movi	%r1, 0		# 8405
	movi	%r3, 3		# 8406
	mov	%r2, %r1		# 8407
	mov	%r1, %r3		# 8408
	addi	%r63, %r63, 6		# 8409
	call	%r60, min_caml_create_float_array		# 8410
	addi	%r63, %r63, -6		# 8411
	ld	%r2, 5(%r63)		# 8412
	addi	%r3, %r2, 4		# 8413
	sto	%r1, (%r3)		# 8414
	movi	%r1, 0		# 8415
	movi	%r3, 1		# 8416
	mov	%r2, %r1		# 8417
	mov	%r1, %r3		# 8418
	addi	%r63, %r63, 6		# 8419
	call	%r60, min_caml_create_array		# 8420
	addi	%r63, %r63, -6		# 8421
	movi	%r2, 0		# 8422
	movi	%r3, 3		# 8423
	sto	%r1, 6(%r63)		# 8424
	mov	%r1, %r3		# 8425
	addi	%r63, %r63, 7		# 8426
	call	%r60, min_caml_create_float_array		# 8427
	addi	%r63, %r63, -7		# 8428
	mov	%r2, %r1		# 8429
	movi	%r1, 5		# 8430
	addi	%r63, %r63, 7		# 8431
	call	%r60, min_caml_create_array		# 8432
	addi	%r63, %r63, -7		# 8433
	movi	%r2, 0		# 8434
	movi	%r3, 3		# 8435
	sto	%r1, 7(%r63)		# 8436
	mov	%r1, %r3		# 8437
	addi	%r63, %r63, 8		# 8438
	call	%r60, min_caml_create_float_array		# 8439
	addi	%r63, %r63, -8		# 8440
	ld	%r2, 7(%r63)		# 8441
	addi	%r3, %r2, 1		# 8442
	sto	%r1, (%r3)		# 8443
	movi	%r1, 0		# 8444
	movi	%r3, 3		# 8445
	mov	%r2, %r1		# 8446
	mov	%r1, %r3		# 8447
	addi	%r63, %r63, 8		# 8448
	call	%r60, min_caml_create_float_array		# 8449
	addi	%r63, %r63, -8		# 8450
	ld	%r2, 7(%r63)		# 8451
	addi	%r3, %r2, 2		# 8452
	sto	%r1, (%r3)		# 8453
	movi	%r1, 0		# 8454
	movi	%r3, 3		# 8455
	mov	%r2, %r1		# 8456
	mov	%r1, %r3		# 8457
	addi	%r63, %r63, 8		# 8458
	call	%r60, min_caml_create_float_array		# 8459
	addi	%r63, %r63, -8		# 8460
	ld	%r2, 7(%r63)		# 8461
	addi	%r3, %r2, 3		# 8462
	sto	%r1, (%r3)		# 8463
	movi	%r1, 0		# 8464
	movi	%r3, 3		# 8465
	mov	%r2, %r1		# 8466
	mov	%r1, %r3		# 8467
	addi	%r63, %r63, 8		# 8468
	call	%r60, min_caml_create_float_array		# 8469
	addi	%r63, %r63, -8		# 8470
	ld	%r2, 7(%r63)		# 8471
	addi	%r3, %r2, 4		# 8472
	sto	%r1, (%r3)		# 8473
	mov	%r1, %r62		# 8474
	addi	%r62, %r62, 8		# 8475
	sto	%r2, 7(%r1)		# 8476
	ld	%r2, 6(%r63)		# 8477
	sto	%r2, 6(%r1)		# 8478
	ld	%r2, 5(%r63)		# 8479
	sto	%r2, 5(%r1)		# 8480
	ld	%r2, 4(%r63)		# 8481
	sto	%r2, 4(%r1)		# 8482
	ld	%r2, 3(%r63)		# 8483
	sto	%r2, 3(%r1)		# 8484
	ld	%r2, 2(%r63)		# 8485
	sto	%r2, 2(%r1)		# 8486
	ld	%r2, 1(%r63)		# 8487
	sto	%r2, 1(%r1)		# 8488
	ld	%r2, (%r63)		# 8489
	sto	%r2, (%r1)		# 8490
	addi	%r63, %r63, -1		# 8491
	ld	%r60, (%r63)		# 8492
	jmpu	%r60		# 8493
.globl	init_line_elements.2887
init_line_elements.2887:
	sto	%r60, (%r63)		# 8494
	addi	%r63, %r63, 1		# 8495
	movi	%r3, 0		# 8496
	cmpgt	%r3, %r3, %r2		# 8497
	jmpi	%r3, else.16556		# 8498
	movi	%r3, 0		# 8499
	movi	%r4, 3		# 8500
	sto	%r2, (%r63)		# 8501
	sto	%r1, 1(%r63)		# 8502
	mov	%r2, %r3		# 8503
	mov	%r1, %r4		# 8504
	addi	%r63, %r63, 2		# 8505
	call	%r60, min_caml_create_float_array		# 8506
	addi	%r63, %r63, -2		# 8507
	movi	%r2, 0		# 8508
	movi	%r3, 3		# 8509
	sto	%r1, 2(%r63)		# 8510
	mov	%r1, %r3		# 8511
	addi	%r63, %r63, 3		# 8512
	call	%r60, min_caml_create_float_array		# 8513
	addi	%r63, %r63, -3		# 8514
	mov	%r2, %r1		# 8515
	movi	%r1, 5		# 8516
	addi	%r63, %r63, 3		# 8517
	call	%r60, min_caml_create_array		# 8518
	addi	%r63, %r63, -3		# 8519
	movi	%r2, 0		# 8520
	movi	%r3, 3		# 8521
	sto	%r1, 3(%r63)		# 8522
	mov	%r1, %r3		# 8523
	addi	%r63, %r63, 4		# 8524
	call	%r60, min_caml_create_float_array		# 8525
	addi	%r63, %r63, -4		# 8526
	ld	%r2, 3(%r63)		# 8527
	addi	%r3, %r2, 1		# 8528
	sto	%r1, (%r3)		# 8529
	movi	%r1, 0		# 8530
	movi	%r3, 3		# 8531
	mov	%r2, %r1		# 8532
	mov	%r1, %r3		# 8533
	addi	%r63, %r63, 4		# 8534
	call	%r60, min_caml_create_float_array		# 8535
	addi	%r63, %r63, -4		# 8536
	ld	%r2, 3(%r63)		# 8537
	addi	%r3, %r2, 2		# 8538
	sto	%r1, (%r3)		# 8539
	movi	%r1, 0		# 8540
	movi	%r3, 3		# 8541
	mov	%r2, %r1		# 8542
	mov	%r1, %r3		# 8543
	addi	%r63, %r63, 4		# 8544
	call	%r60, min_caml_create_float_array		# 8545
	addi	%r63, %r63, -4		# 8546
	ld	%r2, 3(%r63)		# 8547
	addi	%r3, %r2, 3		# 8548
	sto	%r1, (%r3)		# 8549
	movi	%r1, 0		# 8550
	movi	%r3, 3		# 8551
	mov	%r2, %r1		# 8552
	mov	%r1, %r3		# 8553
	addi	%r63, %r63, 4		# 8554
	call	%r60, min_caml_create_float_array		# 8555
	addi	%r63, %r63, -4		# 8556
	ld	%r2, 3(%r63)		# 8557
	addi	%r3, %r2, 4		# 8558
	sto	%r1, (%r3)		# 8559
	movi	%r1, 0		# 8560
	movi	%r3, 5		# 8561
	mov	%r2, %r1		# 8562
	mov	%r1, %r3		# 8563
	addi	%r63, %r63, 4		# 8564
	call	%r60, min_caml_create_array		# 8565
	addi	%r63, %r63, -4		# 8566
	movi	%r2, 0		# 8567
	movi	%r3, 5		# 8568
	sto	%r1, 4(%r63)		# 8569
	mov	%r1, %r3		# 8570
	addi	%r63, %r63, 5		# 8571
	call	%r60, min_caml_create_array		# 8572
	addi	%r63, %r63, -5		# 8573
	movi	%r2, 0		# 8574
	movi	%r3, 3		# 8575
	sto	%r1, 5(%r63)		# 8576
	mov	%r1, %r3		# 8577
	addi	%r63, %r63, 6		# 8578
	call	%r60, min_caml_create_float_array		# 8579
	addi	%r63, %r63, -6		# 8580
	mov	%r2, %r1		# 8581
	movi	%r1, 5		# 8582
	addi	%r63, %r63, 6		# 8583
	call	%r60, min_caml_create_array		# 8584
	addi	%r63, %r63, -6		# 8585
	movi	%r2, 0		# 8586
	movi	%r3, 3		# 8587
	sto	%r1, 6(%r63)		# 8588
	mov	%r1, %r3		# 8589
	addi	%r63, %r63, 7		# 8590
	call	%r60, min_caml_create_float_array		# 8591
	addi	%r63, %r63, -7		# 8592
	ld	%r2, 6(%r63)		# 8593
	addi	%r3, %r2, 1		# 8594
	sto	%r1, (%r3)		# 8595
	movi	%r1, 0		# 8596
	movi	%r3, 3		# 8597
	mov	%r2, %r1		# 8598
	mov	%r1, %r3		# 8599
	addi	%r63, %r63, 7		# 8600
	call	%r60, min_caml_create_float_array		# 8601
	addi	%r63, %r63, -7		# 8602
	ld	%r2, 6(%r63)		# 8603
	addi	%r3, %r2, 2		# 8604
	sto	%r1, (%r3)		# 8605
	movi	%r1, 0		# 8606
	movi	%r3, 3		# 8607
	mov	%r2, %r1		# 8608
	mov	%r1, %r3		# 8609
	addi	%r63, %r63, 7		# 8610
	call	%r60, min_caml_create_float_array		# 8611
	addi	%r63, %r63, -7		# 8612
	ld	%r2, 6(%r63)		# 8613
	addi	%r3, %r2, 3		# 8614
	sto	%r1, (%r3)		# 8615
	movi	%r1, 0		# 8616
	movi	%r3, 3		# 8617
	mov	%r2, %r1		# 8618
	mov	%r1, %r3		# 8619
	addi	%r63, %r63, 7		# 8620
	call	%r60, min_caml_create_float_array		# 8621
	addi	%r63, %r63, -7		# 8622
	ld	%r2, 6(%r63)		# 8623
	addi	%r3, %r2, 4		# 8624
	sto	%r1, (%r3)		# 8625
	movi	%r1, 0		# 8626
	movi	%r3, 3		# 8627
	mov	%r2, %r1		# 8628
	mov	%r1, %r3		# 8629
	addi	%r63, %r63, 7		# 8630
	call	%r60, min_caml_create_float_array		# 8631
	addi	%r63, %r63, -7		# 8632
	mov	%r2, %r1		# 8633
	movi	%r1, 5		# 8634
	addi	%r63, %r63, 7		# 8635
	call	%r60, min_caml_create_array		# 8636
	addi	%r63, %r63, -7		# 8637
	movi	%r2, 0		# 8638
	movi	%r3, 3		# 8639
	sto	%r1, 7(%r63)		# 8640
	mov	%r1, %r3		# 8641
	addi	%r63, %r63, 8		# 8642
	call	%r60, min_caml_create_float_array		# 8643
	addi	%r63, %r63, -8		# 8644
	ld	%r2, 7(%r63)		# 8645
	addi	%r3, %r2, 1		# 8646
	sto	%r1, (%r3)		# 8647
	movi	%r1, 0		# 8648
	movi	%r3, 3		# 8649
	mov	%r2, %r1		# 8650
	mov	%r1, %r3		# 8651
	addi	%r63, %r63, 8		# 8652
	call	%r60, min_caml_create_float_array		# 8653
	addi	%r63, %r63, -8		# 8654
	ld	%r2, 7(%r63)		# 8655
	addi	%r3, %r2, 2		# 8656
	sto	%r1, (%r3)		# 8657
	movi	%r1, 0		# 8658
	movi	%r3, 3		# 8659
	mov	%r2, %r1		# 8660
	mov	%r1, %r3		# 8661
	addi	%r63, %r63, 8		# 8662
	call	%r60, min_caml_create_float_array		# 8663
	addi	%r63, %r63, -8		# 8664
	ld	%r2, 7(%r63)		# 8665
	addi	%r3, %r2, 3		# 8666
	sto	%r1, (%r3)		# 8667
	movi	%r1, 0		# 8668
	movi	%r3, 3		# 8669
	mov	%r2, %r1		# 8670
	mov	%r1, %r3		# 8671
	addi	%r63, %r63, 8		# 8672
	call	%r60, min_caml_create_float_array		# 8673
	addi	%r63, %r63, -8		# 8674
	ld	%r2, 7(%r63)		# 8675
	addi	%r3, %r2, 4		# 8676
	sto	%r1, (%r3)		# 8677
	movi	%r1, 0		# 8678
	movi	%r3, 1		# 8679
	mov	%r2, %r1		# 8680
	mov	%r1, %r3		# 8681
	addi	%r63, %r63, 8		# 8682
	call	%r60, min_caml_create_array		# 8683
	addi	%r63, %r63, -8		# 8684
	movi	%r2, 0		# 8685
	movi	%r3, 3		# 8686
	sto	%r1, 8(%r63)		# 8687
	mov	%r1, %r3		# 8688
	addi	%r63, %r63, 9		# 8689
	call	%r60, min_caml_create_float_array		# 8690
	addi	%r63, %r63, -9		# 8691
	mov	%r2, %r1		# 8692
	movi	%r1, 5		# 8693
	addi	%r63, %r63, 9		# 8694
	call	%r60, min_caml_create_array		# 8695
	addi	%r63, %r63, -9		# 8696
	movi	%r2, 0		# 8697
	movi	%r3, 3		# 8698
	sto	%r1, 9(%r63)		# 8699
	mov	%r1, %r3		# 8700
	addi	%r63, %r63, 10		# 8701
	call	%r60, min_caml_create_float_array		# 8702
	addi	%r63, %r63, -10		# 8703
	ld	%r2, 9(%r63)		# 8704
	addi	%r3, %r2, 1		# 8705
	sto	%r1, (%r3)		# 8706
	movi	%r1, 0		# 8707
	movi	%r3, 3		# 8708
	mov	%r2, %r1		# 8709
	mov	%r1, %r3		# 8710
	addi	%r63, %r63, 10		# 8711
	call	%r60, min_caml_create_float_array		# 8712
	addi	%r63, %r63, -10		# 8713
	ld	%r2, 9(%r63)		# 8714
	addi	%r3, %r2, 2		# 8715
	sto	%r1, (%r3)		# 8716
	movi	%r1, 0		# 8717
	movi	%r3, 3		# 8718
	mov	%r2, %r1		# 8719
	mov	%r1, %r3		# 8720
	addi	%r63, %r63, 10		# 8721
	call	%r60, min_caml_create_float_array		# 8722
	addi	%r63, %r63, -10		# 8723
	ld	%r2, 9(%r63)		# 8724
	addi	%r3, %r2, 3		# 8725
	sto	%r1, (%r3)		# 8726
	movi	%r1, 0		# 8727
	movi	%r3, 3		# 8728
	mov	%r2, %r1		# 8729
	mov	%r1, %r3		# 8730
	addi	%r63, %r63, 10		# 8731
	call	%r60, min_caml_create_float_array		# 8732
	addi	%r63, %r63, -10		# 8733
	ld	%r2, 9(%r63)		# 8734
	addi	%r3, %r2, 4		# 8735
	sto	%r1, (%r3)		# 8736
	mov	%r1, %r62		# 8737
	addi	%r62, %r62, 8		# 8738
	sto	%r2, 7(%r1)		# 8739
	ld	%r2, 8(%r63)		# 8740
	sto	%r2, 6(%r1)		# 8741
	ld	%r2, 7(%r63)		# 8742
	sto	%r2, 5(%r1)		# 8743
	ld	%r2, 6(%r63)		# 8744
	sto	%r2, 4(%r1)		# 8745
	ld	%r2, 5(%r63)		# 8746
	sto	%r2, 3(%r1)		# 8747
	ld	%r2, 4(%r63)		# 8748
	sto	%r2, 2(%r1)		# 8749
	ld	%r2, 3(%r63)		# 8750
	sto	%r2, 1(%r1)		# 8751
	ld	%r2, 2(%r63)		# 8752
	sto	%r2, (%r1)		# 8753
	ld	%r2, (%r63)		# 8754
	ld	%r3, 1(%r63)		# 8755
	add	%r4, %r3, %r2		# 8756
	sto	%r1, (%r4)		# 8757
	addi	%r2, %r2, -1		# 8758
	movi	%r1, 0		# 8759
	cmpgt	%r1, %r1, %r2		# 8760
	jmpi	%r1, else.16557		# 8761
	sto	%r2, 10(%r63)		# 8762
	addi	%r63, %r63, 11		# 8763
	call	%r60, create_pixel.2885		# 8764
	addi	%r63, %r63, -11		# 8765
	ld	%r2, 10(%r63)		# 8766
	ld	%r3, 1(%r63)		# 8767
	add	%r4, %r3, %r2		# 8768
	sto	%r1, (%r4)		# 8769
	addi	%r2, %r2, -1		# 8770
	mov	%r1, %r3		# 8771
	addi	%r63, %r63, -1		# 8772
	ld	%r60, (%r63)		# 8773
	jmpui	init_line_elements.2887		# 8774
else.16557:
	mov	%r1, %r3		# 8775
	addi	%r63, %r63, -1		# 8776
	ld	%r60, (%r63)		# 8777
	jmpu	%r60		# 8778
else.16556:
	addi	%r63, %r63, -1		# 8779
	ld	%r60, (%r63)		# 8780
	jmpu	%r60		# 8781
.globl	calc_dirvec.2897
calc_dirvec.2897:
	sto	%r60, (%r63)		# 8782
	addi	%r63, %r63, 1		# 8783
	movi	%r8, 5		# 8784
	cmpgt	%r8, %r8, %r1		# 8785
	jmpi	%r8, else.16558		# 8786
	fmul	%r1, %r4, %r4		# 8787
	fmul	%r6, %r5, %r5		# 8788
	fadd	%r1, %r1, %r6		# 8789
	movhiz	%r6, 260096		# 8790
	fadd	%r1, %r1, %r6		# 8791
	fsqrt	%r1, %r1		# 8792
	finv	%r6, %r1		# 8793
	fmul	%r4, %r4, %r6		# 8794
	finv	%r6, %r1		# 8795
	fmul	%r5, %r5, %r6		# 8796
	movhiz	%r6, 260096		# 8797
	finv	%r1, %r1		# 8798
	fmul	%r6, %r6, %r1		# 8799
	movi	%r1, min_caml_dirvecs		# 8800
	add	%r1, %r1, %r2		# 8801
	ld	%r1, (%r1)		# 8802
	add	%r2, %r1, %r3		# 8803
	ld	%r2, (%r2)		# 8804
	ld	%r2, (%r2)		# 8805
	addi	%r7, %r2, 0		# 8806
	sto	%r4, (%r7)		# 8807
	addi	%r7, %r2, 1		# 8808
	sto	%r5, (%r7)		# 8809
	addi	%r2, %r2, 2		# 8810
	sto	%r6, (%r2)		# 8811
	addi	%r2, %r3, 40		# 8812
	add	%r2, %r1, %r2		# 8813
	ld	%r2, (%r2)		# 8814
	ld	%r2, (%r2)		# 8815
	fneg	%r7, %r5		# 8816
	addi	%r8, %r2, 0		# 8817
	sto	%r4, (%r8)		# 8818
	addi	%r8, %r2, 1		# 8819
	sto	%r6, (%r8)		# 8820
	addi	%r2, %r2, 2		# 8821
	sto	%r7, (%r2)		# 8822
	addi	%r2, %r3, 80		# 8823
	add	%r2, %r1, %r2		# 8824
	ld	%r2, (%r2)		# 8825
	ld	%r2, (%r2)		# 8826
	fneg	%r7, %r4		# 8827
	fneg	%r8, %r5		# 8828
	addi	%r9, %r2, 0		# 8829
	sto	%r6, (%r9)		# 8830
	addi	%r9, %r2, 1		# 8831
	sto	%r7, (%r9)		# 8832
	addi	%r2, %r2, 2		# 8833
	sto	%r8, (%r2)		# 8834
	addi	%r2, %r3, 1		# 8835
	add	%r2, %r1, %r2		# 8836
	ld	%r2, (%r2)		# 8837
	ld	%r2, (%r2)		# 8838
	fneg	%r7, %r4		# 8839
	fneg	%r8, %r5		# 8840
	fneg	%r9, %r6		# 8841
	addi	%r10, %r2, 0		# 8842
	sto	%r7, (%r10)		# 8843
	addi	%r7, %r2, 1		# 8844
	sto	%r8, (%r7)		# 8845
	addi	%r2, %r2, 2		# 8846
	sto	%r9, (%r2)		# 8847
	addi	%r2, %r3, 41		# 8848
	add	%r2, %r1, %r2		# 8849
	ld	%r2, (%r2)		# 8850
	ld	%r2, (%r2)		# 8851
	fneg	%r7, %r4		# 8852
	fneg	%r8, %r6		# 8853
	addi	%r9, %r2, 0		# 8854
	sto	%r7, (%r9)		# 8855
	addi	%r7, %r2, 1		# 8856
	sto	%r8, (%r7)		# 8857
	addi	%r2, %r2, 2		# 8858
	sto	%r5, (%r2)		# 8859
	addi	%r3, %r3, 81		# 8860
	add	%r1, %r1, %r3		# 8861
	ld	%r1, (%r1)		# 8862
	ld	%r1, (%r1)		# 8863
	fneg	%r6, %r6		# 8864
	addi	%r2, %r1, 0		# 8865
	sto	%r6, (%r2)		# 8866
	addi	%r2, %r1, 1		# 8867
	sto	%r4, (%r2)		# 8868
	addi	%r1, %r1, 2		# 8869
	sto	%r5, (%r1)		# 8870
	addi	%r63, %r63, -1		# 8871
	ld	%r60, (%r63)		# 8872
	jmpu	%r60		# 8873
else.16558:
	fmul	%r5, %r5, %r5		# 8874
	movhiz	%r4, 253132		# 8875
	addi	%r4, %r4, 3277		# 8876
	fadd	%r5, %r5, %r4		# 8877
	fsqrt	%r5, %r5		# 8878
	movhiz	%r4, 260096		# 8879
	finv	%r8, %r5		# 8880
	fmul	%r4, %r4, %r8		# 8881
	sto	%r3, (%r63)		# 8882
	sto	%r2, 1(%r63)		# 8883
	sto	%r7, 2(%r63)		# 8884
	sto	%r1, 3(%r63)		# 8885
	sto	%r5, 4(%r63)		# 8886
	sto	%r6, 5(%r63)		# 8887
	mov	%r1, %r4		# 8888
	addi	%r63, %r63, 6		# 8889
	call	%r60, min_caml_atan		# 8890
	addi	%r63, %r63, -6		# 8891
	ld	%r2, 5(%r63)		# 8892
	fmul	%r1, %r1, %r2		# 8893
	sto	%r1, 6(%r63)		# 8894
	addi	%r63, %r63, 7		# 8895
	call	%r60, min_caml_sin		# 8896
	addi	%r63, %r63, -7		# 8897
	ld	%r2, 6(%r63)		# 8898
	sto	%r1, 7(%r63)		# 8899
	mov	%r1, %r2		# 8900
	addi	%r63, %r63, 8		# 8901
	call	%r60, min_caml_cos		# 8902
	addi	%r63, %r63, -8		# 8903
	finv	%r1, %r1		# 8904
	ld	%r2, 7(%r63)		# 8905
	fmul	%r2, %r2, %r1		# 8906
	ld	%r1, 4(%r63)		# 8907
	fmul	%r2, %r2, %r1		# 8908
	ld	%r1, 3(%r63)		# 8909
	addi	%r1, %r1, 1		# 8910
	fmul	%r3, %r2, %r2		# 8911
	movhiz	%r4, 253132		# 8912
	addi	%r4, %r4, 3277		# 8913
	fadd	%r3, %r3, %r4		# 8914
	fsqrt	%r3, %r3		# 8915
	movhiz	%r4, 260096		# 8916
	finv	%r5, %r3		# 8917
	fmul	%r4, %r4, %r5		# 8918
	sto	%r2, 8(%r63)		# 8919
	sto	%r1, 9(%r63)		# 8920
	sto	%r3, 10(%r63)		# 8921
	mov	%r1, %r4		# 8922
	addi	%r63, %r63, 11		# 8923
	call	%r60, min_caml_atan		# 8924
	addi	%r63, %r63, -11		# 8925
	ld	%r2, 2(%r63)		# 8926
	fmul	%r1, %r1, %r2		# 8927
	sto	%r1, 11(%r63)		# 8928
	addi	%r63, %r63, 12		# 8929
	call	%r60, min_caml_sin		# 8930
	addi	%r63, %r63, -12		# 8931
	ld	%r2, 11(%r63)		# 8932
	sto	%r1, 12(%r63)		# 8933
	mov	%r1, %r2		# 8934
	addi	%r63, %r63, 13		# 8935
	call	%r60, min_caml_cos		# 8936
	addi	%r63, %r63, -13		# 8937
	finv	%r1, %r1		# 8938
	ld	%r2, 12(%r63)		# 8939
	fmul	%r2, %r2, %r1		# 8940
	ld	%r1, 10(%r63)		# 8941
	fmul	%r5, %r2, %r1		# 8942
	ld	%r4, 8(%r63)		# 8943
	ld	%r6, 5(%r63)		# 8944
	ld	%r7, 2(%r63)		# 8945
	ld	%r1, 9(%r63)		# 8946
	ld	%r2, 1(%r63)		# 8947
	ld	%r3, (%r63)		# 8948
	addi	%r63, %r63, -1		# 8949
	ld	%r60, (%r63)		# 8950
	jmpui	calc_dirvec.2897		# 8951
.globl	calc_dirvecs.2905
calc_dirvecs.2905:
	sto	%r60, (%r63)		# 8952
	addi	%r63, %r63, 1		# 8953
	movi	%r5, 0		# 8954
	cmpgt	%r5, %r5, %r1		# 8955
	jmpi	%r5, else.16560		# 8956
	sto	%r1, (%r63)		# 8957
	sto	%r4, 1(%r63)		# 8958
	sto	%r3, 2(%r63)		# 8959
	sto	%r2, 3(%r63)		# 8960
	addi	%r63, %r63, 4		# 8961
	call	%r60, min_caml_float_of_int		# 8962
	addi	%r63, %r63, -4		# 8963
	movhiz	%r2, 255180		# 8964
	addi	%r2, %r2, 3277		# 8965
	fmul	%r1, %r1, %r2		# 8966
	movhiz	%r2, 259686		# 8967
	addi	%r2, %r2, 1638		# 8968
	fsub	%r6, %r1, %r2		# 8969
	movi	%r1, 0		# 8970
	movi	%r4, 0		# 8971
	movi	%r5, 0		# 8972
	ld	%r7, 1(%r63)		# 8973
	ld	%r2, 3(%r63)		# 8974
	ld	%r3, 2(%r63)		# 8975
	addi	%r63, %r63, 4		# 8976
	call	%r60, calc_dirvec.2897		# 8977
	addi	%r63, %r63, -4		# 8978
	ld	%r1, (%r63)		# 8979
	addi	%r63, %r63, 4		# 8980
	call	%r60, min_caml_float_of_int		# 8981
	addi	%r63, %r63, -4		# 8982
	movhiz	%r2, 255180		# 8983
	addi	%r2, %r2, 3277		# 8984
	fmul	%r1, %r1, %r2		# 8985
	movhiz	%r2, 253132		# 8986
	addi	%r2, %r2, 3277		# 8987
	fadd	%r6, %r1, %r2		# 8988
	movi	%r1, 0		# 8989
	movi	%r4, 0		# 8990
	movi	%r5, 0		# 8991
	ld	%r2, 2(%r63)		# 8992
	addi	%r3, %r2, 2		# 8993
	ld	%r7, 1(%r63)		# 8994
	ld	%r8, 3(%r63)		# 8995
	mov	%r2, %r8		# 8996
	addi	%r63, %r63, 4		# 8997
	call	%r60, calc_dirvec.2897		# 8998
	addi	%r63, %r63, -4		# 8999
	ld	%r1, (%r63)		# 9000
	addi	%r1, %r1, -1		# 9001
	ld	%r2, 3(%r63)		# 9002
	addi	%r2, %r2, 1		# 9003
	movi	%r3, 5		# 9004
	cmpgt	%r3, %r3, %r2		# 9005
	jmpi	%r3, else.16561		# 9006
	addi	%r2, %r2, -5		# 9007
	jmpui	if_cont.16562		# 9008
else.16561:
if_cont.16562:
	ld	%r4, 1(%r63)		# 9009
	ld	%r3, 2(%r63)		# 9010
	addi	%r63, %r63, -1		# 9011
	ld	%r60, (%r63)		# 9012
	jmpui	calc_dirvecs.2905		# 9013
else.16560:
	addi	%r63, %r63, -1		# 9014
	ld	%r60, (%r63)		# 9015
	jmpu	%r60		# 9016
.globl	calc_dirvec_rows.2910
calc_dirvec_rows.2910:
	sto	%r60, (%r63)		# 9017
	addi	%r63, %r63, 1		# 9018
	movi	%r4, 0		# 9019
	cmpgt	%r4, %r4, %r1		# 9020
	jmpi	%r4, else.16564		# 9021
	sto	%r1, (%r63)		# 9022
	sto	%r3, 1(%r63)		# 9023
	sto	%r2, 2(%r63)		# 9024
	addi	%r63, %r63, 3		# 9025
	call	%r60, min_caml_float_of_int		# 9026
	addi	%r63, %r63, -3		# 9027
	movhiz	%r2, 255180		# 9028
	addi	%r2, %r2, 3277		# 9029
	fmul	%r1, %r1, %r2		# 9030
	movhiz	%r2, 259686		# 9031
	addi	%r2, %r2, 1638		# 9032
	fsub	%r4, %r1, %r2		# 9033
	movi	%r1, 4		# 9034
	ld	%r2, 2(%r63)		# 9035
	ld	%r3, 1(%r63)		# 9036
	addi	%r63, %r63, 3		# 9037
	call	%r60, calc_dirvecs.2905		# 9038
	addi	%r63, %r63, -3		# 9039
	ld	%r1, (%r63)		# 9040
	addi	%r1, %r1, -1		# 9041
	ld	%r2, 2(%r63)		# 9042
	addi	%r2, %r2, 2		# 9043
	movi	%r3, 5		# 9044
	cmpgt	%r3, %r3, %r2		# 9045
	jmpi	%r3, else.16565		# 9046
	addi	%r2, %r2, -5		# 9047
	jmpui	if_cont.16566		# 9048
else.16565:
if_cont.16566:
	ld	%r3, 1(%r63)		# 9049
	addi	%r3, %r3, 4		# 9050
	movi	%r4, 0		# 9051
	cmpgt	%r4, %r4, %r1		# 9052
	jmpi	%r4, else.16567		# 9053
	sto	%r1, 3(%r63)		# 9054
	sto	%r3, 4(%r63)		# 9055
	sto	%r2, 5(%r63)		# 9056
	addi	%r63, %r63, 6		# 9057
	call	%r60, min_caml_float_of_int		# 9058
	addi	%r63, %r63, -6		# 9059
	movhiz	%r2, 255180		# 9060
	addi	%r2, %r2, 3277		# 9061
	fmul	%r1, %r1, %r2		# 9062
	movhiz	%r2, 259686		# 9063
	addi	%r2, %r2, 1638		# 9064
	fsub	%r4, %r1, %r2		# 9065
	movi	%r1, 4		# 9066
	ld	%r2, 5(%r63)		# 9067
	ld	%r3, 4(%r63)		# 9068
	addi	%r63, %r63, 6		# 9069
	call	%r60, calc_dirvecs.2905		# 9070
	addi	%r63, %r63, -6		# 9071
	ld	%r1, 3(%r63)		# 9072
	addi	%r1, %r1, -1		# 9073
	ld	%r2, 5(%r63)		# 9074
	addi	%r2, %r2, 2		# 9075
	movi	%r3, 5		# 9076
	cmpgt	%r3, %r3, %r2		# 9077
	jmpi	%r3, else.16568		# 9078
	addi	%r2, %r2, -5		# 9079
	jmpui	if_cont.16569		# 9080
else.16568:
if_cont.16569:
	ld	%r3, 4(%r63)		# 9081
	addi	%r3, %r3, 4		# 9082
	addi	%r63, %r63, -1		# 9083
	ld	%r60, (%r63)		# 9084
	jmpui	calc_dirvec_rows.2910		# 9085
else.16567:
	addi	%r63, %r63, -1		# 9086
	ld	%r60, (%r63)		# 9087
	jmpu	%r60		# 9088
else.16564:
	addi	%r63, %r63, -1		# 9089
	ld	%r60, (%r63)		# 9090
	jmpu	%r60		# 9091
.globl	create_dirvec_elements.2916
create_dirvec_elements.2916:
	sto	%r60, (%r63)		# 9092
	addi	%r63, %r63, 1		# 9093
	movi	%r3, 0		# 9094
	cmpgt	%r3, %r3, %r2		# 9095
	jmpi	%r3, else.16572		# 9096
	movi	%r3, 0		# 9097
	movi	%r4, 3		# 9098
	sto	%r2, (%r63)		# 9099
	sto	%r1, 1(%r63)		# 9100
	mov	%r2, %r3		# 9101
	mov	%r1, %r4		# 9102
	addi	%r63, %r63, 2		# 9103
	call	%r60, min_caml_create_float_array		# 9104
	addi	%r63, %r63, -2		# 9105
	mov	%r2, %r1		# 9106
	movi	%r1, min_caml_n_objects		# 9107
	addi	%r1, %r1, 0		# 9108
	ld	%r1, (%r1)		# 9109
	sto	%r2, 2(%r63)		# 9110
	addi	%r63, %r63, 3		# 9111
	call	%r60, min_caml_create_array		# 9112
	addi	%r63, %r63, -3		# 9113
	mov	%r2, %r62		# 9114
	addi	%r62, %r62, 2		# 9115
	sto	%r1, 1(%r2)		# 9116
	ld	%r1, 2(%r63)		# 9117
	sto	%r1, (%r2)		# 9118
	ld	%r1, (%r63)		# 9119
	ld	%r3, 1(%r63)		# 9120
	add	%r4, %r3, %r1		# 9121
	sto	%r2, (%r4)		# 9122
	addi	%r1, %r1, -1		# 9123
	movi	%r2, 0		# 9124
	cmpgt	%r2, %r2, %r1		# 9125
	jmpi	%r2, else.16573		# 9126
	movi	%r2, 0		# 9127
	movi	%r4, 3		# 9128
	sto	%r1, 3(%r63)		# 9129
	mov	%r1, %r4		# 9130
	addi	%r63, %r63, 4		# 9131
	call	%r60, min_caml_create_float_array		# 9132
	addi	%r63, %r63, -4		# 9133
	mov	%r2, %r1		# 9134
	movi	%r1, min_caml_n_objects		# 9135
	addi	%r1, %r1, 0		# 9136
	ld	%r1, (%r1)		# 9137
	sto	%r2, 4(%r63)		# 9138
	addi	%r63, %r63, 5		# 9139
	call	%r60, min_caml_create_array		# 9140
	addi	%r63, %r63, -5		# 9141
	mov	%r2, %r62		# 9142
	addi	%r62, %r62, 2		# 9143
	sto	%r1, 1(%r2)		# 9144
	ld	%r1, 4(%r63)		# 9145
	sto	%r1, (%r2)		# 9146
	ld	%r1, 3(%r63)		# 9147
	ld	%r3, 1(%r63)		# 9148
	add	%r4, %r3, %r1		# 9149
	sto	%r2, (%r4)		# 9150
	addi	%r1, %r1, -1		# 9151
	movi	%r2, 0		# 9152
	cmpgt	%r2, %r2, %r1		# 9153
	jmpi	%r2, else.16574		# 9154
	movi	%r2, 0		# 9155
	movi	%r4, 3		# 9156
	sto	%r1, 5(%r63)		# 9157
	mov	%r1, %r4		# 9158
	addi	%r63, %r63, 6		# 9159
	call	%r60, min_caml_create_float_array		# 9160
	addi	%r63, %r63, -6		# 9161
	mov	%r2, %r1		# 9162
	movi	%r1, min_caml_n_objects		# 9163
	addi	%r1, %r1, 0		# 9164
	ld	%r1, (%r1)		# 9165
	sto	%r2, 6(%r63)		# 9166
	addi	%r63, %r63, 7		# 9167
	call	%r60, min_caml_create_array		# 9168
	addi	%r63, %r63, -7		# 9169
	mov	%r2, %r62		# 9170
	addi	%r62, %r62, 2		# 9171
	sto	%r1, 1(%r2)		# 9172
	ld	%r1, 6(%r63)		# 9173
	sto	%r1, (%r2)		# 9174
	ld	%r1, 5(%r63)		# 9175
	ld	%r3, 1(%r63)		# 9176
	add	%r4, %r3, %r1		# 9177
	sto	%r2, (%r4)		# 9178
	addi	%r1, %r1, -1		# 9179
	movi	%r2, 0		# 9180
	cmpgt	%r2, %r2, %r1		# 9181
	jmpi	%r2, else.16575		# 9182
	movi	%r2, 0		# 9183
	movi	%r4, 3		# 9184
	sto	%r1, 7(%r63)		# 9185
	mov	%r1, %r4		# 9186
	addi	%r63, %r63, 8		# 9187
	call	%r60, min_caml_create_float_array		# 9188
	addi	%r63, %r63, -8		# 9189
	mov	%r2, %r1		# 9190
	movi	%r1, min_caml_n_objects		# 9191
	addi	%r1, %r1, 0		# 9192
	ld	%r1, (%r1)		# 9193
	sto	%r2, 8(%r63)		# 9194
	addi	%r63, %r63, 9		# 9195
	call	%r60, min_caml_create_array		# 9196
	addi	%r63, %r63, -9		# 9197
	mov	%r2, %r62		# 9198
	addi	%r62, %r62, 2		# 9199
	sto	%r1, 1(%r2)		# 9200
	ld	%r1, 8(%r63)		# 9201
	sto	%r1, (%r2)		# 9202
	ld	%r1, 7(%r63)		# 9203
	ld	%r3, 1(%r63)		# 9204
	add	%r4, %r3, %r1		# 9205
	sto	%r2, (%r4)		# 9206
	addi	%r2, %r1, -1		# 9207
	mov	%r1, %r3		# 9208
	addi	%r63, %r63, -1		# 9209
	ld	%r60, (%r63)		# 9210
	jmpui	create_dirvec_elements.2916		# 9211
else.16575:
	addi	%r63, %r63, -1		# 9212
	ld	%r60, (%r63)		# 9213
	jmpu	%r60		# 9214
else.16574:
	addi	%r63, %r63, -1		# 9215
	ld	%r60, (%r63)		# 9216
	jmpu	%r60		# 9217
else.16573:
	addi	%r63, %r63, -1		# 9218
	ld	%r60, (%r63)		# 9219
	jmpu	%r60		# 9220
else.16572:
	addi	%r63, %r63, -1		# 9221
	ld	%r60, (%r63)		# 9222
	jmpu	%r60		# 9223
.globl	create_dirvecs.2919
create_dirvecs.2919:
	sto	%r60, (%r63)		# 9224
	addi	%r63, %r63, 1		# 9225
	movi	%r2, 0		# 9226
	cmpgt	%r2, %r2, %r1		# 9227
	jmpi	%r2, else.16580		# 9228
	movi	%r2, min_caml_dirvecs		# 9229
	movi	%r3, 0		# 9230
	movi	%r4, 3		# 9231
	sto	%r1, (%r63)		# 9232
	sto	%r2, 1(%r63)		# 9233
	mov	%r2, %r3		# 9234
	mov	%r1, %r4		# 9235
	addi	%r63, %r63, 2		# 9236
	call	%r60, min_caml_create_float_array		# 9237
	addi	%r63, %r63, -2		# 9238
	mov	%r2, %r1		# 9239
	movi	%r1, min_caml_n_objects		# 9240
	addi	%r1, %r1, 0		# 9241
	ld	%r1, (%r1)		# 9242
	sto	%r2, 2(%r63)		# 9243
	addi	%r63, %r63, 3		# 9244
	call	%r60, min_caml_create_array		# 9245
	addi	%r63, %r63, -3		# 9246
	mov	%r2, %r62		# 9247
	addi	%r62, %r62, 2		# 9248
	sto	%r1, 1(%r2)		# 9249
	ld	%r1, 2(%r63)		# 9250
	sto	%r1, (%r2)		# 9251
	movi	%r1, 120		# 9252
	addi	%r63, %r63, 3		# 9253
	call	%r60, min_caml_create_array		# 9254
	addi	%r63, %r63, -3		# 9255
	ld	%r2, (%r63)		# 9256
	ld	%r3, 1(%r63)		# 9257
	add	%r3, %r3, %r2		# 9258
	sto	%r1, (%r3)		# 9259
	movi	%r1, min_caml_dirvecs		# 9260
	add	%r1, %r1, %r2		# 9261
	ld	%r1, (%r1)		# 9262
	movi	%r3, 0		# 9263
	movi	%r4, 3		# 9264
	sto	%r1, 3(%r63)		# 9265
	mov	%r2, %r3		# 9266
	mov	%r1, %r4		# 9267
	addi	%r63, %r63, 4		# 9268
	call	%r60, min_caml_create_float_array		# 9269
	addi	%r63, %r63, -4		# 9270
	mov	%r2, %r1		# 9271
	movi	%r1, min_caml_n_objects		# 9272
	addi	%r1, %r1, 0		# 9273
	ld	%r1, (%r1)		# 9274
	sto	%r2, 4(%r63)		# 9275
	addi	%r63, %r63, 5		# 9276
	call	%r60, min_caml_create_array		# 9277
	addi	%r63, %r63, -5		# 9278
	mov	%r2, %r62		# 9279
	addi	%r62, %r62, 2		# 9280
	sto	%r1, 1(%r2)		# 9281
	ld	%r1, 4(%r63)		# 9282
	sto	%r1, (%r2)		# 9283
	ld	%r1, 3(%r63)		# 9284
	addi	%r3, %r1, 118		# 9285
	sto	%r2, (%r3)		# 9286
	movi	%r2, 0		# 9287
	movi	%r3, 3		# 9288
	mov	%r1, %r3		# 9289
	addi	%r63, %r63, 5		# 9290
	call	%r60, min_caml_create_float_array		# 9291
	addi	%r63, %r63, -5		# 9292
	mov	%r2, %r1		# 9293
	movi	%r1, min_caml_n_objects		# 9294
	addi	%r1, %r1, 0		# 9295
	ld	%r1, (%r1)		# 9296
	sto	%r2, 5(%r63)		# 9297
	addi	%r63, %r63, 6		# 9298
	call	%r60, min_caml_create_array		# 9299
	addi	%r63, %r63, -6		# 9300
	mov	%r2, %r62		# 9301
	addi	%r62, %r62, 2		# 9302
	sto	%r1, 1(%r2)		# 9303
	ld	%r1, 5(%r63)		# 9304
	sto	%r1, (%r2)		# 9305
	ld	%r1, 3(%r63)		# 9306
	addi	%r3, %r1, 117		# 9307
	sto	%r2, (%r3)		# 9308
	movi	%r2, 0		# 9309
	movi	%r3, 3		# 9310
	mov	%r1, %r3		# 9311
	addi	%r63, %r63, 6		# 9312
	call	%r60, min_caml_create_float_array		# 9313
	addi	%r63, %r63, -6		# 9314
	mov	%r2, %r1		# 9315
	movi	%r1, min_caml_n_objects		# 9316
	addi	%r1, %r1, 0		# 9317
	ld	%r1, (%r1)		# 9318
	sto	%r2, 6(%r63)		# 9319
	addi	%r63, %r63, 7		# 9320
	call	%r60, min_caml_create_array		# 9321
	addi	%r63, %r63, -7		# 9322
	mov	%r2, %r62		# 9323
	addi	%r62, %r62, 2		# 9324
	sto	%r1, 1(%r2)		# 9325
	ld	%r1, 6(%r63)		# 9326
	sto	%r1, (%r2)		# 9327
	ld	%r1, 3(%r63)		# 9328
	addi	%r3, %r1, 116		# 9329
	sto	%r2, (%r3)		# 9330
	movi	%r2, 115		# 9331
	addi	%r63, %r63, 7		# 9332
	call	%r60, create_dirvec_elements.2916		# 9333
	addi	%r63, %r63, -7		# 9334
	ld	%r1, (%r63)		# 9335
	addi	%r1, %r1, -1		# 9336
	movi	%r2, 0		# 9337
	cmpgt	%r2, %r2, %r1		# 9338
	jmpi	%r2, else.16581		# 9339
	movi	%r2, min_caml_dirvecs		# 9340
	movi	%r3, 0		# 9341
	movi	%r4, 3		# 9342
	sto	%r1, 7(%r63)		# 9343
	sto	%r2, 8(%r63)		# 9344
	mov	%r2, %r3		# 9345
	mov	%r1, %r4		# 9346
	addi	%r63, %r63, 9		# 9347
	call	%r60, min_caml_create_float_array		# 9348
	addi	%r63, %r63, -9		# 9349
	mov	%r2, %r1		# 9350
	movi	%r1, min_caml_n_objects		# 9351
	addi	%r1, %r1, 0		# 9352
	ld	%r1, (%r1)		# 9353
	sto	%r2, 9(%r63)		# 9354
	addi	%r63, %r63, 10		# 9355
	call	%r60, min_caml_create_array		# 9356
	addi	%r63, %r63, -10		# 9357
	mov	%r2, %r62		# 9358
	addi	%r62, %r62, 2		# 9359
	sto	%r1, 1(%r2)		# 9360
	ld	%r1, 9(%r63)		# 9361
	sto	%r1, (%r2)		# 9362
	movi	%r1, 120		# 9363
	addi	%r63, %r63, 10		# 9364
	call	%r60, min_caml_create_array		# 9365
	addi	%r63, %r63, -10		# 9366
	ld	%r2, 7(%r63)		# 9367
	ld	%r3, 8(%r63)		# 9368
	add	%r3, %r3, %r2		# 9369
	sto	%r1, (%r3)		# 9370
	movi	%r1, min_caml_dirvecs		# 9371
	add	%r1, %r1, %r2		# 9372
	ld	%r1, (%r1)		# 9373
	movi	%r3, 0		# 9374
	movi	%r4, 3		# 9375
	sto	%r1, 10(%r63)		# 9376
	mov	%r2, %r3		# 9377
	mov	%r1, %r4		# 9378
	addi	%r63, %r63, 11		# 9379
	call	%r60, min_caml_create_float_array		# 9380
	addi	%r63, %r63, -11		# 9381
	mov	%r2, %r1		# 9382
	movi	%r1, min_caml_n_objects		# 9383
	addi	%r1, %r1, 0		# 9384
	ld	%r1, (%r1)		# 9385
	sto	%r2, 11(%r63)		# 9386
	addi	%r63, %r63, 12		# 9387
	call	%r60, min_caml_create_array		# 9388
	addi	%r63, %r63, -12		# 9389
	mov	%r2, %r62		# 9390
	addi	%r62, %r62, 2		# 9391
	sto	%r1, 1(%r2)		# 9392
	ld	%r1, 11(%r63)		# 9393
	sto	%r1, (%r2)		# 9394
	ld	%r1, 10(%r63)		# 9395
	addi	%r3, %r1, 118		# 9396
	sto	%r2, (%r3)		# 9397
	movi	%r2, 0		# 9398
	movi	%r3, 3		# 9399
	mov	%r1, %r3		# 9400
	addi	%r63, %r63, 12		# 9401
	call	%r60, min_caml_create_float_array		# 9402
	addi	%r63, %r63, -12		# 9403
	mov	%r2, %r1		# 9404
	movi	%r1, min_caml_n_objects		# 9405
	addi	%r1, %r1, 0		# 9406
	ld	%r1, (%r1)		# 9407
	sto	%r2, 12(%r63)		# 9408
	addi	%r63, %r63, 13		# 9409
	call	%r60, min_caml_create_array		# 9410
	addi	%r63, %r63, -13		# 9411
	mov	%r2, %r62		# 9412
	addi	%r62, %r62, 2		# 9413
	sto	%r1, 1(%r2)		# 9414
	ld	%r1, 12(%r63)		# 9415
	sto	%r1, (%r2)		# 9416
	ld	%r1, 10(%r63)		# 9417
	addi	%r3, %r1, 117		# 9418
	sto	%r2, (%r3)		# 9419
	movi	%r2, 116		# 9420
	addi	%r63, %r63, 13		# 9421
	call	%r60, create_dirvec_elements.2916		# 9422
	addi	%r63, %r63, -13		# 9423
	ld	%r1, 7(%r63)		# 9424
	addi	%r1, %r1, -1		# 9425
	addi	%r63, %r63, -1		# 9426
	ld	%r60, (%r63)		# 9427
	jmpui	create_dirvecs.2919		# 9428
else.16581:
	addi	%r63, %r63, -1		# 9429
	ld	%r60, (%r63)		# 9430
	jmpu	%r60		# 9431
else.16580:
	addi	%r63, %r63, -1		# 9432
	ld	%r60, (%r63)		# 9433
	jmpu	%r60		# 9434
.globl	init_dirvec_constants.2921
init_dirvec_constants.2921:
	sto	%r60, (%r63)		# 9435
	addi	%r63, %r63, 1		# 9436
	movi	%r3, 0		# 9437
	cmpgt	%r3, %r3, %r2		# 9438
	jmpi	%r3, else.16584		# 9439
	add	%r3, %r1, %r2		# 9440
	ld	%r3, (%r3)		# 9441
	movi	%r4, min_caml_n_objects		# 9442
	addi	%r4, %r4, 0		# 9443
	ld	%r4, (%r4)		# 9444
	addi	%r4, %r4, -1		# 9445
	sto	%r1, (%r63)		# 9446
	sto	%r2, 1(%r63)		# 9447
	mov	%r2, %r4		# 9448
	mov	%r1, %r3		# 9449
	addi	%r63, %r63, 2		# 9450
	call	%r60, iter_setup_dirvec_constants.2703		# 9451
	addi	%r63, %r63, -2		# 9452
	ld	%r1, 1(%r63)		# 9453
	addi	%r1, %r1, -1		# 9454
	movi	%r2, 0		# 9455
	cmpgt	%r2, %r2, %r1		# 9456
	jmpi	%r2, else.16585		# 9457
	ld	%r2, (%r63)		# 9458
	add	%r3, %r2, %r1		# 9459
	ld	%r3, (%r3)		# 9460
	movi	%r4, min_caml_n_objects		# 9461
	addi	%r4, %r4, 0		# 9462
	ld	%r4, (%r4)		# 9463
	addi	%r4, %r4, -1		# 9464
	movi	%r5, 0		# 9465
	cmpgt	%r5, %r5, %r4		# 9466
	sto	%r1, 2(%r63)		# 9467
	jmpi	%r5, else.16586		# 9468
	movi	%r5, min_caml_objects		# 9469
	add	%r5, %r5, %r4		# 9470
	ld	%r5, (%r5)		# 9471
	ld	%r6, 1(%r3)		# 9472
	ld	%r7, (%r3)		# 9473
	ld	%r8, 1(%r5)		# 9474
	cmpeqi	%r9, %r8, 1		# 9475
	sto	%r3, 3(%r63)		# 9476
	jmpzi	%r9, else.16588		# 9477
	sto	%r4, 4(%r63)		# 9478
	sto	%r6, 5(%r63)		# 9479
	mov	%r2, %r5		# 9480
	mov	%r1, %r7		# 9481
	addi	%r63, %r63, 6		# 9482
	call	%r60, setup_rect_table.2694		# 9483
	addi	%r63, %r63, -6		# 9484
	ld	%r2, 4(%r63)		# 9485
	ld	%r3, 5(%r63)		# 9486
	add	%r3, %r3, %r2		# 9487
	sto	%r1, (%r3)		# 9488
	jmpui	if_cont.16589		# 9489
else.16588:
	cmpeqi	%r8, %r8, 2		# 9490
	jmpzi	%r8, else.16590		# 9491
	sto	%r4, 4(%r63)		# 9492
	sto	%r6, 5(%r63)		# 9493
	mov	%r2, %r5		# 9494
	mov	%r1, %r7		# 9495
	addi	%r63, %r63, 6		# 9496
	call	%r60, setup_surface_table.2697		# 9497
	addi	%r63, %r63, -6		# 9498
	ld	%r2, 4(%r63)		# 9499
	ld	%r3, 5(%r63)		# 9500
	add	%r3, %r3, %r2		# 9501
	sto	%r1, (%r3)		# 9502
	jmpui	if_cont.16591		# 9503
else.16590:
	sto	%r4, 4(%r63)		# 9504
	sto	%r6, 5(%r63)		# 9505
	mov	%r2, %r5		# 9506
	mov	%r1, %r7		# 9507
	addi	%r63, %r63, 6		# 9508
	call	%r60, setup_second_table.2700		# 9509
	addi	%r63, %r63, -6		# 9510
	ld	%r2, 4(%r63)		# 9511
	ld	%r3, 5(%r63)		# 9512
	add	%r3, %r3, %r2		# 9513
	sto	%r1, (%r3)		# 9514
if_cont.16591:
if_cont.16589:
	addi	%r2, %r2, -1		# 9515
	ld	%r1, 3(%r63)		# 9516
	addi	%r63, %r63, 6		# 9517
	call	%r60, iter_setup_dirvec_constants.2703		# 9518
	addi	%r63, %r63, -6		# 9519
	jmpui	if_cont.16587		# 9520
else.16586:
if_cont.16587:
	ld	%r1, 2(%r63)		# 9521
	addi	%r1, %r1, -1		# 9522
	movi	%r2, 0		# 9523
	cmpgt	%r2, %r2, %r1		# 9524
	jmpi	%r2, else.16592		# 9525
	ld	%r2, (%r63)		# 9526
	add	%r3, %r2, %r1		# 9527
	ld	%r3, (%r3)		# 9528
	movi	%r4, min_caml_n_objects		# 9529
	addi	%r4, %r4, 0		# 9530
	ld	%r4, (%r4)		# 9531
	addi	%r4, %r4, -1		# 9532
	sto	%r1, 6(%r63)		# 9533
	mov	%r2, %r4		# 9534
	mov	%r1, %r3		# 9535
	addi	%r63, %r63, 7		# 9536
	call	%r60, iter_setup_dirvec_constants.2703		# 9537
	addi	%r63, %r63, -7		# 9538
	ld	%r1, 6(%r63)		# 9539
	addi	%r1, %r1, -1		# 9540
	movi	%r2, 0		# 9541
	cmpgt	%r2, %r2, %r1		# 9542
	jmpi	%r2, else.16593		# 9543
	ld	%r2, (%r63)		# 9544
	add	%r3, %r2, %r1		# 9545
	ld	%r3, (%r3)		# 9546
	movi	%r4, min_caml_n_objects		# 9547
	addi	%r4, %r4, 0		# 9548
	ld	%r4, (%r4)		# 9549
	addi	%r4, %r4, -1		# 9550
	movi	%r5, 0		# 9551
	cmpgt	%r5, %r5, %r4		# 9552
	sto	%r1, 7(%r63)		# 9553
	jmpi	%r5, else.16594		# 9554
	movi	%r5, min_caml_objects		# 9555
	add	%r5, %r5, %r4		# 9556
	ld	%r5, (%r5)		# 9557
	ld	%r6, 1(%r3)		# 9558
	ld	%r7, (%r3)		# 9559
	ld	%r8, 1(%r5)		# 9560
	cmpeqi	%r9, %r8, 1		# 9561
	sto	%r3, 8(%r63)		# 9562
	jmpzi	%r9, else.16596		# 9563
	sto	%r4, 9(%r63)		# 9564
	sto	%r6, 10(%r63)		# 9565
	mov	%r2, %r5		# 9566
	mov	%r1, %r7		# 9567
	addi	%r63, %r63, 11		# 9568
	call	%r60, setup_rect_table.2694		# 9569
	addi	%r63, %r63, -11		# 9570
	ld	%r2, 9(%r63)		# 9571
	ld	%r3, 10(%r63)		# 9572
	add	%r3, %r3, %r2		# 9573
	sto	%r1, (%r3)		# 9574
	jmpui	if_cont.16597		# 9575
else.16596:
	cmpeqi	%r8, %r8, 2		# 9576
	jmpzi	%r8, else.16598		# 9577
	sto	%r4, 9(%r63)		# 9578
	sto	%r6, 10(%r63)		# 9579
	mov	%r2, %r5		# 9580
	mov	%r1, %r7		# 9581
	addi	%r63, %r63, 11		# 9582
	call	%r60, setup_surface_table.2697		# 9583
	addi	%r63, %r63, -11		# 9584
	ld	%r2, 9(%r63)		# 9585
	ld	%r3, 10(%r63)		# 9586
	add	%r3, %r3, %r2		# 9587
	sto	%r1, (%r3)		# 9588
	jmpui	if_cont.16599		# 9589
else.16598:
	sto	%r4, 9(%r63)		# 9590
	sto	%r6, 10(%r63)		# 9591
	mov	%r2, %r5		# 9592
	mov	%r1, %r7		# 9593
	addi	%r63, %r63, 11		# 9594
	call	%r60, setup_second_table.2700		# 9595
	addi	%r63, %r63, -11		# 9596
	ld	%r2, 9(%r63)		# 9597
	ld	%r3, 10(%r63)		# 9598
	add	%r3, %r3, %r2		# 9599
	sto	%r1, (%r3)		# 9600
if_cont.16599:
if_cont.16597:
	addi	%r2, %r2, -1		# 9601
	ld	%r1, 8(%r63)		# 9602
	addi	%r63, %r63, 11		# 9603
	call	%r60, iter_setup_dirvec_constants.2703		# 9604
	addi	%r63, %r63, -11		# 9605
	jmpui	if_cont.16595		# 9606
else.16594:
if_cont.16595:
	ld	%r1, 7(%r63)		# 9607
	addi	%r2, %r1, -1		# 9608
	ld	%r1, (%r63)		# 9609
	addi	%r63, %r63, -1		# 9610
	ld	%r60, (%r63)		# 9611
	jmpui	init_dirvec_constants.2921		# 9612
else.16593:
	addi	%r63, %r63, -1		# 9613
	ld	%r60, (%r63)		# 9614
	jmpu	%r60		# 9615
else.16592:
	addi	%r63, %r63, -1		# 9616
	ld	%r60, (%r63)		# 9617
	jmpu	%r60		# 9618
else.16585:
	addi	%r63, %r63, -1		# 9619
	ld	%r60, (%r63)		# 9620
	jmpu	%r60		# 9621
else.16584:
	addi	%r63, %r63, -1		# 9622
	ld	%r60, (%r63)		# 9623
	jmpu	%r60		# 9624
.globl	init_vecset_constants.2924
init_vecset_constants.2924:
	sto	%r60, (%r63)		# 9625
	addi	%r63, %r63, 1		# 9626
	movi	%r2, 0		# 9627
	cmpgt	%r2, %r2, %r1		# 9628
	jmpi	%r2, else.16604		# 9629
	movi	%r2, min_caml_dirvecs		# 9630
	add	%r2, %r2, %r1		# 9631
	ld	%r2, (%r2)		# 9632
	addi	%r3, %r2, 119		# 9633
	ld	%r3, (%r3)		# 9634
	movi	%r4, min_caml_n_objects		# 9635
	addi	%r4, %r4, 0		# 9636
	ld	%r4, (%r4)		# 9637
	addi	%r4, %r4, -1		# 9638
	movi	%r5, 0		# 9639
	cmpgt	%r5, %r5, %r4		# 9640
	sto	%r1, (%r63)		# 9641
	sto	%r2, 1(%r63)		# 9642
	jmpi	%r5, else.16605		# 9643
	movi	%r5, min_caml_objects		# 9644
	add	%r5, %r5, %r4		# 9645
	ld	%r5, (%r5)		# 9646
	ld	%r6, 1(%r3)		# 9647
	ld	%r7, (%r3)		# 9648
	ld	%r8, 1(%r5)		# 9649
	cmpeqi	%r9, %r8, 1		# 9650
	sto	%r3, 2(%r63)		# 9651
	jmpzi	%r9, else.16607		# 9652
	sto	%r4, 3(%r63)		# 9653
	sto	%r6, 4(%r63)		# 9654
	mov	%r2, %r5		# 9655
	mov	%r1, %r7		# 9656
	addi	%r63, %r63, 5		# 9657
	call	%r60, setup_rect_table.2694		# 9658
	addi	%r63, %r63, -5		# 9659
	ld	%r2, 3(%r63)		# 9660
	ld	%r3, 4(%r63)		# 9661
	add	%r3, %r3, %r2		# 9662
	sto	%r1, (%r3)		# 9663
	jmpui	if_cont.16608		# 9664
else.16607:
	cmpeqi	%r8, %r8, 2		# 9665
	jmpzi	%r8, else.16609		# 9666
	sto	%r4, 3(%r63)		# 9667
	sto	%r6, 4(%r63)		# 9668
	mov	%r2, %r5		# 9669
	mov	%r1, %r7		# 9670
	addi	%r63, %r63, 5		# 9671
	call	%r60, setup_surface_table.2697		# 9672
	addi	%r63, %r63, -5		# 9673
	ld	%r2, 3(%r63)		# 9674
	ld	%r3, 4(%r63)		# 9675
	add	%r3, %r3, %r2		# 9676
	sto	%r1, (%r3)		# 9677
	jmpui	if_cont.16610		# 9678
else.16609:
	sto	%r4, 3(%r63)		# 9679
	sto	%r6, 4(%r63)		# 9680
	mov	%r2, %r5		# 9681
	mov	%r1, %r7		# 9682
	addi	%r63, %r63, 5		# 9683
	call	%r60, setup_second_table.2700		# 9684
	addi	%r63, %r63, -5		# 9685
	ld	%r2, 3(%r63)		# 9686
	ld	%r3, 4(%r63)		# 9687
	add	%r3, %r3, %r2		# 9688
	sto	%r1, (%r3)		# 9689
if_cont.16610:
if_cont.16608:
	addi	%r2, %r2, -1		# 9690
	ld	%r1, 2(%r63)		# 9691
	addi	%r63, %r63, 5		# 9692
	call	%r60, iter_setup_dirvec_constants.2703		# 9693
	addi	%r63, %r63, -5		# 9694
	jmpui	if_cont.16606		# 9695
else.16605:
if_cont.16606:
	ld	%r1, 1(%r63)		# 9696
	addi	%r2, %r1, 118		# 9697
	ld	%r2, (%r2)		# 9698
	movi	%r3, min_caml_n_objects		# 9699
	addi	%r3, %r3, 0		# 9700
	ld	%r3, (%r3)		# 9701
	addi	%r3, %r3, -1		# 9702
	mov	%r1, %r2		# 9703
	mov	%r2, %r3		# 9704
	addi	%r63, %r63, 5		# 9705
	call	%r60, iter_setup_dirvec_constants.2703		# 9706
	addi	%r63, %r63, -5		# 9707
	ld	%r1, 1(%r63)		# 9708
	addi	%r2, %r1, 117		# 9709
	ld	%r2, (%r2)		# 9710
	movi	%r3, min_caml_n_objects		# 9711
	addi	%r3, %r3, 0		# 9712
	ld	%r3, (%r3)		# 9713
	addi	%r3, %r3, -1		# 9714
	movi	%r4, 0		# 9715
	cmpgt	%r4, %r4, %r3		# 9716
	jmpi	%r4, else.16611		# 9717
	movi	%r4, min_caml_objects		# 9718
	add	%r4, %r4, %r3		# 9719
	ld	%r4, (%r4)		# 9720
	ld	%r5, 1(%r2)		# 9721
	ld	%r6, (%r2)		# 9722
	ld	%r7, 1(%r4)		# 9723
	cmpeqi	%r8, %r7, 1		# 9724
	sto	%r2, 5(%r63)		# 9725
	jmpzi	%r8, else.16613		# 9726
	sto	%r3, 6(%r63)		# 9727
	sto	%r5, 7(%r63)		# 9728
	mov	%r2, %r4		# 9729
	mov	%r1, %r6		# 9730
	addi	%r63, %r63, 8		# 9731
	call	%r60, setup_rect_table.2694		# 9732
	addi	%r63, %r63, -8		# 9733
	ld	%r2, 6(%r63)		# 9734
	ld	%r3, 7(%r63)		# 9735
	add	%r3, %r3, %r2		# 9736
	sto	%r1, (%r3)		# 9737
	jmpui	if_cont.16614		# 9738
else.16613:
	cmpeqi	%r7, %r7, 2		# 9739
	jmpzi	%r7, else.16615		# 9740
	sto	%r3, 6(%r63)		# 9741
	sto	%r5, 7(%r63)		# 9742
	mov	%r2, %r4		# 9743
	mov	%r1, %r6		# 9744
	addi	%r63, %r63, 8		# 9745
	call	%r60, setup_surface_table.2697		# 9746
	addi	%r63, %r63, -8		# 9747
	ld	%r2, 6(%r63)		# 9748
	ld	%r3, 7(%r63)		# 9749
	add	%r3, %r3, %r2		# 9750
	sto	%r1, (%r3)		# 9751
	jmpui	if_cont.16616		# 9752
else.16615:
	sto	%r3, 6(%r63)		# 9753
	sto	%r5, 7(%r63)		# 9754
	mov	%r2, %r4		# 9755
	mov	%r1, %r6		# 9756
	addi	%r63, %r63, 8		# 9757
	call	%r60, setup_second_table.2700		# 9758
	addi	%r63, %r63, -8		# 9759
	ld	%r2, 6(%r63)		# 9760
	ld	%r3, 7(%r63)		# 9761
	add	%r3, %r3, %r2		# 9762
	sto	%r1, (%r3)		# 9763
if_cont.16616:
if_cont.16614:
	addi	%r2, %r2, -1		# 9764
	ld	%r1, 5(%r63)		# 9765
	addi	%r63, %r63, 8		# 9766
	call	%r60, iter_setup_dirvec_constants.2703		# 9767
	addi	%r63, %r63, -8		# 9768
	jmpui	if_cont.16612		# 9769
else.16611:
if_cont.16612:
	movi	%r2, 116		# 9770
	ld	%r1, 1(%r63)		# 9771
	addi	%r63, %r63, 8		# 9772
	call	%r60, init_dirvec_constants.2921		# 9773
	addi	%r63, %r63, -8		# 9774
	ld	%r1, (%r63)		# 9775
	addi	%r1, %r1, -1		# 9776
	movi	%r2, 0		# 9777
	cmpgt	%r2, %r2, %r1		# 9778
	jmpi	%r2, else.16617		# 9779
	movi	%r2, min_caml_dirvecs		# 9780
	add	%r2, %r2, %r1		# 9781
	ld	%r2, (%r2)		# 9782
	addi	%r3, %r2, 119		# 9783
	ld	%r3, (%r3)		# 9784
	movi	%r4, min_caml_n_objects		# 9785
	addi	%r4, %r4, 0		# 9786
	ld	%r4, (%r4)		# 9787
	addi	%r4, %r4, -1		# 9788
	sto	%r1, 8(%r63)		# 9789
	sto	%r2, 9(%r63)		# 9790
	mov	%r2, %r4		# 9791
	mov	%r1, %r3		# 9792
	addi	%r63, %r63, 10		# 9793
	call	%r60, iter_setup_dirvec_constants.2703		# 9794
	addi	%r63, %r63, -10		# 9795
	ld	%r1, 9(%r63)		# 9796
	addi	%r2, %r1, 118		# 9797
	ld	%r2, (%r2)		# 9798
	movi	%r3, min_caml_n_objects		# 9799
	addi	%r3, %r3, 0		# 9800
	ld	%r3, (%r3)		# 9801
	addi	%r3, %r3, -1		# 9802
	movi	%r4, 0		# 9803
	cmpgt	%r4, %r4, %r3		# 9804
	jmpi	%r4, else.16618		# 9805
	movi	%r4, min_caml_objects		# 9806
	add	%r4, %r4, %r3		# 9807
	ld	%r4, (%r4)		# 9808
	ld	%r5, 1(%r2)		# 9809
	ld	%r6, (%r2)		# 9810
	ld	%r7, 1(%r4)		# 9811
	cmpeqi	%r8, %r7, 1		# 9812
	sto	%r2, 10(%r63)		# 9813
	jmpzi	%r8, else.16620		# 9814
	sto	%r3, 11(%r63)		# 9815
	sto	%r5, 12(%r63)		# 9816
	mov	%r2, %r4		# 9817
	mov	%r1, %r6		# 9818
	addi	%r63, %r63, 13		# 9819
	call	%r60, setup_rect_table.2694		# 9820
	addi	%r63, %r63, -13		# 9821
	ld	%r2, 11(%r63)		# 9822
	ld	%r3, 12(%r63)		# 9823
	add	%r3, %r3, %r2		# 9824
	sto	%r1, (%r3)		# 9825
	jmpui	if_cont.16621		# 9826
else.16620:
	cmpeqi	%r7, %r7, 2		# 9827
	jmpzi	%r7, else.16622		# 9828
	sto	%r3, 11(%r63)		# 9829
	sto	%r5, 12(%r63)		# 9830
	mov	%r2, %r4		# 9831
	mov	%r1, %r6		# 9832
	addi	%r63, %r63, 13		# 9833
	call	%r60, setup_surface_table.2697		# 9834
	addi	%r63, %r63, -13		# 9835
	ld	%r2, 11(%r63)		# 9836
	ld	%r3, 12(%r63)		# 9837
	add	%r3, %r3, %r2		# 9838
	sto	%r1, (%r3)		# 9839
	jmpui	if_cont.16623		# 9840
else.16622:
	sto	%r3, 11(%r63)		# 9841
	sto	%r5, 12(%r63)		# 9842
	mov	%r2, %r4		# 9843
	mov	%r1, %r6		# 9844
	addi	%r63, %r63, 13		# 9845
	call	%r60, setup_second_table.2700		# 9846
	addi	%r63, %r63, -13		# 9847
	ld	%r2, 11(%r63)		# 9848
	ld	%r3, 12(%r63)		# 9849
	add	%r3, %r3, %r2		# 9850
	sto	%r1, (%r3)		# 9851
if_cont.16623:
if_cont.16621:
	addi	%r2, %r2, -1		# 9852
	ld	%r1, 10(%r63)		# 9853
	addi	%r63, %r63, 13		# 9854
	call	%r60, iter_setup_dirvec_constants.2703		# 9855
	addi	%r63, %r63, -13		# 9856
	jmpui	if_cont.16619		# 9857
else.16618:
if_cont.16619:
	movi	%r2, 117		# 9858
	ld	%r1, 9(%r63)		# 9859
	addi	%r63, %r63, 13		# 9860
	call	%r60, init_dirvec_constants.2921		# 9861
	addi	%r63, %r63, -13		# 9862
	ld	%r1, 8(%r63)		# 9863
	addi	%r1, %r1, -1		# 9864
	movi	%r2, 0		# 9865
	cmpgt	%r2, %r2, %r1		# 9866
	jmpi	%r2, else.16624		# 9867
	movi	%r2, min_caml_dirvecs		# 9868
	add	%r2, %r2, %r1		# 9869
	ld	%r2, (%r2)		# 9870
	addi	%r3, %r2, 119		# 9871
	ld	%r3, (%r3)		# 9872
	movi	%r4, min_caml_n_objects		# 9873
	addi	%r4, %r4, 0		# 9874
	ld	%r4, (%r4)		# 9875
	addi	%r4, %r4, -1		# 9876
	movi	%r5, 0		# 9877
	cmpgt	%r5, %r5, %r4		# 9878
	sto	%r1, 13(%r63)		# 9879
	sto	%r2, 14(%r63)		# 9880
	jmpi	%r5, else.16625		# 9881
	movi	%r5, min_caml_objects		# 9882
	add	%r5, %r5, %r4		# 9883
	ld	%r5, (%r5)		# 9884
	ld	%r6, 1(%r3)		# 9885
	ld	%r7, (%r3)		# 9886
	ld	%r8, 1(%r5)		# 9887
	cmpeqi	%r9, %r8, 1		# 9888
	sto	%r3, 15(%r63)		# 9889
	jmpzi	%r9, else.16627		# 9890
	sto	%r4, 16(%r63)		# 9891
	sto	%r6, 17(%r63)		# 9892
	mov	%r2, %r5		# 9893
	mov	%r1, %r7		# 9894
	addi	%r63, %r63, 18		# 9895
	call	%r60, setup_rect_table.2694		# 9896
	addi	%r63, %r63, -18		# 9897
	ld	%r2, 16(%r63)		# 9898
	ld	%r3, 17(%r63)		# 9899
	add	%r3, %r3, %r2		# 9900
	sto	%r1, (%r3)		# 9901
	jmpui	if_cont.16628		# 9902
else.16627:
	cmpeqi	%r8, %r8, 2		# 9903
	jmpzi	%r8, else.16629		# 9904
	sto	%r4, 16(%r63)		# 9905
	sto	%r6, 17(%r63)		# 9906
	mov	%r2, %r5		# 9907
	mov	%r1, %r7		# 9908
	addi	%r63, %r63, 18		# 9909
	call	%r60, setup_surface_table.2697		# 9910
	addi	%r63, %r63, -18		# 9911
	ld	%r2, 16(%r63)		# 9912
	ld	%r3, 17(%r63)		# 9913
	add	%r3, %r3, %r2		# 9914
	sto	%r1, (%r3)		# 9915
	jmpui	if_cont.16630		# 9916
else.16629:
	sto	%r4, 16(%r63)		# 9917
	sto	%r6, 17(%r63)		# 9918
	mov	%r2, %r5		# 9919
	mov	%r1, %r7		# 9920
	addi	%r63, %r63, 18		# 9921
	call	%r60, setup_second_table.2700		# 9922
	addi	%r63, %r63, -18		# 9923
	ld	%r2, 16(%r63)		# 9924
	ld	%r3, 17(%r63)		# 9925
	add	%r3, %r3, %r2		# 9926
	sto	%r1, (%r3)		# 9927
if_cont.16630:
if_cont.16628:
	addi	%r2, %r2, -1		# 9928
	ld	%r1, 15(%r63)		# 9929
	addi	%r63, %r63, 18		# 9930
	call	%r60, iter_setup_dirvec_constants.2703		# 9931
	addi	%r63, %r63, -18		# 9932
	jmpui	if_cont.16626		# 9933
else.16625:
if_cont.16626:
	movi	%r2, 118		# 9934
	ld	%r1, 14(%r63)		# 9935
	addi	%r63, %r63, 18		# 9936
	call	%r60, init_dirvec_constants.2921		# 9937
	addi	%r63, %r63, -18		# 9938
	ld	%r1, 13(%r63)		# 9939
	addi	%r1, %r1, -1		# 9940
	movi	%r2, 0		# 9941
	cmpgt	%r2, %r2, %r1		# 9942
	jmpi	%r2, else.16631		# 9943
	movi	%r2, min_caml_dirvecs		# 9944
	add	%r2, %r2, %r1		# 9945
	ld	%r2, (%r2)		# 9946
	movi	%r3, 119		# 9947
	sto	%r1, 18(%r63)		# 9948
	mov	%r1, %r2		# 9949
	mov	%r2, %r3		# 9950
	addi	%r63, %r63, 19		# 9951
	call	%r60, init_dirvec_constants.2921		# 9952
	addi	%r63, %r63, -19		# 9953
	ld	%r1, 18(%r63)		# 9954
	addi	%r1, %r1, -1		# 9955
	addi	%r63, %r63, -1		# 9956
	ld	%r60, (%r63)		# 9957
	jmpui	init_vecset_constants.2924		# 9958
else.16631:
	addi	%r63, %r63, -1		# 9959
	ld	%r60, (%r63)		# 9960
	jmpu	%r60		# 9961
else.16624:
	addi	%r63, %r63, -1		# 9962
	ld	%r60, (%r63)		# 9963
	jmpu	%r60		# 9964
else.16617:
	addi	%r63, %r63, -1		# 9965
	ld	%r60, (%r63)		# 9966
	jmpu	%r60		# 9967
else.16604:
	addi	%r63, %r63, -1		# 9968
	ld	%r60, (%r63)		# 9969
	jmpu	%r60		# 9970
.globl	setup_rect_reflection.2935
setup_rect_reflection.2935:
	sto	%r60, (%r63)		# 9971
	addi	%r63, %r63, 1		# 9972
	movi	%r3, 0		# 9973
	cmplti	%r4, %r1, 0		# 9974
	jmpzi	%r4, else.16636		# 9975
	neg	%r1, %r1		# 9976
	lshifti	%r1, %r1, 2		# 9977
	add	%r3, %r3, %r1		# 9978
	neg	%r3, %r3		# 9979
	jmpui	if_cont.16637		# 9980
else.16636:
	lshifti	%r1, %r1, 2		# 9981
	add	%r3, %r3, %r1		# 9982
if_cont.16637:
	movi	%r1, min_caml_n_reflections		# 9983
	addi	%r1, %r1, 0		# 9984
	ld	%r1, (%r1)		# 9985
	movhiz	%r4, 260096		# 9986
	ld	%r2, 7(%r2)		# 9987
	addi	%r2, %r2, 0		# 9988
	ld	%r2, (%r2)		# 9989
	fsub	%r4, %r4, %r2		# 9990
	movi	%r2, min_caml_light		# 9991
	addi	%r2, %r2, 0		# 9992
	ld	%r2, (%r2)		# 9993
	fneg	%r2, %r2		# 9994
	movi	%r5, min_caml_light		# 9995
	addi	%r5, %r5, 1		# 9996
	ld	%r5, (%r5)		# 9997
	fneg	%r5, %r5		# 9998
	movi	%r6, min_caml_light		# 9999
	addi	%r6, %r6, 2		# 10000
	ld	%r6, (%r6)		# 10001
	fneg	%r6, %r6		# 10002
	addi	%r7, %r3, 1		# 10003
	movi	%r8, min_caml_light		# 10004
	addi	%r8, %r8, 0		# 10005
	ld	%r8, (%r8)		# 10006
	movi	%r9, 0		# 10007
	movi	%r10, 3		# 10008
	sto	%r2, (%r63)		# 10009
	sto	%r3, 1(%r63)		# 10010
	sto	%r1, 2(%r63)		# 10011
	sto	%r7, 3(%r63)		# 10012
	sto	%r4, 4(%r63)		# 10013
	sto	%r6, 5(%r63)		# 10014
	sto	%r5, 6(%r63)		# 10015
	sto	%r8, 7(%r63)		# 10016
	mov	%r2, %r9		# 10017
	mov	%r1, %r10		# 10018
	addi	%r63, %r63, 8		# 10019
	call	%r60, min_caml_create_float_array		# 10020
	addi	%r63, %r63, -8		# 10021
	mov	%r2, %r1		# 10022
	movi	%r1, min_caml_n_objects		# 10023
	addi	%r1, %r1, 0		# 10024
	ld	%r1, (%r1)		# 10025
	sto	%r2, 8(%r63)		# 10026
	addi	%r63, %r63, 9		# 10027
	call	%r60, min_caml_create_array		# 10028
	addi	%r63, %r63, -9		# 10029
	mov	%r2, %r62		# 10030
	addi	%r62, %r62, 2		# 10031
	sto	%r1, 1(%r2)		# 10032
	ld	%r3, 8(%r63)		# 10033
	sto	%r3, (%r2)		# 10034
	addi	%r4, %r3, 0		# 10035
	ld	%r5, 7(%r63)		# 10036
	sto	%r5, (%r4)		# 10037
	addi	%r4, %r3, 1		# 10038
	ld	%r5, 6(%r63)		# 10039
	sto	%r5, (%r4)		# 10040
	addi	%r4, %r3, 2		# 10041
	ld	%r6, 5(%r63)		# 10042
	sto	%r6, (%r4)		# 10043
	movi	%r4, min_caml_n_objects		# 10044
	addi	%r4, %r4, 0		# 10045
	ld	%r4, (%r4)		# 10046
	addi	%r4, %r4, -1		# 10047
	movi	%r7, 0		# 10048
	cmpgt	%r7, %r7, %r4		# 10049
	sto	%r2, 9(%r63)		# 10050
	jmpi	%r7, else.16638		# 10051
	movi	%r7, min_caml_objects		# 10052
	add	%r7, %r7, %r4		# 10053
	ld	%r7, (%r7)		# 10054
	ld	%r8, 1(%r7)		# 10055
	cmpeqi	%r9, %r8, 1		# 10056
	jmpzi	%r9, else.16640		# 10057
	sto	%r4, 10(%r63)		# 10058
	sto	%r1, 11(%r63)		# 10059
	mov	%r2, %r7		# 10060
	mov	%r1, %r3		# 10061
	addi	%r63, %r63, 12		# 10062
	call	%r60, setup_rect_table.2694		# 10063
	addi	%r63, %r63, -12		# 10064
	ld	%r2, 10(%r63)		# 10065
	ld	%r3, 11(%r63)		# 10066
	add	%r3, %r3, %r2		# 10067
	sto	%r1, (%r3)		# 10068
	jmpui	if_cont.16641		# 10069
else.16640:
	cmpeqi	%r8, %r8, 2		# 10070
	jmpzi	%r8, else.16642		# 10071
	sto	%r4, 10(%r63)		# 10072
	sto	%r1, 11(%r63)		# 10073
	mov	%r2, %r7		# 10074
	mov	%r1, %r3		# 10075
	addi	%r63, %r63, 12		# 10076
	call	%r60, setup_surface_table.2697		# 10077
	addi	%r63, %r63, -12		# 10078
	ld	%r2, 10(%r63)		# 10079
	ld	%r3, 11(%r63)		# 10080
	add	%r3, %r3, %r2		# 10081
	sto	%r1, (%r3)		# 10082
	jmpui	if_cont.16643		# 10083
else.16642:
	sto	%r4, 10(%r63)		# 10084
	sto	%r1, 11(%r63)		# 10085
	mov	%r2, %r7		# 10086
	mov	%r1, %r3		# 10087
	addi	%r63, %r63, 12		# 10088
	call	%r60, setup_second_table.2700		# 10089
	addi	%r63, %r63, -12		# 10090
	ld	%r2, 10(%r63)		# 10091
	ld	%r3, 11(%r63)		# 10092
	add	%r3, %r3, %r2		# 10093
	sto	%r1, (%r3)		# 10094
if_cont.16643:
if_cont.16641:
	addi	%r2, %r2, -1		# 10095
	ld	%r1, 9(%r63)		# 10096
	addi	%r63, %r63, 12		# 10097
	call	%r60, iter_setup_dirvec_constants.2703		# 10098
	addi	%r63, %r63, -12		# 10099
	jmpui	if_cont.16639		# 10100
else.16638:
if_cont.16639:
	movi	%r1, min_caml_reflections		# 10101
	mov	%r2, %r62		# 10102
	addi	%r62, %r62, 3		# 10103
	ld	%r3, 4(%r63)		# 10104
	sto	%r3, 2(%r2)		# 10105
	ld	%r4, 9(%r63)		# 10106
	sto	%r4, 1(%r2)		# 10107
	ld	%r4, 3(%r63)		# 10108
	sto	%r4, (%r2)		# 10109
	ld	%r4, 2(%r63)		# 10110
	add	%r1, %r1, %r4		# 10111
	sto	%r2, (%r1)		# 10112
	addi	%r1, %r4, 1		# 10113
	ld	%r2, 1(%r63)		# 10114
	addi	%r5, %r2, 2		# 10115
	movi	%r6, min_caml_light		# 10116
	addi	%r6, %r6, 1		# 10117
	ld	%r6, (%r6)		# 10118
	movi	%r7, 0		# 10119
	movi	%r8, 3		# 10120
	sto	%r1, 12(%r63)		# 10121
	sto	%r5, 13(%r63)		# 10122
	sto	%r6, 14(%r63)		# 10123
	mov	%r2, %r7		# 10124
	mov	%r1, %r8		# 10125
	addi	%r63, %r63, 15		# 10126
	call	%r60, min_caml_create_float_array		# 10127
	addi	%r63, %r63, -15		# 10128
	mov	%r2, %r1		# 10129
	movi	%r1, min_caml_n_objects		# 10130
	addi	%r1, %r1, 0		# 10131
	ld	%r1, (%r1)		# 10132
	sto	%r2, 15(%r63)		# 10133
	addi	%r63, %r63, 16		# 10134
	call	%r60, min_caml_create_array		# 10135
	addi	%r63, %r63, -16		# 10136
	mov	%r2, %r62		# 10137
	addi	%r62, %r62, 2		# 10138
	sto	%r1, 1(%r2)		# 10139
	ld	%r3, 15(%r63)		# 10140
	sto	%r3, (%r2)		# 10141
	addi	%r4, %r3, 0		# 10142
	ld	%r5, (%r63)		# 10143
	sto	%r5, (%r4)		# 10144
	addi	%r4, %r3, 1		# 10145
	ld	%r6, 14(%r63)		# 10146
	sto	%r6, (%r4)		# 10147
	addi	%r4, %r3, 2		# 10148
	ld	%r6, 5(%r63)		# 10149
	sto	%r6, (%r4)		# 10150
	movi	%r4, min_caml_n_objects		# 10151
	addi	%r4, %r4, 0		# 10152
	ld	%r4, (%r4)		# 10153
	addi	%r4, %r4, -1		# 10154
	movi	%r6, 0		# 10155
	cmpgt	%r6, %r6, %r4		# 10156
	sto	%r2, 16(%r63)		# 10157
	jmpi	%r6, else.16644		# 10158
	movi	%r6, min_caml_objects		# 10159
	add	%r6, %r6, %r4		# 10160
	ld	%r6, (%r6)		# 10161
	ld	%r7, 1(%r6)		# 10162
	cmpeqi	%r8, %r7, 1		# 10163
	jmpzi	%r8, else.16646		# 10164
	sto	%r4, 17(%r63)		# 10165
	sto	%r1, 18(%r63)		# 10166
	mov	%r2, %r6		# 10167
	mov	%r1, %r3		# 10168
	addi	%r63, %r63, 19		# 10169
	call	%r60, setup_rect_table.2694		# 10170
	addi	%r63, %r63, -19		# 10171
	ld	%r2, 17(%r63)		# 10172
	ld	%r3, 18(%r63)		# 10173
	add	%r3, %r3, %r2		# 10174
	sto	%r1, (%r3)		# 10175
	jmpui	if_cont.16647		# 10176
else.16646:
	cmpeqi	%r7, %r7, 2		# 10177
	jmpzi	%r7, else.16648		# 10178
	sto	%r4, 17(%r63)		# 10179
	sto	%r1, 18(%r63)		# 10180
	mov	%r2, %r6		# 10181
	mov	%r1, %r3		# 10182
	addi	%r63, %r63, 19		# 10183
	call	%r60, setup_surface_table.2697		# 10184
	addi	%r63, %r63, -19		# 10185
	ld	%r2, 17(%r63)		# 10186
	ld	%r3, 18(%r63)		# 10187
	add	%r3, %r3, %r2		# 10188
	sto	%r1, (%r3)		# 10189
	jmpui	if_cont.16649		# 10190
else.16648:
	sto	%r4, 17(%r63)		# 10191
	sto	%r1, 18(%r63)		# 10192
	mov	%r2, %r6		# 10193
	mov	%r1, %r3		# 10194
	addi	%r63, %r63, 19		# 10195
	call	%r60, setup_second_table.2700		# 10196
	addi	%r63, %r63, -19		# 10197
	ld	%r2, 17(%r63)		# 10198
	ld	%r3, 18(%r63)		# 10199
	add	%r3, %r3, %r2		# 10200
	sto	%r1, (%r3)		# 10201
if_cont.16649:
if_cont.16647:
	addi	%r2, %r2, -1		# 10202
	ld	%r1, 16(%r63)		# 10203
	addi	%r63, %r63, 19		# 10204
	call	%r60, iter_setup_dirvec_constants.2703		# 10205
	addi	%r63, %r63, -19		# 10206
	jmpui	if_cont.16645		# 10207
else.16644:
if_cont.16645:
	movi	%r1, min_caml_reflections		# 10208
	mov	%r2, %r62		# 10209
	addi	%r62, %r62, 3		# 10210
	ld	%r3, 4(%r63)		# 10211
	sto	%r3, 2(%r2)		# 10212
	ld	%r4, 16(%r63)		# 10213
	sto	%r4, 1(%r2)		# 10214
	ld	%r4, 13(%r63)		# 10215
	sto	%r4, (%r2)		# 10216
	ld	%r4, 12(%r63)		# 10217
	add	%r1, %r1, %r4		# 10218
	sto	%r2, (%r1)		# 10219
	ld	%r1, 2(%r63)		# 10220
	addi	%r2, %r1, 2		# 10221
	ld	%r4, 1(%r63)		# 10222
	addi	%r4, %r4, 3		# 10223
	movi	%r5, min_caml_light		# 10224
	addi	%r5, %r5, 2		# 10225
	ld	%r5, (%r5)		# 10226
	movi	%r6, 0		# 10227
	movi	%r7, 3		# 10228
	sto	%r2, 19(%r63)		# 10229
	sto	%r4, 20(%r63)		# 10230
	sto	%r5, 21(%r63)		# 10231
	mov	%r2, %r6		# 10232
	mov	%r1, %r7		# 10233
	addi	%r63, %r63, 22		# 10234
	call	%r60, min_caml_create_float_array		# 10235
	addi	%r63, %r63, -22		# 10236
	mov	%r2, %r1		# 10237
	movi	%r1, min_caml_n_objects		# 10238
	addi	%r1, %r1, 0		# 10239
	ld	%r1, (%r1)		# 10240
	sto	%r2, 22(%r63)		# 10241
	addi	%r63, %r63, 23		# 10242
	call	%r60, min_caml_create_array		# 10243
	addi	%r63, %r63, -23		# 10244
	mov	%r2, %r62		# 10245
	addi	%r62, %r62, 2		# 10246
	sto	%r1, 1(%r2)		# 10247
	ld	%r3, 22(%r63)		# 10248
	sto	%r3, (%r2)		# 10249
	addi	%r4, %r3, 0		# 10250
	ld	%r5, (%r63)		# 10251
	sto	%r5, (%r4)		# 10252
	addi	%r4, %r3, 1		# 10253
	ld	%r5, 6(%r63)		# 10254
	sto	%r5, (%r4)		# 10255
	addi	%r4, %r3, 2		# 10256
	ld	%r5, 21(%r63)		# 10257
	sto	%r5, (%r4)		# 10258
	movi	%r4, min_caml_n_objects		# 10259
	addi	%r4, %r4, 0		# 10260
	ld	%r4, (%r4)		# 10261
	addi	%r4, %r4, -1		# 10262
	movi	%r5, 0		# 10263
	cmpgt	%r5, %r5, %r4		# 10264
	sto	%r2, 23(%r63)		# 10265
	jmpi	%r5, else.16650		# 10266
	movi	%r5, min_caml_objects		# 10267
	add	%r5, %r5, %r4		# 10268
	ld	%r5, (%r5)		# 10269
	ld	%r6, 1(%r5)		# 10270
	cmpeqi	%r7, %r6, 1		# 10271
	jmpzi	%r7, else.16652		# 10272
	sto	%r4, 24(%r63)		# 10273
	sto	%r1, 25(%r63)		# 10274
	mov	%r2, %r5		# 10275
	mov	%r1, %r3		# 10276
	addi	%r63, %r63, 26		# 10277
	call	%r60, setup_rect_table.2694		# 10278
	addi	%r63, %r63, -26		# 10279
	ld	%r2, 24(%r63)		# 10280
	ld	%r3, 25(%r63)		# 10281
	add	%r3, %r3, %r2		# 10282
	sto	%r1, (%r3)		# 10283
	jmpui	if_cont.16653		# 10284
else.16652:
	cmpeqi	%r6, %r6, 2		# 10285
	jmpzi	%r6, else.16654		# 10286
	sto	%r4, 24(%r63)		# 10287
	sto	%r1, 25(%r63)		# 10288
	mov	%r2, %r5		# 10289
	mov	%r1, %r3		# 10290
	addi	%r63, %r63, 26		# 10291
	call	%r60, setup_surface_table.2697		# 10292
	addi	%r63, %r63, -26		# 10293
	ld	%r2, 24(%r63)		# 10294
	ld	%r3, 25(%r63)		# 10295
	add	%r3, %r3, %r2		# 10296
	sto	%r1, (%r3)		# 10297
	jmpui	if_cont.16655		# 10298
else.16654:
	sto	%r4, 24(%r63)		# 10299
	sto	%r1, 25(%r63)		# 10300
	mov	%r2, %r5		# 10301
	mov	%r1, %r3		# 10302
	addi	%r63, %r63, 26		# 10303
	call	%r60, setup_second_table.2700		# 10304
	addi	%r63, %r63, -26		# 10305
	ld	%r2, 24(%r63)		# 10306
	ld	%r3, 25(%r63)		# 10307
	add	%r3, %r3, %r2		# 10308
	sto	%r1, (%r3)		# 10309
if_cont.16655:
if_cont.16653:
	addi	%r2, %r2, -1		# 10310
	ld	%r1, 23(%r63)		# 10311
	addi	%r63, %r63, 26		# 10312
	call	%r60, iter_setup_dirvec_constants.2703		# 10313
	addi	%r63, %r63, -26		# 10314
	jmpui	if_cont.16651		# 10315
else.16650:
if_cont.16651:
	movi	%r1, min_caml_reflections		# 10316
	mov	%r2, %r62		# 10317
	addi	%r62, %r62, 3		# 10318
	ld	%r3, 4(%r63)		# 10319
	sto	%r3, 2(%r2)		# 10320
	ld	%r3, 23(%r63)		# 10321
	sto	%r3, 1(%r2)		# 10322
	ld	%r3, 20(%r63)		# 10323
	sto	%r3, (%r2)		# 10324
	ld	%r3, 19(%r63)		# 10325
	add	%r1, %r1, %r3		# 10326
	sto	%r2, (%r1)		# 10327
	movi	%r1, min_caml_n_reflections		# 10328
	ld	%r2, 2(%r63)		# 10329
	addi	%r2, %r2, 3		# 10330
	addi	%r1, %r1, 0		# 10331
	sto	%r2, (%r1)		# 10332
	addi	%r63, %r63, -1		# 10333
	ld	%r60, (%r63)		# 10334
	jmpu	%r60		# 10335
.globl	setup_surface_reflection.2938
setup_surface_reflection.2938:
	sto	%r60, (%r63)		# 10336
	addi	%r63, %r63, 1		# 10337
	movi	%r3, 0		# 10338
	cmplti	%r4, %r1, 0		# 10339
	jmpzi	%r4, else.16657		# 10340
	neg	%r1, %r1		# 10341
	lshifti	%r1, %r1, 2		# 10342
	add	%r3, %r3, %r1		# 10343
	neg	%r3, %r3		# 10344
	jmpui	if_cont.16658		# 10345
else.16657:
	lshifti	%r1, %r1, 2		# 10346
	add	%r3, %r3, %r1		# 10347
if_cont.16658:
	addi	%r3, %r3, 1		# 10348
	movi	%r1, min_caml_n_reflections		# 10349
	addi	%r1, %r1, 0		# 10350
	ld	%r1, (%r1)		# 10351
	movhiz	%r4, 260096		# 10352
	ld	%r5, 7(%r2)		# 10353
	addi	%r5, %r5, 0		# 10354
	ld	%r5, (%r5)		# 10355
	fsub	%r4, %r4, %r5		# 10356
	movi	%r5, min_caml_light		# 10357
	ld	%r6, 4(%r2)		# 10358
	addi	%r7, %r5, 0		# 10359
	ld	%r7, (%r7)		# 10360
	addi	%r8, %r6, 0		# 10361
	ld	%r8, (%r8)		# 10362
	fmul	%r7, %r7, %r8		# 10363
	addi	%r8, %r5, 1		# 10364
	ld	%r8, (%r8)		# 10365
	addi	%r9, %r6, 1		# 10366
	ld	%r9, (%r9)		# 10367
	fmul	%r8, %r8, %r9		# 10368
	fadd	%r7, %r7, %r8		# 10369
	addi	%r5, %r5, 2		# 10370
	ld	%r5, (%r5)		# 10371
	addi	%r6, %r6, 2		# 10372
	ld	%r6, (%r6)		# 10373
	fmul	%r5, %r5, %r6		# 10374
	fadd	%r7, %r7, %r5		# 10375
	movhiz	%r5, 262144		# 10376
	ld	%r6, 4(%r2)		# 10377
	addi	%r6, %r6, 0		# 10378
	ld	%r6, (%r6)		# 10379
	fmul	%r5, %r5, %r6		# 10380
	fmul	%r5, %r5, %r7		# 10381
	movi	%r6, min_caml_light		# 10382
	addi	%r6, %r6, 0		# 10383
	ld	%r6, (%r6)		# 10384
	fsub	%r5, %r5, %r6		# 10385
	movhiz	%r6, 262144		# 10386
	ld	%r8, 4(%r2)		# 10387
	addi	%r8, %r8, 1		# 10388
	ld	%r8, (%r8)		# 10389
	fmul	%r6, %r6, %r8		# 10390
	fmul	%r6, %r6, %r7		# 10391
	movi	%r8, min_caml_light		# 10392
	addi	%r8, %r8, 1		# 10393
	ld	%r8, (%r8)		# 10394
	fsub	%r6, %r6, %r8		# 10395
	movhiz	%r8, 262144		# 10396
	ld	%r2, 4(%r2)		# 10397
	addi	%r2, %r2, 2		# 10398
	ld	%r2, (%r2)		# 10399
	fmul	%r8, %r8, %r2		# 10400
	fmul	%r8, %r8, %r7		# 10401
	movi	%r2, min_caml_light		# 10402
	addi	%r2, %r2, 2		# 10403
	ld	%r2, (%r2)		# 10404
	fsub	%r8, %r8, %r2		# 10405
	movi	%r2, 0		# 10406
	movi	%r7, 3		# 10407
	sto	%r1, (%r63)		# 10408
	sto	%r3, 1(%r63)		# 10409
	sto	%r4, 2(%r63)		# 10410
	sto	%r8, 3(%r63)		# 10411
	sto	%r6, 4(%r63)		# 10412
	sto	%r5, 5(%r63)		# 10413
	mov	%r1, %r7		# 10414
	addi	%r63, %r63, 6		# 10415
	call	%r60, min_caml_create_float_array		# 10416
	addi	%r63, %r63, -6		# 10417
	mov	%r2, %r1		# 10418
	movi	%r1, min_caml_n_objects		# 10419
	addi	%r1, %r1, 0		# 10420
	ld	%r1, (%r1)		# 10421
	sto	%r2, 6(%r63)		# 10422
	addi	%r63, %r63, 7		# 10423
	call	%r60, min_caml_create_array		# 10424
	addi	%r63, %r63, -7		# 10425
	mov	%r2, %r62		# 10426
	addi	%r62, %r62, 2		# 10427
	sto	%r1, 1(%r2)		# 10428
	ld	%r3, 6(%r63)		# 10429
	sto	%r3, (%r2)		# 10430
	addi	%r4, %r3, 0		# 10431
	ld	%r5, 5(%r63)		# 10432
	sto	%r5, (%r4)		# 10433
	addi	%r4, %r3, 1		# 10434
	ld	%r5, 4(%r63)		# 10435
	sto	%r5, (%r4)		# 10436
	addi	%r4, %r3, 2		# 10437
	ld	%r5, 3(%r63)		# 10438
	sto	%r5, (%r4)		# 10439
	movi	%r4, min_caml_n_objects		# 10440
	addi	%r4, %r4, 0		# 10441
	ld	%r4, (%r4)		# 10442
	addi	%r4, %r4, -1		# 10443
	movi	%r5, 0		# 10444
	cmpgt	%r5, %r5, %r4		# 10445
	sto	%r2, 7(%r63)		# 10446
	jmpi	%r5, else.16659		# 10447
	movi	%r5, min_caml_objects		# 10448
	add	%r5, %r5, %r4		# 10449
	ld	%r5, (%r5)		# 10450
	ld	%r6, 1(%r5)		# 10451
	cmpeqi	%r7, %r6, 1		# 10452
	jmpzi	%r7, else.16661		# 10453
	sto	%r4, 8(%r63)		# 10454
	sto	%r1, 9(%r63)		# 10455
	mov	%r2, %r5		# 10456
	mov	%r1, %r3		# 10457
	addi	%r63, %r63, 10		# 10458
	call	%r60, setup_rect_table.2694		# 10459
	addi	%r63, %r63, -10		# 10460
	ld	%r2, 8(%r63)		# 10461
	ld	%r3, 9(%r63)		# 10462
	add	%r3, %r3, %r2		# 10463
	sto	%r1, (%r3)		# 10464
	jmpui	if_cont.16662		# 10465
else.16661:
	cmpeqi	%r6, %r6, 2		# 10466
	jmpzi	%r6, else.16663		# 10467
	sto	%r4, 8(%r63)		# 10468
	sto	%r1, 9(%r63)		# 10469
	mov	%r2, %r5		# 10470
	mov	%r1, %r3		# 10471
	addi	%r63, %r63, 10		# 10472
	call	%r60, setup_surface_table.2697		# 10473
	addi	%r63, %r63, -10		# 10474
	ld	%r2, 8(%r63)		# 10475
	ld	%r3, 9(%r63)		# 10476
	add	%r3, %r3, %r2		# 10477
	sto	%r1, (%r3)		# 10478
	jmpui	if_cont.16664		# 10479
else.16663:
	sto	%r4, 8(%r63)		# 10480
	sto	%r1, 9(%r63)		# 10481
	mov	%r2, %r5		# 10482
	mov	%r1, %r3		# 10483
	addi	%r63, %r63, 10		# 10484
	call	%r60, setup_second_table.2700		# 10485
	addi	%r63, %r63, -10		# 10486
	ld	%r2, 8(%r63)		# 10487
	ld	%r3, 9(%r63)		# 10488
	add	%r3, %r3, %r2		# 10489
	sto	%r1, (%r3)		# 10490
if_cont.16664:
if_cont.16662:
	addi	%r2, %r2, -1		# 10491
	ld	%r1, 7(%r63)		# 10492
	addi	%r63, %r63, 10		# 10493
	call	%r60, iter_setup_dirvec_constants.2703		# 10494
	addi	%r63, %r63, -10		# 10495
	jmpui	if_cont.16660		# 10496
else.16659:
if_cont.16660:
	movi	%r1, min_caml_reflections		# 10497
	mov	%r2, %r62		# 10498
	addi	%r62, %r62, 3		# 10499
	ld	%r3, 2(%r63)		# 10500
	sto	%r3, 2(%r2)		# 10501
	ld	%r3, 7(%r63)		# 10502
	sto	%r3, 1(%r2)		# 10503
	ld	%r3, 1(%r63)		# 10504
	sto	%r3, (%r2)		# 10505
	ld	%r3, (%r63)		# 10506
	add	%r1, %r1, %r3		# 10507
	sto	%r2, (%r1)		# 10508
	movi	%r1, min_caml_n_reflections		# 10509
	addi	%r3, %r3, 1		# 10510
	addi	%r1, %r1, 0		# 10511
	sto	%r3, (%r1)		# 10512
	addi	%r63, %r63, -1		# 10513
	ld	%r60, (%r63)		# 10514
	jmpu	%r60		# 10515
.globl	rt.2943
rt.2943:
	sto	%r60, (%r63)		# 10516
	addi	%r63, %r63, 1		# 10517
	movi	%r3, min_caml_image_size		# 10518
	addi	%r3, %r3, 0		# 10519
	sto	%r1, (%r3)		# 10520
	movi	%r3, min_caml_image_size		# 10521
	addi	%r3, %r3, 1		# 10522
	sto	%r2, (%r3)		# 10523
	movi	%r3, min_caml_image_center		# 10524
	cmplti	%r4, %r1, 0		# 10525
	jmpzi	%r4, else.16666		# 10526
	neg	%r4, %r1		# 10527
	rshifti	%r4, %r4, 1		# 10528
	neg	%r4, %r4		# 10529
	jmpui	if_cont.16667		# 10530
else.16666:
	rshifti	%r4, %r1, 1		# 10531
if_cont.16667:
	addi	%r3, %r3, 0		# 10532
	sto	%r4, (%r3)		# 10533
	movi	%r3, min_caml_image_center		# 10534
	cmplti	%r4, %r2, 0		# 10535
	jmpzi	%r4, else.16668		# 10536
	neg	%r2, %r2		# 10537
	rshifti	%r2, %r2, 1		# 10538
	neg	%r2, %r2		# 10539
	jmpui	if_cont.16669		# 10540
else.16668:
	rshifti	%r2, %r2, 1		# 10541
if_cont.16669:
	addi	%r3, %r3, 1		# 10542
	sto	%r2, (%r3)		# 10543
	movi	%r2, min_caml_scan_pitch		# 10544
	movhiz	%r3, 274432		# 10545
	sto	%r2, (%r63)		# 10546
	sto	%r3, 1(%r63)		# 10547
	addi	%r63, %r63, 2		# 10548
	call	%r60, min_caml_float_of_int		# 10549
	addi	%r63, %r63, -2		# 10550
	finv	%r1, %r1		# 10551
	ld	%r2, 1(%r63)		# 10552
	fmul	%r2, %r2, %r1		# 10553
	ld	%r1, (%r63)		# 10554
	addi	%r1, %r1, 0		# 10555
	sto	%r2, (%r1)		# 10556
	movi	%r1, min_caml_image_size		# 10557
	addi	%r1, %r1, 0		# 10558
	ld	%r1, (%r1)		# 10559
	sto	%r1, 2(%r63)		# 10560
	addi	%r63, %r63, 3		# 10561
	call	%r60, create_pixel.2885		# 10562
	addi	%r63, %r63, -3		# 10563
	mov	%r2, %r1		# 10564
	ld	%r1, 2(%r63)		# 10565
	addi	%r63, %r63, 3		# 10566
	call	%r60, min_caml_create_array		# 10567
	addi	%r63, %r63, -3		# 10568
	movi	%r2, min_caml_image_size		# 10569
	addi	%r2, %r2, 0		# 10570
	ld	%r2, (%r2)		# 10571
	addi	%r2, %r2, -2		# 10572
	addi	%r63, %r63, 3		# 10573
	call	%r60, init_line_elements.2887		# 10574
	addi	%r63, %r63, -3		# 10575
	movi	%r2, min_caml_image_size		# 10576
	addi	%r2, %r2, 0		# 10577
	ld	%r2, (%r2)		# 10578
	sto	%r1, 3(%r63)		# 10579
	sto	%r2, 4(%r63)		# 10580
	addi	%r63, %r63, 5		# 10581
	call	%r60, create_pixel.2885		# 10582
	addi	%r63, %r63, -5		# 10583
	mov	%r2, %r1		# 10584
	ld	%r1, 4(%r63)		# 10585
	addi	%r63, %r63, 5		# 10586
	call	%r60, min_caml_create_array		# 10587
	addi	%r63, %r63, -5		# 10588
	movi	%r2, min_caml_image_size		# 10589
	addi	%r2, %r2, 0		# 10590
	ld	%r2, (%r2)		# 10591
	addi	%r2, %r2, -2		# 10592
	addi	%r63, %r63, 5		# 10593
	call	%r60, init_line_elements.2887		# 10594
	addi	%r63, %r63, -5		# 10595
	movi	%r2, min_caml_image_size		# 10596
	addi	%r2, %r2, 0		# 10597
	ld	%r2, (%r2)		# 10598
	sto	%r1, 5(%r63)		# 10599
	sto	%r2, 6(%r63)		# 10600
	addi	%r63, %r63, 7		# 10601
	call	%r60, create_pixel.2885		# 10602
	addi	%r63, %r63, -7		# 10603
	mov	%r2, %r1		# 10604
	ld	%r1, 6(%r63)		# 10605
	addi	%r63, %r63, 7		# 10606
	call	%r60, min_caml_create_array		# 10607
	addi	%r63, %r63, -7		# 10608
	movi	%r2, min_caml_image_size		# 10609
	addi	%r2, %r2, 0		# 10610
	ld	%r2, (%r2)		# 10611
	addi	%r2, %r2, -2		# 10612
	addi	%r63, %r63, 7		# 10613
	call	%r60, init_line_elements.2887		# 10614
	addi	%r63, %r63, -7		# 10615
	sto	%r1, 7(%r63)		# 10616
	addi	%r63, %r63, 8		# 10617
	call	%r60, read_screen_settings.2589		# 10618
	addi	%r63, %r63, -8		# 10619
	addi	%r63, %r63, 8		# 10620
	call	%r60, read_light.2591		# 10621
	addi	%r63, %r63, -8		# 10622
	movi	%r1, 0		# 10623
	sto	%r1, 8(%r63)		# 10624
	addi	%r63, %r63, 9		# 10625
	call	%r60, read_nth_object.2596		# 10626
	addi	%r63, %r63, -9		# 10627
	cmpeqi	%r1, %r1, 0		# 10628
	jmpzi	%r1, else.16670		# 10629
	movi	%r1, min_caml_n_objects		# 10630
	addi	%r1, %r1, 0		# 10631
	ld	%r2, 8(%r63)		# 10632
	sto	%r2, (%r1)		# 10633
	jmpui	if_cont.16671		# 10634
else.16670:
	movi	%r1, 1		# 10635
	addi	%r63, %r63, 9		# 10636
	call	%r60, read_object.2598		# 10637
	addi	%r63, %r63, -9		# 10638
if_cont.16671:
	movi	%r1, 0		# 10639
	addi	%r63, %r63, 9		# 10640
	call	%r60, read_and_network.2606		# 10641
	addi	%r63, %r63, -9		# 10642
	movi	%r1, min_caml_or_net		# 10643
	movi	%r2, 0		# 10644
	sto	%r1, 9(%r63)		# 10645
	mov	%r1, %r2		# 10646
	addi	%r63, %r63, 10		# 10647
	call	%r60, read_or_network.2604		# 10648
	addi	%r63, %r63, -10		# 10649
	ld	%r2, 9(%r63)		# 10650
	addi	%r2, %r2, 0		# 10651
	sto	%r1, (%r2)		# 10652
	movi	%r1, 80		# 10653
	out	%r1		# 10654
	movi	%r1, 51		# 10655
	out	%r1		# 10656
	movi	%r1, 10		# 10657
	out	%r1		# 10658
	movi	%r1, min_caml_image_size		# 10659
	addi	%r1, %r1, 0		# 10660
	ld	%r1, (%r1)		# 10661
	addi	%r63, %r63, 10		# 10662
	call	%r60, min_caml_print_int		# 10663
	addi	%r63, %r63, -10		# 10664
	movi	%r1, 32		# 10665
	out	%r1		# 10666
	movi	%r1, min_caml_image_size		# 10667
	addi	%r1, %r1, 1		# 10668
	ld	%r1, (%r1)		# 10669
	addi	%r63, %r63, 10		# 10670
	call	%r60, min_caml_print_int		# 10671
	addi	%r63, %r63, -10		# 10672
	movi	%r1, 32		# 10673
	out	%r1		# 10674
	movi	%r1, 255		# 10675
	addi	%r63, %r63, 10		# 10676
	call	%r60, min_caml_print_int		# 10677
	addi	%r63, %r63, -10		# 10678
	movi	%r1, 10		# 10679
	out	%r1		# 10680
	movi	%r1, 4		# 10681
	addi	%r63, %r63, 10		# 10682
	call	%r60, create_dirvecs.2919		# 10683
	addi	%r63, %r63, -10		# 10684
	movi	%r1, 9		# 10685
	movi	%r2, 0		# 10686
	movi	%r3, 0		# 10687
	addi	%r63, %r63, 10		# 10688
	call	%r60, calc_dirvec_rows.2910		# 10689
	addi	%r63, %r63, -10		# 10690
	movi	%r1, min_caml_dirvecs		# 10691
	addi	%r1, %r1, 4		# 10692
	ld	%r1, (%r1)		# 10693
	addi	%r2, %r1, 119		# 10694
	ld	%r2, (%r2)		# 10695
	movi	%r3, min_caml_n_objects		# 10696
	addi	%r3, %r3, 0		# 10697
	ld	%r3, (%r3)		# 10698
	addi	%r3, %r3, -1		# 10699
	movi	%r4, 0		# 10700
	cmpgt	%r4, %r4, %r3		# 10701
	sto	%r1, 10(%r63)		# 10702
	jmpi	%r4, else.16672		# 10703
	movi	%r4, min_caml_objects		# 10704
	add	%r4, %r4, %r3		# 10705
	ld	%r4, (%r4)		# 10706
	ld	%r5, 1(%r2)		# 10707
	ld	%r6, (%r2)		# 10708
	ld	%r7, 1(%r4)		# 10709
	cmpeqi	%r8, %r7, 1		# 10710
	sto	%r2, 11(%r63)		# 10711
	jmpzi	%r8, else.16674		# 10712
	sto	%r3, 12(%r63)		# 10713
	sto	%r5, 13(%r63)		# 10714
	mov	%r2, %r4		# 10715
	mov	%r1, %r6		# 10716
	addi	%r63, %r63, 14		# 10717
	call	%r60, setup_rect_table.2694		# 10718
	addi	%r63, %r63, -14		# 10719
	ld	%r2, 12(%r63)		# 10720
	ld	%r3, 13(%r63)		# 10721
	add	%r3, %r3, %r2		# 10722
	sto	%r1, (%r3)		# 10723
	jmpui	if_cont.16675		# 10724
else.16674:
	cmpeqi	%r7, %r7, 2		# 10725
	jmpzi	%r7, else.16676		# 10726
	sto	%r3, 12(%r63)		# 10727
	sto	%r5, 13(%r63)		# 10728
	mov	%r2, %r4		# 10729
	mov	%r1, %r6		# 10730
	addi	%r63, %r63, 14		# 10731
	call	%r60, setup_surface_table.2697		# 10732
	addi	%r63, %r63, -14		# 10733
	ld	%r2, 12(%r63)		# 10734
	ld	%r3, 13(%r63)		# 10735
	add	%r3, %r3, %r2		# 10736
	sto	%r1, (%r3)		# 10737
	jmpui	if_cont.16677		# 10738
else.16676:
	sto	%r3, 12(%r63)		# 10739
	sto	%r5, 13(%r63)		# 10740
	mov	%r2, %r4		# 10741
	mov	%r1, %r6		# 10742
	addi	%r63, %r63, 14		# 10743
	call	%r60, setup_second_table.2700		# 10744
	addi	%r63, %r63, -14		# 10745
	ld	%r2, 12(%r63)		# 10746
	ld	%r3, 13(%r63)		# 10747
	add	%r3, %r3, %r2		# 10748
	sto	%r1, (%r3)		# 10749
if_cont.16677:
if_cont.16675:
	addi	%r2, %r2, -1		# 10750
	ld	%r1, 11(%r63)		# 10751
	addi	%r63, %r63, 14		# 10752
	call	%r60, iter_setup_dirvec_constants.2703		# 10753
	addi	%r63, %r63, -14		# 10754
	jmpui	if_cont.16673		# 10755
else.16672:
if_cont.16673:
	movi	%r2, 118		# 10756
	ld	%r1, 10(%r63)		# 10757
	addi	%r63, %r63, 14		# 10758
	call	%r60, init_dirvec_constants.2921		# 10759
	addi	%r63, %r63, -14		# 10760
	movi	%r1, min_caml_dirvecs		# 10761
	addi	%r1, %r1, 3		# 10762
	ld	%r1, (%r1)		# 10763
	movi	%r2, 119		# 10764
	addi	%r63, %r63, 14		# 10765
	call	%r60, init_dirvec_constants.2921		# 10766
	addi	%r63, %r63, -14		# 10767
	movi	%r1, 2		# 10768
	addi	%r63, %r63, 14		# 10769
	call	%r60, init_vecset_constants.2924		# 10770
	addi	%r63, %r63, -14		# 10771
	movi	%r1, min_caml_light_dirvec		# 10772
	ld	%r1, (%r1)		# 10773
	movi	%r2, min_caml_light		# 10774
	addi	%r3, %r2, 0		# 10775
	ld	%r3, (%r3)		# 10776
	addi	%r4, %r1, 0		# 10777
	sto	%r3, (%r4)		# 10778
	addi	%r3, %r2, 1		# 10779
	ld	%r3, (%r3)		# 10780
	addi	%r4, %r1, 1		# 10781
	sto	%r3, (%r4)		# 10782
	addi	%r2, %r2, 2		# 10783
	ld	%r2, (%r2)		# 10784
	addi	%r1, %r1, 2		# 10785
	sto	%r2, (%r1)		# 10786
	movi	%r1, min_caml_light_dirvec		# 10787
	movi	%r2, min_caml_n_objects		# 10788
	addi	%r2, %r2, 0		# 10789
	ld	%r2, (%r2)		# 10790
	addi	%r2, %r2, -1		# 10791
	addi	%r63, %r63, 14		# 10792
	call	%r60, iter_setup_dirvec_constants.2703		# 10793
	addi	%r63, %r63, -14		# 10794
	movi	%r1, min_caml_n_objects		# 10795
	addi	%r1, %r1, 0		# 10796
	ld	%r1, (%r1)		# 10797
	addi	%r1, %r1, -1		# 10798
	movi	%r2, 0		# 10799
	cmpgt	%r2, %r2, %r1		# 10800
	jmpi	%r2, else.16678		# 10801
	movi	%r2, min_caml_objects		# 10802
	add	%r2, %r2, %r1		# 10803
	ld	%r2, (%r2)		# 10804
	ld	%r3, 2(%r2)		# 10805
	cmpeqi	%r3, %r3, 2		# 10806
	jmpzi	%r3, else.16680		# 10807
	ld	%r3, 7(%r2)		# 10808
	addi	%r3, %r3, 0		# 10809
	ld	%r3, (%r3)		# 10810
	movhiz	%r4, 260096		# 10811
	cmpfgt	%r4, %r4, %r3		# 10812
	jmpi	%r4, else.16682		# 10813
	jmpui	if_cont.16683		# 10814
else.16682:
	ld	%r3, 1(%r2)		# 10815
	cmpeqi	%r4, %r3, 1		# 10816
	jmpzi	%r4, else.16684		# 10817
	addi	%r63, %r63, 14		# 10818
	call	%r60, setup_rect_reflection.2935		# 10819
	addi	%r63, %r63, -14		# 10820
	jmpui	if_cont.16685		# 10821
else.16684:
	cmpeqi	%r3, %r3, 2		# 10822
	jmpzi	%r3, else.16686		# 10823
	addi	%r63, %r63, 14		# 10824
	call	%r60, setup_surface_reflection.2938		# 10825
	addi	%r63, %r63, -14		# 10826
	jmpui	if_cont.16687		# 10827
else.16686:
if_cont.16687:
if_cont.16685:
if_cont.16683:
	jmpui	if_cont.16681		# 10828
else.16680:
if_cont.16681:
	jmpui	if_cont.16679		# 10829
else.16678:
if_cont.16679:
	movi	%r2, 0		# 10830
	movi	%r3, 0		# 10831
	ld	%r1, 5(%r63)		# 10832
	addi	%r63, %r63, 14		# 10833
	call	%r60, pretrace_line.2867		# 10834
	addi	%r63, %r63, -14		# 10835
	movi	%r2, 0		# 10836
	movi	%r3, 2		# 10837
	movi	%r1, min_caml_image_size		# 10838
	addi	%r1, %r1, 1		# 10839
	ld	%r1, (%r1)		# 10840
	cmpgti	%r1, %r1, 0		# 10841
	jmpi	%r1, else.16688		# 10842
	addi	%r63, %r63, -1		# 10843
	ld	%r60, (%r63)		# 10844
	jmpu	%r60		# 10845
else.16688:
	movi	%r1, min_caml_image_size		# 10846
	addi	%r1, %r1, 1		# 10847
	ld	%r1, (%r1)		# 10848
	addi	%r1, %r1, -1		# 10849
	cmpgti	%r1, %r1, 0		# 10850
	sto	%r2, 14(%r63)		# 10851
	jmpi	%r1, else.16690		# 10852
	jmpui	if_cont.16691		# 10853
else.16690:
	movi	%r1, 1		# 10854
	ld	%r4, 7(%r63)		# 10855
	mov	%r2, %r1		# 10856
	mov	%r1, %r4		# 10857
	addi	%r63, %r63, 15		# 10858
	call	%r60, pretrace_line.2867		# 10859
	addi	%r63, %r63, -15		# 10860
if_cont.16691:
	movi	%r1, 0		# 10861
	ld	%r2, 14(%r63)		# 10862
	ld	%r3, 3(%r63)		# 10863
	ld	%r4, 5(%r63)		# 10864
	ld	%r5, 7(%r63)		# 10865
	addi	%r63, %r63, 15		# 10866
	call	%r60, scan_pixel.2871		# 10867
	addi	%r63, %r63, -15		# 10868
	movi	%r1, 1		# 10869
	movi	%r5, 4		# 10870
	ld	%r2, 5(%r63)		# 10871
	ld	%r3, 7(%r63)		# 10872
	ld	%r4, 3(%r63)		# 10873
	addi	%r63, %r63, -1		# 10874
	ld	%r60, (%r63)		# 10875
	jmpui	scan_line.2877		# 10876
.globl	min_caml_native_read.12550
min_caml_native_read.12550:
	in	%r1		# 10877
	jmpu	%r60		# 10878
.globl	min_caml_native_fabs.12551
min_caml_native_fabs.12551:
	fabs	%r1, %r1		# 10879
	jmpu	%r60		# 10880
.globl	min_caml_native_fneg.12552
min_caml_native_fneg.12552:
	fneg	%r1, %r1		# 10881
	jmpu	%r60		# 10882
.globl	min_caml_native_fsqrt.12553
min_caml_native_fsqrt.12553:
	fsqrt	%r1, %r1		# 10883
	jmpu	%r60		# 10884
.globl	min_caml_native_out.12554
min_caml_native_out.12554:
	out	%r1		# 10885
	jmpu	%r60		# 10886
