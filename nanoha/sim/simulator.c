#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <signal.h>


extern uint32_t finv(uint32_t);
extern uint32_t fsqrt(uint32_t);
extern void print_as(unsigned int pc, int v);

unsigned int memory[1048576];
unsigned int rst[64];
unsigned int pc = 0;
unsigned int previouspc = 0;
unsigned int outflag = 0;
unsigned int breakflag = 0;
unsigned int inflag  = 0;
unsigned int asflag = 0;
volatile unsigned int signalflag = 0;
FILE *infrom = NULL;

unsigned long long counter = 0;

unsigned long long add  = 0;
unsigned long long addi = 0;
unsigned long long sub = 0;
unsigned long long subi = 0;
unsigned long long rshift = 0;
unsigned long long rshifti = 0;
unsigned long long lshift = 0;
unsigned long long lshifti = 0;
unsigned long long compeq = 0;
unsigned long long compeqi = 0;
unsigned long long compgt = 0;
unsigned long long compgti = 0;
unsigned long long complt = 0;
unsigned long long complti = 0;
unsigned long long compfeq = 0;
unsigned long long compfeqi = 0;
unsigned long long compfgt = 0;
unsigned long long compfgti = 0;
unsigned long long fneg = 0;
unsigned long long fabs_ = 0;
unsigned long long movi = 0;
unsigned long long movhiz = 0;
unsigned long long fadd = 0;
unsigned long long faddi = 0;
unsigned long long fsub = 0;
unsigned long long fsubi = 0;
unsigned long long fmul = 0;
unsigned long long fmuli = 0;
unsigned long long finv_ = 0;
unsigned long long fsqrt_ = 0;
unsigned long long ld1 = 0;
unsigned long long ld2 = 0;
unsigned long long sto1 = 0;
unsigned long long sto2 = 0;
unsigned long long jmpz = 0;
unsigned long long jmpzi = 0;
unsigned long long jmp = 0;
unsigned long long jmpi = 0;
unsigned long long call = 0;
unsigned long long out = 0;
unsigned long long in = 0;
unsigned long long halt = 0;
unsigned long long total = 0;

union mydata{
  unsigned int i;
  float f;
};

union mydata data1;
union mydata data2;
union mydata data3;

int instruction (unsigned int op){
  unsigned int opcode,p,q,r,i14,i20,fimm,sign,addr,pcflag = 0;
  int v = 0;
  rst[0] = 0;
  opcode = op >> 26;
  p = (op & 0x03f00000) >> 20;
  q = (op & 0x000fc000) >> 14;
  r = (op &0x00003f00) >> 8;
  i14 = op & 0x00003fff;
  i20 = op & 0x000fffff;
  fimm = i14 << 18;
  addr = i20;
  if ((i14 >> 13) == 1){
    i14 = i14 | 0xffffc000;
  }
  if ((i20 >> 19) == 1){
    i20 = i20 | 0xfff00000;
  }
  switch (opcode) {
  case 0://add
    rst[p] = rst[q] + rst[r];
    add++;
    break;

  case 1://addi
    rst[p] = rst[q] + i14;
    addi++;
    break;

  case 2://sub
    rst[p] = rst[r] - rst[q];
    sub++;
    break;

  case 3://subi
    rst[p] = i14 - rst[q];
    subi++;
    break;

  case 4://rshift
    sign = rst[r] >> 31;
    rshift++;
    if (sign == 0){
      rst[p] = rst[q] >> (rst[r] & 31);
    }else{
      rst[p] = rst[q] << ((rst[r] * -1) & 31);
    }
    break;

  case 5://rshifti
    sign = i14 >> 31;
    rshifti++;
    if (sign == 0){
      rst[p] = rst[q] >> (i14 & 31);
    }else{
      rst[p] = rst[q] << ((i14 * -1) & 31);
    }
    break;

  case 6://lshift
    sign = rst[r] >> 31;
    lshift++;
    if (sign == 0){
      rst[p] = rst[q] << (rst[r] & 31);
    }else{
      rst[p] = rst[q] >> ((rst[r] * -1) & 31);
    }
    break;
  
  case 7://lshifti
    sign = i14 >> 31;
    lshifti++;
    if (sign == 0){
      rst[p] = rst[q] << (i14 & 31);
    }else{
      rst[p] = rst[q] >> ((i14 * -1) & 31);
    }
    break;

  case 8://compeq
    compeq++;
    if(rst[q] == rst[r]){
      rst[p] = 1;
    }else{
      rst[p] = 0;
    }
    break;

  case 9://compeqi
    compeqi++;
    if((int)rst[q] == (int)i14){
      rst[p] = 1;
    }else{
      rst[p] = 0;
    }
    break;

  case 10://compgt
    compgt++;
    if((int)rst[q] > (int)rst[r]){
      rst[p] = 1;
    }else{
      rst[p] = 0;
    }
    break;

  case 11://compgti
    compgti++;
    if((int)rst[q] > (int)i14){
      rst[p] = 1;
    }else{
      rst[p] = 0;
    }
    break;

  case 12://complt
    complt++;
    if((int)rst[q] < (int)rst[r]){
      rst[p] = 1;
    }else{
      rst[p] = 0;
    }
    break;

  case 13://complti
    complti++;
    if((int)rst[q] < (int)i14){
      rst[p] = 1;
    }else{
      rst[p] = 0;
    }
    break;

  case 14://compfeq
    compfeq++;
    data1.i = rst[q];
    data2.i = rst[r];
    if (data1.f == data2.f){
      rst[p] = 1;
    }else{
      rst[p] = 0;
    }
    break;

  case 15://compfeqi
    compfeqi++; 
    data1.i = rst[q];
    data2.i = i14;
    if(data1.f == data2.f){
      rst[p] = 1;
    }else{
      rst[p] = 0;
    }
    break;

  case 16://compfgt
    compfgt++;
    data1.i = rst[q];
    data2.i = rst[r];
    if(data1.f > data2.f){
      rst[p] = 1;
    }else{
      rst[p] = 0;
    }
    break;

  case 17://compfgti
    compfgti++;
    data1.i = rst[q];
    data2.i = i14;
    if(data1.f > data2.f){
      rst[p] = 1;
    }else{
      rst[p] = 0;
    }
    break;

  case 19://fneg
    sign = rst[q] >> 31;
    fneg++;
    if (sign == 0){
      rst[p] = rst[q] | 0x80000000;
    }else{
      rst[p] = (rst[q] << 1) >> 1;
    }
    break;

  case 21://fabs
    rst[p] = (rst[q] << 1) >> 1;
    fabs_++;
    break;

  case 22://movi
    rst[p] = i20;
    movi++;
    break;

  case 23://movhiz
    rst[p] = i20 << 12;
    movhiz++;
    break;

  case 24://fadd
    data2.i = rst[q];
    data3.i = rst[r];
    data1.f = data2.f + data3.f;
    rst[p] = data1.i;
    fadd++;
    break;

  case 25://faddi
    data2.i = rst[q];
    data3.i = fimm;
    data1.f = data2.f + data3.f;
    rst[p] = data1.i;
    faddi++;
    break;

  case 26://fsub
    data2.i = rst[q];
    data3.i = rst[r];
    data1.f = data3.f - data2.f;
    rst[p] = data1.i;
    fsub++;
    break;

  case 27://fsubi
    data2.i = rst[q];
    data3.i = fimm;
    data1.f = data3.f - data2.f;
    rst[p] = data1.i;
    fsubi++;
    break;

  case 28://fmul
    data2.i = rst[q];
    data3.i = rst[r];
    data1.f = data2.f * data3.f;
    rst[p] = data1.i;
    fmul++;
    break;

  case 29://fmuli
    data2.i = rst[q];
    data3.i = fimm;
    data1.f = data2.f * data3.f;
    rst[p] = data1.i;
    fmuli++;
    break;

  case 30://finv
    rst[p] = finv(rst[q]);
    finv_++;
    break;

  case 31://fsqrt
    rst[p] = fsqrt(rst[q]);
    fsqrt_++;
    break;

  case 32://ld1
    rst[p] = memory[addr];
    ld1++;
    break;

  case 33://ld2
    rst[p] = memory[(rst[q] + i14) & 0x000fffff];
    ld2++;
    break;

  case 36://sto1
    memory[addr] = rst[p];
    sto1++;
    break;

  case 37://sto2
    memory[(rst[q] + i14) & 0x000fffff] = rst[p];
    sto2++;
    break;

  case 48://jmpz
    jmpz++;
    if(rst[p] == 0){
      pc = rst[q];
      pcflag = 1;
    }
    break;

  case 49://jmpzi
    jmpzi++;
    if(rst[p] == 0){
      pc = i20;
      pcflag = 1;
    }
    break;

  case 50://jmp
    jmp++;
    if(rst[p] != 0){
      pc = rst[q];
      pcflag = 1;
    }
    break;

  case 51://jmpi
    jmpi++;
    if(rst[p] != 0){
      pc = i20;
      pcflag = 1;
    }
    break;

  case 52://call
    call++;
    rst[p] = pc + 1;
    pc = i20;
    pcflag = 1;
    break;

  case 60://out
    out++;
    unsigned char output;
    output = rst[p] & 0xff;
    if (outflag == 0){
      fprintf(stdout, "%c", (unsigned int)output);
    }else{
      fprintf(stdout, "%02x", (unsigned int)output);
    }      
    break;

  case 61://in
    in++;
    unsigned char input;
    if(inflag == 0){
      if(fscanf(stdin, "%c", &input) == EOF){
        perror("in1");
      }
    }else{
      if(fscanf(infrom, "%c", &input) == EOF){
        perror("in2");
      }
    } 
    rst[p] = input & 0xff;
    break;
      
  case 63://halt
      halt++;
    pcflag = 1;
    v = 1;
    if(asflag == 1){
      fprintf(stderr, "[%10llu] :   ", total);
      fprintf(stderr, "halt\n");
    }
    break;

  default:
    break;
  }
  if(pcflag == 0){
    pc++;
  }
  
  return v;
}

void isaprint(int printflag, int countflag){
  
  fprintf(stderr, "total   : %llu\n", ++total);
  if (countflag != 0){
    fprintf(stderr, "counter : %llu\n", counter);
  }
  if (printflag != -1){
    fprintf(stderr, "\nadd     : %llu\n", add);
    fprintf(stderr, "addi    : %llu\n", addi);
    fprintf(stderr, "sub     : %llu\n", sub);
    fprintf(stderr, "subi    : %llu\n", subi);
    fprintf(stderr, "rshift  : %llu\n", rshift);
    fprintf(stderr, "rshifti : %llu\n", rshifti);
    fprintf(stderr, "lshift  : %llu\n", lshift);
    fprintf(stderr, "lshifti : %llu\n", lshifti);
    fprintf(stderr, "compeq  : %llu\n", compeq);
    fprintf(stderr, "compeqi : %llu\n", compeqi);
    fprintf(stderr, "compgt  : %llu\n", compgt);
    fprintf(stderr, "compgti : %llu\n", compgti);
    fprintf(stderr, "complt  : %llu\n", complt);
    fprintf(stderr, "complti : %llu\n", complti);
    fprintf(stderr, "compfeq : %llu\n", compfeq);
    fprintf(stderr, "compfeqi: %llu\n", compfeqi);
    fprintf(stderr, "compfgt : %llu\n", compfgt);
    fprintf(stderr, "compfgti: %llu\n", compfgti);
    fprintf(stderr, "fneg    : %llu\n", fneg);
    fprintf(stderr, "fabs    : %llu\n", fabs_);
    fprintf(stderr, "movi    : %llu\n", movi);
    fprintf(stderr, "movhiz  : %llu\n", movhiz);
    fprintf(stderr, "fadd    : %llu\n", fadd);
    fprintf(stderr, "faddi   : %llu\n", faddi);
    fprintf(stderr, "fsub    : %llu\n", fsub);
    fprintf(stderr, "fsubi   : %llu\n", fsubi);
    fprintf(stderr, "fmul    : %llu\n", fmul);
    fprintf(stderr, "fmuli   : %llu\n", fmuli);
    fprintf(stderr, "finv    : %llu\n", finv_);
    fprintf(stderr, "fsqrt   : %llu\n", fsqrt_);
    fprintf(stderr, "ld1     : %llu\n", ld1);
    fprintf(stderr, "ld2     : %llu\n", ld2);
    fprintf(stderr, "sto1    : %llu\n", sto1);
    fprintf(stderr, "sto2    : %llu\n", sto2);
    fprintf(stderr, "jmpz    : %llu\n", jmpz);
    fprintf(stderr, "jmpzi   : %llu\n", jmpzi);
    fprintf(stderr, "jmp     : %llu\n", jmp);
    fprintf(stderr, "jmpi    : %llu\n", jmpi);
    fprintf(stderr, "call    : %llu\n", call);
    fprintf(stderr, "out     : %llu\n", out);
    fprintf(stderr, "in      : %llu\n", in);
    fprintf(stderr, "halt    : %llu\n", halt);
  }
}

int breakf(int breakpoint){
  int v = 0;
  char s[10];
  fprintf(stderr, "=======PC : %d=======\n", pc);
  fprintf(stderr, "press \"mmr\" or \"reg\" or \"step\" , if you want continue, \"skip\" : ");
  
  while(scanf("%s",s) != EOF && strcmp(s,"skip") != 0 && strcmp(s,"skipi") != 0){//skipの時以外この中
    if (strcmp(s, "reg") == 0){//メモリの中身をint型で表示
      int j;
      for(j=0;j<64;j++){
	fprintf(stderr,"reg %2d : %-10d",j, rst[j]);
	if(j%8 == 7){
	  fprintf(stderr, "\n");
	}
      }
    }

    if(strcmp(s, "regf") == 0){//メモリの中身をfloat型で表示
      int j;
      for(j=0;j<64;j++){
	data1.i = rst[j];
	fprintf(stderr,"reg %2d : %-10e    ", j, data1.f);
	if(j%4 == 3){
	  fprintf(stderr, "\n");
	}
      }
    }
    
    if(strcmp(s, "regx") == 0){//メモリの中身を16進数型で表示
      int j;
      for(j=0;j<64;j++){
	fprintf(stderr,"reg %2d : %-9x", j,rst[j]);
	if(j%8 == 7){
	  fprintf(stderr, "\n");
	}
      }
    }
    
    if (strcmp(s, "mmr") == 0){//メモリの中身をintで表示
      fprintf(stderr, "show memory mode\ngive me memory's number : ");
      int i = 0;
      while(scanf("%d", &i) != EOF && i > -1){
	fprintf(stderr, "memory[%d] : %d\n",i,memory[i]);
	fprintf(stderr, "give me memory's number : ");	
      }
    }

    if (strcmp(s, "mmrx") == 0){//メモリの中身をhexで表示
      fprintf(stderr, "show memoryx mode\ngive me memory's number : ");
      int i = 0;
      while(scanf("%d", &i) != EOF && i > -1){
	fprintf(stderr, "memory[%d] : %x\n",i,memory[i]);
	fprintf(stderr, "give me memory's number : ");	
      }
    }

    if (strcmp(s, "mmrf") == 0){//メモリの中身をfloatで表示
      fprintf(stderr, "show memoryf mode\ngive me memory's number : ");
      int i = 0;
      while(scanf("%d", &i) != EOF && i > -1){
	data1.i = memory[i];
	fprintf(stderr, "memory[%d] : %e\n",i, data1.f);
	fprintf(stderr, "give me memory's number : ");	
      }
    }

    if (strcmp(s,"step") == 0){//逐次実行
      int n;
      scanf("%d",&n);
      int i;
      for (i = 0; i<n;i++){
        if(instruction(memory[pc]) == 1){
          halt--;
          fprintf(stderr,"halt\n");
        }else{
          v++;
          print_as(previouspc, v);
          previouspc = pc;
        }
      }
    } 
    fprintf(stderr, "=======PC : %d=======\n", pc);
    fprintf(stderr, "press \"mmr\" or \"reg\" or \"step\" , if you want continue, \"skip\" : ");
  }

  if (strcmp(s,"skipi") == 0){
    breakflag = 0;
  }

  fprintf(stderr,"========================================================\n");

  return v;

}

typedef void (*sighandler_t)(int);

sighandler_t trap_signal (int sig, sighandler_t handler)
{
  struct sigaction act, old;
  
  act.sa_handler = handler;
  sigemptyset(&act.sa_mask);
  act.sa_flags = SA_RESTART;
  
  if (sigaction(sig, &act, &old) < 0){
    return NULL;
  }
  
  return old.sa_handler;
}

void handler (int sig){
  signalflag = 1;
}


  


int main(int argc, char  *argv[]){
  unsigned int filesize;
  FILE *fd;
  int i, readsize = 0;
  unsigned int opt,printflag=0,countpoint = 0,countflag = 0,breakpoint = 0;

  while((opt = getopt(argc, argv, "b:c:i:ex:la")) != -1){//オプションの処理
    switch(opt){
    case 'e'://print only total 
      printflag = -1;
      break;
    case 'x'://out with hex
      outflag = 1;
      break;
    case 'c'://count mode
      countpoint = atoi(optarg);
      countflag = 1;
      break;
    case 'b'://breakpoint mode
      breakpoint = atoi(optarg);
      breakflag = 1;
      break;
    case 'i'://in from file
      infrom = fopen(optarg, "r");
      if(!infrom){
        perror(optarg);
        exit(1);
      }
      inflag = 1;
      break;
    case 'l'://change ^c; quit looping program after print numbers of instructions
      trap_signal(SIGINT, handler);
      break;
    case 'a':
      asflag = 1;
      break;
    case '?':
      break;
    }
  }

  if ((fd = fopen(argv[optind], "rb")) == NULL){//引数で与えられたファイルの読み込み
    fprintf(stderr,"failed to open file.\n");
    exit(1);
  }  

  if (fread(&filesize, sizeof(int), 1, fd) != 1){//最初の４バイトの読み込み
    fprintf(stderr,"failed to read the first byte.\n");
    exit(1);
  }
  
  while(readsize != filesize){//命令の書き込み
    i = fread(&memory[readsize], sizeof(int), 1, fd);
    if (i != 1){
      fprintf(stderr,"failed to read,\n");
      break;
    }
    readsize++;
  }

  while(instruction(memory[pc]) != 1){//do instructions
    if(pc >= filesize){
      fprintf(stderr, "Program Counter is too large. Instruction size is %d, but you tried to access to memory[%d].\n",filesize,pc);
      fprintf(stderr, "Previous PC is %d\n", previouspc);
      exit(1);
    }

    if (asflag == 1){
      print_as(previouspc, 0);
    }

    previouspc = pc;

    if(pc == countpoint){//use counterpoint
      ++counter;
    }

    if (breakflag == 1 && breakpoint+1 == pc){//use breakpoint
      total = total + breakf(breakpoint+1);
    }
    total++;
    
    if(signalflag == 1){
      break;
    }
  }

  fprintf(stderr,"\n");
  fprintf(stderr, "filesize : %d\n", filesize);//print filesize
  isaprint(printflag,countflag); //命令数の表示 flagが-1の時はtotalのみ

  if(inflag == 1){
    fclose(infrom);
  }

  fclose(fd); 

  return 0;
}


void print_as(unsigned int pc, int v)//v is for moving correctly in breakpoint.
{
  unsigned int op,opcode,p,q,r,i14,i20,fimm,addr;
  op = memory[pc];
  opcode = op >> 26;
  p = (op & 0x03f00000) >> 20;
  q = (op & 0x000fc000) >> 14;
  r = (op &0x00003f00) >> 8;
  i14 = op & 0x00003fff;
  i20 = op & 0x000fffff;
  fimm = i14 << 18;
  addr = i20;

 if ((i14 >> 13) == 1){
    i14 = i14 | 0xffffc000;
  }
  if ((i20 >> 19) == 1){
    i20 = i20 | 0xfff00000;
  }

  fprintf(stderr, "[%10llu] [pc :%10u]   :   ", total + v, pc);

  switch (opcode) {
  case 0://add
    fprintf(stderr, "add %%r%u, %%r%u, %%r%u\n", p, q, r);
    break;

  case 1://addi
    fprintf(stderr, "addi %%r%u, %%r%u, %d\n", p, q, i14);
    //rst[p] = rst[q] + i14;
    break;

  case 2://sub
    fprintf(stderr, "sub %%r%u, %%r%u, %%r%u\n", p, r, q);
    // rst[p] = rst[r] - rst[q];
    break;

  case 3://subi
    fprintf(stderr, "subi %%r%u, %d, %%r%u\n", p, i14, q);
    //rst[p] = i14 - rst[q];
    break;

  case 4://rshift
    fprintf(stderr, "rshift %%r%u, %%r%u, %%r%u\n", p, q, r);
    //rst[p] = rst[q] << ((rst[r] * -1) & 31);
    break;

  case 5://rshifti
    fprintf(stderr, "rshifti %%r%u, %%r%u, %d\n", p, q, i14);
    //      rst[p] = rst[q] >> (i14 & 31);
    break;

  case 6://lshift
    fprintf(stderr, "lshift %%r%u, %%r%u, %%r%u\n", p, q, r);
    //rst[p] = rst[q] << (rst[r] & 31);
    //rst[p] = rst[q] >> ((rst[r] * -1) & 31);
    break;
  
  case 7://lshifti
    fprintf(stderr, "lshifti %%r%u, %%r%u, %d\n", p, q, i14);
    //rst[p] = rst[q] << (i14 & 31);
    //rst[p] = rst[q] >> ((i14 * -1) & 31);
    break;

  case 8://compeq
    fprintf(stderr, "cmpeq %%r%u, %%r%u, %%r%u\n", p, q, r);
    break;

  case 9://compeqi
    fprintf(stderr, "cmpeqi %%r%u, %%r%u, %d\n", p, q, i14);
    //    if((int)rst[q] == (int)i14){
    break;

  case 10://compgt
    fprintf(stderr, "cmpgt %%r%u, %%r%u, %%r%u\n", p, q, r);
    //if((int)rst[q] > (int)rst[r]){
    break;

  case 11://compgti
    fprintf(stderr, "cmpgti %%r%u, %%r%u, %d\n", p, q, i14);
    //if((int)rst[q] > (int)i14){
    break;

  case 12://complt
    fprintf(stderr, "cmplt %%r%u, %%r%u, %%r%u\n", p, q, r);
    //if((int)rst[q] < (int)rst[r]){
    break;

  case 13://complti
    fprintf(stderr, "cmplti %%r%u, %%r%u, %d\n", p, q, i14);
    //if((int)rst[q] < (int)i14){
    break;

  case 14://compfeq
    fprintf(stderr, "cmpfeq %%r%u, %%r%u, %%r%u\n", p, q, r);
    break;

  case 15://compfeqi
    fprintf(stderr, "cmpfeqi %%r%u, %%r%u, 0x%x\n", p, q, i14);
    break;

  case 16://compfgt
    fprintf(stderr, "cmpfgt %%r%u, %%r%u, %%r%u\n", p, q, r);
    break;

  case 17://compfgti
    fprintf(stderr, "cmpfgti %%r%u, %%r%u, 0x%x\n", p, q, i14);
    break;

  case 19://fneg
    fprintf(stderr, "fneg %%r%u, %%r%u\n", p, q);
    break;

  case 21://fabs
    fprintf(stderr, "fabs %%r%u, %%r%u\n", p, q);
    //rst[p] = (rst[q] << 1) >> 1;
    break;

  case 22://movi
    fprintf(stderr, "movi %%r%u, %d\n", p, i20);
    //rst[p] = i20;
    break;

  case 23://movhiz
    fprintf(stderr, "movhiz %%r%u, %d\n", p, i20);
    //rst[p] = i20 << 12;
    break;

  case 24://fadd
    fprintf(stderr, "fadd %%r%u, %%r%u, %%r%u\n", p, q, r);
    break;

  case 25://faddi
    fprintf(stderr, "faddi %%r%u, %%r%u, 0x%x\n", p, q, (i14 & 0x3fff));
    break;

  case 26://fsub
    fprintf(stderr, "fsub %%r%u, %%r%u, %%r%u\n", p, r, q);
    break;

  case 27://fsubi
    fprintf(stderr, "fsubi %%r%u, 0x%x, %%r%u\n", p, (i14 & 0x3fff), q);
    break;

  case 28://fmul
    fprintf(stderr, "fmul %%r%u, %%r%u, %%r%u\n", p, q, r);
    break;

  case 29://fmuli
    fprintf(stderr, "fmuli %%r%u, %%r%u, 0x%x\n", p, q, (i14 & 0x3fff));
    break;

  case 30://finv
    fprintf(stderr, "finv %%r%u, %%r%u\n", p, q);
    //rst[p] = finv(rst[q]);
    break;

  case 31://fsqrt
    fprintf(stderr, "fsqrt %%r%u, %%r%u\n", p, q);
    //rst[p] = fsqrt(rst[q]);
    break;

  case 32://ld1
    fprintf(stderr, "ld %%r%u, 0x%x\n", p, addr);
    //rst[p] = memory[addr];
    break;

  case 33://ld2
    if(i14==0){
      fprintf(stderr, "ld %%r%u, (%%r%u)\n", p, q);
    }else{
      fprintf(stderr, "ld %%r%u, %d(%%r%u)\n", p, i14, r);
    }
    //rst[p] = memory[(rst[q] + i14) & 0x000fffff];
    break;

  case 36://sto1
    fprintf(stderr, "sto %%r%u, 0x%x\n", p, addr);
    //memory[addr] = rst[p];
    break;

  case 37://sto2
    if(i14 == 0){
      fprintf(stderr, "sto %%r%u, (%%r%u)\n", p, q);
    }else{
      fprintf(stderr, "sto %%r%u, %d(%%r%u)\n", p, i14, q);
    }
    //memory[(rst[q] + i14) & 0x000fffff] = rst[p];
    break;

  case 48://jmpz
    fprintf(stderr, "jmpz %%r%u, %%r%u\n", p, q);
    break;

  case 49://jmpzi
    fprintf(stderr, "jmpzi %%r%u, 0x%x\n", p, (i20 & 0xfffff));
    break;

  case 50://jmp
    fprintf(stderr, "jmp %%r%u, %%r%u\n", p, q);
    break;

  case 51://jmpi
    fprintf(stderr, "jmpi %%r%u, 0x%x\n", p, (i20 & 0xfffff));
    break;

  case 52://call
    fprintf(stderr, "call %%r%u, %d\n", p, i20);
    break;

  case 60://out
    fprintf(stderr, "out %%r%u\n", p);
    break;

  case 61://in
    fprintf(stderr, "in %%r%u\n", p);
    break;
      
  case 63://halt
    fprintf(stderr, "halt\n");
    break;

  default:
    break;
  }
}
