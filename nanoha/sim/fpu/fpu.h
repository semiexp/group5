#ifndef FPU_H
#define FPU_H

extern void myread(int, char*, int);
extern int bit_check(uint32_t, int);
extern uint64_t myround(uint64_t, int);

#endif
