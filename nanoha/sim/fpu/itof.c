#include <stdio.h>
#include <stdint.h>

uint32_t itof(uint32_t input)
{
	union f {
		float f;
		uint32_t u;
	};

	union f output;
	long l;
	if (input >= ((uint32_t)1<<31)) {
		l = -(long)(~input)-1;
	} else {
		l = input;
	}

	output.f = (float)l;

	return output.u;
}
