#include <stdio.h>
#include <stdint.h>
#include <math.h>

uint32_t ftoi(uint32_t input)
{
	union f {
		uint32_t u;
		float f;
	};

	union f in;

	in.u = input;
	return (int32_t)round(in.f);
}
